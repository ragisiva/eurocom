"use strict";
var stripe1;
$(document).ready(function () {
    var addresses = [];
    function razorpay_setup(key, amount, app_name, logo, razorpay_order_id, username, user_email, user_contact) {
        var options = {
            "key": key, // Enter the Key ID generated from the Dashboard
            "amount": (amount * 100), // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
            "currency": "INR",
            "name": app_name,
            "description": "Product Purchase",
            "image": logo,
            "order_id": razorpay_order_id, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
            "handler": function (response) {
                $('#razorpay_payment_id').val(response.razorpay_payment_id);
                $('#razorpay_signature').val(response.razorpay_signature);
                place_order().done(function (result) {
                    if (result.error == false) {
                        setTimeout(function () {
                            location.href = base_url + 'payment/success';
                        }, 3000);
                    }
                });
            },
            "prefill": {
                "name": username,
                "email": user_email,
                "contact": user_contact
            },
            "notes": {
                "address": app_name + " Purchase"
            },
            "theme": {
                "color": "#3399cc"
            },
            "escape": false,
            "modal": {
                "ondismiss": function () {
                    $('#place_order_btn').attr('disabled', false).html('Place Order');
                }
            }
        };
        var rzp = new Razorpay(options);
        return rzp;
    }

    function paystack_setup(key, user_email, order_amount) {
        var handler = PaystackPop.setup({
            key: key,
            email: user_email,
            amount: (order_amount * 100),
            currency: "NGN",
            callback: function (response) {
                console.log(response);
                $('#paystack_reference').val(response.reference);
                if (response.status == "success") {
                    place_order().done(function (result) {
                        if (result.error == false) {
                            setTimeout(function () {
                                location.href = base_url + 'payment/success';
                            }, 3000);
                        }
                    });
                } else {
                    location.href = base_url + 'payment/cancel';
                }
            },
            onClose: function () {
                $('#place_order_btn').attr('disabled', false).html('Place Order');
            }
        });
        return handler;
    }
    function stripe_setup(key) {
        // A reference to Stripe.js initialized with a fake API key.
        // Sign in to see examples pre-filled with your key.
        var stripe = Stripe(key);
        // Disable the button until we have Stripe set up on the page
        var elements = stripe.elements();
        var style = {
            base: {
                color: "#32325d",
                fontFamily: 'Arial, sans-serif',
                fontSmoothing: "antialiased",
                fontSize: "16px",
                "::placeholder": {
                    color: "#32325d"
                }
            },
            invalid: {
                fontFamily: 'Arial, sans-serif',
                color: "#fa755a",
                iconColor: "#fa755a"
            }
        };

        var card = elements.create("card", {
            style: style
        });
        card.mount("#stripe-card-element");

        card.on("change", function (event) {
            // Disable the Pay button if there are no card details in the Element
            document.querySelector("button").disabled = event.empty;
            document.querySelector("#card-error").textContent = event.error ? event.error.message : "";
        });
        return {
            'stripe': stripe,
            'card': card
        };
    }
    function stripe_payment(stripe, card, clientSecret) {
        // Calls stripe.confirmCardPayment
        // If the card requires authentication Stripe shows a pop-up modal to
        // prompt the user to enter authentication details without leaving your page.
        stripe.confirmCardPayment(clientSecret, {
            payment_method: {
                card: card
            }
        })
            .then(function (result) {
                if (result.error) {
                    // Show error to your customer
                    var errorMsg = document.querySelector("#card-error");
                    errorMsg.textContent = result.error.message;
                    setTimeout(function () {
                        errorMsg.textContent = "";
                    }, 4000);
                    Toast.fire({
                        icon: 'error',
                        title: result.error.message
                    });
                    $('#place_order_btn').attr('disabled', false).html('Place Order');
                } else {
                    // The payment succeeded!
                    place_order().done(function (result) {
                        if (result.error == false) {
                            setTimeout(function () {
                                location.href = base_url + 'payment/success';
                            }, 1000);
                        }
                    });
                }
            });
    };
    $("#checkout_form").on('submit', function (event) {
        event.preventDefault();
        var btn_html = $('#place_order_btn').html();
        $('#place_order_btn').attr('disabled', true).html('Please Wait...');
        if($('#is_time_slots_enabled').val()==1 && $('input[name="delivery_date"]').is(':checked')==false){
            Toast.fire({
                icon: 'error',
                title: "Please select Delivery Date & Time."
            });
            $('#place_order_btn').attr('disabled', false).html(btn_html);
            return false;
        }
        var payment_methods = $("input[name='payment_method']:checked").val();
        if (payment_methods == "Stripe") {
            var stripe_client_secret = $('#stripe_client_secret').val();
            stripe_payment(stripe1.stripe, stripe1.card, stripe_client_secret);
        } else if (payment_methods == "Paystack") {
            var key = $('#paystack_key_id').val();
            var user_email = $('#user_email').val();
            var order_amount = $('#order_amount').val();
            var handler = paystack_setup(key, user_email, order_amount);
            handler.openIframe();
        } else if (payment_methods == "Razorpay") {
            var key = $('#razorpay_key_id').val();
            var order_amount = $('#order_amount').val();
            var app_name = $('#app_name').val();
            var logo = $('#logo').val();
            var razorpay_order_id = $('#razorpay_order_id').val();
            var username = $('#username').val();
            var user_email = $('#user_email').val();
            var user_contact = $('#user_contact').val();
            var rzp1 = razorpay_setup(key, order_amount, app_name, logo, razorpay_order_id, username, user_email, user_contact);
            rzp1.open();
            rzp1.on('payment.failed', function (response) {
                location.href = base_url + 'payment/cancel';
            });
        } else if (payment_methods == "Paypal") {
            place_order().done(function (result) {
                $('#paypal_order_id').val(result.data.order_id);
                $('#csrf_token').val(csrfHash);
                $('#paypal_form').submit();
            });
        } else if (payment_methods == "COD") {
            place_order().done(function (result) {
                window.location.reload();
            });
        }
    });

    function place_order() {
        let myForm = document.getElementById('checkout_form');
        var formdata = new FormData(myForm);
        formdata.append(csrfName, csrfHash);
        formdata.append('promo_code', $('#promocode_input').val());
        return $.ajax({
            type: 'POST',
            data: formdata,
            url: base_url + 'cart/place-order',
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $('#place_order_btn').attr('disabled', true).html('Please Wait...');
            },
            success: function (data) {
                csrfName = data.csrfName;
                csrfHash = data.csrfHash;
                $('#place_order_btn').attr('disabled', false).html('Place Order');
                if (data.error == false) {
                    Toast.fire({
                        icon: 'success',
                        title: data.message
                    });
                } else {
                    Toast.fire({
                        icon: 'error',
                        title: data.message
                    });
                }
            }
        })
    }

    $("input[name='payment_method']").on('change', function (e) {
        e.preventDefault();
        if ($(this).val() == "Stripe") {
            $.post(base_url + "cart/pre-payment-setup", { [csrfName]: csrfHash, 'payment_method': 'Stripe' }, function (data) {
                console.log(data);
                $('#stripe_client_secret').val(data.client_secret);
                $('#stripe_payment_id').val(data.id);
                csrfName = data.csrfName;
                csrfHash = data.csrfHash;
            }, "json");
            stripe1 = stripe_setup($('#stripe_key_id').val());
            $('#stripe_div').slideDown();
        } else if ($(this).val() == "Razorpay") {
            $('#stripe_div').slideUp();
            $.post(base_url + "cart/pre-payment-setup", { [csrfName]: csrfHash, 'payment_method': 'Razorpay' }, function (data) {
                csrfName = data.csrfName;
                csrfHash = data.csrfHash;
                if (data.error == false) {
                    $('#razorpay_order_id').val(data.order_id);
                } else {
                    Toast.fire({
                        icon: 'error',
                        title: data.message
                    });
                }
            }, "json");
        } else {
            $('#stripe_div').slideUp();
        }
    });

    $("#redeem_btn").on('click', function (event) {
        event.preventDefault();
        var formdata = new FormData();
        formdata.append(csrfName, csrfHash);
        formdata.append('promo_code', $('#promocode_input').val());
        $.ajax({
            type: 'POST',
            data: formdata,
            url: base_url + 'cart/validate-promo-code',
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: false,
            success: function (data) {
                csrfName = data.csrfName;
                csrfHash = data.csrfHash;
                if (data.error == false) {
                    Toast.fire({
                        icon: 'success',
                        title: data.message
                    });
                    $('#promocode_div').removeClass('d-none');
                    $('#promocode').text(data.data[0].promo_code);
                    $('#promocode_amount').text(data.data[0].final_discount);
                    $('#final_total').text(data.data[0].final_total);
                    $('#clear_promo_btn').removeClass('d-none');
                    $('#redeem_btn').hide();

                } else {
                    Toast.fire({
                        icon: 'error',
                        title: data.message
                    });
                }
            }
        })
    });
    $("#clear_promo_btn").on('click', function (event) {
        event.preventDefault();
        $('#promocode_div').addClass('d-none');
        var promocode_amount = $('#promocode_amount').text();
        var final_total = $('#final_total').text();
        var new_final_total = parseInt(promocode_amount) + parseInt(final_total);
        $('#final_total').text(new_final_total);
        $('#clear_promo_btn').addClass('d-none');
        $('#redeem_btn').show();
        $('#promocode_input').val('');
    });
    /* Instantiating iziModal */
    $(".address-modal").iziModal({
        overlayClose: false,
        overlayColor: 'rgba(0, 0, 0, 0.6)',
        onOpening: function (modal) {
            modal.startLoading();
            $.ajax({
                type: 'POST',
                data: {
                    [csrfName]: csrfHash,
                },
                url: base_url + 'my-account/get-address/',
                dataType: 'json',
                success: function (data) {
                    csrfName = data.csrfName;
                    csrfHash = data.csrfHash;
                    var html = '';
                    if (data.error == false) {
                        $.each(data.data, function (i, e) {
                            addresses.push(e);
                            html += '<label for="select-address-' + e.id + '"><li class="list-group-item d-flex justify-content-between lh-condensed mt-3">' +
                                '<div class="col-md-1 h-100 my-auto">' +
                                '<input type="radio" class="select-address" name="select-address" data-index=' + i + ' id="select-address-' + e.id + '" class="m-0"/>' +
                                '</div>' +
                                '<div class="col-11 row p-0">' +
                                '<div class="col-6 text-dark"><i class="fa fa-map-marker-alt"></i> ' + e.name + ' - ' + e.type + '</div>' +
                                '<small class="col-12 text-muted">' + e.area + ' , ' + e.city + ' , ' + e.state + ' , ' + e.country + ' - ' + e.pincode + '</small>' +
                                '<small class="col-12 text-muted">' + e.mobile + '</small>' +
                                '</div>' +
                                '</li></label>';
                        });

                        $('#address-list').html(html);
                    }
                    modal.stopLoading();
                }
            })
        }
    });

    $(".address-modal").on('click', '.submit', function (event) {
        event.preventDefault();
        var index = $('input[class="select-address"]:checked').data('index');
        var address = addresses[index];
        $('#address-name-type').html(address.name + ' - ' + address.type);
        $('#address-full').html(address.area + ' , ' + address.city);
        $('#address-country').html(address.state + ' , ' + address.country + ' - ' + address.pincode);
        $('#address-mobile').html(address.mobile);
        $('#address_id').val(address.id);
        $('#mobile').val(address.mobile);
        $('.address-modal').iziModal('close');
    });
});

$('#datepicker').attr({
    'placeholder': 'Preferred Delivery Date',
    'autocomplete': 'off'
});
$('#datepicker').on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('');
    $('#start_date').val('');
});
$('#datepicker').on('apply.daterangepicker', function (ev, picker) {
    var drp = $('#datepicker').data('daterangepicker');
    var current_time = moment().format("HH:mm");
    if (moment(drp.startDate).isSame(moment(), 'd')) {
        $('.time-slot-inputs').each(function (i, e) {
            if ($(this).data('last_order_time') < current_time) {
                $(this).prop('checked',false).attr('required',false);
                $(this).parent().hide();
            } else {
                $(this).attr('required',true);
                $(this).parent().show();
            }
        });
    } else {
        $('.time-slot-inputs').each(function (i, e) {
            $(this).attr('required',true);
            $(this).parent().show();
        });
    }
    $('#start_date').val(drp.startDate.format('YYYY-MM-DD'));
    $(this).val(picker.startDate.format('MM/DD/YYYY'));
});
var mindate = '', maxdate = '';
if ($('#delivery_starts_from').val() != "") {
    mindate = moment().add(($('#delivery_starts_from').val() - 1), 'days');
} else {
    mindate = null;
}

if ($('#delivery_ends_in').val() != "") {
    maxdate = moment(mindate).add(($('#delivery_ends_in').val() - 1), 'days');
} else {
    maxdate = null;
}
$('#datepicker').daterangepicker({
    showDropdowns: false,
    alwaysShowCalendars: true,
    autoUpdateInput: false,
    singleDatePicker: true,
    minDate: mindate,
    maxDate: maxdate,
    locale: {
        "format": "DD/MM/YYYY",
        "separator": " - ",
        "cancelLabel": 'Clear',
        'label': 'Preferred Delivery Date'
    }
});