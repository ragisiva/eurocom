const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
});
(function($) {
    "use strict"; // Start of use strict
    //Document Ready

    jQuery(document).ready(function() {
        //Back To Top
        $('.back-to-top').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        });
        //Menu Responsive
        if ($(window).width() < 768) {
            $('body').on('click', function(event) {
                $('.main-nav>ul').slideUp('slow');
            });
            $('.toggle-mobile-menu').on('click', function(event) {
                event.preventDefault();
                event.stopPropagation();
                $('.main-nav>ul').slideToggle('slow');
            });
            $('.main-nav li.menu-item-has-children>a').on('click', function(event) {
                event.preventDefault();
                event.stopPropagation();
                $(this).next().slideToggle('slow');
            });

            $('.top-info li.has-child>a').on('click', function(event) {
                event.preventDefault();
                event.stopPropagation();
                $(this).next().toggle();
            })
        }

        //Accordions
        if ($('.accordion-box').length > 0) {
            $('.accordion-box').each(function() {
                $('.title-accordion').click(function() {
                    $(this).parent().parent().find('.item-accordion').removeClass('active');
                    $(this).parent().addClass('active');
                    $(this).parent().parent().find('.desc-accordion').stop(true, true).slideUp();
                    $(this).next().stop(true, true).slideDown();
                });
            });
        }
        //Toggle Filter
        $('body').on('click', function() {
            $('.box-product-filter').slideUp('slow');
        });
        $('.toggle-link-filter').on('click', function(event) {
            event.stopPropagation();
            event.preventDefault();
            $('.box-product-filter').slideToggle('slow');
        });
        //Product Quick View
        $('.quickview-link').each(function() {
            $(this).fancybox();
        });
        $('.team-gallery-thumb').each(function() {
            $(this).fancybox();
        });
        //Faqs Widget
        $('.list-post-faq li h3').on('click', function(event) {
            $('.list-post-faq li').removeClass('active');
            $(this).parent().addClass('active');
        });
        //World Hover Dir
        $('.world-ad-box').each(function() {
            $(this).hoverdir();
        });
        //Close Adv
        $('.adv-close-link').on('click', function(event) {
            event.preventDefault();
            $('.adv-close').slideUp('slow');
        });
        //Category Hover
        $('.list-category-hover>li:gt(12)').hide();
        $('.expand-list-link').on('click', function(event) {
            event.preventDefault();
            $(this).toggleClass('expanding');
            $('.list-category-hover>li:gt(12)').slideToggle('slow');
        });
        $('.list-category-hover a').on('mouseover', function() {
            var id_hv = $(this).attr('data-id');
            /* console.log(id_hv); */
            $('.inner-right-category-hover').each(function() {
                if ($(this).attr('id') == id_hv) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            })

        });
        //Category Toggle 
        // $('.list-category-dropdown >li:gt(9)').hide();
        // $('.expand-category-link').on('click',function(event) {
        // 	event.preventDefault();
        // 	$(this).toggleClass('expanding');
        // 	$('.list-category-dropdown >li:gt(9)').slideToggle('slow');
        // });

        //Category Toggle Home 8
        // $('.list-category-dropdown8 >li:gt(10)').hide();
        // $('.expand-category-link8').on('click',function(event) {
        // 	event.preventDefault();
        // 	$(this).toggleClass('expanding');
        // 	$('.list-category-dropdown8 >li:gt(10)').slideToggle('slow');
        // });

        $('.category-dropdown').each(function() {
            var items = $(this).data('items');
            if (typeof(items) == 'undefined') {
                items = 10;
            }
            items = items - 1;
            $(this).find('.list-category-dropdown >li:gt(' + items + ')').hide();
        });

        $('.expand-category-link').on('click', function(event) {

            event.preventDefault();
            $(this).toggleClass('expanding');
            var wapper = $(this).closest('.category-dropdown');
            var items = wapper.data('items');
            if (typeof(items) == 'undefined') {
                items = 10;
            }
            items = items - 1;
            wapper.find('.list-category-dropdown >li:gt(' + items + ')').slideToggle('slow');
        });


        //Outlet mCustom Scrollbar
        if ($('.list-outlet-brand').length > 0) {
            $(".list-outlet-brand").mCustomScrollbar();
        }
        //Deal Count Down
        if ($('.super-deal-countdown').length > 0) {
            $(".super-deal-countdown").TimeCircles({
                fg_width: 0.01,
                bg_width: 1.2,
                text_size: 0.07,
                circle_bg_color: "#ffffff",
                time: {
                    Days: {
                        show: true,
                        text: "Days",
                        color: "#f9bc02"
                    },
                    Hours: {
                        show: true,
                        text: "Hour",
                        color: "#f9bc02"
                    },
                    Minutes: {
                        show: true,
                        text: "Mins",
                        color: "#f9bc02"
                    },
                    Seconds: {
                        show: true,
                        text: "Secs",
                        color: "#f9bc02"
                    }
                }
            });
        }
        if ($('.top-toggle-coutdown').length > 0) {
            $(".top-toggle-coutdown").TimeCircles({
                fg_width: 0.03,
                bg_width: 1.2,
                text_size: 0.07,
                circle_bg_color: "rgba(27,29,31,0.5)",
                time: {
                    Days: {
                        show: true,
                        text: "day",
                        color: "#fbb450"
                    },
                    Hours: {
                        show: true,
                        text: "hou",
                        color: "#fbb450"
                    },
                    Minutes: {
                        show: true,
                        text: "min",
                        color: "#fbb450"
                    },
                    Seconds: {
                        show: true,
                        text: "sec",
                        color: "#fbb450"
                    }
                }
            });
        }
        if ($('.hot-deal-tab-countdown').length > 0) {
            $(".hot-deal-tab-countdown").TimeCircles({
                fg_width: 0,
                bg_width: 1,
                text_size: 0.07,
                time: {
                    Days: {
                        show: true,
                        text: "D",
                    },
                    Hours: {
                        show: true,
                        text: "H",
                    },
                    Minutes: {
                        show: true,
                        text: "M",
                    },
                    Seconds: {
                        show: true,
                        text: "S",
                    }
                }
            });
        }
        if ($('.hotdeal-countdown').length > 0) {
            $(".hotdeal-countdown").TimeCircles({
                fg_width: 0,
                bg_width: 1,
                text_size: 0.07,
                time: {
                    Days: {
                        show: true,
                        text: "D",
                    },
                    Hours: {
                        show: true,
                        text: "H",
                    },
                    Minutes: {
                        show: true,
                        text: "M",
                    },
                    Seconds: {
                        show: true,
                        text: "S",
                    }
                }
            });
        }
        if ($('.hotdeal-countdown5').length > 0) {
            $(".hotdeal-countdown5").TimeCircles({
                fg_width: 0,
                bg_width: 1,
                text_size: 0.07,
                circle_bg_color: "#f4f4f4",
                time: {
                    Days: {
                        show: false,
                        text: "Days",
                        color: "#e62e04"
                    },
                    Hours: {
                        show: true,
                        text: "Hour",
                        color: "#e62e04"
                    },
                    Minutes: {
                        show: true,
                        text: "Mins",
                        color: "#e62e04"
                    },
                    Seconds: {
                        show: true,
                        text: "Secs",
                        color: "#e62e04"
                    }
                }
            });
        }
        if ($('.dealoff-countdown').length > 0) {
            $(".dealoff-countdown").TimeCircles({
                fg_width: 0,
                bg_width: 1,
                text_size: 0.07,
                circle_bg_color: "#fff",
                time: {
                    Days: {
                        show: false,
                        text: "d",
                    },
                    Hours: {
                        show: true,
                        text: "h",
                    },
                    Minutes: {
                        show: true,
                        text: "m",
                    },
                    Seconds: {
                        show: true,
                        text: "s",
                    }
                }
            });
        }
        if ($('.great-deal-countdown').length > 0) {
            $(".great-deal-countdown").TimeCircles({
                fg_width: 0,
                bg_width: 1,
                text_size: 0.07,
                circle_bg_color: "#fff",
                time: {
                    Days: {
                        show: true,
                        text: "d",
                    },
                    Hours: {
                        show: true,
                        text: "h",
                    },
                    Minutes: {
                        show: true,
                        text: "m",
                    },
                    Seconds: {
                        show: true,
                        text: "s",
                    }
                }
            });
        }
        if ($('.deal-countdown8').length > 0) {
            $(".deal-countdown8").TimeCircles({
                fg_width: 0.01,
                bg_width: 1.2,
                text_size: 0.07,
                circle_bg_color: "#fafafa",
                time: {
                    Days: {
                        show: true,
                        text: "D",
                        color: "#e62e04"
                    },
                    Hours: {
                        show: true,
                        text: "H",
                        color: "#e62e04"
                    },
                    Minutes: {
                        show: true,
                        text: "M",
                        color: "#e62e04"
                    },
                    Seconds: {
                        show: true,
                        text: "S",
                        color: "#e62e04"
                    }
                }
            });
        }
        if ($('.supperdeal-countdown').length > 0) {
            $(".supperdeal-countdown").TimeCircles({
                fg_width: 0.03,
                bg_width: 1.2,
                text_size: 0.07,
                circle_bg_color: "#5f6062",
                time: {
                    Days: {
                        show: true,
                        text: "day",
                        color: "#c6d43a"
                    },
                    Hours: {
                        show: true,
                        text: "hou",
                        color: "#c6d43a"
                    },
                    Minutes: {
                        show: true,
                        text: "min",
                        color: "#c6d43a"
                    },
                    Seconds: {
                        show: true,
                        text: "sec",
                        color: "#c6d43a"
                    }
                }
            });
        }
        //Tab Control
        $('.title-tab-product li a').on('click', function(event) {
            event.preventDefault();
            $('.title-tab-product li').removeClass('active');
            $(this).parent().addClass('active');
            $('.content-tab-product .tab-pane').each(function() {
                if ($(this).attr('id') == $('.title-tab-product li.active a').attr('data-id')) {
                    $(this).slideDown();
                } else {
                    $(this).slideUp();
                }
            });
        });
        //Close Service Box
        $('.close-service-box').on('click', function(event) {
            event.preventDefault();
            $('.list-service-box').slideUp('slow');
        });
        //Close Top Toggle
        $('.close-top-toggle').on('click', function(event) {
            event.preventDefault();
            $('.top-toggle').slideUp('slow');
        });
        //Detail Gallery
        if ($('.detail-gallery').length > 0) {
            $(".detail-gallery .carousel").jCarouselLite({
                btnNext: ".gallery-control .next",
                btnPrev: ".gallery-control .prev",
                speed: 800,
                visible: 4,
            });
            //Elevate Zoom
            $('.detail-gallery .mid img').elevateZoom({
                zoomType: "inner",
                cursor: "crosshair",
                zoomWindowFadeIn: 500,
                zoomWindowFadeOut: 750
            });
            $(".detail-gallery .carousel a").on('click', function(event) {
                event.preventDefault();
                $(".detail-gallery .carousel a").removeClass('active');
                $(this).addClass('active');
                $(".detail-gallery .mid img").attr("src", $(this).find('img').attr("src"));
                var z_url = $('.detail-gallery .mid img').attr('src');
                $('.zoomWindow').css('background-image', 'url("' + z_url + '")');
            });
        }
        //Sort Pagi Bar
        $('body').on('click', function() {
            $('.product-order-list').slideUp();
            $('.per-page-list').slideUp();
        });
        $('.product-order-toggle').on('click', function(event) {
            event.stopPropagation();
            event.preventDefault();
            $('.product-order-list').slideToggle();
        });
        $('.per-page-toggle').on('click', function(event) {
            event.stopPropagation();
            event.preventDefault();
            $('.per-page-list').slideToggle();
        });
        //Attr Product
        $('body').on('click', function() {
            $('.list-color').slideUp();
            $('.list-size').slideUp();
        });
        $('.toggle-color').on('click', function(event) {
            event.stopPropagation();
            event.preventDefault();
            $('.list-color').slideToggle();
        });
        $('.toggle-size').on('click', function(event) {
            event.stopPropagation();
            event.preventDefault();
            $('.list-size').slideToggle();
        });
        $('.list-color a').on('click', function(event) {
            event.preventDefault();
            $('.list-color a').removeClass('selected');
            $(this).addClass('selected');
            $('.toggle-color').text($(this).text());
        });
        $('.list-size a').on('click', function(event) {
            event.preventDefault();
            $('.list-size a').removeClass('selected');
            $(this).addClass('selected');
            $('.toggle-size').text($(this).text());
        });

        //Qty Up-Down
      //  $('.info-qty').each(function() {
            var qtyval =0;
            $('.qty-up').on('click', function(event) {
                event.preventDefault(); 
                var url = window.location.href.replace(/\/$/, '');  /* remove optional end / */ 
                var lastSeg = url.substr(url.lastIndexOf('/') + 1);
                if(lastSeg !='checkout'){  
                var step =  $(this).next().data('step');
                var max_qty =  $(this).next().data('max');
                qtyval =0;            
                qtyval =  parseInt($(this).prev().text(),10) + step ;
                if(qtyval >max_qty){
                    alert('max allowed quantity is ' + max_qty);
                }
                else{
                $(this).prev().text(qtyval);
                $(this).next().val(qtyval);
                }

                $(this).next().change();
                return false;
            }
            });
            var qtyval =0;
            $('.qty-down').on('click', function(event) {
                event.preventDefault();
                var url = window.location.href.replace(/\/$/, '');  /* remove optional end / */ 
                var lastSeg = url.substr(url.lastIndexOf('/') + 1);
                if(lastSeg !='checkout'){
                qtyval =0;
                var step =   $(this).next().next().next().data('step');
                var min_qty =   $(this).next().next().next().data('min');
                qtyval = parseInt($(this).next().text(),10) - step;
                if (qtyval >= min_qty ) {
                    $(this).next().text(qtyval);
                    $(this).next().next().next().val(qtyval);
                } else {
                    alert('min allowed quantity is ' + min_qty);
                }
                $(this).next().next().next().change();
                return false;
            }
                
            });
            $(document).on('click', '.qty-down', function (e) {
                e.preventDefault();
                var url = window.location.href.replace(/\/$/, '');  /* remove optional end / */ 
                var lastSeg = url.substr(url.lastIndexOf('/') + 1);
                if(lastSeg !='checkout'){
                var qtyval =0;
                var step =   $(this).next().next().next().data('step');
                var min_qty =   $(this).next().next().next().data('min');
                qtyval = parseInt($(this).next().text(),10) - step;
                if (qtyval >= min_qty ) {
                    $(this).next().text(qtyval);
                    $(this).next().next().next().val(qtyval);
                } else {
                    alert('min allowed quantity is ' + min_qty);
                }
                $(this).next().next().next().change();
                return false;
            }
            }) ;
            $(document).on('click', '.qty-up', function (e) {
                e.preventDefault();   
                var url = window.location.href.replace(/\/$/, '');  /* remove optional end / */ 
                var lastSeg = url.substr(url.lastIndexOf('/') + 1);
                if(lastSeg !='checkout'){
                    var step =  $(this).next().data('step');
                var max_qty =  $(this).next().data('max');
               var qtyval =0;            
                qtyval =  parseInt($(this).prev().text(),10) + step ;
                if(qtyval >max_qty){
                    alert('max allowed quantity is ' + max_qty);
                }
                else{
                $(this).prev().text(qtyval);
                $(this).next().val(qtyval);
                }

                $(this).next().change();
                return false;
            }
            }) ;
            $(document).on('change', '.product-quantity input,.product-sm-quantity input', function (e) {
                e.preventDefault();
                var url = window.location.href.replace(/\/$/, '');  /* remove optional end / */ 
                var lastSeg = url.substr(url.lastIndexOf('/') + 1);
                if(lastSeg !='checkout'){
                var id = $(this).data('id');
                var price = $(this).data('price');
                var qty = $(this).val();
                var linePrice = price*qty;
                $("#popcart_"+id+" .cart_info ").html('<span class="price_cart" data-price="'+linePrice.toFixed(2)+'">'+qty+' x '+currency+' '+price.toFixed(2)+'</span>');
                //$("#popcart_"+id+" .cart_info .price_cart").html(currency + ' ' + linePrice.toFixed(2));
                $("#popcart_"+id+" .product-sm-quantity .qty-val").html(qty);
                $("#popcart_"+id+" .product-sm-quantity input").val(qty);

                $("#cart_"+id+" .cart_info1").html('<label>Line Total:</label> <span class="product-price price_cart1" data-price="'+linePrice.toFixed(2)+'">'+currency+' '+linePrice.toFixed(2)+'</span>');
                //$("#popcart_"+id+" .cart_info .price_cart").html(currency + ' ' + linePrice.toFixed(2));
                $("#cart_"+id+" .product-quantity .qty-val").html(qty);
                $("#cart_"+id+" .product-quantity input").val(qty);
                $.ajax({
                    url: base_url + "cart/update_cart",
                    type: "POST",
                    data: {
                        product_variant_id: id,
                        qty: qty,
                        [csrfName]: csrfHash,
                    },
                    dataType: 'json',
                    success: function (result) {
                        csrfName = result.csrfName;
                        csrfHash = result.csrfHash;
                        if (result.error == false) {
                            recalculateCart();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: result.message
                            });
                        }
                    }
                });
            }
        
            });



            $('.qtydown').on('click', function(event) {           
                
                event.preventDefault();
                var qtyval =0;
                 qtyval = parseInt($(this).next().text(),10) - 1;
                 if (qtyval > 1) {
                     $(this).next().text(qtyval);
                     $(this).next().next().next().val(qtyval);
                 } else {
                     qtyval = 1;
                     $(this).next().text(qtyval);
                     $(this).next().next().next().val(qtyval);
                 }
 
            
            
         });
         $('.qtyup').on('click', function(event) { 
            event.preventDefault();      
            var qtyval =0;         
                 qtyval =  parseInt($(this).prev().text(),10) + 1;
                 $(this).prev().text(qtyval);
                 $(this).next().val(qtyval);   
 
         });
            $(document).on('click', '#qtyDown', function (e) { 
                
                   e.preventDefault();
                   var qtyval =0;
                    qtyval = parseInt($(this).next().text(),10) - 1;
                    if (qtyval > 1) {
                        $(this).next().text(qtyval);
                        $(this).next().next().next().val(qtyval);
                    } else {
                        qtyval = 1;
                        $(this).next().text(qtyval);
                        $(this).next().next().next().val(qtyval);
                    }
    
               
               
            });
            $(document).on('click', '#qtyUp', function (e) {                
                    e.preventDefault();     
                    var qtyval =0;          
                    qtyval =  parseInt($(this).prev().text(),10) + 1;
                    $(this).prev().text(qtyval);
                    $(this).next().val(qtyval);   
    
            });
      //  });
        //Rev Slider
        if ($('.rev-slider').length > 0) {
            $('.rev-slider').revolution({
                startwidth: 1170,
                startheight: 410,
            });
        }
        $('body').on('click', function() {
            $('.list-category-toggle').slideUp();
        });

        $('.category-toggle-link').on('click', function(event) {
            event.stopPropagation();
            event.preventDefault();
            $('.list-category-toggle').slideToggle();
        });
        $('.title-category-dropdown').on('click', function() {
            $(this).next().slideToggle();
        });
        //Widget Shop
        $('.widget.widget-vote a').on('click', function(event) {
            event.preventDefault();
            $(this).toggleClass('active');
        });
        $('.widget-filter .widget-title').on('click', function(event) {
            $(this).toggleClass('active');
            $(this).next().slideToggle('slow');
        });
        $('.box-filter li a,.list-color-filter a').on('click', function(event) {
            event.preventDefault();
            $(this).toggleClass('active');
        });
        if ($('.range-filter').length > 0) {
            var min_p = $("#filter_min_price").val();
            var max_p = $("#filter_max_price").val();
            $(".range-filter #slider-range").slider({
                range: true,
                min: 0,
                max: 500,
                values: [min_p, max_p],
                slide: function(event, ui) {
                    $("#amount").html("<span>" + ui.values[0] + "</span>" + " - " + "<span>" + ui.values[1] + "</span>");
                    $("#filter_min_price").val(ui.values[0] );
                    $("#filter_max_price").val(ui.values[1]);
                }
            });
            $(".range-filter #amount").html("<span>" + $("#slider-range").slider("values", 0) + "</span>" + " - " + "<span>" + $("#slider-range").slider("values", 1) + "</span>");
        }
        //End Widget Shop
        //Sticker Slider
        if ($('.bxslider-ticker').length > 0) {
            $('.bxslider-ticker').bxSlider({
                maxSlides: 2,
                minSlides: 1,
                slideWidth: 400,
                slideMargin: 10,
                ticker: true,
                tickerHover: true,
                useCSS: false,
                speed: 50000,
            });
        }
        //Hot Deal Slider Home 12
        if ($('.hot-deal-tab-slider12 .hot-deal-slider').length > 0) {
            $('.hot-deal-tab-slider12 .hot-deal-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 5,
                    itemsCustom: [
                        [0, 2],
                        [480, 2],
                        [768, 3],
                        [992, 4],
                        [1200, 5]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Hot Deal Slider
        if ($('.hot-deal-slider').length > 0) {
            $('.hot-deal-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 4,
                    itemsCustom: [
                        [0, 2],
                        [480, 2],
                        [768, 2],
                        [992, 4],
                        [1200, 4]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Product Best Seller
        if ($('.product-bestseller-slider').length > 0) {
            $('.product-bestseller-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 2],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //From Blog Slider
        if ($('.from-blog-slider').length > 0) {
            $('.from-blog-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Testimonial Slider
        if ($('.testimo-slider').length > 0) {
            $('.testimo-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Product Upsell
        if ($('.upsell-detail').length > 0) {
            $('.upsell-detail-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 3,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 2],
                        [992, 3],
                        [1200, 3]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Product Tab
        if ($('.product-tab-slider').length > 0) {
            $('.product-tab-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 3,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 2],
                        [992, 3],
                        [1200, 3]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Banner Shop Slider
        if ($('.banner-shop-slider').length > 0) {
            $('.banner-shop-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Category Filter
        if ($('.category-filter-slider').length > 0) {
            $('.category-filter-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 2,
                    itemsCustom: [
                        [0, 2],
                        [480, 2],
                        [768, 2],
                        [992, 2],
                        [1200, 2]
                    ],
                    autoPlay: true,
                    pagination: false,
                    navigation: false,
                });
            });
        }
        //Category Brand
        if ($('.category-brand-slider').length > 0) {
            $('.category-brand-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 2],
                        [480, 2],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: false,
                    navigation: true,
                    autoPlay: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Widget Adv
        if ($('.widget-adv').length > 0) {
            $('.widget-adv').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: true,
                    navigation: false,
                });
            });
        }
        //Paginav Featured Slider
        if ($('.paginav-featured-slider').length > 0) {
            $('.paginav-featured-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: true,
                    navigation: false,
                });
            });
        }
        //Testimo Home 3
        if ($('.tab-testimo-slider').length > 0) {
            $('.tab-testimo-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: true,
                    navigation: false,
                });
            });
        }
        //Outlet Slider
        if ($('.outlet-slider').length > 0) {
            $('.outlet-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: true,
                    navigation: false,
                });
            });
        }
        //Gift Slider 
        if ($('.gift-icon-slider').length > 0) {
            $('.gift-icon-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: true,
                    navigation: false,
                    addClassActive: true,
                });
            });
        }
        //Category Best Sellser
        if ($('.cat-bestsale-slider').length > 0) {
            $('.cat-bestsale-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Best Seller Right
        if ($('.best-seller-right').length > 0) {
            $('.best-seller-right').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Featured Product Category
        if ($('.featured-product-category').length > 0) {
            $('.featured-product-category').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 3,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 1],
                        [992, 2],
                        [1200, 3]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Slider Cat Parent
        if ($('.slider-cat-parent').length > 0) {
            $('.slider-cat-parent').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 3,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 2],
                        [992, 2],
                        [1200, 3]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Brand Cat
        if ($('.brand-cat-slider').length > 0) {
            $('.brand-cat-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 3,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 1],
                        [992, 2],
                        [1200, 3]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //The New Slider
        if ($('.thenew-slider').length > 0) {
            $('.thenew-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: false,
                    navigation: true,
                    autoPlay: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Manufacture Slider
        if ($('.manufacture-slider').length > 0) {
            $('.manufacture-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 6,
                    itemsCustom: [
                        [0, 2],
                        [480, 3],
                        [768, 4],
                        [992, 5],
                        [1200, 6]
                    ],
                    pagination: false,
                    navigation: false,
                    autoPlay: true
                });
            });
        }
        //The New Slider
        if ($('.best-seller3').length > 0) {
            $('.best-seller3').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 4,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 2],
                        [992, 3],
                        [1200, 4]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Popualar Category Slider
        if ($('.popular-cat-slider.popular-cat-slider11').length > 0) {
            $('.popular-cat-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 5,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 3],
                        [992, 4],
                        [1200, 5]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Popualar Category Slider
        if ($('.popular-cat-slider.popular-cat-slider12').length > 0) {
            $('.popular-cat-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 5,
                    itemsCustom: [
                        [0, 2],
                        [480, 2],
                        [768, 3],
                        [992, 4],
                        [1200, 5]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Popualar Category Slider
        if ($('.popular-cat-slider.slider-home5').length > 0) {
            $('.popular-cat-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 4,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 2],
                        [992, 3],
                        [1200, 4]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        if ($('.popular-cat-slider.slider-home6').length > 0) {
            $('.popular-cat-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 4,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 2],
                        [992, 3],
                        [1200, 4]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Popualar Category Slider
        if ($('.popular-cat-slider').length > 0) {
            $('.popular-cat-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 4,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 2],
                        [992, 3],
                        [1200, 4]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Deal Of The day
        if ($('.dealoff-theday-slider').length > 0) {
            $('.dealoff-theday-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 5,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 3],
                        [992, 4],
                        [1200, 5]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Simple Owl Slider
        if ($('.simple-owl-slider').length > 0) {
            $('.simple-owl-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    autoPlay: true,
                    delay: 5000,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Owl Direct Nav
        if ($('.owl-directnav').length > 0) {
            $('.owl-directnav').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Best Seller Slider
        if ($('.best-seller-slider').length > 0) {
            $('.best-seller-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 3,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 2],
                        [992, 3],
                        [1200, 3]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<span class="lnr lnr-chevron-left"></span>', '<span class="lnr lnr-chevron-right"></span>']
                });
            });
        }
        //Popular Category
        if ($('.pop-cat-slider').length > 0) {
            $('.pop-cat-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 9,
                    itemsCustom: [
                        [0, 3],
                        [480, 4],
                        [768, 7],
                        [992, 8],
                        [1200, 9]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<span class="lnr lnr-chevron-left"></span>', '<span class="lnr lnr-chevron-right"></span>']
                });
            });
        }
        //Single Relared Post
        if ($('.single-related-post-slider').length > 0) {
            $('.single-related-post-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 2],
                        [992, 3],
                        [1200, 3]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<span class="lnr lnr-chevron-left"></span>', '<span class="lnr lnr-chevron-right"></span>']
                });
            });
        }
        //Great Deal category
        if ($('.great-deal-cat-slider').length > 0) {
            $('.great-deal-cat-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 5,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 3],
                        [992, 4],
                        [1200, 5]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Category Brand Slider
        if ($('.cat-brand-slider').length > 0) {
            $('.cat-brand-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 2,
                    itemsCustom: [
                        [0, 2],
                        [480, 2],
                        [768, 2],
                        [992, 2],
                        [1200, 2]
                    ],
                    pagination: false,
                    navigation: true,
                    autoPlay: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Single Relared Post
        if ($('.fromblog-slider').length > 0) {
            $('.fromblog-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 2,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 2],
                        [992, 2],
                        [1200, 2]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<span class="lnr lnr-chevron-left"></span>', '<span class="lnr lnr-chevron-right"></span>']
                });
            });
        }
        //Mega Hot Deal
        if ($('.mega-hot-deal-slider').length > 0) {
            $('.mega-hot-deal-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Mega New Arrival
        if ($('.mega-new-arrival-slider').length > 0) {
            $('.mega-new-arrival-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 4,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 2],
                        [992, 2],
                        [1200, 4]
                    ],
                    pagination: false,
                    navigation: true,
                    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                });
            });
        }
        //Testimonial Slider
        if ($('.testimonial-slider').length > 0) {
            $('.testimonial-slider').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 1],
                        [768, 1],
                        [992, 1],
                        [1200, 1]
                    ],
                    pagination: true,
                    navigation: false,
                });
            });
        }
        //Customer Slider
        if ($('.customer-saying').length > 0) {
            $('.customer-saying').each(function() {
                $(this).find('.wrap-item').owlCarousel({
                    items: 1,
                    itemsCustom: [
                        [0, 1],
                        [480, 2],
                        [768, 2],
                        [992, 3],
                        [1200, 3]
                    ],
                    pagination: false,
                    navigation: false,
                    autoPlay: true
                });
            });
        }
        //Blog Masonry 
        if ($('.masonry-list-post').length > 0) {
            $('.masonry-list-post').masonry({
                // options
                itemSelector: '.item-post-masonry',
            });
        }
    });



    //Window Load
    jQuery(window).load(function() {

    });




})(jQuery); // End of use strict



$(document).ready(function() {

    $('.sidebar-menu-icon').on('click', function() {
        $('.sidebar-menu-wrap').toggleClass('active');
        $('.overlays').toggleClass('active');
    });

    $('.sidebar-menu-close').on("click", function() {
        $('.sidebar-menu-wrap').removeClass('active');
        $('.overlays').removeClass('active');

    });


});

$(document).ready(function() {


    $('ul.nav-pills li a').click(function(e) {
        $('ul.nav-pills li.active').removeClass('active')
        $(this).parent('li').addClass('active')
    })

});





/*=====================
 20. Sidebar js
 ==========================*/
function openCart() {
    document.getElementById("cart_side").classList.add('open-side');
}

function closeCart() {
    document.getElementById("cart_side").classList.remove('open-side');
}


function openWish() {
    document.getElementById("wish_side").classList.add('open-side');
}

function closeWish() {
    document.getElementById("wish_side").classList.remove('open-side');
}


$('.delivery-add li').on('click', function() {
    $(this).addClass('active').siblings().removeClass('active');
});

function handleLiveSearch() {
    $('body').on('click', function(e) {
        if (
            $(e.target).closest('.ps-form--search-header') ||
            e.target.className === '.ps-form--search-header'
        ) {
            $('.ps-panel--search-result').removeClass('active');
        }
    });
    $('#input-search').on('keyup', function() {
        var search_key = $('#input-search').val();
        var cat_id = $('#search_cat').val();
        $('.ps-panel--search-result .ps-panel__footer').html('');
        $('.ps-panel--search-result .ps-panel__content').html('');
        $.ajax({
            url: base_url + 'home/get_ajax_products',
            type: 'POST',
            data: {search_key:search_key, search_cat: cat_id,[csrfName]: csrfHash },            
            dataType: 'json',
            success: function (data) {
                csrfName = data['csrfName'];
                csrfHash = data['csrfHash'];              
                var result ='';
                if(data['error']== false){
                    $.each(data['data'], function (i, item) {
                        result ='<div class="ps-product ps-product--wide ps-product--search-result"><div class="ps-product__thumbnail"><a href="'+item['link']+'"><img src="'+item['image_sm']+'" alt=""></a></div><div class="ps-product__content"><a class="ps-product__title" href="'+item['link']+'">'+item['name']+'</a><br>';
                        if(item['min_max_price']['max_price'] > item['min_max_price']['min_price'])
                            result = result +'<p class="ps-product__price">'+currency+' '+item['min_max_price']['min_price']+' - '+currency+' '+item['min_max_price']['max_price']+'</p>';
                        else
                            result = result +'<p class="ps-product__price">'+currency+' '+item['min_max_price']['min_price']+'</p>';
                        result = result+'</div></div>';
                        $('.ps-panel--search-result .ps-panel__content').append(result);
                    });
                    $('.ps-panel--search-result .ps-panel__footer').append('<a href="'+base_url +'products/search?q='+search_key+'">See all results</a>');
                }
                else{
                    $('.ps-panel--search-result .ps-panel__content').append('No product found!!');
                }
      
            }
          });
        $('.ps-panel--search-result').addClass('active');
    })
}


$(function() {
    handleLiveSearch();
});

$(document).ready(function() {
    $('.sidebar-toggle').click(function() {
        $('.content-shop').toggleClass("sidebar-opened");
    });
});
$('.manual-ajax').click(function(event) {
    $("#myModal").remove();
    event.preventDefault();
    this.blur(); // Manually remove focus from clicked link.
    $.get(this.href, function(html) {       
         //
         //$(html).appendTo('body').modal();
         $("#product_modal").html(html);
         $("#myModal").modal();
    });
});



$(function() {
    var k = $(".more-categories"),
        S = $(".category-dropdown"),
        x = $(".category-nav-inner"),
        T = $(".category-dropdown-wrap"),
        E = $(".vertical-megamenu > li");
    x.on("click", (function() { T.toggleClass("show") })), t.on("load resize", (function() {
        var t = 0,
            e = F.height();
        S.css("height", "".concat(e, "px")), E.each((function() {
            var n = $(this);
            if ((t += n.height()) + 78 > e) return n.addClass("hide"), void k.removeClass("hide");
            n.removeClass("hide"), k.addClass("hide")
        }))
    }));
});

$('.add-to-fav-btn').on('click', function (e) {
    e.preventDefault();	
    var formdata = new FormData();
    var product_id = $(this).data('product-id');
    var fav_btn = $(this);
    formdata.append(csrfName, csrfHash);
    formdata.append('product_id', product_id);
    $.ajax({
        type: 'POST',
        url: base_url + 'my-account/manage-favorites',
        data: formdata,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (result) {
            csrfName = result.csrfName;
            csrfHash = result.csrfHash;
            if (result.error == true) {
                Toast.fire({
                    icon: 'error',
                    title: result.message
                });
            } else {
                
					Toast.fire({
                    icon: 'success',
                    title: result.message
                });
                fav_btn.removeClass('text-danger');
                if(result.message == 'Product Added to favorite')
                    fav_btn.addClass('text-danger');
                
				$('#wishlist-count').html(result.data);
                var html = '<ul class="cart_product">';
                    $.each(result.favorites, function (i, e) {
                        html += ' <li id="fav-favorites"><div class="media"><a href="'+base_url+'products/details/' + e.slug +'">';
                        if(e.image_sm!='')
                            html += '<img class="mr-3" src="' +e.image_sm + '" alt=' + e.name + '>';
                        else
                            html += '<img class="mr-3" src="' + base_url + '/assets/no-image.png' + '" alt=' + e.name + '>';
                        html += '</a>'
         
                            + '<div class="media-body"><a href="#"><h4>' + e.name + '</h4></a>';
                            if(e.variants.length <=1){
                            html += '<a href="javascript:void(0);" class="btn btn-dark btn-sm add-to-cart" data-product-qty="1" data-product-id="'+e.id+'" data-product-variant-id="'+e.variants[0]['id'] +'" >'+
                            'Add to Cart'+
                        '</a>';
                            }
                            else{
                                html += '<a href="'+base_url+'products/details/' + e.slug +'" class="btn btn-dark btn-sm" >'+
                                'Add to Cart'+
                            '</a>';
                            }
                            html += '</div> <div class="close-circle">'+
                            '<a href="javascript:void(0);" class="btn btn-view delete-from-fav fa text-danger" data-product-id="'+e.id+'"><i class="fas fa-trash"></i></a>'+
                           '</div></li>';


                           
                    });
					html +='</ul><ul class="cart_total">'+
                    ' <li><div class="buttons"><a href="' + base_url +'my-account/favorites" class="btn btn-solid btn-solid-sm btn-block checkout">Wishlist</a>'+
                    '</div>'+
                '</li>'
                    html +='</ul>';
                    $('#wish_side .cart_media').html(html);
                
            }
        } 
    });
});
$(document).on('click', '.add-to-fav-btn', function (e) {
    e.preventDefault();
	
    var formdata = new FormData();
    var product_id = $(this).data('product-id');
    var fav_btn = $(this);
    formdata.append(csrfName, csrfHash);
    formdata.append('product_id', product_id);
    $.ajax({
        type: 'POST',
        url: base_url + 'my-account/manage-favorites',
        data: formdata,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (result) {
            csrfName = result.csrfName;
            csrfHash = result.csrfHash;
            if (result.error == true) {
                Toast.fire({
                    icon: 'error',
                    title: result.message
                });
            } else {
                
					Toast.fire({
                    icon: 'success',
                    title: result.message
                });
                fav_btn.removeClass('text-danger');
                if(result.message == 'Product Added to favorite')
                    fav_btn.addClass('text-danger');
                
				$('#wishlist-count').html(result.data);
                var html = '<ul class="cart_product">';
                    $.each(result.favorites, function (i, e) {
                        html += ' <li id="fav-favorites"><div class="media"><a href="'+base_url+'products/details/' + e.slug +'">';
                        if(e.image_sm!='')
                            html += '<img class="mr-3" src="' +e.image_sm + '" alt=' + e.name + '>';
                        else
                            html += '<img class="mr-3" src="' + base_url + '/assets/no-image.png' + '" alt=' + e.name + '>';
                        html += '</a>'
         
                            + '<div class="media-body"><a href="#"><h4>' + e.name + '</h4></a>';
                            if(e.variants.length <=1){
                            html += '<a href="javascript:void(0);" class="btn btn-dark btn-sm add-to-cart" data-product-qty="1" data-product-id="'+e.id+'" data-product-variant-id="'+e.variants[0]['id'] +'" >'+
                            'Add to Cart'+
                        '</a>';
                            }
                            else{
                                html += '<a href="'+base_url+'products/details/' + e.slug +'" class="btn btn-dark btn-sm" >'+
                                'Add to Cart'+
                            '</a>';
                            }
                            html += '</div> <div class="close-circle">'+
                            '<a href="javascript:void(0);" class="btn btn-view delete-from-fav fa text-danger" data-product-id="'+e.id+'"><i class="fas fa-trash"></i></a>'+
                           '</div></li>';


                           
                    });
					html +='</ul><ul class="cart_total">'+
                    ' <li><div class="buttons"><a href="' + base_url +'my-account/favorites" class="btn btn-solid btn-solid-sm btn-block checkout">Wishlist</a>'+
                    '</div>'+
                '</li>'
                    html +='</ul>';
                    $('#wish_side .cart_media').html(html);
                
            }
        } 
    });
});
$('.delete-from-fav').on('click', function (e) {
    e.preventDefault();
	
    var formdata = new FormData();
    var product_id = $(this).data('product-id');
    var fav_btn = $(this);
    formdata.append(csrfName, csrfHash);
    formdata.append('product_id', product_id);
    $.ajax({
        type: 'POST',
        url: base_url + 'my-account/delete-favorites',
        data: formdata,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (result) {
            csrfName = result.csrfName;
            csrfHash = result.csrfHash;
            if (result.error == true) {
                Toast.fire({
                    icon: 'error',
                    title: result.message
                });
            } else {
                //if (fav_btn.hasClass('far')) {
                  //  fav_btn.removeClass('far').addClass('fa text-danger');
                //} else {
					Toast.fire({
                    icon: 'success',
                    title: result.message
                });
                $('#fav-'+product_id).remove();
				$('#wishlist-count').html(result.data);
                    fav_btn.removeClass('fa text-danger').addClass('far');
                    fav_btn.css('color', '#adadad');
                //}
            }
        } 
    });
});
$(document).on('click', '.delete-from-fav', function (e) {
    e.preventDefault();
	
    var formdata = new FormData();
    var product_id = $(this).data('product-id');
    var fav_btn = $(this);
    formdata.append(csrfName, csrfHash);
    formdata.append('product_id', product_id);
    $.ajax({
        type: 'POST',
        url: base_url + 'my-account/delete-favorites',
        data: formdata,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (result) {
            csrfName = result.csrfName;
            csrfHash = result.csrfHash;
            if (result.error == true) {
                Toast.fire({
                    icon: 'error',
                    title: result.message
                });
            } else {
                //if (fav_btn.hasClass('far')) {
                  //  fav_btn.removeClass('far').addClass('fa text-danger');
                //} else {
					Toast.fire({
                    icon: 'success',
                    title: result.message
                });
                $('#fav-'+product_id).remove();
				$('#wishlist-count').html(result.data);
                    fav_btn.removeClass('fa text-danger').addClass('far');
                    fav_btn.css('color', '#adadad');
                //}
            }
        } 
    });
});

$('.add-to-cart').on('click', function (e) {
    e.preventDefault();
   // $('#quick-view').data('data-product-id', $(this).data('productId'));
    var product_variant_id = $(this).attr('data-product-variant-id');
    var qty = $(this).attr('data-product-qty');
    var btn = $(this);
    var btn_html = $(this).html();
    var iprice =0;
    if(qty <=0){
        Toast.fire({
            icon: 'error',
            title: 'Quantity must be valid number'
        });
    }else{
    $.ajax({
        type: 'POST',
        url: base_url + 'cart/manage',
        data: {
            'product_variant_id': product_variant_id,
            'qty': qty,
            'is_saved_for_later': false,
            [csrfName]: csrfHash,
        },
        dataType: 'json',
        beforeSend: function () {
            btn.html('Please Wait').text('Please Wait').attr('disabled', true);
        },
        success: function (result) {
            csrfName = result.csrfName;
            csrfHash = result.csrfHash;
            btn.html(btn_html).attr('disabled', false);
            if (result.error == false) {
                Toast.fire({
                    icon: 'success',
                    title: result.message
                });
                $('#cart-count').text(result.data.cart_count);
                var total=0;
                    var html = '<ul class="cart_product">';
                    $.each(result.data.items, function (i, e) {
                        if(e.special_price > 0 && e.special_price < e.price){
                            iprice = e.special_price;
                        }
                        else{
                            iprice = e.price;
                        }
                        html += '<li class="cart_item" id="popcart_'+ e.product_variant_id + '"><div class="media"><a href="#">';
                        if(e.image!='')
                            html += '<img class="mr-3" src="' + base_url + e.image + '" alt=' + e.name + '>';
                        else
                            html += '<img class="mr-3" src="' + base_url + '/assets/no-image.png' + '" alt=' + e.name + '>';
                        html += '</a>'
         
                            + '<div class="media-body"><a href="#"><h4>' + e.name + '</h4></a>'
                   		    + ' <h4  class="cart_info"> <span class="price_cart" data-price="'+e.qty * iprice+'" >' + e.qty + ' X '+currency + ' ' +iprice+'</span></h4>'
							+ '<div class="attr-product pull-right">'+

                            '<div class="info-qty product-sm-quantity">'+
                               '<a class="qty-down" href="#"><i class="fa fa-minus"></i></a>'+
                                '<span class="qty-val">' + e.qty + '</span>'+
                                '<a class="qty-up" href="#"><i class="fa fa-plus"></i></a>'+
                                ' <input type="hidden" name="qty" value="'+ e.qty +'" data-page="cart" data-id="'+ e.product_variant_id +'" data-price="'+iprice+'" data-step="1" data-min="1" data-max="100">'+
                            '</div>'+
                       ' </div></div><div class="close-circle cart_remove">'+
                       '<a  href="javascript:void(0)" class="remove-product" data-id="' + e.product_variant_id + '">'+
                          ' <i class="fas fa-trash" aria-hidden="true" ></i>'+
                       '</a> </div></li>';

            total=total+(e.qty * iprice);
                           
                    });
					html +='</ul><ul class="cart_total"><li><div class="total cart_total"> <h5>subtotal : <span>' + currency + ' <span class="cart-subtotal">' + total.toFixed(3) +'</span></span></h5></div></li>'+
                    ' <li><div class="buttons"><a href="' + base_url +'cart" class="btn btn-solid btn-block btn-solid-sm view-cart">view cart</a>'+
                        '<a href="' + base_url +'cart/checkout" class="btn btn-solid btn-solid-sm btn-block checkout">checkout</a>'+
                    '</div>'+
                '</li>'
                    html +='</ul>';
                    $('#cart_side .cart_media').html(html);
            } else {
                Toast.fire({
                    icon: 'error',
                    title: result.message
                });
            }
        }
    })
}
});
$(document).on('click', '.add-to-cart', function (e) {
    e.preventDefault();
    var qty = $('#mqty').val();    
    // $('#quick-view').data('data-product-id', $(this).data('productId'));
    var product_variant_id = $(this).attr('data-product-variant-id');
    var btn = $(this);
    var btn_html = $(this).html();
    var izi_modal = $(this).attr('data-izimodal-open');
    if(qty <=0){
        Toast.fire({
            icon: 'error',
            title: 'Quantity must be valid number'
        });
    }
   else{

    if (izi_modal == "" || izi_modal == undefined) {

        $.ajax({
            type: 'POST',
            url: base_url + 'cart/manage',
            data: {
                'product_variant_id': product_variant_id,
                'qty': qty,
                'is_saved_for_later': false,
                [csrfName]: csrfHash,
            },
            dataType: 'json',
            beforeSend: function () {
                btn.html('Please Wait').text('Please Wait').attr('disabled', true);
            },
            success: function (result) {
                csrfName = result.csrfName;
                csrfHash = result.csrfHash;
                btn.html(btn_html).attr('disabled', false);
                if (result.error == false) {
                    Toast.fire({
                        icon: 'success',
                        title: result.message
                    });
                    $('#cart-count').text(result.data.cart_count);
                   var total=0;
                    var html = '<ul class="cart_product">';
                    var iprice =0;
                    $.each(result.data.items, function (i, e) {
                       // alert( e.special_price);
                        if(e.special_price > 0 && e.special_price < e.price){
                            iprice = e.special_price;
                        }
                        else{
                            iprice = e.price;
                        }
                        html += '<li class="cart_item" id="popcart_'+ e.product_variant_id + '"><div class="media"><a href="#">';
                        if(e.image!='')
                            html += '<img class="mr-3" src="' + base_url + e.image + '" alt=' + e.name + '>';
                        else
                            html += '<img class="mr-3" src="' + base_url + '/assets/no-image.png' + '" alt=' + e.name + '>';
                        html += '</a>'
         
                            + '<div class="media-body"><a href="#"><h4>' + e.name + '</h4></a>'
                   		    + ' <h4  class="cart_info"> <span class="price_cart" data-price="'+e.qty * iprice+'" >' + e.qty + ' X '+currency + ' ' +iprice+'</span></h4>'
							+ '<div class="attr-product pull-right">'+

                            '<div class="info-qty product-sm-quantity">'+
                               '<a class="qty-down" href="#"><i class="fa fa-minus"></i></a>'+
                                '<span class="qty-val">' + e.qty + '</span>'+
                                '<a class="qty-up" href="#"><i class="fa fa-plus"></i></a>'+
                                ' <input type="hidden" name="qty" value="' + e.qty + '" data-page="cart" data-id="'+e.product_variant_id+'" data-price="'+iprice+'" data-step="1" data-min="1" data-max="100">'+
                            '</div>'+
                       ' </div></div> <div class="close-circle cart_remove">'+
                       '<a  href="javascript:void(0)" class="remove-product" data-id="' + e.product_variant_id + '">'+
                       
                          ' <i class="fas fa-trash" aria-hidden="true" ></i>'+
                       '</a> </div></li>';

            total=total+(e.qty * iprice);
                           
                    });
					html +='</ul><ul class="cart_total"><li><div class="total"> <h5>subtotal : <span >' + currency + '<span class="cart-subtotal">' + total.toFixed(3) +'</span> </span></h5></div></li>'+
                    ' <li><div class="buttons"><a href="' + base_url +'cart" class="btn btn-solid btn-block btn-solid-sm view-cart">view cart</a>'+
                        '<a href="' + base_url +'cart/checkout" class="btn btn-solid btn-solid-sm btn-block checkout">checkout</a>'+
                    '</div>'+
                '</li>'
                    html +='</ul>';
                    $('#cart_side .cart_media').html(html);
                } else {
                    Toast.fire({
                        icon: 'error',
                        title: result.message
                    });
                }
            }
        })
    }
}
});
$('.add_tocart').on('click', function (e) {
    e.preventDefault();
    var qty = $('#prd-qty').val();    
    $('#quick-view').data('data-product-id', $(this).data('productId'));
    var product_variant_id = $(this).attr('data-product-variant-id');
    var btn = $(this);
    var btn_html = $(this).html();
    var izi_modal = $(this).attr('data-izimodal-open');
    if(qty <=0){
        Toast.fire({
            icon: 'error',
            title: 'Quantity must be valid number'
        });
    }
   else{

    if (izi_modal == "" || izi_modal == undefined) {

        $.ajax({
            type: 'POST',
            url: base_url + 'cart/manage',
            data: {
                'product_variant_id': product_variant_id,
                'qty': qty,
                'is_saved_for_later': false,
                [csrfName]: csrfHash,
            },
            dataType: 'json',
            beforeSend: function () {
                btn.html('Please Wait').text('Please Wait').attr('disabled', true);
            },
            success: function (result) {
                csrfName = result.csrfName;
                csrfHash = result.csrfHash;
                btn.html(btn_html).attr('disabled', false);
                if (result.error == false) {
                    Toast.fire({
                        icon: 'success',
                        title: result.message
                    });
                    $('#cart-count').text(result.data.cart_count);
                   var total=0;
                    var html = '<ul class="cart_product">';
                    var iprice =0;
                    $.each(result.data.items, function (i, e) {
                       // alert( e.special_price);
                        if(e.special_price > 0 && e.special_price < e.price){
                            iprice = e.special_price;
                        }
                        else{
                            iprice = e.price;
                        }
                        html += '<li class="cart_item" id="popcart_'+e.product_variant_id+'"><div class="media"><a href="#">';
                        if(e.image!='')
                            html += '<img class="mr-3" src="' + base_url + e.image + '" alt=' + e.name + '>';
                        else
                            html += '<img class="mr-3" src="' + base_url + '/assets/no-image.png' + '" alt=' + e.name + '>';
                        html += '</a>'
         
                            + '<div class="media-body"><a href="#"><h4>' + e.name + '</h4></a>'
                   		    + ' <h4  class="cart_info"> <span class="price_cart" data-price="'+e.qty * iprice+'" >' + e.qty + ' X '+currency + ' ' +iprice+'</span></h4>'
							+ '<div class="attr-product pull-right">'+

                            '<div class="info-qty product-sm-quantity">'+
                               '<a class="qty-down" href="#"><i class="fa fa-minus"></i></a>'+
                                '<span class="qty-val">' + e.qty + '</span>'+
                                '<a class="qty-up" href="#"><i class="fa fa-plus"></i></a>'+
                                ' <input type="hidden" name="qty" value="'+e.qty+'" data-page="cart" data-id="'+e.product_variant_id+'" data-price="'+iprice+'" data-step="1" data-min="1" data-max="100">'+
                            '</div>'+
                       ' </div></div> <div class="close-circle cart_remove">'+
                       '<a  href="javascript:void(0)" class="remove-product" data-id="'+ e.product_variant_id +'">'+
                          ' <i class="fas fa-trash" aria-hidden="true" ></i>'+
                       '</a> </div></li>';

            total=total+(e.qty * iprice);
                           
                    });
					html +='</ul><ul class="cart_total"><li><div class="total"> <h5>subtotal : <span >' + currency + '<span class="cart-subtotal">' + total.toFixed(3) +'</span> </span></h5></div></li>'+
                    ' <li><div class="buttons"><a href="' + base_url +'cart" class="btn btn-solid btn-block btn-solid-sm view-cart">view cart</a>'+
                        '<a href="' + base_url +'cart/checkout" class="btn btn-solid btn-solid-sm btn-block checkout">checkout</a>'+
                    '</div>'+
                '</li>'
                    html +='</ul>';
                    $('#cart_side .cart_media').html(html);
                } else {
                    Toast.fire({
                        icon: 'error',
                        title: result.message
                    });
                }
            }
        })
    }
}
});
// $('.cart_remove .remove-product,.cart_total .remove-product').on('click',  function (e) {
//     e.preventDefault();
//     var id = $(this).data('id');
//     var product = $(this).parent().parent();
    
//     if (confirm("Are you sure want to remove this?")) {
//         $.ajax({
//             url: base_url + 'cart/remove',
//             type: "POST",
//             data: {
//                 product_variant_id: id,
//                 [csrfName]: csrfHash
//             },
//             dataType: 'json',
//             success: function (result) {
//                 csrfName = result['csrfName'];
//                 csrfHash = result['csrfHash'];
//                 if (result.error == false) {
//                     var cart_count = $('#cart-count').text();
//                     cart_count--;
//                     $('#cart-count').text(cart_count);
//                     $('#popcart_'+id).remove();
//                     $('.cart_'+id).remove();
//                     recalculateCart();
//                 } else {
//                     Toast.fire({
//                         icon: 'error',
//                         title: result.message
//                     });
//                 }
//             }
//         });
//     }
// });
$(document).on('click', '.cart_remove .remove-product,.cart_total .remove-product', function (e) {
    var url = window.location.href.replace(/\/$/, '');  /* remove optional end / */ 
    var lastSeg = url.substr(url.lastIndexOf('/') + 1);
    if(lastSeg != 'checkout'){
    e.preventDefault();
    var id = $(this).data('id');
    var product = $(this).parent().parent();
    
    if (confirm("Are you sure want to remove this?")) {
        $.ajax({
            url: base_url + 'cart/remove',
            type: "POST",
            data: {
                product_variant_id: id,
                [csrfName]: csrfHash
            },
            dataType: 'json',
            success: function (result) {
                csrfName = result['csrfName'];
                csrfHash = result['csrfHash'];
                if (result.error == false) {
                    var cart_count = $('#cart-count').text();
                    cart_count--;
                    $('#cart-count').text(cart_count);
                    $('#popcart_'+id).remove();
                    $('.cart_'+id).remove();
                    recalculateCart();
                } else {
                    Toast.fire({
                        icon: 'error',
                        title: result.message
                    });
                }
            }
        });
    }
}
else{
    alert('Item can\'t delete in checkout page');
}
});
var taxRate = 0.05;
var shippingRate = 10.00;
var fadeTime = 300;

    function recalculateCart() {
        var subtotal = 0;
        var subtotal1 = 0;
        /* Sum up row totals */
        $('.cart_item .cart_info').each(function () {
            
            subtotal += parseFloat($(this).children('.price_cart').attr('data-price'));
        });
        $('.cart_item1 .cart_info1').each(function () {
            
            subtotal1 += parseFloat($(this).children('.price_cart1').attr('data-price'));
        });

        /* Calculate totals */
        var tax = subtotal * taxRate;
        var shipping = (subtotal > 0 ? shippingRate : 0);
        var total = subtotal + shipping ;

        var tax1 = subtotal1 * taxRate;
        var shipping1 = (subtotal1 > 0 ? shippingRate : 0);
        var total1 = subtotal1 + shipping1 ;
        /* Update totals display */
       // $('.totals-value').fadeOut(fadeTime, function () {
            $('.cart-subtotal').html(subtotal.toFixed(3));
            $('#cart-tax').html(tax.toFixed(2));
            $('#cart-shipping').html(shipping.toFixed(2));
            $('#cart-total').html(total.toFixed(3));

            $('.cart-subtotal1').html(subtotal1.toFixed(3));
            $('#cart-tax1').html(tax1.toFixed(2));
            $('#cart-shipping1').html(shipping1.toFixed(2));
            $('#cart-total1').html(total1.toFixed(3));

            if (total == 0) {
               // $('.cart_total').fadeOut(fadeTime);
                $('.cart_total').html('<li><h4>Your cart is empty</h4></li>');
                $('.shopping-cart').html('<h4>Your cart is empty</h4>');
                $("#save-order").html('Cart Empty').text('Cart Empty').attr('disabled', true);
                var url = window.location.href.replace(/\/$/, '');  /* remove optional end / */ 
                var lastSeg = url.substr(url.lastIndexOf('/') + 1);
                if(lastSeg == 'checkout'){
                    location.href=base_url+"cart";
                }
            } else {
                $('.cart_total').fadeIn(fadeTime);
            }
            $('.totals-value').fadeIn(fadeTime);
        //});
    }

    /* Update quantity */
    function updateQuantity(quantity, price) {
        /* Calculate line price */
        // if (quantityInput.data('page') == "cart") {
        //     var productRow = $(quantityInput).parent().parent().parent().siblings('.total-price');
        // } else {
        //     var productRow = $(quantityInput).parent().parent();
        // }
     
        var linePrice = price * quantity;
        $('.cart_info1 .price_cart1').text(currency + ' ' + linePrice.toFixed(2));
        $().text();
        recalculateCart();
        /* Update line price display and recalc cart totals */
        // productRow.children('.product-line-price').each(function () {
        //     $(this).fadeOut(fadeTime, function () {
        //         $(this).text(currency + ' ' + linePrice.toFixed(2));
        //         recalculateCart();
        //         $(this).fadeIn(fadeTime);
        //     });
        // });
    }

    /* Remove item from cart */
    function removeItem(removeProduct) {
        /* Remove row from DOM and recalc cart total */
       // var productRow = $(removeProduct);
       // productRow.slideUp(fadeTime, function () {
            //productRow.remove();
            recalculateCart();
       // });
    }
  
    $(document).on('change', '.modal-product-attributes', function (e) {
    
      e.preventDefault();
        var selected_attributes = [];
        var attributes_length = "";
        var price = "";var sku='';
        var steps="";
        var imgsrc="";
        var is_variant_available = false;
        var variant = [];
        var prices = [];
         var stocks = [];
        var variant_prices = [];
         var variant_stock = [];
         var variant_skus = [];
        var variants = [];
        var variant_ids = [];
        var image_indexes = [];
        var selected_image_index;
        //selected_attributes.push($(this).val());
        steps=$(this).attr('data-step');
       
         $('.single-product-active').trigger('to.owl.carousel', steps);
         imgsrc=$('#'+steps).attr('src');
        $('#zoom1').attr('src',imgsrc);
        $('.modal-product-variants').each(function () {
            var pricing=0;
            var special=0;
            sku=$(this).data('sku');
            pricing=$(this).data('price');
            special=$(this).data('special_price');
            pricing=pricing.toFixed(3);
            special=special.toFixed(3);
            prices = {
                price: pricing,
                special_price: special
            };
            variant_skus.push($(this).data('sku'));
            variant_ids.push($(this).data('id'));
            variant_stock.push($(this).data('stock'));
            variant_prices.push(prices);
            variant = $(this).val().split(',');
            variants.push(variant);
            image_indexes.push($(this).data('image-index'));
        });
        attributes_length = variant.length;
        $('.modal-product-attributes').each(function (i, e) {
            if ($(this).is('select')) {
            selected_attributes.push($(this).val());
            }
            if ($(this).prop('checked')) {
            //if ($(this).is(':checked') || $(this).val() != null ) {
               
                selected_attributes.push($(this).val());
            }
            if(selected_attributes.length>=1)
            {
                if (selected_attributes.length == attributes_length) {
                    
                    /* compare the aselected_attributes.lengthrrays */
                    prices = [];
                    stocks=[];
                    var selected_variant_id = '';
                    var skus =[];
                    $.each(variants, function (i, e) {
                        if (arrays_equal(selected_attributes, e)) {
                            
                            
                            is_variant_available = true;
                            prices.push(variant_prices[i]);
                            stocks.push(variant_stock[i]);
                            skus.push(variant_skus[i]);
                            //alert(variant_stock[i]);
                            if(stocks[0]==0)
                            {
                                
                                $('#stockmsg').show();
                            }
                            else
                            {
                                $('#stockmsg').hide();
                            }
                            
                            
                            selected_variant_id = variant_ids[i];
                            selected_image_index = image_indexes[i];
                           imgsrc=$('#'+selected_image_index).attr('srcset');
                           
        $('#Zoom-1').attr('href',imgsrc);
        $('#Zoom-1').attr('data-zoom-image-2x',imgsrc);
        $('#Zoom-1 img').attr('src',imgsrc); 
        $(".mz-thumb").removeClass( "mz-thumb-selected" );     
        $('#'+selected_image_index).parent().addClass( "mz-thumb-selected" );
                        }
                    });
                    if (is_variant_available) {
                        
                        $('#modal-add_cart').attr('data-product-variant-id', selected_variant_id);
                        //galleryTop.slideTo(selected_image_index, 500, false);
                        //swiperF.slideTo(selected_image_index, 500, false);
                        
                        if (prices[0].special_price < prices[0].price && prices[0].special_price != 0) {
                            price = prices[0].special_price;
                            $('#modal-sku').html(skus[0]);
                            $('#modal-price').html(currency + ' ' + price);
                            $('#modal-striped-price').html(currency + ' ' + prices[0].price);
                            $('#modal-striped-price').show();
                            $('#modal-add_cart').removeAttr('disabled');
                        } else {
                            
                                
                            $('#modal-sku').html(skus[0]);
                            price = prices[0].price;
                            $('#modal-price').html(currency + ' ' + price);
                            $('#modal-striped-price').hide();
                            $('#modal-add_cart').removeAttr('disabled');
                        }
                    } else {
                        
                        price = '<small class="text-danger h5">No Variant available!</small>';
                        $('#modal-price').html(price);
                        $('#modal-striped-price').hide();
                        $('#modal-striped-price').html('');
                        $('#modal-sku').html(variant_skus[i]);
                        $('#modal-add_cart').attr('disabled', 'true');
                    }
                }
            }
        });
    });
    
    $('.attributes').on('change', function (e) {
    
	
        var selected_attributes = [];
        var attributes_length = "";
        var price = "";var sku='';
        var steps="";
        var imgsrc="";
        var is_variant_available = false;
        var variant = [];
        var prices = [];
         var stocks = [];
        var variant_prices = [];
         var variant_stock = [];
         var variant_skus = [];
        var variants = [];
        var variant_ids = [];
        var image_indexes = [];
        var selected_image_index;
        //selected_attributes.push($(this).val());
        steps=$(this).attr('data-step');
    
         $('.single-product-active').trigger('to.owl.carousel', steps);
         imgsrc=$('#'+steps).attr('src');
        $('#zoom1').attr('src',imgsrc);
        $('.variants').each(function () {
            var pricing=0;
            var special=0;
            sku=$(this).data('sku');
            pricing=$(this).data('price');
            special=$(this).data('special_price');
            pricing=pricing.toFixed(3);
            special=special.toFixed(3);
            prices = {
                price: pricing,
                special_price: special
            };
            variant_skus.push($(this).data('sku'));
            variant_ids.push($(this).data('id'));
            variant_stock.push($(this).data('stock'));
            variant_prices.push(prices);
            variant = $(this).val().split(',');
            variants.push(variant);
            image_indexes.push($(this).data('image-index'));
        });
        attributes_length = variant.length;
        $('.attributes').each(function (i, e) {
            if ($(this).is('select')) {
            selected_attributes.push($(this).val());
            }
            if ($(this).prop('checked')) {
            //if ($(this).is(':checked') || $(this).val() != null ) {
               
                selected_attributes.push($(this).val());
            }
            if(selected_attributes.length>=1)
            {
                if (selected_attributes.length == attributes_length) {
                    
                    /* compare the aselected_attributes.lengthrrays */
                    prices = [];
                    stocks=[];
                    var selected_variant_id = '';
                    var skus =[];
                    $.each(variants, function (i, e) {
                        if (arrays_equal(selected_attributes, e)) {
                            
                            
                            is_variant_available = true;
                            prices.push(variant_prices[i]);
                            stocks.push(variant_stock[i]);
                            skus.push(variant_skus[i]);
                            //alert(variant_stock[i]);
                            if(stocks[0]==0)
                            {
                                
                                $('#stockmsg').show();
                            }
                            else
                            {
                                $('#stockmsg').hide();
                            }
                            
                            
                            selected_variant_id = variant_ids[i];
                            selected_image_index = image_indexes[i];
                           imgsrc=$('#'+selected_image_index).attr('srcset');
                           
        $('#Zoom-1').attr('href',imgsrc);
        $('#Zoom-1').attr('data-zoom-image-2x',imgsrc);
        $('#Zoom-1 img').attr('src',imgsrc); 
        $(".mz-thumb").removeClass( "mz-thumb-selected" );     
        $('#'+selected_image_index).parent().addClass( "mz-thumb-selected" );
                        }
                    });
                    if (is_variant_available) {
                        
                        $('#add_cart').attr('data-product-variant-id', selected_variant_id);
                        //galleryTop.slideTo(selected_image_index, 500, false);
                        //swiperF.slideTo(selected_image_index, 500, false);
                        
                        if (prices[0].special_price < prices[0].price && prices[0].special_price != 0) {
                            price = prices[0].special_price;
                            $('#sku').html(skus[0]);
                            $('#price').html(currency + ' ' + price);
                            $('#striped-price').html(currency + ' ' + prices[0].price);
                            $('#striped-price').show();
                            $('#add_cart').removeAttr('disabled');
                        } else {
                            
                                
                            $('#sku').html(skus[0]);
                            price = prices[0].price;
                            $('#price').html(currency + ' ' + price);
                            $('#striped-price').hide();
                            $('#add_cart').removeAttr('disabled');
                        }
                    } else {
                        
                        price = '<small class="text-danger h5">No Variant available!</small>';
                        $('#price').html(price);
                        $('#striped-price').hide();
                        $('#striped-price').html('');
                        $('#sku').html(variant_skus[i]);
                        $('#add_cart').attr('disabled', 'true');
                    }
                }
            }
        });
    });
    function arrays_equal(_arr1, _arr2) {
        if (
            !Array.isArray(_arr1) ||
            !Array.isArray(_arr2) ||
            _arr1.length !== _arr2.length
        ) {
            return false;
        }
    
        const arr1 = _arr1.concat().sort();
        const arr2 = _arr2.concat().sort();
    
        for (let i = 0; i < arr1.length; i++) {
            if (arr1[i] !== arr2[i]) {
                return false;
            }
        }
    
        return true;
    }    
    $(document).on('submit', '.form-submit-event', function (e) {
        e.preventDefault();
        var formData = new FormData(this);
    
        var error_box = $('#error_box', this);
        var submit_btn = $(this).find('.submit_btn');
        var btn_html = $(this).find('.submit_btn').html();
        var btn_val = $(this).find('.submit_btn').val();
        var button_text = (btn_html != '' || btn_html != 'undefined') ? btn_html : btn_val;
    
    
        formData.append(csrfName, csrfHash);
    
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            beforeSend: function () {
                submit_btn.html('Please Wait..');
                submit_btn.attr('disabled', true);
            },
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (result) {
                csrfName = result['csrfName'];
                csrfHash = result['csrfHash'];
                if (result['error'] == true) {
                    error_box.addClass("rounded p-3 alert alert-danger").removeClass('d-none alert-success');
                    error_box.show().delay(5000).fadeOut();
                    error_box.html(result['message']);
                    submit_btn.html(button_text);
                    submit_btn.attr('disabled', false);
                } else {
                    error_box.addClass("rounded p-3 alert alert-success").removeClass('d-none alert-danger');
                    error_box.show();
                    error_box.html(result['message']);
                    submit_btn.html(button_text);
                    submit_btn.attr('disabled', false);
                    $('.form-submit-event')[0].reset();
                   location.href=base_url;
    
                }
            }
        });
    });   

    $(document).on('submit', '.form-submit-event1', function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        var type = $(this).find('#type').val();
        var error_box = $('#error_box', this);
        var submit_btn = $(this).find('.submit_btn');
        var btn_html = $(this).find('.submit_btn').html();
        var btn_val = $(this).find('.submit_btn').val();
        var button_text = (btn_html != '' || btn_html != 'undefined') ? btn_html : btn_val;
          formData.append(csrfName, csrfHash);
    
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            beforeSend: function () {
                submit_btn.html('Please Wait..');
                submit_btn.attr('disabled', true);
            },
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (result) {
                csrfName = result['csrfName'];
                csrfHash = result['csrfHash'];
                if (result['error'] == true) {
                    error_box.addClass("rounded p-3 alert alert-danger").removeClass('d-none alert-success');
                    error_box.show().delay(5000).fadeOut();
                    error_box.html(result['message']);
                    submit_btn.html(button_text);
                    submit_btn.attr('disabled', false);
                } else {
                    error_box.addClass("rounded p-3 alert alert-success").removeClass('d-none alert-danger');
                    error_box.show().delay(3000).fadeOut();
                    error_box.html(result['message']);
                    submit_btn.html(button_text);
                    submit_btn.attr('disabled', false);
                    
                   if(type == 'checkout'){
                    location.reload();
                   }
    
                }
            }
        });
    });
    $('#sign-up-error').hide();
$(document).on('submit', '.sign-up-form', function (e) {
	
    e.preventDefault();
$('#sign-up-error').hide();
    
	var $first_name='';
    var $last_name ='';
	var $phone='';
	var $email='';
	var $passwd='';
    var $confirmpasswd='';
    var $captcha='';
	$first_name = $('input[name="first_name"]').val();
    $last_name = $('input[name="last_name"]').val();
    $phone =  $('input[name="phone"]').val();    
    $email = $('input[name="email"]').val();
    $passwd = $('input[name="password"]').val();
    $confirmpasswd = $('input[name="password_confirmation"]').val();
    $privacy_policy = $('input[name="privacy_policy"]:checked').val();
    $captcha =  $('input[name="captcha"]').val();
    $.ajax({
        type: 'POST',
        url: base_url + 'auth/register_user',
        data: { mobile: $phone, first_name: $first_name,last_name: $last_name, email: $email, password: $passwd, confirm_password:$confirmpasswd,captcha:$captcha,privacy_policy:$privacy_policy,[csrfName]: csrfHash },
        dataType: 'json',
        success: function (result) {
            csrfName = result.csrfName;
            csrfHash = result.csrfHash;
			$('#sign-up-error').show();
            if (result.error == true) {
				
                $('#sign-up-error').html('<span class="text-danger" >' + result.message + '</span>');
            }
			else
			{
				
				$('#sign-up-error').html('<span class="text-success" >' + result.message + '</span>');
				 $('.sign-up-form')[0].reset();
				  
			}
        }
    });
});
$('#add-address-form').on('submit', function (e) {
    e.preventDefault();
    var formdata = new FormData(this);
    formdata.append(csrfName, csrfHash);
    $.ajax({
        type: 'POST',
        data: formdata,
        url: $(this).attr('action'),
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $('#save-address-submit-btn').val('Please Wait...').attr('disabled', true);
        },
        success: function (result) {
            csrfName = result.csrfName;
            csrfHash = result.csrfHash;
            if (result.error == false) {
                $('#save-address-result').html("<div class='alert alert-success'>" + result.message + "</div>").delay(1500).fadeOut();
                $('#add-address-form')[0].reset();
                $('#save-address-submit-btn').val('Save').attr('disabled', false);
                //$('#address_list_table').bootstrapTable('refresh');
                location.href= base_url +'my-account/manage-address';
            } else {
                $('#save-address-result').html("<div class='alert alert-danger'>" + result.message + "</div>").delay(1500).fadeOut();
            }
            $('#save-address-submit-btn').val('Save').attr('disabled', false);
        }
    })
});

$(document).on('click', '.delete-address', function (e) {
    e.preventDefault();
    var addr_id = $(this).data('id');
    if (confirm('Are you sure ? You want to delete this address?')) {
        $.ajax({
            type: 'POST',
            data: {
                'id': $(this).data('id'),
                [csrfName]: csrfHash,
            },
            url: base_url + 'my-account/delete-address',
            dataType: 'json',
            success: function (result) {
                csrfName = result.csrfName;
                csrfHash = result.csrfHash;
                if (result.error == false) {
                    Toast.fire({
                        icon: 'success',
                        title: result.message
                    });
                    $('#addr_'+addr_id).remove();
                } else {
                    Toast.fire({
                        icon: 'error',
                        title: result.message
                    });
                }
            }
        })
    }
});


$('#checkout_form').on('submit', function (e) {
    e.preventDefault();
    var formdata = new FormData(this);
   
    formdata.append(csrfName, csrfHash);
    $.ajax({
        type: 'POST',
        data: formdata,
        url: $(this).attr('action'),
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $('#save-order').val('Please Wait...').attr('disabled', true);
        },
        success: function (result) {
            csrfName = result.csrfName;
            csrfHash = result.csrfHash;
            if (result.error == false) {
                // $('#save-address-result').html("<div class='alert alert-success'>" + result.message + "</div>").delay(1500).fadeOut();
                // $('#add-address-form')[0].reset();
                // $('#save-address-submit-btn').val('Save').attr('disabled', false);
                //$('#address_list_table').bootstrapTable('refresh');
                Toast.fire({
                        icon: 'success',
                        title: result.message
                    });
					location.href = base_url + 'payment/success';
            } else {
                $('#save-result').html("<div class='alert alert-danger'>" + result.message + "</div>");
                $('#save-order').val('PAY').attr('disabled', false);
            }
        
        }
    })
});
$('.update-order').on('click', function (e) {
    e.preventDefault();
    var formdata = new FormData();
    var order_id = $(this).data('order-id');
    var status = $(this).data('status');
    var temp = '';
    if (status == "cancelled") {
        temp = "Cancel";
    } else {
        temp = 'Return';
    }
    if (confirm('Are you sure you want to ' + temp + ' this order ?')) {
        var t = $(this);
        var btn_text = t.text();
        formdata.append(csrfName, csrfHash);
        formdata.append('order_id', order_id);
        formdata.append('status', status);
        $.ajax({
            type: 'POST',
            url: base_url + 'my-account/update-order',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            beforeSend: function () {
                t.html('Please Wait').attr('disabled', true);
            },
            success: function (result) {
                csrfName = result.csrfName;
                csrfHash = result.csrfHash;
                if (result.error == false) {
                    Toast.fire({
                        icon: 'success',
                        title: result.message
                    });
                    window.location.reload();
                } else {
                    Toast.fire({
                        icon: 'error',
                        title: result.message
                    });
                }
                t.html(btn_text).attr('disabled', false);
            }
        });
    }
});
$('.update-order-item').on('click', function (e) {
    e.preventDefault();
    var formdata = new FormData();
    var order_item_id = $(this).data('item-id');
    var status = $(this).data('status');
    var t = $(this);
    var btn_text = t.text();
    formdata.append(csrfName, csrfHash);
    formdata.append('order_item_id', order_item_id);
    formdata.append('status', status);
    $.ajax({
        type: 'POST',
        url: base_url + 'my-account/update-order-item-status',
        data: formdata,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function () {
            t.html('Please Wait').attr('disabled', true);
        },
        success: function (result) {
            csrfName = result.csrfName;
            csrfHash = result.csrfHash;
            if (result.error == false) {
                Toast.fire({
                    icon: 'success',
                    title: result.message
                });
                window.location.reload();
            } else {
                Toast.fire({
                    icon: 'error',
                    title: result.message
                });
            }
            t.html(btn_text).attr('disabled', false);
        }
    });
});

function sort_product(sort){
   
    location.href = setUrlParameter(location.href, 'sort', sort);
}
function setUrlParameter(url, paramName, paramValue) {
    paramName = paramName.replace(/\s+/g, '-');
    if (paramValue == null || paramValue == '') {
        return url.replace(new RegExp('[?&]' + paramName + '=[^&#]*(#.*)?$'), '$1')
            .replace(new RegExp('([?&])' + paramName + '=[^&]*&'), '$1');
    }
    var pattern = new RegExp('\\b(' + paramName + '=).*?(&|#|$)');
    if (url.search(pattern) >= 0) {
        return url.replace(pattern, '$1' + paramValue + '$2');
    }
    url = url.replace(/[?#]$/, '');
    return url + (url.indexOf('?') > 0 ? '&' : '?') + paramName + '=' + paramValue;
}
$('.filter-checkbox input[type=checkbox]').change(function (e) {
    e.preventDefault();
    var arr = [];
    var i=0;
    $('.brand_filter:checked').each(function () {
        arr[i++] = $(this).val();
    });
    var brand = arr.toString();
    location.href = setUrlParameter(location.href, 'brand', brand);
});

$("#redeem_btn").on('click', function (event) {
    event.preventDefault();
    var formdata = new FormData();
    formdata.append(csrfName, csrfHash);
    formdata.append('promo_code', $('#promocode_input').val());
    $.ajax({
        type: 'POST',
        data: formdata,
        url: base_url + 'cart/validate-promo-code',
        dataType: 'json',
        cache: false,
        processData: false,
        contentType: false,
        success: function (data) {
            csrfName = data.csrfName;
            csrfHash = data.csrfHash;
            if (data.error == false) {
                Toast.fire({
                    icon: 'success',
                    title: data.message
                });
                var $total =0;
                var $promo_code_discount = 0;
                var $cartSubTotal =  parseFloat($('#cartSubTotal').val());
                var $finaltotal =  parseFloat($('#final_total').val());
                if(data.data[0].discount_type =='percentage'){
                    $promocode_discount = $cartSubTotal  * parseFloat(data.data[0].discount) / 100;
                }
                else{
                    $promocode_discount = parseFloat(data.data[0].discount);
                }
                if ($promocode_discount <= data.data[0].max_discount_amount) {
                    $total = $finaltotal - $promocode_discount;
                } else {
                    $total = $finaltotal - parseFloat(data.data[0].max_discount_amount);
                    $promocode_discount = parseFloat(data.data[0].max_discount_amount);
                }
                
                $("#promo-input-box").hide();
                $("#promo-text-box").show();
                $("#promocodeBox").show();
               // alert($total);
              //  $('#promocode_div').removeClass('d-none');
                $("#promo-text-box").html('<span>Coupon Applied: '+data.data[0].promo_code +' </span>');
                $("#promocode_text").html(data.data[0].promo_code);
                $('#promocode_amount').text($promocode_discount.toFixed(2));
                $('#cart-total').text($total.toFixed(2));
                $('#final_total').val($total.toFixed(2));
               $('#clear_promo_btn').removeClass('d-none');
                $('#redeem_btn').hide();

            } else {
                Toast.fire({
                    icon: 'error',
                    title: data.message
                });
            }
        }
    })
});
$("#clear_promo_btn").on('click', function (event) {
    event.preventDefault();
    $("#promo-input-box").show();
    $("#promo-text-box").hide();
    $("#promo-text-box").html('');
    $("#promocodeBox").hide();
    $('#promocode_div').addClass('d-none');
    var promocode_amount = $('#promocode_amount').text();
    var final_total = $('#final_total').val();
    var new_final_total = parseFloat(promocode_amount) + parseFloat(final_total);
    $('#final_total').val(new_final_total.toFixed(2));
    $('#cart-total').text(new_final_total.toFixed(2));
    $('#clear_promo_btn').addClass('d-none');
    $('#redeem_btn').show();
    $('#promocode_input').val('');
});