-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 10, 2021 at 10:07 AM
-- Server version: 5.7.36
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eurocomk_dbdemo`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alternate_mobile` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` mediumtext COLLATE utf8mb4_unicode_ci,
  `street` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `block` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avenue` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `house` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `landmark` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `country_code` int(11) DEFAULT NULL,
  `state` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `user_id`, `first_name`, `last_name`, `name`, `type`, `email`, `mobile`, `alternate_mobile`, `address`, `street`, `block`, `avenue`, `house`, `landmark`, `area_id`, `city_id`, `pincode`, `country_code`, `state`, `country`, `latitude`, `longitude`, `is_default`) VALUES
(1, 34, 'Ragi KG', 'sdfds', 'Ragi KG sdfds', 'office', NULL, '5454455445', NULL, 'sfsdf\r\nsfsdfsd\r\nasdfsdf', 'fdsfsdfsd', '345', 'dfsd', 'dfd', '22wrqw', 1, 1, 680545, 0, '1', '1', 'dasd', 'asdas', 1),
(2, 34, 'Ragi', 'Siva', 'Ragi Siva', 'home', NULL, '9947375424', NULL, NULL, 'dsf', '343', 'fgdfgd', '233', NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(3, 34, 'ammu', 'ws', 'ammu ws', 'office', NULL, '2323234545', NULL, NULL, 'dsf', '22', 'sfsdf', 'sfsdf', NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(5, 34, 'ammu', 'ws', 'ammu ws', 'home', NULL, '1254652314', NULL, NULL, 'dsf', '12', '545245', 'mbkj', NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(6, 28, 'Ragi', 'Siva', 'Ragi Siva', 'home', NULL, '07025903270', NULL, NULL, 'dsf', '2', 'gjgj', 'gcfhggh', NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(7, 29, 'shefiz', 'sha', 'shefiz sha', 'home', 'shefiz.sha@gmail.com', '09995533783', NULL, NULL, 'thomson center . varandarpilly', '96', '325', 'ffasf', NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 1),
(8, 1, 'Admin', 'test', 'Admin test', 'home', 'ragigopal@gmail.com', '07025903270', NULL, NULL, 'dsf', '12', '22', 'jghjg', NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 1),
(9, 32, 'Abdul', 'Gafoor', 'Abdul Gafoor', 'office', NULL, '66329078', NULL, NULL, '34', 'Block 9', 'a', '35', NULL, 2, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(10, 33, 'ragi', 'K g', 'ragi K g', NULL, 'ragigopal1@gmail.com', '4334343434', '', NULL, '', '', '', '', NULL, 1, NULL, NULL, 0, NULL, '117', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `id` int(11) NOT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `city_id` int(11) NOT NULL,
  `minimum_free_delivery_order_amount` double NOT NULL DEFAULT '100',
  `delivery_charges` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `name`, `ar_name`, `city_id`, `minimum_free_delivery_order_amount`, `delivery_charges`) VALUES
(1, 'Test area', '', 1, 300, 15),
(2, 'Maidan Hawally', '', 1, 200, 10);

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `attribute_set_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `attribute_set_id`, `name`, `ar_name`, `type`, `date_created`, `status`) VALUES
(7, 2, 'colour', '', NULL, '2021-06-16 18:23:14', 1),
(19, 6, 'Storage Capacity', '', NULL, '2021-09-02 06:50:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_set`
--

CREATE TABLE `attribute_set` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_set`
--

INSERT INTO `attribute_set` (`id`, `name`, `ar_name`, `status`) VALUES
(1, 'Sizes', '', 1),
(2, 'colour', '', 1),
(6, 'Capacity', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

CREATE TABLE `attribute_values` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `filterable` int(11) DEFAULT '0',
  `value` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_value` varchar(200) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_values`
--

INSERT INTO `attribute_values` (`id`, `attribute_id`, `filterable`, `value`, `ar_value`, `status`) VALUES
(25, 7, 0, 'Red', '', 1),
(26, 7, 0, 'Blue', '', 1),
(27, 7, 0, 'Yellow', '', 1),
(28, 7, 0, 'Purple', '', 1),
(29, 7, 0, 'Green', '', 1),
(30, 7, 0, 'Pink', '', 1),
(39, 7, 0, 'Lilac', '', 1),
(46, 7, 0, 'White', '', 1),
(47, 7, 0, 'Peach', '', 1),
(48, 7, 0, 'Black', '', 1),
(76, 19, 0, '128GB', '', 1),
(77, 19, 0, '256GB', '', 1),
(78, 7, 0, 'Rose GOld', '', 1),
(79, 19, 0, '512GB', '', 1),
(80, 19, 0, '64GB', '', 1),
(81, 19, 0, '32GB', '', 1),
(82, 19, 0, '16GB', '', 1),
(83, 7, 0, 'Gold', 'ذهبي', 1),
(84, 7, 0, 'Silver', 'فضة', 1),
(85, 7, 0, 'Graphite', 'الجرافيت', 1),
(86, 7, 0, 'Grey', 'رمادي', 1),
(87, 7, 0, 'Violet', 'البنفسجي', 1),
(88, 7, 0, 'Awesome White', 'رهيبة بيضاء', 1),
(89, 7, 0, 'Awesome Violet', 'البنفسجي الرائع', 1),
(90, 7, 0, 'Awesome Blue', 'رائع الأزرق', 1);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner` text COLLATE utf8mb4_unicode_ci,
  `row_order` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `ar_name`, `parent_id`, `slug`, `image`, `banner`, `row_order`, `status`) VALUES
(1, 'Cellularline', 'سيلولارلاين', 0, 'cellularline', 'uploads/media/2021/6.png', '', 0, 1),
(2, 'Otter Box', '', 0, 'otter-box', 'uploads/media/2021/1.png', NULL, 0, 1),
(3, 'Apple', '', 0, 'apple', 'uploads/media/2021/2.png', NULL, 0, 1),
(4, 'Huawei', '', 0, 'huawei', 'uploads/media/2021/4.png', NULL, 0, 1),
(5, 'Anker', '', 0, 'anker', 'uploads/media/2021/5.png', NULL, 0, 1),
(6, '', '', 0, '', '', NULL, 0, NULL),
(7, 'Apple', '', 0, 'apple-2', 'uploads/media/2021/564654dsaf.png', '', 0, NULL),
(8, 'Samsung', 'سامسونج', 0, 'samsung-5', 'uploads/media/2021/sam.jpg', NULL, 0, 1),
(9, 'Xiaomi', 'شاومى', 0, 'xiaomi-2', 'uploads/media/2021/xiomi.jpg', NULL, 0, 1),
(10, 'Arun', 'آرون', 0, 'arun', 'uploads/media/2021/11.png', NULL, 0, 1),
(11, 'Dexim', 'دكسيم', 0, 'dexim', 'uploads/media/2021/dexim.jpg', NULL, 0, 1),
(12, 'Porodo', 'برودو', 0, 'porodo-1', 'uploads/media/2021/porodo.jpg', NULL, 0, 1),
(13, 'Baseus', 'باسوس', 0, 'baseus', 'uploads/media/2021/baseus.jpg', NULL, 0, 1),
(14, 'Patchworks', 'باتش ووركس', 0, 'patchworks', 'uploads/media/2021/patchworks.jpg', NULL, 0, 1),
(15, 'Grip2U', 'جريب تو يو', 0, 'grip2u', 'uploads/media/2021/grip2u.jpg', NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `product_variant_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `is_saved_for_later` int(11) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `customer_id`, `product_variant_id`, `qty`, `is_saved_for_later`, `date_created`) VALUES
(2, '80760', NULL, 1, 1, 0, '2021-08-17 09:29:36'),
(3, '2', NULL, 69, 1, 0, '2021-08-23 14:45:02'),
(5, '5', NULL, 68, 1, 0, '2021-08-24 07:23:07'),
(7, '3710000', NULL, 69, 1, 0, '2021-08-24 10:33:07'),
(11, '0', NULL, 67, 1, 0, '2021-08-25 16:47:54'),
(12, '3', NULL, 69, 2, 0, '2021-08-25 16:55:46'),
(13, '3', NULL, 66, 1, 0, '2021-08-25 16:56:05'),
(14, '5e56a96dd1f906bcdbcdafdb274262925b676108', 0, 68, 2, 0, '2021-08-26 22:28:50'),
(15, 'd2dd7c85230564394c05496ef0efc5a31b387306', 0, 67, 1, 0, '2021-08-30 10:32:05'),
(22, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 68, 4, 0, '2021-08-30 14:49:26'),
(23, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 67, 1, 0, '2021-08-30 14:49:27'),
(24, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 66, 1, 0, '2021-08-30 14:49:29'),
(25, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 65, 1, 0, '2021-08-30 14:49:31'),
(26, 'de006396523b82f427fc277e43e089c1a71a3fbc', 29, 54, 1, 0, '2021-08-30 15:03:26'),
(27, 'de006396523b82f427fc277e43e089c1a71a3fbc', 29, 67, 1, 0, '2021-08-30 15:12:15'),
(30, '51a408f7a4fc83967487ae362d849405a1d860d8', 0, 67, 1, 0, '2021-09-01 10:04:04'),
(32, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 47, 2, 0, '2021-09-02 12:21:35'),
(33, '7834f7a363576cfe7196a85337f7d12441dfa903', 0, 68, 3, 0, '2021-09-04 09:30:03'),
(34, '7834f7a363576cfe7196a85337f7d12441dfa903', 0, 65, 1, 0, '2021-09-04 09:55:54'),
(35, '7834f7a363576cfe7196a85337f7d12441dfa903', 0, 71, 1, 0, '2021-09-04 10:16:14'),
(40, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 72, 1, 0, '2021-09-06 08:57:45'),
(41, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 70, 1, 0, '2021-09-06 08:57:48'),
(42, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 71, 1, 0, '2021-09-06 08:57:57'),
(43, 'b99106109e9ee52c894a2b9deb4979d93584cd57', 0, 135, 1, 0, '2021-09-14 10:23:07'),
(44, '6a6eb19b586fa6840111a50f9e8b0169ecb5cef9', 0, 137, 1, 0, '2021-09-16 17:27:53'),
(45, '6a6eb19b586fa6840111a50f9e8b0169ecb5cef9', 0, 47, 2, 0, '2021-09-16 17:57:42'),
(46, 'eeba365fbb4a318b501540905a6834ac0ce7e93e', 0, 137, 4, 0, '2021-09-17 14:30:07'),
(47, '2b3dc58da8122d454eee23bc4346f6fa01439ae2', 0, 137, 1, 0, '2021-09-18 14:10:11'),
(48, '37faf5ce48385c17e2fc8a297473252023428786', 0, 133, 3, 0, '2021-09-18 15:55:15'),
(49, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 46, 1, 0, '2021-09-21 13:44:03'),
(50, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 144, 1, 0, '2021-09-23 12:37:22'),
(51, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 150, 1, 0, '2021-09-23 15:01:27'),
(52, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 149, 1, 0, '2021-09-23 15:08:53'),
(53, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 149, 1, 0, '2021-09-25 11:51:28'),
(54, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 152, 1, 0, '2021-09-25 12:49:07'),
(55, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 153, 1, 0, '2021-09-25 12:49:35'),
(56, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 154, 1, 0, '2021-09-25 13:42:09'),
(57, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 157, 1, 0, '2021-09-25 14:16:34'),
(58, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 166, 1, 0, '2021-09-25 15:02:17'),
(59, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 165, 1, 0, '2021-09-25 15:07:57'),
(61, '71f9c747b67a1771866d6d7f777bc8885cf51439', 0, 149, 1, 0, '2021-09-26 22:06:43'),
(62, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 202, 1, 0, '2021-09-27 05:47:30'),
(63, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 201, 1, 0, '2021-09-27 05:52:18'),
(64, '967ac18cf26dc533f1810d5ba56554ebaf143015', 0, 160, 1, 0, '2021-09-27 10:13:40'),
(65, '2c3d0b4e54483e7244d05b61d76bf0b364a38783', 0, 194, 1, 0, '2021-09-27 11:35:12'),
(66, '993cf567517af250cad4d0a2b394bf4a48e81d41', 0, 190, 1, 0, '2021-09-28 05:14:44'),
(67, 'a9ebcb82f615612b41ac141d9338b2296bd14c9c', 0, 197, 1, 0, '2021-09-28 05:22:01'),
(70, '68ee5f411637119f6e5bbb84b98fbec7e6c94129', 0, 193, 1, 0, '2021-09-28 12:52:00'),
(72, '2c399abd0c28dadff9c410f60d9ea3d4d3c902ec', 0, 149, 1, 0, '2021-09-29 11:43:59'),
(76, '4cc3511f35a28a53242e50711537b851f0fdad28', 0, 185, 1, 0, '2021-10-01 23:23:03'),
(77, '0e8e018b6c8c623f988061fc4084150d4c0621dd', 0, 148, 1, 0, '2021-10-02 10:50:24'),
(78, '0e8e018b6c8c623f988061fc4084150d4c0621dd', 0, 144, 1, 0, '2021-10-02 10:51:08'),
(80, '6f89fea6253e954c3dc9e09301769d303cf58bc0', 0, 186, 2, 0, '2021-10-02 11:56:58'),
(81, '3f3e439efe024cae880942e7d115464e92a05828', 0, 192, 1, 0, '2021-10-02 13:25:00'),
(82, '3f3e439efe024cae880942e7d115464e92a05828', 0, 190, 2, 0, '2021-10-02 13:25:33'),
(83, '604cb731edc2c97feaad90c13665e35e49088a30', 0, 150, 2, 0, '2021-10-05 08:03:32'),
(84, 'a10a869bf426728bfd9429c12d406ccd8c33653f', 0, 192, 1, 0, '2021-10-05 08:34:13'),
(86, 'dfcd2142ec941cafdced07af3942871d27852242', 1, 151, 1, 0, '2021-10-05 10:17:55'),
(89, 'd1c72be8ba638e4107e5de4140654b968637cfd7', 0, 192, 1, 0, '2021-10-05 10:43:24'),
(90, 'd1c72be8ba638e4107e5de4140654b968637cfd7', 0, 198, 1, 0, '2021-10-05 10:48:57'),
(91, 'e2fcd61955ada39d406819a772cfca4faab8ecd0', 1, 190, 1, 0, '2021-10-05 12:20:19'),
(92, 'a10a869bf426728bfd9429c12d406ccd8c33653f', 1, 197, 1, 0, '2021-10-05 12:21:35'),
(93, 'e2fcd61955ada39d406819a772cfca4faab8ecd0', 1, 181, 2, 0, '2021-10-05 12:35:50'),
(94, 'e2fcd61955ada39d406819a772cfca4faab8ecd0', 1, 186, 1, 0, '2021-10-05 12:57:02'),
(95, 'e2fcd61955ada39d406819a772cfca4faab8ecd0', 1, 148, 1, 0, '2021-10-05 13:59:40'),
(99, '473cc70821b2659d8c8ef52aaebd7b2ca5cf8f4a', 0, 189, 1, 0, '2021-10-07 10:04:04'),
(100, 'd3582f334c7f173cb4e61c9ef03b93c668cd4039', 1, 194, 1, 0, '2021-10-07 13:44:49'),
(101, '28e10a49beae6000778fb9e9976d71f1e87166ab', 0, 212, 1, 0, '2021-10-09 11:05:29'),
(102, '28e10a49beae6000778fb9e9976d71f1e87166ab', 0, 185, 1, 0, '2021-10-09 11:05:38'),
(103, '28e10a49beae6000778fb9e9976d71f1e87166ab', 0, 190, 6, 0, '2021-10-09 11:07:55'),
(106, '69382f20d65e9091a977b324d2a3b215f4a1133d', 0, 194, 1, 0, '2021-10-11 12:45:04'),
(107, '69382f20d65e9091a977b324d2a3b215f4a1133d', 0, 192, 1, 0, '2021-10-11 13:46:04'),
(108, '17048da9a6b48bdd9e20c9dd28ad0f68a97bebe1', 0, 209, 1, 0, '2021-10-11 14:08:08'),
(109, '8448c227767b87442a991fa3405efadf6526807a', 1, 169, 1, 0, '2021-10-12 12:16:59'),
(110, 'c6c9fd8eb670e03959007095d6e6217acfa1e28d', 1, 185, 1, 0, '2021-10-12 15:15:47'),
(112, '228fb7a16e3a00eb64dad4beab32e4b0b18f668b', 1, 186, 1, 0, '2021-10-16 13:20:22'),
(113, 'b72d229840fd36110db816077cfb87c27234c8a8', 0, 189, 1, 0, '2021-10-17 10:11:05'),
(114, 'bf4a391165d6444d8e3aaba9ef573f1c1e1199dc', 0, 189, 1, 0, '2021-10-18 10:20:18'),
(115, '0045e32a37054b549823eb1c15520c7b7843e7fa', 0, 197, 1, 0, '2021-10-18 13:35:38'),
(116, '0045e32a37054b549823eb1c15520c7b7843e7fa', 1, 192, 1, 0, '2021-10-18 13:37:36'),
(117, '0045e32a37054b549823eb1c15520c7b7843e7fa', 1, 189, 1, 0, '2021-10-18 13:42:36'),
(118, '0045e32a37054b549823eb1c15520c7b7843e7fa', 1, 185, 1, 0, '2021-10-18 13:42:38'),
(119, '0045e32a37054b549823eb1c15520c7b7843e7fa', 1, 181, 1, 0, '2021-10-18 13:42:41'),
(120, '0045e32a37054b549823eb1c15520c7b7843e7fa', 1, 199, 1, 0, '2021-10-18 13:43:32'),
(121, '0045e32a37054b549823eb1c15520c7b7843e7fa', 1, 176, 1, 0, '2021-10-18 13:44:37'),
(122, '0045e32a37054b549823eb1c15520c7b7843e7fa', 1, 210, 1, 0, '2021-10-18 14:12:52'),
(123, 'fb9abaee375b30f8b2606fbc8e6e6512a0e8a127', 0, 198, 1, 0, '2021-10-19 07:45:41'),
(124, '5a09b1662a6584a99da48cbdee568df3212033d2', 0, 196, 1, 0, '2021-10-19 07:46:53'),
(125, 'fb9abaee375b30f8b2606fbc8e6e6512a0e8a127', 0, 197, 1, 0, '2021-10-19 07:56:16'),
(126, '2c84f0c5fce1b64881cb02766b0ee9a4a8529c92', 1, 194, 1, 0, '2021-10-19 11:47:06'),
(127, '2c84f0c5fce1b64881cb02766b0ee9a4a8529c92', 1, 187, 1, 0, '2021-10-19 11:47:38'),
(128, '2c84f0c5fce1b64881cb02766b0ee9a4a8529c92', 1, 190, 1, 0, '2021-10-19 11:50:55'),
(129, '2c84f0c5fce1b64881cb02766b0ee9a4a8529c92', 1, 144, 1, 0, '2021-10-19 11:52:25'),
(130, '5c2766f436fc52df7d9e9034170289dce4d2957d', 0, 198, 1, 0, '2021-10-19 22:56:25'),
(131, '5c2766f436fc52df7d9e9034170289dce4d2957d', 0, 197, 1, 0, '2021-10-19 22:56:27'),
(132, '5c2766f436fc52df7d9e9034170289dce4d2957d', 0, 196, 1, 0, '2021-10-19 22:56:28'),
(134, '3da5d83c7565dfe8fe7fd451a72f70007793b6f3', 0, 196, 1, 0, '2021-10-20 11:49:10'),
(138, 'a656004a1e4dd7444c4c3dab1833a5cd0baa6017', 0, 198, 1, 0, '2021-10-21 10:58:34'),
(139, 'a656004a1e4dd7444c4c3dab1833a5cd0baa6017', 0, 147, 1, 0, '2021-10-21 13:29:43'),
(140, 'a656004a1e4dd7444c4c3dab1833a5cd0baa6017', 0, 152, 1, 0, '2021-10-21 13:30:18'),
(141, 'a656004a1e4dd7444c4c3dab1833a5cd0baa6017', 0, 144, 1, 0, '2021-10-21 13:30:24'),
(142, '516922cd97893c029cc884d291debf4c239a99c3', 0, 196, 1, 0, '2021-10-21 14:59:59'),
(143, '01dddca6f3df495fbd7adbd6f8b23815469904b4', 0, 192, 1, 0, '2021-10-24 00:52:59'),
(144, '3ff554f8ad677c5d8854b59d9096303171794881', 1, 160, 3, 0, '2021-10-24 06:47:04'),
(145, '3ff554f8ad677c5d8854b59d9096303171794881', 1, 197, 1, 0, '2021-10-24 06:50:33'),
(146, '3ff554f8ad677c5d8854b59d9096303171794881', 1, 198, 1, 0, '2021-10-24 06:50:47'),
(148, '7b5894d6727110bf040bcb4006722e33298d7a77', 33, 192, 1, 0, '2021-10-25 05:26:51'),
(149, '5e273a14c8154fe1b02616de28d6462486325344', 1, 196, 1, 0, '2021-10-26 09:52:21'),
(151, '650e7f8ab659a2e380fc1453ad0c7862a75cfb28', 0, 196, 1, 0, '2021-10-26 10:00:04'),
(152, '9eef0b81d46abafe3fb11689400637f3e668e5a8', 0, 210, 1, 0, '2021-10-31 05:15:56'),
(158, 'c04fb25255a447d979383525d559a92ea9b92ad4', 0, 222, 1, 0, '2021-11-08 11:27:37'),
(159, '6f19545006bcf8fcdb50f16913ce95c16378d00b', 0, 193, 3, 0, '2021-11-08 11:27:52'),
(160, '2135ce601e10b5379ef614ae54421930b2422de5', 0, 222, 1, 0, '2021-11-08 11:42:24'),
(166, '82b61ef92d6b9fe763a6d2ca36d8bade2c498d94', 0, 228, 1, 0, '2021-11-08 12:47:19'),
(170, 'a057d69112e34d066ea10936c994733a28e3ea93', 1, 230, 1, 0, '2021-11-08 12:59:01'),
(174, '82b61ef92d6b9fe763a6d2ca36d8bade2c498d94', 1, 223, 1, 0, '2021-11-08 13:03:55'),
(175, 'daa6ab4f1b1398cff66297e51b831e4c2115f7ba', 0, 224, 1, 0, '2021-11-08 15:34:15'),
(176, 'daa6ab4f1b1398cff66297e51b831e4c2115f7ba', 0, 148, 1, 0, '2021-11-08 15:36:04'),
(177, '7355d9960dd725c8d3063e9272cf42729427fa2e', 0, 198, 1, 0, '2021-11-09 07:42:39'),
(179, 'ff9ad5ee98d25e1a7f3320086b34d76ec461556e', 0, 242, 1, 0, '2021-11-10 03:23:07');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_icon` varchar(50) CHARACTER SET utf8 NOT NULL,
  `top_sub_cat` int(11) DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner` text COLLATE utf8mb4_unicode_ci,
  `row_order` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `ar_name`, `parent_id`, `slug`, `cat_icon`, `top_sub_cat`, `image`, `banner`, `row_order`, `status`) VALUES
(1, 'Accessories', 'اكسسوارات', 0, 'accessories-1', 'icon-cable', NULL, 'uploads/media/2021/2518389-200.png', '', 14, 1),
(2, 'Powerbank', 'باور بانك', 1, 'powerbank-1', '', 1, 'uploads/media/2021/5.png', '', 16, 1),
(3, 'Aux cable', '', 1, 'aux-cable', '', NULL, '', '', 4, 0),
(4, 'Charger', 'الشاحن', 1, 'charger', '', NULL, '', '', 19, 0),
(5, 'Speaker', '', 1, 'speaker', '', NULL, '', '', 6, 0),
(6, 'Cables', '', 1, 'cables-1', '', 1, '', '', 4, NULL),
(7, 'Car Charger', '', 1, 'car-charger', '', NULL, '', '', 7, 0),
(8, 'Car Charger', '', 1, 'car-charger-1', '', NULL, 'uploads/media/2021/000940784.jpg', NULL, 0, NULL),
(9, 'Mobile Phone', 'هاتف محمول', 0, 'mobile-phone-1', 'icon-phone', 1, 'uploads/media/2021/000941797.jpg', '', 0, 1),
(10, 'Airpod Pro', 'ايربود برو', 0, 'airpod-pro-1', 'icon-tablet2', NULL, 'uploads/media/2021/0100011086.jpg', '', 9, 1),
(11, 'Airpod', 'ايربود', 0, 'airpod-1', 'icon-tablet', NULL, 'uploads/media/2021/0100011086.jpg', '', 10, 1),
(12, 'Ipad', 'اى باد', 0, 'ipad-1', 'icon-tablet', NULL, 'uploads/media/2021/0100011253.jpg', '', 7, 1),
(13, 'Watch', 'ساعة', 0, 'watch-1', 'icon-watch', NULL, 'uploads/media/2021/0100011310.jpg', '', 11, 1),
(14, 'Apple TV', 'آبل', 0, 'apple-tv-1', 'icon-apple', NULL, 'uploads/media/2021/000942131.jpg', '', 8, 1),
(15, 'Battery Case', '', 18, 'battery-case-1', '', NULL, 'uploads/media/2021/000941168.jpg', '', 0, 1),
(16, 'Pencil', '', 19, 'pencil', '', NULL, 'uploads/media/2021/000941213.jpg', '', 0, 1),
(17, 'MOBILES', '', 9, 'mobiles', '', NULL, 'uploads/media/2021/0100011186.jpg', NULL, 0, 0),
(18, 'MOBILE ACCESSORIES', '', 9, 'mobile-accessories-1', '', NULL, 'uploads/media/2021/000941327.jpeg', '', 0, 0),
(19, 'MOBILE ACCESSORIES', '', 1, 'mobile-accessories', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 0),
(20, 'Apple', 'ابل', 9, 'apple-3', '', NULL, 'uploads/media/2021/0100010992.jpeg', '', 1, 1),
(21, 'Anker', 'انكر', 2, 'anker-1', '', NULL, 'uploads/media/2021/5.png', '', 0, 1),
(22, 'Anker', 'انكر', 144, 'anker-11', '', NULL, 'uploads/media/2021/5.png', '', 0, 1),
(23, 'Anker', 'انكر', 145, 'anker', '', NULL, 'uploads/media/2021/5.png', '', 0, 1),
(24, 'Anker', 'انكر', 147, 'anker-2', '', NULL, 'uploads/media/2021/5.png', '', 0, 1),
(25, 'Anker', '', 6, 'anker-4', '', NULL, 'uploads/media/2021/5.png', NULL, 0, 1),
(26, 'Anker', '', 7, 'anker-5', '', NULL, 'uploads/media/2021/5.png', NULL, 0, 1),
(27, 'Earbuds', '', 1, 'earbuds', '', NULL, 'uploads/media/2021/000941885.png', NULL, 8, 0),
(28, 'Anker', '', 27, 'anker-6', '', NULL, 'uploads/media/2021/000941885.png', NULL, 0, 1),
(29, 'Travel Charger', '', 1, 'travel-charger', '', NULL, 'uploads/media/2021/000941060.jpg', NULL, 9, 0),
(30, 'Earphone', 'سماعة الأذن', 1, 'earphone-1', '', NULL, 'uploads/media/2021/000941312.jpg', '', 23, 1),
(31, 'Travel Kit', '', 1, 'travel-kit', '', NULL, 'uploads/media/2021/000941324.jpg', NULL, 11, 0),
(32, 'Wireless Charger', '', 1, 'wireless-charger', '', NULL, 'uploads/media/2021/000941338.jpg', NULL, 12, 0),
(33, 'Anker', '', 29, 'anker-7', '', NULL, 'uploads/media/2021/000941060.jpg', NULL, 0, 1),
(34, 'Anker', '', 30, 'anker-8', '', NULL, 'uploads/media/2021/5.png', NULL, 0, 1),
(35, 'Anker', '', 31, 'anker-9', '', NULL, 'uploads/media/2021/5.png', NULL, 0, 1),
(36, 'Anker', '', 32, 'anker-10', '', NULL, 'uploads/media/2021/5.png', NULL, 0, 1),
(37, 'iPhone 12 Pro Max', 'آيفون 12 برو ماكس', 20, 'iphone-12-pro-max-2', '', NULL, 'uploads/media/2021/2.png', '', 0, 1),
(38, 'Xiaomi', 'شاومي', 2, 'xiaomi-2', '', NULL, 'uploads/media/2021/xiomi.jpg', '', 0, 1),
(39, 'Otterbox', 'اوتر بوكس', 2, 'otterbox', '', NULL, 'uploads/media/2021/2.png', '', 0, 1),
(40, 'Zendure', '', 2, 'zendure', '', NULL, 'uploads/media/2021/2.png', NULL, 0, 0),
(41, 'Arun', 'آرون', 2, 'arun-1', '', NULL, 'uploads/media/2021/11.png', '', 0, 1),
(42, 'Belkin', '', 2, 'belkin', '', NULL, 'uploads/media/2021/2.png', NULL, 0, 0),
(43, 'MI', '', 2, 'mi', '', NULL, 'uploads/media/2021/2.png', NULL, 0, 0),
(44, 'Patchworks', 'باتش ووركس', 144, 'patchworks', '', NULL, 'uploads/media/2021/patchworks.jpg', '', 0, 1),
(45, 'Qmadix', '', 4, 'qmadix-1', '', NULL, 'uploads/media/2021/000942131.jpg', '', 0, 1),
(46, 'Zendure', '', 4, 'zendure-1', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 0),
(47, 'Samsung', '', 4, 'samsung-3', '', NULL, 'uploads/media/2021/000942131.jpg', '', 0, NULL),
(48, 'Belkin', '', 4, 'belkin-1', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 0),
(49, 'Vidvie', '', 4, 'vidvie', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 0),
(50, 'Patchworks', 'باتش ووركس', 60, 'patchworks-1', '', NULL, 'uploads/media/2021/9.png', '', 0, 1),
(51, 'Qmadix', '', 6, 'qmadix-2', '', NULL, 'uploads/media/2021/000942132.jpg', '', 0, 1),
(52, 'Belkin', '', 6, 'belkin-2', '', NULL, 'uploads/media/2021/000942132.jpg', NULL, 0, 0),
(53, 'Optiva', '', 6, 'optiva', '', NULL, 'uploads/media/2021/000942132.jpg', NULL, 0, 0),
(54, 'Zendure', '', 6, 'zendure-2', '', NULL, 'uploads/media/2021/000942132.jpg', NULL, 0, 0),
(55, 'Samsung', '', 6, 'samsung-4', '', NULL, 'uploads/media/2021/000942132.jpg', '', 0, NULL),
(56, 'Apple Accessories', '', 1, 'apple-accessories', '', 1, 'uploads/media/2021/2.png', '', 15, NULL),
(57, 'Cables', '', 56, 'cables-2', '', 1, 'uploads/media/2021/2.png', '', 0, NULL),
(58, 'Adapter', '', 56, 'adapter', '', NULL, 'uploads/media/2021/2.png', NULL, 0, NULL),
(59, 'Earphones', '', 56, 'earphones', '', NULL, 'uploads/media/2021/2.png', NULL, 0, NULL),
(60, 'Case', 'قضية', 1, 'case', '', NULL, 'uploads/media/2021/2.png', '', 17, 1),
(61, 'Smart Case', '', 56, 'smart-case', '', NULL, 'uploads/media/2021/2.png', NULL, 0, NULL),
(62, 'Audio', '', 1, 'audio', '', 1, 'uploads/media/2021/000941313.jpg', '', 17, NULL),
(63, 'Wireless Earphones', '', 62, 'wireless-earphones', '', NULL, 'uploads/media/2021/000941312.jpg', NULL, 0, NULL),
(64, 'Wireless Headphones', '', 62, 'wireless-headphones', '', NULL, 'uploads/media/2021/000941312.jpg', NULL, 0, NULL),
(65, 'Wired Earphones', '', 62, 'wired-earphones', '', NULL, 'uploads/media/2021/000941312.jpg', NULL, 0, NULL),
(66, 'Portable Speakers', '', 62, 'portable-speakers', '', NULL, 'uploads/media/2021/000941312.jpg', NULL, 0, NULL),
(67, 'Ipods', '', 62, 'ipods', '', NULL, 'uploads/media/2021/000941312.jpg', NULL, 0, NULL),
(68, 'Cases/Covers/Screen Guard', '', 1, 'casescoversscreen-guard-1', '', 1, 'uploads/media/2021/000942131.jpg', '', 18, NULL),
(69, 'Apple', '', 68, 'apple', '', NULL, 'uploads/media/2021/2.png', NULL, 0, NULL),
(70, 'Samsung', '', 68, 'samsung', '', NULL, 'uploads/media/2021/2.png', NULL, 0, NULL),
(71, 'Huawei', '', 68, 'huawei', '', NULL, 'uploads/media/2021/2.png', NULL, 0, NULL),
(72, 'OnePlus', '', 68, 'oneplus', '', NULL, 'uploads/media/2021/2.png', NULL, 0, 0),
(73, 'HTC', '', 68, 'htc', '', NULL, 'uploads/media/2021/2.png', NULL, 0, 0),
(74, 'Nokia', '', 68, 'nokia', '', NULL, 'uploads/media/2021/2.png', NULL, 0, NULL),
(75, 'Xiaomi', '', 68, 'xiaomi-1', '', NULL, 'uploads/media/2021/2.png', '', 0, NULL),
(76, 'iPhone 12 Pro', 'ايفون 12 برو', 20, 'iphone-12-pro', '', NULL, 'uploads/media/2021/2.png', '', 0, 1),
(77, 'iPhone 12 Mini', 'اي فون 12 ميني', 20, 'iphone-12-mini-1', '', NULL, 'uploads/media/2021/2.png', '', 0, 1),
(78, 'iPhone 12', '', 20, 'iphone-12', '', NULL, 'uploads/media/2021/2.png', NULL, 0, 0),
(79, 'iPhone 11 Pro Max', 'ايفون 11 برو ماكس', 20, 'iphone-11-pro-max-2', '', NULL, 'uploads/media/2021/2.png', '', 0, 1),
(80, 'iPhone 11 Pro', 'آيفون 11 برو', 20, 'iphone-11-pro-2', '', NULL, 'uploads/media/2021/2.png', '', 0, 1),
(81, 'iPhone 11', '', 20, 'iphone-11', '', NULL, 'uploads/media/2021/2.png', NULL, 0, 0),
(82, 'iPhone SE 2020', 'آيفون SE 2020', 20, 'iphone-se-2', '', NULL, 'uploads/media/2021/2.png', '', 0, 1),
(83, 'Samsung', 'سامسونج', 9, 'samsung-2', '', NULL, 'uploads/media/2021/3.png', '', 3, 1),
(84, 'HUAWEI', 'هواوى', 9, 'huawei-3', '', NULL, 'uploads/media/2021/000942131.jpg', '', 2, 1),
(85, 'ONEPLUS', '', 9, 'oneplus-1', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 13, 0),
(86, 'VIVO', '', 9, 'vivo-1', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 11, 0),
(87, 'XIAOMI', 'شاومي', 9, 'xiaomi-3', '', NULL, 'uploads/media/2021/000942131.jpg', '', 4, 1),
(88, 'RED MAGIC', '', 9, 'red-magic', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 15, 0),
(89, 'HTC', '', 9, 'htc-1', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 14, 0),
(90, 'SONY', '', 9, 'sony', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 16, 0),
(91, 'NOKIA', 'نوكيا', 9, 'nokia-1', '', NULL, 'uploads/media/2021/000942131.jpg', '', 5, 1),
(92, 'ALCATEL', '', 9, 'alcatel', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 18, 0),
(93, 'OTHERS', '', 9, 'others', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 19, 0),
(94, 'Samsung S20 Series', '', 83, 'samsung-s20-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(95, 'Samsung A Series', '', 83, 'samsung-a-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(96, 'Samsung M Series', '', 83, 'samsung-m-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(97, 'Samsung J Serie', '', 83, 'samsung-j-serie', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(98, 'Galaxy Fold', '', 83, 'galaxy-fold', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(99, 'Z Flip', '', 83, 'z-flip', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(100, 'Galaxy Note', '', 83, 'galaxy-note', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(101, 'Mate 30 Pro', 'ميت 30 برو', 84, 'mate-30-pro-1', '', NULL, 'uploads/media/2021/000942131.jpg', '', 0, 1),
(102, 'Y Series', 'Y Series', 84, 'y-series-2', '', NULL, 'uploads/media/2021/000942131.jpg', '', 0, 1),
(103, 'Nova Series', 'Nova series', 84, 'nova-series-1', '', NULL, 'uploads/media/2021/000942131.jpg', '', 0, 1),
(104, 'P Series', '', 84, 'p-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(105, '8 Series', '', 85, '8-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(106, '7 Series', '', 85, '7-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(107, 'Nord Series', '', 85, 'nord-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(108, 'S Series', '', 86, 's-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(109, 'Y Series', '', 86, 'y-series-1', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 0),
(110, 'V Series', '', 86, 'v-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(111, 'MI 9 Series', '', 87, 'mi-9-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(112, 'REDMI 8 Series', '', 87, 'redmi-8-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(113, 'REDMI NOTE 8 Series', '', 87, 'redmi-note-8-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(114, 'Red Magic 5G', '', 88, 'red-magic-5g', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 0),
(115, 'U Series', '', 89, 'u-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(116, 'Desire Series', '', 89, 'desire-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(117, 'Xperia L Series', '', 90, 'xperia-l-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(118, 'Xperia XA Series', '', 90, 'xperia-xa-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(119, 'Xperia Z Series', '', 90, 'xperia-z-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(120, 'Nokia Series (5, 3, 2)', '', 91, 'nokia-series-5-3-2', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(121, 'Nokia Dual Series', '', 91, 'nokia-dual-series', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(122, 'Nokia N105 Single', '', 91, 'nokia-n105-single', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(123, '2008D', '', 92, '2008d', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(124, '2053D', '', 92, '2053d', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(125, 'SOL', '', 93, 'sol', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(126, 'Black Shark', '', 93, 'black-shark', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(127, 'Microsoft', '', 93, 'microsoft', '', NULL, 'uploads/media/2021/000942131.jpg', NULL, 0, 1),
(128, 'iPhone 11 Pro', '', 9, 'iphone-11-pro-1', '', NULL, '', NULL, 0, NULL),
(129, 'iPhone 11', '', 9, 'iphone-1', '', NULL, '', NULL, 0, NULL),
(130, 'iPhone 11 Pro Max', '', 9, 'iphone-11-pro-max-1', '', NULL, '', NULL, 0, NULL),
(131, 'iPhone SE 2020', '', 9, 'iphone-se-1', '', NULL, '', NULL, 0, NULL),
(132, 'iPhone 12', '', 9, 'iphone-2', '', NULL, '', NULL, 0, NULL),
(133, 'iPhone 12 Pro', '', 9, 'iphone-12-pro-1', '', NULL, '', NULL, 0, NULL),
(134, 'iPhone 12 Pro Max', '', 9, 'iphone-12-pro-max-1', '', NULL, '', NULL, 0, NULL),
(135, 'test', '', 9, 'test', '', 1, 'uploads/media/2021/000942134.jpg', NULL, 0, 0),
(136, 'test-1', '', 135, 'test-1', '', NULL, 'uploads/media/2021/000942134.jpg', NULL, 0, 0),
(137, 'Powerbank', '', 1, 'powerbank', '', NULL, 'uploads/media/2021/i1-2.jpg', NULL, 0, 0),
(138, 'Unco', '', 2, 'unco', '', NULL, 'uploads/media/2021/000942134.jpg', NULL, 0, NULL),
(139, 'Iphone 8 Plus', 'ايفون 8 بلس', 20, 'iphone-8-plus', '', NULL, 'uploads/media/2021/2.png', NULL, 0, 1),
(140, 'Redmi Note 10 Series', '10 ريدمي نوت', 87, 'redmi-note-10-series', '', NULL, 'uploads/media/2021/Xiaomi_Redmi_Note-blue.jpg', NULL, 0, 1),
(141, 'Ipad Pro', 'ايباد برو', 0, 'ipad-pro-1', 'icon-tablet', NULL, 'uploads/media/2021/ipad_gld.jpg', '', 6, 1),
(142, 'Router', 'جهاز التوجيه', 0, 'router-1', 'icon-wifi-lock', NULL, 'uploads/media/2021/000942135.jpg', '', 12, 1),
(143, 'Tablet', 'تابلت', 0, 'tablet', 'icon-tablet', NULL, 'uploads/media/2021/ipad_gr.jpg', NULL, 13, 1),
(144, 'USB cable', 'كابل USB', 1, 'usb-cable', '', NULL, 'uploads/media/2021/11.png', NULL, 15, 1),
(145, 'Aux Cable', 'كابل Aux', 1, 'aux-cable-1', '', NULL, 'uploads/media/2021/5.png', NULL, 18, 1),
(146, 'Car Mount', 'جبل السيارة', 1, 'car-mount', '', NULL, 'uploads/media/2021/9.png', NULL, 21, 1),
(147, 'Car Charger', 'شاحن السيارة', 1, 'car-charger-2', '', NULL, 'uploads/media/2021/5.png', NULL, 20, 1),
(148, 'Back Strap', 'حزام الظهر', 1, 'back-strap', '', NULL, 'uploads/media/2021/13.png', NULL, 19, 1),
(149, 'Charger', 'الشاحن', 1, 'charger-1', '', NULL, 'uploads/media/2021/5.png', NULL, 22, 1),
(150, 'Samsung', 'سامسونج', 144, 'samsung-1', '', NULL, 'uploads/media/2021/3.png', NULL, 0, 1),
(151, 'HUAWEI', 'هواوى', 144, 'huawei-1', '', NULL, 'uploads/media/2021/4.png', NULL, 0, 1),
(152, 'Apple', 'ابل', 144, 'apple-2', '', NULL, 'uploads/media/2021/2.png', NULL, 0, 1),
(153, 'Cellularline', 'سيلولارلاين', 144, 'cellularline', '', NULL, 'uploads/media/2021/6.png', NULL, 0, 1),
(154, 'Dexim', 'دكسيم', 144, 'dexim', '', NULL, 'uploads/media/2021/dexim.jpg', NULL, 0, 1),
(155, 'Arun', 'آرون', 144, 'arun', '', NULL, 'uploads/media/2021/11.png', NULL, 0, 1),
(156, 'Porodo', 'برودو', 144, 'porodo', '', NULL, 'uploads/media/2021/porodo.jpg', NULL, 0, 1),
(157, 'Baseus', 'باسوس', 144, 'baseus', '', NULL, 'uploads/media/2021/baseus.jpg', NULL, 0, 1),
(158, 'Grip2U', 'جريب تو يو', 60, 'grip2u', '', NULL, 'uploads/media/2021/grip2u.jpg', NULL, 0, 1),
(159, 'Cellularline', 'سيلولارلاين', 60, 'cellularline-1', '', NULL, 'uploads/media/2021/6.png', NULL, 0, 1),
(160, 'Apple', 'ابل', 60, 'apple-1', '', NULL, 'uploads/media/2021/2.png', NULL, 0, 1),
(161, 'Porodo', 'برودو', 145, 'porodo-1', '', NULL, 'uploads/media/2021/porodo.jpg', NULL, 0, 1),
(162, 'Baseus', 'باسوس', 145, 'baseus-1', '', NULL, 'uploads/media/2021/baseus.jpg', NULL, 0, 1),
(163, 'HUAWEI', 'هواوى', 147, 'huawei-2', '', NULL, 'uploads/media/2021/4.png', NULL, 0, 1),
(164, 'Cellularline', 'سيلولارلاين', 147, 'cellularline-2', '', NULL, 'uploads/media/2021/6.png', NULL, 0, 1),
(165, 'flygrip', 'فلاى جريب', 148, 'flygrip', '', NULL, 'uploads/media/2021/flygrip.jpg', NULL, 0, 1),
(166, 'iPhone 13 Pro', 'iPhone 13 Pro', 9, 'iphone-13-pro', '', NULL, '', NULL, 0, NULL),
(167, 'iPhone 13 Pro Max', 'iPhone 13 Pro Max', 9, 'iphone-13-pro-max', '', NULL, '', NULL, 0, NULL),
(168, 'iPhone 13 Pro Max', 'iPhone 13 Pro Max', 9, 'iphone-13-pro-max-1', '', NULL, '', NULL, 0, NULL),
(169, 'iPhone 13 Pro Max', 'iPhone 13 Pro Max', 20, 'iphone-13-pro-max-3', '', NULL, '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(200) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `ar_name`) VALUES
(1, 'Hawally', '');

-- --------------------------------------------------------

--
-- Table structure for table `client_api_keys`
--

CREATE TABLE `client_api_keys` (
  `id` int(11) NOT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci,
  `secret` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client_api_keys`
--

INSERT INTO `client_api_keys` (`id`, `name`, `secret`, `status`) VALUES
(1, 'Jaydeep Goswami', '65c9cd19cd138f19ddf2f6320c7a802ee936c548', 1);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iso3` char(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numeric_code` char(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capital` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_symbol` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tld` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `native` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subregion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezones` text COLLATE utf8mb4_unicode_ci,
  `translations` text COLLATE utf8mb4_unicode_ci,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `emoji` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emojiU` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `wikiDataId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Rapid API GeoDB Cities'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country`, `iso3`, `numeric_code`, `country_code`, `phone_code`, `capital`, `currency_code`, `currency_symbol`, `tld`, `native`, `region`, `subregion`, `timezones`, `translations`, `latitude`, `longitude`, `emoji`, `emojiU`, `created_at`, `updated_at`, `flag`, `wikiDataId`) VALUES
(1, 'Afghanistan', 'AFG', '004', 'AF', '93', 'Kabul', 'AFN', '؋', '.af', 'افغانستان', 'Asia', 'Southern Asia', '[{\"zoneName\":\"Asia\\/Kabul\",\"gmtOffset\":16200,\"gmtOffsetName\":\"UTC+04:30\",\"abbreviation\":\"AFT\",\"tzName\":\"Afghanistan Time\"}]', '{\"kr\":\"아프가니스탄\",\"br\":\"Afeganistão\",\"pt\":\"Afeganistão\",\"nl\":\"Afghanistan\",\"hr\":\"Afganistan\",\"fa\":\"افغانستان\",\"de\":\"Afghanistan\",\"es\":\"Afganistán\",\"fr\":\"Afghanistan\",\"ja\":\"アフガニスタン\",\"it\":\"Afghanistan\",\"cn\":\"阿富汗\"}', 33.00000000, 65.00000000, '🇦🇫', 'U+1F1E6 U+1F1EB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q889'),
(2, 'Aland Islands', 'ALA', '248', 'AX', '+358-18', 'Mariehamn', 'EUR', '€', '.ax', 'Åland', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/Mariehamn\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"올란드 제도\",\"br\":\"Ilhas de Aland\",\"pt\":\"Ilhas de Aland\",\"nl\":\"Ålandeilanden\",\"hr\":\"Ålandski otoci\",\"fa\":\"جزایر الند\",\"de\":\"Åland\",\"es\":\"Alandia\",\"fr\":\"Åland\",\"ja\":\"オーランド諸島\",\"it\":\"Isole Aland\",\"cn\":\"奥兰群岛\"}', 60.11666700, 19.90000000, '🇦🇽', 'U+1F1E6 U+1F1FD', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(3, 'Albania', 'ALB', '008', 'AL', '355', 'Tirana', 'ALL', 'Lek', '.al', 'Shqipëria', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Tirane\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"알바니아\",\"br\":\"Albânia\",\"pt\":\"Albânia\",\"nl\":\"Albanië\",\"hr\":\"Albanija\",\"fa\":\"آلبانی\",\"de\":\"Albanien\",\"es\":\"Albania\",\"fr\":\"Albanie\",\"ja\":\"アルバニア\",\"it\":\"Albania\",\"cn\":\"阿尔巴尼亚\"}', 41.00000000, 20.00000000, '🇦🇱', 'U+1F1E6 U+1F1F1', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q222'),
(4, 'Algeria', 'DZA', '012', 'DZ', '213', 'Algiers', 'DZD', 'دج', '.dz', 'الجزائر', 'Africa', 'Northern Africa', '[{\"zoneName\":\"Africa\\/Algiers\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"알제리\",\"br\":\"Argélia\",\"pt\":\"Argélia\",\"nl\":\"Algerije\",\"hr\":\"Alžir\",\"fa\":\"الجزایر\",\"de\":\"Algerien\",\"es\":\"Argelia\",\"fr\":\"Algérie\",\"ja\":\"アルジェリア\",\"it\":\"Algeria\",\"cn\":\"阿尔及利亚\"}', 28.00000000, 3.00000000, '🇩🇿', 'U+1F1E9 U+1F1FF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q262'),
(5, 'American Samoa', 'ASM', '016', 'AS', '+1-684', 'Pago Pago', 'USD', '$', '.as', 'American Samoa', 'Oceania', 'Polynesia', '[{\"zoneName\":\"Pacific\\/Pago_Pago\",\"gmtOffset\":-39600,\"gmtOffsetName\":\"UTC-11:00\",\"abbreviation\":\"SST\",\"tzName\":\"Samoa Standard Time\"}]', '{\"kr\":\"아메리칸사모아\",\"br\":\"Samoa Americana\",\"pt\":\"Samoa Americana\",\"nl\":\"Amerikaans Samoa\",\"hr\":\"Američka Samoa\",\"fa\":\"ساموآی آمریکا\",\"de\":\"Amerikanisch-Samoa\",\"es\":\"Samoa Americana\",\"fr\":\"Samoa américaines\",\"ja\":\"アメリカ領サモア\",\"it\":\"Samoa Americane\",\"cn\":\"美属萨摩亚\"}', -14.33333333, -170.00000000, '🇦🇸', 'U+1F1E6 U+1F1F8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(6, 'Andorra', 'AND', '020', 'AD', '376', 'Andorra la Vella', 'EUR', '€', '.ad', 'Andorra', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Andorra\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"안도라\",\"br\":\"Andorra\",\"pt\":\"Andorra\",\"nl\":\"Andorra\",\"hr\":\"Andora\",\"fa\":\"آندورا\",\"de\":\"Andorra\",\"es\":\"Andorra\",\"fr\":\"Andorre\",\"ja\":\"アンドラ\",\"it\":\"Andorra\",\"cn\":\"安道尔\"}', 42.50000000, 1.50000000, '🇦🇩', 'U+1F1E6 U+1F1E9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q228'),
(7, 'Angola', 'AGO', '024', 'AO', '244', 'Luanda', 'AOA', 'Kz', '.ao', 'Angola', 'Africa', 'Middle Africa', '[{\"zoneName\":\"Africa\\/Luanda\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WAT\",\"tzName\":\"West Africa Time\"}]', '{\"kr\":\"앙골라\",\"br\":\"Angola\",\"pt\":\"Angola\",\"nl\":\"Angola\",\"hr\":\"Angola\",\"fa\":\"آنگولا\",\"de\":\"Angola\",\"es\":\"Angola\",\"fr\":\"Angola\",\"ja\":\"アンゴラ\",\"it\":\"Angola\",\"cn\":\"安哥拉\"}', -12.50000000, 18.50000000, '🇦🇴', 'U+1F1E6 U+1F1F4', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q916'),
(8, 'Anguilla', 'AIA', '660', 'AI', '+1-264', 'The Valley', 'XCD', '$', '.ai', 'Anguilla', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Anguilla\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"앵귈라\",\"br\":\"Anguila\",\"pt\":\"Anguila\",\"nl\":\"Anguilla\",\"hr\":\"Angvila\",\"fa\":\"آنگویلا\",\"de\":\"Anguilla\",\"es\":\"Anguilla\",\"fr\":\"Anguilla\",\"ja\":\"アンギラ\",\"it\":\"Anguilla\",\"cn\":\"安圭拉\"}', 18.25000000, -63.16666666, '🇦🇮', 'U+1F1E6 U+1F1EE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(9, 'Antarctica', 'ATA', '010', 'AQ', '672', '', 'AAD', '$', '.aq', 'Antarctica', 'Polar', '', '[{\"zoneName\":\"Antarctica\\/Casey\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"AWST\",\"tzName\":\"Australian Western Standard Time\"},{\"zoneName\":\"Antarctica\\/Davis\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"DAVT\",\"tzName\":\"Davis Time\"},{\"zoneName\":\"Antarctica\\/DumontDUrville\",\"gmtOffset\":36000,\"gmtOffsetName\":\"UTC+10:00\",\"abbreviation\":\"DDUT\",\"tzName\":\"Dumont d\'Urville Time\"},{\"zoneName\":\"Antarctica\\/Mawson\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"MAWT\",\"tzName\":\"Mawson Station Time\"},{\"zoneName\":\"Antarctica\\/McMurdo\",\"gmtOffset\":46800,\"gmtOffsetName\":\"UTC+13:00\",\"abbreviation\":\"NZDT\",\"tzName\":\"New Zealand Daylight Time\"},{\"zoneName\":\"Antarctica\\/Palmer\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"CLST\",\"tzName\":\"Chile Summer Time\"},{\"zoneName\":\"Antarctica\\/Rothera\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ROTT\",\"tzName\":\"Rothera Research Station Time\"},{\"zoneName\":\"Antarctica\\/Syowa\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"SYOT\",\"tzName\":\"Showa Station Time\"},{\"zoneName\":\"Antarctica\\/Troll\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"},{\"zoneName\":\"Antarctica\\/Vostok\",\"gmtOffset\":21600,\"gmtOffsetName\":\"UTC+06:00\",\"abbreviation\":\"VOST\",\"tzName\":\"Vostok Station Time\"}]', '{\"kr\":\"남극\",\"br\":\"Antártida\",\"pt\":\"Antárctida\",\"nl\":\"Antarctica\",\"hr\":\"Antarktika\",\"fa\":\"جنوبگان\",\"de\":\"Antarktika\",\"es\":\"Antártida\",\"fr\":\"Antarctique\",\"ja\":\"南極大陸\",\"it\":\"Antartide\",\"cn\":\"南极洲\"}', -74.65000000, 4.48000000, '🇦🇶', 'U+1F1E6 U+1F1F6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(10, 'Antigua And Barbuda', 'ATG', '028', 'AG', '+1-268', 'St. John\'s', 'XCD', '$', '.ag', 'Antigua and Barbuda', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Antigua\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"앤티가 바부다\",\"br\":\"Antígua e Barbuda\",\"pt\":\"Antígua e Barbuda\",\"nl\":\"Antigua en Barbuda\",\"hr\":\"Antigva i Barbuda\",\"fa\":\"آنتیگوا و باربودا\",\"de\":\"Antigua und Barbuda\",\"es\":\"Antigua y Barbuda\",\"fr\":\"Antigua-et-Barbuda\",\"ja\":\"アンティグア・バーブーダ\",\"it\":\"Antigua e Barbuda\",\"cn\":\"安提瓜和巴布达\"}', 17.05000000, -61.80000000, '🇦🇬', 'U+1F1E6 U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q781'),
(11, 'Argentina', 'ARG', '032', 'AR', '54', 'Buenos Aires', 'ARS', '$', '.ar', 'Argentina', 'Americas', 'South America', '[{\"zoneName\":\"America\\/Argentina\\/Buenos_Aires\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ART\",\"tzName\":\"Argentina Time\"},{\"zoneName\":\"America\\/Argentina\\/Catamarca\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ART\",\"tzName\":\"Argentina Time\"},{\"zoneName\":\"America\\/Argentina\\/Cordoba\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ART\",\"tzName\":\"Argentina Time\"},{\"zoneName\":\"America\\/Argentina\\/Jujuy\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ART\",\"tzName\":\"Argentina Time\"},{\"zoneName\":\"America\\/Argentina\\/La_Rioja\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ART\",\"tzName\":\"Argentina Time\"},{\"zoneName\":\"America\\/Argentina\\/Mendoza\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ART\",\"tzName\":\"Argentina Time\"},{\"zoneName\":\"America\\/Argentina\\/Rio_Gallegos\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ART\",\"tzName\":\"Argentina Time\"},{\"zoneName\":\"America\\/Argentina\\/Salta\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ART\",\"tzName\":\"Argentina Time\"},{\"zoneName\":\"America\\/Argentina\\/San_Juan\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ART\",\"tzName\":\"Argentina Time\"},{\"zoneName\":\"America\\/Argentina\\/San_Luis\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ART\",\"tzName\":\"Argentina Time\"},{\"zoneName\":\"America\\/Argentina\\/Tucuman\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ART\",\"tzName\":\"Argentina Time\"},{\"zoneName\":\"America\\/Argentina\\/Ushuaia\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"ART\",\"tzName\":\"Argentina Time\"}]', '{\"kr\":\"아르헨티나\",\"br\":\"Argentina\",\"pt\":\"Argentina\",\"nl\":\"Argentinië\",\"hr\":\"Argentina\",\"fa\":\"آرژانتین\",\"de\":\"Argentinien\",\"es\":\"Argentina\",\"fr\":\"Argentine\",\"ja\":\"アルゼンチン\",\"it\":\"Argentina\",\"cn\":\"阿根廷\"}', -34.00000000, -64.00000000, '🇦🇷', 'U+1F1E6 U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q414'),
(12, 'Armenia', 'ARM', '051', 'AM', '374', 'Yerevan', 'AMD', '֏', '.am', 'Հայաստան', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Yerevan\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"AMT\",\"tzName\":\"Armenia Time\"}]', '{\"kr\":\"아르메니아\",\"br\":\"Armênia\",\"pt\":\"Arménia\",\"nl\":\"Armenië\",\"hr\":\"Armenija\",\"fa\":\"ارمنستان\",\"de\":\"Armenien\",\"es\":\"Armenia\",\"fr\":\"Arménie\",\"ja\":\"アルメニア\",\"it\":\"Armenia\",\"cn\":\"亚美尼亚\"}', 40.00000000, 45.00000000, '🇦🇲', 'U+1F1E6 U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q399'),
(13, 'Aruba', 'ABW', '533', 'AW', '297', 'Oranjestad', 'AWG', 'ƒ', '.aw', 'Aruba', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Aruba\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"아루바\",\"br\":\"Aruba\",\"pt\":\"Aruba\",\"nl\":\"Aruba\",\"hr\":\"Aruba\",\"fa\":\"آروبا\",\"de\":\"Aruba\",\"es\":\"Aruba\",\"fr\":\"Aruba\",\"ja\":\"アルバ\",\"it\":\"Aruba\",\"cn\":\"阿鲁巴\"}', 12.50000000, -69.96666666, '🇦🇼', 'U+1F1E6 U+1F1FC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(14, 'Australia', 'AUS', '036', 'AU', '61', 'Canberra', 'AUD', '$', '.au', 'Australia', 'Oceania', 'Australia and New Zealand', '[{\"zoneName\":\"Antarctica\\/Macquarie\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"MIST\",\"tzName\":\"Macquarie Island Station Time\"},{\"zoneName\":\"Australia\\/Adelaide\",\"gmtOffset\":37800,\"gmtOffsetName\":\"UTC+10:30\",\"abbreviation\":\"ACDT\",\"tzName\":\"Australian Central Daylight Saving Time\"},{\"zoneName\":\"Australia\\/Brisbane\",\"gmtOffset\":36000,\"gmtOffsetName\":\"UTC+10:00\",\"abbreviation\":\"AEST\",\"tzName\":\"Australian Eastern Standard Time\"},{\"zoneName\":\"Australia\\/Broken_Hill\",\"gmtOffset\":37800,\"gmtOffsetName\":\"UTC+10:30\",\"abbreviation\":\"ACDT\",\"tzName\":\"Australian Central Daylight Saving Time\"},{\"zoneName\":\"Australia\\/Currie\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"AEDT\",\"tzName\":\"Australian Eastern Daylight Saving Time\"},{\"zoneName\":\"Australia\\/Darwin\",\"gmtOffset\":34200,\"gmtOffsetName\":\"UTC+09:30\",\"abbreviation\":\"ACST\",\"tzName\":\"Australian Central Standard Time\"},{\"zoneName\":\"Australia\\/Eucla\",\"gmtOffset\":31500,\"gmtOffsetName\":\"UTC+08:45\",\"abbreviation\":\"ACWST\",\"tzName\":\"Australian Central Western Standard Time (Unofficial)\"},{\"zoneName\":\"Australia\\/Hobart\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"AEDT\",\"tzName\":\"Australian Eastern Daylight Saving Time\"},{\"zoneName\":\"Australia\\/Lindeman\",\"gmtOffset\":36000,\"gmtOffsetName\":\"UTC+10:00\",\"abbreviation\":\"AEST\",\"tzName\":\"Australian Eastern Standard Time\"},{\"zoneName\":\"Australia\\/Lord_Howe\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"LHST\",\"tzName\":\"Lord Howe Summer Time\"},{\"zoneName\":\"Australia\\/Melbourne\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"AEDT\",\"tzName\":\"Australian Eastern Daylight Saving Time\"},{\"zoneName\":\"Australia\\/Perth\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"AWST\",\"tzName\":\"Australian Western Standard Time\"},{\"zoneName\":\"Australia\\/Sydney\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"AEDT\",\"tzName\":\"Australian Eastern Daylight Saving Time\"}]', '{\"kr\":\"호주\",\"br\":\"Austrália\",\"pt\":\"Austrália\",\"nl\":\"Australië\",\"hr\":\"Australija\",\"fa\":\"استرالیا\",\"de\":\"Australien\",\"es\":\"Australia\",\"fr\":\"Australie\",\"ja\":\"オーストラリア\",\"it\":\"Australia\",\"cn\":\"澳大利亚\"}', -27.00000000, 133.00000000, '🇦🇺', 'U+1F1E6 U+1F1FA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q408'),
(15, 'Austria', 'AUT', '040', 'AT', '43', 'Vienna', 'EUR', '€', '.at', 'Österreich', 'Europe', 'Western Europe', '[{\"zoneName\":\"Europe\\/Vienna\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"오스트리아\",\"br\":\"áustria\",\"pt\":\"áustria\",\"nl\":\"Oostenrijk\",\"hr\":\"Austrija\",\"fa\":\"اتریش\",\"de\":\"Österreich\",\"es\":\"Austria\",\"fr\":\"Autriche\",\"ja\":\"オーストリア\",\"it\":\"Austria\",\"cn\":\"奥地利\"}', 47.33333333, 13.33333333, '🇦🇹', 'U+1F1E6 U+1F1F9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q40'),
(16, 'Azerbaijan', 'AZE', '031', 'AZ', '994', 'Baku', 'AZN', 'm', '.az', 'Azərbaycan', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Baku\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"AZT\",\"tzName\":\"Azerbaijan Time\"}]', '{\"kr\":\"아제르바이잔\",\"br\":\"Azerbaijão\",\"pt\":\"Azerbaijão\",\"nl\":\"Azerbeidzjan\",\"hr\":\"Azerbajdžan\",\"fa\":\"آذربایجان\",\"de\":\"Aserbaidschan\",\"es\":\"Azerbaiyán\",\"fr\":\"Azerbaïdjan\",\"ja\":\"アゼルバイジャン\",\"it\":\"Azerbaijan\",\"cn\":\"阿塞拜疆\"}', 40.50000000, 47.50000000, '🇦🇿', 'U+1F1E6 U+1F1FF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q227'),
(17, 'Bahamas The', 'BHS', '044', 'BS', '+1-242', 'Nassau', 'BSD', 'B$', '.bs', 'Bahamas', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Nassau\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America)\"}]', '{\"kr\":\"바하마\",\"br\":\"Bahamas\",\"pt\":\"Baamas\",\"nl\":\"Bahama’s\",\"hr\":\"Bahami\",\"fa\":\"باهاما\",\"de\":\"Bahamas\",\"es\":\"Bahamas\",\"fr\":\"Bahamas\",\"ja\":\"バハマ\",\"it\":\"Bahamas\",\"cn\":\"巴哈马\"}', 24.25000000, -76.00000000, '🇧🇸', 'U+1F1E7 U+1F1F8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q778'),
(18, 'Bahrain', 'BHR', '048', 'BH', '973', 'Manama', 'BHD', '.د.ب', '.bh', '‏البحرين', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Bahrain\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"AST\",\"tzName\":\"Arabia Standard Time\"}]', '{\"kr\":\"바레인\",\"br\":\"Bahrein\",\"pt\":\"Barém\",\"nl\":\"Bahrein\",\"hr\":\"Bahrein\",\"fa\":\"بحرین\",\"de\":\"Bahrain\",\"es\":\"Bahrein\",\"fr\":\"Bahreïn\",\"ja\":\"バーレーン\",\"it\":\"Bahrein\",\"cn\":\"巴林\"}', 26.00000000, 50.55000000, '🇧🇭', 'U+1F1E7 U+1F1ED', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q398'),
(19, 'Bangladesh', 'BGD', '050', 'BD', '880', 'Dhaka', 'BDT', '৳', '.bd', 'Bangladesh', 'Asia', 'Southern Asia', '[{\"zoneName\":\"Asia\\/Dhaka\",\"gmtOffset\":21600,\"gmtOffsetName\":\"UTC+06:00\",\"abbreviation\":\"BDT\",\"tzName\":\"Bangladesh Standard Time\"}]', '{\"kr\":\"방글라데시\",\"br\":\"Bangladesh\",\"pt\":\"Bangladeche\",\"nl\":\"Bangladesh\",\"hr\":\"Bangladeš\",\"fa\":\"بنگلادش\",\"de\":\"Bangladesch\",\"es\":\"Bangladesh\",\"fr\":\"Bangladesh\",\"ja\":\"バングラデシュ\",\"it\":\"Bangladesh\",\"cn\":\"孟加拉\"}', 24.00000000, 90.00000000, '🇧🇩', 'U+1F1E7 U+1F1E9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q902'),
(20, 'Barbados', 'BRB', '052', 'BB', '+1-246', 'Bridgetown', 'BBD', 'Bds$', '.bb', 'Barbados', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Barbados\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"바베이도스\",\"br\":\"Barbados\",\"pt\":\"Barbados\",\"nl\":\"Barbados\",\"hr\":\"Barbados\",\"fa\":\"باربادوس\",\"de\":\"Barbados\",\"es\":\"Barbados\",\"fr\":\"Barbade\",\"ja\":\"バルバドス\",\"it\":\"Barbados\",\"cn\":\"巴巴多斯\"}', 13.16666666, -59.53333333, '🇧🇧', 'U+1F1E7 U+1F1E7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q244'),
(21, 'Belarus', 'BLR', '112', 'BY', '375', 'Minsk', 'BYN', 'Br', '.by', 'Белару́сь', 'Europe', 'Eastern Europe', '[{\"zoneName\":\"Europe\\/Minsk\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"MSK\",\"tzName\":\"Moscow Time\"}]', '{\"kr\":\"벨라루스\",\"br\":\"Bielorrússia\",\"pt\":\"Bielorrússia\",\"nl\":\"Wit-Rusland\",\"hr\":\"Bjelorusija\",\"fa\":\"بلاروس\",\"de\":\"Weißrussland\",\"es\":\"Bielorrusia\",\"fr\":\"Biélorussie\",\"ja\":\"ベラルーシ\",\"it\":\"Bielorussia\",\"cn\":\"白俄罗斯\"}', 53.00000000, 28.00000000, '🇧🇾', 'U+1F1E7 U+1F1FE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q184'),
(22, 'Belgium', 'BEL', '056', 'BE', '32', 'Brussels', 'EUR', '€', '.be', 'België', 'Europe', 'Western Europe', '[{\"zoneName\":\"Europe\\/Brussels\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"벨기에\",\"br\":\"Bélgica\",\"pt\":\"Bélgica\",\"nl\":\"België\",\"hr\":\"Belgija\",\"fa\":\"بلژیک\",\"de\":\"Belgien\",\"es\":\"Bélgica\",\"fr\":\"Belgique\",\"ja\":\"ベルギー\",\"it\":\"Belgio\",\"cn\":\"比利时\"}', 50.83333333, 4.00000000, '🇧🇪', 'U+1F1E7 U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q31'),
(23, 'Belize', 'BLZ', '084', 'BZ', '501', 'Belmopan', 'BZD', '$', '.bz', 'Belize', 'Americas', 'Central America', '[{\"zoneName\":\"America\\/Belize\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America)\"}]', '{\"kr\":\"벨리즈\",\"br\":\"Belize\",\"pt\":\"Belize\",\"nl\":\"Belize\",\"hr\":\"Belize\",\"fa\":\"بلیز\",\"de\":\"Belize\",\"es\":\"Belice\",\"fr\":\"Belize\",\"ja\":\"ベリーズ\",\"it\":\"Belize\",\"cn\":\"伯利兹\"}', 17.25000000, -88.75000000, '🇧🇿', 'U+1F1E7 U+1F1FF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q242'),
(24, 'Benin', 'BEN', '204', 'BJ', '229', 'Porto-Novo', 'XOF', 'CFA', '.bj', 'Bénin', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Porto-Novo\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WAT\",\"tzName\":\"West Africa Time\"}]', '{\"kr\":\"베냉\",\"br\":\"Benin\",\"pt\":\"Benim\",\"nl\":\"Benin\",\"hr\":\"Benin\",\"fa\":\"بنین\",\"de\":\"Benin\",\"es\":\"Benín\",\"fr\":\"Bénin\",\"ja\":\"ベナン\",\"it\":\"Benin\",\"cn\":\"贝宁\"}', 9.50000000, 2.25000000, '🇧🇯', 'U+1F1E7 U+1F1EF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q962'),
(25, 'Bermuda', 'BMU', '060', 'BM', '+1-441', 'Hamilton', 'BMD', '$', '.bm', 'Bermuda', 'Americas', 'Northern America', '[{\"zoneName\":\"Atlantic\\/Bermuda\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"버뮤다\",\"br\":\"Bermudas\",\"pt\":\"Bermudas\",\"nl\":\"Bermuda\",\"hr\":\"Bermudi\",\"fa\":\"برمودا\",\"de\":\"Bermuda\",\"es\":\"Bermudas\",\"fr\":\"Bermudes\",\"ja\":\"バミューダ\",\"it\":\"Bermuda\",\"cn\":\"百慕大\"}', 32.33333333, -64.75000000, '🇧🇲', 'U+1F1E7 U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(26, 'Bhutan', 'BTN', '064', 'BT', '975', 'Thimphu', 'BTN', 'Nu.', '.bt', 'ʼbrug-yul', 'Asia', 'Southern Asia', '[{\"zoneName\":\"Asia\\/Thimphu\",\"gmtOffset\":21600,\"gmtOffsetName\":\"UTC+06:00\",\"abbreviation\":\"BTT\",\"tzName\":\"Bhutan Time\"}]', '{\"kr\":\"부탄\",\"br\":\"Butão\",\"pt\":\"Butão\",\"nl\":\"Bhutan\",\"hr\":\"Butan\",\"fa\":\"بوتان\",\"de\":\"Bhutan\",\"es\":\"Bután\",\"fr\":\"Bhoutan\",\"ja\":\"ブータン\",\"it\":\"Bhutan\",\"cn\":\"不丹\"}', 27.50000000, 90.50000000, '🇧🇹', 'U+1F1E7 U+1F1F9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q917'),
(27, 'Bolivia', 'BOL', '068', 'BO', '591', 'Sucre', 'BOB', 'Bs.', '.bo', 'Bolivia', 'Americas', 'South America', '[{\"zoneName\":\"America\\/La_Paz\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"BOT\",\"tzName\":\"Bolivia Time\"}]', '{\"kr\":\"볼리비아\",\"br\":\"Bolívia\",\"pt\":\"Bolívia\",\"nl\":\"Bolivia\",\"hr\":\"Bolivija\",\"fa\":\"بولیوی\",\"de\":\"Bolivien\",\"es\":\"Bolivia\",\"fr\":\"Bolivie\",\"ja\":\"ボリビア多民族国\",\"it\":\"Bolivia\",\"cn\":\"玻利维亚\"}', -17.00000000, -65.00000000, '🇧🇴', 'U+1F1E7 U+1F1F4', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q750'),
(28, 'Bosnia and Herzegovina', 'BIH', '070', 'BA', '387', 'Sarajevo', 'BAM', 'KM', '.ba', 'Bosna i Hercegovina', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Sarajevo\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"보스니아 헤르체고비나\",\"br\":\"Bósnia e Herzegovina\",\"pt\":\"Bósnia e Herzegovina\",\"nl\":\"Bosnië en Herzegovina\",\"hr\":\"Bosna i Hercegovina\",\"fa\":\"بوسنی و هرزگوین\",\"de\":\"Bosnien und Herzegowina\",\"es\":\"Bosnia y Herzegovina\",\"fr\":\"Bosnie-Herzégovine\",\"ja\":\"ボスニア・ヘルツェゴビナ\",\"it\":\"Bosnia ed Erzegovina\",\"cn\":\"波斯尼亚和黑塞哥维那\"}', 44.00000000, 18.00000000, '🇧🇦', 'U+1F1E7 U+1F1E6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q225'),
(29, 'Botswana', 'BWA', '072', 'BW', '267', 'Gaborone', 'BWP', 'P', '.bw', 'Botswana', 'Africa', 'Southern Africa', '[{\"zoneName\":\"Africa\\/Gaborone\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"CAT\",\"tzName\":\"Central Africa Time\"}]', '{\"kr\":\"보츠와나\",\"br\":\"Botsuana\",\"pt\":\"Botsuana\",\"nl\":\"Botswana\",\"hr\":\"Bocvana\",\"fa\":\"بوتسوانا\",\"de\":\"Botswana\",\"es\":\"Botswana\",\"fr\":\"Botswana\",\"ja\":\"ボツワナ\",\"it\":\"Botswana\",\"cn\":\"博茨瓦纳\"}', -22.00000000, 24.00000000, '🇧🇼', 'U+1F1E7 U+1F1FC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q963'),
(30, 'Bouvet Island', 'BVT', '074', 'BV', '0055', '', 'NOK', 'kr', '.bv', 'Bouvetøya', '', '', '[{\"zoneName\":\"Europe\\/Oslo\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"부벳 섬\",\"br\":\"Ilha Bouvet\",\"pt\":\"Ilha Bouvet\",\"nl\":\"Bouveteiland\",\"hr\":\"Otok Bouvet\",\"fa\":\"جزیره بووه\",\"de\":\"Bouvetinsel\",\"es\":\"Isla Bouvet\",\"fr\":\"Île Bouvet\",\"ja\":\"ブーベ島\",\"it\":\"Isola Bouvet\",\"cn\":\"布维岛\"}', -54.43333333, 3.40000000, '🇧🇻', 'U+1F1E7 U+1F1FB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(31, 'Brazil', 'BRA', '076', 'BR', '55', 'Brasilia', 'BRL', 'R$', '.br', 'Brasil', 'Americas', 'South America', '[{\"zoneName\":\"America\\/Araguaina\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"BRT\",\"tzName\":\"Bras\\u00edlia Time\"},{\"zoneName\":\"America\\/Bahia\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"BRT\",\"tzName\":\"Bras\\u00edlia Time\"},{\"zoneName\":\"America\\/Belem\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"BRT\",\"tzName\":\"Bras\\u00edlia Time\"},{\"zoneName\":\"America\\/Boa_Vista\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AMT\",\"tzName\":\"Amazon Time (Brazil)[3\"},{\"zoneName\":\"America\\/Campo_Grande\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AMT\",\"tzName\":\"Amazon Time (Brazil)[3\"},{\"zoneName\":\"America\\/Cuiaba\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"BRT\",\"tzName\":\"Brasilia Time\"},{\"zoneName\":\"America\\/Eirunepe\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"ACT\",\"tzName\":\"Acre Time\"},{\"zoneName\":\"America\\/Fortaleza\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"BRT\",\"tzName\":\"Bras\\u00edlia Time\"},{\"zoneName\":\"America\\/Maceio\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"BRT\",\"tzName\":\"Bras\\u00edlia Time\"},{\"zoneName\":\"America\\/Manaus\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AMT\",\"tzName\":\"Amazon Time (Brazil)\"},{\"zoneName\":\"America\\/Noronha\",\"gmtOffset\":-7200,\"gmtOffsetName\":\"UTC-02:00\",\"abbreviation\":\"FNT\",\"tzName\":\"Fernando de Noronha Time\"},{\"zoneName\":\"America\\/Porto_Velho\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AMT\",\"tzName\":\"Amazon Time (Brazil)[3\"},{\"zoneName\":\"America\\/Recife\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"BRT\",\"tzName\":\"Bras\\u00edlia Time\"},{\"zoneName\":\"America\\/Rio_Branco\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"ACT\",\"tzName\":\"Acre Time\"},{\"zoneName\":\"America\\/Santarem\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"BRT\",\"tzName\":\"Bras\\u00edlia Time\"},{\"zoneName\":\"America\\/Sao_Paulo\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"BRT\",\"tzName\":\"Bras\\u00edlia Time\"}]', '{\"kr\":\"브라질\",\"br\":\"Brasil\",\"pt\":\"Brasil\",\"nl\":\"Brazilië\",\"hr\":\"Brazil\",\"fa\":\"برزیل\",\"de\":\"Brasilien\",\"es\":\"Brasil\",\"fr\":\"Brésil\",\"ja\":\"ブラジル\",\"it\":\"Brasile\",\"cn\":\"巴西\"}', -10.00000000, -55.00000000, '🇧🇷', 'U+1F1E7 U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q155'),
(32, 'British Indian Ocean Territory', 'IOT', '086', 'IO', '246', 'Diego Garcia', 'USD', '$', '.io', 'British Indian Ocean Territory', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Indian\\/Chagos\",\"gmtOffset\":21600,\"gmtOffsetName\":\"UTC+06:00\",\"abbreviation\":\"IOT\",\"tzName\":\"Indian Ocean Time\"}]', '{\"kr\":\"영국령 인도양 지역\",\"br\":\"Território Britânico do Oceano íÍdico\",\"pt\":\"Território Britânico do Oceano Índico\",\"nl\":\"Britse Gebieden in de Indische Oceaan\",\"hr\":\"Britanski Indijskooceanski teritorij\",\"fa\":\"قلمرو بریتانیا در اقیانوس هند\",\"de\":\"Britisches Territorium im Indischen Ozean\",\"es\":\"Territorio Británico del Océano Índico\",\"fr\":\"Territoire britannique de l\'océan Indien\",\"ja\":\"イギリス領インド洋地域\",\"it\":\"Territorio britannico dell\'oceano indiano\",\"cn\":\"英属印度洋领地\"}', -6.00000000, 71.50000000, '🇮🇴', 'U+1F1EE U+1F1F4', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(33, 'Brunei', 'BRN', '096', 'BN', '673', 'Bandar Seri Begawan', 'BND', 'B$', '.bn', 'Negara Brunei Darussalam', 'Asia', 'South-Eastern Asia', '[{\"zoneName\":\"Asia\\/Brunei\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"BNT\",\"tzName\":\"Brunei Darussalam Time\"}]', '{\"kr\":\"브루나이\",\"br\":\"Brunei\",\"pt\":\"Brunei\",\"nl\":\"Brunei\",\"hr\":\"Brunej\",\"fa\":\"برونئی\",\"de\":\"Brunei\",\"es\":\"Brunei\",\"fr\":\"Brunei\",\"ja\":\"ブルネイ・ダルサラーム\",\"it\":\"Brunei\",\"cn\":\"文莱\"}', 4.50000000, 114.66666666, '🇧🇳', 'U+1F1E7 U+1F1F3', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q921'),
(34, 'Bulgaria', 'BGR', '100', 'BG', '359', 'Sofia', 'BGN', 'Лв.', '.bg', 'България', 'Europe', 'Eastern Europe', '[{\"zoneName\":\"Europe\\/Sofia\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"불가리아\",\"br\":\"Bulgária\",\"pt\":\"Bulgária\",\"nl\":\"Bulgarije\",\"hr\":\"Bugarska\",\"fa\":\"بلغارستان\",\"de\":\"Bulgarien\",\"es\":\"Bulgaria\",\"fr\":\"Bulgarie\",\"ja\":\"ブルガリア\",\"it\":\"Bulgaria\",\"cn\":\"保加利亚\"}', 43.00000000, 25.00000000, '🇧🇬', 'U+1F1E7 U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q219'),
(35, 'Burkina Faso', 'BFA', '854', 'BF', '226', 'Ouagadougou', 'XOF', 'CFA', '.bf', 'Burkina Faso', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Ouagadougou\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"부르키나 파소\",\"br\":\"Burkina Faso\",\"pt\":\"Burquina Faso\",\"nl\":\"Burkina Faso\",\"hr\":\"Burkina Faso\",\"fa\":\"بورکینافاسو\",\"de\":\"Burkina Faso\",\"es\":\"Burkina Faso\",\"fr\":\"Burkina Faso\",\"ja\":\"ブルキナファソ\",\"it\":\"Burkina Faso\",\"cn\":\"布基纳法索\"}', 13.00000000, -2.00000000, '🇧🇫', 'U+1F1E7 U+1F1EB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q965'),
(36, 'Burundi', 'BDI', '108', 'BI', '257', 'Bujumbura', 'BIF', 'FBu', '.bi', 'Burundi', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Bujumbura\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"CAT\",\"tzName\":\"Central Africa Time\"}]', '{\"kr\":\"부룬디\",\"br\":\"Burundi\",\"pt\":\"Burúndi\",\"nl\":\"Burundi\",\"hr\":\"Burundi\",\"fa\":\"بوروندی\",\"de\":\"Burundi\",\"es\":\"Burundi\",\"fr\":\"Burundi\",\"ja\":\"ブルンジ\",\"it\":\"Burundi\",\"cn\":\"布隆迪\"}', -3.50000000, 30.00000000, '🇧🇮', 'U+1F1E7 U+1F1EE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q967'),
(37, 'Cambodia', 'KHM', '116', 'KH', '855', 'Phnom Penh', 'KHR', 'KHR', '.kh', 'Kâmpŭchéa', 'Asia', 'South-Eastern Asia', '[{\"zoneName\":\"Asia\\/Phnom_Penh\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"ICT\",\"tzName\":\"Indochina Time\"}]', '{\"kr\":\"캄보디아\",\"br\":\"Camboja\",\"pt\":\"Camboja\",\"nl\":\"Cambodja\",\"hr\":\"Kambodža\",\"fa\":\"کامبوج\",\"de\":\"Kambodscha\",\"es\":\"Camboya\",\"fr\":\"Cambodge\",\"ja\":\"カンボジア\",\"it\":\"Cambogia\",\"cn\":\"柬埔寨\"}', 13.00000000, 105.00000000, '🇰🇭', 'U+1F1F0 U+1F1ED', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q424'),
(38, 'Cameroon', 'CMR', '120', 'CM', '237', 'Yaounde', 'XAF', 'FCFA', '.cm', 'Cameroon', 'Africa', 'Middle Africa', '[{\"zoneName\":\"Africa\\/Douala\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WAT\",\"tzName\":\"West Africa Time\"}]', '{\"kr\":\"카메룬\",\"br\":\"Camarões\",\"pt\":\"Camarões\",\"nl\":\"Kameroen\",\"hr\":\"Kamerun\",\"fa\":\"کامرون\",\"de\":\"Kamerun\",\"es\":\"Camerún\",\"fr\":\"Cameroun\",\"ja\":\"カメルーン\",\"it\":\"Camerun\",\"cn\":\"喀麦隆\"}', 6.00000000, 12.00000000, '🇨🇲', 'U+1F1E8 U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1009'),
(39, 'Canada', 'CAN', '124', 'CA', '1', 'Ottawa', 'CAD', '$', '.ca', 'Canada', 'Americas', 'Northern America', '[{\"zoneName\":\"America\\/Atikokan\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America)\"},{\"zoneName\":\"America\\/Blanc-Sablon\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"},{\"zoneName\":\"America\\/Cambridge_Bay\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America)\"},{\"zoneName\":\"America\\/Creston\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America)\"},{\"zoneName\":\"America\\/Dawson\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America)\"},{\"zoneName\":\"America\\/Dawson_Creek\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America)\"},{\"zoneName\":\"America\\/Edmonton\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America)\"},{\"zoneName\":\"America\\/Fort_Nelson\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America)\"},{\"zoneName\":\"America\\/Glace_Bay\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"},{\"zoneName\":\"America\\/Goose_Bay\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"},{\"zoneName\":\"America\\/Halifax\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"},{\"zoneName\":\"America\\/Inuvik\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America\"},{\"zoneName\":\"America\\/Iqaluit\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Moncton\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"},{\"zoneName\":\"America\\/Nipigon\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Pangnirtung\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Rainy_River\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Rankin_Inlet\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Regina\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Resolute\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/St_Johns\",\"gmtOffset\":-12600,\"gmtOffsetName\":\"UTC-03:30\",\"abbreviation\":\"NST\",\"tzName\":\"Newfoundland Standard Time\"},{\"zoneName\":\"America\\/Swift_Current\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Thunder_Bay\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Toronto\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Vancouver\",\"gmtOffset\":-28800,\"gmtOffsetName\":\"UTC-08:00\",\"abbreviation\":\"PST\",\"tzName\":\"Pacific Standard Time (North America\"},{\"zoneName\":\"America\\/Whitehorse\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America\"},{\"zoneName\":\"America\\/Winnipeg\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Yellowknife\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America\"}]', '{\"kr\":\"캐나다\",\"br\":\"Canadá\",\"pt\":\"Canadá\",\"nl\":\"Canada\",\"hr\":\"Kanada\",\"fa\":\"کانادا\",\"de\":\"Kanada\",\"es\":\"Canadá\",\"fr\":\"Canada\",\"ja\":\"カナダ\",\"it\":\"Canada\",\"cn\":\"加拿大\"}', 60.00000000, -95.00000000, '🇨🇦', 'U+1F1E8 U+1F1E6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q16'),
(40, 'Cape Verde', 'CPV', '132', 'CV', '238', 'Praia', 'CVE', '$', '.cv', 'Cabo Verde', 'Africa', 'Western Africa', '[{\"zoneName\":\"Atlantic\\/Cape_Verde\",\"gmtOffset\":-3600,\"gmtOffsetName\":\"UTC-01:00\",\"abbreviation\":\"CVT\",\"tzName\":\"Cape Verde Time\"}]', '{\"kr\":\"카보베르데\",\"br\":\"Cabo Verde\",\"pt\":\"Cabo Verde\",\"nl\":\"Kaapverdië\",\"hr\":\"Zelenortska Republika\",\"fa\":\"کیپ ورد\",\"de\":\"Kap Verde\",\"es\":\"Cabo Verde\",\"fr\":\"Cap Vert\",\"ja\":\"カーボベルデ\",\"it\":\"Capo Verde\",\"cn\":\"佛得角\"}', 16.00000000, -24.00000000, '🇨🇻', 'U+1F1E8 U+1F1FB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1011'),
(41, 'Cayman Islands', 'CYM', '136', 'KY', '+1-345', 'George Town', 'KYD', '$', '.ky', 'Cayman Islands', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Cayman\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"}]', '{\"kr\":\"케이먼 제도\",\"br\":\"Ilhas Cayman\",\"pt\":\"Ilhas Caimão\",\"nl\":\"Caymaneilanden\",\"hr\":\"Kajmanski otoci\",\"fa\":\"جزایر کیمن\",\"de\":\"Kaimaninseln\",\"es\":\"Islas Caimán\",\"fr\":\"Îles Caïmans\",\"ja\":\"ケイマン諸島\",\"it\":\"Isole Cayman\",\"cn\":\"开曼群岛\"}', 19.50000000, -80.50000000, '🇰🇾', 'U+1F1F0 U+1F1FE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(42, 'Central African Republic', 'CAF', '140', 'CF', '236', 'Bangui', 'XAF', 'FCFA', '.cf', 'Ködörösêse tî Bêafrîka', 'Africa', 'Middle Africa', '[{\"zoneName\":\"Africa\\/Bangui\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WAT\",\"tzName\":\"West Africa Time\"}]', '{\"kr\":\"중앙아프리카 공화국\",\"br\":\"República Centro-Africana\",\"pt\":\"República Centro-Africana\",\"nl\":\"Centraal-Afrikaanse Republiek\",\"hr\":\"Srednjoafrička Republika\",\"fa\":\"جمهوری آفریقای مرکزی\",\"de\":\"Zentralafrikanische Republik\",\"es\":\"República Centroafricana\",\"fr\":\"République centrafricaine\",\"ja\":\"中央アフリカ共和国\",\"it\":\"Repubblica Centrafricana\",\"cn\":\"中非\"}', 7.00000000, 21.00000000, '🇨🇫', 'U+1F1E8 U+1F1EB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q929'),
(43, 'Chad', 'TCD', '148', 'TD', '235', 'N\'Djamena', 'XAF', 'FCFA', '.td', 'Tchad', 'Africa', 'Middle Africa', '[{\"zoneName\":\"Africa\\/Ndjamena\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WAT\",\"tzName\":\"West Africa Time\"}]', '{\"kr\":\"차드\",\"br\":\"Chade\",\"pt\":\"Chade\",\"nl\":\"Tsjaad\",\"hr\":\"Čad\",\"fa\":\"چاد\",\"de\":\"Tschad\",\"es\":\"Chad\",\"fr\":\"Tchad\",\"ja\":\"チャド\",\"it\":\"Ciad\",\"cn\":\"乍得\"}', 15.00000000, 19.00000000, '🇹🇩', 'U+1F1F9 U+1F1E9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q657'),
(44, 'Chile', 'CHL', '152', 'CL', '56', 'Santiago', 'CLP', '$', '.cl', 'Chile', 'Americas', 'South America', '[{\"zoneName\":\"America\\/Punta_Arenas\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"CLST\",\"tzName\":\"Chile Summer Time\"},{\"zoneName\":\"America\\/Santiago\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"CLST\",\"tzName\":\"Chile Summer Time\"},{\"zoneName\":\"Pacific\\/Easter\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EASST\",\"tzName\":\"Easter Island Summer Time\"}]', '{\"kr\":\"칠리\",\"br\":\"Chile\",\"pt\":\"Chile\",\"nl\":\"Chili\",\"hr\":\"Čile\",\"fa\":\"شیلی\",\"de\":\"Chile\",\"es\":\"Chile\",\"fr\":\"Chili\",\"ja\":\"チリ\",\"it\":\"Cile\",\"cn\":\"智利\"}', -30.00000000, -71.00000000, '🇨🇱', 'U+1F1E8 U+1F1F1', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q298'),
(45, 'China', 'CHN', '156', 'CN', '86', 'Beijing', 'CNY', '¥', '.cn', '中国', 'Asia', 'Eastern Asia', '[{\"zoneName\":\"Asia\\/Shanghai\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"CST\",\"tzName\":\"China Standard Time\"},{\"zoneName\":\"Asia\\/Urumqi\",\"gmtOffset\":21600,\"gmtOffsetName\":\"UTC+06:00\",\"abbreviation\":\"XJT\",\"tzName\":\"China Standard Time\"}]', '{\"kr\":\"중국\",\"br\":\"China\",\"pt\":\"China\",\"nl\":\"China\",\"hr\":\"Kina\",\"fa\":\"چین\",\"de\":\"China\",\"es\":\"China\",\"fr\":\"Chine\",\"ja\":\"中国\",\"it\":\"Cina\",\"cn\":\"中国\"}', 35.00000000, 105.00000000, '🇨🇳', 'U+1F1E8 U+1F1F3', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q148'),
(46, 'Christmas Island', 'CXR', '162', 'CX', '61', 'Flying Fish Cove', 'AUD', '$', '.cx', 'Christmas Island', 'Oceania', 'Australia and New Zealand', '[{\"zoneName\":\"Indian\\/Christmas\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"CXT\",\"tzName\":\"Christmas Island Time\"}]', '{\"kr\":\"크리스마스 섬\",\"br\":\"Ilha Christmas\",\"pt\":\"Ilha do Natal\",\"nl\":\"Christmaseiland\",\"hr\":\"Božićni otok\",\"fa\":\"جزیره کریسمس\",\"de\":\"Weihnachtsinsel\",\"es\":\"Isla de Navidad\",\"fr\":\"Île Christmas\",\"ja\":\"クリスマス島\",\"it\":\"Isola di Natale\",\"cn\":\"圣诞岛\"}', -10.50000000, 105.66666666, '🇨🇽', 'U+1F1E8 U+1F1FD', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(47, 'Cocos (Keeling) Islands', 'CCK', '166', 'CC', '61', 'West Island', 'AUD', '$', '.cc', 'Cocos (Keeling) Islands', 'Oceania', 'Australia and New Zealand', '[{\"zoneName\":\"Indian\\/Cocos\",\"gmtOffset\":23400,\"gmtOffsetName\":\"UTC+06:30\",\"abbreviation\":\"CCT\",\"tzName\":\"Cocos Islands Time\"}]', '{\"kr\":\"코코스 제도\",\"br\":\"Ilhas Cocos\",\"pt\":\"Ilhas dos Cocos\",\"nl\":\"Cocoseilanden\",\"hr\":\"Kokosovi Otoci\",\"fa\":\"جزایر کوکوس\",\"de\":\"Kokosinseln\",\"es\":\"Islas Cocos o Islas Keeling\",\"fr\":\"Îles Cocos\",\"ja\":\"ココス（キーリング）諸島\",\"it\":\"Isole Cocos e Keeling\",\"cn\":\"科科斯（基林）群岛\"}', -12.50000000, 96.83333333, '🇨🇨', 'U+1F1E8 U+1F1E8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(48, 'Colombia', 'COL', '170', 'CO', '57', 'Bogota', 'COP', '$', '.co', 'Colombia', 'Americas', 'South America', '[{\"zoneName\":\"America\\/Bogota\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"COT\",\"tzName\":\"Colombia Time\"}]', '{\"kr\":\"콜롬비아\",\"br\":\"Colômbia\",\"pt\":\"Colômbia\",\"nl\":\"Colombia\",\"hr\":\"Kolumbija\",\"fa\":\"کلمبیا\",\"de\":\"Kolumbien\",\"es\":\"Colombia\",\"fr\":\"Colombie\",\"ja\":\"コロンビア\",\"it\":\"Colombia\",\"cn\":\"哥伦比亚\"}', 4.00000000, -72.00000000, '🇨🇴', 'U+1F1E8 U+1F1F4', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q739'),
(49, 'Comoros', 'COM', '174', 'KM', '269', 'Moroni', 'KMF', 'CF', '.km', 'Komori', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Indian\\/Comoro\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"EAT\",\"tzName\":\"East Africa Time\"}]', '{\"kr\":\"코모로\",\"br\":\"Comores\",\"pt\":\"Comores\",\"nl\":\"Comoren\",\"hr\":\"Komori\",\"fa\":\"کومور\",\"de\":\"Union der Komoren\",\"es\":\"Comoras\",\"fr\":\"Comores\",\"ja\":\"コモロ\",\"it\":\"Comore\",\"cn\":\"科摩罗\"}', -12.16666666, 44.25000000, '🇰🇲', 'U+1F1F0 U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q970'),
(50, 'Congo', 'COG', '178', 'CG', '242', 'Brazzaville', 'XAF', 'FC', '.cg', 'République du Congo', 'Africa', 'Middle Africa', '[{\"zoneName\":\"Africa\\/Brazzaville\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WAT\",\"tzName\":\"West Africa Time\"}]', '{\"kr\":\"콩고\",\"br\":\"Congo\",\"pt\":\"Congo\",\"nl\":\"Congo [Republiek]\",\"hr\":\"Kongo\",\"fa\":\"کنگو\",\"de\":\"Kongo\",\"es\":\"Congo\",\"fr\":\"Congo\",\"ja\":\"コンゴ共和国\",\"it\":\"Congo\",\"cn\":\"刚果\"}', -1.00000000, 15.00000000, '🇨🇬', 'U+1F1E8 U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q971'),
(51, 'Democratic Republic of the Congo', 'COD', '180', 'CD', '243', 'Kinshasa', 'CDF', 'FC', '.cd', 'République démocratique du Congo', 'Africa', 'Middle Africa', '[{\"zoneName\":\"Africa\\/Kinshasa\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WAT\",\"tzName\":\"West Africa Time\"},{\"zoneName\":\"Africa\\/Lubumbashi\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"CAT\",\"tzName\":\"Central Africa Time\"}]', '{\"kr\":\"콩고 민주 공화국\",\"br\":\"RD Congo\",\"pt\":\"RD Congo\",\"nl\":\"Congo [DRC]\",\"hr\":\"Kongo, Demokratska Republika\",\"fa\":\"جمهوری کنگو\",\"de\":\"Kongo (Dem. Rep.)\",\"es\":\"Congo (Rep. Dem.)\",\"fr\":\"Congo (Rép. dém.)\",\"ja\":\"コンゴ民主共和国\",\"it\":\"Congo (Rep. Dem.)\",\"cn\":\"刚果（金）\"}', 0.00000000, 25.00000000, '🇨🇩', 'U+1F1E8 U+1F1E9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q974'),
(52, 'Cook Islands', 'COK', '184', 'CK', '682', 'Avarua', 'NZD', '$', '.ck', 'Cook Islands', 'Oceania', 'Polynesia', '[{\"zoneName\":\"Pacific\\/Rarotonga\",\"gmtOffset\":-36000,\"gmtOffsetName\":\"UTC-10:00\",\"abbreviation\":\"CKT\",\"tzName\":\"Cook Island Time\"}]', '{\"kr\":\"쿡 제도\",\"br\":\"Ilhas Cook\",\"pt\":\"Ilhas Cook\",\"nl\":\"Cookeilanden\",\"hr\":\"Cookovo Otočje\",\"fa\":\"جزایر کوک\",\"de\":\"Cookinseln\",\"es\":\"Islas Cook\",\"fr\":\"Îles Cook\",\"ja\":\"クック諸島\",\"it\":\"Isole Cook\",\"cn\":\"库克群岛\"}', -21.23333333, -159.76666666, '🇨🇰', 'U+1F1E8 U+1F1F0', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q26988'),
(53, 'Costa Rica', 'CRI', '188', 'CR', '506', 'San Jose', 'CRC', '₡', '.cr', 'Costa Rica', 'Americas', 'Central America', '[{\"zoneName\":\"America\\/Costa_Rica\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"}]', '{\"kr\":\"코스타리카\",\"br\":\"Costa Rica\",\"pt\":\"Costa Rica\",\"nl\":\"Costa Rica\",\"hr\":\"Kostarika\",\"fa\":\"کاستاریکا\",\"de\":\"Costa Rica\",\"es\":\"Costa Rica\",\"fr\":\"Costa Rica\",\"ja\":\"コスタリカ\",\"it\":\"Costa Rica\",\"cn\":\"哥斯达黎加\"}', 10.00000000, -84.00000000, '🇨🇷', 'U+1F1E8 U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q800'),
(54, 'Cote D\'Ivoire (Ivory Coast)', 'CIV', '384', 'CI', '225', 'Yamoussoukro', 'XOF', 'CFA', '.ci', NULL, 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Abidjan\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"코트디부아르\",\"br\":\"Costa do Marfim\",\"pt\":\"Costa do Marfim\",\"nl\":\"Ivoorkust\",\"hr\":\"Obala Bjelokosti\",\"fa\":\"ساحل عاج\",\"de\":\"Elfenbeinküste\",\"es\":\"Costa de Marfil\",\"fr\":\"Côte d\'Ivoire\",\"ja\":\"コートジボワール\",\"it\":\"Costa D\'Avorio\",\"cn\":\"科特迪瓦\"}', 8.00000000, -5.00000000, '🇨🇮', 'U+1F1E8 U+1F1EE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1008'),
(55, 'Croatia', 'HRV', '191', 'HR', '385', 'Zagreb', 'HRK', 'kn', '.hr', 'Hrvatska', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Zagreb\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"크로아티아\",\"br\":\"Croácia\",\"pt\":\"Croácia\",\"nl\":\"Kroatië\",\"hr\":\"Hrvatska\",\"fa\":\"کرواسی\",\"de\":\"Kroatien\",\"es\":\"Croacia\",\"fr\":\"Croatie\",\"ja\":\"クロアチア\",\"it\":\"Croazia\",\"cn\":\"克罗地亚\"}', 45.16666666, 15.50000000, '🇭🇷', 'U+1F1ED U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q224'),
(56, 'Cuba', 'CUB', '192', 'CU', '53', 'Havana', 'CUP', '$', '.cu', 'Cuba', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Havana\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"CST\",\"tzName\":\"Cuba Standard Time\"}]', '{\"kr\":\"쿠바\",\"br\":\"Cuba\",\"pt\":\"Cuba\",\"nl\":\"Cuba\",\"hr\":\"Kuba\",\"fa\":\"کوبا\",\"de\":\"Kuba\",\"es\":\"Cuba\",\"fr\":\"Cuba\",\"ja\":\"キューバ\",\"it\":\"Cuba\",\"cn\":\"古巴\"}', 21.50000000, -80.00000000, '🇨🇺', 'U+1F1E8 U+1F1FA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q241'),
(57, 'Cyprus', 'CYP', '196', 'CY', '357', 'Nicosia', 'EUR', '€', '.cy', 'Κύπρος', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Asia\\/Famagusta\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"},{\"zoneName\":\"Asia\\/Nicosia\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"키프로스\",\"br\":\"Chipre\",\"pt\":\"Chipre\",\"nl\":\"Cyprus\",\"hr\":\"Cipar\",\"fa\":\"قبرس\",\"de\":\"Zypern\",\"es\":\"Chipre\",\"fr\":\"Chypre\",\"ja\":\"キプロス\",\"it\":\"Cipro\",\"cn\":\"塞浦路斯\"}', 35.00000000, 33.00000000, '🇨🇾', 'U+1F1E8 U+1F1FE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q229'),
(58, 'Czech Republic', 'CZE', '203', 'CZ', '420', 'Prague', 'CZK', 'Kč', '.cz', 'Česká republika', 'Europe', 'Eastern Europe', '[{\"zoneName\":\"Europe\\/Prague\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"체코\",\"br\":\"República Tcheca\",\"pt\":\"República Checa\",\"nl\":\"Tsjechië\",\"hr\":\"Češka\",\"fa\":\"جمهوری چک\",\"de\":\"Tschechische Republik\",\"es\":\"República Checa\",\"fr\":\"République tchèque\",\"ja\":\"チェコ\",\"it\":\"Repubblica Ceca\",\"cn\":\"捷克\"}', 49.75000000, 15.50000000, '🇨🇿', 'U+1F1E8 U+1F1FF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q213');
INSERT INTO `country` (`id`, `country`, `iso3`, `numeric_code`, `country_code`, `phone_code`, `capital`, `currency_code`, `currency_symbol`, `tld`, `native`, `region`, `subregion`, `timezones`, `translations`, `latitude`, `longitude`, `emoji`, `emojiU`, `created_at`, `updated_at`, `flag`, `wikiDataId`) VALUES
(59, 'Denmark', 'DNK', '208', 'DK', '45', 'Copenhagen', 'DKK', 'Kr.', '.dk', 'Danmark', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/Copenhagen\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"덴마크\",\"br\":\"Dinamarca\",\"pt\":\"Dinamarca\",\"nl\":\"Denemarken\",\"hr\":\"Danska\",\"fa\":\"دانمارک\",\"de\":\"Dänemark\",\"es\":\"Dinamarca\",\"fr\":\"Danemark\",\"ja\":\"デンマーク\",\"it\":\"Danimarca\",\"cn\":\"丹麦\"}', 56.00000000, 10.00000000, '🇩🇰', 'U+1F1E9 U+1F1F0', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q35'),
(60, 'Djibouti', 'DJI', '262', 'DJ', '253', 'Djibouti', 'DJF', 'Fdj', '.dj', 'Djibouti', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Djibouti\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"EAT\",\"tzName\":\"East Africa Time\"}]', '{\"kr\":\"지부티\",\"br\":\"Djibuti\",\"pt\":\"Djibuti\",\"nl\":\"Djibouti\",\"hr\":\"Džibuti\",\"fa\":\"جیبوتی\",\"de\":\"Dschibuti\",\"es\":\"Yibuti\",\"fr\":\"Djibouti\",\"ja\":\"ジブチ\",\"it\":\"Gibuti\",\"cn\":\"吉布提\"}', 11.50000000, 43.00000000, '🇩🇯', 'U+1F1E9 U+1F1EF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q977'),
(61, 'Dominica', 'DMA', '212', 'DM', '+1-767', 'Roseau', 'XCD', '$', '.dm', 'Dominica', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Dominica\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"도미니카 연방\",\"br\":\"Dominica\",\"pt\":\"Dominica\",\"nl\":\"Dominica\",\"hr\":\"Dominika\",\"fa\":\"دومینیکا\",\"de\":\"Dominica\",\"es\":\"Dominica\",\"fr\":\"Dominique\",\"ja\":\"ドミニカ国\",\"it\":\"Dominica\",\"cn\":\"多米尼加\"}', 15.41666666, -61.33333333, '🇩🇲', 'U+1F1E9 U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q784'),
(62, 'Dominican Republic', 'DOM', '214', 'DO', '+1-809 and 1-829', 'Santo Domingo', 'DOP', '$', '.do', 'República Dominicana', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Santo_Domingo\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"도미니카 공화국\",\"br\":\"República Dominicana\",\"pt\":\"República Dominicana\",\"nl\":\"Dominicaanse Republiek\",\"hr\":\"Dominikanska Republika\",\"fa\":\"جمهوری دومینیکن\",\"de\":\"Dominikanische Republik\",\"es\":\"República Dominicana\",\"fr\":\"République dominicaine\",\"ja\":\"ドミニカ共和国\",\"it\":\"Repubblica Dominicana\",\"cn\":\"多明尼加共和国\"}', 19.00000000, -70.66666666, '🇩🇴', 'U+1F1E9 U+1F1F4', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q786'),
(63, 'East Timor', 'TLS', '626', 'TL', '670', 'Dili', 'USD', '$', '.tl', 'Timor-Leste', 'Asia', 'South-Eastern Asia', '[{\"zoneName\":\"Asia\\/Dili\",\"gmtOffset\":32400,\"gmtOffsetName\":\"UTC+09:00\",\"abbreviation\":\"TLT\",\"tzName\":\"Timor Leste Time\"}]', '{\"kr\":\"동티모르\",\"br\":\"Timor Leste\",\"pt\":\"Timor Leste\",\"nl\":\"Oost-Timor\",\"hr\":\"Istočni Timor\",\"fa\":\"تیمور شرقی\",\"de\":\"Timor-Leste\",\"es\":\"Timor Oriental\",\"fr\":\"Timor oriental\",\"ja\":\"東ティモール\",\"it\":\"Timor Est\",\"cn\":\"东帝汶\"}', -8.83333333, 125.91666666, '🇹🇱', 'U+1F1F9 U+1F1F1', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q574'),
(64, 'Ecuador', 'ECU', '218', 'EC', '593', 'Quito', 'USD', '$', '.ec', 'Ecuador', 'Americas', 'South America', '[{\"zoneName\":\"America\\/Guayaquil\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"ECT\",\"tzName\":\"Ecuador Time\"},{\"zoneName\":\"Pacific\\/Galapagos\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"GALT\",\"tzName\":\"Gal\\u00e1pagos Time\"}]', '{\"kr\":\"에콰도르\",\"br\":\"Equador\",\"pt\":\"Equador\",\"nl\":\"Ecuador\",\"hr\":\"Ekvador\",\"fa\":\"اکوادور\",\"de\":\"Ecuador\",\"es\":\"Ecuador\",\"fr\":\"Équateur\",\"ja\":\"エクアドル\",\"it\":\"Ecuador\",\"cn\":\"厄瓜多尔\"}', -2.00000000, -77.50000000, '🇪🇨', 'U+1F1EA U+1F1E8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q736'),
(65, 'Egypt', 'EGY', '818', 'EG', '20', 'Cairo', 'EGP', 'ج.م', '.eg', 'مصر‎', 'Africa', 'Northern Africa', '[{\"zoneName\":\"Africa\\/Cairo\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"이집트\",\"br\":\"Egito\",\"pt\":\"Egipto\",\"nl\":\"Egypte\",\"hr\":\"Egipat\",\"fa\":\"مصر\",\"de\":\"Ägypten\",\"es\":\"Egipto\",\"fr\":\"Égypte\",\"ja\":\"エジプト\",\"it\":\"Egitto\",\"cn\":\"埃及\"}', 27.00000000, 30.00000000, '🇪🇬', 'U+1F1EA U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q79'),
(66, 'El Salvador', 'SLV', '222', 'SV', '503', 'San Salvador', 'USD', '$', '.sv', 'El Salvador', 'Americas', 'Central America', '[{\"zoneName\":\"America\\/El_Salvador\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"}]', '{\"kr\":\"엘살바도르\",\"br\":\"El Salvador\",\"pt\":\"El Salvador\",\"nl\":\"El Salvador\",\"hr\":\"Salvador\",\"fa\":\"السالوادور\",\"de\":\"El Salvador\",\"es\":\"El Salvador\",\"fr\":\"Salvador\",\"ja\":\"エルサルバドル\",\"it\":\"El Salvador\",\"cn\":\"萨尔瓦多\"}', 13.83333333, -88.91666666, '🇸🇻', 'U+1F1F8 U+1F1FB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q792'),
(67, 'Equatorial Guinea', 'GNQ', '226', 'GQ', '240', 'Malabo', 'XAF', 'FCFA', '.gq', 'Guinea Ecuatorial', 'Africa', 'Middle Africa', '[{\"zoneName\":\"Africa\\/Malabo\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WAT\",\"tzName\":\"West Africa Time\"}]', '{\"kr\":\"적도 기니\",\"br\":\"Guiné Equatorial\",\"pt\":\"Guiné Equatorial\",\"nl\":\"Equatoriaal-Guinea\",\"hr\":\"Ekvatorijalna Gvineja\",\"fa\":\"گینه استوایی\",\"de\":\"Äquatorial-Guinea\",\"es\":\"Guinea Ecuatorial\",\"fr\":\"Guinée-Équatoriale\",\"ja\":\"赤道ギニア\",\"it\":\"Guinea Equatoriale\",\"cn\":\"赤道几内亚\"}', 2.00000000, 10.00000000, '🇬🇶', 'U+1F1EC U+1F1F6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q983'),
(68, 'Eritrea', 'ERI', '232', 'ER', '291', 'Asmara', 'ERN', 'Nfk', '.er', 'ኤርትራ', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Asmara\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"EAT\",\"tzName\":\"East Africa Time\"}]', '{\"kr\":\"에리트레아\",\"br\":\"Eritreia\",\"pt\":\"Eritreia\",\"nl\":\"Eritrea\",\"hr\":\"Eritreja\",\"fa\":\"اریتره\",\"de\":\"Eritrea\",\"es\":\"Eritrea\",\"fr\":\"Érythrée\",\"ja\":\"エリトリア\",\"it\":\"Eritrea\",\"cn\":\"厄立特里亚\"}', 15.00000000, 39.00000000, '🇪🇷', 'U+1F1EA U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q986'),
(69, 'Estonia', 'EST', '233', 'EE', '372', 'Tallinn', 'EUR', '€', '.ee', 'Eesti', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/Tallinn\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"에스토니아\",\"br\":\"Estônia\",\"pt\":\"Estónia\",\"nl\":\"Estland\",\"hr\":\"Estonija\",\"fa\":\"استونی\",\"de\":\"Estland\",\"es\":\"Estonia\",\"fr\":\"Estonie\",\"ja\":\"エストニア\",\"it\":\"Estonia\",\"cn\":\"爱沙尼亚\"}', 59.00000000, 26.00000000, '🇪🇪', 'U+1F1EA U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q191'),
(70, 'Ethiopia', 'ETH', '231', 'ET', '251', 'Addis Ababa', 'ETB', 'Nkf', '.et', 'ኢትዮጵያ', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Addis_Ababa\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"EAT\",\"tzName\":\"East Africa Time\"}]', '{\"kr\":\"에티오피아\",\"br\":\"Etiópia\",\"pt\":\"Etiópia\",\"nl\":\"Ethiopië\",\"hr\":\"Etiopija\",\"fa\":\"اتیوپی\",\"de\":\"Äthiopien\",\"es\":\"Etiopía\",\"fr\":\"Éthiopie\",\"ja\":\"エチオピア\",\"it\":\"Etiopia\",\"cn\":\"埃塞俄比亚\"}', 8.00000000, 38.00000000, '🇪🇹', 'U+1F1EA U+1F1F9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q115'),
(71, 'Falkland Islands', 'FLK', '238', 'FK', '500', 'Stanley', 'FKP', '£', '.fk', 'Falkland Islands', 'Americas', 'South America', '[{\"zoneName\":\"Atlantic\\/Stanley\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"FKST\",\"tzName\":\"Falkland Islands Summer Time\"}]', '{\"kr\":\"포클랜드 제도\",\"br\":\"Ilhas Malvinas\",\"pt\":\"Ilhas Falkland\",\"nl\":\"Falklandeilanden [Islas Malvinas]\",\"hr\":\"Falklandski Otoci\",\"fa\":\"جزایر فالکلند\",\"de\":\"Falklandinseln\",\"es\":\"Islas Malvinas\",\"fr\":\"Îles Malouines\",\"ja\":\"フォークランド（マルビナス）諸島\",\"it\":\"Isole Falkland o Isole Malvine\",\"cn\":\"福克兰群岛\"}', -51.75000000, -59.00000000, '🇫🇰', 'U+1F1EB U+1F1F0', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(72, 'Faroe Islands', 'FRO', '234', 'FO', '298', 'Torshavn', 'DKK', 'Kr.', '.fo', 'Føroyar', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Atlantic\\/Faroe\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"WET\",\"tzName\":\"Western European Time\"}]', '{\"kr\":\"페로 제도\",\"br\":\"Ilhas Faroé\",\"pt\":\"Ilhas Faroé\",\"nl\":\"Faeröer\",\"hr\":\"Farski Otoci\",\"fa\":\"جزایر فارو\",\"de\":\"Färöer-Inseln\",\"es\":\"Islas Faroe\",\"fr\":\"Îles Féroé\",\"ja\":\"フェロー諸島\",\"it\":\"Isole Far Oer\",\"cn\":\"法罗群岛\"}', 62.00000000, -7.00000000, '🇫🇴', 'U+1F1EB U+1F1F4', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(73, 'Fiji Islands', 'FJI', '242', 'FJ', '679', 'Suva', 'FJD', 'FJ$', '.fj', 'Fiji', 'Oceania', 'Melanesia', '[{\"zoneName\":\"Pacific\\/Fiji\",\"gmtOffset\":43200,\"gmtOffsetName\":\"UTC+12:00\",\"abbreviation\":\"FJT\",\"tzName\":\"Fiji Time\"}]', '{\"kr\":\"피지\",\"br\":\"Fiji\",\"pt\":\"Fiji\",\"nl\":\"Fiji\",\"hr\":\"Fiđi\",\"fa\":\"فیجی\",\"de\":\"Fidschi\",\"es\":\"Fiyi\",\"fr\":\"Fidji\",\"ja\":\"フィジー\",\"it\":\"Figi\",\"cn\":\"斐济\"}', -18.00000000, 175.00000000, '🇫🇯', 'U+1F1EB U+1F1EF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q712'),
(74, 'Finland', 'FIN', '246', 'FI', '358', 'Helsinki', 'EUR', '€', '.fi', 'Suomi', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/Helsinki\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"핀란드\",\"br\":\"Finlândia\",\"pt\":\"Finlândia\",\"nl\":\"Finland\",\"hr\":\"Finska\",\"fa\":\"فنلاند\",\"de\":\"Finnland\",\"es\":\"Finlandia\",\"fr\":\"Finlande\",\"ja\":\"フィンランド\",\"it\":\"Finlandia\",\"cn\":\"芬兰\"}', 64.00000000, 26.00000000, '🇫🇮', 'U+1F1EB U+1F1EE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q33'),
(75, 'France', 'FRA', '250', 'FR', '33', 'Paris', 'EUR', '€', '.fr', 'France', 'Europe', 'Western Europe', '[{\"zoneName\":\"Europe\\/Paris\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"프랑스\",\"br\":\"França\",\"pt\":\"França\",\"nl\":\"Frankrijk\",\"hr\":\"Francuska\",\"fa\":\"فرانسه\",\"de\":\"Frankreich\",\"es\":\"Francia\",\"fr\":\"France\",\"ja\":\"フランス\",\"it\":\"Francia\",\"cn\":\"法国\"}', 46.00000000, 2.00000000, '🇫🇷', 'U+1F1EB U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q142'),
(76, 'French Guiana', 'GUF', '254', 'GF', '594', 'Cayenne', 'EUR', '€', '.gf', 'Guyane française', 'Americas', 'South America', '[{\"zoneName\":\"America\\/Cayenne\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"GFT\",\"tzName\":\"French Guiana Time\"}]', '{\"kr\":\"프랑스령 기아나\",\"br\":\"Guiana Francesa\",\"pt\":\"Guiana Francesa\",\"nl\":\"Frans-Guyana\",\"hr\":\"Francuska Gvajana\",\"fa\":\"گویان فرانسه\",\"de\":\"Französisch Guyana\",\"es\":\"Guayana Francesa\",\"fr\":\"Guayane\",\"ja\":\"フランス領ギアナ\",\"it\":\"Guyana francese\",\"cn\":\"法属圭亚那\"}', 4.00000000, -53.00000000, '🇬🇫', 'U+1F1EC U+1F1EB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(77, 'French Polynesia', 'PYF', '258', 'PF', '689', 'Papeete', 'XPF', '₣', '.pf', 'Polynésie française', 'Oceania', 'Polynesia', '[{\"zoneName\":\"Pacific\\/Gambier\",\"gmtOffset\":-32400,\"gmtOffsetName\":\"UTC-09:00\",\"abbreviation\":\"GAMT\",\"tzName\":\"Gambier Islands Time\"},{\"zoneName\":\"Pacific\\/Marquesas\",\"gmtOffset\":-34200,\"gmtOffsetName\":\"UTC-09:30\",\"abbreviation\":\"MART\",\"tzName\":\"Marquesas Islands Time\"},{\"zoneName\":\"Pacific\\/Tahiti\",\"gmtOffset\":-36000,\"gmtOffsetName\":\"UTC-10:00\",\"abbreviation\":\"TAHT\",\"tzName\":\"Tahiti Time\"}]', '{\"kr\":\"프랑스령 폴리네시아\",\"br\":\"Polinésia Francesa\",\"pt\":\"Polinésia Francesa\",\"nl\":\"Frans-Polynesië\",\"hr\":\"Francuska Polinezija\",\"fa\":\"پلی‌نزی فرانسه\",\"de\":\"Französisch-Polynesien\",\"es\":\"Polinesia Francesa\",\"fr\":\"Polynésie française\",\"ja\":\"フランス領ポリネシア\",\"it\":\"Polinesia Francese\",\"cn\":\"法属波利尼西亚\"}', -15.00000000, -140.00000000, '🇵🇫', 'U+1F1F5 U+1F1EB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(78, 'French Southern Territories', 'ATF', '260', 'TF', '262', 'Port-aux-Francais', 'EUR', '€', '.tf', 'Territoire des Terres australes et antarctiques fr', 'Africa', 'Southern Africa', '[{\"zoneName\":\"Indian\\/Kerguelen\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"TFT\",\"tzName\":\"French Southern and Antarctic Time\"}]', '{\"kr\":\"프랑스령 남방 및 남극\",\"br\":\"Terras Austrais e Antárticas Francesas\",\"pt\":\"Terras Austrais e Antárticas Francesas\",\"nl\":\"Franse Gebieden in de zuidelijke Indische Oceaan\",\"hr\":\"Francuski južni i antarktički teritoriji\",\"fa\":\"سرزمین‌های جنوبی و جنوبگانی فرانسه\",\"de\":\"Französische Süd- und Antarktisgebiete\",\"es\":\"Tierras Australes y Antárticas Francesas\",\"fr\":\"Terres australes et antarctiques françaises\",\"ja\":\"フランス領南方・南極地域\",\"it\":\"Territori Francesi del Sud\",\"cn\":\"法属南部领地\"}', -49.25000000, 69.16700000, '🇹🇫', 'U+1F1F9 U+1F1EB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(79, 'Gabon', 'GAB', '266', 'GA', '241', 'Libreville', 'XAF', 'FCFA', '.ga', 'Gabon', 'Africa', 'Middle Africa', '[{\"zoneName\":\"Africa\\/Libreville\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WAT\",\"tzName\":\"West Africa Time\"}]', '{\"kr\":\"가봉\",\"br\":\"Gabão\",\"pt\":\"Gabão\",\"nl\":\"Gabon\",\"hr\":\"Gabon\",\"fa\":\"گابن\",\"de\":\"Gabun\",\"es\":\"Gabón\",\"fr\":\"Gabon\",\"ja\":\"ガボン\",\"it\":\"Gabon\",\"cn\":\"加蓬\"}', -1.00000000, 11.75000000, '🇬🇦', 'U+1F1EC U+1F1E6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1000'),
(80, 'Gambia The', 'GMB', '270', 'GM', '220', 'Banjul', 'GMD', 'D', '.gm', 'Gambia', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Banjul\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"감비아\",\"br\":\"Gâmbia\",\"pt\":\"Gâmbia\",\"nl\":\"Gambia\",\"hr\":\"Gambija\",\"fa\":\"گامبیا\",\"de\":\"Gambia\",\"es\":\"Gambia\",\"fr\":\"Gambie\",\"ja\":\"ガンビア\",\"it\":\"Gambia\",\"cn\":\"冈比亚\"}', 13.46666666, -16.56666666, '🇬🇲', 'U+1F1EC U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1005'),
(81, 'Georgia', 'GEO', '268', 'GE', '995', 'Tbilisi', 'GEL', 'ლ', '.ge', 'საქართველო', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Tbilisi\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"GET\",\"tzName\":\"Georgia Standard Time\"}]', '{\"kr\":\"조지아\",\"br\":\"Geórgia\",\"pt\":\"Geórgia\",\"nl\":\"Georgië\",\"hr\":\"Gruzija\",\"fa\":\"گرجستان\",\"de\":\"Georgien\",\"es\":\"Georgia\",\"fr\":\"Géorgie\",\"ja\":\"グルジア\",\"it\":\"Georgia\",\"cn\":\"格鲁吉亚\"}', 42.00000000, 43.50000000, '🇬🇪', 'U+1F1EC U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q230'),
(82, 'Germany', 'DEU', '276', 'DE', '49', 'Berlin', 'EUR', '€', '.de', 'Deutschland', 'Europe', 'Western Europe', '[{\"zoneName\":\"Europe\\/Berlin\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"},{\"zoneName\":\"Europe\\/Busingen\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"독일\",\"br\":\"Alemanha\",\"pt\":\"Alemanha\",\"nl\":\"Duitsland\",\"hr\":\"Njemačka\",\"fa\":\"آلمان\",\"de\":\"Deutschland\",\"es\":\"Alemania\",\"fr\":\"Allemagne\",\"ja\":\"ドイツ\",\"it\":\"Germania\",\"cn\":\"德国\"}', 51.00000000, 9.00000000, '🇩🇪', 'U+1F1E9 U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q183'),
(83, 'Ghana', 'GHA', '288', 'GH', '233', 'Accra', 'GHS', 'GH₵', '.gh', 'Ghana', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Accra\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"가나\",\"br\":\"Gana\",\"pt\":\"Gana\",\"nl\":\"Ghana\",\"hr\":\"Gana\",\"fa\":\"غنا\",\"de\":\"Ghana\",\"es\":\"Ghana\",\"fr\":\"Ghana\",\"ja\":\"ガーナ\",\"it\":\"Ghana\",\"cn\":\"加纳\"}', 8.00000000, -2.00000000, '🇬🇭', 'U+1F1EC U+1F1ED', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q117'),
(84, 'Gibraltar', 'GIB', '292', 'GI', '350', 'Gibraltar', 'GIP', '£', '.gi', 'Gibraltar', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Gibraltar\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"지브롤터\",\"br\":\"Gibraltar\",\"pt\":\"Gibraltar\",\"nl\":\"Gibraltar\",\"hr\":\"Gibraltar\",\"fa\":\"جبل‌طارق\",\"de\":\"Gibraltar\",\"es\":\"Gibraltar\",\"fr\":\"Gibraltar\",\"ja\":\"ジブラルタル\",\"it\":\"Gibilterra\",\"cn\":\"直布罗陀\"}', 36.13333333, -5.35000000, '🇬🇮', 'U+1F1EC U+1F1EE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(85, 'Greece', 'GRC', '300', 'GR', '30', 'Athens', 'EUR', '€', '.gr', 'Ελλάδα', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Athens\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"그리스\",\"br\":\"Grécia\",\"pt\":\"Grécia\",\"nl\":\"Griekenland\",\"hr\":\"Grčka\",\"fa\":\"یونان\",\"de\":\"Griechenland\",\"es\":\"Grecia\",\"fr\":\"Grèce\",\"ja\":\"ギリシャ\",\"it\":\"Grecia\",\"cn\":\"希腊\"}', 39.00000000, 22.00000000, '🇬🇷', 'U+1F1EC U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q41'),
(86, 'Greenland', 'GRL', '304', 'GL', '299', 'Nuuk', 'DKK', 'Kr.', '.gl', 'Kalaallit Nunaat', 'Americas', 'Northern America', '[{\"zoneName\":\"America\\/Danmarkshavn\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"},{\"zoneName\":\"America\\/Nuuk\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"WGT\",\"tzName\":\"West Greenland Time\"},{\"zoneName\":\"America\\/Scoresbysund\",\"gmtOffset\":-3600,\"gmtOffsetName\":\"UTC-01:00\",\"abbreviation\":\"EGT\",\"tzName\":\"Eastern Greenland Time\"},{\"zoneName\":\"America\\/Thule\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"그린란드\",\"br\":\"Groelândia\",\"pt\":\"Gronelândia\",\"nl\":\"Groenland\",\"hr\":\"Grenland\",\"fa\":\"گرینلند\",\"de\":\"Grönland\",\"es\":\"Groenlandia\",\"fr\":\"Groenland\",\"ja\":\"グリーンランド\",\"it\":\"Groenlandia\",\"cn\":\"格陵兰岛\"}', 72.00000000, -40.00000000, '🇬🇱', 'U+1F1EC U+1F1F1', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(87, 'Grenada', 'GRD', '308', 'GD', '+1-473', 'St. George\'s', 'XCD', '$', '.gd', 'Grenada', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Grenada\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"그레나다\",\"br\":\"Granada\",\"pt\":\"Granada\",\"nl\":\"Grenada\",\"hr\":\"Grenada\",\"fa\":\"گرنادا\",\"de\":\"Grenada\",\"es\":\"Grenada\",\"fr\":\"Grenade\",\"ja\":\"グレナダ\",\"it\":\"Grenada\",\"cn\":\"格林纳达\"}', 12.11666666, -61.66666666, '🇬🇩', 'U+1F1EC U+1F1E9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q769'),
(88, 'Guadeloupe', 'GLP', '312', 'GP', '590', 'Basse-Terre', 'EUR', '€', '.gp', 'Guadeloupe', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Guadeloupe\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"과들루프\",\"br\":\"Guadalupe\",\"pt\":\"Guadalupe\",\"nl\":\"Guadeloupe\",\"hr\":\"Gvadalupa\",\"fa\":\"جزیره گوادلوپ\",\"de\":\"Guadeloupe\",\"es\":\"Guadalupe\",\"fr\":\"Guadeloupe\",\"ja\":\"グアドループ\",\"it\":\"Guadeloupa\",\"cn\":\"瓜德罗普岛\"}', 16.25000000, -61.58333300, '🇬🇵', 'U+1F1EC U+1F1F5', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(89, 'Guam', 'GUM', '316', 'GU', '+1-671', 'Hagatna', 'USD', '$', '.gu', 'Guam', 'Oceania', 'Micronesia', '[{\"zoneName\":\"Pacific\\/Guam\",\"gmtOffset\":36000,\"gmtOffsetName\":\"UTC+10:00\",\"abbreviation\":\"CHST\",\"tzName\":\"Chamorro Standard Time\"}]', '{\"kr\":\"괌\",\"br\":\"Guam\",\"pt\":\"Guame\",\"nl\":\"Guam\",\"hr\":\"Guam\",\"fa\":\"گوام\",\"de\":\"Guam\",\"es\":\"Guam\",\"fr\":\"Guam\",\"ja\":\"グアム\",\"it\":\"Guam\",\"cn\":\"关岛\"}', 13.46666666, 144.78333333, '🇬🇺', 'U+1F1EC U+1F1FA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(90, 'Guatemala', 'GTM', '320', 'GT', '502', 'Guatemala City', 'GTQ', 'Q', '.gt', 'Guatemala', 'Americas', 'Central America', '[{\"zoneName\":\"America\\/Guatemala\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"}]', '{\"kr\":\"과테말라\",\"br\":\"Guatemala\",\"pt\":\"Guatemala\",\"nl\":\"Guatemala\",\"hr\":\"Gvatemala\",\"fa\":\"گواتمالا\",\"de\":\"Guatemala\",\"es\":\"Guatemala\",\"fr\":\"Guatemala\",\"ja\":\"グアテマラ\",\"it\":\"Guatemala\",\"cn\":\"危地马拉\"}', 15.50000000, -90.25000000, '🇬🇹', 'U+1F1EC U+1F1F9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q774'),
(91, 'Guernsey and Alderney', 'GGY', '831', 'GG', '+44-1481', 'St Peter Port', 'GBP', '£', '.gg', 'Guernsey', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/Guernsey\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"건지, 올더니\",\"br\":\"Guernsey\",\"pt\":\"Guernsey\",\"nl\":\"Guernsey\",\"hr\":\"Guernsey\",\"fa\":\"گرنزی\",\"de\":\"Guernsey\",\"es\":\"Guernsey\",\"fr\":\"Guernesey\",\"ja\":\"ガーンジー\",\"it\":\"Guernsey\",\"cn\":\"根西岛\"}', 49.46666666, -2.58333333, '🇬🇬', 'U+1F1EC U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(92, 'Guinea', 'GIN', '324', 'GN', '224', 'Conakry', 'GNF', 'FG', '.gn', 'Guinée', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Conakry\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"기니\",\"br\":\"Guiné\",\"pt\":\"Guiné\",\"nl\":\"Guinee\",\"hr\":\"Gvineja\",\"fa\":\"گینه\",\"de\":\"Guinea\",\"es\":\"Guinea\",\"fr\":\"Guinée\",\"ja\":\"ギニア\",\"it\":\"Guinea\",\"cn\":\"几内亚\"}', 11.00000000, -10.00000000, '🇬🇳', 'U+1F1EC U+1F1F3', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1006'),
(93, 'Guinea-Bissau', 'GNB', '624', 'GW', '245', 'Bissau', 'XOF', 'CFA', '.gw', 'Guiné-Bissau', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Bissau\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"기니비사우\",\"br\":\"Guiné-Bissau\",\"pt\":\"Guiné-Bissau\",\"nl\":\"Guinee-Bissau\",\"hr\":\"Gvineja Bisau\",\"fa\":\"گینه بیسائو\",\"de\":\"Guinea-Bissau\",\"es\":\"Guinea-Bisáu\",\"fr\":\"Guinée-Bissau\",\"ja\":\"ギニアビサウ\",\"it\":\"Guinea-Bissau\",\"cn\":\"几内亚比绍\"}', 12.00000000, -15.00000000, '🇬🇼', 'U+1F1EC U+1F1FC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1007'),
(94, 'Guyana', 'GUY', '328', 'GY', '592', 'Georgetown', 'GYD', '$', '.gy', 'Guyana', 'Americas', 'South America', '[{\"zoneName\":\"America\\/Guyana\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"GYT\",\"tzName\":\"Guyana Time\"}]', '{\"kr\":\"가이아나\",\"br\":\"Guiana\",\"pt\":\"Guiana\",\"nl\":\"Guyana\",\"hr\":\"Gvajana\",\"fa\":\"گویان\",\"de\":\"Guyana\",\"es\":\"Guyana\",\"fr\":\"Guyane\",\"ja\":\"ガイアナ\",\"it\":\"Guyana\",\"cn\":\"圭亚那\"}', 5.00000000, -59.00000000, '🇬🇾', 'U+1F1EC U+1F1FE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q734'),
(95, 'Haiti', 'HTI', '332', 'HT', '509', 'Port-au-Prince', 'HTG', 'G', '.ht', 'Haïti', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Port-au-Prince\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"}]', '{\"kr\":\"아이티\",\"br\":\"Haiti\",\"pt\":\"Haiti\",\"nl\":\"Haïti\",\"hr\":\"Haiti\",\"fa\":\"هائیتی\",\"de\":\"Haiti\",\"es\":\"Haiti\",\"fr\":\"Haïti\",\"ja\":\"ハイチ\",\"it\":\"Haiti\",\"cn\":\"海地\"}', 19.00000000, -72.41666666, '🇭🇹', 'U+1F1ED U+1F1F9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q790'),
(96, 'Heard Island and McDonald Islands', 'HMD', '334', 'HM', '672', '', 'AUD', '$', '.hm', 'Heard Island and McDonald Islands', '', '', '[{\"zoneName\":\"Indian\\/Kerguelen\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"TFT\",\"tzName\":\"French Southern and Antarctic Time\"}]', '{\"kr\":\"허드 맥도날드 제도\",\"br\":\"Ilha Heard e Ilhas McDonald\",\"pt\":\"Ilha Heard e Ilhas McDonald\",\"nl\":\"Heard- en McDonaldeilanden\",\"hr\":\"Otok Heard i otočje McDonald\",\"fa\":\"جزیره هرد و جزایر مک‌دونالد\",\"de\":\"Heard und die McDonaldinseln\",\"es\":\"Islas Heard y McDonald\",\"fr\":\"Îles Heard-et-MacDonald\",\"ja\":\"ハード島とマクドナルド諸島\",\"it\":\"Isole Heard e McDonald\",\"cn\":\"赫德·唐纳岛及麦唐纳岛\"}', -53.10000000, 72.51666666, '🇭🇲', 'U+1F1ED U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(97, 'Honduras', 'HND', '340', 'HN', '504', 'Tegucigalpa', 'HNL', 'L', '.hn', 'Honduras', 'Americas', 'Central America', '[{\"zoneName\":\"America\\/Tegucigalpa\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"}]', '{\"kr\":\"온두라스\",\"br\":\"Honduras\",\"pt\":\"Honduras\",\"nl\":\"Honduras\",\"hr\":\"Honduras\",\"fa\":\"هندوراس\",\"de\":\"Honduras\",\"es\":\"Honduras\",\"fr\":\"Honduras\",\"ja\":\"ホンジュラス\",\"it\":\"Honduras\",\"cn\":\"洪都拉斯\"}', 15.00000000, -86.50000000, '🇭🇳', 'U+1F1ED U+1F1F3', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q783'),
(98, 'Hong Kong S.A.R.', 'HKG', '344', 'HK', '852', 'Hong Kong', 'HKD', '$', '.hk', '香港', 'Asia', 'Eastern Asia', '[{\"zoneName\":\"Asia\\/Hong_Kong\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"HKT\",\"tzName\":\"Hong Kong Time\"}]', '{\"kr\":\"홍콩\",\"br\":\"Hong Kong\",\"pt\":\"Hong Kong\",\"nl\":\"Hongkong\",\"hr\":\"Hong Kong\",\"fa\":\"هنگ‌کنگ\",\"de\":\"Hong Kong\",\"es\":\"Hong Kong\",\"fr\":\"Hong Kong\",\"ja\":\"香港\",\"it\":\"Hong Kong\",\"cn\":\"中国香港\"}', 22.25000000, 114.16666666, '🇭🇰', 'U+1F1ED U+1F1F0', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q8646'),
(99, 'Hungary', 'HUN', '348', 'HU', '36', 'Budapest', 'HUF', 'Ft', '.hu', 'Magyarország', 'Europe', 'Eastern Europe', '[{\"zoneName\":\"Europe\\/Budapest\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"헝가리\",\"br\":\"Hungria\",\"pt\":\"Hungria\",\"nl\":\"Hongarije\",\"hr\":\"Mađarska\",\"fa\":\"مجارستان\",\"de\":\"Ungarn\",\"es\":\"Hungría\",\"fr\":\"Hongrie\",\"ja\":\"ハンガリー\",\"it\":\"Ungheria\",\"cn\":\"匈牙利\"}', 47.00000000, 20.00000000, '🇭🇺', 'U+1F1ED U+1F1FA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q28'),
(100, 'Iceland', 'ISL', '352', 'IS', '354', 'Reykjavik', 'ISK', 'kr', '.is', 'Ísland', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Atlantic\\/Reykjavik\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"아이슬란드\",\"br\":\"Islândia\",\"pt\":\"Islândia\",\"nl\":\"IJsland\",\"hr\":\"Island\",\"fa\":\"ایسلند\",\"de\":\"Island\",\"es\":\"Islandia\",\"fr\":\"Islande\",\"ja\":\"アイスランド\",\"it\":\"Islanda\",\"cn\":\"冰岛\"}', 65.00000000, -18.00000000, '🇮🇸', 'U+1F1EE U+1F1F8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q189'),
(101, 'India', 'IND', '356', 'IN', '91', 'New Delhi', 'INR', '₹', '.in', 'भारत', 'Asia', 'Southern Asia', '[{\"zoneName\":\"Asia\\/Kolkata\",\"gmtOffset\":19800,\"gmtOffsetName\":\"UTC+05:30\",\"abbreviation\":\"IST\",\"tzName\":\"Indian Standard Time\"}]', '{\"kr\":\"인도\",\"br\":\"Índia\",\"pt\":\"Índia\",\"nl\":\"India\",\"hr\":\"Indija\",\"fa\":\"هند\",\"de\":\"Indien\",\"es\":\"India\",\"fr\":\"Inde\",\"ja\":\"インド\",\"it\":\"India\",\"cn\":\"印度\"}', 20.00000000, 77.00000000, '🇮🇳', 'U+1F1EE U+1F1F3', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q668'),
(102, 'Indonesia', 'IDN', '360', 'ID', '62', 'Jakarta', 'IDR', 'Rp', '.id', 'Indonesia', 'Asia', 'South-Eastern Asia', '[{\"zoneName\":\"Asia\\/Jakarta\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"WIB\",\"tzName\":\"Western Indonesian Time\"},{\"zoneName\":\"Asia\\/Jayapura\",\"gmtOffset\":32400,\"gmtOffsetName\":\"UTC+09:00\",\"abbreviation\":\"WIT\",\"tzName\":\"Eastern Indonesian Time\"},{\"zoneName\":\"Asia\\/Makassar\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"WITA\",\"tzName\":\"Central Indonesia Time\"},{\"zoneName\":\"Asia\\/Pontianak\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"WIB\",\"tzName\":\"Western Indonesian Time\"}]', '{\"kr\":\"인도네시아\",\"br\":\"Indonésia\",\"pt\":\"Indonésia\",\"nl\":\"Indonesië\",\"hr\":\"Indonezija\",\"fa\":\"اندونزی\",\"de\":\"Indonesien\",\"es\":\"Indonesia\",\"fr\":\"Indonésie\",\"ja\":\"インドネシア\",\"it\":\"Indonesia\",\"cn\":\"印度尼西亚\"}', -5.00000000, 120.00000000, '🇮🇩', 'U+1F1EE U+1F1E9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q252'),
(103, 'Iran', 'IRN', '364', 'IR', '98', 'Tehran', 'IRR', '﷼', '.ir', 'ایران', 'Asia', 'Southern Asia', '[{\"zoneName\":\"Asia\\/Tehran\",\"gmtOffset\":12600,\"gmtOffsetName\":\"UTC+03:30\",\"abbreviation\":\"IRDT\",\"tzName\":\"Iran Daylight Time\"}]', '{\"kr\":\"이란\",\"br\":\"Irã\",\"pt\":\"Irão\",\"nl\":\"Iran\",\"hr\":\"Iran\",\"fa\":\"ایران\",\"de\":\"Iran\",\"es\":\"Iran\",\"fr\":\"Iran\",\"ja\":\"イラン・イスラム共和国\",\"cn\":\"伊朗\"}', 32.00000000, 53.00000000, '🇮🇷', 'U+1F1EE U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q794'),
(104, 'Iraq', 'IRQ', '368', 'IQ', '964', 'Baghdad', 'IQD', 'د.ع', '.iq', 'العراق', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Baghdad\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"AST\",\"tzName\":\"Arabia Standard Time\"}]', '{\"kr\":\"이라크\",\"br\":\"Iraque\",\"pt\":\"Iraque\",\"nl\":\"Irak\",\"hr\":\"Irak\",\"fa\":\"عراق\",\"de\":\"Irak\",\"es\":\"Irak\",\"fr\":\"Irak\",\"ja\":\"イラク\",\"it\":\"Iraq\",\"cn\":\"伊拉克\"}', 33.00000000, 44.00000000, '🇮🇶', 'U+1F1EE U+1F1F6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q796'),
(105, 'Ireland', 'IRL', '372', 'IE', '353', 'Dublin', 'EUR', '€', '.ie', 'Éire', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/Dublin\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"아일랜드\",\"br\":\"Irlanda\",\"pt\":\"Irlanda\",\"nl\":\"Ierland\",\"hr\":\"Irska\",\"fa\":\"ایرلند\",\"de\":\"Irland\",\"es\":\"Irlanda\",\"fr\":\"Irlande\",\"ja\":\"アイルランド\",\"it\":\"Irlanda\",\"cn\":\"爱尔兰\"}', 53.00000000, -8.00000000, '🇮🇪', 'U+1F1EE U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q27'),
(106, 'Israel', 'ISR', '376', 'IL', '972', 'Jerusalem', 'ILS', '₪', '.il', 'יִשְׂרָאֵל', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Jerusalem\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"IST\",\"tzName\":\"Israel Standard Time\"}]', '{\"kr\":\"이스라엘\",\"br\":\"Israel\",\"pt\":\"Israel\",\"nl\":\"Israël\",\"hr\":\"Izrael\",\"fa\":\"اسرائیل\",\"de\":\"Israel\",\"es\":\"Israel\",\"fr\":\"Israël\",\"ja\":\"イスラエル\",\"it\":\"Israele\",\"cn\":\"以色列\"}', 31.50000000, 34.75000000, '🇮🇱', 'U+1F1EE U+1F1F1', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q801'),
(107, 'Italy', 'ITA', '380', 'IT', '39', 'Rome', 'EUR', '€', '.it', 'Italia', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Rome\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"이탈리아\",\"br\":\"Itália\",\"pt\":\"Itália\",\"nl\":\"Italië\",\"hr\":\"Italija\",\"fa\":\"ایتالیا\",\"de\":\"Italien\",\"es\":\"Italia\",\"fr\":\"Italie\",\"ja\":\"イタリア\",\"it\":\"Italia\",\"cn\":\"意大利\"}', 42.83333333, 12.83333333, '🇮🇹', 'U+1F1EE U+1F1F9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q38'),
(108, 'Jamaica', 'JAM', '388', 'JM', '+1-876', 'Kingston', 'JMD', 'J$', '.jm', 'Jamaica', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Jamaica\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"}]', '{\"kr\":\"자메이카\",\"br\":\"Jamaica\",\"pt\":\"Jamaica\",\"nl\":\"Jamaica\",\"hr\":\"Jamajka\",\"fa\":\"جامائیکا\",\"de\":\"Jamaika\",\"es\":\"Jamaica\",\"fr\":\"Jamaïque\",\"ja\":\"ジャマイカ\",\"it\":\"Giamaica\",\"cn\":\"牙买加\"}', 18.25000000, -77.50000000, '🇯🇲', 'U+1F1EF U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q766'),
(109, 'Japan', 'JPN', '392', 'JP', '81', 'Tokyo', 'JPY', '¥', '.jp', '日本', 'Asia', 'Eastern Asia', '[{\"zoneName\":\"Asia\\/Tokyo\",\"gmtOffset\":32400,\"gmtOffsetName\":\"UTC+09:00\",\"abbreviation\":\"JST\",\"tzName\":\"Japan Standard Time\"}]', '{\"kr\":\"일본\",\"br\":\"Japão\",\"pt\":\"Japão\",\"nl\":\"Japan\",\"hr\":\"Japan\",\"fa\":\"ژاپن\",\"de\":\"Japan\",\"es\":\"Japón\",\"fr\":\"Japon\",\"ja\":\"日本\",\"it\":\"Giappone\",\"cn\":\"日本\"}', 36.00000000, 138.00000000, '🇯🇵', 'U+1F1EF U+1F1F5', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q17'),
(110, 'Jersey', 'JEY', '832', 'JE', '+44-1534', 'Saint Helier', 'GBP', '£', '.je', 'Jersey', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/Jersey\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"저지 섬\",\"br\":\"Jersey\",\"pt\":\"Jersey\",\"nl\":\"Jersey\",\"hr\":\"Jersey\",\"fa\":\"جرزی\",\"de\":\"Jersey\",\"es\":\"Jersey\",\"fr\":\"Jersey\",\"ja\":\"ジャージー\",\"it\":\"Isola di Jersey\",\"cn\":\"泽西岛\"}', 49.25000000, -2.16666666, '🇯🇪', 'U+1F1EF U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q785'),
(111, 'Jordan', 'JOR', '400', 'JO', '962', 'Amman', 'JOD', 'ا.د', '.jo', 'الأردن', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Amman\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"요르단\",\"br\":\"Jordânia\",\"pt\":\"Jordânia\",\"nl\":\"Jordanië\",\"hr\":\"Jordan\",\"fa\":\"اردن\",\"de\":\"Jordanien\",\"es\":\"Jordania\",\"fr\":\"Jordanie\",\"ja\":\"ヨルダン\",\"it\":\"Giordania\",\"cn\":\"约旦\"}', 31.00000000, 36.00000000, '🇯🇴', 'U+1F1EF U+1F1F4', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q810'),
(112, 'Kazakhstan', 'KAZ', '398', 'KZ', '7', 'Astana', 'KZT', 'лв', '.kz', 'Қазақстан', 'Asia', 'Central Asia', '[{\"zoneName\":\"Asia\\/Almaty\",\"gmtOffset\":21600,\"gmtOffsetName\":\"UTC+06:00\",\"abbreviation\":\"ALMT\",\"tzName\":\"Alma-Ata Time[1\"},{\"zoneName\":\"Asia\\/Aqtau\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"AQTT\",\"tzName\":\"Aqtobe Time\"},{\"zoneName\":\"Asia\\/Aqtobe\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"AQTT\",\"tzName\":\"Aqtobe Time\"},{\"zoneName\":\"Asia\\/Atyrau\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"MSD+1\",\"tzName\":\"Moscow Daylight Time+1\"},{\"zoneName\":\"Asia\\/Oral\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"ORAT\",\"tzName\":\"Oral Time\"},{\"zoneName\":\"Asia\\/Qostanay\",\"gmtOffset\":21600,\"gmtOffsetName\":\"UTC+06:00\",\"abbreviation\":\"QYZST\",\"tzName\":\"Qyzylorda Summer Time\"},{\"zoneName\":\"Asia\\/Qyzylorda\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"QYZT\",\"tzName\":\"Qyzylorda Summer Time\"}]', '{\"kr\":\"카자흐스탄\",\"br\":\"Cazaquistão\",\"pt\":\"Cazaquistão\",\"nl\":\"Kazachstan\",\"hr\":\"Kazahstan\",\"fa\":\"قزاقستان\",\"de\":\"Kasachstan\",\"es\":\"Kazajistán\",\"fr\":\"Kazakhstan\",\"ja\":\"カザフスタン\",\"it\":\"Kazakistan\",\"cn\":\"哈萨克斯坦\"}', 48.00000000, 68.00000000, '🇰🇿', 'U+1F1F0 U+1F1FF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q232'),
(113, 'Kenya', 'KEN', '404', 'KE', '254', 'Nairobi', 'KES', 'KSh', '.ke', 'Kenya', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Nairobi\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"EAT\",\"tzName\":\"East Africa Time\"}]', '{\"kr\":\"케냐\",\"br\":\"Quênia\",\"pt\":\"Quénia\",\"nl\":\"Kenia\",\"hr\":\"Kenija\",\"fa\":\"کنیا\",\"de\":\"Kenia\",\"es\":\"Kenia\",\"fr\":\"Kenya\",\"ja\":\"ケニア\",\"it\":\"Kenya\",\"cn\":\"肯尼亚\"}', 1.00000000, 38.00000000, '🇰🇪', 'U+1F1F0 U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q114'),
(114, 'Kiribati', 'KIR', '296', 'KI', '686', 'Tarawa', 'AUD', '$', '.ki', 'Kiribati', 'Oceania', 'Micronesia', '[{\"zoneName\":\"Pacific\\/Enderbury\",\"gmtOffset\":46800,\"gmtOffsetName\":\"UTC+13:00\",\"abbreviation\":\"PHOT\",\"tzName\":\"Phoenix Island Time\"},{\"zoneName\":\"Pacific\\/Kiritimati\",\"gmtOffset\":50400,\"gmtOffsetName\":\"UTC+14:00\",\"abbreviation\":\"LINT\",\"tzName\":\"Line Islands Time\"},{\"zoneName\":\"Pacific\\/Tarawa\",\"gmtOffset\":43200,\"gmtOffsetName\":\"UTC+12:00\",\"abbreviation\":\"GILT\",\"tzName\":\"Gilbert Island Time\"}]', '{\"kr\":\"키리바시\",\"br\":\"Kiribati\",\"pt\":\"Quiribáti\",\"nl\":\"Kiribati\",\"hr\":\"Kiribati\",\"fa\":\"کیریباتی\",\"de\":\"Kiribati\",\"es\":\"Kiribati\",\"fr\":\"Kiribati\",\"ja\":\"キリバス\",\"it\":\"Kiribati\",\"cn\":\"基里巴斯\"}', 1.41666666, 173.00000000, '🇰🇮', 'U+1F1F0 U+1F1EE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q710'),
(115, 'North Korea', 'PRK', '408', 'KP', '850', 'Pyongyang', 'KPW', '₩', '.kp', '북한', 'Asia', 'Eastern Asia', '[{\"zoneName\":\"Asia\\/Pyongyang\",\"gmtOffset\":32400,\"gmtOffsetName\":\"UTC+09:00\",\"abbreviation\":\"KST\",\"tzName\":\"Korea Standard Time\"}]', '{\"kr\":\"조선민주주의인민공화국\",\"br\":\"Coreia do Norte\",\"pt\":\"Coreia do Norte\",\"nl\":\"Noord-Korea\",\"hr\":\"Sjeverna Koreja\",\"fa\":\"کره جنوبی\",\"de\":\"Nordkorea\",\"es\":\"Corea del Norte\",\"fr\":\"Corée du Nord\",\"ja\":\"朝鮮民主主義人民共和国\",\"it\":\"Corea del Nord\",\"cn\":\"朝鲜\"}', 40.00000000, 127.00000000, '🇰🇵', 'U+1F1F0 U+1F1F5', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q423'),
(116, 'South Korea', 'KOR', '410', 'KR', '82', 'Seoul', 'KRW', '₩', '.kr', '대한민국', 'Asia', 'Eastern Asia', '[{\"zoneName\":\"Asia\\/Seoul\",\"gmtOffset\":32400,\"gmtOffsetName\":\"UTC+09:00\",\"abbreviation\":\"KST\",\"tzName\":\"Korea Standard Time\"}]', '{\"kr\":\"대한민국\",\"br\":\"Coreia do Sul\",\"pt\":\"Coreia do Sul\",\"nl\":\"Zuid-Korea\",\"hr\":\"Južna Koreja\",\"fa\":\"کره شمالی\",\"de\":\"Südkorea\",\"es\":\"Corea del Sur\",\"fr\":\"Corée du Sud\",\"ja\":\"大韓民国\",\"it\":\"Corea del Sud\",\"cn\":\"韩国\"}', 37.00000000, 127.50000000, '🇰🇷', 'U+1F1F0 U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q884'),
(117, 'Kuwait', 'KWT', '414', 'KW', '965', 'Kuwait City', 'KWD', 'ك.د', '.kw', 'الكويت', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Kuwait\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"AST\",\"tzName\":\"Arabia Standard Time\"}]', '{\"kr\":\"쿠웨이트\",\"br\":\"Kuwait\",\"pt\":\"Kuwait\",\"nl\":\"Koeweit\",\"hr\":\"Kuvajt\",\"fa\":\"کویت\",\"de\":\"Kuwait\",\"es\":\"Kuwait\",\"fr\":\"Koweït\",\"ja\":\"クウェート\",\"it\":\"Kuwait\",\"cn\":\"科威特\"}', 29.50000000, 45.75000000, '🇰🇼', 'U+1F1F0 U+1F1FC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q817'),
(118, 'Kyrgyzstan', 'KGZ', '417', 'KG', '996', 'Bishkek', 'KGS', 'лв', '.kg', 'Кыргызстан', 'Asia', 'Central Asia', '[{\"zoneName\":\"Asia\\/Bishkek\",\"gmtOffset\":21600,\"gmtOffsetName\":\"UTC+06:00\",\"abbreviation\":\"KGT\",\"tzName\":\"Kyrgyzstan Time\"}]', '{\"kr\":\"키르기스스탄\",\"br\":\"Quirguistão\",\"pt\":\"Quirguizistão\",\"nl\":\"Kirgizië\",\"hr\":\"Kirgistan\",\"fa\":\"قرقیزستان\",\"de\":\"Kirgisistan\",\"es\":\"Kirguizistán\",\"fr\":\"Kirghizistan\",\"ja\":\"キルギス\",\"it\":\"Kirghizistan\",\"cn\":\"吉尔吉斯斯坦\"}', 41.00000000, 75.00000000, '🇰🇬', 'U+1F1F0 U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q813'),
(119, 'Laos', 'LAO', '418', 'LA', '856', 'Vientiane', 'LAK', '₭', '.la', 'ສປປລາວ', 'Asia', 'South-Eastern Asia', '[{\"zoneName\":\"Asia\\/Vientiane\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"ICT\",\"tzName\":\"Indochina Time\"}]', '{\"kr\":\"라오스\",\"br\":\"Laos\",\"pt\":\"Laos\",\"nl\":\"Laos\",\"hr\":\"Laos\",\"fa\":\"لائوس\",\"de\":\"Laos\",\"es\":\"Laos\",\"fr\":\"Laos\",\"ja\":\"ラオス人民民主共和国\",\"it\":\"Laos\",\"cn\":\"寮人民民主共和国\"}', 18.00000000, 105.00000000, '🇱🇦', 'U+1F1F1 U+1F1E6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q819'),
(120, 'Latvia', 'LVA', '428', 'LV', '371', 'Riga', 'EUR', '€', '.lv', 'Latvija', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/Riga\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"라트비아\",\"br\":\"Letônia\",\"pt\":\"Letónia\",\"nl\":\"Letland\",\"hr\":\"Latvija\",\"fa\":\"لتونی\",\"de\":\"Lettland\",\"es\":\"Letonia\",\"fr\":\"Lettonie\",\"ja\":\"ラトビア\",\"it\":\"Lettonia\",\"cn\":\"拉脱维亚\"}', 57.00000000, 25.00000000, '🇱🇻', 'U+1F1F1 U+1F1FB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q211'),
(121, 'Lebanon', 'LBN', '422', 'LB', '961', 'Beirut', 'LBP', '£', '.lb', 'لبنان', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Beirut\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"레바논\",\"br\":\"Líbano\",\"pt\":\"Líbano\",\"nl\":\"Libanon\",\"hr\":\"Libanon\",\"fa\":\"لبنان\",\"de\":\"Libanon\",\"es\":\"Líbano\",\"fr\":\"Liban\",\"ja\":\"レバノン\",\"it\":\"Libano\",\"cn\":\"黎巴嫩\"}', 33.83333333, 35.83333333, '🇱🇧', 'U+1F1F1 U+1F1E7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q822'),
(122, 'Lesotho', 'LSO', '426', 'LS', '266', 'Maseru', 'LSL', 'L', '.ls', 'Lesotho', 'Africa', 'Southern Africa', '[{\"zoneName\":\"Africa\\/Maseru\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"SAST\",\"tzName\":\"South African Standard Time\"}]', '{\"kr\":\"레소토\",\"br\":\"Lesoto\",\"pt\":\"Lesoto\",\"nl\":\"Lesotho\",\"hr\":\"Lesoto\",\"fa\":\"لسوتو\",\"de\":\"Lesotho\",\"es\":\"Lesotho\",\"fr\":\"Lesotho\",\"ja\":\"レソト\",\"it\":\"Lesotho\",\"cn\":\"莱索托\"}', -29.50000000, 28.50000000, '🇱🇸', 'U+1F1F1 U+1F1F8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1013'),
(123, 'Liberia', 'LBR', '430', 'LR', '231', 'Monrovia', 'LRD', '$', '.lr', 'Liberia', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Monrovia\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"라이베리아\",\"br\":\"Libéria\",\"pt\":\"Libéria\",\"nl\":\"Liberia\",\"hr\":\"Liberija\",\"fa\":\"لیبریا\",\"de\":\"Liberia\",\"es\":\"Liberia\",\"fr\":\"Liberia\",\"ja\":\"リベリア\",\"it\":\"Liberia\",\"cn\":\"利比里亚\"}', 6.50000000, -9.50000000, '🇱🇷', 'U+1F1F1 U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1014'),
(124, 'Libya', 'LBY', '434', 'LY', '218', 'Tripolis', 'LYD', 'د.ل', '.ly', '‏ليبيا', 'Africa', 'Northern Africa', '[{\"zoneName\":\"Africa\\/Tripoli\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"리비아\",\"br\":\"Líbia\",\"pt\":\"Líbia\",\"nl\":\"Libië\",\"hr\":\"Libija\",\"fa\":\"لیبی\",\"de\":\"Libyen\",\"es\":\"Libia\",\"fr\":\"Libye\",\"ja\":\"リビア\",\"it\":\"Libia\",\"cn\":\"利比亚\"}', 25.00000000, 17.00000000, '🇱🇾', 'U+1F1F1 U+1F1FE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1016'),
(125, 'Liechtenstein', 'LIE', '438', 'LI', '423', 'Vaduz', 'CHF', 'CHf', '.li', 'Liechtenstein', 'Europe', 'Western Europe', '[{\"zoneName\":\"Europe\\/Vaduz\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"리히텐슈타인\",\"br\":\"Liechtenstein\",\"pt\":\"Listenstaine\",\"nl\":\"Liechtenstein\",\"hr\":\"Lihtenštajn\",\"fa\":\"لیختن‌اشتاین\",\"de\":\"Liechtenstein\",\"es\":\"Liechtenstein\",\"fr\":\"Liechtenstein\",\"ja\":\"リヒテンシュタイン\",\"it\":\"Liechtenstein\",\"cn\":\"列支敦士登\"}', 47.26666666, 9.53333333, '🇱🇮', 'U+1F1F1 U+1F1EE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q347'),
(126, 'Lithuania', 'LTU', '440', 'LT', '370', 'Vilnius', 'EUR', '€', '.lt', 'Lietuva', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/Vilnius\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"리투아니아\",\"br\":\"Lituânia\",\"pt\":\"Lituânia\",\"nl\":\"Litouwen\",\"hr\":\"Litva\",\"fa\":\"لیتوانی\",\"de\":\"Litauen\",\"es\":\"Lituania\",\"fr\":\"Lituanie\",\"ja\":\"リトアニア\",\"it\":\"Lituania\",\"cn\":\"立陶宛\"}', 56.00000000, 24.00000000, '🇱🇹', 'U+1F1F1 U+1F1F9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q37'),
(127, 'Luxembourg', 'LUX', '442', 'LU', '352', 'Luxembourg', 'EUR', '€', '.lu', 'Luxembourg', 'Europe', 'Western Europe', '[{\"zoneName\":\"Europe\\/Luxembourg\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"룩셈부르크\",\"br\":\"Luxemburgo\",\"pt\":\"Luxemburgo\",\"nl\":\"Luxemburg\",\"hr\":\"Luksemburg\",\"fa\":\"لوکزامبورگ\",\"de\":\"Luxemburg\",\"es\":\"Luxemburgo\",\"fr\":\"Luxembourg\",\"ja\":\"ルクセンブルク\",\"it\":\"Lussemburgo\",\"cn\":\"卢森堡\"}', 49.75000000, 6.16666666, '🇱🇺', 'U+1F1F1 U+1F1FA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q32'),
(128, 'Macau S.A.R.', 'MAC', '446', 'MO', '853', 'Macao', 'MOP', '$', '.mo', '澳門', 'Asia', 'Eastern Asia', '[{\"zoneName\":\"Asia\\/Macau\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"CST\",\"tzName\":\"China Standard Time\"}]', '{\"kr\":\"마카오\",\"br\":\"Macau\",\"pt\":\"Macau\",\"nl\":\"Macao\",\"hr\":\"Makao\",\"fa\":\"مکائو\",\"de\":\"Macao\",\"es\":\"Macao\",\"fr\":\"Macao\",\"ja\":\"マカオ\",\"it\":\"Macao\",\"cn\":\"中国澳门\"}', 22.16666666, 113.55000000, '🇲🇴', 'U+1F1F2 U+1F1F4', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(129, 'Macedonia', 'MKD', '807', 'MK', '389', 'Skopje', 'MKD', 'ден', '.mk', 'Северна Македонија', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Skopje\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"마케도니아\",\"br\":\"Macedônia\",\"pt\":\"Macedónia\",\"nl\":\"Macedonië\",\"hr\":\"Makedonija\",\"fa\":\"\",\"de\":\"Mazedonien\",\"es\":\"Macedonia\",\"fr\":\"Macédoine\",\"ja\":\"マケドニア旧ユーゴスラビア共和国\",\"it\":\"Macedonia\",\"cn\":\"马其顿\"}', 41.83333333, 22.00000000, '🇲🇰', 'U+1F1F2 U+1F1F0', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q221'),
(130, 'Madagascar', 'MDG', '450', 'MG', '261', 'Antananarivo', 'MGA', 'Ar', '.mg', 'Madagasikara', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Indian\\/Antananarivo\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"EAT\",\"tzName\":\"East Africa Time\"}]', '{\"kr\":\"마다가스카르\",\"br\":\"Madagascar\",\"pt\":\"Madagáscar\",\"nl\":\"Madagaskar\",\"hr\":\"Madagaskar\",\"fa\":\"ماداگاسکار\",\"de\":\"Madagaskar\",\"es\":\"Madagascar\",\"fr\":\"Madagascar\",\"ja\":\"マダガスカル\",\"it\":\"Madagascar\",\"cn\":\"马达加斯加\"}', -20.00000000, 47.00000000, '🇲🇬', 'U+1F1F2 U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1019'),
(131, 'Malawi', 'MWI', '454', 'MW', '265', 'Lilongwe', 'MWK', 'MK', '.mw', 'Malawi', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Blantyre\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"CAT\",\"tzName\":\"Central Africa Time\"}]', '{\"kr\":\"말라위\",\"br\":\"Malawi\",\"pt\":\"Malávi\",\"nl\":\"Malawi\",\"hr\":\"Malavi\",\"fa\":\"مالاوی\",\"de\":\"Malawi\",\"es\":\"Malawi\",\"fr\":\"Malawi\",\"ja\":\"マラウイ\",\"it\":\"Malawi\",\"cn\":\"马拉维\"}', -13.50000000, 34.00000000, '🇲🇼', 'U+1F1F2 U+1F1FC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1020'),
(132, 'Malaysia', 'MYS', '458', 'MY', '60', 'Kuala Lumpur', 'MYR', 'RM', '.my', 'Malaysia', 'Asia', 'South-Eastern Asia', '[{\"zoneName\":\"Asia\\/Kuala_Lumpur\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"MYT\",\"tzName\":\"Malaysia Time\"},{\"zoneName\":\"Asia\\/Kuching\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"MYT\",\"tzName\":\"Malaysia Time\"}]', '{\"kr\":\"말레이시아\",\"br\":\"Malásia\",\"pt\":\"Malásia\",\"nl\":\"Maleisië\",\"hr\":\"Malezija\",\"fa\":\"مالزی\",\"de\":\"Malaysia\",\"es\":\"Malasia\",\"fr\":\"Malaisie\",\"ja\":\"マレーシア\",\"it\":\"Malesia\",\"cn\":\"马来西亚\"}', 2.50000000, 112.50000000, '🇲🇾', 'U+1F1F2 U+1F1FE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q833'),
(133, 'Maldives', 'MDV', '462', 'MV', '960', 'Male', 'MVR', 'Rf', '.mv', 'Maldives', 'Asia', 'Southern Asia', '[{\"zoneName\":\"Indian\\/Maldives\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"MVT\",\"tzName\":\"Maldives Time\"}]', '{\"kr\":\"몰디브\",\"br\":\"Maldivas\",\"pt\":\"Maldivas\",\"nl\":\"Maldiven\",\"hr\":\"Maldivi\",\"fa\":\"مالدیو\",\"de\":\"Malediven\",\"es\":\"Maldivas\",\"fr\":\"Maldives\",\"ja\":\"モルディブ\",\"it\":\"Maldive\",\"cn\":\"马尔代夫\"}', 3.25000000, 73.00000000, '🇲🇻', 'U+1F1F2 U+1F1FB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q826');
INSERT INTO `country` (`id`, `country`, `iso3`, `numeric_code`, `country_code`, `phone_code`, `capital`, `currency_code`, `currency_symbol`, `tld`, `native`, `region`, `subregion`, `timezones`, `translations`, `latitude`, `longitude`, `emoji`, `emojiU`, `created_at`, `updated_at`, `flag`, `wikiDataId`) VALUES
(134, 'Mali', 'MLI', '466', 'ML', '223', 'Bamako', 'XOF', 'CFA', '.ml', 'Mali', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Bamako\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"말리\",\"br\":\"Mali\",\"pt\":\"Mali\",\"nl\":\"Mali\",\"hr\":\"Mali\",\"fa\":\"مالی\",\"de\":\"Mali\",\"es\":\"Mali\",\"fr\":\"Mali\",\"ja\":\"マリ\",\"it\":\"Mali\",\"cn\":\"马里\"}', 17.00000000, -4.00000000, '🇲🇱', 'U+1F1F2 U+1F1F1', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q912'),
(135, 'Malta', 'MLT', '470', 'MT', '356', 'Valletta', 'EUR', '€', '.mt', 'Malta', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Malta\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"몰타\",\"br\":\"Malta\",\"pt\":\"Malta\",\"nl\":\"Malta\",\"hr\":\"Malta\",\"fa\":\"مالت\",\"de\":\"Malta\",\"es\":\"Malta\",\"fr\":\"Malte\",\"ja\":\"マルタ\",\"it\":\"Malta\",\"cn\":\"马耳他\"}', 35.83333333, 14.58333333, '🇲🇹', 'U+1F1F2 U+1F1F9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q233'),
(136, 'Man (Isle of)', 'IMN', '833', 'IM', '+44-1624', 'Douglas, Isle of Man', 'GBP', '£', '.im', 'Isle of Man', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/Isle_of_Man\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"맨 섬\",\"br\":\"Ilha de Man\",\"pt\":\"Ilha de Man\",\"nl\":\"Isle of Man\",\"hr\":\"Otok Man\",\"fa\":\"جزیره من\",\"de\":\"Insel Man\",\"es\":\"Isla de Man\",\"fr\":\"Île de Man\",\"ja\":\"マン島\",\"it\":\"Isola di Man\",\"cn\":\"马恩岛\"}', 54.25000000, -4.50000000, '🇮🇲', 'U+1F1EE U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(137, 'Marshall Islands', 'MHL', '584', 'MH', '692', 'Majuro', 'USD', '$', '.mh', 'M̧ajeļ', 'Oceania', 'Micronesia', '[{\"zoneName\":\"Pacific\\/Kwajalein\",\"gmtOffset\":43200,\"gmtOffsetName\":\"UTC+12:00\",\"abbreviation\":\"MHT\",\"tzName\":\"Marshall Islands Time\"},{\"zoneName\":\"Pacific\\/Majuro\",\"gmtOffset\":43200,\"gmtOffsetName\":\"UTC+12:00\",\"abbreviation\":\"MHT\",\"tzName\":\"Marshall Islands Time\"}]', '{\"kr\":\"마셜 제도\",\"br\":\"Ilhas Marshall\",\"pt\":\"Ilhas Marshall\",\"nl\":\"Marshalleilanden\",\"hr\":\"Maršalovi Otoci\",\"fa\":\"جزایر مارشال\",\"de\":\"Marshallinseln\",\"es\":\"Islas Marshall\",\"fr\":\"Îles Marshall\",\"ja\":\"マーシャル諸島\",\"it\":\"Isole Marshall\",\"cn\":\"马绍尔群岛\"}', 9.00000000, 168.00000000, '🇲🇭', 'U+1F1F2 U+1F1ED', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q709'),
(138, 'Martinique', 'MTQ', '474', 'MQ', '596', 'Fort-de-France', 'EUR', '€', '.mq', 'Martinique', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Martinique\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"마르티니크\",\"br\":\"Martinica\",\"pt\":\"Martinica\",\"nl\":\"Martinique\",\"hr\":\"Martinique\",\"fa\":\"مونتسرات\",\"de\":\"Martinique\",\"es\":\"Martinica\",\"fr\":\"Martinique\",\"ja\":\"マルティニーク\",\"it\":\"Martinica\",\"cn\":\"马提尼克岛\"}', 14.66666700, -61.00000000, '🇲🇶', 'U+1F1F2 U+1F1F6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(139, 'Mauritania', 'MRT', '478', 'MR', '222', 'Nouakchott', 'MRO', 'MRU', '.mr', 'موريتانيا', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Nouakchott\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"모리타니\",\"br\":\"Mauritânia\",\"pt\":\"Mauritânia\",\"nl\":\"Mauritanië\",\"hr\":\"Mauritanija\",\"fa\":\"موریتانی\",\"de\":\"Mauretanien\",\"es\":\"Mauritania\",\"fr\":\"Mauritanie\",\"ja\":\"モーリタニア\",\"it\":\"Mauritania\",\"cn\":\"毛里塔尼亚\"}', 20.00000000, -12.00000000, '🇲🇷', 'U+1F1F2 U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1025'),
(140, 'Mauritius', 'MUS', '480', 'MU', '230', 'Port Louis', 'MUR', '₨', '.mu', 'Maurice', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Indian\\/Mauritius\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"MUT\",\"tzName\":\"Mauritius Time\"}]', '{\"kr\":\"모리셔스\",\"br\":\"Maurício\",\"pt\":\"Maurícia\",\"nl\":\"Mauritius\",\"hr\":\"Mauricijus\",\"fa\":\"موریس\",\"de\":\"Mauritius\",\"es\":\"Mauricio\",\"fr\":\"Île Maurice\",\"ja\":\"モーリシャス\",\"it\":\"Mauritius\",\"cn\":\"毛里求斯\"}', -20.28333333, 57.55000000, '🇲🇺', 'U+1F1F2 U+1F1FA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1027'),
(141, 'Mayotte', 'MYT', '175', 'YT', '262', 'Mamoudzou', 'EUR', '€', '.yt', 'Mayotte', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Indian\\/Mayotte\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"EAT\",\"tzName\":\"East Africa Time\"}]', '{\"kr\":\"마요트\",\"br\":\"Mayotte\",\"pt\":\"Mayotte\",\"nl\":\"Mayotte\",\"hr\":\"Mayotte\",\"fa\":\"مایوت\",\"de\":\"Mayotte\",\"es\":\"Mayotte\",\"fr\":\"Mayotte\",\"ja\":\"マヨット\",\"it\":\"Mayotte\",\"cn\":\"马约特\"}', -12.83333333, 45.16666666, '🇾🇹', 'U+1F1FE U+1F1F9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(142, 'Mexico', 'MEX', '484', 'MX', '52', 'Mexico City', 'MXN', '$', '.mx', 'México', 'Americas', 'Central America', '[{\"zoneName\":\"America\\/Bahia_Banderas\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Cancun\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Chihuahua\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America\"},{\"zoneName\":\"America\\/Hermosillo\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America\"},{\"zoneName\":\"America\\/Matamoros\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Mazatlan\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America\"},{\"zoneName\":\"America\\/Merida\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Mexico_City\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Monterrey\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Ojinaga\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America\"},{\"zoneName\":\"America\\/Tijuana\",\"gmtOffset\":-28800,\"gmtOffsetName\":\"UTC-08:00\",\"abbreviation\":\"PST\",\"tzName\":\"Pacific Standard Time (North America\"}]', '{\"kr\":\"멕시코\",\"br\":\"México\",\"pt\":\"México\",\"nl\":\"Mexico\",\"hr\":\"Meksiko\",\"fa\":\"مکزیک\",\"de\":\"Mexiko\",\"es\":\"México\",\"fr\":\"Mexique\",\"ja\":\"メキシコ\",\"it\":\"Messico\",\"cn\":\"墨西哥\"}', 23.00000000, -102.00000000, '🇲🇽', 'U+1F1F2 U+1F1FD', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q96'),
(143, 'Micronesia', 'FSM', '583', 'FM', '691', 'Palikir', 'USD', '$', '.fm', 'Micronesia', 'Oceania', 'Micronesia', '[{\"zoneName\":\"Pacific\\/Chuuk\",\"gmtOffset\":36000,\"gmtOffsetName\":\"UTC+10:00\",\"abbreviation\":\"CHUT\",\"tzName\":\"Chuuk Time\"},{\"zoneName\":\"Pacific\\/Kosrae\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"KOST\",\"tzName\":\"Kosrae Time\"},{\"zoneName\":\"Pacific\\/Pohnpei\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"PONT\",\"tzName\":\"Pohnpei Standard Time\"}]', '{\"kr\":\"미크로네시아 연방\",\"br\":\"Micronésia\",\"pt\":\"Micronésia\",\"nl\":\"Micronesië\",\"hr\":\"Mikronezija\",\"fa\":\"ایالات فدرال میکرونزی\",\"de\":\"Mikronesien\",\"es\":\"Micronesia\",\"fr\":\"Micronésie\",\"ja\":\"ミクロネシア連邦\",\"it\":\"Micronesia\",\"cn\":\"密克罗尼西亚\"}', 6.91666666, 158.25000000, '🇫🇲', 'U+1F1EB U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q702'),
(144, 'Moldova', 'MDA', '498', 'MD', '373', 'Chisinau', 'MDL', 'L', '.md', 'Moldova', 'Europe', 'Eastern Europe', '[{\"zoneName\":\"Europe\\/Chisinau\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"몰도바\",\"br\":\"Moldávia\",\"pt\":\"Moldávia\",\"nl\":\"Moldavië\",\"hr\":\"Moldova\",\"fa\":\"مولداوی\",\"de\":\"Moldawie\",\"es\":\"Moldavia\",\"fr\":\"Moldavie\",\"ja\":\"モルドバ共和国\",\"it\":\"Moldavia\",\"cn\":\"摩尔多瓦\"}', 47.00000000, 29.00000000, '🇲🇩', 'U+1F1F2 U+1F1E9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q217'),
(145, 'Monaco', 'MCO', '492', 'MC', '377', 'Monaco', 'EUR', '€', '.mc', 'Monaco', 'Europe', 'Western Europe', '[{\"zoneName\":\"Europe\\/Monaco\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"모나코\",\"br\":\"Mônaco\",\"pt\":\"Mónaco\",\"nl\":\"Monaco\",\"hr\":\"Monako\",\"fa\":\"موناکو\",\"de\":\"Monaco\",\"es\":\"Mónaco\",\"fr\":\"Monaco\",\"ja\":\"モナコ\",\"it\":\"Principato di Monaco\",\"cn\":\"摩纳哥\"}', 43.73333333, 7.40000000, '🇲🇨', 'U+1F1F2 U+1F1E8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q235'),
(146, 'Mongolia', 'MNG', '496', 'MN', '976', 'Ulan Bator', 'MNT', '₮', '.mn', 'Монгол улс', 'Asia', 'Eastern Asia', '[{\"zoneName\":\"Asia\\/Choibalsan\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"CHOT\",\"tzName\":\"Choibalsan Standard Time\"},{\"zoneName\":\"Asia\\/Hovd\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"HOVT\",\"tzName\":\"Hovd Time\"},{\"zoneName\":\"Asia\\/Ulaanbaatar\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"ULAT\",\"tzName\":\"Ulaanbaatar Standard Time\"}]', '{\"kr\":\"몽골\",\"br\":\"Mongólia\",\"pt\":\"Mongólia\",\"nl\":\"Mongolië\",\"hr\":\"Mongolija\",\"fa\":\"مغولستان\",\"de\":\"Mongolei\",\"es\":\"Mongolia\",\"fr\":\"Mongolie\",\"ja\":\"モンゴル\",\"it\":\"Mongolia\",\"cn\":\"蒙古\"}', 46.00000000, 105.00000000, '🇲🇳', 'U+1F1F2 U+1F1F3', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q711'),
(147, 'Montenegro', 'MNE', '499', 'ME', '382', 'Podgorica', 'EUR', '€', '.me', 'Црна Гора', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Podgorica\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"몬테네그로\",\"br\":\"Montenegro\",\"pt\":\"Montenegro\",\"nl\":\"Montenegro\",\"hr\":\"Crna Gora\",\"fa\":\"مونته‌نگرو\",\"de\":\"Montenegro\",\"es\":\"Montenegro\",\"fr\":\"Monténégro\",\"ja\":\"モンテネグロ\",\"it\":\"Montenegro\",\"cn\":\"黑山\"}', 42.50000000, 19.30000000, '🇲🇪', 'U+1F1F2 U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q236'),
(148, 'Montserrat', 'MSR', '500', 'MS', '+1-664', 'Plymouth', 'XCD', '$', '.ms', 'Montserrat', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Montserrat\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"몬트세랫\",\"br\":\"Montserrat\",\"pt\":\"Monserrate\",\"nl\":\"Montserrat\",\"hr\":\"Montserrat\",\"fa\":\"مایوت\",\"de\":\"Montserrat\",\"es\":\"Montserrat\",\"fr\":\"Montserrat\",\"ja\":\"モントセラト\",\"it\":\"Montserrat\",\"cn\":\"蒙特塞拉特\"}', 16.75000000, -62.20000000, '🇲🇸', 'U+1F1F2 U+1F1F8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(149, 'Morocco', 'MAR', '504', 'MA', '212', 'Rabat', 'MAD', 'DH', '.ma', 'المغرب', 'Africa', 'Northern Africa', '[{\"zoneName\":\"Africa\\/Casablanca\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WEST\",\"tzName\":\"Western European Summer Time\"}]', '{\"kr\":\"모로코\",\"br\":\"Marrocos\",\"pt\":\"Marrocos\",\"nl\":\"Marokko\",\"hr\":\"Maroko\",\"fa\":\"مراکش\",\"de\":\"Marokko\",\"es\":\"Marruecos\",\"fr\":\"Maroc\",\"ja\":\"モロッコ\",\"it\":\"Marocco\",\"cn\":\"摩洛哥\"}', 32.00000000, -5.00000000, '🇲🇦', 'U+1F1F2 U+1F1E6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1028'),
(150, 'Mozambique', 'MOZ', '508', 'MZ', '258', 'Maputo', 'MZN', 'MT', '.mz', 'Moçambique', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Maputo\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"CAT\",\"tzName\":\"Central Africa Time\"}]', '{\"kr\":\"모잠비크\",\"br\":\"Moçambique\",\"pt\":\"Moçambique\",\"nl\":\"Mozambique\",\"hr\":\"Mozambik\",\"fa\":\"موزامبیک\",\"de\":\"Mosambik\",\"es\":\"Mozambique\",\"fr\":\"Mozambique\",\"ja\":\"モザンビーク\",\"it\":\"Mozambico\",\"cn\":\"莫桑比克\"}', -18.25000000, 35.00000000, '🇲🇿', 'U+1F1F2 U+1F1FF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1029'),
(151, 'Myanmar', 'MMR', '104', 'MM', '95', 'Nay Pyi Taw', 'MMK', 'K', '.mm', 'မြန်မာ', 'Asia', 'South-Eastern Asia', '[{\"zoneName\":\"Asia\\/Yangon\",\"gmtOffset\":23400,\"gmtOffsetName\":\"UTC+06:30\",\"abbreviation\":\"MMT\",\"tzName\":\"Myanmar Standard Time\"}]', '{\"kr\":\"미얀마\",\"br\":\"Myanmar\",\"pt\":\"Myanmar\",\"nl\":\"Myanmar\",\"hr\":\"Mijanmar\",\"fa\":\"میانمار\",\"de\":\"Myanmar\",\"es\":\"Myanmar\",\"fr\":\"Myanmar\",\"ja\":\"ミャンマー\",\"it\":\"Birmania\",\"cn\":\"缅甸\"}', 22.00000000, 98.00000000, '🇲🇲', 'U+1F1F2 U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q836'),
(152, 'Namibia', 'NAM', '516', 'NA', '264', 'Windhoek', 'NAD', '$', '.na', 'Namibia', 'Africa', 'Southern Africa', '[{\"zoneName\":\"Africa\\/Windhoek\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"WAST\",\"tzName\":\"West Africa Summer Time\"}]', '{\"kr\":\"나미비아\",\"br\":\"Namíbia\",\"pt\":\"Namíbia\",\"nl\":\"Namibië\",\"hr\":\"Namibija\",\"fa\":\"نامیبیا\",\"de\":\"Namibia\",\"es\":\"Namibia\",\"fr\":\"Namibie\",\"ja\":\"ナミビア\",\"it\":\"Namibia\",\"cn\":\"纳米比亚\"}', -22.00000000, 17.00000000, '🇳🇦', 'U+1F1F3 U+1F1E6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1030'),
(153, 'Nauru', 'NRU', '520', 'NR', '674', 'Yaren', 'AUD', '$', '.nr', 'Nauru', 'Oceania', 'Micronesia', '[{\"zoneName\":\"Pacific\\/Nauru\",\"gmtOffset\":43200,\"gmtOffsetName\":\"UTC+12:00\",\"abbreviation\":\"NRT\",\"tzName\":\"Nauru Time\"}]', '{\"kr\":\"나우루\",\"br\":\"Nauru\",\"pt\":\"Nauru\",\"nl\":\"Nauru\",\"hr\":\"Nauru\",\"fa\":\"نائورو\",\"de\":\"Nauru\",\"es\":\"Nauru\",\"fr\":\"Nauru\",\"ja\":\"ナウル\",\"it\":\"Nauru\",\"cn\":\"瑙鲁\"}', -0.53333333, 166.91666666, '🇳🇷', 'U+1F1F3 U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q697'),
(154, 'Nepal', 'NPL', '524', 'NP', '977', 'Kathmandu', 'NPR', '₨', '.np', 'नपल', 'Asia', 'Southern Asia', '[{\"zoneName\":\"Asia\\/Kathmandu\",\"gmtOffset\":20700,\"gmtOffsetName\":\"UTC+05:45\",\"abbreviation\":\"NPT\",\"tzName\":\"Nepal Time\"}]', '{\"kr\":\"네팔\",\"br\":\"Nepal\",\"pt\":\"Nepal\",\"nl\":\"Nepal\",\"hr\":\"Nepal\",\"fa\":\"نپال\",\"de\":\"Népal\",\"es\":\"Nepal\",\"fr\":\"Népal\",\"ja\":\"ネパール\",\"it\":\"Nepal\",\"cn\":\"尼泊尔\"}', 28.00000000, 84.00000000, '🇳🇵', 'U+1F1F3 U+1F1F5', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q837'),
(155, 'Bonaire, Sint Eustatius and Saba', 'BES', '535', 'BQ', '599', 'Kralendijk', 'USD', '$', '.an', 'Caribisch Nederland', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Anguilla\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"보네르 섬\",\"br\":\"Bonaire\",\"pt\":\"Bonaire\",\"fa\":\"بونیر\",\"de\":\"Bonaire, Sint Eustatius und Saba\",\"fr\":\"Bonaire, Saint-Eustache et Saba\",\"it\":\"Bonaire, Saint-Eustache e Saba\",\"cn\":\"博内尔岛、圣尤斯特歇斯和萨巴岛\"}', 12.15000000, -68.26666700, '🇧🇶', 'U+1F1E7 U+1F1F6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q27561'),
(156, 'Netherlands', 'NLD', '528', 'NL', '31', 'Amsterdam', 'EUR', '€', '.nl', 'Nederland', 'Europe', 'Western Europe', '[{\"zoneName\":\"Europe\\/Amsterdam\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"네덜란드 \",\"br\":\"Holanda\",\"pt\":\"Países Baixos\",\"nl\":\"Nederland\",\"hr\":\"Nizozemska\",\"fa\":\"پادشاهی هلند\",\"de\":\"Niederlande\",\"es\":\"Países Bajos\",\"fr\":\"Pays-Bas\",\"ja\":\"オランダ\",\"it\":\"Paesi Bassi\",\"cn\":\"荷兰\"}', 52.50000000, 5.75000000, '🇳🇱', 'U+1F1F3 U+1F1F1', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q55'),
(157, 'New Caledonia', 'NCL', '540', 'NC', '687', 'Noumea', 'XPF', '₣', '.nc', 'Nouvelle-Calédonie', 'Oceania', 'Melanesia', '[{\"zoneName\":\"Pacific\\/Noumea\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"NCT\",\"tzName\":\"New Caledonia Time\"}]', '{\"kr\":\"누벨칼레도니\",\"br\":\"Nova Caledônia\",\"pt\":\"Nova Caledónia\",\"nl\":\"Nieuw-Caledonië\",\"hr\":\"Nova Kaledonija\",\"fa\":\"کالدونیای جدید\",\"de\":\"Neukaledonien\",\"es\":\"Nueva Caledonia\",\"fr\":\"Nouvelle-Calédonie\",\"ja\":\"ニューカレドニア\",\"it\":\"Nuova Caledonia\",\"cn\":\"新喀里多尼亚\"}', -21.50000000, 165.50000000, '🇳🇨', 'U+1F1F3 U+1F1E8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(158, 'New Zealand', 'NZL', '554', 'NZ', '64', 'Wellington', 'NZD', '$', '.nz', 'New Zealand', 'Oceania', 'Australia and New Zealand', '[{\"zoneName\":\"Pacific\\/Auckland\",\"gmtOffset\":46800,\"gmtOffsetName\":\"UTC+13:00\",\"abbreviation\":\"NZDT\",\"tzName\":\"New Zealand Daylight Time\"},{\"zoneName\":\"Pacific\\/Chatham\",\"gmtOffset\":49500,\"gmtOffsetName\":\"UTC+13:45\",\"abbreviation\":\"CHAST\",\"tzName\":\"Chatham Standard Time\"}]', '{\"kr\":\"뉴질랜드\",\"br\":\"Nova Zelândia\",\"pt\":\"Nova Zelândia\",\"nl\":\"Nieuw-Zeeland\",\"hr\":\"Novi Zeland\",\"fa\":\"نیوزیلند\",\"de\":\"Neuseeland\",\"es\":\"Nueva Zelanda\",\"fr\":\"Nouvelle-Zélande\",\"ja\":\"ニュージーランド\",\"it\":\"Nuova Zelanda\",\"cn\":\"新西兰\"}', -41.00000000, 174.00000000, '🇳🇿', 'U+1F1F3 U+1F1FF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q664'),
(159, 'Nicaragua', 'NIC', '558', 'NI', '505', 'Managua', 'NIO', 'C$', '.ni', 'Nicaragua', 'Americas', 'Central America', '[{\"zoneName\":\"America\\/Managua\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"}]', '{\"kr\":\"니카라과\",\"br\":\"Nicarágua\",\"pt\":\"Nicarágua\",\"nl\":\"Nicaragua\",\"hr\":\"Nikaragva\",\"fa\":\"نیکاراگوئه\",\"de\":\"Nicaragua\",\"es\":\"Nicaragua\",\"fr\":\"Nicaragua\",\"ja\":\"ニカラグア\",\"it\":\"Nicaragua\",\"cn\":\"尼加拉瓜\"}', 13.00000000, -85.00000000, '🇳🇮', 'U+1F1F3 U+1F1EE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q811'),
(160, 'Niger', 'NER', '562', 'NE', '227', 'Niamey', 'XOF', 'CFA', '.ne', 'Niger', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Niamey\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WAT\",\"tzName\":\"West Africa Time\"}]', '{\"kr\":\"니제르\",\"br\":\"Níger\",\"pt\":\"Níger\",\"nl\":\"Niger\",\"hr\":\"Niger\",\"fa\":\"نیجر\",\"de\":\"Niger\",\"es\":\"Níger\",\"fr\":\"Niger\",\"ja\":\"ニジェール\",\"it\":\"Niger\",\"cn\":\"尼日尔\"}', 16.00000000, 8.00000000, '🇳🇪', 'U+1F1F3 U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1032'),
(161, 'Nigeria', 'NGA', '566', 'NG', '234', 'Abuja', 'NGN', '₦', '.ng', 'Nigeria', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Lagos\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WAT\",\"tzName\":\"West Africa Time\"}]', '{\"kr\":\"나이지리아\",\"br\":\"Nigéria\",\"pt\":\"Nigéria\",\"nl\":\"Nigeria\",\"hr\":\"Nigerija\",\"fa\":\"نیجریه\",\"de\":\"Nigeria\",\"es\":\"Nigeria\",\"fr\":\"Nigéria\",\"ja\":\"ナイジェリア\",\"it\":\"Nigeria\",\"cn\":\"尼日利亚\"}', 10.00000000, 8.00000000, '🇳🇬', 'U+1F1F3 U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1033'),
(162, 'Niue', 'NIU', '570', 'NU', '683', 'Alofi', 'NZD', '$', '.nu', 'Niuē', 'Oceania', 'Polynesia', '[{\"zoneName\":\"Pacific\\/Niue\",\"gmtOffset\":-39600,\"gmtOffsetName\":\"UTC-11:00\",\"abbreviation\":\"NUT\",\"tzName\":\"Niue Time\"}]', '{\"kr\":\"니우에\",\"br\":\"Niue\",\"pt\":\"Niue\",\"nl\":\"Niue\",\"hr\":\"Niue\",\"fa\":\"نیووی\",\"de\":\"Niue\",\"es\":\"Niue\",\"fr\":\"Niue\",\"ja\":\"ニウエ\",\"it\":\"Niue\",\"cn\":\"纽埃\"}', -19.03333333, -169.86666666, '🇳🇺', 'U+1F1F3 U+1F1FA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q34020'),
(163, 'Norfolk Island', 'NFK', '574', 'NF', '672', 'Kingston', 'AUD', '$', '.nf', 'Norfolk Island', 'Oceania', 'Australia and New Zealand', '[{\"zoneName\":\"Pacific\\/Norfolk\",\"gmtOffset\":43200,\"gmtOffsetName\":\"UTC+12:00\",\"abbreviation\":\"NFT\",\"tzName\":\"Norfolk Time\"}]', '{\"kr\":\"노퍽 섬\",\"br\":\"Ilha Norfolk\",\"pt\":\"Ilha Norfolk\",\"nl\":\"Norfolkeiland\",\"hr\":\"Otok Norfolk\",\"fa\":\"جزیره نورفک\",\"de\":\"Norfolkinsel\",\"es\":\"Isla de Norfolk\",\"fr\":\"Île de Norfolk\",\"ja\":\"ノーフォーク島\",\"it\":\"Isola Norfolk\",\"cn\":\"诺福克岛\"}', -29.03333333, 167.95000000, '🇳🇫', 'U+1F1F3 U+1F1EB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(164, 'Northern Mariana Islands', 'MNP', '580', 'MP', '+1-670', 'Saipan', 'USD', '$', '.mp', 'Northern Mariana Islands', 'Oceania', 'Micronesia', '[{\"zoneName\":\"Pacific\\/Saipan\",\"gmtOffset\":36000,\"gmtOffsetName\":\"UTC+10:00\",\"abbreviation\":\"ChST\",\"tzName\":\"Chamorro Standard Time\"}]', '{\"kr\":\"북마리아나 제도\",\"br\":\"Ilhas Marianas\",\"pt\":\"Ilhas Marianas\",\"nl\":\"Noordelijke Marianeneilanden\",\"hr\":\"Sjevernomarijanski otoci\",\"fa\":\"جزایر ماریانای شمالی\",\"de\":\"Nördliche Marianen\",\"es\":\"Islas Marianas del Norte\",\"fr\":\"Îles Mariannes du Nord\",\"ja\":\"北マリアナ諸島\",\"it\":\"Isole Marianne Settentrionali\",\"cn\":\"北马里亚纳群岛\"}', 15.20000000, 145.75000000, '🇲🇵', 'U+1F1F2 U+1F1F5', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(165, 'Norway', 'NOR', '578', 'NO', '47', 'Oslo', 'NOK', 'kr', '.no', 'Norge', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/Oslo\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"노르웨이\",\"br\":\"Noruega\",\"pt\":\"Noruega\",\"nl\":\"Noorwegen\",\"hr\":\"Norveška\",\"fa\":\"نروژ\",\"de\":\"Norwegen\",\"es\":\"Noruega\",\"fr\":\"Norvège\",\"ja\":\"ノルウェー\",\"it\":\"Norvegia\",\"cn\":\"挪威\"}', 62.00000000, 10.00000000, '🇳🇴', 'U+1F1F3 U+1F1F4', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q20'),
(166, 'Oman', 'OMN', '512', 'OM', '968', 'Muscat', 'OMR', '.ع.ر', '.om', 'عمان', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Muscat\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"GST\",\"tzName\":\"Gulf Standard Time\"}]', '{\"kr\":\"오만\",\"br\":\"Omã\",\"pt\":\"Omã\",\"nl\":\"Oman\",\"hr\":\"Oman\",\"fa\":\"عمان\",\"de\":\"Oman\",\"es\":\"Omán\",\"fr\":\"Oman\",\"ja\":\"オマーン\",\"it\":\"oman\",\"cn\":\"阿曼\"}', 21.00000000, 57.00000000, '🇴🇲', 'U+1F1F4 U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q842'),
(167, 'Pakistan', 'PAK', '586', 'PK', '92', 'Islamabad', 'PKR', '₨', '.pk', 'Pakistan', 'Asia', 'Southern Asia', '[{\"zoneName\":\"Asia\\/Karachi\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"PKT\",\"tzName\":\"Pakistan Standard Time\"}]', '{\"kr\":\"파키스탄\",\"br\":\"Paquistão\",\"pt\":\"Paquistão\",\"nl\":\"Pakistan\",\"hr\":\"Pakistan\",\"fa\":\"پاکستان\",\"de\":\"Pakistan\",\"es\":\"Pakistán\",\"fr\":\"Pakistan\",\"ja\":\"パキスタン\",\"it\":\"Pakistan\",\"cn\":\"巴基斯坦\"}', 30.00000000, 70.00000000, '🇵🇰', 'U+1F1F5 U+1F1F0', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q843'),
(168, 'Palau', 'PLW', '585', 'PW', '680', 'Melekeok', 'USD', '$', '.pw', 'Palau', 'Oceania', 'Micronesia', '[{\"zoneName\":\"Pacific\\/Palau\",\"gmtOffset\":32400,\"gmtOffsetName\":\"UTC+09:00\",\"abbreviation\":\"PWT\",\"tzName\":\"Palau Time\"}]', '{\"kr\":\"팔라우\",\"br\":\"Palau\",\"pt\":\"Palau\",\"nl\":\"Palau\",\"hr\":\"Palau\",\"fa\":\"پالائو\",\"de\":\"Palau\",\"es\":\"Palau\",\"fr\":\"Palaos\",\"ja\":\"パラオ\",\"it\":\"Palau\",\"cn\":\"帕劳\"}', 7.50000000, 134.50000000, '🇵🇼', 'U+1F1F5 U+1F1FC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q695'),
(169, 'Palestinian Territory Occupied', 'PSE', '275', 'PS', '970', 'East Jerusalem', 'ILS', '₪', '.ps', 'فلسطين', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Gaza\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"},{\"zoneName\":\"Asia\\/Hebron\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"팔레스타인 영토\",\"br\":\"Palestina\",\"pt\":\"Palestina\",\"nl\":\"Palestijnse gebieden\",\"hr\":\"Palestina\",\"fa\":\"فلسطین\",\"de\":\"Palästina\",\"es\":\"Palestina\",\"fr\":\"Palestine\",\"ja\":\"パレスチナ\",\"it\":\"Palestina\",\"cn\":\"巴勒斯坦\"}', 31.90000000, 35.20000000, '🇵🇸', 'U+1F1F5 U+1F1F8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(170, 'Panama', 'PAN', '591', 'PA', '507', 'Panama City', 'PAB', 'B/.', '.pa', 'Panamá', 'Americas', 'Central America', '[{\"zoneName\":\"America\\/Panama\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"}]', '{\"kr\":\"파나마\",\"br\":\"Panamá\",\"pt\":\"Panamá\",\"nl\":\"Panama\",\"hr\":\"Panama\",\"fa\":\"پاناما\",\"de\":\"Panama\",\"es\":\"Panamá\",\"fr\":\"Panama\",\"ja\":\"パナマ\",\"it\":\"Panama\",\"cn\":\"巴拿马\"}', 9.00000000, -80.00000000, '🇵🇦', 'U+1F1F5 U+1F1E6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q804'),
(171, 'Papua new Guinea', 'PNG', '598', 'PG', '675', 'Port Moresby', 'PGK', 'K', '.pg', 'Papua Niugini', 'Oceania', 'Melanesia', '[{\"zoneName\":\"Pacific\\/Bougainville\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"BST\",\"tzName\":\"Bougainville Standard Time[6\"},{\"zoneName\":\"Pacific\\/Port_Moresby\",\"gmtOffset\":36000,\"gmtOffsetName\":\"UTC+10:00\",\"abbreviation\":\"PGT\",\"tzName\":\"Papua New Guinea Time\"}]', '{\"kr\":\"파푸아뉴기니\",\"br\":\"Papua Nova Guiné\",\"pt\":\"Papua Nova Guiné\",\"nl\":\"Papoea-Nieuw-Guinea\",\"hr\":\"Papua Nova Gvineja\",\"fa\":\"پاپوآ گینه نو\",\"de\":\"Papua-Neuguinea\",\"es\":\"Papúa Nueva Guinea\",\"fr\":\"Papouasie-Nouvelle-Guinée\",\"ja\":\"パプアニューギニア\",\"it\":\"Papua Nuova Guinea\",\"cn\":\"巴布亚新几内亚\"}', -6.00000000, 147.00000000, '🇵🇬', 'U+1F1F5 U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q691'),
(172, 'Paraguay', 'PRY', '600', 'PY', '595', 'Asuncion', 'PYG', '₲', '.py', 'Paraguay', 'Americas', 'South America', '[{\"zoneName\":\"America\\/Asuncion\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"PYST\",\"tzName\":\"Paraguay Summer Time\"}]', '{\"kr\":\"파라과이\",\"br\":\"Paraguai\",\"pt\":\"Paraguai\",\"nl\":\"Paraguay\",\"hr\":\"Paragvaj\",\"fa\":\"پاراگوئه\",\"de\":\"Paraguay\",\"es\":\"Paraguay\",\"fr\":\"Paraguay\",\"ja\":\"パラグアイ\",\"it\":\"Paraguay\",\"cn\":\"巴拉圭\"}', -23.00000000, -58.00000000, '🇵🇾', 'U+1F1F5 U+1F1FE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q733'),
(173, 'Peru', 'PER', '604', 'PE', '51', 'Lima', 'PEN', 'S/.', '.pe', 'Perú', 'Americas', 'South America', '[{\"zoneName\":\"America\\/Lima\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"PET\",\"tzName\":\"Peru Time\"}]', '{\"kr\":\"페루\",\"br\":\"Peru\",\"pt\":\"Peru\",\"nl\":\"Peru\",\"hr\":\"Peru\",\"fa\":\"پرو\",\"de\":\"Peru\",\"es\":\"Perú\",\"fr\":\"Pérou\",\"ja\":\"ペルー\",\"it\":\"Perù\",\"cn\":\"秘鲁\"}', -10.00000000, -76.00000000, '🇵🇪', 'U+1F1F5 U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q419'),
(174, 'Philippines', 'PHL', '608', 'PH', '63', 'Manila', 'PHP', '₱', '.ph', 'Pilipinas', 'Asia', 'South-Eastern Asia', '[{\"zoneName\":\"Asia\\/Manila\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"PHT\",\"tzName\":\"Philippine Time\"}]', '{\"kr\":\"필리핀\",\"br\":\"Filipinas\",\"pt\":\"Filipinas\",\"nl\":\"Filipijnen\",\"hr\":\"Filipini\",\"fa\":\"جزایر الندفیلیپین\",\"de\":\"Philippinen\",\"es\":\"Filipinas\",\"fr\":\"Philippines\",\"ja\":\"フィリピン\",\"it\":\"Filippine\",\"cn\":\"菲律宾\"}', 13.00000000, 122.00000000, '🇵🇭', 'U+1F1F5 U+1F1ED', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q928'),
(175, 'Pitcairn Island', 'PCN', '612', 'PN', '870', 'Adamstown', 'NZD', '$', '.pn', 'Pitcairn Islands', 'Oceania', 'Polynesia', '[{\"zoneName\":\"Pacific\\/Pitcairn\",\"gmtOffset\":-28800,\"gmtOffsetName\":\"UTC-08:00\",\"abbreviation\":\"PST\",\"tzName\":\"Pacific Standard Time (North America\"}]', '{\"kr\":\"핏케언 제도\",\"br\":\"Ilhas Pitcairn\",\"pt\":\"Ilhas Picárnia\",\"nl\":\"Pitcairneilanden\",\"hr\":\"Pitcairnovo otočje\",\"fa\":\"پیتکرن\",\"de\":\"Pitcairn\",\"es\":\"Islas Pitcairn\",\"fr\":\"Îles Pitcairn\",\"ja\":\"ピトケアン\",\"it\":\"Isole Pitcairn\",\"cn\":\"皮特凯恩群岛\"}', -25.06666666, -130.10000000, '🇵🇳', 'U+1F1F5 U+1F1F3', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(176, 'Poland', 'POL', '616', 'PL', '48', 'Warsaw', 'PLN', 'zł', '.pl', 'Polska', 'Europe', 'Eastern Europe', '[{\"zoneName\":\"Europe\\/Warsaw\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"폴란드\",\"br\":\"Polônia\",\"pt\":\"Polónia\",\"nl\":\"Polen\",\"hr\":\"Poljska\",\"fa\":\"لهستان\",\"de\":\"Polen\",\"es\":\"Polonia\",\"fr\":\"Pologne\",\"ja\":\"ポーランド\",\"it\":\"Polonia\",\"cn\":\"波兰\"}', 52.00000000, 20.00000000, '🇵🇱', 'U+1F1F5 U+1F1F1', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q36'),
(177, 'Portugal', 'PRT', '620', 'PT', '351', 'Lisbon', 'EUR', '€', '.pt', 'Portugal', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Atlantic\\/Azores\",\"gmtOffset\":-3600,\"gmtOffsetName\":\"UTC-01:00\",\"abbreviation\":\"AZOT\",\"tzName\":\"Azores Standard Time\"},{\"zoneName\":\"Atlantic\\/Madeira\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"WET\",\"tzName\":\"Western European Time\"},{\"zoneName\":\"Europe\\/Lisbon\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"WET\",\"tzName\":\"Western European Time\"}]', '{\"kr\":\"포르투갈\",\"br\":\"Portugal\",\"pt\":\"Portugal\",\"nl\":\"Portugal\",\"hr\":\"Portugal\",\"fa\":\"پرتغال\",\"de\":\"Portugal\",\"es\":\"Portugal\",\"fr\":\"Portugal\",\"ja\":\"ポルトガル\",\"it\":\"Portogallo\",\"cn\":\"葡萄牙\"}', 39.50000000, -8.00000000, '🇵🇹', 'U+1F1F5 U+1F1F9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q45'),
(178, 'Puerto Rico', 'PRI', '630', 'PR', '+1-787 and 1-939', 'San Juan', 'USD', '$', '.pr', 'Puerto Rico', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Puerto_Rico\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"푸에르토리코\",\"br\":\"Porto Rico\",\"pt\":\"Porto Rico\",\"nl\":\"Puerto Rico\",\"hr\":\"Portoriko\",\"fa\":\"پورتو ریکو\",\"de\":\"Puerto Rico\",\"es\":\"Puerto Rico\",\"fr\":\"Porto Rico\",\"ja\":\"プエルトリコ\",\"it\":\"Porto Rico\",\"cn\":\"波多黎各\"}', 18.25000000, -66.50000000, '🇵🇷', 'U+1F1F5 U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(179, 'Qatar', 'QAT', '634', 'QA', '974', 'Doha', 'QAR', 'ق.ر', '.qa', 'قطر', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Qatar\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"AST\",\"tzName\":\"Arabia Standard Time\"}]', '{\"kr\":\"카타르\",\"br\":\"Catar\",\"pt\":\"Catar\",\"nl\":\"Qatar\",\"hr\":\"Katar\",\"fa\":\"قطر\",\"de\":\"Katar\",\"es\":\"Catar\",\"fr\":\"Qatar\",\"ja\":\"カタール\",\"it\":\"Qatar\",\"cn\":\"卡塔尔\"}', 25.50000000, 51.25000000, '🇶🇦', 'U+1F1F6 U+1F1E6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q846'),
(180, 'Reunion', 'REU', '638', 'RE', '262', 'Saint-Denis', 'EUR', '€', '.re', 'La Réunion', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Indian\\/Reunion\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"RET\",\"tzName\":\"R\\u00e9union Time\"}]', '{\"kr\":\"레위니옹\",\"br\":\"Reunião\",\"pt\":\"Reunião\",\"nl\":\"Réunion\",\"hr\":\"Réunion\",\"fa\":\"رئونیون\",\"de\":\"Réunion\",\"es\":\"Reunión\",\"fr\":\"Réunion\",\"ja\":\"レユニオン\",\"it\":\"Riunione\",\"cn\":\"留尼汪岛\"}', -21.15000000, 55.50000000, '🇷🇪', 'U+1F1F7 U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(181, 'Romania', 'ROU', '642', 'RO', '40', 'Bucharest', 'RON', 'lei', '.ro', 'România', 'Europe', 'Eastern Europe', '[{\"zoneName\":\"Europe\\/Bucharest\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"루마니아\",\"br\":\"Romênia\",\"pt\":\"Roménia\",\"nl\":\"Roemenië\",\"hr\":\"Rumunjska\",\"fa\":\"رومانی\",\"de\":\"Rumänien\",\"es\":\"Rumania\",\"fr\":\"Roumanie\",\"ja\":\"ルーマニア\",\"it\":\"Romania\",\"cn\":\"罗马尼亚\"}', 46.00000000, 25.00000000, '🇷🇴', 'U+1F1F7 U+1F1F4', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q218'),
(182, 'Russia', 'RUS', '643', 'RU', '7', 'Moscow', 'RUB', '₽', '.ru', 'Россия', 'Europe', 'Eastern Europe', '[{\"zoneName\":\"Asia\\/Anadyr\",\"gmtOffset\":43200,\"gmtOffsetName\":\"UTC+12:00\",\"abbreviation\":\"ANAT\",\"tzName\":\"Anadyr Time[4\"},{\"zoneName\":\"Asia\\/Barnaul\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"KRAT\",\"tzName\":\"Krasnoyarsk Time\"},{\"zoneName\":\"Asia\\/Chita\",\"gmtOffset\":32400,\"gmtOffsetName\":\"UTC+09:00\",\"abbreviation\":\"YAKT\",\"tzName\":\"Yakutsk Time\"},{\"zoneName\":\"Asia\\/Irkutsk\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"IRKT\",\"tzName\":\"Irkutsk Time\"},{\"zoneName\":\"Asia\\/Kamchatka\",\"gmtOffset\":43200,\"gmtOffsetName\":\"UTC+12:00\",\"abbreviation\":\"PETT\",\"tzName\":\"Kamchatka Time\"},{\"zoneName\":\"Asia\\/Khandyga\",\"gmtOffset\":32400,\"gmtOffsetName\":\"UTC+09:00\",\"abbreviation\":\"YAKT\",\"tzName\":\"Yakutsk Time\"},{\"zoneName\":\"Asia\\/Krasnoyarsk\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"KRAT\",\"tzName\":\"Krasnoyarsk Time\"},{\"zoneName\":\"Asia\\/Magadan\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"MAGT\",\"tzName\":\"Magadan Time\"},{\"zoneName\":\"Asia\\/Novokuznetsk\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"KRAT\",\"tzName\":\"Krasnoyarsk Time\"},{\"zoneName\":\"Asia\\/Novosibirsk\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"NOVT\",\"tzName\":\"Novosibirsk Time\"},{\"zoneName\":\"Asia\\/Omsk\",\"gmtOffset\":21600,\"gmtOffsetName\":\"UTC+06:00\",\"abbreviation\":\"OMST\",\"tzName\":\"Omsk Time\"},{\"zoneName\":\"Asia\\/Sakhalin\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"SAKT\",\"tzName\":\"Sakhalin Island Time\"},{\"zoneName\":\"Asia\\/Srednekolymsk\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"SRET\",\"tzName\":\"Srednekolymsk Time\"},{\"zoneName\":\"Asia\\/Tomsk\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"MSD+3\",\"tzName\":\"Moscow Daylight Time+3\"},{\"zoneName\":\"Asia\\/Ust-Nera\",\"gmtOffset\":36000,\"gmtOffsetName\":\"UTC+10:00\",\"abbreviation\":\"VLAT\",\"tzName\":\"Vladivostok Time\"},{\"zoneName\":\"Asia\\/Vladivostok\",\"gmtOffset\":36000,\"gmtOffsetName\":\"UTC+10:00\",\"abbreviation\":\"VLAT\",\"tzName\":\"Vladivostok Time\"},{\"zoneName\":\"Asia\\/Yakutsk\",\"gmtOffset\":32400,\"gmtOffsetName\":\"UTC+09:00\",\"abbreviation\":\"YAKT\",\"tzName\":\"Yakutsk Time\"},{\"zoneName\":\"Asia\\/Yekaterinburg\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"YEKT\",\"tzName\":\"Yekaterinburg Time\"},{\"zoneName\":\"Europe\\/Astrakhan\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"SAMT\",\"tzName\":\"Samara Time\"},{\"zoneName\":\"Europe\\/Kaliningrad\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"},{\"zoneName\":\"Europe\\/Kirov\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"MSK\",\"tzName\":\"Moscow Time\"},{\"zoneName\":\"Europe\\/Moscow\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"MSK\",\"tzName\":\"Moscow Time\"},{\"zoneName\":\"Europe\\/Samara\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"SAMT\",\"tzName\":\"Samara Time\"},{\"zoneName\":\"Europe\\/Saratov\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"MSD\",\"tzName\":\"Moscow Daylight Time+4\"},{\"zoneName\":\"Europe\\/Ulyanovsk\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"SAMT\",\"tzName\":\"Samara Time\"},{\"zoneName\":\"Europe\\/Volgograd\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"MSK\",\"tzName\":\"Moscow Standard Time\"}]', '{\"kr\":\"러시아\",\"br\":\"Rússia\",\"pt\":\"Rússia\",\"nl\":\"Rusland\",\"hr\":\"Rusija\",\"fa\":\"روسیه\",\"de\":\"Russland\",\"es\":\"Rusia\",\"fr\":\"Russie\",\"ja\":\"ロシア連邦\",\"it\":\"Russia\",\"cn\":\"俄罗斯联邦\"}', 60.00000000, 100.00000000, '🇷🇺', 'U+1F1F7 U+1F1FA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q159'),
(183, 'Rwanda', 'RWA', '646', 'RW', '250', 'Kigali', 'RWF', 'FRw', '.rw', 'Rwanda', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Kigali\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"CAT\",\"tzName\":\"Central Africa Time\"}]', '{\"kr\":\"르완다\",\"br\":\"Ruanda\",\"pt\":\"Ruanda\",\"nl\":\"Rwanda\",\"hr\":\"Ruanda\",\"fa\":\"رواندا\",\"de\":\"Ruanda\",\"es\":\"Ruanda\",\"fr\":\"Rwanda\",\"ja\":\"ルワンダ\",\"it\":\"Ruanda\",\"cn\":\"卢旺达\"}', -2.00000000, 30.00000000, '🇷🇼', 'U+1F1F7 U+1F1FC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1037'),
(184, 'Saint Helena', 'SHN', '654', 'SH', '290', 'Jamestown', 'SHP', '£', '.sh', 'Saint Helena', 'Africa', 'Western Africa', '[{\"zoneName\":\"Atlantic\\/St_Helena\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"세인트헬레나\",\"br\":\"Santa Helena\",\"pt\":\"Santa Helena\",\"nl\":\"Sint-Helena\",\"hr\":\"Sveta Helena\",\"fa\":\"سنت هلنا، اسنشن و تریستان دا کونا\",\"de\":\"Sankt Helena\",\"es\":\"Santa Helena\",\"fr\":\"Sainte-Hélène\",\"ja\":\"セントヘレナ・アセンションおよびトリスタンダクーニャ\",\"it\":\"Sant\'Elena\",\"cn\":\"圣赫勒拿\"}', -15.95000000, -5.70000000, '🇸🇭', 'U+1F1F8 U+1F1ED', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(185, 'Saint Kitts And Nevis', 'KNA', '659', 'KN', '+1-869', 'Basseterre', 'XCD', '$', '.kn', 'Saint Kitts and Nevis', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/St_Kitts\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"세인트키츠 네비스\",\"br\":\"São Cristóvão e Neves\",\"pt\":\"São Cristóvão e Neves\",\"nl\":\"Saint Kitts en Nevis\",\"hr\":\"Sveti Kristof i Nevis\",\"fa\":\"سنت کیتس و نویس\",\"de\":\"St. Kitts und Nevis\",\"es\":\"San Cristóbal y Nieves\",\"fr\":\"Saint-Christophe-et-Niévès\",\"ja\":\"セントクリストファー・ネイビス\",\"it\":\"Saint Kitts e Nevis\",\"cn\":\"圣基茨和尼维斯\"}', 17.33333333, -62.75000000, '🇰🇳', 'U+1F1F0 U+1F1F3', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q763'),
(186, 'Saint Lucia', 'LCA', '662', 'LC', '+1-758', 'Castries', 'XCD', '$', '.lc', 'Saint Lucia', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/St_Lucia\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"세인트루시아\",\"br\":\"Santa Lúcia\",\"pt\":\"Santa Lúcia\",\"nl\":\"Saint Lucia\",\"hr\":\"Sveta Lucija\",\"fa\":\"سنت لوسیا\",\"de\":\"Saint Lucia\",\"es\":\"Santa Lucía\",\"fr\":\"Saint-Lucie\",\"ja\":\"セントルシア\",\"it\":\"Santa Lucia\",\"cn\":\"圣卢西亚\"}', 13.88333333, -60.96666666, '🇱🇨', 'U+1F1F1 U+1F1E8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q760'),
(187, 'Saint Pierre and Miquelon', 'SPM', '666', 'PM', '508', 'Saint-Pierre', 'EUR', '€', '.pm', 'Saint-Pierre-et-Miquelon', 'Americas', 'Northern America', '[{\"zoneName\":\"America\\/Miquelon\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"PMDT\",\"tzName\":\"Pierre & Miquelon Daylight Time\"}]', '{\"kr\":\"생피에르 미클롱\",\"br\":\"Saint-Pierre e Miquelon\",\"pt\":\"São Pedro e Miquelon\",\"nl\":\"Saint Pierre en Miquelon\",\"hr\":\"Sveti Petar i Mikelon\",\"fa\":\"سن پیر و میکلن\",\"de\":\"Saint-Pierre und Miquelon\",\"es\":\"San Pedro y Miquelón\",\"fr\":\"Saint-Pierre-et-Miquelon\",\"ja\":\"サンピエール島・ミクロン島\",\"it\":\"Saint-Pierre e Miquelon\",\"cn\":\"圣皮埃尔和密克隆\"}', 46.83333333, -56.33333333, '🇵🇲', 'U+1F1F5 U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(188, 'Saint Vincent And The Grenadines', 'VCT', '670', 'VC', '+1-784', 'Kingstown', 'XCD', '$', '.vc', 'Saint Vincent and the Grenadines', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/St_Vincent\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"세인트빈센트 그레나딘\",\"br\":\"São Vicente e Granadinas\",\"pt\":\"São Vicente e Granadinas\",\"nl\":\"Saint Vincent en de Grenadines\",\"hr\":\"Sveti Vincent i Grenadini\",\"fa\":\"سنت وینسنت و گرنادین‌ها\",\"de\":\"Saint Vincent und die Grenadinen\",\"es\":\"San Vicente y Granadinas\",\"fr\":\"Saint-Vincent-et-les-Grenadines\",\"ja\":\"セントビンセントおよびグレナディーン諸島\",\"it\":\"Saint Vincent e Grenadine\",\"cn\":\"圣文森特和格林纳丁斯\"}', 13.25000000, -61.20000000, '🇻🇨', 'U+1F1FB U+1F1E8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q757'),
(189, 'Saint-Barthelemy', 'BLM', '652', 'BL', '590', 'Gustavia', 'EUR', '€', '.bl', 'Saint-Barthélemy', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/St_Barthelemy\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"생바르텔레미\",\"br\":\"São Bartolomeu\",\"pt\":\"São Bartolomeu\",\"nl\":\"Saint Barthélemy\",\"hr\":\"Saint Barthélemy\",\"fa\":\"سن-بارتلمی\",\"de\":\"Saint-Barthélemy\",\"es\":\"San Bartolomé\",\"fr\":\"Saint-Barthélemy\",\"ja\":\"サン・バルテルミー\",\"it\":\"Antille Francesi\",\"cn\":\"圣巴泰勒米\"}', 18.50000000, -63.41666666, '🇧🇱', 'U+1F1E7 U+1F1F1', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(190, 'Saint-Martin (French part)', 'MAF', '663', 'MF', '590', 'Marigot', 'EUR', '€', '.mf', 'Saint-Martin', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Marigot\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"세인트마틴 섬\",\"br\":\"Saint Martin\",\"pt\":\"Ilha São Martinho\",\"nl\":\"Saint-Martin\",\"hr\":\"Sveti Martin\",\"fa\":\"سینت مارتن\",\"de\":\"Saint Martin\",\"es\":\"Saint Martin\",\"fr\":\"Saint-Martin\",\"ja\":\"サン・マルタン（フランス領）\",\"it\":\"Saint Martin\",\"cn\":\"密克罗尼西亚\"}', 18.08333333, -63.95000000, '🇲🇫', 'U+1F1F2 U+1F1EB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(191, 'Samoa', 'WSM', '882', 'WS', '685', 'Apia', 'WST', 'SAT', '.ws', 'Samoa', 'Oceania', 'Polynesia', '[{\"zoneName\":\"Pacific\\/Apia\",\"gmtOffset\":50400,\"gmtOffsetName\":\"UTC+14:00\",\"abbreviation\":\"WST\",\"tzName\":\"West Samoa Time\"}]', '{\"kr\":\"사모아\",\"br\":\"Samoa\",\"pt\":\"Samoa\",\"nl\":\"Samoa\",\"hr\":\"Samoa\",\"fa\":\"ساموآ\",\"de\":\"Samoa\",\"es\":\"Samoa\",\"fr\":\"Samoa\",\"ja\":\"サモア\",\"it\":\"Samoa\",\"cn\":\"萨摩亚\"}', -13.58333333, -172.33333333, '🇼🇸', 'U+1F1FC U+1F1F8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q683'),
(192, 'San Marino', 'SMR', '674', 'SM', '378', 'San Marino', 'EUR', '€', '.sm', 'San Marino', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/San_Marino\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"산마리노\",\"br\":\"San Marino\",\"pt\":\"São Marinho\",\"nl\":\"San Marino\",\"hr\":\"San Marino\",\"fa\":\"سان مارینو\",\"de\":\"San Marino\",\"es\":\"San Marino\",\"fr\":\"Saint-Marin\",\"ja\":\"サンマリノ\",\"it\":\"San Marino\",\"cn\":\"圣马力诺\"}', 43.76666666, 12.41666666, '🇸🇲', 'U+1F1F8 U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q238'),
(193, 'Sao Tome and Principe', 'STP', '678', 'ST', '239', 'Sao Tome', 'STD', 'Db', '.st', 'São Tomé e Príncipe', 'Africa', 'Middle Africa', '[{\"zoneName\":\"Africa\\/Sao_Tome\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"상투메 프린시페\",\"br\":\"São Tomé e Príncipe\",\"pt\":\"São Tomé e Príncipe\",\"nl\":\"Sao Tomé en Principe\",\"hr\":\"Sveti Toma i Princip\",\"fa\":\"کواترو دو فرویرو\",\"de\":\"São Tomé und Príncipe\",\"es\":\"Santo Tomé y Príncipe\",\"fr\":\"Sao Tomé-et-Principe\",\"ja\":\"サントメ・プリンシペ\",\"it\":\"São Tomé e Príncipe\",\"cn\":\"圣多美和普林西比\"}', 1.00000000, 7.00000000, '🇸🇹', 'U+1F1F8 U+1F1F9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1039'),
(194, 'Saudi Arabia', 'SAU', '682', 'SA', '966', 'Riyadh', 'SAR', '﷼', '.sa', 'العربية السعودية', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Riyadh\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"AST\",\"tzName\":\"Arabia Standard Time\"}]', '{\"kr\":\"사우디아라비아\",\"br\":\"Arábia Saudita\",\"pt\":\"Arábia Saudita\",\"nl\":\"Saoedi-Arabië\",\"hr\":\"Saudijska Arabija\",\"fa\":\"عربستان سعودی\",\"de\":\"Saudi-Arabien\",\"es\":\"Arabia Saudí\",\"fr\":\"Arabie Saoudite\",\"ja\":\"サウジアラビア\",\"it\":\"Arabia Saudita\",\"cn\":\"沙特阿拉伯\"}', 25.00000000, 45.00000000, '🇸🇦', 'U+1F1F8 U+1F1E6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q851'),
(195, 'Senegal', 'SEN', '686', 'SN', '221', 'Dakar', 'XOF', 'CFA', '.sn', 'Sénégal', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Dakar\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"세네갈\",\"br\":\"Senegal\",\"pt\":\"Senegal\",\"nl\":\"Senegal\",\"hr\":\"Senegal\",\"fa\":\"سنگال\",\"de\":\"Senegal\",\"es\":\"Senegal\",\"fr\":\"Sénégal\",\"ja\":\"セネガル\",\"it\":\"Senegal\",\"cn\":\"塞内加尔\"}', 14.00000000, -14.00000000, '🇸🇳', 'U+1F1F8 U+1F1F3', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1041'),
(196, 'Serbia', 'SRB', '688', 'RS', '381', 'Belgrade', 'RSD', 'din', '.rs', 'Србија', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Belgrade\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"세르비아\",\"br\":\"Sérvia\",\"pt\":\"Sérvia\",\"nl\":\"Servië\",\"hr\":\"Srbija\",\"fa\":\"صربستان\",\"de\":\"Serbien\",\"es\":\"Serbia\",\"fr\":\"Serbie\",\"ja\":\"セルビア\",\"it\":\"Serbia\",\"cn\":\"塞尔维亚\"}', 44.00000000, 21.00000000, '🇷🇸', 'U+1F1F7 U+1F1F8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q403'),
(197, 'Seychelles', 'SYC', '690', 'SC', '248', 'Victoria', 'SCR', 'SRe', '.sc', 'Seychelles', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Indian\\/Mahe\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"SCT\",\"tzName\":\"Seychelles Time\"}]', '{\"kr\":\"세이셸\",\"br\":\"Seicheles\",\"pt\":\"Seicheles\",\"nl\":\"Seychellen\",\"hr\":\"Sejšeli\",\"fa\":\"سیشل\",\"de\":\"Seychellen\",\"es\":\"Seychelles\",\"fr\":\"Seychelles\",\"ja\":\"セーシェル\",\"it\":\"Seychelles\",\"cn\":\"塞舌尔\"}', -4.58333333, 55.66666666, '🇸🇨', 'U+1F1F8 U+1F1E8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1042'),
(198, 'Sierra Leone', 'SLE', '694', 'SL', '232', 'Freetown', 'SLL', 'Le', '.sl', 'Sierra Leone', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Freetown\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"시에라리온\",\"br\":\"Serra Leoa\",\"pt\":\"Serra Leoa\",\"nl\":\"Sierra Leone\",\"hr\":\"Sijera Leone\",\"fa\":\"سیرالئون\",\"de\":\"Sierra Leone\",\"es\":\"Sierra Leone\",\"fr\":\"Sierra Leone\",\"ja\":\"シエラレオネ\",\"it\":\"Sierra Leone\",\"cn\":\"塞拉利昂\"}', 8.50000000, -11.50000000, '🇸🇱', 'U+1F1F8 U+1F1F1', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1044'),
(199, 'Singapore', 'SGP', '702', 'SG', '65', 'Singapur', 'SGD', '$', '.sg', 'Singapore', 'Asia', 'South-Eastern Asia', '[{\"zoneName\":\"Asia\\/Singapore\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"SGT\",\"tzName\":\"Singapore Time\"}]', '{\"kr\":\"싱가포르\",\"br\":\"Singapura\",\"pt\":\"Singapura\",\"nl\":\"Singapore\",\"hr\":\"Singapur\",\"fa\":\"سنگاپور\",\"de\":\"Singapur\",\"es\":\"Singapur\",\"fr\":\"Singapour\",\"ja\":\"シンガポール\",\"it\":\"Singapore\",\"cn\":\"新加坡\"}', 1.36666666, 103.80000000, '🇸🇬', 'U+1F1F8 U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q334'),
(200, 'Slovakia', 'SVK', '703', 'SK', '421', 'Bratislava', 'EUR', '€', '.sk', 'Slovensko', 'Europe', 'Eastern Europe', '[{\"zoneName\":\"Europe\\/Bratislava\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"슬로바키아\",\"br\":\"Eslováquia\",\"pt\":\"Eslováquia\",\"nl\":\"Slowakije\",\"hr\":\"Slovačka\",\"fa\":\"اسلواکی\",\"de\":\"Slowakei\",\"es\":\"República Eslovaca\",\"fr\":\"Slovaquie\",\"ja\":\"スロバキア\",\"it\":\"Slovacchia\",\"cn\":\"斯洛伐克\"}', 48.66666666, 19.50000000, '🇸🇰', 'U+1F1F8 U+1F1F0', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q214');
INSERT INTO `country` (`id`, `country`, `iso3`, `numeric_code`, `country_code`, `phone_code`, `capital`, `currency_code`, `currency_symbol`, `tld`, `native`, `region`, `subregion`, `timezones`, `translations`, `latitude`, `longitude`, `emoji`, `emojiU`, `created_at`, `updated_at`, `flag`, `wikiDataId`) VALUES
(201, 'Slovenia', 'SVN', '705', 'SI', '386', 'Ljubljana', 'EUR', '€', '.si', 'Slovenija', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Ljubljana\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"슬로베니아\",\"br\":\"Eslovênia\",\"pt\":\"Eslovénia\",\"nl\":\"Slovenië\",\"hr\":\"Slovenija\",\"fa\":\"اسلوونی\",\"de\":\"Slowenien\",\"es\":\"Eslovenia\",\"fr\":\"Slovénie\",\"ja\":\"スロベニア\",\"it\":\"Slovenia\",\"cn\":\"斯洛文尼亚\"}', 46.11666666, 14.81666666, '🇸🇮', 'U+1F1F8 U+1F1EE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q215'),
(202, 'Solomon Islands', 'SLB', '090', 'SB', '677', 'Honiara', 'SBD', 'Si$', '.sb', 'Solomon Islands', 'Oceania', 'Melanesia', '[{\"zoneName\":\"Pacific\\/Guadalcanal\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"SBT\",\"tzName\":\"Solomon Islands Time\"}]', '{\"kr\":\"솔로몬 제도\",\"br\":\"Ilhas Salomão\",\"pt\":\"Ilhas Salomão\",\"nl\":\"Salomonseilanden\",\"hr\":\"Solomonski Otoci\",\"fa\":\"جزایر سلیمان\",\"de\":\"Salomonen\",\"es\":\"Islas Salomón\",\"fr\":\"Îles Salomon\",\"ja\":\"ソロモン諸島\",\"it\":\"Isole Salomone\",\"cn\":\"所罗门群岛\"}', -8.00000000, 159.00000000, '🇸🇧', 'U+1F1F8 U+1F1E7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q685'),
(203, 'Somalia', 'SOM', '706', 'SO', '252', 'Mogadishu', 'SOS', 'Sh.so.', '.so', 'Soomaaliya', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Mogadishu\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"EAT\",\"tzName\":\"East Africa Time\"}]', '{\"kr\":\"소말리아\",\"br\":\"Somália\",\"pt\":\"Somália\",\"nl\":\"Somalië\",\"hr\":\"Somalija\",\"fa\":\"سومالی\",\"de\":\"Somalia\",\"es\":\"Somalia\",\"fr\":\"Somalie\",\"ja\":\"ソマリア\",\"it\":\"Somalia\",\"cn\":\"索马里\"}', 10.00000000, 49.00000000, '🇸🇴', 'U+1F1F8 U+1F1F4', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1045'),
(204, 'South Africa', 'ZAF', '710', 'ZA', '27', 'Pretoria', 'ZAR', 'R', '.za', 'South Africa', 'Africa', 'Southern Africa', '[{\"zoneName\":\"Africa\\/Johannesburg\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"SAST\",\"tzName\":\"South African Standard Time\"}]', '{\"kr\":\"남아프리카 공화국\",\"br\":\"República Sul-Africana\",\"pt\":\"República Sul-Africana\",\"nl\":\"Zuid-Afrika\",\"hr\":\"Južnoafrička Republika\",\"fa\":\"آفریقای جنوبی\",\"de\":\"Republik Südafrika\",\"es\":\"República de Sudáfrica\",\"fr\":\"Afrique du Sud\",\"ja\":\"南アフリカ\",\"it\":\"Sud Africa\",\"cn\":\"南非\"}', -29.00000000, 24.00000000, '🇿🇦', 'U+1F1FF U+1F1E6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q258'),
(205, 'South Georgia', 'SGS', '239', 'GS', '500', 'Grytviken', 'GBP', '£', '.gs', 'South Georgia', 'Americas', 'South America', '[{\"zoneName\":\"Atlantic\\/South_Georgia\",\"gmtOffset\":-7200,\"gmtOffsetName\":\"UTC-02:00\",\"abbreviation\":\"GST\",\"tzName\":\"South Georgia and the South Sandwich Islands Time\"}]', '{\"kr\":\"사우스조지아\",\"br\":\"Ilhas Geórgias do Sul e Sandwich do Sul\",\"pt\":\"Ilhas Geórgia do Sul e Sanduíche do Sul\",\"nl\":\"Zuid-Georgia en Zuidelijke Sandwicheilanden\",\"hr\":\"Južna Georgija i otočje Južni Sandwich\",\"fa\":\"جزایر جورجیای جنوبی و ساندویچ جنوبی\",\"de\":\"Südgeorgien und die Südlichen Sandwichinseln\",\"es\":\"Islas Georgias del Sur y Sandwich del Sur\",\"fr\":\"Géorgie du Sud-et-les Îles Sandwich du Sud\",\"ja\":\"サウスジョージア・サウスサンドウィッチ諸島\",\"it\":\"Georgia del Sud e Isole Sandwich Meridionali\",\"cn\":\"南乔治亚\"}', -54.50000000, -37.00000000, '🇬🇸', 'U+1F1EC U+1F1F8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(206, 'South Sudan', 'SSD', '728', 'SS', '211', 'Juba', 'SSP', '£', '.ss', 'South Sudan', 'Africa', 'Middle Africa', '[{\"zoneName\":\"Africa\\/Juba\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"EAT\",\"tzName\":\"East Africa Time\"}]', '{\"kr\":\"남수단\",\"br\":\"Sudão do Sul\",\"pt\":\"Sudão do Sul\",\"nl\":\"Zuid-Soedan\",\"hr\":\"Južni Sudan\",\"fa\":\"سودان جنوبی\",\"de\":\"Südsudan\",\"es\":\"Sudán del Sur\",\"fr\":\"Soudan du Sud\",\"ja\":\"南スーダン\",\"it\":\"Sudan del sud\",\"cn\":\"南苏丹\"}', 7.00000000, 30.00000000, '🇸🇸', 'U+1F1F8 U+1F1F8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q958'),
(207, 'Spain', 'ESP', '724', 'ES', '34', 'Madrid', 'EUR', '€', '.es', 'España', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Africa\\/Ceuta\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"},{\"zoneName\":\"Atlantic\\/Canary\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"WET\",\"tzName\":\"Western European Time\"},{\"zoneName\":\"Europe\\/Madrid\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"스페인\",\"br\":\"Espanha\",\"pt\":\"Espanha\",\"nl\":\"Spanje\",\"hr\":\"Španjolska\",\"fa\":\"اسپانیا\",\"de\":\"Spanien\",\"es\":\"España\",\"fr\":\"Espagne\",\"ja\":\"スペイン\",\"it\":\"Spagna\",\"cn\":\"西班牙\"}', 40.00000000, -4.00000000, '🇪🇸', 'U+1F1EA U+1F1F8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q29'),
(208, 'Sri Lanka', 'LKA', '144', 'LK', '94', 'Colombo', 'LKR', 'Rs', '.lk', 'śrī laṃkāva', 'Asia', 'Southern Asia', '[{\"zoneName\":\"Asia\\/Colombo\",\"gmtOffset\":19800,\"gmtOffsetName\":\"UTC+05:30\",\"abbreviation\":\"IST\",\"tzName\":\"Indian Standard Time\"}]', '{\"kr\":\"스리랑카\",\"br\":\"Sri Lanka\",\"pt\":\"Sri Lanka\",\"nl\":\"Sri Lanka\",\"hr\":\"Šri Lanka\",\"fa\":\"سری‌لانکا\",\"de\":\"Sri Lanka\",\"es\":\"Sri Lanka\",\"fr\":\"Sri Lanka\",\"ja\":\"スリランカ\",\"it\":\"Sri Lanka\",\"cn\":\"斯里兰卡\"}', 7.00000000, 81.00000000, '🇱🇰', 'U+1F1F1 U+1F1F0', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q854'),
(209, 'Sudan', 'SDN', '729', 'SD', '249', 'Khartoum', 'SDG', '.س.ج', '.sd', 'السودان', 'Africa', 'Northern Africa', '[{\"zoneName\":\"Africa\\/Khartoum\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EAT\",\"tzName\":\"Eastern African Time\"}]', '{\"kr\":\"수단\",\"br\":\"Sudão\",\"pt\":\"Sudão\",\"nl\":\"Soedan\",\"hr\":\"Sudan\",\"fa\":\"سودان\",\"de\":\"Sudan\",\"es\":\"Sudán\",\"fr\":\"Soudan\",\"ja\":\"スーダン\",\"it\":\"Sudan\",\"cn\":\"苏丹\"}', 15.00000000, 30.00000000, '🇸🇩', 'U+1F1F8 U+1F1E9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1049'),
(210, 'Suriname', 'SUR', '740', 'SR', '597', 'Paramaribo', 'SRD', '$', '.sr', 'Suriname', 'Americas', 'South America', '[{\"zoneName\":\"America\\/Paramaribo\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"SRT\",\"tzName\":\"Suriname Time\"}]', '{\"kr\":\"수리남\",\"br\":\"Suriname\",\"pt\":\"Suriname\",\"nl\":\"Suriname\",\"hr\":\"Surinam\",\"fa\":\"سورینام\",\"de\":\"Suriname\",\"es\":\"Surinam\",\"fr\":\"Surinam\",\"ja\":\"スリナム\",\"it\":\"Suriname\",\"cn\":\"苏里南\"}', 4.00000000, -56.00000000, '🇸🇷', 'U+1F1F8 U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q730'),
(211, 'Svalbard And Jan Mayen Islands', 'SJM', '744', 'SJ', '47', 'Longyearbyen', 'NOK', 'kr', '.sj', 'Svalbard og Jan Mayen', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Arctic\\/Longyearbyen\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"스발바르 얀마옌 제도\",\"br\":\"Svalbard\",\"pt\":\"Svalbard\",\"nl\":\"Svalbard en Jan Mayen\",\"hr\":\"Svalbard i Jan Mayen\",\"fa\":\"سوالبارد و یان ماین\",\"de\":\"Svalbard und Jan Mayen\",\"es\":\"Islas Svalbard y Jan Mayen\",\"fr\":\"Svalbard et Jan Mayen\",\"ja\":\"スヴァールバル諸島およびヤンマイエン島\",\"it\":\"Svalbard e Jan Mayen\",\"cn\":\"斯瓦尔巴和扬马延群岛\"}', 78.00000000, 20.00000000, '🇸🇯', 'U+1F1F8 U+1F1EF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(212, 'Swaziland', 'SWZ', '748', 'SZ', '268', 'Mbabane', 'SZL', 'E', '.sz', 'Swaziland', 'Africa', 'Southern Africa', '[{\"zoneName\":\"Africa\\/Mbabane\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"SAST\",\"tzName\":\"South African Standard Time\"}]', '{\"kr\":\"에스와티니\",\"br\":\"Suazilândia\",\"pt\":\"Suazilândia\",\"nl\":\"Swaziland\",\"hr\":\"Svazi\",\"fa\":\"سوازیلند\",\"de\":\"Swasiland\",\"es\":\"Suazilandia\",\"fr\":\"Swaziland\",\"ja\":\"スワジランド\",\"it\":\"Swaziland\",\"cn\":\"斯威士兰\"}', -26.50000000, 31.50000000, '🇸🇿', 'U+1F1F8 U+1F1FF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1050'),
(213, 'Sweden', 'SWE', '752', 'SE', '46', 'Stockholm', 'SEK', 'kr', '.se', 'Sverige', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/Stockholm\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"스웨덴\",\"br\":\"Suécia\",\"pt\":\"Suécia\",\"nl\":\"Zweden\",\"hr\":\"Švedska\",\"fa\":\"سوئد\",\"de\":\"Schweden\",\"es\":\"Suecia\",\"fr\":\"Suède\",\"ja\":\"スウェーデン\",\"it\":\"Svezia\",\"cn\":\"瑞典\"}', 62.00000000, 15.00000000, '🇸🇪', 'U+1F1F8 U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q34'),
(214, 'Switzerland', 'CHE', '756', 'CH', '41', 'Bern', 'CHF', 'CHf', '.ch', 'Schweiz', 'Europe', 'Western Europe', '[{\"zoneName\":\"Europe\\/Zurich\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"스위스\",\"br\":\"Suíça\",\"pt\":\"Suíça\",\"nl\":\"Zwitserland\",\"hr\":\"Švicarska\",\"fa\":\"سوئیس\",\"de\":\"Schweiz\",\"es\":\"Suiza\",\"fr\":\"Suisse\",\"ja\":\"スイス\",\"it\":\"Svizzera\",\"cn\":\"瑞士\"}', 47.00000000, 8.00000000, '🇨🇭', 'U+1F1E8 U+1F1ED', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q39'),
(215, 'Syria', 'SYR', '760', 'SY', '963', 'Damascus', 'SYP', 'LS', '.sy', 'سوريا', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Damascus\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"시리아\",\"br\":\"Síria\",\"pt\":\"Síria\",\"nl\":\"Syrië\",\"hr\":\"Sirija\",\"fa\":\"سوریه\",\"de\":\"Syrien\",\"es\":\"Siria\",\"fr\":\"Syrie\",\"ja\":\"シリア・アラブ共和国\",\"it\":\"Siria\",\"cn\":\"叙利亚\"}', 35.00000000, 38.00000000, '🇸🇾', 'U+1F1F8 U+1F1FE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q858'),
(216, 'Taiwan', 'TWN', '158', 'TW', '886', 'Taipei', 'TWD', '$', '.tw', '臺灣', 'Asia', 'Eastern Asia', '[{\"zoneName\":\"Asia\\/Taipei\",\"gmtOffset\":28800,\"gmtOffsetName\":\"UTC+08:00\",\"abbreviation\":\"CST\",\"tzName\":\"China Standard Time\"}]', '{\"kr\":\"대만\",\"br\":\"Taiwan\",\"pt\":\"Taiwan\",\"nl\":\"Taiwan\",\"hr\":\"Tajvan\",\"fa\":\"تایوان\",\"de\":\"Taiwan\",\"es\":\"Taiwán\",\"fr\":\"Taïwan\",\"ja\":\"台湾（中華民国）\",\"it\":\"Taiwan\",\"cn\":\"中国台湾\"}', 23.50000000, 121.00000000, '🇹🇼', 'U+1F1F9 U+1F1FC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q865'),
(217, 'Tajikistan', 'TJK', '762', 'TJ', '992', 'Dushanbe', 'TJS', 'SM', '.tj', 'Тоҷикистон', 'Asia', 'Central Asia', '[{\"zoneName\":\"Asia\\/Dushanbe\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"TJT\",\"tzName\":\"Tajikistan Time\"}]', '{\"kr\":\"타지키스탄\",\"br\":\"Tajiquistão\",\"pt\":\"Tajiquistão\",\"nl\":\"Tadzjikistan\",\"hr\":\"Tađikistan\",\"fa\":\"تاجیکستان\",\"de\":\"Tadschikistan\",\"es\":\"Tayikistán\",\"fr\":\"Tadjikistan\",\"ja\":\"タジキスタン\",\"it\":\"Tagikistan\",\"cn\":\"塔吉克斯坦\"}', 39.00000000, 71.00000000, '🇹🇯', 'U+1F1F9 U+1F1EF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q863'),
(218, 'Tanzania', 'TZA', '834', 'TZ', '255', 'Dodoma', 'TZS', 'TSh', '.tz', 'Tanzania', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Dar_es_Salaam\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"EAT\",\"tzName\":\"East Africa Time\"}]', '{\"kr\":\"탄자니아\",\"br\":\"Tanzânia\",\"pt\":\"Tanzânia\",\"nl\":\"Tanzania\",\"hr\":\"Tanzanija\",\"fa\":\"تانزانیا\",\"de\":\"Tansania\",\"es\":\"Tanzania\",\"fr\":\"Tanzanie\",\"ja\":\"タンザニア\",\"it\":\"Tanzania\",\"cn\":\"坦桑尼亚\"}', -6.00000000, 35.00000000, '🇹🇿', 'U+1F1F9 U+1F1FF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q924'),
(219, 'Thailand', 'THA', '764', 'TH', '66', 'Bangkok', 'THB', '฿', '.th', 'ประเทศไทย', 'Asia', 'South-Eastern Asia', '[{\"zoneName\":\"Asia\\/Bangkok\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"ICT\",\"tzName\":\"Indochina Time\"}]', '{\"kr\":\"태국\",\"br\":\"Tailândia\",\"pt\":\"Tailândia\",\"nl\":\"Thailand\",\"hr\":\"Tajland\",\"fa\":\"تایلند\",\"de\":\"Thailand\",\"es\":\"Tailandia\",\"fr\":\"Thaïlande\",\"ja\":\"タイ\",\"it\":\"Tailandia\",\"cn\":\"泰国\"}', 15.00000000, 100.00000000, '🇹🇭', 'U+1F1F9 U+1F1ED', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q869'),
(220, 'Togo', 'TGO', '768', 'TG', '228', 'Lome', 'XOF', 'CFA', '.tg', 'Togo', 'Africa', 'Western Africa', '[{\"zoneName\":\"Africa\\/Lome\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"토고\",\"br\":\"Togo\",\"pt\":\"Togo\",\"nl\":\"Togo\",\"hr\":\"Togo\",\"fa\":\"توگو\",\"de\":\"Togo\",\"es\":\"Togo\",\"fr\":\"Togo\",\"ja\":\"トーゴ\",\"it\":\"Togo\",\"cn\":\"多哥\"}', 8.00000000, 1.16666666, '🇹🇬', 'U+1F1F9 U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q945'),
(221, 'Tokelau', 'TKL', '772', 'TK', '690', '', 'NZD', '$', '.tk', 'Tokelau', 'Oceania', 'Polynesia', '[{\"zoneName\":\"Pacific\\/Fakaofo\",\"gmtOffset\":46800,\"gmtOffsetName\":\"UTC+13:00\",\"abbreviation\":\"TKT\",\"tzName\":\"Tokelau Time\"}]', '{\"kr\":\"토켈라우\",\"br\":\"Tokelau\",\"pt\":\"Toquelau\",\"nl\":\"Tokelau\",\"hr\":\"Tokelau\",\"fa\":\"توکلائو\",\"de\":\"Tokelau\",\"es\":\"Islas Tokelau\",\"fr\":\"Tokelau\",\"ja\":\"トケラウ\",\"it\":\"Isole Tokelau\",\"cn\":\"托克劳\"}', -9.00000000, -172.00000000, '🇹🇰', 'U+1F1F9 U+1F1F0', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(222, 'Tonga', 'TON', '776', 'TO', '676', 'Nuku\'alofa', 'TOP', '$', '.to', 'Tonga', 'Oceania', 'Polynesia', '[{\"zoneName\":\"Pacific\\/Tongatapu\",\"gmtOffset\":46800,\"gmtOffsetName\":\"UTC+13:00\",\"abbreviation\":\"TOT\",\"tzName\":\"Tonga Time\"}]', '{\"kr\":\"통가\",\"br\":\"Tonga\",\"pt\":\"Tonga\",\"nl\":\"Tonga\",\"hr\":\"Tonga\",\"fa\":\"تونگا\",\"de\":\"Tonga\",\"es\":\"Tonga\",\"fr\":\"Tonga\",\"ja\":\"トンガ\",\"it\":\"Tonga\",\"cn\":\"汤加\"}', -20.00000000, -175.00000000, '🇹🇴', 'U+1F1F9 U+1F1F4', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q678'),
(223, 'Trinidad And Tobago', 'TTO', '780', 'TT', '+1-868', 'Port of Spain', 'TTD', '$', '.tt', 'Trinidad and Tobago', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Port_of_Spain\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"트리니다드 토바고\",\"br\":\"Trinidad e Tobago\",\"pt\":\"Trindade e Tobago\",\"nl\":\"Trinidad en Tobago\",\"hr\":\"Trinidad i Tobago\",\"fa\":\"ترینیداد و توباگو\",\"de\":\"Trinidad und Tobago\",\"es\":\"Trinidad y Tobago\",\"fr\":\"Trinité et Tobago\",\"ja\":\"トリニダード・トバゴ\",\"it\":\"Trinidad e Tobago\",\"cn\":\"特立尼达和多巴哥\"}', 11.00000000, -61.00000000, '🇹🇹', 'U+1F1F9 U+1F1F9', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q754'),
(224, 'Tunisia', 'TUN', '788', 'TN', '216', 'Tunis', 'TND', 'ت.د', '.tn', 'تونس', 'Africa', 'Northern Africa', '[{\"zoneName\":\"Africa\\/Tunis\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"튀니지\",\"br\":\"Tunísia\",\"pt\":\"Tunísia\",\"nl\":\"Tunesië\",\"hr\":\"Tunis\",\"fa\":\"تونس\",\"de\":\"Tunesien\",\"es\":\"Túnez\",\"fr\":\"Tunisie\",\"ja\":\"チュニジア\",\"it\":\"Tunisia\",\"cn\":\"突尼斯\"}', 34.00000000, 9.00000000, '🇹🇳', 'U+1F1F9 U+1F1F3', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q948'),
(225, 'Turkey', 'TUR', '792', 'TR', '90', 'Ankara', 'TRY', '₺', '.tr', 'Türkiye', 'Asia', 'Western Asia', '[{\"zoneName\":\"Europe\\/Istanbul\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"터키\",\"br\":\"Turquia\",\"pt\":\"Turquia\",\"nl\":\"Turkije\",\"hr\":\"Turska\",\"fa\":\"ترکیه\",\"de\":\"Türkei\",\"es\":\"Turquía\",\"fr\":\"Turquie\",\"ja\":\"トルコ\",\"it\":\"Turchia\",\"cn\":\"土耳其\"}', 39.00000000, 35.00000000, '🇹🇷', 'U+1F1F9 U+1F1F7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q43'),
(226, 'Turkmenistan', 'TKM', '795', 'TM', '993', 'Ashgabat', 'TMT', 'T', '.tm', 'Türkmenistan', 'Asia', 'Central Asia', '[{\"zoneName\":\"Asia\\/Ashgabat\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"TMT\",\"tzName\":\"Turkmenistan Time\"}]', '{\"kr\":\"투르크메니스탄\",\"br\":\"Turcomenistão\",\"pt\":\"Turquemenistão\",\"nl\":\"Turkmenistan\",\"hr\":\"Turkmenistan\",\"fa\":\"ترکمنستان\",\"de\":\"Turkmenistan\",\"es\":\"Turkmenistán\",\"fr\":\"Turkménistan\",\"ja\":\"トルクメニスタン\",\"it\":\"Turkmenistan\",\"cn\":\"土库曼斯坦\"}', 40.00000000, 60.00000000, '🇹🇲', 'U+1F1F9 U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q874'),
(227, 'Turks And Caicos Islands', 'TCA', '796', 'TC', '+1-649', 'Cockburn Town', 'USD', '$', '.tc', 'Turks and Caicos Islands', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Grand_Turk\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"}]', '{\"kr\":\"터크스 케이커스 제도\",\"br\":\"Ilhas Turcas e Caicos\",\"pt\":\"Ilhas Turcas e Caicos\",\"nl\":\"Turks- en Caicoseilanden\",\"hr\":\"Otoci Turks i Caicos\",\"fa\":\"جزایر تورکس و کایکوس\",\"de\":\"Turks- und Caicosinseln\",\"es\":\"Islas Turks y Caicos\",\"fr\":\"Îles Turques-et-Caïques\",\"ja\":\"タークス・カイコス諸島\",\"it\":\"Isole Turks e Caicos\",\"cn\":\"特克斯和凯科斯群岛\"}', 21.75000000, -71.58333333, '🇹🇨', 'U+1F1F9 U+1F1E8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(228, 'Tuvalu', 'TUV', '798', 'TV', '688', 'Funafuti', 'AUD', '$', '.tv', 'Tuvalu', 'Oceania', 'Polynesia', '[{\"zoneName\":\"Pacific\\/Funafuti\",\"gmtOffset\":43200,\"gmtOffsetName\":\"UTC+12:00\",\"abbreviation\":\"TVT\",\"tzName\":\"Tuvalu Time\"}]', '{\"kr\":\"투발루\",\"br\":\"Tuvalu\",\"pt\":\"Tuvalu\",\"nl\":\"Tuvalu\",\"hr\":\"Tuvalu\",\"fa\":\"تووالو\",\"de\":\"Tuvalu\",\"es\":\"Tuvalu\",\"fr\":\"Tuvalu\",\"ja\":\"ツバル\",\"it\":\"Tuvalu\",\"cn\":\"图瓦卢\"}', -8.00000000, 178.00000000, '🇹🇻', 'U+1F1F9 U+1F1FB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q672'),
(229, 'Uganda', 'UGA', '800', 'UG', '256', 'Kampala', 'UGX', 'USh', '.ug', 'Uganda', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Kampala\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"EAT\",\"tzName\":\"East Africa Time\"}]', '{\"kr\":\"우간다\",\"br\":\"Uganda\",\"pt\":\"Uganda\",\"nl\":\"Oeganda\",\"hr\":\"Uganda\",\"fa\":\"اوگاندا\",\"de\":\"Uganda\",\"es\":\"Uganda\",\"fr\":\"Uganda\",\"ja\":\"ウガンダ\",\"it\":\"Uganda\",\"cn\":\"乌干达\"}', 1.00000000, 32.00000000, '🇺🇬', 'U+1F1FA U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q1036'),
(230, 'Ukraine', 'UKR', '804', 'UA', '380', 'Kiev', 'UAH', '₴', '.ua', 'Україна', 'Europe', 'Eastern Europe', '[{\"zoneName\":\"Europe\\/Kiev\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"},{\"zoneName\":\"Europe\\/Simferopol\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"MSK\",\"tzName\":\"Moscow Time\"},{\"zoneName\":\"Europe\\/Uzhgorod\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"},{\"zoneName\":\"Europe\\/Zaporozhye\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"EET\",\"tzName\":\"Eastern European Time\"}]', '{\"kr\":\"우크라이나\",\"br\":\"Ucrânia\",\"pt\":\"Ucrânia\",\"nl\":\"Oekraïne\",\"hr\":\"Ukrajina\",\"fa\":\"وکراین\",\"de\":\"Ukraine\",\"es\":\"Ucrania\",\"fr\":\"Ukraine\",\"ja\":\"ウクライナ\",\"it\":\"Ucraina\",\"cn\":\"乌克兰\"}', 49.00000000, 32.00000000, '🇺🇦', 'U+1F1FA U+1F1E6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q212'),
(231, 'United Arab Emirates', 'ARE', '784', 'AE', '971', 'Abu Dhabi', 'AED', 'إ.د', '.ae', 'دولة الإمارات العربية المتحدة', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Dubai\",\"gmtOffset\":14400,\"gmtOffsetName\":\"UTC+04:00\",\"abbreviation\":\"GST\",\"tzName\":\"Gulf Standard Time\"}]', '{\"kr\":\"아랍에미리트\",\"br\":\"Emirados árabes Unidos\",\"pt\":\"Emirados árabes Unidos\",\"nl\":\"Verenigde Arabische Emiraten\",\"hr\":\"Ujedinjeni Arapski Emirati\",\"fa\":\"امارات متحده عربی\",\"de\":\"Vereinigte Arabische Emirate\",\"es\":\"Emiratos Árabes Unidos\",\"fr\":\"Émirats arabes unis\",\"ja\":\"アラブ首長国連邦\",\"it\":\"Emirati Arabi Uniti\",\"cn\":\"阿拉伯联合酋长国\"}', 24.00000000, 54.00000000, '🇦🇪', 'U+1F1E6 U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q878'),
(232, 'United Kingdom', 'GBR', '826', 'GB', '44', 'London', 'GBP', '£', '.uk', 'United Kingdom', 'Europe', 'Northern Europe', '[{\"zoneName\":\"Europe\\/London\",\"gmtOffset\":0,\"gmtOffsetName\":\"UTC\\u00b100\",\"abbreviation\":\"GMT\",\"tzName\":\"Greenwich Mean Time\"}]', '{\"kr\":\"영국\",\"br\":\"Reino Unido\",\"pt\":\"Reino Unido\",\"nl\":\"Verenigd Koninkrijk\",\"hr\":\"Ujedinjeno Kraljevstvo\",\"fa\":\"بریتانیای کبیر و ایرلند شمالی\",\"de\":\"Vereinigtes Königreich\",\"es\":\"Reino Unido\",\"fr\":\"Royaume-Uni\",\"ja\":\"イギリス\",\"it\":\"Regno Unito\",\"cn\":\"英国\"}', 54.00000000, -2.00000000, '🇬🇧', 'U+1F1EC U+1F1E7', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q145'),
(233, 'United States', 'USA', '840', 'US', '1', 'Washington', 'USD', '$', '.us', 'United States', 'Americas', 'Northern America', '[{\"zoneName\":\"America\\/Adak\",\"gmtOffset\":-36000,\"gmtOffsetName\":\"UTC-10:00\",\"abbreviation\":\"HST\",\"tzName\":\"Hawaii\\u2013Aleutian Standard Time\"},{\"zoneName\":\"America\\/Anchorage\",\"gmtOffset\":-32400,\"gmtOffsetName\":\"UTC-09:00\",\"abbreviation\":\"AKST\",\"tzName\":\"Alaska Standard Time\"},{\"zoneName\":\"America\\/Boise\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America\"},{\"zoneName\":\"America\\/Chicago\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Denver\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America\"},{\"zoneName\":\"America\\/Detroit\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Indiana\\/Indianapolis\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Indiana\\/Knox\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Indiana\\/Marengo\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Indiana\\/Petersburg\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Indiana\\/Tell_City\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Indiana\\/Vevay\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Indiana\\/Vincennes\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Indiana\\/Winamac\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Juneau\",\"gmtOffset\":-32400,\"gmtOffsetName\":\"UTC-09:00\",\"abbreviation\":\"AKST\",\"tzName\":\"Alaska Standard Time\"},{\"zoneName\":\"America\\/Kentucky\\/Louisville\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Kentucky\\/Monticello\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Los_Angeles\",\"gmtOffset\":-28800,\"gmtOffsetName\":\"UTC-08:00\",\"abbreviation\":\"PST\",\"tzName\":\"Pacific Standard Time (North America\"},{\"zoneName\":\"America\\/Menominee\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Metlakatla\",\"gmtOffset\":-32400,\"gmtOffsetName\":\"UTC-09:00\",\"abbreviation\":\"AKST\",\"tzName\":\"Alaska Standard Time\"},{\"zoneName\":\"America\\/New_York\",\"gmtOffset\":-18000,\"gmtOffsetName\":\"UTC-05:00\",\"abbreviation\":\"EST\",\"tzName\":\"Eastern Standard Time (North America\"},{\"zoneName\":\"America\\/Nome\",\"gmtOffset\":-32400,\"gmtOffsetName\":\"UTC-09:00\",\"abbreviation\":\"AKST\",\"tzName\":\"Alaska Standard Time\"},{\"zoneName\":\"America\\/North_Dakota\\/Beulah\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/North_Dakota\\/Center\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/North_Dakota\\/New_Salem\",\"gmtOffset\":-21600,\"gmtOffsetName\":\"UTC-06:00\",\"abbreviation\":\"CST\",\"tzName\":\"Central Standard Time (North America\"},{\"zoneName\":\"America\\/Phoenix\",\"gmtOffset\":-25200,\"gmtOffsetName\":\"UTC-07:00\",\"abbreviation\":\"MST\",\"tzName\":\"Mountain Standard Time (North America\"},{\"zoneName\":\"America\\/Sitka\",\"gmtOffset\":-32400,\"gmtOffsetName\":\"UTC-09:00\",\"abbreviation\":\"AKST\",\"tzName\":\"Alaska Standard Time\"},{\"zoneName\":\"America\\/Yakutat\",\"gmtOffset\":-32400,\"gmtOffsetName\":\"UTC-09:00\",\"abbreviation\":\"AKST\",\"tzName\":\"Alaska Standard Time\"},{\"zoneName\":\"Pacific\\/Honolulu\",\"gmtOffset\":-36000,\"gmtOffsetName\":\"UTC-10:00\",\"abbreviation\":\"HST\",\"tzName\":\"Hawaii\\u2013Aleutian Standard Time\"}]', '{\"kr\":\"미국\",\"br\":\"Estados Unidos\",\"pt\":\"Estados Unidos\",\"nl\":\"Verenigde Staten\",\"hr\":\"Sjedinjene Američke Države\",\"fa\":\"ایالات متحده آمریکا\",\"de\":\"Vereinigte Staaten von Amerika\",\"es\":\"Estados Unidos\",\"fr\":\"États-Unis\",\"ja\":\"アメリカ合衆国\",\"it\":\"Stati Uniti D\'America\",\"cn\":\"美国\"}', 38.00000000, -97.00000000, '🇺🇸', 'U+1F1FA U+1F1F8', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q30'),
(234, 'United States Minor Outlying Islands', 'UMI', '581', 'UM', '1', '', 'USD', '$', '.us', 'United States Minor Outlying Islands', 'Americas', 'Northern America', '[{\"zoneName\":\"Pacific\\/Midway\",\"gmtOffset\":-39600,\"gmtOffsetName\":\"UTC-11:00\",\"abbreviation\":\"SST\",\"tzName\":\"Samoa Standard Time\"},{\"zoneName\":\"Pacific\\/Wake\",\"gmtOffset\":43200,\"gmtOffsetName\":\"UTC+12:00\",\"abbreviation\":\"WAKT\",\"tzName\":\"Wake Island Time\"}]', '{\"kr\":\"미국령 군소 제도\",\"br\":\"Ilhas Menores Distantes dos Estados Unidos\",\"pt\":\"Ilhas Menores Distantes dos Estados Unidos\",\"nl\":\"Kleine afgelegen eilanden van de Verenigde Staten\",\"hr\":\"Mali udaljeni otoci SAD-a\",\"fa\":\"جزایر کوچک حاشیه‌ای ایالات متحده آمریکا\",\"de\":\"Kleinere Inselbesitzungen der Vereinigten Staaten\",\"es\":\"Islas Ultramarinas Menores de Estados Unidos\",\"fr\":\"Îles mineures éloignées des États-Unis\",\"ja\":\"合衆国領有小離島\",\"it\":\"Isole minori esterne degli Stati Uniti d\'America\",\"cn\":\"美国本土外小岛屿\"}', 0.00000000, 0.00000000, '🇺🇲', 'U+1F1FA U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(235, 'Uruguay', 'URY', '858', 'UY', '598', 'Montevideo', 'UYU', '$', '.uy', 'Uruguay', 'Americas', 'South America', '[{\"zoneName\":\"America\\/Montevideo\",\"gmtOffset\":-10800,\"gmtOffsetName\":\"UTC-03:00\",\"abbreviation\":\"UYT\",\"tzName\":\"Uruguay Standard Time\"}]', '{\"kr\":\"우루과이\",\"br\":\"Uruguai\",\"pt\":\"Uruguai\",\"nl\":\"Uruguay\",\"hr\":\"Urugvaj\",\"fa\":\"اروگوئه\",\"de\":\"Uruguay\",\"es\":\"Uruguay\",\"fr\":\"Uruguay\",\"ja\":\"ウルグアイ\",\"it\":\"Uruguay\",\"cn\":\"乌拉圭\"}', -33.00000000, -56.00000000, '🇺🇾', 'U+1F1FA U+1F1FE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q77'),
(236, 'Uzbekistan', 'UZB', '860', 'UZ', '998', 'Tashkent', 'UZS', 'лв', '.uz', 'O‘zbekiston', 'Asia', 'Central Asia', '[{\"zoneName\":\"Asia\\/Samarkand\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"UZT\",\"tzName\":\"Uzbekistan Time\"},{\"zoneName\":\"Asia\\/Tashkent\",\"gmtOffset\":18000,\"gmtOffsetName\":\"UTC+05:00\",\"abbreviation\":\"UZT\",\"tzName\":\"Uzbekistan Time\"}]', '{\"kr\":\"우즈베키스탄\",\"br\":\"Uzbequistão\",\"pt\":\"Usbequistão\",\"nl\":\"Oezbekistan\",\"hr\":\"Uzbekistan\",\"fa\":\"ازبکستان\",\"de\":\"Usbekistan\",\"es\":\"Uzbekistán\",\"fr\":\"Ouzbékistan\",\"ja\":\"ウズベキスタン\",\"it\":\"Uzbekistan\",\"cn\":\"乌兹别克斯坦\"}', 41.00000000, 64.00000000, '🇺🇿', 'U+1F1FA U+1F1FF', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q265'),
(237, 'Vanuatu', 'VUT', '548', 'VU', '678', 'Port Vila', 'VUV', 'VT', '.vu', 'Vanuatu', 'Oceania', 'Melanesia', '[{\"zoneName\":\"Pacific\\/Efate\",\"gmtOffset\":39600,\"gmtOffsetName\":\"UTC+11:00\",\"abbreviation\":\"VUT\",\"tzName\":\"Vanuatu Time\"}]', '{\"kr\":\"바누아투\",\"br\":\"Vanuatu\",\"pt\":\"Vanuatu\",\"nl\":\"Vanuatu\",\"hr\":\"Vanuatu\",\"fa\":\"وانواتو\",\"de\":\"Vanuatu\",\"es\":\"Vanuatu\",\"fr\":\"Vanuatu\",\"ja\":\"バヌアツ\",\"it\":\"Vanuatu\",\"cn\":\"瓦努阿图\"}', -16.00000000, 167.00000000, '🇻🇺', 'U+1F1FB U+1F1FA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q686'),
(238, 'Vatican City State (Holy See)', 'VAT', '336', 'VA', '379', 'Vatican City', 'EUR', '€', '.va', 'Vaticano', 'Europe', 'Southern Europe', '[{\"zoneName\":\"Europe\\/Vatican\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"바티칸 시국\",\"br\":\"Vaticano\",\"pt\":\"Vaticano\",\"nl\":\"Heilige Stoel\",\"hr\":\"Sveta Stolica\",\"fa\":\"سریر مقدس\",\"de\":\"Heiliger Stuhl\",\"es\":\"Santa Sede\",\"fr\":\"voir Saint\",\"ja\":\"聖座\",\"it\":\"Santa Sede\",\"cn\":\"梵蒂冈\"}', 41.90000000, 12.45000000, '🇻🇦', 'U+1F1FB U+1F1E6', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q237'),
(239, 'Venezuela', 'VEN', '862', 'VE', '58', 'Caracas', 'VEF', 'Bs', '.ve', 'Venezuela', 'Americas', 'South America', '[{\"zoneName\":\"America\\/Caracas\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"VET\",\"tzName\":\"Venezuelan Standard Time\"}]', '{\"kr\":\"베네수엘라\",\"br\":\"Venezuela\",\"pt\":\"Venezuela\",\"nl\":\"Venezuela\",\"hr\":\"Venezuela\",\"fa\":\"ونزوئلا\",\"de\":\"Venezuela\",\"es\":\"Venezuela\",\"fr\":\"Venezuela\",\"ja\":\"ベネズエラ・ボリバル共和国\",\"it\":\"Venezuela\",\"cn\":\"委内瑞拉\"}', 8.00000000, -66.00000000, '🇻🇪', 'U+1F1FB U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q717'),
(240, 'Vietnam', 'VNM', '704', 'VN', '84', 'Hanoi', 'VND', '₫', '.vn', 'Việt Nam', 'Asia', 'South-Eastern Asia', '[{\"zoneName\":\"Asia\\/Ho_Chi_Minh\",\"gmtOffset\":25200,\"gmtOffsetName\":\"UTC+07:00\",\"abbreviation\":\"ICT\",\"tzName\":\"Indochina Time\"}]', '{\"kr\":\"베트남\",\"br\":\"Vietnã\",\"pt\":\"Vietname\",\"nl\":\"Vietnam\",\"hr\":\"Vijetnam\",\"fa\":\"ویتنام\",\"de\":\"Vietnam\",\"es\":\"Vietnam\",\"fr\":\"Viêt Nam\",\"ja\":\"ベトナム\",\"it\":\"Vietnam\",\"cn\":\"越南\"}', 16.16666666, 107.83333333, '🇻🇳', 'U+1F1FB U+1F1F3', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q881'),
(241, 'Virgin Islands (British)', 'VGB', '092', 'VG', '+1-284', 'Road Town', 'USD', '$', '.vg', 'British Virgin Islands', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Tortola\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"영국령 버진아일랜드\",\"br\":\"Ilhas Virgens Britânicas\",\"pt\":\"Ilhas Virgens Britânicas\",\"nl\":\"Britse Maagdeneilanden\",\"hr\":\"Britanski Djevičanski Otoci\",\"fa\":\"جزایر ویرجین بریتانیا\",\"de\":\"Britische Jungferninseln\",\"es\":\"Islas Vírgenes del Reino Unido\",\"fr\":\"Îles Vierges britanniques\",\"ja\":\"イギリス領ヴァージン諸島\",\"it\":\"Isole Vergini Britanniche\",\"cn\":\"圣文森特和格林纳丁斯\"}', 18.43138300, -64.62305000, '🇻🇬', 'U+1F1FB U+1F1EC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(242, 'Virgin Islands (US)', 'VIR', '850', 'VI', '+1-340', 'Charlotte Amalie', 'USD', '$', '.vi', 'United States Virgin Islands', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/St_Thomas\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"미국령 버진아일랜드\",\"br\":\"Ilhas Virgens Americanas\",\"pt\":\"Ilhas Virgens Americanas\",\"nl\":\"Verenigde Staten Maagdeneilanden\",\"fa\":\"جزایر ویرجین آمریکا\",\"de\":\"Amerikanische Jungferninseln\",\"es\":\"Islas Vírgenes de los Estados Unidos\",\"fr\":\"Îles Vierges des États-Unis\",\"ja\":\"アメリカ領ヴァージン諸島\",\"it\":\"Isole Vergini americane\",\"cn\":\"维尔京群岛（美国）\"}', 18.34000000, -64.93000000, '🇻🇮', 'U+1F1FB U+1F1EE', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(243, 'Wallis And Futuna Islands', 'WLF', '876', 'WF', '681', 'Mata Utu', 'XPF', '₣', '.wf', 'Wallis et Futuna', 'Oceania', 'Polynesia', '[{\"zoneName\":\"Pacific\\/Wallis\",\"gmtOffset\":43200,\"gmtOffsetName\":\"UTC+12:00\",\"abbreviation\":\"WFT\",\"tzName\":\"Wallis & Futuna Time\"}]', '{\"kr\":\"왈리스 푸투나\",\"br\":\"Wallis e Futuna\",\"pt\":\"Wallis e Futuna\",\"nl\":\"Wallis en Futuna\",\"hr\":\"Wallis i Fortuna\",\"fa\":\"والیس و فوتونا\",\"de\":\"Wallis und Futuna\",\"es\":\"Wallis y Futuna\",\"fr\":\"Wallis-et-Futuna\",\"ja\":\"ウォリス・フツナ\",\"it\":\"Wallis e Futuna\",\"cn\":\"瓦利斯群岛和富图纳群岛\"}', -13.30000000, -176.20000000, '🇼🇫', 'U+1F1FC U+1F1EB', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(244, 'Western Sahara', 'ESH', '732', 'EH', '212', 'El-Aaiun', 'MAD', 'MAD', '.eh', 'الصحراء الغربية', 'Africa', 'Northern Africa', '[{\"zoneName\":\"Africa\\/El_Aaiun\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"WEST\",\"tzName\":\"Western European Summer Time\"}]', '{\"kr\":\"서사하라\",\"br\":\"Saara Ocidental\",\"pt\":\"Saara Ocidental\",\"nl\":\"Westelijke Sahara\",\"hr\":\"Zapadna Sahara\",\"fa\":\"جمهوری دموکراتیک عربی صحرا\",\"de\":\"Westsahara\",\"es\":\"Sahara Occidental\",\"fr\":\"Sahara Occidental\",\"ja\":\"西サハラ\",\"it\":\"Sahara Occidentale\",\"cn\":\"西撒哈拉\"}', 24.50000000, -13.00000000, '🇪🇭', 'U+1F1EA U+1F1ED', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, NULL),
(245, 'Yemen', 'YEM', '887', 'YE', '967', 'Sanaa', 'YER', '﷼', '.ye', 'اليَمَن', 'Asia', 'Western Asia', '[{\"zoneName\":\"Asia\\/Aden\",\"gmtOffset\":10800,\"gmtOffsetName\":\"UTC+03:00\",\"abbreviation\":\"AST\",\"tzName\":\"Arabia Standard Time\"}]', '{\"kr\":\"예멘\",\"br\":\"Iêmen\",\"pt\":\"Iémen\",\"nl\":\"Jemen\",\"hr\":\"Jemen\",\"fa\":\"یمن\",\"de\":\"Jemen\",\"es\":\"Yemen\",\"fr\":\"Yémen\",\"ja\":\"イエメン\",\"it\":\"Yemen\",\"cn\":\"也门\"}', 15.00000000, 48.00000000, '🇾🇪', 'U+1F1FE U+1F1EA', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q805'),
(246, 'Zambia', 'ZMB', '894', 'ZM', '260', 'Lusaka', 'ZMW', 'ZK', '.zm', 'Zambia', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Lusaka\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"CAT\",\"tzName\":\"Central Africa Time\"}]', '{\"kr\":\"잠비아\",\"br\":\"Zâmbia\",\"pt\":\"Zâmbia\",\"nl\":\"Zambia\",\"hr\":\"Zambija\",\"fa\":\"زامبیا\",\"de\":\"Sambia\",\"es\":\"Zambia\",\"fr\":\"Zambie\",\"ja\":\"ザンビア\",\"it\":\"Zambia\",\"cn\":\"赞比亚\"}', -15.00000000, 30.00000000, '🇿🇲', 'U+1F1FF U+1F1F2', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q953'),
(247, 'Zimbabwe', 'ZWE', '716', 'ZW', '263', 'Harare', 'ZWL', '$', '.zw', 'Zimbabwe', 'Africa', 'Eastern Africa', '[{\"zoneName\":\"Africa\\/Harare\",\"gmtOffset\":7200,\"gmtOffsetName\":\"UTC+02:00\",\"abbreviation\":\"CAT\",\"tzName\":\"Central Africa Time\"}]', '{\"kr\":\"짐바브웨\",\"br\":\"Zimbabwe\",\"pt\":\"Zimbabué\",\"nl\":\"Zimbabwe\",\"hr\":\"Zimbabve\",\"fa\":\"زیمباوه\",\"de\":\"Simbabwe\",\"es\":\"Zimbabue\",\"fr\":\"Zimbabwe\",\"ja\":\"ジンバブエ\",\"it\":\"Zimbabwe\",\"cn\":\"津巴布韦\"}', -20.00000000, 30.00000000, '🇿🇼', 'U+1F1FF U+1F1FC', '2018-07-20 20:11:03', '2021-08-01 14:37:27', 1, 'Q954'),
(248, 'Kosovo', 'XKX', '926', 'XK', '383', 'Pristina', 'EUR', '€', '.xk', 'Republika e Kosovës', 'Europe', 'Eastern Europe', '[{\"zoneName\":\"Europe\\/Belgrade\",\"gmtOffset\":3600,\"gmtOffsetName\":\"UTC+01:00\",\"abbreviation\":\"CET\",\"tzName\":\"Central European Time\"}]', '{\"kr\":\"코소보\",\"cn\":\"科索沃\"}', 42.56129090, 20.34030350, '🇽🇰', 'U+1F1FD U+1F1F0', '2020-08-15 15:33:50', '2021-08-01 14:37:57', 1, 'Q1246'),
(249, 'Curaçao', 'CUW', '531', 'CW', '599', 'Willemstad', 'ANG', 'ƒ', '.cw', 'Curaçao', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Curacao\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"퀴라소\",\"br\":\"Curaçao\",\"pt\":\"Curaçao\",\"nl\":\"Curaçao\",\"fa\":\"کوراسائو\",\"de\":\"Curaçao\",\"fr\":\"Curaçao\",\"it\":\"Curaçao\",\"cn\":\"库拉索\"}', 12.11666700, -68.93333300, '🇨🇼', 'U+1F1E8 U+1F1FC', '2020-10-25 14:54:20', '2021-08-01 14:37:27', 1, 'Q25279'),
(250, 'Sint Maarten (Dutch part)', 'SXM', '534', 'SX', '1721', 'Philipsburg', 'ANG', 'ƒ', '.sx', 'Sint Maarten', 'Americas', 'Caribbean', '[{\"zoneName\":\"America\\/Anguilla\",\"gmtOffset\":-14400,\"gmtOffsetName\":\"UTC-04:00\",\"abbreviation\":\"AST\",\"tzName\":\"Atlantic Standard Time\"}]', '{\"kr\":\"신트마르턴\",\"br\":\"Sint Maarten\",\"pt\":\"São Martinho\",\"nl\":\"Sint Maarten\",\"fa\":\"سینت مارتن\",\"de\":\"Sint Maarten (niederl. Teil)\",\"fr\":\"Saint Martin (partie néerlandaise)\",\"it\":\"Saint Martin (parte olandese)\",\"cn\":\"圣马丁岛（荷兰部分）\"}', 18.03333300, -63.05000000, '🇸🇽', 'U+1F1F8 U+1F1FD', '2020-12-05 13:03:39', '2021-08-01 14:37:27', 1, 'Q26273');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_boy_notifications`
--

CREATE TABLE `delivery_boy_notifications` (
  `id` int(11) NOT NULL,
  `delivery_boy_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `question` mediumtext COLLATE utf8mb4_unicode_ci,
  `answer` mediumtext COLLATE utf8mb4_unicode_ci,
  `status` char(1) COLLATE utf8mb4_unicode_ci DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_variant_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `user_id`, `product_id`, `product_variant_id`) VALUES
(1, 1, 1, 0),
(5, 1, 71, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fund_transfers`
--

CREATE TABLE `fund_transfers` (
  `id` int(11) NOT NULL,
  `delivery_boy_id` int(11) NOT NULL,
  `opening_balance` double NOT NULL,
  `closing_balance` double NOT NULL,
  `amount` double NOT NULL,
  `status` varchar(28) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'delivery_boy', 'Delivery Boys');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `language` varchar(128) DEFAULT NULL,
  `code` varchar(8) DEFAULT NULL,
  `is_rtl` tinyint(4) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `language`, `code`, `is_rtl`, `created_on`) VALUES
(1, 'English', 'en', 0, '2021-02-26 14:48:01'),
(2, 'arabic', 'ar', 1, '2021-07-26 06:29:02');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_directory` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `title`, `name`, `extension`, `type`, `sub_directory`, `size`, `date_created`) VALUES
(2557, 'faviocn', 'faviocn.png', 'png', 'image', 'uploads/media/2021/', '536', '2021-08-03 11:33:03'),
(2558, 'logo2', 'logo2.png', 'png', 'image', 'uploads/media/2021/', '21050', '2021-08-03 11:33:04'),
(2559, 'slide03', 'slide03.jpg', 'jpg', 'image', 'uploads/media/2021/', '21469', '2021-08-03 13:02:42'),
(2560, 'slide3', 'slide3.jpg', 'jpg', 'image', 'uploads/media/2021/', '14072', '2021-08-03 13:02:43'),
(2561, 'slide1', 'slide1.jpg', 'jpg', 'image', 'uploads/media/2021/', '63265', '2021-08-03 14:33:04'),
(2562, 'slide2', 'slide2.jpg', 'jpg', 'image', 'uploads/media/2021/', '75748', '2021-08-03 14:33:05'),
(2563, 'ad3', 'ad3.png', 'png', 'image', 'uploads/media/2021/', '194491', '2021-08-03 14:37:46'),
(2564, 'ad4', 'ad4.png', 'png', 'image', 'uploads/media/2021/', '167258', '2021-08-03 14:37:46'),
(2565, '1', '1.png', 'png', 'image', 'uploads/media/2021/', '53043', '2021-08-03 16:34:12'),
(2566, '2', '2.png', 'png', 'image', 'uploads/media/2021/', '28113', '2021-08-03 16:34:12'),
(2567, '3', '3.png', 'png', 'image', 'uploads/media/2021/', '34311', '2021-08-03 16:34:13'),
(2568, '4', '4.png', 'png', 'image', 'uploads/media/2021/', '38147', '2021-08-03 16:34:13'),
(2569, '5', '5.png', 'png', 'image', 'uploads/media/2021/', '37721', '2021-08-03 16:34:13'),
(2570, '6', '6.png', 'png', 'image', 'uploads/media/2021/', '31795', '2021-08-03 16:34:13'),
(2571, '7', '7.png', 'png', 'image', 'uploads/media/2021/', '30186', '2021-08-03 16:34:13'),
(2572, '8', '8.png', 'png', 'image', 'uploads/media/2021/', '25290', '2021-08-03 16:34:14'),
(2573, '9', '9.png', 'png', 'image', 'uploads/media/2021/', '30428', '2021-08-03 16:34:14'),
(2574, '10', '10.png', 'png', 'image', 'uploads/media/2021/', '30414', '2021-08-03 16:34:14'),
(2575, '11', '11.png', 'png', 'image', 'uploads/media/2021/', '34138', '2021-08-03 16:34:14'),
(2576, '12', '12.png', 'png', 'image', 'uploads/media/2021/', '25219', '2021-08-03 16:34:14'),
(2577, '13', '13.png', 'png', 'image', 'uploads/media/2021/', '35632', '2021-08-03 16:35:09'),
(2578, '14', '14.png', 'png', 'image', 'uploads/media/2021/', '30179', '2021-08-03 16:35:09'),
(2735, '000940834', '000940834.jpg', 'jpg', 'image', 'uploads/media/2021/', '104450', '2021-08-18 05:29:54'),
(2899, 'iphone-13-rru-main_thumb800', 'iphone-13-rru-main_thumb800.png', 'png', 'image', 'uploads/media/2021/', '157686', '2021-08-30 14:26:59'),
(2901, 'i1-2', 'i1-2.jpg', 'jpg', 'image', 'uploads/media/2021/', '61889', '2021-08-30 14:37:27'),
(2902, 'dhdfhdfhdh', 'dhdfhdfhdh.jpg', 'jpg', 'image', 'uploads/media/2021/', '10844', '2021-08-30 14:45:03'),
(2903, 'download', 'download.png', 'png', 'image', 'uploads/media/2021/', '34357', '2021-09-06 08:13:04'),
(2904, '564654dsaf', '564654dsaf.png', 'png', 'image', 'uploads/media/2021/', '38292', '2021-09-06 08:21:11'),
(2905, 'apple_pencil_2nd', 'apple_pencil_2nd.jpg', 'jpg', 'image', 'uploads/media/2021/', '12935', '2021-09-07 08:57:05'),
(2906, 'DJI', 'DJI.jpg', 'jpg', 'image', 'uploads/media/2021/', '12739', '2021-09-08 08:16:37'),
(2909, '000940834', '000940834.jpg', 'jpg', 'image', 'uploads/media/2021/', '104450', '2021-09-08 09:15:58'),
(2910, '000940834_2_500x600', '000940834_2_500x600.jpg', 'jpg', 'image', 'uploads/media/2021/', '39371', '2021-09-08 09:23:45'),
(2911, 'xcite_image_template_500x500_px_-_2021-04-19t124402_189_(1)', 'xcite_image_template_500x500_px_-_2021-04-19t124402_189_(1).jpg', 'jpg', 'image', 'uploads/media/2021/', '31721', '2021-09-08 11:34:01'),
(2915, '11gold', '11gold.jpg', 'jpg', 'image', 'uploads/media/2021/', '32547', '2021-09-21 11:34:36'),
(2916, '11silver-1', '11silver-1.jpg', 'jpg', 'image', 'uploads/media/2021/', '18869', '2021-09-21 11:34:36'),
(2917, '11silver-2', '11silver-2.jpg', 'jpg', 'image', 'uploads/media/2021/', '39384', '2021-09-21 11:34:37'),
(2918, '8plus-2', '8plus-2.jpg', 'jpg', 'image', 'uploads/media/2021/', '15531', '2021-09-23 12:58:40'),
(2919, '8plus-3', '8plus-3.jpg', 'jpg', 'image', 'uploads/media/2021/', '10023', '2021-09-23 12:58:40'),
(2921, '8plus-sil-1', '8plus-sil-1.jpg', 'jpg', 'image', 'uploads/media/2021/', '26587', '2021-09-23 13:57:42'),
(2922, '8plus-sil-2', '8plus-sil-2.jpg', 'jpg', 'image', 'uploads/media/2021/', '10136', '2021-09-23 13:57:42'),
(2923, '8plus-sil-3', '8plus-sil-3.jpg', 'jpg', 'image', 'uploads/media/2021/', '14659', '2021-09-23 13:57:42'),
(2924, 'Iphone_8plus', 'Iphone_8plus.jpg', 'jpg', 'image', 'uploads/media/2021/', '28913', '2021-09-23 14:18:50'),
(2925, '12-5G-bl-1', '12-5G-bl-1.jpg', 'jpg', 'image', 'uploads/media/2021/', '26283', '2021-09-23 14:55:39'),
(2926, '12-5G-bl-2', '12-5G-bl-2.jpg', 'jpg', 'image', 'uploads/media/2021/', '28026', '2021-09-23 14:55:40'),
(2927, '12-5G-wh-1', '12-5G-wh-1.jpg', 'jpg', 'image', 'uploads/media/2021/', '21729', '2021-09-23 14:55:49'),
(2928, '12-5G-wh-2', '12-5G-wh-2.jpg', 'jpg', 'image', 'uploads/media/2021/', '26676', '2021-09-23 14:55:49'),
(2929, 'slide11', 'slide11.jpg', 'jpg', 'image', 'uploads/media/2021/', '63116', '2021-09-25 06:03:29'),
(2930, 'slide21', 'slide21.jpg', 'jpg', 'image', 'uploads/media/2021/', '75320', '2021-09-25 06:03:48'),
(2931, 'slider_new', 'slider_new.jpg', 'jpg', 'image', 'uploads/media/2021/', '63643', '2021-09-25 06:23:52'),
(2932, 'slider_new1', 'slider_new1.jpg', 'jpg', 'image', 'uploads/media/2021/', '75661', '2021-09-25 06:23:52'),
(2933, 'Iphone_12_Pro_128GB_5G_Blue', 'Iphone_12_Pro_128GB_5G_Blue.jpg', 'jpg', 'image', 'uploads/media/2021/', '25945', '2021-09-25 12:21:46'),
(2934, 'Iphone_12_Pro_128GB_5G_Silver', 'Iphone_12_Pro_128GB_5G_Silver.jpg', 'jpg', 'image', 'uploads/media/2021/', '26193', '2021-09-25 12:21:46'),
(2935, 'Iphone_12_Pro_128GB_5G_Silver-2', 'Iphone_12_Pro_128GB_5G_Silver-2.jpg', 'jpg', 'image', 'uploads/media/2021/', '22271', '2021-09-25 12:21:46'),
(2936, 'Iphone_12_Pro_128GB_5G_grphite', 'Iphone_12_Pro_128GB_5G_grphite.jpg', 'jpg', 'image', 'uploads/media/2021/', '24779', '2021-09-25 12:45:28'),
(2937, 'Iphone_12_Pro_128GB_5G_gold', 'Iphone_12_Pro_128GB_5G_gold.jpg', 'jpg', 'image', 'uploads/media/2021/', '27893', '2021-09-25 12:45:29'),
(2938, 'Iphone_12_Pro_Max_256GB_5G-BLUE', 'Iphone_12_Pro_Max_256GB_5G-BLUE.jpg', 'jpg', 'image', 'uploads/media/2021/', '26627', '2021-09-25 13:59:58'),
(2939, 'Iphone_12_Pro_Max_256GB_5G-GOLD', 'Iphone_12_Pro_Max_256GB_5G-GOLD.jpg', 'jpg', 'image', 'uploads/media/2021/', '25810', '2021-09-25 13:59:58'),
(2940, 'Iphone_12_Pro_Max_256GB_5G-graphite', 'Iphone_12_Pro_Max_256GB_5G-graphite.jpg', 'jpg', 'image', 'uploads/media/2021/', '26656', '2021-09-25 13:59:59'),
(2941, 'Iphone_12_Pro_Max_256GB_5G-SILVER', 'Iphone_12_Pro_Max_256GB_5G-SILVER.jpg', 'jpg', 'image', 'uploads/media/2021/', '23665', '2021-09-25 13:59:59'),
(2942, 'Xiaomi_Redmi_Note-blue', 'Xiaomi_Redmi_Note-blue.jpg', 'jpg', 'image', 'uploads/media/2021/', '27632', '2021-09-25 15:14:28'),
(2943, 'Xiaomi_Redmi_Note-grey', 'Xiaomi_Redmi_Note-grey.jpg', 'jpg', 'image', 'uploads/media/2021/', '32461', '2021-09-25 15:14:28'),
(2944, 'Samsung_A02S', 'Samsung_A02S.jpg', 'jpg', 'image', 'uploads/media/2021/', '33828', '2021-09-25 15:54:44'),
(2945, 'Samsung_A02S-black', 'Samsung_A02S-black.jpg', 'jpg', 'image', 'uploads/media/2021/', '34065', '2021-09-25 15:54:44'),
(2946, 'xiomi', 'xiomi.jpg', 'jpg', 'image', 'uploads/media/2021/', '5807', '2021-09-25 16:00:15'),
(2947, 'sam', 'sam.jpg', 'jpg', 'image', 'uploads/media/2021/', '7547', '2021-09-25 16:00:15'),
(2948, 'samA22-black', 'samA22-black.jpg', 'jpg', 'image', 'uploads/media/2021/', '43451', '2021-09-26 08:19:45'),
(2949, 'samA22-violet', 'samA22-violet.jpg', 'jpg', 'image', 'uploads/media/2021/', '43615', '2021-09-26 08:19:45'),
(2950, 'samA22-white', 'samA22-white.jpg', 'jpg', 'image', 'uploads/media/2021/', '35956', '2021-09-26 08:19:45'),
(2951, 'a32_av', 'a32_av.jpg', 'jpg', 'image', 'uploads/media/2021/', '36684', '2021-09-26 15:23:28'),
(2952, 'a32_aw', 'a32_aw.jpg', 'jpg', 'image', 'uploads/media/2021/', '43913', '2021-09-26 15:23:28'),
(2953, 'a32_ab', 'a32_ab.jpg', 'jpg', 'image', 'uploads/media/2021/', '46662', '2021-09-26 15:23:28'),
(2954, 'a01_blue', 'a01_blue.jpg', 'jpg', 'image', 'uploads/media/2021/', '33975', '2021-09-26 15:32:51'),
(2955, 'a1_blk', 'a1_blk.jpg', 'jpg', 'image', 'uploads/media/2021/', '49460', '2021-09-26 15:32:51'),
(2956, 'a12_blk', 'a12_blk.jpg', 'jpg', 'image', 'uploads/media/2021/', '65406', '2021-09-26 15:45:02'),
(2957, 'a12_wht', 'a12_wht.jpg', 'jpg', 'image', 'uploads/media/2021/', '40607', '2021-09-26 15:45:02'),
(2958, 'a12_bl', 'a12_bl.jpg', 'jpg', 'image', 'uploads/media/2021/', '50283', '2021-09-26 15:45:02'),
(2959, 'ipad_gld', 'ipad_gld.jpg', 'jpg', 'image', 'uploads/media/2021/', '27829', '2021-09-26 15:59:57'),
(2960, 'ipad_gr', 'ipad_gr.jpg', 'jpg', 'image', 'uploads/media/2021/', '27265', '2021-09-26 15:59:57'),
(2961, 'ipad_sil', 'ipad_sil.jpg', 'jpg', 'image', 'uploads/media/2021/', '26672', '2021-09-26 15:59:57'),
(2962, 'ipad', 'ipad.jpg', 'jpg', 'image', 'uploads/media/2021/', '23247', '2021-09-26 16:05:30'),
(2963, 'dexim', 'dexim.jpg', 'jpg', 'image', 'uploads/media/2021/', '8176', '2021-10-05 09:09:49'),
(2964, 'porodo', 'porodo.jpg', 'jpg', 'image', 'uploads/media/2021/', '7647', '2021-10-05 09:15:10'),
(2965, 'baseus', 'baseus.jpg', 'jpg', 'image', 'uploads/media/2021/', '6691', '2021-10-05 09:21:10'),
(2966, 'patchworks', 'patchworks.jpg', 'jpg', 'image', 'uploads/media/2021/', '5294', '2021-10-05 09:27:13'),
(2967, 'grip2u', 'grip2u.jpg', 'jpg', 'image', 'uploads/media/2021/', '5708', '2021-10-05 10:08:52'),
(2968, 'flygrip', 'flygrip.jpg', 'jpg', 'image', 'uploads/media/2021/', '7221', '2021-10-05 10:29:52'),
(2989, 'final-check-ps-2', 'final-check-ps-2.jpg', 'jpg', 'image', 'uploads/media/2021/', '28581', '2021-11-09 07:00:08'),
(2990, '11-pro-max', '11-pro-max.jpg', 'jpg', 'image', 'uploads/media/2021/', '35591', '2021-11-09 10:59:28'),
(2991, '13-pro-max-blue', '13-pro-max-blue.jpg', 'jpg', 'image', 'uploads/media/2021/', '25118', '2021-11-09 10:59:28'),
(2992, '13-pro-max-gold', '13-pro-max-gold.jpg', 'jpg', 'image', 'uploads/media/2021/', '24060', '2021-11-09 10:59:28'),
(2993, '13-pro-max-graphite', '13-pro-max-graphite.jpg', 'jpg', 'image', 'uploads/media/2021/', '23632', '2021-11-09 10:59:28'),
(2994, 'airpods-pro', 'airpods-pro.jpg', 'jpg', 'image', 'uploads/media/2021/', '12765', '2021-11-09 10:59:28'),
(2995, '11-pro-gold', '11-pro-gold.jpg', 'jpg', 'image', 'uploads/media/2021/', '26910', '2021-11-10 08:15:43'),
(2996, '11-pro-grey', '11-pro-grey.jpg', 'jpg', 'image', 'uploads/media/2021/', '29997', '2021-11-10 08:15:43'),
(2997, '11-pro-silver', '11-pro-silver.jpg', 'jpg', 'image', 'uploads/media/2021/', '26955', '2021-11-10 08:15:43'),
(2998, 'galaxt-tab-a', 'galaxt-tab-a.jpg', 'jpg', 'image', 'uploads/media/2021/', '23384', '2021-11-10 09:44:09'),
(2999, 'ipad-9', 'ipad-9.jpg', 'jpg', 'image', 'uploads/media/2021/', '22218', '2021-11-10 09:44:09'),
(3000, 'ipad-pro-12_1-grey', 'ipad-pro-12_1-grey.jpg', 'jpg', 'image', 'uploads/media/2021/', '52020', '2021-11-10 09:44:09'),
(3001, 'ipad-pro-grey', 'ipad-pro-grey.jpg', 'jpg', 'image', 'uploads/media/2021/', '51096', '2021-11-10 09:44:09'),
(3002, 's21-plus', 's21-plus.jpg', 'jpg', 'image', 'uploads/media/2021/', '39214', '2021-11-10 09:44:09');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(2);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_subscribers`
--

CREATE TABLE `newsletter_subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `subscribed_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `newsletter_subscribers`
--

INSERT INTO `newsletter_subscribers` (`id`, `email`, `user_id`, `status`, `subscribed_date`) VALUES
(1, 'ragigopal@gmail.com', NULL, 1, '2021-09-28'),
(2, 'hashimmohammedkw@gmail.com', NULL, 1, '2021-10-24'),
(3, 'ragigopal1@gmail.com', 33, 1, '2021-10-25');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT '0',
  `image` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `title`, `message`, `type`, `type_id`, `image`, `date_sent`) VALUES
(1, 'New Product', 'New Product', 'products', 24, NULL, '2021-09-06 06:30:32');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(11) NOT NULL,
  `type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int(11) DEFAULT '0',
  `image` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `type`, `type_id`, `image`, `date_added`) VALUES
(1, 'categories', 9, 'uploads/media/2021/ad4.png', '2021-05-14 18:17:43'),
(2, 'categories', 131, 'uploads/media/2021/ad3.png', '2021-05-14 18:18:37'),
(3, 'categories', 104, 'uploads/media/2021/slide3.jpg', '2021-05-14 18:19:06'),
(4, 'categories', 141, 'uploads/media/2021/slide03.jpg', '2021-05-14 18:19:53');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `delivery_boy_id` int(11) DEFAULT NULL,
  `mobile` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` double NOT NULL,
  `delivery_charge` double DEFAULT '0',
  `wallet_balance` double DEFAULT '0',
  `total_payable` double DEFAULT NULL,
  `promo_code` varchar(28) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promo_discount` double DEFAULT NULL,
  `discount` double DEFAULT '0',
  `final_total` double DEFAULT NULL,
  `payment_method` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` mediumtext COLLATE utf8mb4_unicode_ci,
  `delivery_time` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `status` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active_status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `otp` int(11) DEFAULT '0',
  `invoiceaddress` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `delivery_boy_id`, `mobile`, `total`, `delivery_charge`, `wallet_balance`, `total_payable`, `promo_code`, `promo_discount`, `discount`, `final_total`, `payment_method`, `latitude`, `longitude`, `address`, `delivery_time`, `delivery_date`, `status`, `active_status`, `date_added`, `otp`, `invoiceaddress`) VALUES
(1, 28, NULL, '07025903270', 159.9, 0, 0, 159.9, '', 0, 0, 159.9, 'COD', '', '', 'Test area, dsf, 2, gjgj, gcfhggh', NULL, NULL, '[[\"received\",\"24-08-2021 10:39:12am\"]]', 'received', '2021-08-24 07:39:12', 736357, 'Test area, dsf, 2, gjgj, gcfhggh'),
(2, 29, NULL, '', 564.8, 0, 0, 564.8, '', 0, 0, 564.8, 'COD', NULL, NULL, 'Test area, thomson center . varandarpilly, 96, 325, ffasf', NULL, NULL, '[[\"received\",\"25-08-2021 07:58:56pm\"]]', 'received', '2021-08-25 16:58:56', 229906, 'Test area, thomson center . varandarpilly, 96, 325, ffasf'),
(4, 29, NULL, '', 1534.5, 10, 0, 1144.5, 'testPRO', 400, 0, 1144.5, 'COD', NULL, NULL, '{\"name\":\"shefiz sha\",\"area\":\"Test area\",\"street\":\"thomson center . varandarpilly\",\"block\":\"96\",\"avenue\":\"325\",\"house\":\"ffasf\",\"city\":\"Hawally\",\"mobile\":\"09995533783\",\"email\":\"shefiz.sha@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"received\",\"30-08-2021 06:02:55pm\"]]', 'received', '2021-08-30 15:02:55', 292058, '{\"name\":\"shefiz sha\",\"area\":\"Test area\",\"street\":\"thomson center . varandarpilly\",\"block\":\"96\",\"avenue\":\"325\",\"house\":\"ffasf\",\"city\":\"Hawally\",\"mobile\":\"09995533783\",\"email\":\"shefiz.sha@gmail.com\",\"type\":\"home\"}'),
(5, 1, NULL, '', 250, 10, 0, 260, '', 0, 0, 260, 'COD', NULL, NULL, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"received\",\"02-09-2021 10:19:38am\"]]', 'received', '2021-09-02 07:19:38', 521534, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(6, 1, 31, '', 159, 10, 0, 169, '', 0, 0, 169, 'COD', NULL, NULL, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"received\",\"06-09-2021 09:21:13am\"],[\"processed\",\"2021-09-06 09:22:45\"],[\"shipped\",\"2021-09-06 09:22:45\"],[\"delivered\",\"2021-09-06 09:29:10\"]]', 'delivered', '2021-09-06 06:21:13', 210526, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(7, 0, NULL, '07025903270', 249.9, 10, 0, 259.9, '', 0, 0, 259.9, 'knet', NULL, NULL, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"trtre\",\"avenue\":\"dgfdg\",\"house\":\"dfgdf\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"27-09-2021 01:07:16am\"]]', 'awaiting', '2021-09-26 22:07:16', 0, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"trtre\",\"avenue\":\"dgfdg\",\"house\":\"dfgdf\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(8, 1, NULL, '', 689.8, 10, 0, 699.8, '', 0, 0, 699.8, 'knet', NULL, NULL, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"27-09-2021 08:53:33am\"]]', 'awaiting', '2021-09-27 05:53:33', 0, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(9, 0, NULL, '07025903270', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"dsfsdg\",\"house\":\"asfa\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"27-09-2021 02:35:58pm\"]]', 'awaiting', '2021-09-27 11:35:58', 0, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"dsfsdg\",\"house\":\"asfa\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(10, 0, NULL, '07025903270', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"dsfsdg\",\"house\":\"asfa\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"27-09-2021 02:37:34pm\"]]', 'awaiting', '2021-09-27 11:37:34', 0, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"dsfsdg\",\"house\":\"asfa\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(11, 0, NULL, '07025903270', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"dsfsdg\",\"house\":\"asfa\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"27-09-2021 02:48:18pm\"]]', 'awaiting', '2021-09-27 11:48:18', 0, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"dsfsdg\",\"house\":\"asfa\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(12, 0, NULL, '07025903270', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"dsfsdg\",\"house\":\"asfa\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"27-09-2021 03:55:47pm\"]]', 'awaiting', '2021-09-27 12:55:47', 0, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"dsfsdg\",\"house\":\"asfa\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(13, 0, NULL, '07025903270', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"adsfda\",\"avenue\":\"safasf\",\"house\":\"sdf\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"office\"}', NULL, NULL, '[[\"awaiting\",\"27-09-2021 04:04:05pm\"]]', 'awaiting', '2021-09-27 13:04:05', 0, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"adsfda\",\"avenue\":\"safasf\",\"house\":\"sdf\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"office\"}'),
(14, 0, NULL, '07025903270', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"adsfda\",\"avenue\":\"safasf\",\"house\":\"sdf\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"office\"}', NULL, NULL, '[[\"awaiting\",\"27-09-2021 04:04:42pm\"]]', 'awaiting', '2021-09-27 13:04:42', 0, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"adsfda\",\"avenue\":\"safasf\",\"house\":\"sdf\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"office\"}'),
(15, 0, NULL, '07025903270', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"adsfda\",\"avenue\":\"safasf\",\"house\":\"sdf\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"office\"}', NULL, NULL, '[[\"awaiting\",\"27-09-2021 04:05:48pm\"]]', 'awaiting', '2021-09-27 13:05:48', 0, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"adsfda\",\"avenue\":\"safasf\",\"house\":\"sdf\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"office\"}'),
(16, 0, NULL, '07025903270', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"Ragi Siva\",\"area\":\"Maidan Hawally\",\"street\":\"dsf\",\"block\":\"34\",\"avenue\":\"fgdfg\",\"house\":\"asfa\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"office\"}', NULL, NULL, '[[\"awaiting\",\"27-09-2021 04:12:31pm\"]]', 'awaiting', '2021-09-27 13:12:31', 0, '{\"name\":\"Ragi Siva\",\"area\":\"Maidan Hawally\",\"street\":\"dsf\",\"block\":\"34\",\"avenue\":\"fgdfg\",\"house\":\"asfa\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"office\"}'),
(17, 0, NULL, '07025903270', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"Ragi Siva\",\"area\":\"Maidan Hawally\",\"street\":\"dsf\",\"block\":\"34\",\"avenue\":\"fgdfg\",\"house\":\"asfa\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"office\"}', NULL, NULL, '[[\"awaiting\",\"27-09-2021 04:15:38pm\"]]', 'awaiting', '2021-09-27 13:15:38', 0, '{\"name\":\"Ragi Siva\",\"area\":\"Maidan Hawally\",\"street\":\"dsf\",\"block\":\"34\",\"avenue\":\"fgdfg\",\"house\":\"asfa\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"office\"}'),
(18, 0, NULL, '07025903270', 26.9, 10, 0, 36.9, '', 0, 0, 36.9, 'knet', NULL, NULL, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"34\",\"avenue\":\"safasf\",\"house\":\"fghfgh\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"office\"}', NULL, NULL, '[[\"awaiting\",\"28-09-2021 08:15:13am\"]]', 'awaiting', '2021-09-28 05:15:13', 0, '{\"name\":\"Ragi Siva\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"34\",\"avenue\":\"safasf\",\"house\":\"fghfgh\",\"city\":null,\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"office\"}'),
(19, 0, NULL, '9946274321', 134.9, 10, 0, 144.9, '', 0, 0, 144.9, 'knet', NULL, NULL, '{\"name\":\"Iyo Dc\",\"area\":\"Test area\",\"street\":\"12cc\",\"block\":\"22\",\"avenue\":\"Xxxd\",\"house\":\"Sss\",\"city\":null,\"mobile\":\"9946274321\",\"email\":\"Ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"28-09-2021 08:30:52am\"]]', 'awaiting', '2021-09-28 05:30:52', 0, '{\"name\":\"Iyo Dc\",\"area\":\"Test area\",\"street\":\"12cc\",\"block\":\"22\",\"avenue\":\"Xxxd\",\"house\":\"Sss\",\"city\":null,\"mobile\":\"9946274321\",\"email\":\"Ragigopal@gmail.com\",\"type\":\"home\"}'),
(20, 1, NULL, '', 110.8, 10, 0, 120.8, '', 0, 0, 120.8, 'knet', NULL, NULL, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"28-09-2021 12:55:14pm\"],[\"cancelled\",\"2021-10-12 18:02:30\"]]', 'cancelled', '2021-09-28 09:55:14', 0, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(21, 1, NULL, '', 455.8, 10, 0, 465.8, '', 0, 0, 465.8, 'knet', NULL, NULL, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"02-10-2021 04:59:18pm\"]]', 'awaiting', '2021-10-02 13:59:18', 0, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(22, 32, NULL, '66329078', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"Abdul Gafoor\",\"area\":\"Maidan Hawally\",\"street\":\"34\",\"block\":\"Block 9\",\"avenue\":\"a\",\"house\":\"35\",\"city\":\"Hawally\",\"mobile\":\"66329078\",\"email\":\"\",\"type\":\"office\"}', NULL, NULL, '[[\"awaiting\",\"05-10-2021 11:35:50am\"],[\"payment_cancelled\",\"2021-10-05 11:36:24\"]]', 'payment_cancelled', '2021-10-05 08:35:50', 0, '{\"name\":\"Abdul Gafoor\",\"area\":\"Maidan Hawally\",\"street\":\"34\",\"block\":\"Block 9\",\"avenue\":\"a\",\"house\":\"35\",\"city\":\"Hawally\",\"mobile\":\"66329078\",\"email\":\"\",\"type\":\"office\"}'),
(23, 0, NULL, '0000000', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"Bdi Bdjs\",\"area\":\"Maidan Hawally\",\"street\":\"67\",\"block\":\"8\",\"avenue\":\"7\",\"house\":\"32\",\"city\":null,\"mobile\":\"0000000\",\"email\":\"Djhfk@gmai.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"05-10-2021 11:43:16am\"]]', 'awaiting', '2021-10-05 08:43:16', 0, '{\"name\":\"Bdi Bdjs\",\"area\":\"Maidan Hawally\",\"street\":\"67\",\"block\":\"8\",\"avenue\":\"7\",\"house\":\"32\",\"city\":null,\"mobile\":\"0000000\",\"email\":\"Djhfk@gmai.com\",\"type\":\"home\"}'),
(24, 0, NULL, '0000000', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'COD', NULL, NULL, '{\"name\":\"Bdi Bdjs\",\"area\":\"Maidan Hawally\",\"street\":\"67\",\"block\":\"8\",\"avenue\":\"7\",\"house\":\"32\",\"city\":null,\"mobile\":\"0000000\",\"email\":\"Djhfk@gmai.com\",\"type\":\"home\"}', NULL, NULL, '[[\"received\",\"05-10-2021 11:43:42am\"]]', 'received', '2021-10-05 08:43:42', 0, '{\"name\":\"Bdi Bdjs\",\"area\":\"Maidan Hawally\",\"street\":\"67\",\"block\":\"8\",\"avenue\":\"7\",\"house\":\"32\",\"city\":null,\"mobile\":\"0000000\",\"email\":\"Djhfk@gmai.com\",\"type\":\"home\"}'),
(25, 0, NULL, '62521354', 303.9, 10, 0, 313.9, '', 0, 0, 313.9, 'COD', NULL, NULL, '{\"name\":\"Test Altest\",\"area\":\"Test area\",\"street\":\"1\",\"block\":\"2\",\"avenue\":\"23\",\"house\":\"345\",\"city\":null,\"mobile\":\"62521354\",\"email\":\"support@eurocom.com\",\"type\":\"home\"}', NULL, NULL, '[[\"received\",\"05-10-2021 01:42:36pm\"]]', 'received', '2021-10-05 10:42:36', 0, '{\"name\":\"Test Altest\",\"area\":\"Test area\",\"street\":\"1\",\"block\":\"2\",\"avenue\":\"23\",\"house\":\"345\",\"city\":null,\"mobile\":\"62521354\",\"email\":\"support@eurocom.com\",\"type\":\"home\"}'),
(26, 0, NULL, '56251245', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"Test testinig\",\"area\":\"Test area\",\"street\":\"1\",\"block\":\"3\",\"avenue\":\"312\",\"house\":\"4234\",\"city\":null,\"mobile\":\"56251245\",\"email\":\"testing@test.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"05-10-2021 01:44:27pm\"],[\"payment_cancelled\",\"2021-10-05 13:45:10\"]]', 'payment_cancelled', '2021-10-05 10:44:27', 0, '{\"name\":\"Test testinig\",\"area\":\"Test area\",\"street\":\"1\",\"block\":\"3\",\"avenue\":\"312\",\"house\":\"4234\",\"city\":null,\"mobile\":\"56251245\",\"email\":\"testing@test.com\",\"type\":\"home\"}'),
(27, 1, NULL, '', 67.9, 10, 0, 77.9, '', 0, 0, 77.9, 'COD', NULL, NULL, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"received\",\"16-10-2021 03:18:18pm\"]]', 'received', '2021-10-16 12:18:18', 0, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(28, 0, NULL, 'fsdfsdfsdfs', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"sfsdf dsfsdf\",\"area\":\"\",\"street\":\"sdfsdf\",\"block\":\"\",\"avenue\":\"sff\",\"house\":\"sdfsdfsd\",\"city\":\"\",\"mobile\":\"fsdfsdfsdfs\",\"country\":\"Equatorial Guinea\",\"alternate_mobile\":\"\",\"email\":\"dfsd@etwtg.dfgs\",\"type\":\"\"}', NULL, NULL, '[[\"awaiting\",\"24-10-2021 03:54:26am\"]]', 'awaiting', '2021-10-24 00:54:26', 0, '{\"name\":\"sfsdf dsfsdf\",\"area\":\"\",\"street\":\"sdfsdf\",\"block\":\"\",\"avenue\":\"sff\",\"house\":\"sdfsdfsd\",\"city\":\"\",\"mobile\":\"fsdfsdfsdfs\",\"country\":\"Equatorial Guinea\",\"alternate_mobile\":\"\",\"email\":\"dfsd@etwtg.dfgs\",\"type\":\"\"}'),
(29, 1, NULL, '07025903270', 1244.5, 10, 0, 1254.5, '', 0, 0, 1254.5, 'knet', NULL, NULL, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"country\":\"\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"24-10-2021 09:54:25am\"],[\"payment_cancelled\",\"2021-10-24 09:54:36\"]]', 'payment_cancelled', '2021-10-24 06:54:25', 0, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"alternate_mobile\":\"\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(30, 1, NULL, '07025903270', 1244.5, 10, 0, 1054.5, 'Eurrocom', 200, 0, 1054.5, 'knet', NULL, NULL, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"country\":\"\",\"mobile\":\"07025903270\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}', NULL, NULL, '[[\"awaiting\",\"24-10-2021 09:55:42am\"],[\"payment_cancelled\",\"2021-10-24 09:55:51\"]]', 'payment_cancelled', '2021-10-24 06:55:42', 0, '{\"name\":\"Admin test\",\"area\":\"Test area\",\"street\":\"dsf\",\"block\":\"12\",\"avenue\":\"22\",\"house\":\"jghjg\",\"city\":\"Hawally\",\"mobile\":\"07025903270\",\"alternate_mobile\":\"\",\"email\":\"ragigopal@gmail.com\",\"type\":\"home\"}'),
(31, 33, NULL, '4334343434', 52.9, 10, 0, 52.32, 'Eurrocom', 10.58, 0, 52.32, 'knet', NULL, NULL, '{\"name\":\"ragi K g\",\"area\":\"Test area\",\"street\":\"\",\"block\":\"\",\"avenue\":\"\",\"house\":\"\",\"city\":\"Hawally\",\"country\":\"Kuwait\",\"mobile\":\"4334343434\",\"email\":\"ragigopal1@gmail.com\",\"type\":\"\"}', NULL, NULL, '[[\"awaiting\",\"25-10-2021 09:01:22am\"]]', 'awaiting', '2021-10-25 06:01:22', 0, '{\"name\":\"ragi K g\",\"area\":\"Test area\",\"street\":\"\",\"block\":\"\",\"avenue\":\"\",\"house\":\"\",\"city\":\"Hawally\",\"country\":\"Kuwait\",\"mobile\":\"4334343434\",\"email\":\"ragigopal1@gmail.com\",\"type\":\"\"}'),
(32, 33, NULL, '4334343434', 52.9, 10, 0, 62.9, '', 0, 0, 62.9, 'knet', NULL, NULL, '{\"name\":\"ragi K g\",\"area\":\"Test area\",\"street\":\"\",\"block\":\"\",\"avenue\":\"\",\"house\":\"\",\"city\":\"Hawally\",\"country\":\"Kuwait\",\"mobile\":\"4334343434\",\"email\":\"ragigopal1@gmail.com\",\"type\":\"\"}', NULL, NULL, '[[\"awaiting\",\"25-10-2021 11:07:12am\"]]', 'awaiting', '2021-10-25 08:07:12', 0, '{\"name\":\"ragi K g\",\"area\":\"Test area\",\"street\":\"\",\"block\":\"\",\"avenue\":\"\",\"house\":\"\",\"city\":\"Hawally\",\"mobile\":\"4334343434\",\"alternate_mobile\":\"\",\"email\":\"ragigopal1@gmail.com\",\"type\":\"\"}'),
(33, 0, NULL, '56586800', 0.01, 10, 0, 10.01, '', 0, 0, 10.01, 'knet', NULL, NULL, '{\"name\":\"Tester Testing\",\"area\":\"Test area\",\"street\":\"1\",\"block\":\"1\",\"avenue\":\"1\",\"house\":\"1\",\"city\":null,\"mobile\":\"56586800\",\"country\":\"Kuwait\",\"alternate_mobile\":\"\",\"email\":\"Tester@eurocom.com.kw\",\"type\":\"\"}', NULL, NULL, '[[\"awaiting\",\"26-10-2021 12:59:53pm\"]]', 'received', '2021-10-26 09:59:53', 0, '{\"name\":\"Tester Testing\",\"area\":\"Test area\",\"street\":\"1\",\"block\":\"1\",\"avenue\":\"1\",\"house\":\"1\",\"city\":null,\"mobile\":\"56586800\",\"country\":\"Kuwait\",\"alternate_mobile\":\"\",\"email\":\"Tester@eurocom.com.kw\",\"type\":\"\"}');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_name` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant_name` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_product_name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_variant_name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_variant_id` int(11) NOT NULL,
  `variant_image` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `special_price` float DEFAULT NULL,
  `discounted_price` double DEFAULT NULL,
  `tax_percent` double DEFAULT NULL,
  `tax_amount` double DEFAULT NULL,
  `discount` double DEFAULT '0',
  `sub_total` double NOT NULL,
  `deliver_by` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active_status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `user_id`, `order_id`, `product_name`, `variant_name`, `ar_product_name`, `ar_variant_name`, `product_variant_id`, `variant_image`, `quantity`, `price`, `special_price`, `discounted_price`, `tax_percent`, `tax_amount`, `discount`, `sub_total`, `deliver_by`, `status`, `active_status`, `date_added`) VALUES
(1, 28, 1, 'IPHONE SE 2ND GEN 256GB BLACK', '', '', '', 68, '', 1, 159.9, NULL, NULL, 0, 0, 0, 159.9, NULL, '[[\"received\",\"24-08-2021 10:39:12am\"]]', 'received', '2021-08-24 07:39:12'),
(2, 29, 2, 'IPHONE 12 PRO MAX 512GB GRAPHITE', '', '', '', 66, '', 1, 404.9, NULL, NULL, 0, 0, 0, 404.9, NULL, '[[\"received\",\"25-08-2021 07:58:56pm\"]]', 'received', '2021-08-25 16:58:56'),
(3, 29, 2, 'IPHONE SE 2ND GEN 256GB WHITE', '', '', '', 69, '', 1, 159.9, NULL, NULL, 0, 0, 0, 159.9, NULL, '[[\"received\",\"25-08-2021 07:58:56pm\"]]', 'received', '2021-08-25 16:58:56'),
(4, 29, 3, 'IPHONE SE 2ND GEN 256GB WHITE', '', '', '', 69, '', 2, 159.9, 0, NULL, 0, 0, 0, 319.8, NULL, '[[\"received\",\"30-08-2021 05:10:44pm\"],[\"cancelled\",\"2021-08-30 17:12:18\"]]', 'cancelled', '2021-08-30 14:10:44'),
(5, 29, 3, 'IPHONE 12 PRO 128GB GOLD 5g', '', '', '', 67, '', 2, 317.5, 0, NULL, 0, 0, 0, 635, NULL, '[[\"received\",\"30-08-2021 05:10:44pm\"],[\"cancelled\",\"2021-08-30 17:12:06\"]]', 'cancelled', '2021-08-30 14:10:44'),
(6, 29, 4, 'IPHONE 12 PRO MAX 512GB GRAPHITE', '', '', '', 66, '', 3, 404.9, 0, NULL, 0, 0, 0, 1214.7, NULL, '[[\"received\",\"30-08-2021 06:02:55pm\"]]', 'received', '2021-08-30 15:02:55'),
(7, 29, 4, 'IPHONE SE 2ND GEN 256GB BLACK', '', '', '', 68, '', 2, 159.9, 0, NULL, 0, 0, 0, 319.8, NULL, '[[\"received\",\"30-08-2021 06:02:55pm\"]]', 'received', '2021-08-30 15:02:55'),
(8, 1, 5, 'IPHONE SE 2ND GEN 256GB WHITE', '256GB', '', '', 71, '', 1, 250, 0, NULL, 0, 0, 0, 250, NULL, '[[\"received\",\"02-09-2021 10:19:38am\"]]', 'received', '2021-09-02 07:19:38'),
(9, 1, 6, 'IPHONE SE 2ND GEN 256GB WHITE', '128GB', '', '', 70, '', 1, 159, 0, NULL, 0, 0, 0, 159, NULL, '[[\"received\",\"06-09-2021 09:21:13am\"],[\"processed\",\"2021-09-06 09:22:45\"],[\"shipped\",\"2021-09-06 09:22:45\"],[\"delivered\",\"2021-09-06 09:29:10\"]]', 'delivered', '2021-09-06 06:21:13'),
(10, 0, 7, 'Apple iPhone 12 128GB 5G Phone - Blue', 'Blue', 'هاتف آيفون 12 بسعة 128 جيجابايت - 5جي - أزرق', '', 149, '', 1, 249.9, 0, NULL, 0, 0, 0, 249.9, NULL, '[[\"awaiting\",\"27-09-2021 01:07:16am\"]]', 'awaiting', '2021-09-26 22:07:16'),
(11, 1, 8, 'Apple iPhone 12 Pro Max 5G 128GB-5G', 'Blue, 256GB', 'أبل, ايفون 12 برو ماكس بسعة 128 جيجا بايت', ', ', 201, '', 1, 344.9, 0, NULL, 0, 0, 0, 344.9, NULL, '[[\"awaiting\",\"27-09-2021 08:53:33am\"]]', 'awaiting', '2021-09-27 05:53:33'),
(12, 1, 8, 'Apple iPhone 12 Pro Max 5G 128GB-5G', '256GB, Gold', 'أبل, ايفون 12 برو ماكس بسعة 128 جيجا بايت', ', ذهبي', 202, '', 1, 344.9, 0, NULL, 0, 0, 0, 344.9, NULL, '[[\"awaiting\",\"27-09-2021 08:53:33am\"]]', 'awaiting', '2021-09-27 05:53:33'),
(13, 0, 9, 'Samsung Galaxy A12', 'Black', 'سامسونج جالكسي ايه 12', '', 194, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"27-09-2021 02:35:58pm\"]]', 'awaiting', '2021-09-27 11:35:58'),
(14, 0, 10, 'Samsung Galaxy A12', 'Black', 'سامسونج جالكسي ايه 12', '', 194, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"27-09-2021 02:37:34pm\"]]', 'awaiting', '2021-09-27 11:37:34'),
(15, 0, 11, 'Samsung Galaxy A12', 'Black', 'سامسونج جالكسي ايه 12', '', 194, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"27-09-2021 02:48:18pm\"]]', 'awaiting', '2021-09-27 11:48:18'),
(16, 0, 12, 'Samsung Galaxy A12', 'Black', 'سامسونج جالكسي ايه 12', '', 194, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"27-09-2021 03:55:47pm\"]]', 'awaiting', '2021-09-27 12:55:47'),
(17, 0, 13, 'Samsung Galaxy A12', 'Black', 'سامسونج جالكسي ايه 12', '', 194, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"27-09-2021 04:04:05pm\"]]', 'awaiting', '2021-09-27 13:04:05'),
(18, 0, 14, 'Samsung Galaxy A12', 'Black', 'سامسونج جالكسي ايه 12', '', 194, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"27-09-2021 04:04:42pm\"]]', 'awaiting', '2021-09-27 13:04:42'),
(19, 0, 15, 'Samsung Galaxy A12', 'Black', 'سامسونج جالكسي ايه 12', '', 194, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"27-09-2021 04:05:48pm\"]]', 'awaiting', '2021-09-27 13:05:48'),
(20, 0, 16, 'Samsung Galaxy A12', 'Black', 'سامسونج جالكسي ايه 12', '', 194, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"27-09-2021 04:12:31pm\"]]', 'awaiting', '2021-09-27 13:12:31'),
(21, 0, 17, 'Samsung Galaxy A12', 'Black', 'سامسونج جالكسي ايه 12', '', 194, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"27-09-2021 04:15:38pm\"]]', 'awaiting', '2021-09-27 13:15:38'),
(22, 0, 18, 'Samsung Galaxy A01 Core 5.3', 'Black', 'سامسونج جالكسي إيه 01 كور 5.3 بوصة ، 16 جيجا بايت هاتف ذكي', '', 190, 'uploads/media/2021/a01_blue.jpg', 1, 26.9, 0, NULL, 0, 0, 0, 26.9, NULL, '[[\"awaiting\",\"28-09-2021 08:15:13am\"]]', 'awaiting', '2021-09-28 05:15:13'),
(23, 0, 19, 'IPAD 8th Generation 128GB 10.2', 'Silver', 'أبل أيباد الجيل الثامن', 'فضة', 197, 'uploads/media/2021/ipad.jpg', 1, 134.9, 0, NULL, 0, 0, 0, 134.9, NULL, '[[\"awaiting\",\"28-09-2021 08:30:52am\"]]', 'awaiting', '2021-09-28 05:30:52'),
(24, 1, 20, 'Samsung Galaxy A01 Core 5.3', 'Black', 'سامسونج جالكسي إيه 01 كور 5.3 بوصة ، 16 جيجا بايت هاتف ذكي', '', 190, 'uploads/media/2021/a01_blue.jpg', 1, 26.9, 0, NULL, 0, 0, 0, 26.9, NULL, '[[\"awaiting\",\"28-09-2021 12:55:14pm\"],[\"cancelled\",\"2021-10-12 18:02:30\"]]', 'cancelled', '2021-09-28 09:55:14'),
(25, 1, 20, 'Samsung Galaxy A32 5G 128GB Phone', 'Awesome Violet', 'هاتف سامسونج جالكسي ايه 32 5G بسعة 128 جيجابايت', 'البنفسجي الرائع', 186, 'uploads/media/2021/a32_ab.jpg', 1, 83.9, 0, NULL, 0, 0, 0, 83.9, NULL, '[[\"awaiting\",\"28-09-2021 12:55:14pm\"],[\"cancelled\",\"2021-10-12 18:02:30\"]]', 'cancelled', '2021-09-28 09:55:14'),
(26, 1, 21, 'Apple iPhone 11 Pro 256GB Phone - Gold', 'Silver', 'شاهد الفيديو هاتف آيفون ١١ برو بسعة ٢٥٦ جيجابايت - ذهبي', 'فضة', 144, 'uploads/media/2021/11silver-2.jpg', 1, 289.9, 0, NULL, 0, 0, 0, 289.9, NULL, '[[\"awaiting\",\"02-10-2021 04:59:18pm\"]]', 'awaiting', '2021-10-02 13:59:18'),
(27, 1, 21, 'Apple iPhone 8 Plus 128GB Phone - Gold', 'Silver', 'موبايل ابل ايفون 8 بلس 128 جيجا بايت - ذهبي', 'فضة', 148, 'uploads/media/2021/8plus-sil-1.jpg', 1, 165.9, 0, NULL, 0, 0, 0, 165.9, NULL, '[[\"awaiting\",\"02-10-2021 04:59:18pm\"]]', 'awaiting', '2021-10-02 13:59:18'),
(28, 32, 22, 'Samsung Galaxy A12', 'Blue', 'سامسونج جالكسي ايه 12', '', 192, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"05-10-2021 11:35:50am\"]]', 'awaiting', '2021-10-05 08:35:50'),
(29, 0, 23, 'Samsung Galaxy A12', 'Black', 'سامسونج جالكسي ايه 12', '', 194, 'uploads/media/2021/a12_blk.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"05-10-2021 11:43:16am\"]]', 'awaiting', '2021-10-05 08:43:16'),
(30, 0, 24, 'Samsung Galaxy A12', 'Black', 'سامسونج جالكسي ايه 12', '', 194, 'uploads/media/2021/a12_blk.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"received\",\"05-10-2021 11:43:42am\"]]', 'received', '2021-10-05 08:43:42'),
(31, 0, 25, 'Iphone 12 Pro 128GB 5G', 'Blue', 'هاتف آيفون 12 برو بسعة 128 جيجابايت - 5 جي', '', 151, 'uploads/media/2021/Iphone_12_Pro_128GB_5G_Blue.jpg', 1, 303.9, 0, NULL, 0, 0, 0, 303.9, NULL, '[[\"received\",\"05-10-2021 01:42:36pm\"]]', 'received', '2021-10-05 10:42:36'),
(32, 0, 26, 'Samsung Galaxy A12', 'Blue', 'سامسونج جالكسي ايه 12', '', 192, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"05-10-2021 01:44:27pm\"]]', 'awaiting', '2021-10-05 10:44:27'),
(33, 1, 27, 'Samsung A22 128 GB Phone', 'Black', 'هاتف سامسونج ايه 22 بسعة 128 جيجابايت', '', 181, 'uploads/media/2021/samA22-black.jpg', 1, 67.9, 0, NULL, 0, 0, 0, 67.9, NULL, '[[\"received\",\"16-10-2021 03:18:18pm\"]]', 'received', '2021-10-16 12:18:18'),
(34, 0, 28, 'Samsung Galaxy A12', 'Blue', 'سامسونج جالكسي ايه 12', '', 192, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"24-10-2021 03:54:26am\"]]', 'awaiting', '2021-10-24 00:54:26'),
(35, 1, 29, 'IPAD 8th Generation 128GB 10.2', 'Grey', 'أبل أيباد الجيل الثامن', 'رمادي', 198, 'uploads/media/2021/ipad_gr.jpg', 1, 134.9, 0, NULL, 0, 0, 0, 134.9, NULL, '[[\"awaiting\",\"24-10-2021 09:54:25am\"]]', 'awaiting', '2021-10-24 06:54:25'),
(36, 1, 29, 'IPAD 8th Generation 128GB 10.2', 'Silver', 'أبل أيباد الجيل الثامن', 'فضة', 197, 'uploads/media/2021/ipad_sil.jpg', 1, 134.9, 0, NULL, 0, 0, 0, 134.9, NULL, '[[\"awaiting\",\"24-10-2021 09:54:25am\"]]', 'awaiting', '2021-10-24 06:54:25'),
(37, 1, 29, 'Apple iPhone 12 Pro Max 5G 256GB', 'Blue', 'أبل, ايفون 12 برو ماكس بسعة 256 جيجا بايت', '', 160, 'uploads/media/2021/Iphone_12_Pro_Max_256GB_5G-BLUE.jpg', 3, 324.9, 0, NULL, 0, 0, 0, 974.7, NULL, '[[\"awaiting\",\"24-10-2021 09:54:25am\"]]', 'awaiting', '2021-10-24 06:54:25'),
(38, 1, 30, 'IPAD 8th Generation 128GB 10.2', 'Grey', 'أبل أيباد الجيل الثامن', 'رمادي', 198, 'uploads/media/2021/ipad_gr.jpg', 1, 134.9, 0, NULL, 0, 0, 0, 134.9, NULL, '[[\"awaiting\",\"24-10-2021 09:55:42am\"]]', 'awaiting', '2021-10-24 06:55:42'),
(39, 1, 30, 'IPAD 8th Generation 128GB 10.2', 'Silver', 'أبل أيباد الجيل الثامن', 'فضة', 197, 'uploads/media/2021/ipad_sil.jpg', 1, 134.9, 0, NULL, 0, 0, 0, 134.9, NULL, '[[\"awaiting\",\"24-10-2021 09:55:42am\"]]', 'awaiting', '2021-10-24 06:55:42'),
(40, 1, 30, 'Apple iPhone 12 Pro Max 5G 256GB', 'Blue', 'أبل, ايفون 12 برو ماكس بسعة 256 جيجا بايت', '', 160, 'uploads/media/2021/Iphone_12_Pro_Max_256GB_5G-BLUE.jpg', 3, 324.9, 0, NULL, 0, 0, 0, 974.7, NULL, '[[\"awaiting\",\"24-10-2021 09:55:42am\"]]', 'awaiting', '2021-10-24 06:55:42'),
(41, 33, 31, 'Samsung Galaxy A12', 'Blue', 'سامسونج جالكسي ايه 12', '', 192, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"25-10-2021 09:01:22am\"]]', 'awaiting', '2021-10-25 06:01:22'),
(42, 33, 32, 'Samsung Galaxy A12', 'Blue', 'سامسونج جالكسي ايه 12', '', 192, 'uploads/media/2021/a12_bl.jpg', 1, 52.9, 0, NULL, 0, 0, 0, 52.9, NULL, '[[\"awaiting\",\"25-10-2021 11:07:12am\"]]', 'awaiting', '2021-10-25 08:07:12'),
(43, 0, 33, 'IPAD 8th Generation 128GB 10.2', 'Gold', 'أبل أيباد الجيل الثامن', 'ذهبي', 196, 'uploads/media/2021/ipad_gld.jpg', 1, 0.01, 0, NULL, 0, 0, 0, 0.01, NULL, '[[\"awaiting\",\"26-10-2021 12:59:53pm\"]]', 'received', '2021-10-26 09:59:53');

-- --------------------------------------------------------

--
-- Table structure for table `payment_requests`
--

CREATE TABLE `payment_requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_type` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_address` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_requested` int(11) NOT NULL,
  `remarks` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `tax` double DEFAULT NULL,
  `row_order` int(11) DEFAULT '0',
  `type` varchar(34) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '0 =>''Simple_Product_Stock_Active'' 1 => "Product_Level" 1 => "Variable_Level"',
  `name` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` mediumtext COLLATE utf8mb4_unicode_ci,
  `ar_short_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `indicator` tinyint(4) DEFAULT NULL COMMENT '0 - none | 1 - veg | 2 - non-veg',
  `cod_allowed` int(11) DEFAULT '1',
  `minimum_order_quantity` int(11) DEFAULT '1',
  `quantity_step_size` int(11) DEFAULT '1',
  `total_allowed_quantity` int(11) DEFAULT NULL,
  `is_returnable` int(11) DEFAULT '0',
  `is_cancelable` int(11) DEFAULT '0',
  `cancelable_till` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` mediumtext COLLATE utf8mb4_unicode_ci,
  `other_images` mediumtext COLLATE utf8mb4_unicode_ci,
  `video_type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` text COLLATE utf8mb4_unicode_ci,
  `ar_tags` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warranty_period` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guarantee_period` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `made_in` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand` int(11) NOT NULL,
  `sku` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `availability` tinyint(4) DEFAULT NULL,
  `rating` double DEFAULT '0',
  `no_of_ratings` int(11) DEFAULT '0',
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `ar_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `specification` text CHARACTER SET utf8 NOT NULL,
  `ar_specification` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(2) DEFAULT '1',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sizechart` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `tax`, `row_order`, `type`, `stock_type`, `name`, `ar_name`, `short_description`, `ar_short_description`, `slug`, `indicator`, `cod_allowed`, `minimum_order_quantity`, `quantity_step_size`, `total_allowed_quantity`, `is_returnable`, `is_cancelable`, `cancelable_till`, `image`, `other_images`, `video_type`, `video`, `tags`, `ar_tags`, `warranty_period`, `guarantee_period`, `made_in`, `brand`, `sku`, `stock`, `availability`, `rating`, `no_of_ratings`, `description`, `ar_description`, `specification`, `ar_specification`, `status`, `date_added`, `sizechart`) VALUES
(59, 139, 0, 0, 'variable_product', '2', 'Apple iPhone 8 Plus 128GB Phone', 'موبايل ابل ايفون 8 بلس 128 جيجا بايت - ذهبي', 'Quick Overview:\r\n128GB Internal Memory, 3GB RAM\r\n5.5-inch (diagonal) widescreen LCD Multi-Touch display with IPS technology (1920 x 1080-pixel resolution at 401 ppi) Retina HD Display\r\nA11 Bionic chip with 64-bit architecture, Neural Engine, Embedded M11 motion coprocessor\r\nPrimary Camera: 12 MP ƒ/1.8 aperture | Secondary Camera: 7MP ƒ/2.2 aperture\r\niOS 11 Operating System\r\nTouch ID Fingerprint sensor', 'نبذة عن المنتج:\r\n\r\nرام 3 جيجابايت، سعة 128 جيجابايت ذاكرة تخزين داخلية\r\nشاشة عريضة 5.5 بوصة إل سي دي متعددة اللمس بتقنية آي بي إس ودقة (1920 × 1080 بكسل بمعدل 401 نقطة في البوصة) شاشة ريتينا عالية الوضوح\r\nرقاقة إلكترونية إيه 11 بيونيك مع بنية 64-بت، محرك عصبي، معالج الحركة إم 11\r\nكاميرا خلفية: 12 ميجابكسل بفتحة عدسة إف / 1.8 | كاميرا أمامية: 7 ميجابكسل بفتحة عدسة: إف / 2.2\r\nنظام التشغيل: آي أو إس 11\r\nمستشعر بصمة الإصبع باللمس', 'apple-iphone-8-plus-128gb-phone', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/Iphone_8plus.jpg', '[]', '', '', '', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '<h3 class=\"acc-content-title\" style=\"font-family: Roboto, sans-serif; line-height: 1.5; color: rgb(51, 51, 51); margin-top: 10px; margin-bottom: 10px; font-size: 1.2em; background-color: rgb(248, 248, 248);\"><span class=\"h4 title-text\" style=\"font-family: Roboto, sans-serif; line-height: 1.1; margin-top: 10px; margin-bottom: 10px; font-size: 17px;\">Information on Apple iPhone 8 Plus 128GB Phone - Gold:</span></h3><p class=\"h3\" style=\"margin: 10px 0px; font-family: Roboto, sans-serif; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\"></p><table class=\"data-table\" id=\"product-attribute-specs-table1\" style=\"border-spacing: 0px; background-color: rgb(248, 248, 248); width: 1282.93px; max-width: 100%; margin-bottom: 20px; border: 1px solid rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: Roboto, sans-serif; font-size: 14px;\"><colgroup><col width=\"25%\"><col></colgroup><tbody><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Model Number</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">MX262AH/A</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Article Number</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">623963</td></tr></tbody></table><p class=\"h3\" style=\"margin: 10px 0px; font-family: Roboto, sans-serif; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\">General</p><table class=\"data-table\" id=\"product-attribute-specs-table2\" style=\"border-spacing: 0px; background-color: rgb(248, 248, 248); width: 1282.93px; max-width: 100%; margin-bottom: 20px; border: 1px solid rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: Roboto, sans-serif; font-size: 14px;\"><colgroup><col width=\"25%\"><col></colgroup><tbody><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Brand</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">APPLE</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Colour</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Gold</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Device Type</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">iPhone 8 Plus</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Processor</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">A11 Bionic Chip + Embedded M11 Motion</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Operating System</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">iOS 11</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Sensors</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Accelerometer, Ambient Light, Barometer, Fingerprint, Proximity, Three-axis gyro</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Graphics Special Features</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">A11 Bionic</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Phone Memory (RAM)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">3 GB</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Storage Capacity</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">128GB</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Expandable Memory</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">No</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Phone Display Size</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">5.5-inch</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Display Type</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Retina HD Display 5.5-inch (diagonal) widescreen LCD Multi-Touch display with IPS technology</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Display Resolution</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">1920 x 1080 Pixels</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Rear Camera</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Phone Rear Camera</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">12 Megapixels</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Front Camera</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Front Camera Resolution</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">7 Megapixels</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Flash</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Video Recording</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">YES | 4K video recording at 24 fps, 30 fps, or 60 fps , 1080p HD video recording at 30 fps or 60 fps , 720p HD video recording at 30 fps , Optical image stabilization for video , Optical zoom; 6x digital zoom (iPhone 8 Plus only) , Quad-LED True Tone</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Other Camera Features</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Optical image stabilization , Six‑element lens , Quad-LED True Tone flash with Slow Sync , Panorama (up to 63MP) , Sapphire crystal lens cover , Backside illumination sensor , Hybrid IR filter , Autofocus with Focus Pixels , Tap to focus with Focu</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Battery Type</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Built-in rechargeable lithium-ion battery , Wireless charging (works with Qi chargers)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Battery Life (Standby Time)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Atleast 60 hours</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Battery Life (Internet Browsing / Play)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Up to 13 hours</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Battery Life (Talk Time)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Up to 21 hours (wireless)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Cellular Connectivity</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">4G LTE</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Number of SIM Cards</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Single SIM</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">SIM Size</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Nano-SIM</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Total Number of Slots</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">1</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Slot Details</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">1 SIM Card Slot</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Supported Network Types</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">EDGE, GSM, LTE</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">GPS</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">GPS Type</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Assisted GPS, GLONASS, Galileo, and QZSS</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Video Playback</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Video Formats Supported</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Video formats supported: HEVC, H.264, MPEG-4 Part 2, and Motion JPEG | Supports Dolby Vision and HDR10 content | AirPlay Mirroring, photos, and video out to Apple TV (2nd generation or later) | Video mirroring and video out support: Up to 1080p through</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Audio Playback</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Audio Formats Supported</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Audio formats supported: AAC-LC, HE-AAC, HE-AAC v2, Protected AAC, MP3, Linear PCM, Apple Lossless, FLAC, Dolby Digital (AC-3), Dolby Digital Plus (E-AC-3), and Audible (formats 2, 3, 4, Audible Enhanced Audio, AAX, and AAX+) | User-configurable maximum</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Charging Port Type</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Lightning Port</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Bluetooth</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">v5.0</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Other Connectivity Features</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">NFC with reader mode</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Dimensions (H x L x W)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">158.4 x 78.1 x 7.5 mm (6.24 x 3.07 x 0.30 in)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Weight</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">7.13 ounces (202 grams)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Special Features</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">FaceTime HD Camera , Touch ID , Apple Pay , Siri</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">In the Box</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">iPhone , EarPods with Lightning Connector , Lightning to 3.5 mm Headphone Jack Adapter , Lightning to USB Cable , USB Power Adapter , Documentation</td></tr></tbody></table>', '<h3 class=\"acc-content-title\" style=\"font-family: \"Droid Arabic Kufi\", sans-serif; font-weight: 600; line-height: 1.5; color: rgb(51, 51, 51); margin-top: 10px; margin-bottom: 10px; font-size: 1.2em; background-color: rgb(248, 248, 248);\"><span class=\"h4 title-text\" style=\"font-family: \"Droid Arabic Kufi\", sans-serif; margin-top: 10px; margin-bottom: 10px; font-size: 1.2em; line-height: 1.8 !important;\">معلومات عن موبايل ابل ايفون 8 بلس 128 جيجا بايت - ذهبي:</span></h3><p class=\"h3\" style=\"margin: 10px 0px; font-family: \"Droid Arabic Kufi\", sans-serif; font-weight: 600; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\"></p><table class=\"data-table\" id=\"product-attribute-specs-table1\" style=\"border-spacing: 0px; background-color: rgb(248, 248, 248); width: 1282.93px; max-width: 100%; margin-bottom: 20px; border: 1px solid rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: \"Droid Arabic Kufi\", sans-serif; font-size: 14px;\"><colgroup><col width=\"25%\"><col></colgroup><tbody><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">الموديل</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">MX262AH/A</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">رمز المنتج</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">623963</td></tr></tbody></table><p class=\"h3\" style=\"margin: 10px 0px; font-family: \"Droid Arabic Kufi\", sans-serif; font-weight: 600; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\">عام</p><p><span class=\"quick-ov dp-blk\" style=\"display: block; font-size: 1.2em; margin-bottom: 5px; color: rgb(51, 51, 51); font-family: \"Droid Arabic Kufi\", sans-serif;\"></span></p><table class=\"data-table\" id=\"product-attribute-specs-table2\" style=\"border-spacing: 0px; background-color: rgb(248, 248, 248); width: 1282.93px; max-width: 100%; margin-bottom: 20px; border: 1px solid rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: \"Droid Arabic Kufi\", sans-serif; font-size: 14px;\"><colgroup><col width=\"25%\"><col></colgroup><tbody><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">العلامة التجارية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">ابل</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">اللون</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">ذهبي</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">نوع الجهاز</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">ايفون 8 بلس</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">المعالج</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">A11 Bionic Chip + Embedded M11 Motion</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">نظام التشغيل</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">آي أو إس ١١</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">المستشعرات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الإضاءة المحيطة, التسارع, التقارب, بارومتر, بصمات الأصابع, ثلاثة محاور الدوران</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">مميزات خاصة للجرافيكس</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">A11 Bionic</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الذاكرة (رام)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">٣ جيجا بايت</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">سعة التخزين</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">128 جيجابايت</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">ذاكرة قابلة للزيادة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">غير متوفر</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">حجم الشاشة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">٥.٥ بوصة</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">نوع الشاشة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Retina HD Display 5.5-inch (diagonal) widescreen LCD Multi-Touch display with IPS technology</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">وضوح الشاشة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">١٩٢٠ × ١٠٨٠ بكسيل</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">كاميرا رئيسية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">متوفر</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">دقة كاميرا الهاتف الخلفية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">١٢ ميجا بكسل</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">كاميرا ثانوية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">متوفر</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">دقة الكاميرا الأمامية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">٧ ميجا بكسل</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الفلاش</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">تسجيل فيديو</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">YES | 4K video recording at 24 fps, 30 fps, or 60 fps , 1080p HD video recording at 30 fps or 60 fps , 720p HD video recording at 30 fps , Optical image stabilization for video , Optical zoom; 6x digital zoom (iPhone 8 Plus only) , Quad-LED True Tone</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">خصائص أخرى للكاميرا</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Optical image stabilization , Six‑element lens , Quad-LED True Tone flash with Slow Sync , Panorama (up to 63MP) , Sapphire crystal lens cover , Backside illumination sensor , Hybrid IR filter , Autofocus with Focus Pixels , Tap to focus with Focu</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">نوع البطارية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Built-in rechargeable lithium-ion battery , Wireless charging (works with Qi chargers)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">(مدة عمل البطارية ( ستاند باي</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Atleast 60 hours</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">(مدة عمل البطارية (للإنترنت، للعب، للتصفح</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Up to 13 hours</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">(مدة عمل البطارية (للمكالمات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Up to 21 hours (wireless)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">الاتصال الخلوي</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">٤ جي إل تي إي</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">(عدد بطاقات الخط (السيم</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">شريحة واحدة</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">(حجم البطاقة (السيم</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">نانو - سيم</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">إجمالي عدد الفتحات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">١</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">تفاصيل الفتحات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">فتحة لشريحة الإتصال</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الشبكات المدعومة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">LTE, إي دي جي إي, جي أس أم</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">(جي بي أس (نظام الملاحة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">متوفر</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">(نوع الجي بي أس (نظام الملاحة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Assisted GPS, GLONASS, Galileo, and QZSS</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">تسجيل فيديو</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">(نوع نظام الفيديو (الفورمات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Video formats supported: HEVC, H.264, MPEG-4 Part 2, and Motion JPEG | Supports Dolby Vision and HDR10 content | AirPlay Mirroring, photos, and video out to Apple TV (2nd generation or later) | Video mirroring and video out support: Up to 1080p through</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">تسجيل المقاطع الصوتية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">(نوع نظام المقطع الصوتي (الفورمات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Audio formats supported: AAC-LC, HE-AAC, HE-AAC v2, Protected AAC, MP3, Linear PCM, Apple Lossless, FLAC, Dolby Digital (AC-3), Dolby Digital Plus (E-AC-3), and Audible (formats 2, 3, 4, Audible Enhanced Audio, AAX, and AAX+) | User-configurable maximum</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">نوع مدخل الشاحن</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Lightning Port</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">بلوتوث</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">v5.0</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">ميزات الاتصال الأخرى</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">NFC with reader mode</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الأبعاد (الطول × العمق × العرض)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">158.4 x 78.1 x 7.5 mm (6.24 x 3.07 x 0.30 in)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">الوزن</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">7.13 ounces (202 grams)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">مميزات خاصة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">FaceTime HD Camera , Touch ID , Apple Pay , Siri</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">في العلبة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">iPhone , EarPods with Lightning Connector , Lightning to 3.5 mm Headphone Jack Adapter , Lightning to USB Cable , USB Power Adapter , Documentation</td></tr></tbody></table>', '', '', 1, '2021-09-23 14:03:34', NULL),
(41, 0, 0, 0, 'variable_product', '2', 'iphone 8', '', 'iphone 8', '', 'iphone-8', NULL, 1, 1, 1, 3, 0, 0, NULL, 'uploads/media/2021/000940834_2_500x600.jpg', '[]', '', '', '', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '', '', '', '', 0, '2021-09-08 09:26:51', NULL),
(42, 0, 0, 0, 'variable_product', '2', 'iphone 8', '', 'iphone 8', '', 'iphone-1', NULL, 1, 1, 1, 2, 0, 0, NULL, 'uploads/media/2021/000940834_2_500x600.jpg', '[]', '', '', '', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '', '', '', '', 0, '2021-09-08 09:29:42', NULL),
(101, 9, 0, 0, 'variable_product', '2', 'Apple iPhone 11 Pro', '', 'Apple iPhone 11 Pro', '', 'apple-iphone-11-pro', NULL, 1, 1, 1, NULL, 1, 1, 'received', 'uploads/media/2021/11-pro-silver.jpg', '[]', '', '', '', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"428\" style=\"width: 321pt;\">\r\n <colgroup><col width=\"428\" style=\"mso-width-source:userset;mso-width-alt:14941;width:321pt\">\r\n </colgroup><tbody><tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:12.5pt;width:321pt\">Triple-lens\r\n  cameras w/ new ultra wide-angle lens</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:12.5pt;width:321pt\">More\r\n  durable, water resistant body</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:12.5pt;width:321pt\">Matte\r\n  finish and new dark green color</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:12.5pt;width:321pt\">Night\r\n  Mode for better low-light images</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:12.5pt;width:321pt\">Haptic\r\n  Touch instead of 3D Touch</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:12.5pt;width:321pt\">Ultra\r\n  Wideband support</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:12.5pt;width:321pt\">A13 chip</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:12.5pt;width:321pt\">Faster\r\n  WiFi and LTE</td>\r\n </tr></tbody></table>', '', '<p><br></p><table class=\"table table-bordered\"><tbody><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"428\" style=\"width: 321pt;\">\r\n <colgroup><col width=\"428\" style=\"mso-width-source:userset;mso-width-alt:14941;width:321pt\">\r\n </colgroup><tbody><tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:13.0pt;width:321pt\"><span style=\"min-width: 150px; display: inline-block;\">Brand:<font class=\"font5\"> Apple</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:13.0pt;width:321pt\"><span style=\"min-width: 150px; display: inline-block;\">Model:<font class=\"font5\"> iPhone 11 pro</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:13.0pt;width:321pt\"><span style=\"min-width: 150px; display: inline-block;\">Processor:<font class=\"font5\"> A13 Bionic chip Third generation Neural Engine</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:13.0pt;width:321pt\"><span style=\"min-width: 150px; display: inline-block;\">Internal\r\n  Memory:<font class=\"font5\"> 256 GB</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:13.0pt;width:321pt\"><span style=\"min-width: 150px; display: inline-block;\">Display:<font class=\"font5\"> 5.8inch (diagonal) all screen OLED Multi Touch display</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:13.0pt;width:321pt\"><span style=\"min-width: 150px; display: inline-block;\">Display\r\n  Resolution:<font class=\"font5\"> 2436‑by‑1125-pixel resolution at 458 pp</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:13.0pt;width:321pt\"><span style=\"min-width: 150px; display: inline-block;\">Camera\r\n  Rear:<font class=\"font5\"> 12MP (f/1.8) + 12MP (f/2.4) + 12MP (f/2.0)</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:13.0pt;width:321pt\"><span style=\"min-width: 150px; display: inline-block;\">Camera\r\n  Front:<font class=\"font5\"> 12MP (f/2.2)</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:13.0pt;width:321pt\"><span style=\"min-width: 150px; display: inline-block;\">Connectivity:<font class=\"font5\"> Wifi, 4G LTE</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"428\" style=\"height:13.0pt;width:321pt\"><span style=\"min-width: 150px; display: inline-block;\">Feature:<font class=\"font5\"> Splash Water and Dust Resistance</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl62\" width=\"428\" style=\"height:12.5pt;width:321pt\"><span style=\"min-width: 150px; display: inline-block;\">Operating\r\n  System:<font class=\"font5\"> IOS 13</font></span></td>\r\n </tr></tbody></table></td></tr><tr><td><br></td></tr></tbody></table><br>', '', 1, '2021-11-10 08:49:52', NULL);
INSERT INTO `products` (`id`, `category_id`, `tax`, `row_order`, `type`, `stock_type`, `name`, `ar_name`, `short_description`, `ar_short_description`, `slug`, `indicator`, `cod_allowed`, `minimum_order_quantity`, `quantity_step_size`, `total_allowed_quantity`, `is_returnable`, `is_cancelable`, `cancelable_till`, `image`, `other_images`, `video_type`, `video`, `tags`, `ar_tags`, `warranty_period`, `guarantee_period`, `made_in`, `brand`, `sku`, `stock`, `availability`, `rating`, `no_of_ratings`, `description`, `ar_description`, `specification`, `ar_specification`, `status`, `date_added`, `sizechart`) VALUES
(97, 10, 0, 0, 'simple_product', '0', 'Apple AirPods Pro', '', 'Apple AirPods Pro', '', 'apple-airpods-pro-1', NULL, 1, 1, 1, NULL, 1, 1, 'received', 'uploads/media/2021/airpods-pro.jpg', '[]', '', '', '', '', '', '', NULL, 3, '0100011187', 28, 1, 0, 0, '<ul style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; list-style: none; color: rgb(51, 51, 51); font-family: Roboto, sans-serif; font-size: 14px;\"><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Active Noise Cancellation</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Dual beamforming microphones</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Transparency mode</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Amazing sound quality with Adaptive EQ</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Motion and speech detecting accelerometer</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">H1 Chip processor</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Sweat and water resistant (IPX4)</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Automatically on, automatically connected</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Easy setup for all your Apple devices</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Charging Case works with Qi-certified chargers or the Lightning connector</li></ul>', '', '<p><br></p><table class=\"table table-bordered\"><tbody><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"400\" style=\"width: 300pt; border-spacing: 0px; max-width: 100%;\">\r\n <colgroup><col width=\"205\" style=\"mso-width-source:userset;mso-width-alt:7168;width:154pt\">\r\n <col width=\"195\" style=\"mso-width-source:userset;mso-width-alt:6795;width:146pt\">\r\n </colgroup><tbody><tr height=\"17\" style=\"height: 13pt;\">\r\n  <td height=\"17\" class=\"xl60\" width=\"205\" style=\"height:13.0pt;width:154pt\">Model\r\n  Number</td>\r\n  <td class=\"xl61\" align=\"left\" width=\"195\" style=\"border-left: none; width: 146pt;\">MWP22ZE/A</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height: 13pt;\">\r\n  <td height=\"17\" class=\"xl62\" width=\"205\" style=\"height:13.0pt;border-top:none;\r\n  width:154pt\">Brand</td>\r\n  <td class=\"xl63\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">APPLE</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl60\" width=\"205\" style=\"height:13.0pt;border-top:none;\r\n  width:154pt\">Colour</td>\r\n  <td class=\"xl61\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">White</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height: 13pt;\">\r\n  <td height=\"17\" class=\"xl62\" width=\"205\" style=\"height:13.0pt;border-top:none;\r\n  width:154pt\">Accessory For</td>\r\n  <td class=\"xl63\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">Phones</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height: 13pt;\">\r\n  <td height=\"17\" class=\"xl60\" width=\"205\" style=\"height:13.0pt;border-top:none;\r\n  width:154pt\">Product Type</td>\r\n  <td class=\"xl61\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">Earphones</td>\r\n </tr>\r\n <tr height=\"169\" style=\"height: 126.5pt;\">\r\n  <td height=\"169\" class=\"xl62\" width=\"205\" style=\"height:126.5pt;border-top:none;\r\n  width:154pt\">Compatibility</td>\r\n  <td class=\"xl63\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">40mm Apple Watch, 42mm Apple Watch, 44mm\r\n  Apple Watch, Apple Tv (4th Generation), iPad, iPad 3, iPad 4, iPad Air, iPad\r\n  Air 2, iPad Mini, Ipad Mini 3, iPad Mini 4, iPad Mini Pro, iPad Mini Retina,\r\n  iPad Pro, iPhone 11, iPhone 11 Pro, iPhone 11 Pro Max, iPhone 4, iPhone 4S,\r\n  iPhone 5, iPhone 5C, iPhone 5S, iPhone 6, iPhone 6 Plus, iPhone 6.1, iPhone\r\n  6S , iPhone 6S Plus, iPhone 7, iPhone 7 Plus, iPhone 8, iPhone 8 Plus, iPhone\r\n  SE, iPhone X, iPhone XR, iPhone XS, iPhone XS MAX, iPod Mini, iPod Nano, iPod\r\n  Shuffle, iPod Touch, Mac Mini</td>\r\n </tr>\r\n <tr height=\"85\" style=\"height: 63.5pt;\">\r\n  <td height=\"85\" class=\"xl60\" width=\"205\" style=\"height:63.5pt;border-top:none;\r\n  width:154pt\">OS Compatibility</td>\r\n  <td class=\"xl61\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">iPhone and iPod touch models with the\r\n  latest version of iOS. iPad models with the latest version of iPadOS .Apple\r\n  Watch models with the latest version of watchOS. Mac models with the latest\r\n  version of macOS. Apple TV models with the latest version of tvOS</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height: 13pt;\">\r\n  <td height=\"17\" class=\"xl62\" width=\"205\" style=\"height:13.0pt;border-top:none;\r\n  width:154pt\">Connection</td>\r\n  <td class=\"xl63\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">True Wireless</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height: 13pt;\">\r\n  <td height=\"17\" class=\"xl60\" width=\"205\" style=\"height:13.0pt;border-top:none;\r\n  width:154pt\">Connection Details</td>\r\n  <td class=\"xl61\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">Bluetooth 5.0</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height: 13pt;\">\r\n  <td height=\"17\" class=\"xl62\" width=\"205\" style=\"height:13.0pt;border-top:none;\r\n  width:154pt\">Water Resistant</td>\r\n  <td class=\"xl63\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">No</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height: 13pt;\">\r\n  <td height=\"17\" class=\"xl60\" width=\"205\" style=\"height:13.0pt;border-top:none;\r\n  width:154pt\">Water Proof</td>\r\n  <td class=\"xl61\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">No</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height: 13pt;\">\r\n  <td height=\"17\" class=\"xl62\" width=\"205\" style=\"height:13.0pt;border-top:none;\r\n  width:154pt\">Noise Cancellation</td>\r\n  <td class=\"xl63\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">No</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height: 13pt;\">\r\n  <td height=\"17\" class=\"xl60\" width=\"205\" style=\"height:13.0pt;border-top:none;\r\n  width:154pt\">Microphone</td>\r\n  <td class=\"xl61\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">Dual beamforming microphones</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height: 13pt;\">\r\n  <td height=\"17\" class=\"xl62\" width=\"205\" style=\"height:13.0pt;border-top:none;\r\n  width:154pt\">Battery Life</td>\r\n  <td class=\"xl63\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">Up to 20 hours</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height: 13pt;\">\r\n  <td height=\"17\" class=\"xl60\" width=\"205\" style=\"height:13.0pt;border-top:none;\r\n  width:154pt\">Weight</td>\r\n  <td class=\"xl61\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">Weight: 1.61 ounces (45.6 grams)</td>\r\n </tr>\r\n <tr height=\"37\" style=\"height: 27.5pt;\">\r\n  <td height=\"37\" class=\"xl62\" width=\"205\" style=\"height:27.5pt;border-top:none;\r\n  width:154pt\">Dimensions</td>\r\n  <td class=\"xl63\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">Height: 1.78 inches (45.2 mm) Width: 2.39\r\n  inches (60.6 mm) Depth: 0.85 inch (21.7 mm)</td>\r\n </tr>\r\n <tr height=\"37\" style=\"height: 27.5pt;\">\r\n  <td height=\"37\" class=\"xl60\" width=\"205\" style=\"height:27.5pt;border-top:none;\r\n  width:154pt\">In the Box</td>\r\n  <td class=\"xl61\" align=\"left\" width=\"195\" style=\"border-top: none; border-left: none; width: 146pt;\">AirPods Pro Wireless Charging Case\r\n  Silicone ear tips (three sizes) Lightning to USB-C Cable Documentation</td>\r\n </tr></tbody></table></td><td></td></tr></tbody></table><p><br></p>', '', 1, '2021-11-10 06:45:01', NULL),
(99, 79, 0, 0, 'variable_product', '2', 'Apple iPhone 11 Pro Max', '', 'Apple iPhone 11 Pro Max', '', 'apple-iphone-11-pro-max', NULL, 1, 1, 1, NULL, 1, 1, 'received', 'uploads/media/2021/11-pro-max.jpg', '[]', '', '', '', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"333\" style=\"width: 250pt;\">\r\n <colgroup><col width=\"333\" style=\"mso-width-source:userset;mso-width-alt:11636;width:250pt\">\r\n </colgroup><tbody><tr height=\"27\" style=\"height:20.0pt\">\r\n  <td height=\"27\" class=\"xl61\" width=\"333\" style=\"height:20.0pt;width:250pt\">Triple-lens\r\n  cameras w/ new ultra wide-angle lens</td>\r\n </tr>\r\n <tr height=\"27\" style=\"height:20.0pt\">\r\n  <td height=\"27\" class=\"xl61\" width=\"333\" style=\"height:20.0pt;width:250pt\">More\r\n  durable, water resistant body</td>\r\n </tr>\r\n <tr height=\"27\" style=\"height:20.0pt\">\r\n  <td height=\"27\" class=\"xl61\" width=\"333\" style=\"height:20.0pt;width:250pt\">Matte\r\n  finish and new dark green color</td>\r\n </tr>\r\n <tr height=\"27\" style=\"height:20.0pt\">\r\n  <td height=\"27\" class=\"xl61\" width=\"333\" style=\"height:20.0pt;width:250pt\">Night\r\n  Mode for better low-light images</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"333\" style=\"height:12.5pt;width:250pt\">Haptic\r\n  Touch instead of 3D Touch</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"333\" style=\"height:12.5pt;width:250pt\">Ultra\r\n  Wideband support</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"333\" style=\"height:12.5pt;width:250pt\">A13 chip</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"333\" style=\"height:12.5pt;width:250pt\">Faster\r\n  WiFi and LTE</td>\r\n </tr></tbody></table>', '', '<p><p><br></p><table class=\"table table-bordered\"><tbody><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"421\" style=\"width: 316pt;\"><p><colgroup><col width=\"421\" style=\"mso-width-source:userset;mso-width-alt:14685;width:316pt\">\r\n </colgroup><tbody><tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"421\" style=\"height:13.0pt;width:316pt\"><span style=\"min-width: 150px; display: inline-block;\">Brand:<font class=\"font5\">&nbsp;Apple</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"421\" style=\"height:13.0pt;width:316pt\"><span style=\"min-width: 150px; display: inline-block;\">Model:<font class=\"font5\">&nbsp;iPhone 11 Pro Max</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"421\" style=\"height:13.0pt;width:316pt\"><span style=\"min-width: 150px; display: inline-block;\">Processor:<font class=\"font5\">&nbsp;Apple A13 Bionic chip</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"421\" style=\"height:13.0pt;width:316pt\"><span style=\"min-width: 150px; display: inline-block;\">Internal\r\n  Memory:<font class=\"font5\">&nbsp;64 GB</font></span></td>\r\n </tr>\r\n <tr height=\"25\" style=\"height:18.5pt\">\r\n  <td height=\"25\" class=\"xl61\" width=\"421\" style=\"height:18.5pt;width:316pt\"><span style=\"min-width: 150px; display: inline-block;\">Display:<font class=\"font5\">&nbsp;6.5 inch (diagonal) all screen OLED Multi Touch display</font></span></td>\r\n </tr>\r\n <tr height=\"25\" style=\"height:18.5pt\">\r\n  <td height=\"25\" class=\"xl61\" width=\"421\" style=\"height:18.5pt;width:316pt\"><span style=\"min-width: 150px; display: inline-block;\">Camera\r\n  Rear:<font class=\"font5\">&nbsp;12MP (f/1.8) + 12MP (f/2.4) + 12MP (f/2.0)\r\n  Rear Camera</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"421\" style=\"height:13.0pt;width:316pt\"><span style=\"min-width: 150px; display: inline-block;\">Camera\r\n  Front:<font class=\"font5\">&nbsp;12MP (f/2.2) Front Camera</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:13.0pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"421\" style=\"height:13.0pt;width:316pt\"><span style=\"min-width: 150px; display: inline-block;\">Connectivity:<font class=\"font5\">&nbsp;Wifi, 4G LTE</font></span></td>\r\n </tr>\r\n <tr height=\"25\" style=\"height:18.5pt\">\r\n  <td height=\"25\" class=\"xl61\" width=\"421\" style=\"height:18.5pt;width:316pt\"><span style=\"min-width: 150px; display: inline-block;\">Feature:<font class=\"font5\">&nbsp;Splash Water and Dust Resistance</font></span></td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl62\" width=\"421\" style=\"height:12.5pt;width:316pt\"><span style=\"min-width: 150px; display: inline-block;\">Operating\r\n  System:<font class=\"font5\">&nbsp;IOS 13</font></span></td>\r\n </tr></tbody></p></table></td></tr><tr><td><br></td></tr></tbody></table></p>', '', 1, '2021-11-10 07:01:49', NULL),
(95, 169, 0, 0, 'variable_product', '2', 'Apple iPhone 13 Pro Max', '', 'iPhone 13 Pro Max', '', 'apple-iphone-13-pro-max', NULL, 1, 1, 1, NULL, 1, 1, 'received', 'uploads/media/2021/13-pro-max-blue.jpg', '[]', '', '', '', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '<ul style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; list-style: none; color: rgb(51, 51, 51); font-family: Roboto, sans-serif; font-size: 14px;\"><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">6.7-inch Super Retina XDR display with ProMotion for a faster, more responsive feel</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Cinematic mode adds shallow depth of field and shifts focus automatically in your videos</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Pro camera system with new 12MP Telephoto, Wide, and Ultra Wide cameras; LiDAR Scanner; 6x optical zoom range; macro photography; Photographic Styles, ProRes video, Smart HDR 4, Night mode, Apple ProRAW, 4K Dolby Vision HDR recording</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">12MP TrueDepth front camera with Night mode, 4K Dolby Vision HDR recording</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">A15 Bionic chip for lightning-fast performance</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Up to 28 hours of video playback, the best battery life ever in an iPhone</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Durable design with Ceramic Shield</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">Industry-leading IP68 water resistance</li><li style=\"line-height: 1.5; font-size: 1em !important; color: rgb(119, 119, 119) !important;\">5G for superfast downloads and high-quality streaming,</li></ul>', '', '<p><br></p><table class=\"table table-bordered\"><tbody><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"400\" style=\"width: 300pt;\">\r\n <colgroup><col width=\"205\" style=\"mso-width-source:userset;mso-width-alt:7168;width:154pt\">\r\n <col width=\"195\" style=\"mso-width-source:userset;mso-width-alt:6795;width:146pt\">\r\n </colgroup><tbody><tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"205\" style=\"height:12.5pt;width:154pt\">Model\r\n  Number</td>\r\n  <td class=\"xl62\" width=\"195\" style=\"border-left:none;width:146pt\">MLLJ3AA/A</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Brand</td>\r\n  <td class=\"xl62\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">APPLE</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl63\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Device Type</td>\r\n  <td class=\"xl64\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">iPhone\r\n  13 Pro Max</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Processor</td>\r\n  <td class=\"xl62\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">A15\r\n  Bionic</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl63\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Operating System</td>\r\n  <td class=\"xl64\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">iOS\r\n  15</td>\r\n </tr>\r\n <tr height=\"31\" style=\"mso-height-source:userset;height:23.0pt\">\r\n  <td height=\"31\" class=\"xl61\" width=\"205\" style=\"height:23.0pt;border-top:none;\r\n  width:154pt\">Sensors</td>\r\n  <td class=\"xl62\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">Accelerometer,\r\n  Ambient Light, Barometer, Face ID, Gyro, Proximity</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl63\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Phone Memory (RAM)</td>\r\n  <td class=\"xl64\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">6\r\n  GB</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Storage Capacity</td>\r\n  <td class=\"xl62\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">128GB</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl63\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Expandable Memory</td>\r\n  <td class=\"xl64\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">No</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Phone Display Size</td>\r\n  <td class=\"xl62\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">6.7-inch</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl63\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Rear Camera</td>\r\n  <td class=\"xl64\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">Yes</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Phone Rear Camera</td>\r\n  <td class=\"xl62\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">12MP\r\n  + 12MP + 12MP</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl63\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Front Camera</td>\r\n  <td class=\"xl64\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">Yes</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl61\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Front Camera Resolution</td>\r\n  <td class=\"xl62\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">12\r\n  Megapixels</td>\r\n </tr>\r\n <tr height=\"35\" style=\"mso-height-source:userset;height:26.0pt\">\r\n  <td height=\"35\" class=\"xl63\" width=\"205\" style=\"height:26.0pt;border-top:none;\r\n  width:154pt\">Video Recording</td>\r\n  <td class=\"xl64\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">Cinematic\r\n  mode for recording videos with shallow depth of field (1080p at 30 fps)</td>\r\n </tr>\r\n <tr height=\"21\" style=\"mso-height-source:userset;height:15.5pt\">\r\n  <td height=\"21\" class=\"xl61\" width=\"205\" style=\"height:15.5pt;border-top:none;\r\n  width:154pt\">Cellular Connectivity</td>\r\n  <td class=\"xl62\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">5G</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl63\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">Wi-Fi</td>\r\n  <td class=\"xl64\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">Yes</td>\r\n </tr>\r\n <tr height=\"17\" style=\"height:12.5pt\">\r\n  <td height=\"17\" class=\"xl63\" width=\"205\" style=\"height:12.5pt;border-top:none;\r\n  width:154pt\">GPS</td>\r\n  <td class=\"xl64\" width=\"195\" style=\"border-top:none;border-left:none;width:146pt\">Yes</td>\r\n </tr></tbody></table></td><td><br></td></tr><tr><td></td><td><br></td></tr></tbody></table><br>', '', 1, '2021-11-09 11:22:32', NULL),
(61, 76, 0, 0, 'variable_product', '2', 'Iphone 12 Pro 128GB 5G', 'هاتف آيفون 12 برو بسعة 128 جيجابايت - 5 جي', 'Display: 6.1“ Super Retina XDR OLED, 1170x2532 Pixel\r\nMemory: 128GB/6GB RAM\r\nCamera: 12MP (f/2.4) + 12MP (f/1.6) + 12MP (f/2.0)\r\nSecondary: 12MP (f/2.0)\r\nCPU: Hexa-core\r\nOS: iOS 14\r\nBattery: Non Removable Li-Po 2775 mAh\r\nSensors: Fingerprint (rear-mounted), accelerometer, proximity', 'نبذة عن المنتج:\r\nالشاشة: 6.1 بوصة Super Retina XDR  أوليد، 1170 × 2532 بكسل\r\nالذاكرة: 128 جيجا بايت / 6 جيجا رام\r\nالكاميرا: 12 ميجابكسل (f / 2.4) + 12 ميجابكسل (f / 1.6) + 12 ميجابكسل (f / 2.0)\r\nالثانوية: 12 ميجابيكسل (f / 2.0)\r\nوحدة المعالجة المركزية: سداسي النواة\r\nنظام التشغيل: iOS 14\r\nالبطارية: غير قابلة للازالة Li-Po 2775 مللي أمبير', 'iphone-12-pro-128gb-5g', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/Iphone_12_Pro_128GB_5G_Blue.jpg', '[]', '', '', '', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '', '', '', '', 1, '2021-09-25 12:40:46', NULL),
(62, 37, 0, 0, 'variable_product', '2', 'Apple iPhone 12 Pro Max 5G 256GB', 'أبل, ايفون 12 برو ماكس بسعة 256 جيجا بايت', 'Super Retina XDR display\r\n\r\n6.7‑inch (diagonal) all‑screen OLED display\r\n\r\n2778‑by‑1284-pixel resolution at 458 ppi\r\n\r\n2,000,000:1 contrast ratio (typical)\r\n\r\n800 nits max brightness (typical)\r\n\r\n1200 nits max brightness (HDR)\r\n\r\nFingerprint-resistant oleophobic coating\r\n\r\nRated IP68 Splash, Water, and Dust Resistant\r\n\r\nHDR video recording with Dolby Vision up to 60 fps\r\n\r\n4K video recording at 24 fps, 30 fps, or 60 fps\r\n\r\n1080p HD video recording at 30 fps or 60 fps\r\n\r\n720p HD video recording at 30 fps\r\n\r\nVideo playback Up to 17 hours\r\n\r\nAudio playback Up to 65 hours', 'الميزات الرئيسية\r\nاللون : Silver\r\nالوزن : 228 g\r\nنظام التشغيل : IOS\r\nالدقة : 2778 x 1284\r\nالسعة : 256GB\r\nالنوع : iPhone 12 Pro Max\r\nالإتصال : 5G\r\nحجم الشاشة : 6.7”\r\nالكاميرا الامامية : 12MP\r\nالكاميرا الخلفية : 12MP + 12MP + 12MP\r\nمعالج : Apple A14 Bionic', 'apple-iphone-12-pro-max-5g-256gb', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/Iphone_12_Pro_Max_256GB_5G-graphite.jpg', '[]', '', '', '', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '', '', '', '', 1, '2021-09-25 14:05:53', NULL),
(63, 37, 0, 0, 'variable_product', '2', 'Apple iPhone 12 Pro Max 5G 128GB', 'أبل, ايفون 12 برو ماكس بسعة 128 جيجا بايت', 'Key Features\r\nColor : Gold\r\nWeight : 228 g\r\nOS : IOS\r\nResolution : 2778 x 1284\r\nCapacity : 128GB\r\nProduct Type : iPhone 12 Pro Max\r\nConnectivity : 5G\r\nScreen Size : 6.7”\r\nFront Camera : 12MP\r\nRear Camera : 12MP + 12MP + 12MP\r\nProcessor : Apple A14 Bioni', 'Key Features\r\nColor : Gold\r\nWeight : 228 g\r\nOS : IOS\r\nResolution : 2778 x 1284\r\nCapacity : 128GB\r\nProduct Type : iPhone 12 Pro Max\r\nConnectivity : 5G\r\nScreen Size : 6.7”\r\nFront Camera : 12MP\r\nRear Camera : 12MP + 12MP + 12MP\r\nProcessor : Apple A14 Bioni', 'apple-iphone-12-pro-max-5g-128gb-1', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/Iphone_12_Pro_128GB_5G_gold.jpg', '[]', '', '', '', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '', '', '', '', 1, '2021-09-25 14:43:08', NULL),
(64, 37, 0, 0, 'variable_product', '2', 'Iphone 12 Pro Max', 'Iphone 12 Pro Max', 'Iphone 12 Pro Max', 'Iphone 12 Pro Max', 'iphone-12-pro-max', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/Iphone_12_Pro_Max_256GB_5G-BLUE.jpg', '[]', '', '', '', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '', '', '', '', 1, '2021-09-25 15:04:04', NULL),
(66, 140, 0, 0, 'variable_product', '2', 'Xiaomi Redmi Note 10 4/64GB', 'شاومي ريدمي نوت 10', 'Xiaomi Redmi Note 10 4/64GB', 'شاومي ريدمي نوت 10', 'xiaomi-redmi-note-10-464gb-1', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/Xiaomi_Redmi_Note-blue.jpg', '[]', '', '', '', '', '', '', NULL, 9, NULL, NULL, NULL, 0, 0, '', '', '', '', 1, '2021-09-25 15:27:33', NULL),
(67, 95, 0, 0, 'variable_product', '2', 'Samsung A02S 64Gb', 'هاتف سامسونج جالكسي ايه 02 إس بسعة 64 جيجابايت', 'Samsung A02S 64Gb', 'هاتف سامسونج جالكسي ايه 02 إس بسعة 64 جيجابايت', 'samsung-a02s-64gb-1', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/Samsung_A02S.jpg', '[]', '', '', '', '', '', '', NULL, 8, NULL, NULL, NULL, 0, 0, '', '', '', '', 1, '2021-09-25 15:56:12', NULL),
(68, 95, 0, 0, 'variable_product', '2', 'Samsung A22 128 GB Phone', 'هاتف سامسونج ايه 22 بسعة 128 جيجابايت', 'Samsung A22 128 GB Phone', 'هاتف سامسونج ايه 22 بسعة 128 جيجابايت', 'samsung-a22-128-gb-phone', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/samA22-black.jpg', '[]', '', '', '', '', '', '', NULL, 8, NULL, NULL, NULL, 0, 0, '', '', '', '', 1, '2021-09-26 08:24:16', NULL),
(69, 95, 0, 0, 'variable_product', '2', 'Samsung Galaxy A32 5G 128GB Phone', 'هاتف سامسونج جالكسي ايه 32 5G بسعة 128 جيجابايت', 'Samsung Galaxy A32 5G 128GB Phone', 'هاتف سامسونج جالكسي ايه 32 5G بسعة 128 جيجابايت', 'samsung-galaxy-a32-5g-128gb-phone', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/a32_ab.jpg', '[]', '', '', '', '', '', '', NULL, 8, NULL, NULL, NULL, 0, 0, '', '', '', '', 1, '2021-09-26 15:26:52', NULL),
(70, 95, 0, 0, 'variable_product', '2', 'Samsung Galaxy A01 Core 5.3', 'سامسونج جالكسي إيه 01 كور 5.3 بوصة ، 16 جيجا بايت هاتف ذكي', 'Samsung Galaxy A01 Core 5.3\", 16GB Smartphone', 'سامسونج جالكسي إيه 01 كور 5.3 بوصة ، 16 جيجا بايت هاتف ذكي', 'samsung-galaxy-a01-core-1', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/a01_blue.jpg', '[]', '', '', '', '', '', '', NULL, 8, NULL, NULL, NULL, 0, 0, '', '', '', '', 1, '2021-09-26 15:34:55', NULL),
(71, 95, 0, 0, 'variable_product', '2', 'Samsung Galaxy A12', 'سامسونج جالكسي ايه 12', 'Samsung Galaxy A12', 'سامسونج جالكسي ايه 12', 'samsung-galaxy-a12', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/a12_bl.jpg', '[]', '', '', '', '', '', '', NULL, 8, NULL, NULL, NULL, 0, 0, '', '', '', '', 1, '2021-09-26 15:46:32', NULL),
(72, 12, 0, 0, 'variable_product', '2', 'IPAD 8th Generation 128GB 10.2', 'أبل أيباد الجيل الثامن', 'IPAD 8th Generation 128GB 10.2\" WIFI', 'أبل أيباد الجيل الثامن', 'ipad-8th-generation-128gb-1', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/ipad.jpg', '[]', '', '', '', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '', '', '', '', 1, '2021-09-26 16:02:08', NULL);
INSERT INTO `products` (`id`, `category_id`, `tax`, `row_order`, `type`, `stock_type`, `name`, `ar_name`, `short_description`, `ar_short_description`, `slug`, `indicator`, `cod_allowed`, `minimum_order_quantity`, `quantity_step_size`, `total_allowed_quantity`, `is_returnable`, `is_cancelable`, `cancelable_till`, `image`, `other_images`, `video_type`, `video`, `tags`, `ar_tags`, `warranty_period`, `guarantee_period`, `made_in`, `brand`, `sku`, `stock`, `availability`, `rating`, `no_of_ratings`, `description`, `ar_description`, `specification`, `ar_specification`, `status`, `date_added`, `sizechart`) VALUES
(57, 80, 0, 0, 'variable_product', '2', 'Apple iPhone 11 Pro 256GB Phone', 'شاهد الفيديو هاتف آيفون ١١ برو بسعة ٢٥٦ جيجابايت - ذهبي', 'Apple A13 Bionic (7 nm+) Processor\r\n4GB RAM, 256GB Internal Memory\r\n5.8‑inch (1125 x 2436) Super Retina XDR OLED Capacitive Touchscreen Display\r\nPrimary Camera: 12 MP, f/1.8, 26mm (wide) + 12 MP, f/2.0, 52mm (telephoto) + 12 MP, f/2.4, 13mm (ultrawide)\r\nSelfie Camera: 12 MP, f/2.2 Front Camera\r\n4G LTE + Wi-Fi + Bluetooth + Dual SIM (nano‑SIM and eSIM)\r\niOS, v13', 'وحدة المعالجة المركزية: آبل A13 بيونيك (7 نانومتر +)\r\nالذاكرة: رام 4 جيجابايت / سعة تخزين 256 جيجابايت\r\nالشاشة: 5.8 بوصة سوبر رينتا إكس دي آر أوليد (1125x2436 بيكسل)\r\nالكاميرا: 12 ميجابيكسل (إف / 1.8) + 12 ميجابيكسل (إف / 2.4) + 12 ميجابيكسل (إف / 2.0)\r\nالثانوية: 12 ميجابيكسل (إف / 2.2)\r\n٤جي إل تي إي + واي فاي + بلوتوث + شريحة مزدوجة (نانو سيم و إي سيم)\r\nنظام التشغيل: iOS إصدار 13.0', 'apple-iphone-11-pro-256gb-phone', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/11gold.jpg', '[\"uploads\\/media\\/2021\\/11silver-1.jpg\",\"uploads\\/media\\/2021\\/11silver-2.jpg\"]', '', '', 'Mobile,Apple,iPhone 11 Pro', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '<h3 class=\"acc-content-title\" style=\"font-family: Roboto, sans-serif; line-height: 1.5; color: rgb(51, 51, 51); margin-top: 10px; margin-bottom: 10px; font-size: 1.2em; background-color: rgb(248, 248, 248);\"><span class=\"h4 title-text\" style=\"font-family: Roboto, sans-serif; line-height: 1.1; margin-top: 10px; margin-bottom: 10px; font-size: 17px;\">Information on Apple iPhone 11 Pro 256GB Phone - Gold:</span></h3><p class=\"h3\" style=\"margin: 10px 0px; font-family: Roboto, sans-serif; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\"></p><table class=\"data-table\" id=\"product-attribute-specs-table1\" style=\"border-spacing: 0px; background-color: rgb(248, 248, 248); width: 1282.93px; max-width: 100%; margin-bottom: 20px; border: 1px solid rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: Roboto, sans-serif; font-size: 14px;\"><colgroup><col width=\"25%\"><col></colgroup><tbody><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Model Number</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">MWC92AH/A</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Article Number</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">525379</td></tr></tbody></table><p class=\"h3\" style=\"margin: 10px 0px; font-family: Roboto, sans-serif; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\">General</p><table class=\"data-table\" id=\"product-attribute-specs-table2\" style=\"border-spacing: 0px; background-color: rgb(248, 248, 248); width: 1282.93px; max-width: 100%; margin-bottom: 20px; border: 1px solid rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: Roboto, sans-serif; font-size: 14px;\"><colgroup><col width=\"25%\"><col></colgroup><tbody><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Brand</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">APPLE</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Colour</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Gold</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Device Type</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">iPhone 11 Pro</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Processor</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">A13 Chip</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Operating System</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">iOS 13.0</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Sensors</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Accelerometer, Ambient Light, Barometer, Face ID, Proximity, Three-axis gyro</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Phone Memory (RAM)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">4 GB</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Storage Capacity</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">256GB</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Expandable Memory</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">No</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Phone Display Size</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">5.8 - inch</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Display Type</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">(1125 x 2436) Super Retina XDR OLED Capacitive Touchscreen Display</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Rear Camera</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Phone Rear Camera</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">12MP + 12MP + 12MP</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Front Camera</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Front Camera Resolution</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">12 Megapixels</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Flash</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Video Recording</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Cellular Connectivity</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">4G LTE</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Wi-Fi</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Number of SIM Cards</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Single SIM</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">SIM Size</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Nano-SIM</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Total Number of Slots</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">1</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Slot Details</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">1 SIM Card Slot + E-Sim</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Supported Network Types</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">GSM, HSPA, LTE</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">GPS</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">GPS Type</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes, with A-GPS, GLONASS, GALILEO, QZSS</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Video Playback</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Video Formats Supported</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">HEVC, H.264, MPEG‑4 Part 2, and Motion JPEG</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Audio Playback</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Audio Formats Supported</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">AAC‑LC, HE‑AAC, HE‑AAC v2, Protected AAC, MP3, Linear PCM, Apple Lossless, FLAC, Dolby Digital (AC‑3), Dolby Digital Plus (E‑AC‑3), Dolby Atmos, and Audible (formats 2, 3, 4, Audible Enhanced Audio, AAX, and AAX+)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Charging Port Type</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Lightning Port</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Bluetooth</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">v5.0</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Dimensions (H x L x W)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">144 x 71.4 x 8.1 mm (5.67 x 2.81 x 0.32 in)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: left; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Weight</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">188 g (6.63 oz)</td></tr></tbody></table>', '<h3 class=\"acc-content-title\" style=\"font-family: \"Droid Arabic Kufi\", sans-serif; font-weight: 600; line-height: 1.5; color: rgb(51, 51, 51); margin-top: 10px; margin-bottom: 10px; font-size: 1.2em; background-color: rgb(248, 248, 248);\"><span class=\"h4 title-text\" style=\"font-family: \"Droid Arabic Kufi\", sans-serif; margin-top: 10px; margin-bottom: 10px; font-size: 1.2em; line-height: 1.8 !important;\">معلومات عن هاتف آيفون ١١ برو بسعة ٢٥٦ جيجابايت - ذهبي:</span></h3><p class=\"h3\" style=\"margin: 10px 0px; font-family: \"Droid Arabic Kufi\", sans-serif; font-weight: 600; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\"></p><table class=\"data-table\" id=\"product-attribute-specs-table1\" style=\"border-spacing: 0px; background-color: rgb(248, 248, 248); width: 1282.93px; max-width: 100%; margin-bottom: 20px; border: 1px solid rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: \"Droid Arabic Kufi\", sans-serif; font-size: 14px;\"><colgroup><col width=\"25%\"><col></colgroup><tbody><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">الموديل</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">MWC92AH/A</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">رمز المنتج</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">525379</td></tr></tbody></table><p class=\"h3\" style=\"margin: 10px 0px; font-family: \"Droid Arabic Kufi\", sans-serif; font-weight: 600; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\">عام</p><table class=\"data-table\" id=\"product-attribute-specs-table2\" style=\"border-spacing: 0px; background-color: rgb(248, 248, 248); width: 1282.93px; max-width: 100%; margin-bottom: 20px; border: 1px solid rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: \"Droid Arabic Kufi\", sans-serif; font-size: 14px;\"><colgroup><col width=\"25%\"><col></colgroup><tbody><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">العلامة التجارية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">ابل</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">اللون</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">ذهبي</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">نوع الجهاز</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">ايفون 11 برو</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">المعالج</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">شريحة اي١٣</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">نظام التشغيل</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">آي أو إس ١٣,٠</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">المستشعرات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الإضاءة المحيطة, التسارع, التقارب, بارومتر, ثلاثة محاور الدوران, معرف الوجه</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">الذاكرة (رام)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">٤ جيجا بايت</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">سعة التخزين</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">256 جيجابايت</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">ذاكرة قابلة للزيادة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">غير متوفر</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">حجم الشاشة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">5.8 بوصة</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">نوع الشاشة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">(1125 x 2436) Super Retina XDR OLED Capacitive Touchscreen Display</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">كاميرا رئيسية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">متوفر</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">دقة كاميرا الهاتف الخلفية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">12 ميجا بيكسل +12 ميجا بيكسل +12 ميجا بيكسل</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">كاميرا ثانوية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">متوفر</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">دقة الكاميرا الأمامية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">١٢ ميجابكسل</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الفلاش</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">تسجيل فيديو</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الاتصال الخلوي</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">٤ جي إل تي إي</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">واي فاي</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">متوفر</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">(عدد بطاقات الخط (السيم</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">يدعم شريحتين (سيم)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">(حجم البطاقة (السيم</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">نانو - سيم</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">إجمالي عدد الفتحات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">١</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">تفاصيل الفتحات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">1 SIM Card Slot + E-Sim</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الشبكات المدعومة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">HSPA, LTE, جي أس أم</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">(جي بي أس (نظام الملاحة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">متوفر</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">(نوع الجي بي أس (نظام الملاحة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes, with A-GPS, GLONASS, GALILEO, QZSS</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">تسجيل فيديو</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">(نوع نظام الفيديو (الفورمات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">HEVC, H.264, MPEG‑4 Part 2, and Motion JPEG</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">تسجيل المقاطع الصوتية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">(نوع نظام المقطع الصوتي (الفورمات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">AAC‑LC, HE‑AAC, HE‑AAC v2, Protected AAC, MP3, Linear PCM, Apple Lossless, FLAC, Dolby Digital (AC‑3), Dolby Digital Plus (E‑AC‑3), Dolby Atmos, and Audible (formats 2, 3, 4, Audible Enhanced Audio, AAX, and AAX+)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">نوع مدخل الشاحن</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Lightning Port</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">بلوتوث</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">v5.0</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">الأبعاد (الطول × العمق × العرض)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">144 x 71.4 x 8.1 mm (5.67 x 2.81 x 0.32 in)</td></tr><tr><th class=\"\" style=\"padding: 8px; text-align: right; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الوزن</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">188 g (6.63 oz)</td></tr></tbody></table>', '', '', 1, '2021-09-21 11:19:30', NULL);
INSERT INTO `products` (`id`, `category_id`, `tax`, `row_order`, `type`, `stock_type`, `name`, `ar_name`, `short_description`, `ar_short_description`, `slug`, `indicator`, `cod_allowed`, `minimum_order_quantity`, `quantity_step_size`, `total_allowed_quantity`, `is_returnable`, `is_cancelable`, `cancelable_till`, `image`, `other_images`, `video_type`, `video`, `tags`, `ar_tags`, `warranty_period`, `guarantee_period`, `made_in`, `brand`, `sku`, `stock`, `availability`, `rating`, `no_of_ratings`, `description`, `ar_description`, `specification`, `ar_specification`, `status`, `date_added`, `sizechart`) VALUES
(60, 0, 0, 0, 'variable_product', '2', 'Apple iPhone 12 128GB 5G Phone', 'هاتف آيفون 12 بسعة 128 جيجابايت - 5جي - أزرق', 'Quick Overview:\r\nDisplay: 6.1“ Super Retina XDR OLED, 1170x2532 Pixel\r\nMemory: 128GB/4GB RAM\r\nCamera: 12MP (f/2.4) + 12MP (f/1.6)\r\nSecondary: 12MP (f/2.0)\r\nCPU: Hexa-core\r\nOS: iOS 14\r\nBattery: Non Removable Li-Po 2775 mAh\r\nSensors: Fingerprint (rear-mounted), accelerometer, proximity', 'نبذة عن المنتج:\r\nالشاشة: 6.1 بوصة سوبر رينتا XDR أوليد ، 1170×2532 بكسل\r\nالذاكرة: 128 جيجا / 4 جيجا رام\r\nالكاميرا: 12 ميجابكسل (إف/2.4) + 12 ميجابكسل (إف/ 1.6)\r\nالثانوية: 12 ميجابيكسل (إف / 2.0)\r\nوحدة المعالجة المركزية: سداسي النواة\r\nنظام التشغيل: iOS 14\r\nالبطارية: بطارية ليثيوم بوليمر 2775 مللي أمبير غير قابلة للفك\r\nالمستشعرات: مستشعر بصمة الأصبع (خلفي) ، ومقياس تسارع ومقياس تقارب', 'apple-iphone-12-128gb-5g-phone', NULL, 1, 1, 1, 5, 0, 0, '', 'uploads/media/2021/12-5G-bl-1.jpg', '[]', '', '', '', '', '', '', NULL, 3, NULL, NULL, NULL, 0, 0, '<h3 class=\"acc-content-title\" style=\"margin-top: 10px; margin-bottom: 10px; font-family: Roboto, sans-serif; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\"><span class=\"h4 title-text\" style=\"margin-bottom: 10px; font-family: Roboto, sans-serif; line-height: 1.1; font-size: 17px; margin-top: 10px;\">Information on Apple iPhone 12 128GB - Blue:</span></h3><p class=\"h3\" style=\"margin: 10px 0px; font-family: Roboto, sans-serif; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\"></p><table class=\"data-table\" id=\"product-attribute-specs-table1\" style=\"border-spacing: 0px; background-color: rgb(248, 248, 248); width: 1282.93px; max-width: 100%; margin-bottom: 20px; border: 1px solid rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: Roboto, sans-serif; font-size: 14px;\"><colgroup><col width=\"25%\"><col></colgroup><tbody><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Model Number</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">IPH 12-128GB-BL-DS</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Article Number</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">531583</td></tr></tbody></table><p class=\"h3\" style=\"margin: 10px 0px; font-family: Roboto, sans-serif; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\">General</p><table class=\"data-table\" id=\"product-attribute-specs-table2\" style=\"border-spacing: 0px; background-color: rgb(248, 248, 248); width: 1282.93px; max-width: 100%; margin-bottom: 20px; border: 1px solid rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: Roboto, sans-serif; font-size: 14px;\"><colgroup><col width=\"25%\"><col></colgroup><tbody><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Brand</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">APPLE</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Colour</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Blue</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Device Type</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">iPhone 12</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Processor</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">A14 Bionic</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Operating System</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">iOS 14</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Sensors</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">-</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Phone Memory (RAM)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">4 GB</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Storage Capacity</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">128GB</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Expandable Memory</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">No</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Phone Display Size</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">6.1-inch</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Rear Camera</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Phone Rear Camera</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">12 Megapixels + 12 Megapixels</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Front Camera</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Front Camera Resolution</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">12 Megapixels</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Flash</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Yes</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Battery Size</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">2775 mAh</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Cellular Connectivity</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">5G</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Wi-Fi</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Number of SIM Cards</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Dual SIM</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Total Number of Slots</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">-</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Slot Details</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">-</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">GPS</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">Bluetooth</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">v5.0</td></tr><tr><th class=\"\" style=\"text-align: left; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">In the Box</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">The device comes with a cable only - without the charger and without the earphones</td></tr></tbody></table>', '<h3 class=\"acc-content-title\" style=\"margin-top: 10px; margin-bottom: 10px; font-family: \"Droid Arabic Kufi\", sans-serif; font-weight: 600; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\"><span class=\"h4 title-text\" style=\"margin-bottom: 10px; font-family: \"Droid Arabic Kufi\", sans-serif; font-size: 1.2em; margin-top: 10px; line-height: 1.8 !important;\">معلومات عن أطلب مسبقا: هاتف آيفون 12 بسعة 128 جيجابايت - 5جي - أزرق:</span></h3><p class=\"h3\" style=\"margin: 10px 0px; font-family: \"Droid Arabic Kufi\", sans-serif; font-weight: 600; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\"></p><table class=\"data-table\" id=\"product-attribute-specs-table1\" style=\"border-spacing: 0px; background-color: rgb(248, 248, 248); width: 1282.93px; max-width: 100%; margin-bottom: 20px; border: 1px solid rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: \"Droid Arabic Kufi\", sans-serif; font-size: 14px;\"><colgroup><col width=\"25%\"><col></colgroup><tbody><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">الموديل</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">PRE ORDER IPHONE 1</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">رمز المنتج</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">531274</td></tr></tbody></table><p class=\"h3\" style=\"margin: 10px 0px; font-family: \"Droid Arabic Kufi\", sans-serif; font-weight: 600; line-height: 1.5; color: rgb(51, 51, 51); font-size: 1.2em; background-color: rgb(248, 248, 248);\">عام</p><table class=\"data-table\" id=\"product-attribute-specs-table2\" style=\"border-spacing: 0px; background-color: rgb(248, 248, 248); width: 1282.93px; max-width: 100%; margin-bottom: 20px; border: 1px solid rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: \"Droid Arabic Kufi\", sans-serif; font-size: 14px;\"><colgroup><col width=\"25%\"><col></colgroup><tbody><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">العلامة التجارية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">ابل</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">اللون</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">أزرق</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">نوع الجهاز</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">أيفون 12</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">المعالج</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">ايه 14 بيونيك</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">عدد الكور</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">هكسا كور</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">نظام التشغيل</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">آي أو إس 14</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">المستشعرات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">التسارع, التقارب, بصمات الأصابع</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الذاكرة (رام)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">٤ جيجا بايت</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">سعة التخزين</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">128 جيجابايت</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">ذاكرة قابلة للزيادة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">غير متوفر</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">حجم الشاشة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">6.1 بوصة</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">نوع الشاشة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Super Retina XDR OLED</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">وضوح الشاشة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">1170*2532 بكسل</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">دقة كاميرا الهاتف الخلفية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">12 ميجابكسل + 12 ميجابكسل</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">دقة الكاميرا الأمامية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">١٢ ميجابكسل</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الفلاش</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Yes</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">تسجيل فيديو</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">HDR video recording with Dolby Vision up to 30 fps / 4K video recording at 24 fps, 30 fps, or 60 fps</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">خصائص أخرى للكاميرا</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Ultra Wide and Wide camera</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">حجم البطارية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">2775 مللي أمبير</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">نوع البطارية</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">Li-Po</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">الاتصال الخلوي</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">٥ جي</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">واي فاي</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">متوفر</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">(عدد بطاقات الخط (السيم</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">-</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">(حجم البطاقة (السيم</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">نانو - سيم</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">إجمالي عدد الفتحات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">١</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">تفاصيل الفتحات</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">فتحة لشريحة الإتصال</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">(جي بي أس (نظام الملاحة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">متوفر</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">(نوع الجي بي أس (نظام الملاحة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">A-GPS, GLONASS, GALILEO, QZSS</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">عدد فتحات اليو أس بي٢.٠</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">متوفر</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">بلوتوث</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">v5.0</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">الأبعاد (الطول × العمق × العرض)</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">146.7x71.5x7.4mm</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">الوزن</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221);\">164 Gram</td></tr><tr><th class=\"\" style=\"text-align: right; padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">في العلبة</th><td class=\"data\" style=\"padding: 8px; line-height: 1.42857; vertical-align: top; border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);\">المنتج يأتي مع العلبة الصغيرة الإصدار الجديد. مع الكابل فقط بدون محول الطاقة وسماعة الأذن</td></tr></tbody></table>', '', '', 1, '2021-09-23 15:00:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_value_ids` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `product_id`, `attribute_value_ids`, `date_created`) VALUES
(1, 1, '', '2021-08-19 09:30:34'),
(2, 2, '', '2021-08-19 09:30:34'),
(3, 3, '', '2021-08-19 09:30:34'),
(4, 4, '', '2021-08-19 09:30:34'),
(5, 5, '', '2021-08-19 09:30:34'),
(6, 6, '', '2021-08-19 09:30:34'),
(7, 7, '', '2021-08-19 09:30:34'),
(8, 8, '', '2021-08-19 09:30:34'),
(9, 9, '', '2021-08-19 09:30:34'),
(10, 10, '', '2021-08-19 09:30:34'),
(11, 11, '', '2021-08-19 09:30:34'),
(12, 12, '', '2021-08-19 09:30:34'),
(13, 13, '', '2021-08-19 09:30:34'),
(14, 14, '', '2021-08-19 09:30:34'),
(15, 15, '', '2021-08-19 09:30:34'),
(16, 16, '', '2021-08-19 09:30:34'),
(17, 17, '', '2021-08-19 09:30:34'),
(18, 18, '', '2021-08-19 09:30:34'),
(19, 19, '', '2021-08-19 09:30:34'),
(20, 20, '', '2021-08-19 09:30:34'),
(21, 21, '', '2021-08-19 09:30:34'),
(22, 22, '', '2021-08-19 09:30:34'),
(23, 23, '', '2021-08-19 09:30:34'),
(24, 24, '76,77,26,30', '2021-08-19 09:30:34'),
(25, 25, '', '2021-09-06 09:35:58'),
(26, 26, '', '2021-09-06 09:35:58'),
(27, 27, '', '2021-09-06 09:35:58'),
(28, 28, '26,29,76,77,79', '2021-09-06 09:48:17'),
(29, 29, '26,29,76,77,79', '2021-09-06 09:48:17'),
(30, 30, '26,29,76,77,79', '2021-09-06 09:48:17'),
(31, 31, '', '2021-09-06 09:53:32'),
(32, 32, '26,27,76,77', '2021-09-06 09:54:47'),
(37, 37, '', '2021-09-06 11:04:03'),
(38, 38, '', '2021-09-07 09:37:00'),
(39, 39, '', '2021-09-07 09:37:00'),
(40, 40, '78', '2021-09-07 10:47:57'),
(41, 41, '25', '2021-09-08 09:26:51'),
(42, 42, '25', '2021-09-08 09:29:42'),
(43, 43, '25', '2021-09-08 09:37:23'),
(44, 44, '77', '2021-09-08 09:50:13'),
(45, 45, '', '2021-09-08 09:54:15'),
(46, 46, '25', '2021-09-08 09:55:01'),
(47, 47, '', '2021-09-08 09:59:37'),
(48, 48, '', '2021-09-08 10:13:37'),
(49, 49, '', '2021-09-08 10:16:34'),
(50, 50, '', '2021-09-08 10:16:34'),
(51, 51, '25', '2021-09-08 10:19:30'),
(52, 52, '25', '2021-09-08 10:19:30'),
(53, 53, '', '2021-09-08 11:45:04'),
(54, 54, '', '2021-09-08 11:45:04'),
(55, 55, '78', '2021-09-21 11:19:30'),
(56, 56, '78', '2021-09-21 11:19:30'),
(57, 57, '83,84', '2021-09-21 11:19:30'),
(58, 58, '26,29', '2021-09-21 11:29:40'),
(59, 59, '83,84', '2021-09-23 14:03:34'),
(60, 60, '26,46', '2021-09-23 15:00:32'),
(61, 61, '26,83,84,85', '2021-09-25 12:40:46'),
(62, 62, '26,83,84,85', '2021-09-25 14:05:53'),
(63, 63, '26,83,84,85,76', '2021-09-25 14:43:08'),
(64, 64, '26,85,76,77,79', '2021-09-25 15:04:04'),
(65, 65, '26,86', '2021-09-25 15:14:12'),
(66, 66, '26,86', '2021-09-25 15:27:33'),
(67, 67, '26,48', '2021-09-25 15:56:12'),
(68, 68, '46,48,87', '2021-09-26 08:24:16'),
(69, 69, '88,89,90', '2021-09-26 15:26:52'),
(70, 70, '26,48', '2021-09-26 15:34:55'),
(71, 71, '26,46,48', '2021-09-26 15:46:32'),
(72, 72, '83,84,86', '2021-09-26 16:02:08'),
(73, 73, '26,76,77', '2021-11-06 09:52:25'),
(74, 74, '26,76,77', '2021-11-06 09:52:25'),
(75, 75, '26,76', '2021-11-06 11:29:21'),
(76, 76, '26,76', '2021-11-06 11:29:21'),
(77, 77, '48,77', '2021-11-08 08:35:14'),
(78, 78, '48,77', '2021-11-08 08:35:14'),
(79, 79, '26,77', '2021-11-08 08:39:36'),
(80, 80, '46,76', '2021-11-08 09:07:17'),
(81, 81, '26,76', '2021-11-08 09:09:58'),
(82, 82, '26,76', '2021-11-08 09:09:58'),
(83, 83, '84,77', '2021-11-08 11:48:24'),
(84, 84, '84,77', '2021-11-08 12:07:51'),
(85, 85, '84,77', '2021-11-08 12:30:39'),
(86, 86, '26,77', '2021-11-08 12:33:07'),
(87, 87, '26,77', '2021-11-08 12:39:03'),
(88, 88, '26,77', '2021-11-08 12:54:30'),
(89, 89, '26,77', '2021-11-09 06:26:52'),
(90, 90, '26,77', '2021-11-09 06:29:18'),
(91, 91, '26,77', '2021-11-09 06:30:49'),
(92, 92, '26,77', '2021-11-09 07:03:26'),
(93, 93, '26,77', '2021-11-09 07:06:08'),
(94, 94, '26,83,85,76,77,79', '2021-11-09 11:22:32'),
(95, 95, '', '2021-11-09 11:22:32'),
(96, 96, '', '2021-11-10 06:36:55'),
(97, 97, '', '2021-11-10 06:45:01'),
(98, 98, '86,80', '2021-11-10 07:01:49'),
(99, 99, '', '2021-11-10 07:01:49'),
(100, 100, '83,84,86,77,79', '2021-11-10 08:49:52'),
(101, 101, '83,84,86,77,79', '2021-11-10 08:49:52');

-- --------------------------------------------------------

--
-- Table structure for table `product_rating`
--

CREATE TABLE `product_rating` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `rating` double NOT NULL DEFAULT '0',
  `images` mediumtext COLLATE utf8mb4_unicode_ci,
  `comment` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

CREATE TABLE `product_variants` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_value_ids` text COLLATE utf8mb4_unicode_ci,
  `attribute_set` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `special_price` double DEFAULT '0',
  `sku` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `default_product` int(11) NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci,
  `availability` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_variants`
--

INSERT INTO `product_variants` (`id`, `product_id`, `attribute_value_ids`, `attribute_set`, `price`, `special_price`, `sku`, `stock`, `default_product`, `images`, `availability`, `status`, `date_added`) VALUES
(124, 41, '25', NULL, 109.9, 0, '0100011213', 3, 0, NULL, 1, 1, '2021-09-08 09:26:51'),
(125, 42, '25', NULL, 109.9, 0, '0100011432', 3, 0, NULL, 1, 1, '2021-09-08 09:29:42'),
(140, 57, '78', NULL, 289.9, 0, '0100011163', 10, 0, '[\"uploads\\/media\\/2020\\/man_(13)-min-min.jpg\"]', 1, 7, '2021-09-21 11:19:30'),
(143, 57, '83', NULL, 289.9, 0, '100011163', 10, 1, '[\"uploads\\/media\\/2021\\/11gold.jpg\"]', 1, 1, '2021-09-21 11:39:42'),
(144, 57, '84', NULL, 289.9, 0, '100011163', 10, 0, '[\"uploads\\/media\\/2021\\/11silver-2.jpg\"]', 1, 1, '2021-09-23 12:29:37'),
(145, 59, '83', NULL, 165.9, 0, '0100011199', 10, 0, '[\"uploads\\/media\\/2020\\/man_(13)-min-min.jpg\",\"uploads\\/media\\/2020\\/man_(13)-min-min.jpg\",\"uploads\\/media\\/2020\\/man_(13)-min-min.jpg\"]', 1, 7, '2021-09-23 14:03:34'),
(146, 59, '84', NULL, 165.9, 0, '0100011199', 10, 0, '[\"uploads\\/media\\/2020\\/man_(13)-min-min.jpg\",\"uploads\\/media\\/2020\\/man_(13)-min-min.jpg\",\"uploads\\/media\\/2020\\/man_(13)-min-min.jpg\"]', 1, 7, '2021-09-23 14:03:34'),
(147, 59, '83', NULL, 165.9, 0, '100011199', 10, 1, '[\"uploads\\/media\\/2021\\/Iphone_8plus.jpg\"]', 1, 1, '2021-09-23 14:12:46'),
(148, 59, '84', NULL, 165.9, 0, '0100011199', 10, 0, '[\"uploads\\/media\\/2021\\/8plus-sil-1.jpg\"]', 1, 1, '2021-09-23 14:12:46'),
(149, 60, '26', NULL, 249.9, 0, '0100011265', 10, 1, '[\"uploads\\/media\\/2021\\/12-5G-bl-1.jpg\"]', 1, 1, '2021-09-23 15:00:32'),
(150, 60, '46', NULL, 249.9, 0, '100011265', 10, 0, '[\"uploads\\/media\\/2021\\/12-5G-wh-1.jpg\"]', 1, 1, '2021-09-23 15:00:32'),
(151, 61, '26', NULL, 303.9, 0, '100011276', 9, 1, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_128GB_5G_Blue.jpg\"]', 1, 1, '2021-09-25 12:40:46'),
(152, 61, '83', NULL, 303.9, 0, '100011276', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_128GB_5G_gold.jpg\"]', 1, 1, '2021-09-25 12:40:46'),
(153, 61, '84', NULL, 303.9, 0, '100011276', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_128GB_5G_Silver.jpg\"]', 1, 1, '2021-09-25 12:40:46'),
(154, 61, '85', NULL, 303.9, 0, '100011276', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_128GB_5G_grphite.jpg\"]', 1, 1, '2021-09-25 12:44:44'),
(155, 62, '26', NULL, 344.9, 0, '0100011277', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-BLUE.jpg\"]', 1, 7, '2021-09-25 14:05:53'),
(156, 62, '83', NULL, 344.9, 0, '0100011277', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-GOLD.jpg\"]', 1, 7, '2021-09-25 14:05:53'),
(157, 62, '84', NULL, 344.9, 0, '0100011277', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-SILVER.jpg\"]', 1, 7, '2021-09-25 14:05:53'),
(158, 62, '85', NULL, 344.9, 0, '0100011277', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-graphite.jpg\"]', 1, 7, '2021-09-25 14:05:53'),
(159, 63, NULL, NULL, 324.9, 0, '', NULL, 0, NULL, NULL, 7, '2021-09-25 14:43:08'),
(160, 62, '26', NULL, 324.9, 0, '0100011278', 10, 1, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-BLUE.jpg\"]', 1, 1, '2021-09-25 14:45:02'),
(161, 62, '83', NULL, 324.9, 0, '0100011278', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-GOLD.jpg\"]', 1, 1, '2021-09-25 14:45:02'),
(162, 62, '84', NULL, 324.9, 0, '0100011278', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-SILVER.jpg\"]', 1, 1, '2021-09-25 14:45:02'),
(163, 62, '85', NULL, 324.9, 0, '0100011278', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-graphite.jpg\"]', 1, 1, '2021-09-25 14:45:02'),
(164, 63, '26,76', NULL, 324.9, 0, '100011278', 10, 1, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-BLUE.jpg\",\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-BLUE.jpg\"]', 1, 1, '2021-09-25 15:01:58'),
(165, 63, '83,76', NULL, 324.9, 0, '100011278', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-GOLD.jpg\",\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-GOLD.jpg\"]', 1, 1, '2021-09-25 15:01:58'),
(166, 63, '84,76', NULL, 324.9, 0, '100011278', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-SILVER.jpg\",\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-SILVER.jpg\"]', 1, 1, '2021-09-25 15:01:58'),
(167, 63, '85,76', NULL, 324.9, 0, '100011278', 0, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-graphite.jpg\",\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-graphite.jpg\"]', 0, 1, '2021-09-25 15:01:58'),
(168, 64, NULL, NULL, 389.9, 0, '', NULL, 0, NULL, NULL, 7, '2021-09-25 15:04:04'),
(169, 64, '85,76', NULL, 370, 0, '0100011311', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-BLUE.jpg\",\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-BLUE.jpg\"]', 1, 1, '2021-09-25 15:05:25'),
(170, 64, '26,76', NULL, 389.9, 0, '0100011311', 0, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-BLUE.jpg\",\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-graphite.jpg\"]', 0, 1, '2021-09-25 15:05:25'),
(176, 66, '26', NULL, 59.9, 0, '0100011313', 10, 1, '[\"uploads\\/media\\/2021\\/Xiaomi_Redmi_Note-blue.jpg\"]', 1, 1, '2021-09-25 15:27:33'),
(177, 66, '86', NULL, 59.9, 0, '0100011313', 10, 0, '[\"uploads\\/media\\/2021\\/Xiaomi_Redmi_Note-grey.jpg\"]', 1, 1, '2021-09-25 15:27:33'),
(178, 67, NULL, NULL, 39.9, 0, '', NULL, 0, NULL, NULL, 7, '2021-09-25 15:56:12'),
(179, 68, NULL, NULL, 67.9, 0, '', NULL, 0, NULL, NULL, 7, '2021-09-26 08:24:16'),
(180, 68, '26', NULL, 67.9, 0, '0100011326', 10, 0, '[\"uploads\\/media\\/2021\\/samA22-white.jpg\"]', 1, 7, '2021-09-26 15:06:15'),
(181, 68, '48', NULL, 67.9, 0, '0100011326', 9, 1, '[\"uploads\\/media\\/2021\\/samA22-black.jpg\"]', 1, 1, '2021-09-26 15:06:15'),
(182, 68, '87', NULL, 67.9, 0, '0100011326', 10, 0, '[\"uploads\\/media\\/2021\\/samA22-violet.jpg\"]', 1, 1, '2021-09-26 15:06:15'),
(183, 68, '46', NULL, 67.9, 0, '0100011326', 10, 0, '[\"uploads\\/media\\/2021\\/samA22-white.jpg\"]', 1, 1, '2021-09-26 15:07:53'),
(184, 69, NULL, NULL, 83.9, 0, '', NULL, 0, NULL, NULL, 7, '2021-09-26 15:26:52'),
(185, 69, '88', NULL, 83.9, 0, '0100011329', 10, 1, '[\"uploads\\/media\\/2021\\/a32_aw.jpg\"]', 1, 1, '2021-09-26 15:28:23'),
(186, 69, '89', NULL, 83.9, 0, '0100011329', 8, 0, '[\"uploads\\/media\\/2021\\/a32_av.jpg\"]', 1, 1, '2021-09-26 15:28:23'),
(187, 69, '90', NULL, 83.9, 0, '0100011329', 10, 0, '[\"uploads\\/media\\/2021\\/a32_ab.jpg\"]', 1, 1, '2021-09-26 15:28:23'),
(188, 70, NULL, NULL, 26.9, 0, '', NULL, 0, NULL, NULL, 7, '2021-09-26 15:34:55'),
(189, 70, '26', NULL, 26.9, 0, '0100011303', 10, 1, '[\"uploads\\/media\\/2021\\/a01_blue.jpg\"]', 1, 1, '2021-09-26 15:36:29'),
(190, 70, '48', NULL, 26.9, 0, '0100011303', 8, 0, '[\"uploads\\/media\\/2021\\/a1_blk.jpg\"]', 1, 1, '2021-09-26 15:36:29'),
(191, 71, NULL, NULL, 52.9, 0, '', NULL, 0, NULL, NULL, 7, '2021-09-26 15:46:32'),
(192, 71, '26', NULL, 52.9, 0, '0100011301', 10, 1, '[\"uploads\\/media\\/2021\\/a12_bl.jpg\"]', 1, 1, '2021-09-26 15:48:40'),
(193, 71, '46', NULL, 52.9, 0, '0100011301', 10, 0, '[\"uploads\\/media\\/2021\\/a12_wht.jpg\"]', 1, 1, '2021-09-26 15:48:40'),
(194, 71, '48', NULL, 52.9, 0, '0100011301', 9, 0, '[\"uploads\\/media\\/2021\\/a12_blk.jpg\"]', 1, 1, '2021-09-26 15:48:40'),
(195, 72, NULL, NULL, 134.9, 0, '', NULL, 0, NULL, NULL, 7, '2021-09-26 16:02:08'),
(196, 72, '83', NULL, 0.01, 0, '100011254', 9, 0, '[\"uploads\\/media\\/2021\\/ipad_gld.jpg\"]', 1, 1, '2021-09-26 16:07:14'),
(197, 72, '84', NULL, 134.9, 0, '100011254', 10, 1, '[\"uploads\\/media\\/2021\\/ipad_sil.jpg\"]', 1, 1, '2021-09-26 16:07:14'),
(198, 72, '86', NULL, 134.9, 0, '100011254', 10, 0, '[\"uploads\\/media\\/2021\\/ipad_gr.jpg\"]', 1, 1, '2021-09-26 16:07:14'),
(199, 67, '26', NULL, 39.9, 0, '0100011318', 10, 1, '[\"uploads\\/media\\/2021\\/Samsung_A02S.jpg\"]', 1, 1, '2021-09-27 05:27:15'),
(200, 67, '48', NULL, 39.9, 0, '0100011318', 10, 0, '[\"uploads\\/media\\/2021\\/Samsung_A02S-black.jpg\"]', 1, 1, '2021-09-27 05:27:15'),
(201, 63, '26,77', NULL, 344.9, 0, '100011277', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-BLUE.jpg\"]', 1, 7, '2021-09-27 05:40:55'),
(202, 63, '83,77', NULL, 344.9, 0, '100011277', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-GOLD.jpg\"]', 1, 7, '2021-09-27 05:40:55'),
(203, 63, '84,77', NULL, 344.9, 0, '0100011277', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-SILVER.jpg\"]', 1, 7, '2021-09-27 05:40:55'),
(204, 63, '85,77', NULL, 344.9, 0, '100011277', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_Max_256GB_5G-graphite.jpg\"]', 1, 7, '2021-09-27 05:40:55'),
(205, 64, '26,76', NULL, 389.9, 0, '0100011311', 0, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_128GB_5G_grphite.jpg\"]', 0, 7, '2021-10-07 11:29:34'),
(206, 64, '26,77', NULL, 360, 0, '0100011311', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_128GB_5G_grphite.jpg\"]', 1, 7, '2021-10-07 11:29:34'),
(207, 64, '85,76', NULL, 370, 0, '0100011311', 10, 0, NULL, 1, 7, '2021-10-07 11:29:34'),
(208, 64, '85,77', NULL, 389, 0, '0100011311', 10, 0, NULL, 1, 7, '2021-10-07 11:29:34'),
(209, 64, '26,77', NULL, 360, 0, '0100011311', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_128GB_5G_grphite.jpg\"]', 1, 1, '2021-10-07 11:31:01'),
(210, 64, '85,77', NULL, 389, 0, '0100011311', 10, 0, '[\"uploads\\/media\\/2021\\/Iphone_12_Pro_128GB_5G_grphite.jpg\"]', 1, 1, '2021-10-07 11:31:01'),
(211, 64, '26,79', NULL, 389.9, 0, '0100011311', 10, 0, NULL, 1, 1, '2021-10-07 11:31:01'),
(212, 64, '85,79', NULL, 389.9, 0, '0100011311', 10, 0, NULL, 1, 1, '2021-10-07 11:31:01'),
(241, 95, '26,76', NULL, 459.9, 0, '0100011348', 5, 0, '[\"uploads\\/media\\/2021\\/13-pro-max-blue.jpg\"]', 1, 1, '2021-11-09 11:22:32'),
(242, 95, '26,77', NULL, 489.9, 0, '0100011336', 6, 0, '[\"uploads\\/media\\/2021\\/13-pro-max-blue.jpg\"]', 1, 1, '2021-11-09 11:22:32'),
(243, 95, '83,79', NULL, 539.9, 0, '0100011350', 1, 0, '[\"uploads\\/media\\/2021\\/13-pro-max-gold.jpg\"]', 1, 1, '2021-11-09 11:22:32'),
(244, 95, '85,76', NULL, 459.9, 0, '0100011349', 3, 0, '[\"uploads\\/media\\/2021\\/13-pro-max-graphite.jpg\"]', 1, 1, '2021-11-09 11:22:32'),
(245, 95, '85,79', NULL, 539.9, 0, '0100011351', 1, 0, '[\"uploads\\/media\\/2021\\/13-pro-max-graphite.jpg\"]', 1, 1, '2021-11-09 11:22:32'),
(247, 97, NULL, NULL, 62.5, 0, '0100011187', NULL, 1, NULL, NULL, 1, '2021-11-10 06:45:01'),
(249, 99, '86,80', NULL, 349.9, 0, '0100011223', 2, 0, '[\"uploads\\/media\\/2021\\/11-pro-max.jpg\"]', 1, 1, '2021-11-10 07:01:49'),
(253, 101, '83,77', NULL, 284.9, 0, '0100011163', 1, 0, '[\"uploads\\/media\\/2021\\/11-pro-gold.jpg\"]', 1, 1, '2021-11-10 08:49:52'),
(254, 101, '84,77', NULL, 289.9, 0, '0100011186', 1, 0, NULL, 1, 1, '2021-11-10 08:49:52'),
(255, 101, '86,79', NULL, 349.9, 0, '0100011223', 2, 0, '[\"uploads\\/media\\/2021\\/11-pro-grey.jpg\"]', 1, 1, '2021-11-10 08:49:52');

-- --------------------------------------------------------

--
-- Table structure for table `promo_codes`
--

CREATE TABLE `promo_codes` (
  `id` int(11) NOT NULL,
  `promo_code` varchar(28) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `no_of_users` int(11) DEFAULT NULL,
  `minimum_order_amount` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `discount_type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_discount_amount` double DEFAULT NULL,
  `repeat_usage` tinyint(4) NOT NULL,
  `no_of_repeat_usage` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `promo_codes`
--

INSERT INTO `promo_codes` (`id`, `promo_code`, `message`, `start_date`, `end_date`, `no_of_users`, `minimum_order_amount`, `discount`, `discount_type`, `max_discount_amount`, `repeat_usage`, `no_of_repeat_usage`, `status`, `date_created`) VALUES
(1, 'testPRO', 'TEST', '2021-08-30', '2021-08-31', 4, 100, 40, 'percentage', 400, 1, 4, 1, '2021-08-30 14:22:13'),
(2, 'E4', 'test', '2021-08-30', '2021-09-04', 100, 100, 40, 'percentage', 300, 1, 4, 1, '2021-08-30 15:08:41'),
(3, 'Eurrocom', 'Eurrocom test', '2021-10-23', '2021-10-31', 50, 100, 20, 'percentage', 200, 1, 50, 1, '2021-10-24 06:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `return_requests`
--

CREATE TABLE `return_requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_variant_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_item_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `remarks` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `title` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_ids` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `row_order` int(11) NOT NULL DEFAULT '0',
  `categories` mediumtext COLLATE utf8mb4_unicode_ci,
  `product_type` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `title`, `short_description`, `style`, `product_ids`, `row_order`, `categories`, `product_type`, `date_added`) VALUES
(1, 'Featured Products', 'Featured Products', 'default', '265,393,436,483,583,597,644,645,674,678,721,758,784', 2, NULL, 'custom_products', '2021-05-14 18:08:32'),
(2, 'Hot Deals', 'Hot Deals', 'default', NULL, 0, '9', 'products_on_sale', '2021-05-14 18:09:06'),
(3, 'Most Selling Products-Details', 'Most Selling Products-Details', 'default', '301,165,762,585,502,390,420,61', 1, NULL, 'custom_products', '2021-05-14 18:09:33');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `variable` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `variable`, `value`) VALUES
(1, 'logo', 'uploads/media/2021/logo2.png'),
(2, 'privacy_policy', '     <p>Your privacy is as important to us as it is to you. We know you hate SPAM and so do we. That is why we will never sell or share your information with anyone without your express permission. We respect your rights and will do everything\r\n                        in our power to protect your personal information. In the interest of full disclosure, we provide this notice explaining our online information collection practices. This privacy notice discloses the privacy practices for <a href=\"https://envaysoft.com/\">EnvaySoft</a>&nbsp;(herein\r\n                        known as we, us and our company) and applies solely to information collected by this web site.</p>\r\n\r\n                    <h4>Information Collection, Use, and Sharing</h4>\r\n                    <p>We are the sole owners of the information collected on this site. We only have access to information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone. We will\r\n                        use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request, e.g. to ship an\r\n                        order.\r\n                    </p>\r\n                    <h4>Disclosure</h4>\r\n                    <p>This site uses Google web analytics service. Google may record mouse clicks, mouse movements, scrolling activity as well as text you type in this website. This site does not use Google to collect any personally identifiable information\r\n                        entered in this website. Google does track your browsing habits across web sites which do not use Google services.</p>\r\n\r\n                    <h4>Security</h4>\r\n                    <p>We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.</p>\r\n                    <p>Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a closed lock icon at the bottom of your web browser, or\r\n                        looking for “https” at the beginning of the address of the web page.</p>\r\n                    <p>While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are\r\n                        granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p>\r\n\r\n                    <h4>Email Policy</h4>\r\n                    <p>The following are situations in which you may provide Your Information to us:</p>\r\n\r\n                    <div class=\"unorder-post\">\r\n                        <ul>\r\n                            <li>\r\n                                When you fill out forms or fields through our Services.\r\n                            </li>\r\n                            <li>\r\n                                When you register for an account with our Service.\r\n                            </li>\r\n                            <li>\r\n                                When you create a subscription for our newsletter or Services.\r\n                            </li>\r\n                            <li>\r\n                                When you provide responses to a survey.\r\n                            </li>\r\n                            <li>\r\n                                When you answer questions on a quiz.\r\n                            </li>\r\n                            <li>\r\n                                When you join or enroll in an event through our Services.\r\n                            </li>\r\n                            <li>\r\n                                When you order services or products from, or through our Service.\r\n                            </li>\r\n                            <li>\r\n                                When you provide information to us through a third-party application, service or Website.\r\n                            </li>\r\n                        </ul>\r\n\r\n\r\n                    </div>'),
(3, 'terms_conditions', '\r\n                    <p>This website is operated by a.season. Throughout the site, the terms “we”, “us” and “our” refer to a.season. a.season offers this website, including all information, tools and services available\r\n                        from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here.</p>\r\n\r\n                    <p>By visiting our site and/ or purchasing something from us, you engage in our “Service” and agree to be bound by the following terms and conditions (“Terms of Service”, “Terms”), including those additional\r\n                        terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Service apply to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/\r\n                        or contributors of content.</p>\r\n\r\n                    <h4>Online Store Terms</h4>\r\n                    <p>By agreeing to these Terms of Service, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us\r\n                        your consent to allow any of your minor dependents to use this site.</p>\r\n\r\n                    <h4>General Conditions</h4>\r\n                    <p>We reserve the right to refuse service to anyone for any reason at any time.<br>You understand that your content (not including credit card information), may be transferred unencrypted and involve (a) transmissions over various networks;\r\n                        and (b) changes to conform and adapt to technical requirements of connecting networks or devices. Credit card information is always encrypted during transfer over networks.</p>\r\n\r\n                    <h4>License</h4>\r\n                    <p>You must not:</p>\r\n\r\n                    <div class=\"unorder-post\">\r\n                        <ul>\r\n                            <li>Republish material from&nbsp;<span class=\"highlight preview_website_name\">Website Name</span></li>\r\n                            <li>Sell, rent or sub-license material from&nbsp;<span class=\"highlight preview_website_name\">Website Name</span></li>\r\n                            <li>Reproduce, duplicate or copy material from&nbsp;<span class=\"highlight preview_website_name\">Website Name</span></li>\r\n                            <li>Redistribute content from&nbsp;<span class=\"highlight preview_website_name\">Website Name</span></li>\r\n                        </ul>\r\n\r\n                        <h4>Disclaimer</h4>\r\n                        <p>To the maximum extent permitted by applicable law, we exclude all representations:</p>\r\n\r\n                        <ul>\r\n                            <li>limit or exclude our or your liability for death or personal injury;</li>\r\n                            <li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>\r\n                            <li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>\r\n                            <li>exclude any of our or your liabilities that may not be excluded under applicable law.</li>\r\n                        </ul>\r\n                    </div>\r\n\r\n                    <p>As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>'),
(4, 'fcm_server_key', 'your_fcm_server_key'),
(5, 'contact_us', '<h2><strong>Contact Us</strong></h2>\\r\\n\\r\\n<p>For any kind of queries related to products, orders or services feel free to contact us on our official email address or phone number as given below :</p>\\r\\n\\r\\n<p> </p>\\r\\n\\r\\n<h3><strong>Areas we deliver : </strong></h3>\\r\\n\\r\\n<p> </p>\\r\\n\\r\\n<h3><strong>Delivery Timings :</strong></h3>\\r\\n\\r\\n<ol>\\r\\n <li><strong>  8:00 AM To 10:30 AM</strong></li>\\r\\n <li><strong>10:30 AM To 12:30 PM</strong></li>\\r\\n <li><strong>  4:00 PM To  7:00 PM</strong></li></ol><h3> <strong></strong>\\r\\n\\r\\n</h3><p><strong>Note : </strong>You can order for maximum 2days in advance. i.e., Today & Tomorrow only.  <br></p>'),
(6, 'system_settings', '{\"system_configurations\":\"1\",\"system_timezone_gmt\":\"+03:00\",\"system_configurations_id\":\"13\",\"app_name\":\"Eurocom\",\"support_number\":\"9876543210\",\"support_email\":\"support@eurocom.com\",\"current_version\":\"1.0.0\",\"minimum_version_required\":\"1.0.0\",\"is_version_system_on\":\"1\",\"area_wise_delivery_charge\":\"1\",\"currency\":\"KD\",\"delivery_charge\":\"1\",\"min_amount\":\"200\",\"system_timezone\":\"Asia\\/Baghdad\",\"is_refer_earn_on\":\"0\",\"min_refer_earn_order_amount\":\"\",\"refer_earn_bonus\":\"\",\"refer_earn_method\":\"\",\"max_refer_earn_amount\":\"\",\"refer_earn_bonus_times\":\"\",\"minimum_cart_amt\":\"1\",\"max_items_cart\":\"12\",\"delivery_boy_bonus_percentage\":\"1\",\"max_product_return_days\":\"1\",\"is_delivery_boy_otp_setting_on\":\"0\",\"cart_btn_on_list\":\"0\",\"expand_product_images\":\"0\",\"tax_name\":\"\",\"tax_number\":\"\"}'),
(7, 'payment_method', '{\"paypal_payment_method\":\"1\",\"paypal_mode\":\"sandbox\",\"paypal_business_email\":\"seller@somedomain.com\",\"currency_code\":\"USD\",\"razorpay_payment_method\":\"1\",\"razorpay_key_id\":\"rzp_test_key\",\"razorpay_secret_key\":\"secret_key\",\"paystack_payment_method\":\"1\",\"paystack_key_id\":\"paystack_public_key\",\"paystack_secret_key\":\"paystack_secret_key\",\"stripe_payment_method\":\"1\",\"stripe_payment_mode\":\"test\",\"stripe_publishable_key\":\"test_key\",\"stripe_secret_key\":\"test_key\",\"stripe_webhook_secret_key\":\"webhook_secret\",\"stripe_currency_code\":\"INR\",\"flutterwave_payment_method\":\"1\",\"flutterwave_public_key\":\"public_key\",\"flutterwave_secret_key\":\"secret_key\",\"flutterwave_encryption_key\":\"enc_key\",\"flutterwave_currency_code\":\"NGN\",\"paytm_payment_method\":\"1\",\"paytm_payment_mode\":\"sandbox\",\"paytm_merchant_key\":\"merchant_key\",\"paytm_merchant_id\":\"merchant_id\",\"cod_method\":\"1\"}'),
(8, 'about_us', '    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage\r\n                        of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator\r\n                        on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition,\r\n                        injected humour, or non-characteristic words etc.</p>\r\n\r\n                    <div class=\"unorder-post\">\r\n                        <ul>\r\n                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n                            <li>In luctus nunc id lectus pellentesque lacinia.</li>\r\n                            <li>Pellentesque laoreet mi molestie tortor aliquam, sed hendrerit nisi consectetur.</li>\r\n                            <li>Nam sed sapien sed lacus placerat euismod in consectetur ex.</li>\r\n                            <li>Sed et odio ultrices, semper sem sed, scelerisque libero.</li>\r\n                            <li>Proin ut ex varius libero viverra pellentesque.</li>\r\n                        </ul>'),
(9, 'currency', 'KD'),
(11, 'email_settings', '{\"email\":\"your@gmail.com\",\"password\":\"your_mail_password\",\"smtp_host\":\"smtp.gmail.com\",\"smtp_port\":\"465\",\"mail_content_type\":\"html\",\"smtp_encryption\":\"ssl\"}'),
(12, 'time_slot_config', '{\"time_slot_config\":\"1\",\"is_time_slots_enabled\":\"1\",\"delivery_starts_from\":\"2\",\"allowed_days\":\"7\"}'),
(13, 'favicon', 'uploads/media/2021/faviocn.png'),
(14, 'delivery_boy_privacy_policy', '<p>ACCESSING, BROWSING OR OTHERWISE USING THE WEBSITE eShop.com, Missed Call Service or mobile application INDICATES user is in AGREEMENT with eShop vegetables & fruits Pvt Ltd for ALL THE TERMS AND CONDITIONS MENTIONED henceforth. User is requested to READ terms and conditions CAREFULLY BEFORE PROCEEDING FURTHER.<br>User is the person, group of person, company, trust, society, legal entity, legal personality or anyone who visits website, mobile app or gives missed call or places order with eShop via phone or website or mobile application or browse through website www.eShop.com.</p><p>eShop reserves the right to add, alter, change, modify or delete any of these terms and conditions at any time without prior information. The altered terms and conditions becomes binding on the user since the moment same are unloaded on the website www.eShop.com</p><p>eShop is in trade of fresh fruits and vegetables and delivers the order to home (user’s desired address) directly.</p><p>That any user who gives missed call/call for order on any number published/used by eShop.com, consents to receive, accept calls and messages or any after communication from eShop vegetables & fruits Pvt Ltd for Promotion and Telemarketing Purposes within a week.</p><p>If a customer do not wish to receive any communication from eShop, please SMS NO OFFERS to 9512512125.</p><p>eShop accept orders on all seven days and user will receive the delivery next day from date of order placement, as we at eShop procure the fresh produce from the procurement center and deliver it straight to user.</p><p>There is Minimum Order value of Rs. 200. There are no delivery charges on an order worth Rs. 200 or above. In special cases, if permitted, order value is less then Rs. 200/– , Rs. 40 as shipping charges shall be charged from user.</p><p>eShop updates the prices on daily basis and the price displayed at our website www.eShop.com, at the time of placement of order by user he/she/it will be charged as per the price listed at the website www.eShop.com.</p><p>In the event, though there are remote possibilities, of wrong invoice generation due to any reason, in case it happens eShop vegetables & fruits Pvt Ltd reserve its right to again raise the correct invoice at the revised amount and same shall be paid by user.</p><p>At times it is difficult to weigh certain vegetables or fruits exactly as per the order or desired quantity of user, hence the delivery might be with five percent variation on both higher or lower side of exact ordered quantity, user are hereby under takes to pay to eShop vegetables & fruits Pvt Ltd as per the final invoice. We at eShop understands and our endeavor is to always deliver in exact quantity in consonance with quantity ordered but every time it’s not possible but eShop guarantee the fair deal and weight to all its users. eShop further assures its users that at no instance delivery weights/quantity vary dramatically from what quantity ordered by user.</p><p>If some product is not available or is not of good quality, the same item will not be delivered and will be adjusted accordingly in the invoice; all rights in this regards are reserved with eShop. Images of Fruits & Vegetables present in the website are for demonstration purpose and may not resemble exactly in size, colour, weight, contrast etc; though we assure our best to maintain the best quality in product, which is being our foremost commitment to the customer.</p><p>All orders placed before 11 PM in the Night will be delivered next day or as per delivery date chosen.</p>'),
(15, 'delivery_boy_terms_conditions', '<p>ACCESSING, BROWSING OR OTHERWISE USING THE WEBSITE eShop.com, Missed Call Service or mobile application INDICATES user is in AGREEMENT with eShop vegetables & fruits Pvt Ltd for ALL THE TERMS AND CONDITIONS MENTIONED henceforth. User is requested to READ terms and conditions CAREFULLY BEFORE PROCEEDING FURTHER.<br>User is the person, group of person, company, trust, society, legal entity, legal personality or anyone who visits website, mobile app or gives missed call or places order with eShop via phone or website or mobile application or browse through website www.eShop.com.</p><p>eShop reserves the right to add, alter, change, modify or delete any of these terms and conditions at any time without prior information. The altered terms and conditions becomes binding on the user since the moment same are unloaded on the website www.eShop.com</p><p>eShop is in trade of fresh fruits and vegetables and delivers the order to home (user’s desired address) directly.</p><p>That any user who gives missed call/call for order on any number published/used by eShop.com, consents to receive, accept calls and messages or any after communication from eShop vegetables & fruits Pvt Ltd for Promotion and Telemarketing Purposes within a week.</p><p>If a customer do not wish to receive any communication from eShop, please SMS NO OFFERS to 9512512125.</p><p>eShop accept orders on all seven days and user will receive the delivery next day from date of order placement, as we at eShop procure the fresh produce from the procurement center and deliver it straight to user.</p><p>There is Minimum Order value of Rs. 200. There are no delivery charges on an order worth Rs. 200 or above. In special cases, if permitted, order value is less then Rs. 200/– , Rs. 40 as shipping charges shall be charged from user.</p><p>eShop updates the prices on daily basis and the price displayed at our website www.eShop.com, at the time of placement of order by user he/she/it will be charged as per the price listed at the website www.eShop.com.</p><p>In the event, though there are remote possibilities, of wrong invoice generation due to any reason, in case it happens eShop vegetables & fruits Pvt Ltd reserve its right to again raise the correct invoice at the revised amount and same shall be paid by user.</p><p>At times it is difficult to weigh certain vegetables or fruits exactly as per the order or desired quantity of user, hence the delivery might be with five percent variation on both higher or lower side of exact ordered quantity, user are hereby under takes to pay to eShop vegetables & fruits Pvt Ltd as per the final invoice. We at eShop understands and our endeavor is to always deliver in exact quantity in consonance with quantity ordered but every time it’s not possible but eShop guarantee the fair deal and weight to all its users. eShop further assures its users that at no instance delivery weights/quantity vary dramatically from what quantity ordered by user.</p><p>If some product is not available or is not of good quality, the same item will not be delivered and will be adjusted accordingly in the invoice; all rights in this regards are reserved with eShop. Images of Fruits & Vegetables present in the website are for demonstration purpose and may not resemble exactly in size, colour, weight, contrast etc; though we assure our best to maintain the best quality in product, which is being our foremost commitment to the customer.</p><p>All orders placed before 11 PM in the Night will be delivered next day or as per delivery date chosen.</p>'),
(16, 'web_logo', 'uploads/media/2021/logo2.png'),
(17, 'web_favicon', 'uploads/media/2021/faviocn.png'),
(18, 'web_settings', '{\"site_title\":\"Eurocom\",\"support_number\":\"+990123456789\",\"support_email\":\"Info@intleuro.com\",\"address\":\"4th Floor, Al Zumorrodah Tower,\\\\r\\\\nAhmed Al Jaber st, Kuwait\",\"app_short_description\":\"test\",\"map_iframe\":\"&lt;iframe src=\\\\\\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m18!1m12!1m3!1d27814.41181998293!2d47.96435716127931!3d29.376101453829342!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3fcf9c83ce455983:0xc3ebaef5af09b90e!2sKuwait City, Kuwait!5e0!3m2!1sen!2sin!4v1626851621540!5m2!1sen!2sin\\\\\\\" width=\\\\\\\"600\\\\\\\" height=\\\\\\\"500\\\\\\\" style=\\\\\\\"border:0;\\\\\\\" allowfullscreen=\\\\\\\"\\\\\\\" loading=\\\\\\\"lazy\\\\\\\"&gt;&lt;\\/iframe&gt;\",\"logo\":\"uploads\\/media\\/2021\\/logo2.png\",\"favicon\":\"uploads\\/media\\/2021\\/faviocn.png\",\"meta_keywords\":\"Eurocom - eCommerce Online Store\",\"meta_description\":\"Eurocom - eCommerce Online Store\",\"app_download_section\":true,\"app_download_section_title\":\"Eurocom Mobile App\",\"app_download_section_tagline\":\"Download eshop\",\"app_download_section_short_description\":\"Shop with us at affordable prices and get exciting cashback & offers.\",\"app_download_section_playstore_url\":\"#\",\"app_download_section_appstore_url\":\"#\",\"twitter_link\":\"#\",\"facebook_link\":\"#\",\"instagram_link\":\"#\",\"youtube_link\":\"#\",\"shipping_mode\":true,\"shipping_title\":\" FREE SHIPPING\",\"shipping_description\":\"on all orders over KD10\",\"return_mode\":true,\"return_title\":\" 30 DAYS RETURN\",\"return_description\":\"money back\",\"support_mode\":true,\"support_title\":\"ACCEPT PAYMENT\",\"support_description\":\"Visa, K-Net, Master\",\"safety_security_mode\":true,\"safety_security_title\":\"SECURED PAYMENT\",\"safety_security_description\":\"guarantee 100%\"}'),
(19, 'services', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage\r\n                        of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator\r\n                        on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition,\r\n                        injected humour, or non-characteristic words etc.</p><div class=\"unorder-post\">\r\n                        <h4>Services Details</h4>\r\n                        <ul>\r\n                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n                            <li>In luctus nunc id lectus pellentesque lacinia.</li>\r\n                            <li>Pellentesque laoreet mi molestie tortor aliquam, sed hendrerit nisi consectetur.</li>\r\n                            <li>Nam sed sapien sed lacus placerat euismod in consectetur ex.</li>\r\n                            <li>Sed et odio ultrices, semper sem sed, scelerisque libero.</li>\r\n                            <li>Proin ut ex varius libero viverra pellentesque.</li>\r\n                        </ul>\r\n                    </div>'),
(20, 'return', ' <h4>Return process can be accepted within 14 days from the invoice with the conditions of the following two steps:</h4>\r\n\r\n                    <ol>\r\n                        <li> The product to be returned in the same condition as supplied, in their original packaging.</li>\r\n                        <li> The invoice to be presented.</li>\r\n                    </ol>\r\n\r\n\r\n                    <h4>Policies</h4>\r\n                    <ul>\r\n                        <li>Any items whatsoever which have been provided free-of-charge with the relevant products must also be returned.\r\n                            </li><li>You must take reasonable care of any products that you wish to return and not use them. The products must be returned in their original conditions. We will not be responsible for any loss or damage caused to the items during\r\n                                your possession and may reject the request to return, exchange or refund.</li>\r\n\r\n                            <li> In accordance with global retail industry standards, we are unable to accept returns or exchanges of products in the following categories: underwear, lingerie, fragrances, health &amp; beauty (such as skincare, make-up &amp; cosmetics)\r\n                                or other products which are personalised to your order.</li>\r\n\r\n                            <li> In the case of faulty, damaged or incorrectly supplied goods, we may offer you a substitute or replacement product or we will give you a full refund through the possible means, including any delivery charges you paid for (if\r\n                                any). In any case you must return the faulty, damaged or incorrect products to us as soon as possible in the same condition as supplied.</li>\r\n                    </ul>\r\n\r\n\r\n                    <h4>Exchanges Process</h4>\r\n                    <p> If you would like to exchange any products, you may:<br> - Contact Customer service on the online platform and provide them with the information.<br> Please note that this exchange is subject to availability and is per our applicable\r\n                        policies.\r\n                    </p>\r\n\r\n                    <h4>Refunds</h4>\r\n                    <p>Subject to the above exclusions, we will process a refund upon receipt of the returned/exchanged products, excluding any delivery charges you paid for through ordering in condition that the product is in the same condition as supplies\r\n                        and in their original packaging, with an invoice presented and the product is not used.</p>\r\n\r\n                    <h4>Cancellation</h4>\r\n                    <p>If you wish to cancel your order, you may do so by calling our Customer Services department during working hours, or by choosing the \" cancel my order\" tap and within one hour of having placed it.</p>\r\n\r\n                    <p>If you paid by credit card or debit card (KNET in Kuwait), and you have cancelled in accordance with this clause then we will reverse the authorization or process a refund transaction, as soon as possible but we are not responsible\r\n                        for how long this will take to be reflected on your account as this is dependent on bank processing procedures. </p>\r\n\r\n                    <p>Please note that we cannot accept cancellations outside of the specified periods as the order will have been processed and can only then be cancelled through our returns/refunds process as mentioned above.</p>'),
(21, 'delivery_informations', ' <p>Studio Lorem Ipsum will accept returns of UNWORN MERCHANDISE within 14 days of receipt of your order. If 14 days have gone by since receipt of your order, unfortunately we can’t offer you a refund or exchange.</p>\r\n\r\n                    <p>To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging.</p>\r\n\r\n                    <p>To complete your return, we require a receipt or proof of purchase.</p>\r\n\r\n                    <h4>Refunds (if applicable)</h4>\r\n                    <p>Once your return is received and inspected, we will send you an email to notify you that we have received your returned item. We will also notify you of the approval or rejection of your refund.<br>If you are approved, then your\r\n                        refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within a certain amount of days.</p>\r\n\r\n                    <h4>Late or missing refunds (if applicable)</h4>\r\n                    <div class=\"unorder-post\">\r\n                        <ul>\r\n                            <li>If you haven’t received a refund yet, first check your bank account again.</li>\r\n                            <li>Then contact your credit card company, it may take some time before your refund is officially posted.</li>\r\n                            <li>Next contact your bank. There is often some processing time before a refund is posted.</li>\r\n                            <li>If you’ve done all of this and you still have not received your refund yet, please contact us.</li>\r\n                        </ul>\r\n                    </div>\r\n\r\n                    <h4>Gifts</h4>\r\n                    <p>If the item was marked as a gift when purchased and shipped directly to you, you’ll receive a gift credit for the value of your return. Once the returned item is received, a gift certificate will be mailed to you.</p>\r\n\r\n                    <p>If the item wasn’t marked as a gift when purchased, or the gift giver had the order shipped to themselves to give to you later, we will send a refund to the gift giver and he will find out about your return.</p>\r\n\r\n                    <h4>Shipping</h4>\r\n                    <p>To return your product, you can request to return the product within 14 days of receipt of your order, and we will email you the return label which you can use to ship the product. Once your return is received and inspected, the refund\r\n                        will be processed to your original method of payment.</p>\r\n\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `settings1`
--

CREATE TABLE `settings1` (
  `id` int(11) NOT NULL,
  `variable` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings1`
--

INSERT INTO `settings1` (`id`, `variable`, `value`) VALUES
(1, 'logo', 'uploads/media/2021/thumb-sm/logo.png'),
(2, 'privacy_policy', '<p></p><h2><b>Privacy policy</b></h2>ACCESSING, BROWSING OR OTHERWISE USING THE \r\nWEBSITE eShop.com, Missed Call Service or mobile application \r\nINDICATES user is in AGREEMENT with eShop vegetables & \r\nfruits Pvt Ltd for ALL THE TERMS AND CONDITIONS MENTIONED henceforth. \r\nUser is requested to READ terms and conditions CAREFULLY BEFORE \r\nPROCEEDING FURTHER.<br>\r\nUser is the person, group of person, company, trust, society, legal \r\nentity, legal personality or anyone who visits website, mobile app or \r\ngives missed call or places order with eShop via phone or website \r\nor mobile application or browse through website www.eShop.com.<p></p>\r\n\r\n<p>eShop reserves the right to add, alter, change, modify or delete\r\n any of these terms and conditions at any time without prior \r\ninformation. The altered terms and conditions becomes binding on the \r\nuser since the moment same are unloaded on the website \r\nwww.eShop.com</p>\r\n\r\n<p>eShop is in trade of fresh fruits and vegetables and delivers the order to home (user’s desired address) directly.</p>\r\n\r\n<p>That any user who gives missed call/call for order on any number \r\npublished/used by eShop.com, consents to receive, accept calls and \r\nmessages or any after communication from eShop vegetables & \r\nfruits Pvt Ltd for Promotion and Telemarketing Purposes within a week.</p>\r\n\r\n<p>If a customer do not wish to receive any communication from eShop, please SMS NO OFFERS to 9512512125.</p>\r\n\r\n<p>eShop accept orders on all seven days and user will receive the \r\ndelivery next day from date of order placement, as we at eShop \r\nprocure the fresh produce from the procurement center and deliver it \r\nstraight to user.</p>\r\n\r\n<p>There is Minimum Order value of Rs. 200. There are no delivery \r\ncharges on an order worth Rs. 200 or above. In special cases, if \r\npermitted, order value is less then Rs. 200/– , Rs. 40 as shipping \r\ncharges shall be charged from user.</p>\r\n\r\n<p>eShop updates the prices on daily basis and the price displayed \r\nat our website www.eShop.com, at the time of placement of order by \r\nuser he/she/it will be charged as per the price listed at the website \r\nwww.eShop.com.</p>\r\n\r\n<p>In the event, though there are remote possibilities, of wrong invoice\r\n generation due to any reason, in case it happens eShop vegetables \r\n& fruits Pvt Ltd reserve its right to again raise the correct \r\ninvoice at the revised amount and same shall be paid by user.</p>\r\n\r\n<p>At times it is difficult to weigh certain vegetables or fruits \r\nexactly as per the order or desired quantity of user, hence the delivery\r\n might be with five percent variation on both higher or lower side of \r\nexact ordered quantity, user are hereby under takes to pay to eShop\r\n vegetables & fruits Pvt Ltd as per the final invoice. We at \r\neShop understands and our endeavor is to always deliver in exact \r\nquantity in consonance with quantity ordered but every time it’s not \r\npossible but eShop guarantee the fair deal and weight to all its \r\nusers. eShop further assures its users that at no instance delivery\r\n weights/quantity vary dramatically from what quantity ordered by user.</p>\r\n\r\n<p>If some product is not available or is not of good quality, the same \r\nitem will not be delivered and will be adjusted accordingly in the \r\ninvoice; all rights in this regards are reserved with eShop. Images\r\n of Fruits & Vegetables present in the website are for demonstration\r\n purpose and may not resemble exactly in size, colour, weight, contrast \r\netc; though we assure our best to maintain the best quality in product, \r\nwhich is being our foremost commitment to the customer.</p>\r\n\r\n<p>All orders placed before 11 PM in the Night will be delivered next day or as per delivery date chosen.</p>'),
(3, 'terms_conditions', '<h3><b>Terms and conditions</b></h3><p>eShop.com is a sole proprietary firm , Juridical rights of eShop.com are reserved with eShop<br>\r\nPersonal Information eShop.com and the website eShop.com (”The\r\n Site”) . respects your privacy. This Privacy Policy succinctly provides\r\n the manner your data is collected and used by eShop.com. on the \r\nSite. As a visitor to the Site/ Customer you are advised to please read \r\nthe Privacy Policy carefully.</p>\r\n\r\n<p>Services Overview As part of the registration process on the Site, \r\neShop.com may collect the following personally identifiable \r\ninformation about you: Name including first and last name, alternate \r\nemail address, mobile phone number and contact details, Postal code, GPS\r\n location, Demographic profile &#40;like your age, gender, occupation, \r\neducation, address etc.&#41; and information about the pages on the site you\r\n visit/access, the links you click on the site, the number of times you \r\naccess the page and any such browsing information.</p>\r\n\r\n<p>Eligibility Services of the Site would be available to only select \r\ngeographies in India. Persons who are \"incompetent to contract\" within \r\nthe meaning of the Indian Contract Act, 1872 including un-discharged \r\ninsolvents etc. are not eligible to use the Site. If you are a minor \r\ni.e. under the age of 18 years but at least 13 years of age you may use \r\nthe Site only under the supervision of a parent or legal guardian who \r\nagrees to be bound by these Terms of Use. If your age is below 18 years,\r\n your parents or legal guardians can transact on behalf of you if they \r\nare registered users. You are prohibited from purchasing any material \r\nwhich is for adult consumption and the sale of which to minors is \r\nprohibited.</p>\r\n\r\n<p>License & Site Access eShop.com grants you a limited \r\nsub-license to access and make personal use of this site and not to \r\ndownload (other than page caching) or modify it, or any portion of it, \r\nexcept with express written consent of eShop.com. This license does\r\n not include any resale or commercial use of this site or its contents; \r\nany collection and use of any product listings, descriptions, or prices;\r\n any derivative use of this site or its contents; any downloading or \r\ncopying of account information for the benefit of another merchant; or \r\nany use of data mining, robots, or similar data gathering and extraction\r\n tools. This site or any portion of this site may not be reproduced, \r\nduplicated, copied, sold, resold, visited or otherwise exploited for any\r\n commercial purpose without express written consent of eShop.com. \r\nYou may not frame or utilize framing techniques to enclose any \r\ntrademark, logo, or other proprietary information (including images, \r\ntext, page layout, or form) of the Site or of eShop.com and its \r\naffiliates without express written consent. You may not use any meta \r\ntags or any other \"hidden text\" utilizing the Site’s or eShop.com’s\r\n name or eShop.com’s name or trademarks without the express written\r\n consent of eShop.com. Any unauthorized use, terminates the \r\npermission or license granted by eShop.com</p>\r\n\r\n<p>Account & Registration Obligations All shoppers have to register \r\nand login for placing orders on the Site. You have to keep your account \r\nand registration details current and correct for communications related \r\nto your purchases from the site. By agreeing to the terms and \r\nconditions, the shopper agrees to receive promotional communication and \r\nnewsletters upon registration. The customer can opt out either by \r\nunsubscribing in \"My Account\" or by contacting the customer service.</p>\r\n\r\n<p>Pricing All the products listed on the Site will be sold at MRP \r\nunless otherwise specified. The prices mentioned at the time of ordering\r\n will be the prices charged on the date of the delivery. Although prices\r\n of most of the products do not fluctuate on a daily basis but some of \r\nthe commodities and fresh food prices do change on a daily basis. In \r\ncase the prices are higher or lower on the date of delivery not \r\nadditional charges will be collected or refunded as the case may be at \r\nthe time of the delivery of the order.</p>\r\n\r\n<p>Cancellation by Site / Customer You as a customer can cancel your \r\norder anytime up to the cut-off time of the slot for which you have \r\nplaced an order by calling our customer service. In such a case we will \r\nCredit your wallet against any payments already made by you for the \r\norder. If we suspect any fraudulent transaction by any customer or any \r\ntransaction which defies the terms & conditions of using the \r\nwebsite, we at our sole discretion could cancel such orders. We will \r\nmaintain a negative list of all fraudulent transactions and customers \r\nand would deny access to them or cancel any orders placed by them.</p>\r\n\r\n<p>Return & Refunds We have a \"no questions asked return policy\" \r\nwhich entitles all our Delivery Ambassadors to return the product at the\r\n time of delivery if due to any reason they are not satisfied with the \r\nquality or freshness of the product. We will take the returned product \r\nback with us and issue a credit note for the value of the return \r\nproducts which will be credited to your account on the Site. This can be\r\n used to pay your subsequent shopping bills. Refund will be processed \r\nthrough same online mode within 7 working days.</p>\r\n\r\n<p><br>\r\nDelivery & Shipping Charge</p>\r\n\r\n<p>1.You can expect to receive your order depending on the delivery option you have chosen.</p>\r\n\r\n<p>2.You can order 24*7 in website & mobile application , Our \r\ndelivery timeings are between 06:00 AM - 02:00PM Same day delivery.</p>\r\n\r\n<p>3.You will get free shipping on order amount above Rs.150.<br>\r\nYou Agree and Confirm<br>\r\n1. That in the event that a non-delivery occurs on account of a mistake \r\nby you (i.e. wrong name or address or any other wrong information) any \r\nextra cost incurred by eShop. for redelivery shall be claimed from \r\nyou.<br>\r\n2. That you will use the services provided by the Site, its affiliates, \r\nconsultants and contracted companies, for lawful purposes only and \r\ncomply with all applicable laws and regulations while using and \r\ntransacting on the Site.<br>\r\n3. You will provide authentic and true information in all instances \r\nwhere such information is requested you. eShop reserves the right \r\nto confirm and validate the information and other details provided by \r\nyou at any point of time. If upon confirmation your details are found \r\nnot to be true (wholly or partly), it has the right in its sole \r\ndiscretion to reject the registration and debar you from using the \r\nServices and / or other affiliated websites without prior intimation \r\nwhatsoever.<br>\r\n4. That you are accessing the services available on this Site and \r\ntransacting at your sole risk and are using your best and prudent \r\njudgment before entering into any transaction through this Site.<br>\r\n5. That the address at which delivery of the product ordered by you is to be made will be correct and proper in all respects.<br>\r\n6. That before placing an order you will check the product description \r\ncarefully. By placing an order for a product you agree to be bound by \r\nthe conditions of sale included in the item\'s description.</p>\r\n\r\n<p>You may not use the Site for any of the following purposes:<br>\r\n1. Disseminating any unlawful, harassing, libelous, abusive, \r\nthreatening, harmful, vulgar, obscene, or otherwise objectionable \r\nmaterial.<br>\r\n2. Transmitting material that encourages conduct that constitutes a \r\ncriminal offence or results in civil liability or otherwise breaches any\r\n relevant laws, regulations or code of practice.<br>\r\n3. Gaining unauthorized access to other computer systems.<br>\r\n4. Interfering with any other person\'s use or enjoyment of the Site.<br>\r\n5. Breaching any applicable laws;<br>\r\n6. Interfering or disrupting networks or web sites connected to the Site.<br>\r\n7. Making, transmitting or storing electronic copies of materials protected by copyright without the permission of the owner.</p>\r\n\r\n<p>Colors we have made every effort to display the colors of our \r\nproducts that appear on the Website as accurately as possible. However, \r\nas the actual colors you see will depend on your monitor, we cannot \r\nguarantee that your monitor\'s display of any color will be accurate.</p>\r\n\r\n<p>Modification of Terms & Conditions of Service eShop may at \r\nany time modify the Terms & Conditions of Use of the Website without\r\n any prior notification to you. You can access the latest version of \r\nthese Terms & Conditions at any given time on the Site. You should \r\nregularly review the Terms & Conditions on the Site. In the event \r\nthe modified Terms & Conditions is not acceptable to you, you should\r\n discontinue using the Service. However, if you continue to use the \r\nService you shall be deemed to have agreed to accept and abide by the \r\nmodified Terms & Conditions of Use of this Site.</p>\r\n\r\n<p>Governing Law and Jurisdiction This User Agreement shall be construed\r\n in accordance with the applicable laws of India. The Courts at \r\nFaridabad shall have exclusive jurisdiction in any proceedings arising \r\nout of this agreement. Any dispute or difference either in \r\ninterpretation or otherwise, of any terms of this User Agreement between\r\n the parties hereto, the same shall be referred to an independent \r\narbitrator who will be appointed by eShop and his decision shall be\r\n final and binding on the parties hereto. The above arbitration shall be\r\n in accordance with the Arbitration and Conciliation Act, 1996 as \r\namended from time to time. The arbitration shall be held in Nagpur. The \r\nHigh Court of judicature at Nagpur Bench of Mumbai High Court alone \r\nshall have the jurisdiction and the Laws of India shall apply.</p>\r\n\r\n<p>Reviews, Feedback, Submissions All reviews, comments, feedback, \r\npostcards, suggestions, ideas, and other submissions disclosed, \r\nsubmitted or offered to the Site on or by this Site or otherwise \r\ndisclosed, submitted or offered in connection with your use of this Site\r\n (collectively, the \"Comments\") shall be and remain the property of \r\neShop Such disclosure, submission or offer of any Comments shall \r\nconstitute an assignment to eShop of all worldwide rights, titles \r\nand interests in all copyrights and other intellectual properties in the\r\n Comments. Thus, eShop owns exclusively all such rights, titles and\r\n interests and shall not be limited in any way in its use, commercial or\r\n otherwise, of any Comments. eShopwill be entitled to use, \r\nreproduce, disclose, modify, adapt, create derivative works from, \r\npublish, display and distribute any Comments you submit for any purpose \r\nwhatsoever, without restriction and without compensating you in any way.\r\n eShop is and shall be under no obligation (1) to maintain any \r\nComments in confidence; (2) to pay you any compensation for any \r\nComments; or (3) to respond to any Comments. You agree that any Comments\r\n submitted by you to the Site will not violate this policy or any right \r\nof any third party, including copyright, trademark, privacy or other \r\npersonal or proprietary right(s), and will not cause injury to any \r\nperson or entity. You further agree that no Comments submitted by you to\r\n the Website will be or contain libelous or otherwise unlawful, \r\nthreatening, abusive or obscene material, or contain software viruses, \r\npolitical campaigning, commercial solicitation, chain letters, mass \r\nmailings or any form of \"spam\". eShop does not regularly review \r\nposted Comments, but does reserve the right (but not the obligation) to \r\nmonitor and edit or remove any Comments submitted to the Site. You grant\r\n eShopthe right to use the name that you submit in connection with \r\nany Comments. You agree not to use a false email address, impersonate \r\nany person or entity, or otherwise mislead as to the origin of any \r\nComments you submit. You are and shall remain solely responsible for the\r\n content of any Comments you make and you agree to indemnify eShop \r\nand its affiliates for all claims resulting from any Comments you \r\nsubmit. eShop and its affiliates take no responsibility and assume \r\nno liability for any Comments submitted by you or any third party.</p>\r\n\r\n<p>Copyright & Trademark eShop.com and eShop.com, its \r\nsuppliers and licensors expressly reserve all intellectual property \r\nrights in all text, programs, products, processes, technology, content \r\nand other materials, which appear on this Site. Access to this Website \r\ndoes not confer and shall not be considered as conferring upon anyone \r\nany license under any of eShop.com or any third party\'s \r\nintellectual property rights. All rights, including copyright, in this \r\nwebsite are owned by or licensed to eShop.com from eShop.com. \r\nAny use of this website or its contents, including copying or storing it\r\n or them in whole or part, other than for your own personal, \r\nnon-commercial use is prohibited without the permission of \r\neShop.com and/or eShop.com. You may not modify, distribute or \r\nre-post anything on this website for any purpose.The names and logos and\r\n all related product and service names, design marks and slogans are the\r\n trademarks or service marks of eShop.com, eShop.com, its \r\naffiliates, its partners or its suppliers. All other marks are the \r\nproperty of their respective companies. No trademark or service mark \r\nlicense is granted in connection with the materials contained on this \r\nSite. Access to this Site does not authorize anyone to use any name, \r\nlogo or mark in any manner.References on this Site to any names, marks, \r\nproducts or services of third parties or hypertext links to third party \r\nsites or information are provided solely as a convenience to you and do \r\nnot in any way constitute or imply eShop.com or eShop.com\'s \r\nendorsement, sponsorship or recommendation of the third party, \r\ninformation, product or service. eShop.com or eShop.com is not\r\n responsible for the content of any third party sites and does not make \r\nany representations regarding the content or accuracy of material on \r\nsuch sites. If you decide to link to any such third party websites, you \r\ndo so entirely at your own risk. All materials, including images, text, \r\nillustrations, designs, icons, photographs, programs, music clips or \r\ndownloads, video clips and written and other materials that are part of \r\nthis Website (collectively, the \"Contents\") are intended solely for \r\npersonal, non-commercial use. You may download or copy the Contents and \r\nother downloadable materials displayed on the Website for your personal \r\nuse only. No right, title or interest in any downloaded materials or \r\nsoftware is transferred to you as a result of any such downloading or \r\ncopying. You may not reproduce (except as noted above), publish, \r\ntransmit, distribute, display, modify, create derivative works from, \r\nsell or participate in any sale of or exploit in any way, in whole or in\r\n part, any of the Contents, the Website or any related software. All \r\nsoftware used on this Website is the property of eShop.com or its \r\nlicensees and suppliers and protected by Indian and international \r\ncopyright laws. The Contents and software on this Website may be used \r\nonly as a shopping resource. Any other use, including the reproduction, \r\nmodification, distribution, transmission, republication, display, or \r\nperformance, of the Contents on this Website is strictly prohibited. \r\nUnless otherwise noted, all Contents are copyrights, trademarks, trade \r\ndress and/or other intellectual property owned, controlled or licensed \r\nby eShop.com, one of its affiliates or by third parties who have \r\nlicensed their materials to eShop.com and are protected by Indian \r\nand international copyright laws. The compilation (meaning the \r\ncollection, arrangement, and assembly) of all Contents on this Website \r\nis the exclusive property of eShop.com and eShop.com and is \r\nalso protected by Indian and international copyright laws.</p>\r\n\r\n<p>Objectionable Material You understand that by using this Site or any \r\nservices provided on the Site, you may encounter Content that may be \r\ndeemed by some to be offensive, indecent, or objectionable, which \r\nContent may or may not be identified as such. You agree to use the Site \r\nand any service at your sole risk and that to the fullest extent \r\npermitted under applicable law, eShop.com and/or eShop.com and\r\n its affiliates shall have no liability to you for Content that may be \r\ndeemed offensive, indecent, or objectionable to you.</p>\r\n\r\n<p>Indemnity You agree to defend, indemnify and hold harmless \r\neShop.com, eShop.com, its employees, directors, Coordinators, \r\nofficers, agents, interns and their successors and assigns from and \r\nagainst any and all claims, liabilities, damages, losses, costs and \r\nexpenses, including attorney\'s fees, caused by or arising out of claims \r\nbased upon your actions or inactions, which may result in any loss or \r\nliability to eShop.com or eShop.com or any third party \r\nincluding but not limited to breach of any warranties, representations \r\nor undertakings or in relation to the non-fulfillment of any of your \r\nobligations under this User Agreement or arising out of the violation of\r\n any applicable laws, regulations including but not limited to \r\nIntellectual Property Rights, payment of statutory dues and taxes, claim\r\n of libel, defamation, violation of rights of privacy or publicity, loss\r\n of service by other subscribers and infringement of intellectual \r\nproperty or other rights. This clause shall survive the expiry or \r\ntermination of this User Agreement.</p>\r\n\r\n<p>Termination This User Agreement is effective unless and until \r\nterminated by either you or eShop.com. You may terminate this User \r\nAgreement at any time, provided that you discontinue any further use of \r\nthis Site. eShop.com may terminate this User Agreement at any time \r\nand may do so immediately without notice, and accordingly deny you \r\naccess to the Site, Such termination will be without any liability to \r\neShop.com. Upon any termination of the User Agreement by either you\r\n or eShop.com, you must promptly destroy all materials downloaded \r\nor otherwise obtained from this Site, as well as all copies of such \r\nmaterials, whether made under the User Agreement or otherwise. \r\neShop.com\'s right to any Comments shall survive any termination of \r\nthis User Agreement. Any such termination of the User Agreement shall \r\nnot cancel your obligation to pay for the product already ordered from \r\nthe Website or affect any liability that may have arisen under the User \r\nAgreement.</p>'),
(4, 'fcm_server_key', 'your_fcm_server_key'),
(5, 'contact_us', '<h2><strong>Contact Us</strong></h2>\\r\\n\\r\\n<p>For any kind of queries related to products, orders or services feel free to contact us on our official email address or phone number as given below :</p>\\r\\n\\r\\n<p> </p>\\r\\n\\r\\n<h3><strong>Areas we deliver : </strong></h3>\\r\\n\\r\\n<p> </p>\\r\\n\\r\\n<h3><strong>Delivery Timings :</strong></h3>\\r\\n\\r\\n<ol>\\r\\n <li><strong>  8:00 AM To 10:30 AM</strong></li>\\r\\n <li><strong>10:30 AM To 12:30 PM</strong></li>\\r\\n <li><strong>  4:00 PM To  7:00 PM</strong></li></ol><h3> <strong></strong>\\r\\n\\r\\n</h3><p><strong>Note : </strong>You can order for maximum 2days in advance. i.e., Today & Tomorrow only.  <br></p>'),
(6, 'system_settings', '{\"system_configurations\":\"1\",\"system_timezone_gmt\":\"+03:00\",\"system_configurations_id\":\"13\",\"app_name\":\"House Of Kids\",\"support_number\":\"9876543210\",\"support_email\":\"support@houseofkidskw.com\",\"current_version\":\"1.0.0\",\"minimum_version_required\":\"1.0.0\",\"is_version_system_on\":\"1\",\"area_wise_delivery_charge\":\"1\",\"currency\":\"KD\",\"delivery_charge\":\"10\",\"min_amount\":\"200\",\"system_timezone\":\"Asia\\/Baghdad\",\"is_refer_earn_on\":\"0\",\"min_refer_earn_order_amount\":\"\",\"refer_earn_bonus\":\"\",\"refer_earn_method\":\"\",\"max_refer_earn_amount\":\"\",\"refer_earn_bonus_times\":\"\",\"minimum_cart_amt\":\"5\",\"max_items_cart\":\"12\",\"delivery_boy_bonus_percentage\":\"1\",\"max_product_return_days\":\"1\",\"is_delivery_boy_otp_setting_on\":\"1\",\"cart_btn_on_list\":\"0\",\"expand_product_images\":\"0\",\"tax_name\":\"\",\"tax_number\":\"\"}'),
(7, 'payment_method', '{\"paypal_payment_method\":\"1\",\"paypal_mode\":\"sandbox\",\"paypal_business_email\":\"seller@somedomain.com\",\"currency_code\":\"USD\",\"razorpay_payment_method\":\"1\",\"razorpay_key_id\":\"rzp_test_key\",\"razorpay_secret_key\":\"secret_key\",\"paystack_payment_method\":\"1\",\"paystack_key_id\":\"paystack_public_key\",\"paystack_secret_key\":\"paystack_secret_key\",\"stripe_payment_method\":\"1\",\"stripe_payment_mode\":\"test\",\"stripe_publishable_key\":\"test_key\",\"stripe_secret_key\":\"test_key\",\"stripe_webhook_secret_key\":\"webhook_secret\",\"stripe_currency_code\":\"INR\",\"flutterwave_payment_method\":\"1\",\"flutterwave_public_key\":\"public_key\",\"flutterwave_secret_key\":\"secret_key\",\"flutterwave_encryption_key\":\"enc_key\",\"flutterwave_currency_code\":\"NGN\",\"paytm_payment_method\":\"1\",\"paytm_payment_mode\":\"sandbox\",\"paytm_merchant_key\":\"merchant_key\",\"paytm_merchant_id\":\"merchant_id\",\"cod_method\":\"1\"}'),
(8, 'about_us', '<p>About us <br></p>'),
(9, 'currency', 'KD'),
(11, 'email_settings', '{\"email\":\"houseofkidskw@gmail.com\",\"password\":\"AJ5ggwje\",\"smtp_host\":\"smtp.gmail.com\",\"smtp_port\":\"587\",\"mail_content_type\":\"html\",\"smtp_encryption\":\"tls\"}'),
(12, 'time_slot_config', '{\"time_slot_config\":\"1\",\"is_time_slots_enabled\":\"1\",\"delivery_starts_from\":\"2\",\"allowed_days\":\"7\"}'),
(13, 'favicon', 'uploads/media/2020/favicon-64.png'),
(14, 'delivery_boy_privacy_policy', '<p>ACCESSING, BROWSING OR OTHERWISE USING THE WEBSITE eShop.com, Missed Call Service or mobile application INDICATES user is in AGREEMENT with eShop vegetables & fruits Pvt Ltd for ALL THE TERMS AND CONDITIONS MENTIONED henceforth. User is requested to READ terms and conditions CAREFULLY BEFORE PROCEEDING FURTHER.<br>User is the person, group of person, company, trust, society, legal entity, legal personality or anyone who visits website, mobile app or gives missed call or places order with eShop via phone or website or mobile application or browse through website www.eShop.com.</p><p>eShop reserves the right to add, alter, change, modify or delete any of these terms and conditions at any time without prior information. The altered terms and conditions becomes binding on the user since the moment same are unloaded on the website www.eShop.com</p><p>eShop is in trade of fresh fruits and vegetables and delivers the order to home (user’s desired address) directly.</p><p>That any user who gives missed call/call for order on any number published/used by eShop.com, consents to receive, accept calls and messages or any after communication from eShop vegetables & fruits Pvt Ltd for Promotion and Telemarketing Purposes within a week.</p><p>If a customer do not wish to receive any communication from eShop, please SMS NO OFFERS to 9512512125.</p><p>eShop accept orders on all seven days and user will receive the delivery next day from date of order placement, as we at eShop procure the fresh produce from the procurement center and deliver it straight to user.</p><p>There is Minimum Order value of Rs. 200. There are no delivery charges on an order worth Rs. 200 or above. In special cases, if permitted, order value is less then Rs. 200/– , Rs. 40 as shipping charges shall be charged from user.</p><p>eShop updates the prices on daily basis and the price displayed at our website www.eShop.com, at the time of placement of order by user he/she/it will be charged as per the price listed at the website www.eShop.com.</p><p>In the event, though there are remote possibilities, of wrong invoice generation due to any reason, in case it happens eShop vegetables & fruits Pvt Ltd reserve its right to again raise the correct invoice at the revised amount and same shall be paid by user.</p><p>At times it is difficult to weigh certain vegetables or fruits exactly as per the order or desired quantity of user, hence the delivery might be with five percent variation on both higher or lower side of exact ordered quantity, user are hereby under takes to pay to eShop vegetables & fruits Pvt Ltd as per the final invoice. We at eShop understands and our endeavor is to always deliver in exact quantity in consonance with quantity ordered but every time it’s not possible but eShop guarantee the fair deal and weight to all its users. eShop further assures its users that at no instance delivery weights/quantity vary dramatically from what quantity ordered by user.</p><p>If some product is not available or is not of good quality, the same item will not be delivered and will be adjusted accordingly in the invoice; all rights in this regards are reserved with eShop. Images of Fruits & Vegetables present in the website are for demonstration purpose and may not resemble exactly in size, colour, weight, contrast etc; though we assure our best to maintain the best quality in product, which is being our foremost commitment to the customer.</p><p>All orders placed before 11 PM in the Night will be delivered next day or as per delivery date chosen.</p>'),
(15, 'delivery_boy_terms_conditions', '<p>ACCESSING, BROWSING OR OTHERWISE USING THE WEBSITE eShop.com, Missed Call Service or mobile application INDICATES user is in AGREEMENT with eShop vegetables & fruits Pvt Ltd for ALL THE TERMS AND CONDITIONS MENTIONED henceforth. User is requested to READ terms and conditions CAREFULLY BEFORE PROCEEDING FURTHER.<br>User is the person, group of person, company, trust, society, legal entity, legal personality or anyone who visits website, mobile app or gives missed call or places order with eShop via phone or website or mobile application or browse through website www.eShop.com.</p><p>eShop reserves the right to add, alter, change, modify or delete any of these terms and conditions at any time without prior information. The altered terms and conditions becomes binding on the user since the moment same are unloaded on the website www.eShop.com</p><p>eShop is in trade of fresh fruits and vegetables and delivers the order to home (user’s desired address) directly.</p><p>That any user who gives missed call/call for order on any number published/used by eShop.com, consents to receive, accept calls and messages or any after communication from eShop vegetables & fruits Pvt Ltd for Promotion and Telemarketing Purposes within a week.</p><p>If a customer do not wish to receive any communication from eShop, please SMS NO OFFERS to 9512512125.</p><p>eShop accept orders on all seven days and user will receive the delivery next day from date of order placement, as we at eShop procure the fresh produce from the procurement center and deliver it straight to user.</p><p>There is Minimum Order value of Rs. 200. There are no delivery charges on an order worth Rs. 200 or above. In special cases, if permitted, order value is less then Rs. 200/– , Rs. 40 as shipping charges shall be charged from user.</p><p>eShop updates the prices on daily basis and the price displayed at our website www.eShop.com, at the time of placement of order by user he/she/it will be charged as per the price listed at the website www.eShop.com.</p><p>In the event, though there are remote possibilities, of wrong invoice generation due to any reason, in case it happens eShop vegetables & fruits Pvt Ltd reserve its right to again raise the correct invoice at the revised amount and same shall be paid by user.</p><p>At times it is difficult to weigh certain vegetables or fruits exactly as per the order or desired quantity of user, hence the delivery might be with five percent variation on both higher or lower side of exact ordered quantity, user are hereby under takes to pay to eShop vegetables & fruits Pvt Ltd as per the final invoice. We at eShop understands and our endeavor is to always deliver in exact quantity in consonance with quantity ordered but every time it’s not possible but eShop guarantee the fair deal and weight to all its users. eShop further assures its users that at no instance delivery weights/quantity vary dramatically from what quantity ordered by user.</p><p>If some product is not available or is not of good quality, the same item will not be delivered and will be adjusted accordingly in the invoice; all rights in this regards are reserved with eShop. Images of Fruits & Vegetables present in the website are for demonstration purpose and may not resemble exactly in size, colour, weight, contrast etc; though we assure our best to maintain the best quality in product, which is being our foremost commitment to the customer.</p><p>All orders placed before 11 PM in the Night will be delivered next day or as per delivery date chosen.</p>'),
(16, 'web_logo', 'uploads/media/2021/logo2.png'),
(17, 'web_favicon', 'uploads/media/2021/faviocn.png'),
(18, 'web_settings', '{\"site_title\":\"House Of Kids\",\"support_number\":\"9876543210\",\"support_email\":\"support@eshop.com\",\"address\":\"test\",\"app_short_description\":\"test\",\"map_iframe\":\"test\",\"logo\":\"uploads\\/media\\/2021\\/logo2.png\",\"favicon\":\"uploads\\/media\\/2021\\/faviocn.png\",\"meta_keywords\":\"House Of Kids- eCommerce Online Store\",\"meta_description\":\"House Of Kids - eCommerce Online Store\",\"app_download_section\":true,\"app_download_section_title\":\"eShop Mobile App\",\"app_download_section_tagline\":\"Download eshop\",\"app_download_section_short_description\":\"Shop with us at affordable prices and get exciting cashback & offers.\",\"app_download_section_playstore_url\":\"#\",\"app_download_section_appstore_url\":\"#\",\"twitter_link\":\"#\",\"facebook_link\":\"#\",\"instagram_link\":\"#\",\"youtube_link\":\"#\",\"shipping_mode\":true,\"shipping_title\":\"Free Shipping\",\"shipping_description\":\"Free Shipping at your doorstep.\",\"return_mode\":true,\"return_title\":\"Free Returns\",\"return_description\":\"Free return if products are damaged.\",\"support_mode\":true,\"support_title\":\"Support 24\\/7\",\"support_description\":\"24\\/7 and 365 days support is available.\",\"safety_security_mode\":true,\"safety_security_title\":\"100% Safe & Secure\",\"safety_security_description\":\"100% safe & secure.\"}'),
(19, 'return_policy', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT '0',
  `image` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title1en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title2en` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `type`, `type_id`, `image`, `date_added`, `title1en`, `title2en`) VALUES
(1, 'categories', 13, 'uploads/media/2021/slider_new.jpg', '2021-05-26 07:02:55', 'Kids', 'Accessories'),
(2, 'categories', 12, 'uploads/media/2021/slider_new1.jpg', '2021-05-26 07:45:17', 'Magical n', 'Party Dress Up');

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8mb4_unicode_ci,
  `percentage` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `slug` varchar(32) NOT NULL,
  `image` varchar(512) DEFAULT NULL,
  `is_default` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `slug`, `image`, `is_default`, `status`, `created_on`) VALUES
(1, 'Classic', 'classic', 'classic.jpg', 0, 0, '2021-02-26 14:48:01'),
(2, 'Eurocom', 'eurocom', 'eurocom.png', 1, 1, '2021-02-26 14:48:01');

-- --------------------------------------------------------

--
-- Table structure for table `time_slots`
--

CREATE TABLE `time_slots` (
  `id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `last_order_time` time NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `transaction_type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `txn_id` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payu_txn_id` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `status` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payer_email` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payer_address` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payer_mobile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `transaction_type`, `user_id`, `order_id`, `type`, `txn_id`, `payu_txn_id`, `amount`, `status`, `currency_code`, `payer_email`, `payer_address`, `payer_mobile`, `message`, `response`, `transaction_date`, `date_created`) VALUES
(1, 'transaction', 0, '11', 'knet', '202127078331842', NULL, 62.9, 'success', NULL, NULL, '', '', 'Success', '', '2021-09-27 11:48:50', '2021-09-27 11:48:50'),
(2, 'transaction', 0, '11', 'knet', '202127078331842', NULL, 62.9, 'success', NULL, NULL, '', '', 'Success', '', '2021-09-27 12:55:36', '2021-09-27 12:55:36'),
(3, 'transaction', 1, '20', 'knet', '202127188523888', NULL, 120.8, 'success', NULL, NULL, '', '', 'Success', '', '2021-09-28 09:55:44', '2021-09-28 09:55:44'),
(4, 'transaction', 1, '20', 'knet', '202127188523888', NULL, 120.8, 'success', NULL, NULL, '', '', 'Success', '', '2021-09-28 09:56:25', '2021-09-28 09:56:25'),
(5, 'transaction', 32, '22', 'knet', '', NULL, 62.9, 'cancelled', NULL, NULL, '', '', 'Expired/Cancelled', '', '2021-10-05 08:36:24', '2021-10-05 08:36:24'),
(6, 'transaction', 0, '26', 'knet', '', NULL, 62.9, 'cancelled', NULL, NULL, '', '', 'Expired/Cancelled', '', '2021-10-05 10:45:10', '2021-10-05 10:45:10'),
(7, 'wallet', 1, NULL, 'credit', NULL, NULL, 120.8, NULL, NULL, NULL, '', '', 'Wallet Amount Credited for Order Item ID  : 20', '', '2021-10-12 15:02:30', '2021-10-12 15:02:30'),
(8, 'transaction', 1, '29', 'knet', '', NULL, 1254.5, 'cancelled', NULL, NULL, '', '', 'Expired/Cancelled', '', '2021-10-24 06:54:36', '2021-10-24 06:54:36'),
(9, 'transaction', 1, '30', 'knet', '', NULL, 1054.5, 'cancelled', NULL, NULL, '', '', 'Expired/Cancelled', '', '2021-10-24 06:55:51', '2021-10-24 06:55:51'),
(10, 'transaction', 0, '33', 'knet', '202129978609294', NULL, 10.01, 'success', NULL, NULL, '', '', 'Success', '', '2021-10-26 10:06:28', '2021-10-26 10:06:28');

-- --------------------------------------------------------

--
-- Table structure for table `updates`
--

CREATE TABLE `updates` (
  `id` int(11) NOT NULL,
  `version` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `updates`
--

INSERT INTO `updates` (`id`, `version`) VALUES
(1, '1.0'),
(2, '1.1'),
(3, '1.1.1'),
(4, '1.1.2'),
(5, '2.0.0'),
(6, '2.0.1'),
(7, '2.0.2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `balance` double DEFAULT '0',
  `activation_selector` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgotten_password_selector` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgotten_password_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgotten_password_time` int(11) DEFAULT NULL,
  `remember_selector` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bonus` int(11) DEFAULT NULL,
  `dob` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `country_code` int(11) DEFAULT NULL,
  `city` text COLLATE utf8mb4_unicode_ci,
  `area` text COLLATE utf8mb4_unicode_ci,
  `street` text COLLATE utf8mb4_unicode_ci,
  `pincode` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apikey` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_code` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `friends_code` varchar(28) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fcm_id` text COLLATE utf8mb4_unicode_ci,
  `latitude` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `how_to_know` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `first_name`, `last_name`, `username`, `password`, `email`, `mobile`, `image`, `balance`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `company`, `address`, `bonus`, `dob`, `country_id`, `country_code`, `city`, `area`, `street`, `pincode`, `apikey`, `referral_code`, `friends_code`, `fcm_id`, `latitude`, `longitude`, `how_to_know`, `created_at`) VALUES
(1, '127.0.0.1', '', '', 'Administrator', '$2y$12$sBCMK.mZ/EMKmd8sIqIlnedHVbN2YTZz0hzPkfaJk49YuHR7K.bdS', 'support@eurocom.com', '50625825', NULL, 1613.8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1268889823, 1636529158, 1, 'ADMIN', NULL, NULL, NULL, 0, 91, '44', '138', NULL, NULL, NULL, NULL, NULL, 'dJXa6kH3Tzm6NBGwON5fhe:APA91bEFYijAUaRSRliyj0JXMTFm7SRGtXBFWoIOwH8f7VwkdG5xy0JsUpBH8sqO-_dGGZFxkP1oocj3kpKh-gOfkVDsaiqUYE_lunE7dlCqec9W-iL4kda6vO7qtOn7pFsAk6D2qLwz', NULL, NULL, '', '2020-06-30 10:20:08'),
(26, '103.103.174.43', '', '', 'shinoj k', '$2y$10$KVg9qVnuUwkpRWveV6S/3u22QmmDxnprWttSp0K9u0yS87hQY6Zxm', 'shinojsnew@gmail.com', '09645649931', NULL, 0, 'd5013904709c7f3396c9', '$2y$10$ffsz3f6fFrvDn9cebNQzie4Lx3N7qoH5H4DifL/FQTi9dptXOEIFK', NULL, NULL, NULL, NULL, NULL, 1627888911, NULL, 1, NULL, NULL, NULL, NULL, 0, 965, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-08-02 07:21:51'),
(27, '37.231.188.73', '', '', 'ammar', '$2y$10$yLlOO07UxPKiby494LPwDuZHg1.hfhJjI/RZVihpb14WjjXRgKQ16', 'ammar@chrisansgroup.com', '96599633042', NULL, 0, '46091ffc1711f4f11af8', '$2y$10$7gJ0shy0ddvoU77KnTsaM.eEj.QDUXFUbNY.KkJyqzvNJXikGY832', NULL, NULL, NULL, NULL, NULL, 1627912122, 1627983512, 1, NULL, NULL, NULL, NULL, 0, 965, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-08-02 13:48:42'),
(28, NULL, 'Ragi', 'Siva', 'ragigopal@gmail.com', '$2y$10$HVS7t0uiKoR7EOHqfcp58.srK/sIyjTpbxaF3WChQ/0iOk7LTE70G', 'ragigopal@gmail.com', '07025903270', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1629790752, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, '1', 'dsf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-08-24 07:39:12'),
(29, '137.97.83.0', 'shefiz', 'sha', 'shefiz.sha@gmail.com', '$2y$10$1jPCoQW/.dVXZBltRdcPHe2n6hCz5HH.BTQyXXz9DLasEXkVhcN8C', 'shefiz.sha@gmail.com', '09995533783', NULL, 0, '3205575fa26b9fb75a72', '$2y$10$rnVoiDQh/eR7Lh8ZQhe.c.A59n7wCvShVLlI.YGqlLf512KPnJSJq', NULL, NULL, NULL, NULL, NULL, 1629909908, 1630336323, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-08-25 16:45:08'),
(30, NULL, 'Ragi', 'Siva', 'ragiammu1991@gmail.com', '$2y$10$HVS7t0uiKoR7EOHqfcp58.srK/sIyjTpbxaF3WChQ/0iOk7LTE70G', 'ragiammu1991@gmail.com', '07025903270', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1629790752, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, '1', 'dsf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-08-24 07:39:12'),
(31, '188.70.29.44', '', '', 'Test', '$2y$10$ZIxFvsboWSMdLzcIv0km/uReFBzUD4ejBlK/MafXfigSG.jwse4Tq', 'test@eurocom.com', NULL, NULL, 0, '59c69c6f4e8a31790aec', '$2y$10$qK1bTIfMPbmlLuBI0urqxuGtFJFF.EX5IRE0Wo6Ld8ofv5KLB8gpi', NULL, NULL, NULL, NULL, NULL, 1630909514, NULL, 1, NULL, 'Hawally', 10, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-09-06 06:25:14'),
(32, NULL, 'Abdul', 'Gafoor', 'stockanalyst@intleuro.com', '$2y$10$O4NGfcf8I65iDv98Q18lxe9Jtp7gz1RS9mohNqd3nCaX4hSitDPka', 'stockanalyst@intleuro.com', '66329078', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1633422950, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2', '34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-10-05 08:35:50'),
(33, '202.164.139.36', 'ragi', 'K g', 'ragigopal1@gmail.com', '$2y$10$JGIbImbuOXhsXAimDQaN/uTSAs.QgnP4wWANEsfHiM6ReTs2lvL/S', 'ragigopal1@gmail.com', '4334343434', NULL, 0, '05e6f608f3e95c135c9c', '$2y$10$CicWugD279e1texU355myOAqQ/Y1FLtrLfLpzgvIPYsJSLZRzNf4K', NULL, NULL, NULL, NULL, NULL, 1635141234, 1635141245, 1, NULL, NULL, NULL, NULL, 117, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Search Engine', '2021-10-25 05:53:54');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 2),
(4, 4, 2),
(5, 5, 2),
(6, 6, 2),
(7, 7, 2),
(8, 8, 2),
(9, 9, 2),
(10, 10, 2),
(11, 11, 2),
(12, 12, 2),
(13, 13, 2),
(14, 14, 2),
(15, 15, 2),
(16, 16, 2),
(17, 17, 2),
(18, 18, 2),
(19, 19, 2),
(20, 20, 2),
(21, 21, 2),
(22, 22, 2),
(23, 23, 2),
(24, 24, 2),
(25, 25, 2),
(26, 26, 2),
(27, 27, 2),
(28, 29, 2),
(29, 31, 3),
(30, 33, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE `user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `permissions` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_permissions`
--

INSERT INTO `user_permissions` (`id`, `user_id`, `role`, `permissions`, `created_by`) VALUES
(1, 1, 0, NULL, '2020-11-18 04:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_transactions`
--

CREATE TABLE `wallet_transactions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'credit | debit',
  `amount` double NOT NULL,
  `message` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_set_id` (`attribute_set_id`);

--
-- Indexes for table `attribute_set`
--
ALTER TABLE `attribute_set`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_variant_id` (`product_variant_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_api_keys`
--
ALTER TABLE `client_api_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_boy_notifications`
--
ALTER TABLE `delivery_boy_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_boy_id` (`delivery_boy_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `fund_transfers`
--
ALTER TABLE `fund_transfers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_boy_id` (`delivery_boy_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter_subscribers`
--
ALTER TABLE `newsletter_subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `delivery_boy_id` (`delivery_boy_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_variant_id` (`product_variant_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `payment_requests`
--
ALTER TABLE `payment_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_rating`
--
ALTER TABLE `product_rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `promo_codes`
--
ALTER TABLE `promo_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `return_requests`
--
ALTER TABLE `return_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `variable` (`variable`);

--
-- Indexes for table `settings1`
--
ALTER TABLE `settings1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `variable` (`variable`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_slots`
--
ALTER TABLE `time_slots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `updates`
--
ALTER TABLE `updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mobile` (`mobile`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wallet_transactions`
--
ALTER TABLE `wallet_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `attribute_set`
--
ALTER TABLE `attribute_set`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `attribute_values`
--
ALTER TABLE `attribute_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `client_api_keys`
--
ALTER TABLE `client_api_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT for table `delivery_boy_notifications`
--
ALTER TABLE `delivery_boy_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fund_transfers`
--
ALTER TABLE `fund_transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3003;

--
-- AUTO_INCREMENT for table `newsletter_subscribers`
--
ALTER TABLE `newsletter_subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `payment_requests`
--
ALTER TABLE `payment_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `product_rating`
--
ALTER TABLE `product_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_variants`
--
ALTER TABLE `product_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;

--
-- AUTO_INCREMENT for table `promo_codes`
--
ALTER TABLE `promo_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `return_requests`
--
ALTER TABLE `return_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `settings1`
--
ALTER TABLE `settings1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `time_slots`
--
ALTER TABLE `time_slots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `updates`
--
ALTER TABLE `updates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `user_permissions`
--
ALTER TABLE `user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wallet_transactions`
--
ALTER TABLE `wallet_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
