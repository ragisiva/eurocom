<?php
defined('BASEPATH') or exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Import_product extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(['ion_auth', 'form_validation', 'upload']);
        $this->load->helper(['url', 'language', 'file']);
        $this->load->model(['product_model', 'category_model', 'rating_model','brand_model','import_model']);

        if (!has_permissions('read', 'product')) {
            $this->session->set_flashdata('authorize_flag', PERMISSION_ERROR_MSG);
            redirect('admin/home', 'refresh');
        }
    }

    public function index(){
        if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
            $this->data['insertedProduct'] = '';
            $this->data['main_page'] = FORMS . 'import';
            $settings = get_settings('system_settings', true);
            $this->data['title'] = 'Import | ' . $settings['app_name'];
            $this->data['meta_description'] = 'Import | ' . $settings['app_name'];
            $this->load->view('admin/template', $this->data);
        }
        else{
            redirect('admin/login', 'refresh');
        }
    }
        public function export(){
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');
        $writer = new Xlsx($spreadsheet);
        $filename = 'name-of-the-generated-file';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file
        }
    public function import(){
        $insertedProduct= array();
        if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {   
            $file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            if(isset($_FILES['upload_file']['name']) && in_array($_FILES['upload_file']['type'], $file_mimes)) {
            $arr_file = explode('.', $_FILES['upload_file']['name']);
            $extension = end($arr_file);
            if('csv' == $extension){
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } else {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            }
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();$j=0;

           
            if (!empty($sheetData)) {
                for ($i=1; $i<count($sheetData); $i++) {
              
                  
                  //  echo $team;
                    $sku = ($sheetData[$i][0]!='') ? $sheetData[$i][0] : '';
                    $product_name = ($sheetData[$i][5]!='') ? $sheetData[$i][5] : '';
                    $short_description = ($sheetData[$i][6]!='') ? $sheetData[$i][6] : '';
                    $price = ($sheetData[$i][7]!='') ? $sheetData[$i][7] : 0;
                    $sale_price = ($sheetData[$i][8]!='') ? $sheetData[$i][8] : 0;
                    $slug = create_unique_slug($product_name, 'products');
                    $tags = ($sheetData[$i][13]!='') ? $sheetData[$i][13] : "";
                    $brand = ($sheetData[$i][1]!='') ? trim(strtoupper($sheetData[$i][1])) : '';
                    $category = ($sheetData[$i][2]!='') ? trim(($sheetData[$i][2])) : '';
                    $sub_cat_1 = ($sheetData[$i][3]!='') ? trim(($sheetData[$i][3])) : '';
                    $sub_cat_2 = ($sheetData[$i][4]!='') ? trim(($sheetData[$i][4])) : '';
                    $main_image = ($sheetData[$i][9]!='') ? $sheetData[$i][9] : '';
                    $other_images = ($sheetData[$i][10]!='') ? explode(',',$sheetData[$i][10]) : '';
                    $description = ($sheetData[$i][11]!='') ? $sheetData[$i][11] : '';
                    $specification = ($sheetData[$i][12]!='') ? $sheetData[$i][12] : '';
                    
                    $total_stock = ($sheetData[$i][14]!='') ? $sheetData[$i][14] : 100;
                    $availability = ($sheetData[$i][15]!='') ? $sheetData[$i][15] : '';
                    $cod_allowed  = ($sheetData[$i][16]!='') ? $sheetData[$i][16] : '';
                    $minimum_order_quantity = ($sheetData[$i][17]!='') ? $sheetData[$i][17] : 1;
                    $quantity_step_size = ($sheetData[$i][18]!='') ? $sheetData[$i][18] : 1;
                    $total_allowed_quantity = ($sheetData[$i][19]!='') ? $sheetData[$i][19] : 10;
                    $is_returnable = ($sheetData[$i][20]!='') ? $sheetData[$i][20] : '';
                    $is_cancelable = ($sheetData[$i][21]!='') ? $sheetData[$i][21] : '';

                    //fetch data from DB
                   
                    $brand_id = $this->import_model->getBrand($brand);
                    $cat_id = $this->import_model->getCategory($category,$sub_cat_1,$sub_cat_2);
                   
                    if($main_image!='' && file_exists('uploads/media/'.date('Y').'/'.$main_image)){
                        $main_image = 'uploads/media/'.date('Y').'/'.$main_image;
                    }
                    else{
                        $main_image ='';
                    }
                    //other images
                    $otherImgArray = []; $oi=0;
                    if($other_images !=''){
                        foreach($other_images as $img_url){
                            if($main_image!='' && file_exists('uploads/media/'.date('Y').'/'. $img_url)){
                                $otherImgArray[$oi] = 'uploads/media/'.date('Y').'/'.$img_url;
                                $oi++;
                            }
                        }
                    }
                    if($sheetData[$i][10]!='')
                        $otherImages = json_encode($otherImgArray);
                    else
                       $otherImages ='';
                    
                   if(strtoupper($availability) =='NO'){
                        $availability = 0;
                   }
                   else{
                    $availability=1;
                   }
                   if(strtoupper($cod_allowed) =='NO'){
                    $cod_allowed =0;
                   }
                   else{
                    $cod_allowed =1;
                   }
                   if(strtoupper($is_returnable) == 'NO'){
                        $is_returnable = 0;
                   }
                   else{
                        $is_returnable = 1;
                   }
                   if(strtoupper($is_cancelable) == 'NO'){
                    $is_cancelable = 0;$cancelable_till ='';
                    }
                    else{
                        $is_cancelable = 1;
                        $cancelable_till ='received';
                    }
                    $pro_type = 'simple_product';
                    $tax =0;
                    $made_in ='';
                    $indicator ='';
                    $pro_input_sizeimage ='';
                    $warranty_period ='';
                    $guarantee_period='';
                    $video_type='';
                    $video='';
                    //data to product table
                    $pro_data = [
                        'sku'=>$sku ,
                        'name' => $product_name,
                        'short_description' => $short_description,
                        'slug' => $slug,
                        'type' => $pro_type,
                        'tax' => $tax,
                        'category_id' => $cat_id,
                        'made_in' => $made_in,
                        'indicator' => $indicator,
                        'image' => $main_image,
                        'sizechart'=>$pro_input_sizeimage,
                        'total_allowed_quantity' => $total_allowed_quantity,
                        'minimum_order_quantity' => $minimum_order_quantity,
                        'quantity_step_size' => $quantity_step_size,
                        'warranty_period' => $warranty_period,
                        'guarantee_period' => $guarantee_period,
                        'other_images' => $otherImages,
                        'video_type' => $video_type,
                        'video' => $video,
                        'tags' => $tags,
                        'description' => $description,
                        'specification' =>$specification,
                        'brand' => $brand_id,
                        'stock_type'=>'0',
                        'stock'=>$total_stock,
                        'availability'=>$availability,
                        'is_returnable'=>$is_returnable,
                        'is_cancelable'=>$is_cancelable,
                        'cancelable_till'=>$cancelable_till
                    ];

                    $pro_attr_data = [
                        'attribute_value_ids' =>'',
            
                    ];
                    $pro_variance_data = [                        
                        'price' => $price,
                        'special_price' =>  $sale_price,
                        'sku' =>$sku,
                         'stock'=>$total_stock,
                        'availability'=>$availability
                    ];
                    $product_id = $this->import_model->addProduct($pro_data, $pro_attr_data,$pro_variance_data);
                    $insertedProduct[$j] = array(
                        'SKU'=>$sku,
                        'product_id' =>$product_id
                    );
                    $j++;
                }
            }
            }
            
            $response['error'] = false;
            $response['message'] = 'Product Imported successfully !';
            $response['result'] = $insertedProduct;
            print_r(json_encode($response));
        }
        else{
            redirect('admin/login', 'refresh');
        }
    }
       
}