<?php defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(['ion_auth', 'form_validation']);
        $this->load->helper(['url', 'language']);
        $this->lang->load('auth');
        $this->data['is_logged_in'] = ($this->ion_auth->logged_in()) ? 1 : 0;
        $this->data['user'] = ($this->ion_auth->logged_in()) ? $this->ion_auth->user()->row() : array();
        $this->response['csrfName'] = $this->security->get_csrf_token_name();
        $this->response['csrfHash'] = $this->security->get_csrf_hash();
        $this->data['settings'] = get_settings('system_settings', true);
        $this->data['web_settings'] = get_settings('web_settings', true);
        $cookie_lang = ($this->session->userdata('lang') !='') ? $this->session->userdata('lang', TRUE) : 'english' ;
        if($cookie_lang=='arabic'){
            $this->data['lang_prefix'] = 'ar_';
        }
        else{
            $this->data['lang_prefix'] ='';
        }
    }
 public function index()
 {
	 if ($this->ion_auth->logged_in()) {
		redirect('My_account', 'refresh'); 
	 }
	 $this->data['main_page'] = 'login';
        $this->data['title'] = 'Login Page | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Login Page, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Login Page | ' . $this->data['web_settings']['meta_description'];
		$this->load->view('front-end/' . THEME . '/template', $this->data);
 }
    public function login_check()
    {
        if (!$this->ion_auth->logged_in()) {
            $this->data['main_page'] = 'home';
            $this->data['title'] = 'Login Panel | ' . $this->data['settings']['app_name'];
            $this->data['meta_description'] = 'Login Panel | ' . $this->data['settings']['app_name'];

            $identity_column = $this->config->item('identity', 'ion_auth');
            if ($identity_column == 'mobile') {
                $this->form_validation->set_rules('mobile', 'Mobile', 'trim|numeric|required|xss_clean');
            } elseif ($identity_column == 'email') {
                $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
            } else {
                $this->form_validation->set_rules('identity', 'Identity', 'trim|required|xss_clean');
            }
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

            $login = $this->ion_auth->login($this->input->post('mobile'), $this->input->post('password'));
            if ($login) {
                $data = fetch_details(['mobile' => $this->input->post('mobile', true)], 'users');
                $username = $this->session->set_userdata('username', $data[0]['username']);
                $this->response['error'] = false;   
                $this->response['message'] = 'Login Succesfully';   
                echo json_encode($this->response);
                return false;
            }else{
                $this->response['error'] = true;   
                $this->response['message'] = 'Mobile Number or Password is wrong.';   
                echo json_encode($this->response);
                return false;
            }
        } else {
            $this->response['error'] = true;
            $this->response['message'] = 'You are already logged in.';
            echo json_encode($this->response);
            return false;
        }
    }

    public function logout()
    {
        $this->ion_auth->logout();
        redirect('home', 'refresh');
    }

    public function update_user()
    { 

        $identity_column = $this->config->item('identity', 'ion_auth');
        $identity = $this->session->userdata('identity');
        $user = $this->ion_auth->user()->row();
        if ($identity_column == 'email') {
            $this->form_validation->set_rules('email', $this->lang->line('email'), 'required|xss_clean|trim|valid_email|edit_unique[users.email.' . $user->id . ']',array('edit_unique' =>  $this->lang->line('reg_email_validation'),'required' => str_replace('{field}',$this->lang->line('email'),$this->lang->line('reqired_field_validation')),'valid_email' => str_replace('{field}',$this->lang->line('email'),$this->lang->line('valid_email_validation'))));
        } else {
            $this->form_validation->set_rules('mobile', $this->lang->line('phone'), 'required|xss_clean|trim|numeric|edit_unique[users.mobile.' . $user->id . ']',array('edit_unique' =>  $this->lang->line('reg_email_validation'),'required' => str_replace('{field}',$this->lang->line('phone'),$this->lang->line('reqired_field_validation')),'numeric' => str_replace('{field}',$this->lang->line('phone'),$this->lang->line('numeric_field_validation'))));
        }
        $this->form_validation->set_rules('first_name', $this->lang->line('first_name'), 'required|xss_clean|trim',array('required' => str_replace('{field}',$this->lang->line('first_name'),$this->lang->line('reqired_field_validation'))));
        $this->form_validation->set_rules('last_name', $this->lang->line('last_name'), 'required|xss_clean|trim',array('required' => str_replace('{field}',$this->lang->line('last_name'),$this->lang->line('reqired_field_validation'))));
        if (!empty($_POST['old']) || !empty($_POST['new']) || !empty($_POST['new_confirm'])) {
            $this->form_validation->set_rules('old', $this->lang->line('old_password'), 'required',array('required' => str_replace('{field}',$this->lang->line('old_password'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('new', $this->lang->line('password'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[new_confirm]',array('required' => str_replace('{field}',$this->lang->line('password'),$this->lang->line('reqired_field_validation')),'min_length' =>str_replace('{field}',$this->lang->line('password'),str_replace('{param}',$this->config->item('min_password_length', 'ion_auth'),$this->lang->line('min_length_validation')))));
            $this->form_validation->set_rules('new_confirm', $this->lang->line('confirm_password'), 'required',array('required' => str_replace('{field}',$this->lang->line('confirm_password'),$this->lang->line('reqired_field_validation'))));
        }


        $tables = $this->config->item('tables', 'ion_auth');
        if (!$this->form_validation->run()) {
            if (validation_errors()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                echo json_encode($this->response);
                return false;
                exit();
            }
            if ($this->session->flashdata('message')) {
                $this->response['error'] = false;
                $this->response['message'] = $this->session->flashdata('message');
                echo json_encode($this->response);
                return false;
                exit();
            }
        } else {

            if (!empty($_POST['old']) || !empty($_POST['new']) || !empty($_POST['new_confirm'])) {
                if (!$this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'))) {
                    // if the login was un-successful
                    $this->response['error'] = true;
                    $this->response['message'] = $this->ion_auth->errors();
                    echo json_encode($this->response);
                    return false;
                }
            }
            $user_details = ['first_name' => $this->input->post('first_name'), 'last_name' => $this->input->post('last_name'), 'email' => $this->input->post('email')];
            $user_details = escape_array($user_details);
            $this->db->set($user_details)->where($identity_column, $identity)->update($tables['login_users']);
            $this->response['error'] = false;
            $this->response['message'] = $this->lang->line('profile_update_success');
            echo json_encode($this->response);
            return false;
        }
    }
}
