<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Knet extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();		
		$this->load->helper('string');
        $this->load->library('knet_lib');
        $this->load->model(['cart_model', 'address_model', 'order_model', 'transaction_model','ion_auth_model']);
        $this->data['is_logged_in'] = ($this->ion_auth->logged_in()) ? 1 : 0;
        $this->data['user'] = ($this->ion_auth->logged_in()) ? $this->ion_auth->user()->row() : array();
        $this->response['csrfName'] = $this->security->get_csrf_token_name();
        $this->response['csrfHash'] = $this->security->get_csrf_hash();
        $this->data['settings'] = get_settings('system_settings', true);
        $this->data['web_settings'] = get_settings('web_settings', true);
        $this->data['cookie_lang'] =$cookie_lang= ($this->session->userdata('lang') !='') ? $this->session->userdata('lang', TRUE) : 'english' ;
        if($cookie_lang=='arabic'){
            $this->data['lang_prefix'] = 'ar_';
        }
        else{
            $this->data['lang_prefix'] ='';
        }
    }

    public function request() {
        if($this->ion_auth->logged_in()){
            $user_id = $this->data['user']->id;
        }
        else{
            $user_id = 0;
        }

        $order_id =$this->input->post('knet_order_id');
        if($order_id >0){
        $order_details = fetch_orders($order_id, $user_id, false, false, 1, NULL, NULL, NULL, NULL);
        $reference_num =uniqid();
    //  print_r($order_details['order_data'][0]);exit;
        if($order_details){
            $order  = $order_details['order_data'][0];
            $amount = $order['final_total'];
            $lang = ($this->data['cookie_lang']=='arabic') ? 'ar' : 'en' ;
            $udf1='';
            $udf2='';
            $udf3='';
            $udf4='';
            $udf5='';
            $return_url = base_url('knet/response');
            $payment_type = 1;
            $currency =  $this->data['settings']['currency'];
            $data = array(
                'transactionID' =>$order_id,
                'amount'=>$amount,
                'referenceID' => $reference_num,
                'udf1'=>$udf1,
                'udf2'=>$udf2,
                'udf3'=>$udf3,
                'udf4'=>$udf4,
                'udf5'=>$udf5,
                'payment_type'=>$payment_type,
                'lang' =>$lang,
                'return_url' =>$return_url 
            );
            echo $this->knet_lib->request($data);
        }
        else{
            redirect(base_url('payment/cancel'));exit;
        }
    }
    else{
        redirect(base_url());exit;
    }
        
    }

    function response() {
        $encrp = $this->input->get('encrp');

     
        $response = $this->knet_lib->response($encrp);
      //  print_r($response);exit;
        if($response->Status ==-1){//invalid
            redirect(base_url());exit;
        }
        elseif($response->Status ==0){//invalid access
            redirect(base_url());exit;
        }
        elseif($response->Status ==1){//payment success and caputred
            $PayId = $response->TrackId;
            $ReceiptNo = $response->ReceiptNo;
            $order_id  = $response->PayId;
            $Message = $response->Message;
            $Amount = $response->Amount;           
            $PayType = $response->PayType;
 $order_details = fetch_orders($order_id, null, false, false, 1, NULL, NULL, NULL, NULL);
  $order  = $order_details['order_data'][0];
  $address = $order['address'];
     $mobile = $order['mobile'];
      $user_id = $order['user_id'];
          
              
                $transaction_data = array(
                    'transaction_type' => 'transaction',
                    'user_id' => $user_id,
                    'order_id' => $order_id,
                    'type' => $PayType,
                    'txn_id' => $ReceiptNo,
                    'payu_txn_id' =>  $PayId,
                    'amount' => $Amount,
                    'payer_address' => $address,
                    'payer_mobile' =>$mobile,
                    'status' => 'success',
                    'message' =>$Message,
                    'response' =>json_encode($response)
                );
                $this->transaction_model->add_transaction($transaction_data); // update transaction table
                $order_items_data =  $this->order_model->get_order_items($order_id);
                $product_variant_ids = array_column($order_items_data,'product_variant_id');
                $qtns = array_column($order_items_data,'qty');
                $order_item_ids = array_column($order_items_data,'id');
                // update order table
                // print_r($order_items_data);
                // print_r($product_variant_ids);
                // print_r($qtns);
                $this->order_model->update_order(array('status'=>'received'), array('id'=>$order_id),true,'orders');
                $this->order_model->update_order(['active_status' =>'received'], ['id' => $order_id]);
                //update order items
                if($order_item_ids){
                    foreach($order_item_ids as $order_item_id){
                        $this->order_model->update_order_item($order_item_id,'received');
                    }
                }
                
               
                 $overall_total = array(
                    'total_amount' => $order['total'],
                    'delivery_charge' => $order['delivery_charge'],
                    'tax_amount' =>$order['total_payable'],
                    'tax_percentage' =>0,
                    'discount' =>  $order['promo_discount'],
                    'wallet' =>  $order['wallet_balance'],
                    'final_total' =>  $order['final_total'],
                    'otp' => $order['otp'],
                    'address' => $order['address'],
                    'payment_method' => $order['payment_method']
                );
                //send  mail to customer and admin
                $system_settings = get_settings('system_settings',true);
                $address = json_decode($order['address']);
                $user[0] =  array(
                    'email' =>$address->email,
                    'first_name' =>$address->name,
                    'last_name' =>'',
                    'mobile' =>$address->mobile,
                    'area' =>$address->area,
                    'street' =>$address->street                  
                );
                $overall_order_data = array(
                    'cart_data' => $order_items_data,
                    'order_data' => $overall_total,
                    'subject' => 'Order received successfully',
                    'user_data' => $user[0],
                    'system_settings' => $system_settings,
                    'user_msg' => 'Hello, Dear ' . ucfirst($user[0]['first_name'].' '.$user[0]['last_name']) . ', We have received your order successfully. Your order summaries are as followed',
                    'otp_msg' => 'Here is your OTP. Please, give it to delivery boy only while getting your order.',
                    'order_id' =>$order_id
                );
               
                if(isset($system_settings['support_email']) && !empty($system_settings['support_email'])){
                    send_mail($system_settings['support_email'], 'New order placed ID #'.$order_id, 'New order received for '.$system_settings['app_name'].' please process it.');
                }
                $mail_response = send_mail($address->email, 'Order received successfully', $this->load->view('admin/pages/view/email-template.php', $overall_order_data, TRUE));
                
                update_stock($product_variant_ids, $qtns); //update stock
                $data['user_id'] = $this->session->userdata('cartsession');
                $data['product_variant_id'] = implode(',',$product_variant_ids);
                $this->cart_model->remove_from_cart($data); //remove cart
               // exit;
                redirect(base_url('payment/success'));exit;
           
        }
        elseif($response->Status ==2){//payment failed
            $PayId = $response->TrackId;
            $ReceiptNo = $response->ReceiptNo;
            $order_id  = $response->PayId;
            $Message = $response->Message;
            $Amount = $response->Amount;           
            $PayType = $response->PayType;
            $CCMessage = $response->CCMessage;
 $order_details = fetch_orders($order_id, null, false, false, 1, NULL, NULL, NULL, NULL);
  $order  = $order_details['order_data'][0];
  $address = $order['address'];
     $mobile = $order['mobile'];
      $user_id = $order['user_id'];          
              
                $transaction_data = array(
                    'transaction_type' => 'transaction',
                    'user_id' => $user_id,
                    'order_id' => $order_id,
                    'type' => $PayType,
                    'txn_id' => $ReceiptNo,
                    'payu_txn_id' =>  $PayId,
                    'amount' => $Amount,
                    'payer_address' => $address,
                    'payer_mobile' =>$mobile,
                    'status' => 'failed',
                    'message' =>$Message,
                    'response' =>json_encode($response)
                );
                $this->transaction_model->add_transaction($transaction_data); // update transaction table
                $order_items_data =  $this->order_model->get_order_items($order_id);
                $product_variant_ids = array_column($order_items_data,'product_variant_id');
                $qtns = array_column($order_items_data,'qty');
                $order_item_ids = array_column($order_items_data,'id');
              
                $this->order_model->update_order(array('status'=>'payment_failed'), array('id'=>$order_id),true,'orders');
                $this->order_model->update_order(['active_status' =>'payment_failed'], ['id' => $order_id]);
                //update order items
                if($order_item_ids){
                    foreach($order_item_ids as $order_item_id){
                        $this->order_model->update_order_item($order_item_id,'payment_failed');
                    }
                }
                
               
                               
                $data['user_id'] = $this->session->userdata('cartsession');
                $data['product_variant_id'] = implode(',',$product_variant_ids);
                //$this->cart_model->remove_from_cart($data); //remove cart
               // exit;
                redirect(base_url('payment/cancel'));exit;
        }
        elseif($response->Status ==3){//expired or cancelled
            $PayId = $response->TrackId;
            $ReceiptNo = $response->ReceiptNo;
            $order_id  = $response->PayId;
            $Message = $response->Message;
            $Amount = $response->Amount;           
            $PayType = $response->PayType;
            $CCMessage = $response->CCMessage;
 $order_details = fetch_orders($order_id, null, false, false, 1, NULL, NULL, NULL, NULL);
  $order  = $order_details['order_data'][0];
  $address = $order['address'];
     $mobile = $order['mobile'];
      $user_id = $order['user_id'];          
              
                $transaction_data = array(
                    'transaction_type' => 'transaction',
                    'user_id' => $user_id,
                    'order_id' => $order_id,
                    'type' => $PayType,
                    'txn_id' => $ReceiptNo,
                    'payu_txn_id' =>  $PayId,
                    'amount' => $Amount,
                    'payer_address' => $address,
                    'payer_mobile' =>$mobile,
                    'status' => 'cancelled',
                    'message' =>$Message,
                    'response' =>json_encode($response)
                );
                $this->transaction_model->add_transaction($transaction_data); // update transaction table
                $order_items_data =  $this->order_model->get_order_items($order_id);
                $product_variant_ids = array_column($order_items_data,'product_variant_id');
                $qtns = array_column($order_items_data,'qty');
                $order_item_ids = array_column($order_items_data,'id');
              
                $this->order_model->update_order(array('status'=>'payment_cancelled'), array('id'=>$order_id),true,'orders');
                $this->order_model->update_order(['active_status' =>'payment_cancelled'], ['id' => $order_id]);
                //update order items
                if($order_item_ids){
                    foreach($order_item_ids as $order_item_id){
                        $this->order_model->update_order_item($order_item_id,'payment_cancelled');
                    }
                }
                
               
                               
                $data['user_id'] = $this->session->userdata('cartsession');
                $data['product_variant_id'] = implode(',',$product_variant_ids);
                //$this->cart_model->remove_from_cart($data); //remove cart
               // exit;
                redirect(base_url('payment/cancel'));exit;
            
        }
        else{
            redirect(base_url('payment/cancel'));exit;
        }
    }

}