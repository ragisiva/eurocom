<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(['url', 'language', 'timezone_helper']);
        $this->load->model(['address_model', 'category_model', 'cart_model', 'faq_model','brand_model','ion_auth_model']);
        $this->data['is_logged_in'] = ($this->ion_auth->logged_in()) ? 1 : 0;
        $this->data['user'] = ($this->ion_auth->logged_in()) ? $this->ion_auth->user()->row() : array();
        $this->data['settings'] = get_settings('system_settings', true);        
        $this->data['web_settings'] = get_settings('web_settings', true);
        $cookie_lang = ($this->session->userdata('lang') !='') ? $this->session->userdata('lang', TRUE) : 'english' ;
        if($cookie_lang=='arabic'){
            $this->data['lang_prefix'] =  'ar_';
        }
        else{
            $this->data['lang_prefix']='';
        }
        
        $this->response['csrfName'] = $this->security->get_csrf_token_name();
        $this->response['csrfHash'] = $this->security->get_csrf_hash();
    }

    public function index()
    {
        $this->data['main_page'] = 'home';
        $this->data['title'] = 'Home | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Home, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Home | ' . $this->data['web_settings']['meta_description'];

        $limit =  12;
        $offset =  0;
        $sort = 'row_order';
        $order =  'ASC';
        $has_child_or_item = 'false';
        $filters = [];
        /* Fetching Categories Sections */
        $categories = $this->category_model->get_categories('', $limit, $offset, $sort, $order, false);
		$brands = $this->brand_model->get_categories('', $limit, $offset, $sort, $order, false);
        /* Fetching Featured Sections */

        $sections = $this->db->limit($limit, $offset)->order_by('row_order')->get('sections')->result_array();
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
        $filters['show_only_active_products'] = true;
        if (!empty($sections)) {
            for ($i = 0; $i < count($sections); $i++) {
                $product_ids = explode(',', $sections[$i]['product_ids']);
                $product_ids = array_filter($product_ids);
                $product_categories = (isset($sections[$i]['categories']) && !empty($sections[$i]['categories']) && $sections[$i]['categories'] != NULL) ? explode(',', $sections[$i]['categories']) : null;
                if (isset($sections[$i]['product_type']) && !empty($sections[$i]['product_type'])) {
                    $filters['product_type'] = (isset($sections[$i]['product_type'])) ? $sections[$i]['product_type'] : null;
                }

                if ($sections[$i]['style'] == "default") {
                    $limit = 10;
                } elseif ($sections[$i]['style'] == "style_1" || $sections[$i]['style'] == "style_2") {
                    $limit = 7;
                } elseif ($sections[$i]['style'] == "style_3" || $sections[$i]['style'] == "style_4") {
                    $limit = 5;
                } else {
                    $limit = null;
                }
               
                if(is_array($product_categories)){
                    $section_categories = $this->db->where_in('id', $product_categories)->order_by('row_order')->get('categories')->result_array();
                    $lang_prefix = $this->data['lang_prefix'];
                    $section_categories = array_column($section_categories,$lang_prefix.'name','id');
                    $sections[$i]['tab_categories'] = $section_categories;
                    $products = array(); 
                    $total_products = 0;
                    foreach($product_categories as $category){
                        $products[$category] = fetch_product_variants($user_id, (isset($filters)) ? $filters : null, (isset($product_ids)) ? $product_ids : null, $category, $limit, null, null, null);
                        $total_products = isset($products[$category]['product']) ? $total_products + count($products[$category]['product']) : $total_products;
                    }
                }
                else{
                    $products = fetch_product_variants($user_id, (isset($filters)) ? $filters : null, (isset($product_ids)) ? $product_ids : null, $product_categories, $limit, null, null, null);
                    $total_products = isset($products[$category]['product']) ? count($products['product']) : 0;
                }
                $sections[$i]['title'] =  output_escaping($sections[$i]['title']);
                $sections[$i]['slug'] =  url_title($sections[$i]['title'], 'dash', true);
                $sections[$i]['short_description'] =  output_escaping($sections[$i]['short_description']);
            //    $sections[$i]['filters'] = (isset($products['filters'])) ? $products['filters'] : [];
            //    $sections[$i]['product_details'] =  $products['product'];
            //    unset($sections[$i]['product_details'][0]['total']);
               $sections[$i]['product_details'] = $products;
               $sections[$i]['total_products'] = $total_products;
               unset($product_details);
            }
        }
        $l_filter=[];
        $l_filter['product_type'] = 'new_added_products';
        $l_filter['show_only_active_products'] = true;
        $this->data['latest_product'] = fetch_product_variants($user_id, (isset($l_filter)) ? $l_filter : null,  null, null, 10, null, null, null);

        if(array_key_exists('recently_viewed',$_COOKIE)){
            $cookieget = get_cookie('recently_viewed');
            $productids = unserialize($cookieget);
          // print_r($productids);
            $this->data['recently_viewed_product'] = fetch_product_variants($user_id, array('product_variant_ids'=>$productids),null, null, 10, null, null, null);
            //print_r( $this->data['recently_viewed_product']);
        }
        else{
            $this->data['recently_viewed_product'] = '';
        }
        $t_filter=[];
        $t_filter['product_type'] = 'most_selling_products';
        $t_filter['show_only_active_products'] = true;
        $this->data['top_selling_product'] =fetch_product_mostselled($user_id, (isset($t_filter)) ? $t_filter : null,  null, null, 10, null, null, null);
        $this->data['sections'] = $sections;
        $this->data['categories'] = $categories;
		$this->data['brands'] = $brands;
        $this->data['username'] = $this->session->userdata('username');
        $this->data['sliders'] = get_sliders();
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }

    public function error_404()
    {
        $this->data['main_page'] = 'error_404';
        $this->data['title'] = 'Product cart | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Product cart, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Product cart | ' . $this->data['web_settings']['meta_description'];
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }

    public function categories()
    {
        $limit =  50;
        $offset =  0;
        $sort = 'row_order';
        $order =  'ASC';
        $has_child_or_item = 'false';
        $this->data['main_page'] = 'categories';
        $this->data['title'] = 'Categories | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Categories, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Categories | ' . $this->data['web_settings']['meta_description'];
        $this->data['categories'] = $this->category_model->get_categories('', $limit, $offset, $sort, $order, false);
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
    public function get_ajax_products()
    {
        $category_id = (isset($_POST['search_cat'])) ? $_POST['search_cat'] : null;
        $search_key =  (isset($_POST['search_key'])) ? $_POST['search_key'] : null;
        $filters['search'] =  (isset($_POST['search_key'])) ? $_POST['search_key'] : null;
        $products = fetch_product_variants(NULL, (isset($filters)) ? $filters : null, NULL, $category_id, 10, '0');
        
        if (!empty($products['product'])) {
           
            $products['product'] = array_map(function ($d) {
                $lang_prefix =($this->session->userdata('lang') =='arabic') ? 'ar_' : '';
                $variant_id =$d['variant_id'] ;
                $image = json_decode($d['variant_images']);
                $main_image =$d['image'];
                $m = $lang_prefix.'variant_values'; 
                if(isset($image[0]) && $image[0] !=''){                                           
                    $d['image_sm'] = get_image_url($image[0],'thumb', 'sm');
                }
                else{
                    $d['image_sm'] =get_image_url($main_image,'thumb', 'sm');
                }
                $d['link'] = base_url('products/details/' . $d['slug'].'/'.$variant_id);
                $d['product_name'] = $d[$lang_prefix.'name'].' '.$d[$m];
                return $d;
            }, $products['product']);
            $this->response['error'] = false;
            $this->response['message'] = !empty($this->lang->line('product_retrive_success')) ? $this->lang->line('product_retrive_success') : "Products retrieved successfully !";
            $this->response['data'] = $products['product'];
        }
        else{
            $this->response['error'] = true;
            $this->response['message'] = !empty($this->lang->line('product_not_found')) ? $this->lang->line('product_not_found') : "Products Not Found !";
            $this->response['data'] =  '';
        }
        print_r(json_encode($this->response));
    }
    public function get_products() 
    {

        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('id', 'Product ID', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('search', 'Search', 'trim|xss_clean');
        $this->form_validation->set_rules('category_id', 'Category id', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('sort', 'sort', 'trim|xss_clean');
        $this->form_validation->set_rules('limit', 'limit', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('offset', 'offset', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('order', 'order', 'trim|xss_clean|alpha');

        if (!$this->form_validation->run()) {
            $this->response['error'] = true;
            $this->response['message'] = strip_tags(validation_errors());
            $this->response['data'] = array();
        } else {

            $limit = (isset($_GET['limit'])) ? $this->input->post('limit', true) : 25;
            $offset = (isset($_GET['offset'])) ? $this->input->post('offset', true) : 0;
            $order = (isset($_GET['order']) && !empty(trim($_GET['order']))) ? $_GET['order'] : 'DESC';
            $sort = (isset($_GET['sort']) && !empty(trim($_GET['sort']))) ? $_GET['sort'] : 'p.id';
            $filters['search'] =  (isset($_GET['search'])) ? $_GET['search'] : null;
            $filters['attribute_value_ids'] = (isset($_GET['attribute_value_ids'])) ? $_GET['attribute_value_ids'] : null;
            $category_id = (isset($_GET['category_id'])) ? $_GET['category_id'] : null;
            $product_id = (isset($_GET['id'])) ? $_GET['id'] : null;
            $user_id = (isset($_GET['user_id'])) ? $_GET['user_id'] : null;

            $products = fetch_product($user_id, (isset($filters)) ? $filters : null, $product_id, $category_id, $limit, $offset, $sort, $order);
            $first_search_option[0] = array(
                'id' => 0,
                'image_sm' => base_url(get_settings('favicon')),
                'name' => 'Search Result for ' . $_GET['search'],
                'category_name' => 'all categories',
                'link' => base_url('products/search?q=' . $_GET['search']),
            );
            if (!empty($products['product'])) {
                $products['product'] = array_map(function ($d) {
                    $d['link'] = base_url('products/details/' . $d['slug']);
                    return $d;
                }, $products['product']);
                $this->response['error'] = false;
                $this->response['message'] = "Products retrieved successfully !";
                $this->response['filters'] = (isset($products['filters']) && !empty($products['filters'])) ? $products['filters'] : [];
                $this->response['total'] = (isset($products['total'])) ? strval($products['total']) : '';
                $this->response['offset'] = (isset($_GET['offset']) && !empty($_GET['offset'])) ? $_GET['offset'] : '0';
                $products['product'] = array_merge($first_search_option, $products['product']);
                $this->response['data'] = $products['product'];
            } else {
                $this->response['error'] = true;
                $this->response['message'] = "Products Not Found !";
                $this->response['data'] =  $first_search_option;
            }
        }
        print_r(json_encode($this->response));
    }

    public function address_list()
    {
        if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
            return $this->address_model->get_address_list();
        } else {
            redirect('admin/login', 'refresh');
        }
    }

    public function checkout()
    {
        $this->data['main_page'] = 'checkout';
        $this->data['title'] = 'Checkout | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Checkout, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Checkout | ' . $this->data['web_settings']['meta_description'];
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }

    public function terms_and_conditions()
    {
        $this->data['main_page'] = 'terms-and-conditions';
        $this->data['title'] = 'Terms & Conditions | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Terms & Conditions, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Terms & Conditions | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'Terms & Conditions | ' . $this->data['web_settings']['site_title'];
        $lang_prefix = $this->data['lang_prefix'];
        $this->data['terms_and_conditions'] =($lang_prefix =='') ? get_settings('terms_conditions') : get_settings($lang_prefix.'terms_conditions');
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
    public function returns()
    {
        $this->data['main_page'] = 'returns';
        $this->data['title'] = 'Returns | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Returns, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Returns | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'Returns | ' . $this->data['web_settings']['site_title'];
        $lang_prefix = $this->data['lang_prefix'];
        $this->data['returns'] = ($lang_prefix =='') ? get_settings('return') : get_settings($lang_prefix.'return');
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
    public function delivery_information()
    {
        $this->data['main_page'] = 'delivery_information';
        $this->data['title'] = 'Delivery Information | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Delivery Information, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Delivery Information | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'Delivery Information | ' . $this->data['web_settings']['site_title'];
        $lang_prefix = $this->data['lang_prefix'];
        $this->data['delivery_informations'] = ($lang_prefix =='') ? get_settings('delivery_informations'): get_settings($lang_prefix.'delivery_informations');
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
    public function privacy_policy()
    {
        $this->data['main_page'] = 'privacy-policy';
        $this->data['title'] = 'Privacy Policy | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Privacy Policy, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Privacy Policy | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'Privacy Policy | ' . $this->data['web_settings']['site_title'];
        $lang_prefix = $this->data['lang_prefix'];
        $this->data['privacy_policy'] = ($lang_prefix =='') ? get_settings('privacy_policy') : get_settings($lang_prefix.'privacy_policy');
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
    public function about_us()
    {
        $this->data['main_page'] = 'about-us';
        $this->data['title'] = 'About US | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'About US, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'About US | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'About US | ' . $this->data['web_settings']['site_title'];
        $lang_prefix = $this->data['lang_prefix'];
        $this->data['about_us'] =  ($lang_prefix =='') ? get_settings('about_us') : get_settings($lang_prefix.'about_us');
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
    public function services() 
    {
        $this->data['main_page'] = 'services';
        $this->data['title'] = 'Services | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Services, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Services| ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'Services | ' . $this->data['web_settings']['site_title'];
        $lang_prefix = $this->data['lang_prefix'];
        $this->data['services'] =  ($lang_prefix =='') ? get_settings('services') : get_settings($lang_prefix.'services');
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
    public function careers()
    {
        $this->data['main_page'] = 'careers';
        $this->data['title'] = 'Careers | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Careers, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Careers | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'Careers | ' . $this->data['web_settings']['site_title'];
        $this->data['services'] = get_settings('careers');
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
    public function contact_us()
    {
        $this->data['main_page'] = 'contact-us';
        $this->data['title'] = 'Contact US | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Contact US, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Contact US | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'Contact US | ' . $this->data['web_settings']['site_title'];
        $this->data['contact_us'] = get_settings('contact_us');
        $this->data['web_settings'] = get_settings('web_settings', true);
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }

    public function faq()
    {
        $this->data['main_page'] = 'faq';
        $this->data['title'] = 'FAQ | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'FAQ, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'FAQ | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'FAQ | ' . $this->data['web_settings']['site_title'];
        $this->data['faq'] = $this->faq_model->get_faqs(null, null, null, null);
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
	
	public function brands()
    {
        $this->data['main_page'] = 'brands';
        $this->data['title'] = 'Brands | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Brands, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Brands | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'Brands | ' . $this->data['web_settings']['site_title'];
        $this->data['brands'] = $this->brand_model->get_categories(null, null, null, 'row_order');
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
	public function createMember()
    {
        if ($this->ion_auth->logged_in()) {
            redirect('my-account', 'refresh');
        }
        else{
        $this->data['main_page'] = 'register';
        $this->data['title'] = 'Register | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Register, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Register | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'Register | ' . $this->data['web_settings']['site_title'];        
        $this->load->view('front-end/' . THEME . '/template', $this->data);
        }
    }

    /**
     * Log the user in
     */
    public function login()
    {
        $this->data['title'] = $this->lang->line('login_heading');
        $identity_column = 'email';
        // validate form input
        $this->form_validation->set_rules('identity', $this->lang->line('email'), 'required',array('required' => str_replace('{field}',$this->lang->line('email'),$this->lang->line('reqired_field_validation'))));
        $this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('password')), 'required',array('required' => str_replace('{field}',$this->lang->line('password'),$this->lang->line('reqired_field_validation'))));

        if ($this->form_validation->run() === TRUE) {
            $tables = $this->config->item('tables', 'ion_auth');
            $identity = $this->input->post('identity', true);
            $res = $this->db->select('id')->where($identity_column, $identity)->get($tables['login_users'])->result_array();
            if (!empty($res)) {
                // check to see if the user is logging in
                // check for "remember me"
                $remember = (bool)$this->input->post('remember');

                if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                    //check cart empty or  not
                    $userData = ($this->ion_auth->logged_in()) ? $this->ion_auth->user()->row() : array();
                    $user_ID = $userData->id;
                    $user_cart_count = $this->cart_model->get_cart_by_customer($user_ID);
                    if($user_cart_count >0){

                        if ($this->session->userdata('cartsession')!='') {                            
                            $this->cart_model->update_cart_by_customer($user_ID,$this->session->userdata('cartsession'));
                        }
                        else{
                            $this->session->set_userdata('cartsession',$this->session->session_id);
                            $this->cart_model->update_cart_by_customer($user_ID,$this->session->userdata('cartsession'));
                        }
                    }
                    if ($this->session->userdata('cartsession')!='') {
                        $this->cart_model->update_cart_customer($user_ID,$this->session->userdata('cartsession'));
                    }
                    //if the login is successful
                    if (!$this->input->is_ajax_request()) {
                        redirect('admin/home', 'refresh');
                    }
                    $this->response['error'] = false;
                    $this->response['message'] = $this->ion_auth->messages();
                    echo json_encode($this->response);
					//redirect('My_account', 'refresh');
                } else {
                    // if the login was un-successful
                    $this->response['error'] = true;
                    $this->response['message'] = $this->ion_auth->errors();
                    echo json_encode($this->response);
                }
            } else {
                $this->response['error'] = true;
                $this->response['message'] = '<div>'.$this->lang->line('incorrect_login').'</div>';
                echo json_encode($this->response);
            }
        } else {
            // the user is not logging in so display the login page
            if (validation_errors()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                echo json_encode($this->response);
                return false;
                exit();
            }
            if ($this->session->flashdata('message')) {
                $this->response['error'] = false;
                $this->response['message'] = $this->session->flashdata('message');
                echo json_encode($this->response);
                return false;
                exit();
            }

            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['identity'] = [
                'name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
            ];

            $this->data['password'] = [
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
            ];

            $this->_render_page('auth' . DIRECTORY_SEPARATOR . 'login', $this->data);
        }
    }
public function forgot_password(){
    if ($this->ion_auth->logged_in()) {
        redirect('my-account', 'refresh');
    }
    else{
    $this->data['main_page'] = 'forgot_password';
    $this->data['title'] = 'Forgot Password | ' . $this->data['web_settings']['site_title'];
    $this->data['keywords'] = 'Forgot Password, ' . $this->data['web_settings']['meta_keywords'];
    $this->data['description'] = 'Forgot Password | ' . $this->data['web_settings']['meta_description'];
    $this->data['meta_description'] = 'Forgot Password | ' . $this->data['web_settings']['site_title'];        
    $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
}
    public function lang($lang_name = '')
    {
        if (empty($lang_name)) {
            redirect(base_url());
        }

        $language = get_languages(null, $lang_name);
        if (empty($language)) {
            redirect(base_url());
        }
        $this->lang->load('web_labels_lang', $lang_name);
        $cookie = array(
            'name'   => 'language',
            'value'  => $lang_name,
            'expire' => time() + 1000
        );
        $this->input->set_cookie($cookie);
        if (isset($_SERVER['HTTP_REFERER'])) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect(base_url());
        }
    }
    public function language_settings(){
        $language_code = $this->input->post('language_code');
        if($language_code!=''){
            $this->lang->load('web_labels_lang', $language_code);
            $this->session->unset_userdata(['lang']);
            $this->session->set_userdata(['lang' =>$language_code]);
            $this->response['error'] = false;
            $this->response['message'] = 'success';
        }
        else{
            $this->response['error'] = true;
            $this->response['message'] = 'failed';
        }
        
        print_r(json_encode($this->response));
      
    }
    
    public function reset_password($code = NULL)
	{
        if ($this->ion_auth->logged_in()) {
            redirect('home', 'refresh');
        }
        else{
		if (!$code)
		{
			show_404();
		}

		$this->data['title'] = $this->lang->line('reset_password_heading');
		
		$user = $this->ion_auth_model->get_user_by_forgotten_password_code($code);

		if (!is_object($user))
		{
			$this->set_error('password_change_unsuccessful');
			return FALSE;
		}
		else
		{
			
			$this->data['main_page'] = 'reset_password';
			$this->data['title'] = 'Reset Password | ' . $this->data['web_settings']['site_title'];
			$this->data['keywords'] = 'Reset Password, ' . $this->data['web_settings']['meta_keywords'];
			$this->data['description'] = 'Reset Password | ' . $this->data['web_settings']['meta_description'];
			$this->data['meta_description'] = 'Reset Password | ' . $this->data['web_settings']['site_title']; 
            $this->data['code'] = $code;
			$this->data['userDetails'] = $user;  
    		$this->load->view('front-end/' . THEME . '/template', $this->data);
		}
    }

	}
    
    public function send_contact_us_email()
    {
        $this->form_validation->set_rules('name', $this->lang->line('name'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('name'),$this->lang->line('reqired_field_validation'))));
        $this->form_validation->set_rules('email', $this->lang->line('email'), 'trim|required|xss_clean|valid_email',array('required'=>$this->lang->line('reqired_field_validation'),'valid_email' => str_replace('{field}',$this->lang->line('email'),$this->lang->line('valid_email_validation'))));
        $this->form_validation->set_rules('mobile', $this->lang->line('mobile'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('reqired_field_validation')),'numeric' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('numeric_field_validation'))));
        $this->form_validation->set_rules('subject', $this->lang->line('subject'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('subject'),$this->lang->line('reqired_field_validation'))));
        $this->form_validation->set_rules('message', $this->lang->line('message'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('message'),$this->lang->line('reqired_field_validation'))));
        if (!$this->form_validation->run()) {
            $this->response['error'] = true;
            $this->response['message'] = validation_errors();
            print_r(json_encode($this->response));
            return false;
        }
else{
        $name = $this->input->post('name', true);
        $email = $this->input->post('email', true);
        $mobile = $this->input->post('mobile', true);
        $subject = $this->input->post('subject', true);
        $message = $this->input->post('message', true);
        $web_settings = get_settings('web_settings',true);
        $to = $web_settings['support_email'];
        $email_message = "Name : " . $name . "<br>"
            . "Email : " . $email . "<br>"
            . "Mobile : " . $mobile . "<br>"
            . "Subject : ".$subject. "<br>"
            . "Message : " . $message . "<br>";
            send_mail($to , 'Contact email from Eurocom.', $email_message);
       
            $this->response['error'] = false;
            $this->response['message'] = $this->lang->line('contact_success');
            $this->response['data'] = array();
            echo json_encode($this->response);
            return false;
}
        
    }
	
    public function send_career_email()
    {
        $this->form_validation->set_rules('name', $this->lang->line('name'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('name'),$this->lang->line('reqired_field_validation'))));
        $this->form_validation->set_rules('email', $this->lang->line('email'), 'trim|required|xss_clean|valid_email',array('required'=>$this->lang->line('reqired_field_validation'),'valid_email' => str_replace('{field}',$this->lang->line('email'),$this->lang->line('valid_email_validation'))));
        $this->form_validation->set_rules('mobile', $this->lang->line('mobile'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('reqired_field_validation')),'numeric' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('numeric_field_validation'))));
        $this->form_validation->set_rules('position', $this->lang->line('position_applied_for'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('position_applied_for'),$this->lang->line('reqired_field_validation'))));
       
        if (!$this->form_validation->run()) {
            $this->response['error'] = true;
            $this->response['message'] = validation_errors();
            print_r(json_encode($this->response));
            return false;
        }
else{
        $name = $this->input->post('name', true);
        $email = $this->input->post('email', true);
        $mobile = $this->input->post('mobile', true);
        $position = $this->input->post('position', true);
       
        $web_settings = get_settings('web_settings',true);
        $to = $web_settings['support_email'];
        $email_message = "Name : " . $name . "<br>"
            . "Email : " . $email . "<br>"
            . "Mobile : " . $mobile . "<br>"
            . "Applied position : ".$position. "<br>";
           
            send_mail($to , 'Career email from Eurocom.', $email_message);
       
            $this->response['error'] = false;
            $this->response['message'] = $this->lang->line('contact_success');
            $this->response['data'] = array();
            echo json_encode($this->response);
            return false;
}
        
    }
	
	 public function track_order()
    {
        $this->form_validation->set_rules('orderid', 'Orderid', 'trim|required|xss_clean');
       

        if (!$this->form_validation->run()) {
            $this->response['error'] = true;
            $this->response['message'] = validation_errors();
            print_r(json_encode($this->response));
            return false;
        }
		 $orderid = $this->input->post('orderid', true);
		$row = fetch_orders($orderid, NULL, false, false, $limit, $offset, 'date_added', 'DESC', NULL);
		if(count($row)==0)
		{
			$this->response['error'] = true;
            $this->response['message'] = "Not Found. Please Try Again";
            $this->response['data'] = "Error";
            echo json_encode($this->response);
            return false;
		}
		else
		{
			$html='';
			$html.='<br/><br/><h4>Your OrderID : '.$orderid.' is '.ucwords($row['order_data'][0]['active_status']).'</h4>';
			
			 $html.='<ul id="progressbar">';
												
                                            $status = array('received', 'processed', 'shipped', 'delivered');
                                            $i = 1;
                                            foreach ($item['status'] as $value) { 
                                               
                                                $class = '';
                                                if ($value[0] == "cancelled" || $value[0] == "returned") {
                                                    $class = 'cancel';
                                                    $status = array();
                                                } elseif (($ar_key = array_search($value[0], $status)) !== false) {
                                                    unset($status[$ar_key]);
                                                }
                                                 $html.='<li class="active $class" id="step'.$i.'">
                                                    <p>'.strtoupper($value[0]).'</p>
                                                    <p>'.$value[1].'</p>
                                                </li>';
                                           
                                                $i++;
                                            } 
 foreach ($status as $value) { 
 $newclass='';
 if($value==$row['order_data'][0]['active_status']) {  $newclass='class="active"'; } 
 
											$html.='<li '.$newclass.'  id="step'.$i.'">
                                                    <p>'.strtoupper($value).'</p>
                                                </li>';
                                            $i++;
                                            } 
											$html.='</ul>';
			$this->response['error'] = false;
            $this->response['message'] = '';
            $this->response['data'] = $html;
            echo json_encode($this->response);
            return false;
		}
       
    }
	
	 public function track_my_order()
    {
        $this->data['main_page'] = 'track-my-order';
        $this->data['title'] = 'Track My Order | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Track My Order, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Track My Order | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'Track My Order | ' . $this->data['web_settings']['site_title'];
  
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
    public function special_offers()
    {
        $this->data['main_page'] = 'special-offers';
        $this->data['title'] = 'Special Offer | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Special Offer, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Special Offer | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = 'Special Offer | ' . $this->data['web_settings']['site_title'];
  
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }


    public function newsletter_submit(){
        $this->form_validation->set_rules('newsletter_email', $this->lang->line('email'), 'trim|required|xss_clean|valid_email',array('required'=>$this->lang->line('reqired_field_validation'),'valid_email' => str_replace('{field}',$this->lang->line('email'),$this->lang->line('valid_email_validation'))));
        if (!$this->form_validation->run()) {
            $this->response['error'] = true;
            $this->response['message'] = validation_errors();
            $this->response['data'] = array();
           echo json_encode($this->response);
            return false;
        }
        else{

            $check_exist = $this->address_model->check_subscribtion_exist($this->input->post('newsletter_email'));
            if($check_exist >0){
                $this->response['error'] = true;
                $this->response['message'] = $this->lang->line('already_subscribed');
                $this->response['data'] = array();
                echo json_encode($this->response);
                return false;
            }
            else{
                $data = array(
                    'email' =>$this->input->post('newsletter_email'),
                    'status'=>1,
                    'subscribed_date' =>date('Y-m-d')
                );
                $this->address_model->add_subscription($data);
                $response['error'] = false;
                $response['csrfName'] = $this->security->get_csrf_token_name();
                $response['csrfHash'] = $this->security->get_csrf_hash();
                $response['message'] =  $this->lang->line('subscribe_success');						
                echo json_encode($response);
            }
           
        }
       
    }
}
