<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cart extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
		
		$this->load->helper('string');
        $this->load->library(['cart', 'razorpay', 'stripe', 'paystack','ion_auth','form_validation','session']);
        $this->paystack->__construct('test');
        $this->load->model(['cart_model', 'address_model', 'order_model', 'transaction_model','ion_auth_model','setting_model']);
        $this->data['is_logged_in'] = ($this->ion_auth->logged_in()) ? 1 : 0;
        $this->data['user'] = ($this->ion_auth->logged_in()) ? $this->ion_auth->user()->row() : array();
        $this->response['csrfName'] = $this->security->get_csrf_token_name();
        $this->response['csrfHash'] = $this->security->get_csrf_hash();
        $this->data['settings'] = get_settings('system_settings', true);
        $this->data['web_settings'] = get_settings('web_settings', true);
        $cookie_lang = ($this->session->userdata('lang') !='') ? $this->session->userdata('lang', TRUE) : 'english' ;
        if($cookie_lang=='arabic'){
            $this->data['lang_prefix'] = 'ar_';
        }
        else{
            $this->data['lang_prefix'] ='';
        }
    }

    public function index()
    {
       
            $this->data['main_page'] = 'cart';
            $this->data['title'] = 'Product Cart | ' . $this->data['web_settings']['site_title'];
            $this->data['keywords'] = 'Product Cart, ' . $this->data['web_settings']['meta_keywords'];
            $this->data['description'] = 'Product Cart | ' . $this->data['web_settings']['meta_description'];
            $this->data['cart'] = get_cart_total($_SESSION['cartsession']);
            $this->data['save_for_later'] = get_cart_total($_SESSION['cartsession'], false, 1);
            $this->load->view('front-end/' . THEME . '/template', $this->data);
        
    }

    public function manage()
    {
	
		if($this->session->userdata('cartsession')=='')
		{
		 $this->session->set_userdata('cartsession',$this->session->session_id);	
		}
        if($this->ion_auth->logged_in()){
            $user_id = $this->data['user']->id;
        }
        else{
            $user_id = 0;
        }

            $this->form_validation->set_rules('product_variant_id', $this->lang->line('product_variant'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('product_variant'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('qty', $this->lang->line('quantity'), 'trim|required|xss_clean|numeric',array('required' => str_replace('{field}',$this->lang->line('quantity'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('is_saved_for_later', 'Saved For Later', 'trim|xss_clean');

            if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return false;
            }
			$qty=$this->input->post('qty');
           
			if(!isset($qty))
			{
			$qty=1;	
			}
			else 
			{
			$qty=$this->input->post('qty');	
			}
            $is_variant_available_in_cart = is_variant_available_in_cart($_POST['product_variant_id'], $this->session->userdata('cartsession'));            
            if (!$is_variant_available_in_cart) {
                $data = array(
                    'product_variant_id' => $this->input->post('product_variant_id', true),
                    'qty' => $qty,
                    'is_saved_for_later' => $this->input->post('is_saved_for_later', true),
                    'user_id' => $this->session->userdata('cartsession'),
                    'customer_id' =>$user_id
                );
            }
        else{
                $existing_row = get_variant_products_in_cart($_POST['product_variant_id'], $this->session->userdata('cartsession'));
                $existing_qty = $existing_row->qty;
                $data = array(
                    'product_variant_id' => $this->input->post('product_variant_id', true),
                    'qty' => $existing_qty+$qty,
                    'is_saved_for_later' => $this->input->post('is_saved_for_later', true),
                    'user_id' => $this->session->userdata('cartsession'),
                    'customer_id' =>$user_id
                );
            }
            
            $_POST['user_id'] = $this->session->userdata('cartsession');
            $settings = get_settings('system_settings', true);
            $cart_count = get_cart_count($this->session->userdata('cartsession'));
            if ($cart_count[0]['total']+$qty > $settings['max_items_cart']) {
                    $this->response['error'] = true;
                    $this->response['message'] = str_replace('%s',$settings['max_items_cart'],$this->lang->line('max_allowed_cart_item'));
                    $this->response['data'] = array();
                    print_r(json_encode($this->response));
                    return;
            }           
         
            if (!$this->cart_model->add_to_cart($data)) {
               // if ($qty == 0) {
                    $res = get_cart_total($this->session->userdata('cartsession'), false);
                // } else {
                //     $res = get_cart_total($this->session->userdata('cartsession'), $_POST['product_variant_id']);
                // }

                $this->response['error'] = false;
                $this->response['message'] = $this->lang->line('item_added_to_cart');
                $this->response['data'] = [
                   // 'total_quantity' => ($qty == 0) ? '0' : strval($_POST['qty']),
                    'total_quantity' => (isset($res[0]['total_items'])) ? strval($res[0]['total_items']) : "0",
                    'sub_total' => strval($res['sub_total']),
                    'total_items' => (isset($res[0]['total_items'])) ? strval($res[0]['total_items']) : "0",
                    'tax_percentage' => (isset($res['tax_percentage'])) ? strval($res['tax_percentage']) : "0",
                    'tax_amount' => (isset($res['tax_amount'])) ? strval($res['tax_amount']) : "0",
                    'cart_count' => (isset($res[0]['cart_count'])) ? strval($res[0]['cart_count']) : "0",
                    'max_items_cart' => $this->data['settings']['max_items_cart'],
                    'overall_amount' => $res['overall_amount'],
                    'items' => $this->cart_model->get_user_cart($this->session->userdata('cartsession')),
                ];
                print_r(json_encode($this->response));
                return false;
            }
        
		
		/*else {
            $this->response['error'] = true;
            $this->response['message'] = 'Please Login first to use Cart.';
            $this->response['data'] = $this->data;
            echo json_encode($this->response);
            return false;
        }*/
    }
    public function update_cart()
    {
	
		if($this->session->userdata('cartsession')=='')
		{
		 $this->session->set_userdata('cartsession',$this->session->session_id);	
		}
        if($this->ion_auth->logged_in()){
            $user_id = $this->data['user']->id;
        }
        else{
            $user_id = 0;
        }

            $this->form_validation->set_rules('product_variant_id', $this->lang->line('product_variant'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('product_variant'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('qty', $this->lang->line('quantity'), 'trim|required|xss_clean|numeric',array('required' => str_replace('{field}',$this->lang->line('quantity'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('is_saved_for_later', 'Saved For Later', 'trim|xss_clean');

            if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return false;
            }
			$qty=$this->input->post('qty');
           
			if(!isset($qty))
			{
			$qty=1;	
			}
			else 
			{
			$qty=$this->input->post('qty');	
			}
            $is_variant_available_in_cart = is_variant_available_in_cart($_POST['product_variant_id'], $this->session->userdata('cartsession'));            
            if (!$is_variant_available_in_cart) {
                $data = array(
                    'product_variant_id' => $this->input->post('product_variant_id', true),
                    'qty' => $qty,
                    'is_saved_for_later' => $this->input->post('is_saved_for_later', true),
                    'user_id' => $this->session->userdata('cartsession'),
                    'customer_id' =>$user_id
                );
            }
        else{
                $existing_row = get_variant_products_in_cart($_POST['product_variant_id'], $this->session->userdata('cartsession'));
                
                $data = array(
                    'product_variant_id' => $this->input->post('product_variant_id', true),
                    'qty' => $qty,
                    'is_saved_for_later' => $this->input->post('is_saved_for_later', true),
                    'user_id' => $this->session->userdata('cartsession'),
                    'customer_id' =>$user_id
                );
            }
            
            $_POST['user_id'] = $this->session->userdata('cartsession');
            $settings = get_settings('system_settings', true);
            $cart_count = get_cart_count($this->session->userdata('cartsession'));
            if ($cart_count[0]['total']+$qty > $settings['max_items_cart']) {
                    $this->response['error'] = true;
                    $this->response['message'] =str_replace('%s',$settings['max_items_cart'],$this->lang->line('max_allowed_cart_item'));
                    $this->response['data'] = array();
                    print_r(json_encode($this->response));
                    return;
            }           
         
            if (!$this->cart_model->add_to_cart($data)) {
               // if ($qty == 0) {
                    $res = get_cart_total($this->session->userdata('cartsession'), false);
                // } else {
                //     $res = get_cart_total($this->session->userdata('cartsession'), $_POST['product_variant_id']);
                // }

                $this->response['error'] = false;
                $this->response['message'] =  $this->lang->line('item_added_to_cart');;
                $this->response['data'] = [
                   // 'total_quantity' => ($qty == 0) ? '0' : strval($_POST['qty']),
                    'total_quantity' => (isset($res[0]['total_items'])) ? strval($res[0]['total_items']) : "0",
                    'sub_total' => strval($res['sub_total']),
                    'total_items' => (isset($res[0]['total_items'])) ? strval($res[0]['total_items']) : "0",
                    'tax_percentage' => (isset($res['tax_percentage'])) ? strval($res['tax_percentage']) : "0",
                    'tax_amount' => (isset($res['tax_amount'])) ? strval($res['tax_amount']) : "0",
                    'cart_count' => (isset($res[0]['cart_count'])) ? strval($res[0]['cart_count']) : "0",
                    'max_items_cart' => $this->data['settings']['max_items_cart'],
                    'overall_amount' => $res['overall_amount'],
                    'items' => $this->cart_model->get_user_cart($this->session->userdata('cartsession')),
                ];
                print_r(json_encode($this->response));
                return false;
            }
        
		
		/*else {
            $this->response['error'] = true;
            $this->response['message'] = 'Please Login first to use Cart.';
            $this->response['data'] = $this->data;
            echo json_encode($this->response);
            return false;
        }*/
    }
    // remove_from_cart
    public function remove()
    {
        $this->form_validation->set_rules('product_variant_id', $this->lang->line('product_variant'), 'trim|numeric|xss_clean|required',array('required' => str_replace('{field}',$this->lang->line('product_variant_id'),$this->lang->line('reqired_field_validation'))));
        if (!$this->form_validation->run()) {
            $this->response['error'] = true;
            $this->response['message'] = validation_errors();
            $this->response['data'] = array();
            print_r(json_encode($this->response));
            return false;
        } else {
            //Fetching cart items to check wheather cart is empty or not
            $cart_total_response = get_cart_total($_SESSION['cartsession']);

            if (!isset($cart_total_response[0]['total_items'])) {
                $this->response['error'] = true;
                $this->response['message'] = $this->lang->line('empty_cart_message');
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return false;
            }

            $data = array(
                'user_id' => $_SESSION['cartsession'],
                'product_variant_id' => $this->input->post('product_variant_id', true),
            );
            if ($this->cart_model->remove_from_cart($data)) {
                $this->response['error'] = false;
                $this->response['message'] = $this->lang->line('remove_from_cart');
                print_r(json_encode($this->response));
                return false;
            } else {
                $this->response['error'] = true;
                $this->response['message'] = $this->lang->line('item_notremove_msg');
                echo json_encode($this->response);
                return false;
            }
        }
    }
    public function clear()
    {
        if ($this->data['is_logged_in']) {
            $cart_total_response = get_cart_total($this->data['user']->id);
            if (!isset($cart_total_response[0]['total_items'])) {
                $this->response['error'] = true;
                $this->response['message'] = $this->lang->line('cart_already_empty');
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return;
            }

            $data = array(
                'user_id' => $this->data['user']->id,
            );
            if ($this->cart_model->remove_from_cart($data)) {
                $cart_total_response = get_cart_total($data['user_id']);
                $this->response['error'] = false;
                $this->response['message'] = $this->lang->line('product_clear_msg');
                if (!empty($cart_total_response) && isset($cart_total_response)) {
                    $this->response['data'] = [
                        'total_quantity' => strval($cart_total_response['quantity']),
                        'sub_total' => strval($cart_total_response['sub_total']),
                        'total_items' => (isset($cart_total_response[0]['total_items'])) ? strval($cart_total_response[0]['total_items']) : "0",
                        'max_items_cart' => $this->data['settings']['max_items_cart']
                    ];
                } else {
                    $this->response['data'] = [];
                }
                print_r(json_encode($this->response));
                return false;
            } else {
                $this->response['error'] = true;
                $this->response['message'] = $this->lang->line('item_notremove_msg');
                echo json_encode($this->response);
                return false;
            }
        } else {
            $this->response['error'] = true;
            $this->response['message'] = $this->lang->line('cart_login_msg');
            echo json_encode($this->response);
            return false;
        }
    }

    public function get_user_cart()
    {
        if ($this->session->userdata('cartsession')!='') {
            $cart_user_data = $this->cart_model->get_user_cart($this->session->userdata('cartsession'));
            $cart_total_response = get_cart_total($_SESSION['cartsession']);
            $tmp_cart_user_data = $cart_user_data;

            if (!empty($tmp_cart_user_data)) {
                for ($i = 0; $i < count($tmp_cart_user_data); $i++) {

                    $product_data = fetch_details(['id' => $tmp_cart_user_data[$i]['product_variant_id']], 'product_variants', 'product_id,availability');
                    $pro_details = fetch_product($this->session->userdata('cartsession'), null, $product_data[0]['product_id']);
                    if (!empty($pro_details['product'])) {

                        if (trim($pro_details['product'][0]['availability']) == 0 && $pro_details['product'][0]['availability'] != null) {
                            unset($cart_user_data[$i]);
                            continue;
                        }
                        if (!empty($pro_details['product'])) {
                            $cart_user_data[$i]['product_details'] = $pro_details['product'];
                        } else {
                            unset($cart_user_data[$i]);
                            continue;
                        }
                    } else {
                        unset($cart_user_data[$i]);
                        continue;
                    }
                }
            }
            if (empty($cart_user_data)) {
                $this->response['error'] = true;
                $this->response['message'] = $this->lang->line('empty_cart_message');
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return;
            }
            $this->response['error'] = false;
            $this->response['message'] =  $this->lang->line('product_retrive_cart_msg');
            $this->response['total_quantity'] = $cart_total_response['quantity'];
            $this->response['sub_total'] = $cart_total_response['sub_total'];
            $this->response['delivery_charge'] = $this->data['settings']['delivery_charge'];
            $this->response['tax_percentage'] = (isset($cart_total_response['tax_percentage'])) ? $cart_total_response['tax_percentage'] : "0";
            $this->response['tax_amount'] = (isset($cart_total_response['tax_amount'])) ? $cart_total_response['tax_amount'] : "0";
            $this->response['total_arr'] =  $cart_total_response['total_arr'];
            $this->response['variant_id'] =  $cart_total_response['variant_id'];
            $this->response['data'] = array_values($cart_user_data);
            print_r($this->response);
            return;
        } 
    }
    public function checkout()
    {
       // if ($this->data['is_logged_in']) {
           // $cart = $this->cart_model->get_user_cart($this->data['user']->id);
            //if (empty($cart)) {
                //redirect(base_url());
            //}
			$cart = $this->cart_model->get_user_cart($this->session->userdata('cartsession'));
			if (empty($cart)) {
                redirect(base_url());
            }
            $this->data['time_slot_config'] = get_settings('time_slot_config', true);
            $payment_methods = get_settings('payment_method', true);
            $this->data['main_page'] = 'checkout';
            $this->data['title'] = 'Checkout | ' . $this->data['web_settings']['site_title'];
            $this->data['keywords'] = 'Checkout, ' . $this->data['web_settings']['meta_keywords'];
            $this->data['description'] = 'Checkout | ' . $this->data['web_settings']['meta_description'];
            $this->data['cart'] = get_cart_total($_SESSION['cartsession']);
            $this->data['payment_methods'] = get_settings('payment_method', true);
            $this->data['time_slots'] = fetch_details('status=1', 'time_slots', '*');
            $this->data['cities'] =get_cities();
            $this->data['areas'] =get_areaslist();
            if($this->data['user'] && isset($this->data['user']->id))
                $this->data['default_address'] = $this->address_model->get_address($this->data['user']->id, NULL, NULL, false);
            else
            $this->data['default_address'] = '';
            $this->data['payment_methods'] = $payment_methods;

            $this->load->view('front-end/' . THEME . '/template', $this->data);
        //} else {
           // redirect(base_url());
        //}
    }

    public function place_order()
    {
        if ($this->data['is_logged_in']) {
            /*
            mobile:9974692496
            product_variant_id: 1,2,3
            quantity: 3,3,1
            latitude:40.1451
            longitude:-45.4545
            promo_code:NEW20 {optional}
            payment_method: Paypal / Payumoney / COD / PAYTM
            address_id:17
            delivery_date:10/12/2012
            delivery_time:Today - Evening (4:00pm to 7:00pm)
            is_wallet_used:1 {By default 0}
            wallet_balance_used:1
            active_status:awaiting {optional}
      
          */
            // total:60.0
            // delivery_charge:20.0
            // tax_amount:10
            // tax_percentage:10
            // final_total:55
            //$this->form_validation->set_rules('mobile', 'Mobile Id', 'trim|required|numeric|xss_clean');
            // $this->form_validation->set_rules('product_variant_id', $this->lang->line('product_variant'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('product_variant'),$this->lang->line('reqired_field_validation'))));
            // $this->form_validation->set_rules('quantity', $this->lang->line('quantity'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('quantity'),$this->lang->line('reqired_field_validation'))));
            // $this->form_validation->set_rules('final_total', 'Final Total', 'trim|required|numeric|xss_clean');
            $this->form_validation->set_rules('promo_code',$this->lang->line('promo_code'), 'trim|xss_clean',array('required' => str_replace('{field}',$this->lang->line('quantity'),$this->lang->line('reqired_field_validation'))));

            /*
            ------------------------------
            If Wallet Balance Is Used
            ------------------------------
            */
            // $this->form_validation->set_rules('latitude', 'Latitude', 'trim|numeric|xss_clean');
            // $this->form_validation->set_rules('longitude', 'Longitude', 'trim|numeric|xss_clean');
            $this->form_validation->set_rules('payment_method', $this->lang->line('payment_method'), 'trim|required|xss_clean|in_list[COD,Stripe,knet]',array('required' => str_replace('{field}',$this->lang->line('payment_method'),$this->lang->line('reqired_field_validation'))));
            // $this->form_validation->set_rules('delivery_date', 'Delivery Date', 'trim|xss_clean');
            // $this->form_validation->set_rules('delivery_time', 'Delivery time', 'trim|xss_clean');
            $this->form_validation->set_rules('address_id', $this->lang->line('address'), 'trim|required|numeric|xss_clean',array('required' => str_replace('{field}',$this->lang->line('address'),$this->lang->line('reqired_field_validation'))));

            // if ($_POST['payment_method'] == "Razorpay") {
            //     $this->form_validation->set_rules('razorpay_order_id', 'Razorpay Order ID', 'trim|required|xss_clean');
            //     $this->form_validation->set_rules('razorpay_payment_id', 'Razorpay Payment ID', 'trim|required|xss_clean');
            //     $this->form_validation->set_rules('razorpay_signature', 'Razorpay Signature', 'trim|required|xss_clean');
            // } else if ($_POST['payment_method'] == "Paystack") {
            //     $this->form_validation->set_rules('paystack_reference', 'Paystack Reference', 'trim|required|xss_clean');
            // }

            $_POST['user_id'] = $this->data['user']->id;
            $_POST['is_wallet_used'] = 0;
            $data = array();
            if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = strip_tags(validation_errors());
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return;
            } else {
               

                $cart = get_cart_total($_SESSION['cartsession']);
                if (empty($cart)) {
                    $this->response['error'] = true;
                    $this->response['message'] =$this->lang->line('empty_cart_message');
                    $this->response['data'] = array();
                    print_r(json_encode($this->response));
                    return false;
                }
                $product_variant_id = array_column($cart, 'id');
                $quantity = array_column($cart, 'qty');
                $_POST['product_variant_id'] = implode(',',$product_variant_id) ;
                $_POST['quantity'] =implode(',',$quantity) ;
                $check_current_stock_status = validate_stock($product_variant_id, $quantity);
                if($check_current_stock_status['error']){
                    $this->response['error'] = true;
                    $this->response['message'] = $check_current_stock_status['message'];
                    $this->response['data'] = array();
                    print_r(json_encode($this->response));
                    return false;
                }
                $_POST['final_total'] = $cart['overall_amount'];
                // if ($_POST['payment_method'] == "Razorpay") {
                //     if (!$this->razorpay->verify_payment($_POST['razorpay_order_id'], $_POST['razorpay_payment_id'], $_POST['razorpay_signature'])) {
                //         $this->response['error'] = true;
                //         $this->response['message'] = "Invalid Razorpay Payment Transaction.";
                //         $this->response['data'] = array();
                //         print_r(json_encode($this->response));
                //         return false;
                //     }
                //     $data['status'] = "success";
                //     $data['txn_id'] = $_POST['razorpay_payment_id'];
                //     $data['message'] = $this->lang->line('order_place_success');
                // } elseif ($_POST['payment_method'] == "Paystack") {
                //     $transfer = $this->paystack->verify_transation($_POST['paystack_reference']);
                //     $transfer = json_decode($transfer, true);
                //     if (isset($transfer['status']) && $transfer['status']) {
                //         if (isset($transfer['data']['status']) && $transfer['data']['status'] != "success") {
                //             $this->response['error'] = true;
                //             $this->response['message'] = "Invalid Paystack Transaction.";
                //             $this->response['data'] = array();
                //             print_r(json_encode($this->response));
                //             return false;
                //         }
                //     } else {
                //         $this->response['error'] = true;
                //         $this->response['message'] = "Error While Fetching the Order Details.Contact Admin ASAP.";
                //         $this->response['data'] = $transfer;
                //         print_r(json_encode($this->response));
                //         return false;
                //     }
                //     $data['status'] = "success";
                // } elseif ($_POST['payment_method'] == "Stripe") {
                //     $_POST['active_status'] = "awaiting";
                //     $data['status'] = "success";
                //     $data['txn_id'] = $_POST['stripe_payment_id'];
                //     $data['message'] = $this->lang->line('order_place_success');
                // } elseif ($_POST['payment_method'] == "Paypal") {
                //     $_POST['active_status'] = "awaiting";
                //     $data['status'] = "success";
                //     $data['txn_id'] = null;
                //     $data['message'] = null;
                // }
                if ($_POST['payment_method'] == "COD") {
                    $_POST['active_status'] = "received";
                }
                elseif ($_POST['payment_method'] == "knet") {
                    $_POST['active_status'] = "awaiting";
                    $data['status'] = "success";
                    $data['txn_id'] = null;
                    $data['message'] = null; 
                }
                
                $res = $this->order_model->place_order($_POST);
               // print_r($res['error']);
                $data['order_id'] = $res['order_id'];
                $data['user_id'] = $_POST['user_id'];
                $data['type'] = $_POST['payment_method'];
                $data['amount'] = $cart['overall_amount'];
                if ($_POST['payment_method'] != "COD" && strtolower($_POST['payment_method']) != "knet"  && $_POST['payment_method'] != "Paypal") {
                    $this->transaction_model->add_transaction($data);
                }
                $this->response['error'] = false;
                $this->response['message'] = $this->lang->line('order_place_success');
                $this->response['data'] = $res;
                $this->response['order_id'] =  $res['order_id'];
                print_r(json_encode($this->response));
                return false;
            }
        } else {
			
        //    $this->form_validation->set_rules('product_variant_id', $this->lang->line('product_variant'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('product_variant'),$this->lang->line('reqired_field_validation'))));
        //     $this->form_validation->set_rules('quantity', $this->lang->line('quantity'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('quantity'),$this->lang->line('reqired_field_validation'))));
            // $this->form_validation->set_rules('final_total', 'Final Total', 'trim|required|numeric|xss_clean');
            $this->form_validation->set_rules('promo_code', $this->lang->line('promo_code'), 'trim|xss_clean',array('required' => str_replace('{field}',$this->lang->line('promo_code'),$this->lang->line('reqired_field_validation'))));

            /*
        ------------------------------
        If Wallet Balance Is Used
        ------------------------------
        */
            // $this->form_validation->set_rules('latitude', 'Latitude', 'trim|numeric|xss_clean');
            // $this->form_validation->set_rules('longitude', 'Longitude', 'trim|numeric|xss_clean');
            $this->form_validation->set_rules('payment_method',  $this->lang->line('payment_method'), 'trim|required|xss_clean|in_list[knet,stripe,COD]',array('required' => str_replace('{field}',$this->lang->line('payment_method'),$this->lang->line('reqired_field_validation'))));
            // $this->form_validation->set_rules('delivery_date', 'Delivery Date', 'trim|xss_clean');
            // $this->form_validation->set_rules('delivery_time', 'Delivery time', 'trim|xss_clean');
            //$this->form_validation->set_rules('address_id', 'Address id', 'trim|required|numeric|xss_clean');
$this->form_validation->set_rules('first_name', $this->lang->line('first_name'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('first_name'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('last_name', $this->lang->line('last_name'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('last_name'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('mobile', $this->lang->line('mobile'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('reqired_field_validation'))));

    $account_creation =0;
    $this->form_validation->set_rules('email', $this->lang->line('email'), 'trim|xss_clean|valid_email',array('required'=>$this->lang->line('reqired_field_validation'),'valid_email' => str_replace('{field}',$this->lang->line('email'),$this->lang->line('valid_email_validation'))));

$this->form_validation->set_rules('area_id', $this->lang->line('area'), 'trim|xss_clean',array('required' => str_replace('{field}',$this->lang->line('area'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('street', $this->lang->line('street'), 'trim|xss_clean',array('required' => str_replace('{field}',$this->lang->line('street'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('block', $this->lang->line('block'), 'trim|xss_clean',array('required' => str_replace('{field}',$this->lang->line('block'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('avenue', $this->lang->line('avenue'), 'trim|xss_clean',array('required' => str_replace('{field}',$this->lang->line('avenue'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('house',  $this->lang->line('house_no'), 'trim|xss_clean',array('required' => str_replace('{field}',$this->lang->line('house_no'),$this->lang->line('reqired_field_validation'))));
// $this->form_validation->set_rules('type', $this->lang->line('type'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('type'),$this->lang->line('reqired_field_validation'))));
// $this->form_validation->set_rules('terms_and_conditions', $this->lang->line('terms_and_condition'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('terms_and_condition'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('payment_method', $this->lang->line('payment_method'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('payment_method'),$this->lang->line('reqired_field_validation'))));
if(isset($_POST['billingaddress']) && $_POST['billingaddress']==1)
{
    $invoice_address =1;
$this->form_validation->set_rules('bfname', $this->lang->line('first_name'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('first_name'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('blname', $this->lang->line('last_name'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('last_name'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('bmobile', $this->lang->line('mobile'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('bemail', $this->lang->line('email'), 'trim|xss_clean|valid_email',array('required'=>$this->lang->line('reqired_field_validation'),'valid_email' => str_replace('{field}',$this->lang->line('email'),$this->lang->line('valid_email_validation'))));
$this->form_validation->set_rules('barea_id', $this->lang->line('area'), 'trim|xss_clean',array('required' => str_replace('{field}',$this->lang->line('area'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('bstreet', $this->lang->line('street'), 'trim|xss_clean',array('required' => str_replace('{field}',$this->lang->line('street'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('bblock',  $this->lang->line('block'), 'trim|xss_clean',array('required' => str_replace('{field}',$this->lang->line('block'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('bavenue', $this->lang->line('avenue'), 'trim|xss_clean',array('required' => str_replace('{field}',$this->lang->line('avenue'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('bhouse',  $this->lang->line('house_no'), 'trim|xss_clean',array('required' => str_replace('{field}',$this->lang->line('house_no'),$this->lang->line('reqired_field_validation'))));
}
else{
    $invoice_address =0;
}
            // if ($_POST['payment_method'] == "Razorpay") {
            //     $this->form_validation->set_rules('razorpay_order_id', 'Razorpay Order ID', 'trim|required|xss_clean');
            //     $this->form_validation->set_rules('razorpay_payment_id', 'Razorpay Payment ID', 'trim|required|xss_clean');
            //     $this->form_validation->set_rules('razorpay_signature', 'Razorpay Signature', 'trim|required|xss_clean');
            // } else if ($_POST['payment_method'] == "Paystack") {
            //     $this->form_validation->set_rules('paystack_reference', 'Paystack Reference', 'trim|required|xss_clean');
            // }

            $_POST['billingaddress'] =$invoice_address;
           // $_POST['account'] = $account_creation;
            $_POST['is_wallet_used'] = 0;
            $data = array();
            if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = strip_tags(validation_errors());
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return;
            } else {
				
		// 		if($_POST['account']==1)
		// 		{				
		// 		    $password1 = $this->input->post('password');                				
        //             $identity_column = $this->config->item('identity', 'ion_auth');
        //             $email = strtolower($this->input->post('email'));
        //             $mobile = $this->input->post('mobile');
        //             $identity = ($identity_column == 'mobile') ? $mobile : $email;          
        //             $password=$password1;
        //         $data = [			
        //             'password' => $this->ion_auth_model->hash_password($password,$email),
        //             'email' => $email,
        //             'ip_address' => $ip_address,
        //             'created_on' => time(),
        //             'active' => ($_POST['account']==1) ? 1 : 0
		//         ];

        //     $additional_data = [
        //         'first_name' => $this->input->post('first_name'),
        //         'last_name' => $this->input->post('last_name'),
        //         'username' => $this->input->post('email'),
        //         'mobile' => $this->input->post('mobile'),
        //         'area' => $this->input->post('area_id'),               
        //         'street' => $this->input->post('street'),                
        //     ];
        //     $user_data = array_merge( $additional_data, $data);
        //     //$userid = $this->ion_auth->register($identity, $password, $email, $additional_data, ['2'],'guest');
		// 	$this->db->insert('users', $user_data);		
        //     $userid = $this->db->insert_id();
        //     $email_data = array(
        //         'first_name' => $this->input->post('first_name'),
        //         'last_name' => $this->input->post('last_name'),
        //         'username' => $this->input->post('email'),
        //         'email' => $this->input->post('email'),
        //         'password' => $password
        //     );
        //     $mail_response = send_mail($this->input->post('email'), 'Registration Confirmation', $this->load->view('admin/pages/view/registration_confirmation.php', $email_data, TRUE));
        //    }
        //else{
            $userid = 0;
         //}
		    //$id = $this->db->insert_id('users' . '_id_seq');
			$_POST['user_id'] =  $userid;
			$arr['first_name'] = $this->input->post('first_name');
            $arr['last_name'] = $this->input->post('last_name');
			$arr['mobile'] = $this->input->post('mobile');
			$arr['type'] = $this->input->post('type');
			$arr['area_id'] = $this->input->post('area_id');
			$arr['street'] = $this->input->post('street');
			$arr['block'] = $this->input->post('block');
			$arr['avenue'] = $this->input->post('avenue');
			$arr['house'] = $this->input->post('house');
            $arr['country'] = $this->input->post('country_id');
            $arr['alternate_number'] = $this->input->post('alternate_number');
            $arr['user_id'] = $userid;
           
            $address_id= 0;
			$_POST['address_id']=$address_id;
			$arr1=array();
            if($_POST['billingaddress']==1){
                $arr1['first_name'] = $this->input->post('bfname');
                $arr1['last_name'] = $this->input->post('blname');
                $arr1['mobile'] = $this->input->post('bmobile');			
                $arr1['area_id'] = $this->input->post('barea_id');
                $arr1['street'] = $this->input->post('bstreet');
                $arr1['block'] = $this->input->post('bblock');
                $arr1['avenue'] = $this->input->post('bavenue');
                $arr1['house'] = $this->input->post('bhouse');
                $arr['country'] = $this->input->post('bcountry_id');
                $arr['alternate_number'] = $this->input->post('balternate_number');
                $arr1['user_id'] = $userid;
              
                $baddress_id= 0;
            }
            else
                $baddress_id=0;
			    $_POST['baddress_id']=$baddress_id;               

                $cart = get_cart_total($_SESSION['cartsession']);
                if (empty($cart)) {
                    $this->response['error'] = true;
                    $this->response['message'] =$this->lang->line('empty_cart_message');
                    $this->response['data'] = array();
                    print_r(json_encode($this->response));
                    return false;
                }
                $product_variant_id = array_column($cart, 'id');
                $quantity =array_column($cart, 'qty');
                $_POST['product_variant_id'] =$product_variant_id ;
                $_POST['quantity'] =$quantity ;
                $_POST['product_variant_id'] = implode(',',$product_variant_id) ;
                $_POST['quantity'] =implode(',',$quantity) ;
                $check_current_stock_status = validate_stock($product_variant_id, $quantity);
                if($check_current_stock_status['error']){
                    $this->response['error'] = true;
                    $this->response['message'] = $check_current_stock_status['message'];
                    $this->response['data'] = array();
                    print_r(json_encode($this->response));
                    return false;
                }
                $_POST['final_total'] = $cart['overall_amount'];
                // if ($_POST['payment_method'] == "Razorpay") {
                //     if (!$this->razorpay->verify_payment($_POST['razorpay_order_id'], $_POST['razorpay_payment_id'], $_POST['razorpay_signature'])) {
                //         $this->response['error'] = true;
                //         $this->response['message'] = "Invalid Razorpay Payment Transaction.";
                //         $this->response['data'] = array();
                //         print_r(json_encode($this->response));
                //         return false;
                //     }
                //     $data['status'] = "success";
                //     $data['txn_id'] = $_POST['razorpay_payment_id'];
                //     $data['message'] = "Order Placed Successfully";
                // } elseif ($_POST['payment_method'] == "Paystack") {
                //     $transfer = $this->paystack->verify_transation($_POST['paystack_reference']);
                //     $transfer = json_decode($transfer, true);
                //     if (isset($transfer['status']) && $transfer['status']) {
                //         if (isset($transfer['data']['status']) && $transfer['data']['status'] != "success") {
                //             $this->response['error'] = true;
                //             $this->response['message'] = "Invalid Paystack Transaction.";
                //             $this->response['data'] = array();
                //             print_r(json_encode($this->response));
                //             return false;
                //         }
                //     } else {
                //         $this->response['error'] = true;
                //         $this->response['message'] = "Error While Fetching the Order Details.Contact Admin ASAP.";
                //         $this->response['data'] = $transfer;
                //         print_r(json_encode($this->response));
                //         return false;
                //     }
                //     $data['status'] = "success";
                // } elseif ($_POST['payment_method'] == "Stripe") {
                //     $_POST['active_status'] = "awaiting";
                //     $data['status'] = "success";
                //     $data['txn_id'] = $_POST['stripe_payment_id'];
                //     $data['message'] = "Order Placed Successfully";
                // } elseif ($_POST['payment_method'] == "Paypal") {
                //     $_POST['active_status'] = "awaiting";
                //     $data['status'] = "success";
                //     $data['txn_id'] = null;
                //     $data['message'] = null;
                // } 
                if ($_POST['payment_method'] == "COD") {
                    $_POST['active_status'] = "received";
                }
                elseif ($_POST['payment_method'] == "knet") {
                    $_POST['active_status'] = "awaiting";
                    $data['status'] = "success";
                    $data['txn_id'] = null;
                    $data['message'] = null;
                }
                //$_POST['user_id']= $this->session->userdata('cartsession');
                $res = $this->order_model->place_order($_POST);
                if($userid <=0)
                    $data['user_id'] =$this->session->userdata('cartsession');
                else
                    $data['user_id'] =$userid;
                $data['order_id'] = $res['order_id'];
                //$data['user_id'] =$userid;
                $data['type'] = $_POST['payment_method'];
                $data['amount'] = $cart['overall_amount'];
                if ($_POST['payment_method'] != "COD" && $_POST['payment_method'] != "Paypal" && strtolower($_POST['payment_method']) != "knet") {
                    $this->transaction_model->add_transaction($data);
                }
                $this->response['error'] = false;
                $this->response['message'] =$this->lang->line('order_place_success'); 
                $this->response['data'] = $res;
                $this->response['order_id'] =  $res['order_id'];
                print_r(json_encode($this->response));
                return false;
            }
        }
		
		
		
    }

    public function validate_promo_code()
    {
        if ($this->data['is_logged_in']) {
            /*
            promo_code:'NEWOFF10'
            user_id:28
            final_total:'300'

            */
            $this->form_validation->set_rules('promo_code', 'Promo Code', 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('quantity'),$this->lang->line('reqired_field_validation'))));
            if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return false;
            } else {
                $cart = get_cart_total($this->session->userdata('cartsession'));
                $validate = validate_promo_code($_POST['promo_code'], $this->data['user']->id, $cart['amount_inclusive_tax']);

                $this->response['error'] = $validate['error'];
                $this->response['message'] = $validate['message'];
                $this->response['data'] = $validate['data'];
                print_r(json_encode($this->response));
                return false;
            }
        } else {
            $this->response['error'] = true;
            $this->response['message'] = 'Please login first for apply coupon';
            print_r(json_encode($this->response));
            return false;
        }
    }

    public function pre_payment_setup()
    {
        if ($this->data['is_logged_in']) {
            $_POST['user_id'] = $this->data['user']->id;
            $cart = get_cart_total($this->data['user']->id);
            if ($_POST['payment_method'] == "Razorpay") {
                $order = $this->razorpay->create_order(($cart['overall_amount'] * 100));
                if (!isset($order['error'])) {
                    $this->response['order_id'] = $order['id'];
                    $this->response['error'] = false;
                    $this->response['message'] = "Client Secret Get Successfully.";
                    print_r(json_encode($this->response));
                    return false;
                } else {
                    $this->response['error'] = true;
                    $this->response['message'] = $order['error']['description'];
                    $this->response['details'] = $order;
                    print_r(json_encode($this->response));
                    return false;
                }
            } elseif ($_POST['payment_method'] == "Stripe") {
                $order = $this->stripe->create_payment_intent(array('amount' => ($cart['overall_amount'] * 100)));
                $this->response['client_secret'] = $order['client_secret'];
                $this->response['id'] = $order['id'];
            }
            $this->response['error'] = false;
            $this->response['message'] = "Client Secret Get Successfully.";
            print_r(json_encode($this->response));
            return false;
        } else {
            $this->response['error'] = false;
            $this->response['message'] = "Unauthorised access is not allowed.";
            print_r(json_encode($this->response));
            return false;
        }
    }
}
