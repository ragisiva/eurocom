<?php
defined('BASEPATH') or exit('No direct script access allowed');

class My_account extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(['ion_auth', 'form_validation', 'pagination']);
        $this->load->helper(['url', 'language']);
        $this->load->model(['cart_model', 'category_model', 'address_model', 'order_model','Transaction_model']);
        $this->lang->load('auth');
        $this->data['is_logged_in'] = ($this->ion_auth->logged_in()) ? 1 : 0;
        $this->data['user'] = ($this->ion_auth->logged_in()) ? $this->ion_auth->user()->row() : array();
        $this->data['settings'] = get_settings('system_settings', true);
        $this->data['web_settings'] = get_settings('web_settings', true);
        $this->response['csrfName'] = $this->security->get_csrf_token_name();
        $this->response['csrfHash'] = $this->security->get_csrf_hash();
        $cookie_lang = ($this->session->userdata('lang') !='') ? $this->session->userdata('lang', TRUE) : 'english' ;
        if($cookie_lang=='arabic'){
            $this->data['lang_prefix'] = 'ar_';
        }
        else{
            $this->data['lang_prefix'] ='';
        }
        
    }


    public function index()
    {
        if ($this->data['is_logged_in']) {
            $this->data['main_page'] = 'dashboard';
            $this->data['title'] = 'Dashboard | ' . $this->data['web_settings']['site_title'];
            $this->data['keywords'] = 'Dashboard, ' . $this->data['web_settings']['meta_keywords'];
            $this->data['description'] = 'Dashboard | ' . $this->data['web_settings']['meta_description'];
            $this->load->view('front-end/' . THEME . '/template', $this->data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    public function profile()
    {
        if ($this->ion_auth->logged_in()) {
            $identity_column = $this->config->item('identity', 'ion_auth');
            $this->data['users'] = $this->ion_auth->user()->row();
            $this->data['identity_column'] = $identity_column;
            $this->data['main_page'] = 'profile';
            $this->data['title'] = 'Profile | ' . $this->data['web_settings']['site_title'];
            $this->data['keywords'] = $this->data['web_settings']['meta_keywords'];
            $this->data['description'] = $this->data['web_settings']['meta_description'];
            $this->load->view('front-end/' . THEME . '/template', $this->data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    public function orders()
    {
        if ($this->ion_auth->logged_in()) {
            $this->data['main_page'] = 'orders';
            $this->data['title'] = 'Orders | ' . $this->data['web_settings']['site_title'];
            $this->data['keywords'] = 'Orders, ' . $this->data['web_settings']['meta_keywords'];
            $this->data['description'] = 'Orders | ' . $this->data['web_settings']['meta_description'];
            $total = fetch_orders(false, $this->data['user']->id, false, false, 1, NULL, NULL, NULL, NULL);
            $limit = 10;
            $config['base_url'] = base_url('my-account/orders');
            $config['total_rows'] = $total['total'];
            $config['per_page'] = $limit;
            $config['num_links'] = 2;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;
            $config['page_query_string'] = FALSE;
            $config['uri_segment'] = 3;
            $config['attributes'] = array('class' => 'page-link');

            $config['full_tag_open'] = '<ul class="pagination justify-content-center">';
            $config['full_tag_close'] = '</ul>';

            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_link'] = 'First';
            $config['first_tag_close'] = '</li>';

            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_link'] = 'Last';
            $config['last_tag_close'] = '</li>';

            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_link'] = '<i class="fa fa-arrow-left"></i>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_link'] = '<i class="fa fa-arrow-right"></i>';
            $config['next_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';

            $page_no = (empty($this->uri->segment(4))) ? 1 : $this->uri->segment(4);
            if (!is_numeric($page_no)) {
                redirect(base_url('my-account/orders'));
            }
            $offset = ($page_no - 1) * $limit;
            $this->pagination->initialize($config);
            $this->data['links'] =  $this->pagination->create_links();
            $this->data['orders'] = fetch_orders(false, $this->data['user']->id, false, false, $limit, $offset, 'date_added', 'DESC', NULL);
            $this->load->view('front-end/' . THEME . '/template', $this->data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    public function order_details()
    {
        if ($this->ion_auth->logged_in()) {
            $this->data['main_page'] = 'order-details';
            $this->data['title'] = 'Orders | ' . $this->data['web_settings']['site_title'];
            $this->data['keywords'] = 'Orders, ' . $this->data['web_settings']['meta_keywords'];
            $this->data['description'] = 'Orders | ' . $this->data['web_settings']['meta_description'];
            $order_id = $this->uri->segment(3);
            $order = fetch_orders($order_id, $this->data['user']->id, false, false, 1, NULL, NULL, NULL, NULL);
            if (!isset($order['order_data']) || empty($order['order_data'])) {
                redirect(base_url('my-account/orders'));
            }
            $this->data['order'] = $order['order_data'][0];
            $this->load->view('front-end/' . THEME . '/template', $this->data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    public function order_invoice($order_id)
    {
        if ($this->ion_auth->logged_in()) {
            $this->data['main_page'] = VIEW . 'api-order-invoice';
            $settings = get_settings('system_settings', true);
            $this->data['title'] = 'Invoice Management |' . $settings['app_name'];
            $this->data['meta_description'] = 'Invoice Management | ' . $this->data['web_settings']['meta_description'];;
            if (isset($order_id) && !empty($order_id)) {
                $res = $this->order_model->get_order_details(['o.id' => $order_id], true);
                if (!empty($res)) {
                    $items = [];
                    $promo_code = [];
                    if (!empty($res[0]['promo_code'])) {
                        $promo_code = fetch_details(['promo_code' => trim($res[0]['promo_code'])], 'promo_codes');
                    }
                    foreach ($res as $row) {
                        $row = output_escaping($row);
                        $temp['product_id'] = $row['product_id'];
                        $temp['product_variant_id'] = $row['product_variant_id'];
                        $temp['pname'] = $row['pname'];
                        $temp['variant_name'] = $row['variant_name'];
                        $temp['quantity'] = $row['quantity'];
                        $temp['discounted_price'] = $row['discounted_price'];
                        $temp['tax_percent'] = $row['tax_percent'];
                        $temp['tax_amount'] = $row['tax_amount'];
                        $temp['price'] = $row['price'];
                        $temp['special_price'] = $row['special_price'];
                        $temp['delivery_boy'] = $row['delivery_boy'];
                        $temp['active_status'] = $row['oi_active_status'];
                        array_push($items, $temp);
                    }
                    $this->data['order_detls'] = $res;
                   
                    $order_details =  fetch_orders($order_id, $this->data['user']->id, false, false, 1, NULL, NULL, NULL, NULL);
                    $this->data['order_detls'] =  $order_details['order_data'];
                    $this->data['items'] = $items;
                    $this->data['promo_code'] = $promo_code;
                    $this->data['print_btn_enabled'] = true;
                    $this->data['settings'] = get_settings('system_settings', true);
                    $this->load->view('admin/invoice-template', $this->data);
                } else {
                    redirect(base_url(), 'refresh');
                }
            } else {
                redirect(base_url(), 'refresh');
            }
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    public function update_order_item_status()
    {
        $this->form_validation->set_rules('order_item_id', 'Order item id', 'trim|required|numeric|xss_clean');
        $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean|in_list[cancelled,returned]');
        if (!$this->form_validation->run()) {
            $this->response['error'] = true;
            $this->response['message'] = strip_tags(validation_errors());
            $this->response['data'] = array();
        } else {
            $this->response = $this->order_model->update_order_item($_POST['order_item_id'], trim($_POST['status']));
        }
        print_r(json_encode($this->response));
    }

    public function update_order()
    {
        $this->form_validation->set_rules('order_id', 'Order id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean|in_list[cancelled,returned]');
        if (!$this->form_validation->run()) {
            $this->response['error'] = true;
            $this->response['message'] = validation_errors();
            $this->response['data'] = array();
            print_r(json_encode($this->response));
            return false;
        } else {
            $res = validate_order_status($_POST['order_id'], $_POST['status'], 'orders');
            if ($res['error']) {
                $this->response['error'] = (isset($res['return_request_flag'])) ? false : true;
                $this->response['message'] = $res['message'];
                $this->response['data'] = $res['data'];
                print_r(json_encode($this->response));
                return false;
            }
            if ($this->order_model->update_order(['status' => $_POST['status']], ['id' => $_POST['order_id']], true)) {
                $this->order_model->update_order(['active_status' => $_POST['status']], ['id' => $_POST['order_id']], false);
                if ($this->order_model->update_order(['status' => $_POST['status']], ['order_id' => $_POST['order_id']], true, 'order_items')) {
                    $this->order_model->update_order(['active_status' => $_POST['status']], ['order_id' => $_POST['order_id']], false, 'order_items');
                    process_refund($_POST['order_id'],$_POST['status'],'orders');
                    $this->response['error'] = false;
                    $this->response['message'] = 'Order Updated Successfully';
                    $this->response['data'] = array();
                    print_r(json_encode($this->response));
                    return false;
                }
            }
        }
    }

    public function notifications()
    {
        if ($this->ion_auth->logged_in()) {
            $this->data['main_page'] = 'notifications';
            $this->data['title'] = 'Notification | ' . $this->data['web_settings']['site_title'];
            $this->data['keywords'] = 'Notification, ' . $this->data['web_settings']['meta_keywords'];
            $this->data['description'] = 'Notification | ' . $this->data['web_settings']['meta_description'];
            $this->load->view('front-end/' . THEME . '/template', $this->data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    public function manage_address()
    {
        if ($this->ion_auth->logged_in()) {
            $this->data['main_page'] = 'address';
            $this->data['title'] = 'Address | ' . $this->data['web_settings']['site_title'];
            $this->data['keywords'] = 'Address, ' . $this->data['web_settings']['meta_keywords'];
            $this->data['description'] = 'Address | ' . $this->data['web_settings']['meta_description'];
            $this->data['cities'] =get_areaslist();
            $user_id = $this->ion_auth->user()->row()->id;
            $res = $this->address_model->get_address($user_id);
            $is_default_counter = array_count_values(array_column($res, 'is_default'));

            if (!isset($is_default_counter['1']) && !empty($res)) {
                update_details(['is_default' => '1'], ['id' => $res[0]['id']], 'addresses');
                $res = $this->address_model->get_address($this->data['user']->id);
            }
            $this->data['addresses'] =  $res;
            $this->load->view('front-end/' . THEME . '/template', $this->data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    public function wallet()
    {
        if ($this->ion_auth->logged_in()) {
            $this->data['main_page'] = 'wallet';
            $this->data['title'] = 'Wallet | ' . $this->data['web_settings']['site_title'];
            $this->data['keywords'] = 'Wallet, ' . $this->data['web_settings']['meta_keywords'];
            $this->data['description'] = 'Wallet | ' . $this->data['web_settings']['meta_description'];
            $this->load->view('front-end/' . THEME . '/template', $this->data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    public function transactions()
    {
        if ($this->ion_auth->logged_in()) {
            $this->data['main_page'] = 'transactions';
            $this->data['title'] = 'Transactions | ' . $this->data['web_settings']['site_title'];
            $this->data['keywords'] = 'Transactions, ' . $this->data['web_settings']['meta_keywords'];
            $this->data['description'] = 'Transactions | ' . $this->data['web_settings']['meta_description'];
            $this->load->view('front-end/' . THEME . '/template', $this->data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }
    public function add_new_address($address_id='')
    {
        if ($this->ion_auth->logged_in()) {
            $this->data['main_page'] = 'add-new-address';
            $this->data['title'] = 'Address | ' . $this->data['web_settings']['site_title'];
            $this->data['keywords'] = 'Address, ' . $this->data['web_settings']['meta_keywords'];
            $this->data['description'] = 'Address | ' . $this->data['web_settings']['meta_description'];
            $this->data['cities'] =get_cities();
            $this->data['areas'] =get_areaslist();
            if($address_id !=''){
                $address =$this->data['address'] = $this->address_model->get_address($this->data['user']->id,$address_id);
                $this->data['id'] = $address[0]['id'];
                $this->data['first_name'] = $address[0]['first_name'];
                $this->data['last_name'] = $address[0]['last_name'];
                $this->data['mobile'] = $address[0]['mobile'];
                $this->data['email'] = $address[0]['email'];
                $this->data['street'] = $address[0]['street'];
                $this->data['block'] = $address[0]['block'];
                $this->data['area_id'] = $address[0]['area_id'];
                $this->data['avenue'] = $address[0]['avenue'];
                $this->data['house'] = $address[0]['house'];
                $this->data['type'] = $address[0]['type'];
                $this->data['is_default'] = $address[0]['is_default'];
            }
            else{
                $this->data['id'] = '';
                $this->data['first_name'] = '';
                $this->data['last_name'] = '';
                $this->data['mobile'] = '';
                $this->data['email'] = '';
                $this->data['street'] = '';
                $this->data['block'] = '';
                $this->data['area_id'] = '';
                $this->data['avenue'] = '';
                $this->data['house'] = '';
                $this->data['type'] = '';
                $this->data['is_default'] = '';
            }
            $this->load->view('front-end/' . THEME . '/template', $this->data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }
    public function add_address() 
    {
        if ($this->ion_auth->logged_in()) {
            $this->form_validation->set_rules('type', 'Type', 'trim|xss_clean');
            
            $this->form_validation->set_rules('first_name', $this->lang->line('first_name'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('first_name'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('last_name', $this->lang->line('last_name'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('last_name'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('mobile', $this->lang->line('mobile'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('reqired_field_validation')),'numeric' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('numeric_field_validation'))));
            $this->form_validation->set_rules('email', $this->lang->line('email'), 'trim|required|xss_clean|valid_email',array('required'=>$this->lang->line('reqired_field_validation'),'valid_email' => str_replace('{field}',$this->lang->line('email'),$this->lang->line('valid_email_validation'))));
            //$this->form_validation->set_rules('city', 'City', 'trim|xss_clean|required');       
            
            $this->form_validation->set_rules('area_id', $this->lang->line('area'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('area'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('street', $this->lang->line('street'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('street'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('block', $this->lang->line('block'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('block'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('avenue', $this->lang->line('avenue'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('avenue'),$this->lang->line('reqired_field_validation'))));
$this->form_validation->set_rules('house',  $this->lang->line('house_no'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('house_no'),$this->lang->line('reqired_field_validation'))));
            

            if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return false;
            }

            $arr = $this->input->post(null, true);
            $arr['user_id'] = $this->data['user']->id;
            $this->address_model->set_address($arr);
            $res = $this->address_model->get_address($this->data['user']->id, false, true);
            $this->response['error'] = false;
            $this->response['message'] =$this->lang->line('address_added_success') ;
            $this->response['data'] = $res;
            print_r(json_encode($this->response));
            return false;
        } else {
            $this->response['error'] = true;
            $this->response['message'] = $this->lang->line('access_not_allowed');
            print_r(json_encode($this->response));
            return false;
        }
    }
    public function checkout_address_change() 
    {
        if ($this->ion_auth->logged_in()) {
            $this->form_validation->set_rules('type', 'Type', 'trim|xss_clean');
            
            $this->form_validation->set_rules('billing_first_name', $this->lang->line('first_name'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('first_name'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('billing_last_name', $this->lang->line('last_name'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('last_name'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('billing_mobile', $this->lang->line('mobile'), 'trim|required|xss_clean|numeric',array('required' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('reqired_field_validation')),'numeric' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('numeric_field_validation'))));

            $this->form_validation->set_rules('billing_alternative_mobile', $this->lang->line('mobile'), 'trim|xss_clean|numeric',array('required' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('reqired_field_validation')),'numeric' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('numeric_field_validation'))));

            
            $this->form_validation->set_rules('billing_email', $this->lang->line('email'), 'trim|required|xss_clean|valid_email',array('required'=>$this->lang->line('reqired_field_validation'),'valid_email' => str_replace('{field}',$this->lang->line('email'),$this->lang->line('valid_email_validation'))));
            //$this->form_validation->set_rules('city', 'City', 'trim|xss_clean|required');       
            
            $this->form_validation->set_rules('billing_area_id', $this->lang->line('area'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('area'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('billing_street', $this->lang->line('street'), 'trim|xss_clean');
            $this->form_validation->set_rules('billing_block', $this->lang->line('block'), 'trim|xss_clean');
            $this->form_validation->set_rules('billing_avenue', $this->lang->line('avenue'), 'trim|xss_clean');
            $this->form_validation->set_rules('billing_house',  $this->lang->line('house_no'), 'trim|xss_clean');
            $this->form_validation->set_rules('billing_country_id', $this->lang->line('country'), 'trim|xss_clean');

            if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return false;
            }

            $arr = array(
                'first_name'=>$this->input->post('billing_first_name'),
                'last_name' =>$this->input->post('billing_last_name'),
                'country'=>$this->input->post('billing_country_id'),
                'area_id'=>$this->input->post('billing_area_id'),
                'email'=>$this->input->post('billing_email'),
                'mobile'=>$this->input->post('billing_mobile'),
                'alternate_mobile' =>$this->input->post('billing_alternate_mobile'),
                'house'=>$this->input->post('billing_house'),
                'street'=>$this->input->post('billing_street'),
                'block'=>$this->input->post('billing_block'),
                'avenue'=>$this->input->post('billing_avenue'),
                'user_id'=> $this->data['user']->id,
                'id' => $this->input->post('billing_id'),
            );
           //print_r( $arr);
            $address_id = $this->address_model->set_address($arr);
            $res = $this->address_model->get_address($this->data['user']->id, $address_id, true);
            $output ='';
            if($res){
            foreach($res as $row){
            $output .=  '<div class="address-column">';
            if($row['is_default'] ==1){
                $output .= '<div class="addresstype alignleft bluebg">'.$this->lang->line('default_address') .'</div>';
               } 
               $output .='<ul>
              <li>'.$row['first_name'].' '.$row['last_name'].'</li>';
            
              $address = '';
              if($row['area_id']>0){
                  $address .=$this->lang->line('area').' : '.$row['area'].' ';
              }
              if($row['block']!=''){
                  $address .=$this->lang->line('block').' : '.$row['block'].' ';
              }
              if($row['street_no']!=''){
                  $address .=$this->lang->line('street_no').' : '.$row['street'].' ';
              }
              if($row['avenue']!=''){
                  $address .=$this->lang->line('avenue').' : '.$row['avenue'].' ';
              }
              if($row['house_no']!=''){
                  $address .=$this->lang->line('house_no').' : '.$row['house'].' ';
              }
              $city ='';
              $country_name ='';
              if($row['city_id']>0){
                  $city = $this->lang->line('city').': '.$row['city'].', ';
              }
              if($row['country']>0){
                  $country_name = $this->lang->line('country').': '.$row['country_name'];
              }
              $output .='<li class="addr-details">'.$address .'</li>
              <li class="addr-state-country">'. $city. $country_name.' </li>
              <li class="addr-phone">'.$this->lang->line('mobile') .': '.$row['mobile'].'</li>';
              if($row['alternate_mobile']){
                $output .= '<li class="addr-phone">'.$this->lang->line('alternate_mobile') .': '.$row['alternate_mobile'].'</li>';
               } 
               $output .= '<li class="addr-phone">'.$this->lang->line('email') .': '.$row['email'] .'</li>';

               $output .= ' </ul>
          <span class="buttoncontrols">
              <button data-id="'.$row['id'].'"  class="btn btn-primary btn_wdth_fit btn-billing-continue">'.$this->lang->line('continue').'</button>
          </span>';
          $output .= '  <div class="address-controls greybg textright fullwidth alignleft">        
          <span class="delete disableanchor"><a class="delete-checkout-address" data-id="'.$row['id'].'"  href="javascript:void(0)">'.$this->lang->line('delete').'</a></span><span class="edit disableanchor"><a class="edit-address"  data-first-name="'.$row['first_name'].'" data-last-name="'.$row['last_name'] .'" data-mobile="'.$row['mobile'].'" data-alternate-mobile="'. $row['alternate_mobile'].'"
          data-email="'.$row['email'].'" data-street="'.$row['street'].'"
          data-block="'.$row['block'].'" data-avenue="'.$row['avenue'].'"
          data-house="'.$row['house_no'].'" data-area="'. $row['area_id'].'" data-country="'.$row['country'] .'"
          data-type="'.$row['type'].'"  data-is-default="'.$row['is_default'].'" data-id="'.$row['id'] .'" 
          href="javascript:void(0)">'. $this->lang->line('edit') 
         .'</a></span>
      </div>
  </div>';
            }}
            $this->response['error'] = false;
            $this->response['message'] =$this->lang->line('address_added_success') ;
            $this->response['data'] = $res;
            $this->response['address'] = $output;
            $this->response['address_id'] =$address_id;
            print_r(json_encode($this->response));
            return false;
        } else {
            $this->response['error'] = true;
            $this->response['message'] = $this->lang->line('access_not_allowed');
            print_r(json_encode($this->response));
            return false;
        }
    }
    public function delivery_checkout_address_change() 
    {
        if ($this->ion_auth->logged_in()) {
            $this->form_validation->set_rules('type', 'Type', 'trim|xss_clean');
            
            $this->form_validation->set_rules('delivery_first_name', $this->lang->line('first_name'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('first_name'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('delivery_last_name', $this->lang->line('last_name'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('last_name'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('delivery_mobile', $this->lang->line('mobile'), 'trim|required|xss_clean|numeric',array('required' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('reqired_field_validation')),'numeric' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('numeric_field_validation'))));

            $this->form_validation->set_rules('delivery_alternative_mobile', $this->lang->line('mobile'), 'trim|xss_clean|numeric',array('required' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('reqired_field_validation')),'numeric' => str_replace('{field}',$this->lang->line('mobile'),$this->lang->line('numeric_field_validation'))));

            
            $this->form_validation->set_rules('delivery_email', $this->lang->line('email'), 'trim|required|xss_clean|valid_email',array('required'=>$this->lang->line('reqired_field_validation'),'valid_email' => str_replace('{field}',$this->lang->line('email'),$this->lang->line('valid_email_validation'))));
            //$this->form_validation->set_rules('city', 'City', 'trim|xss_clean|required');       
            
            $this->form_validation->set_rules('delivery_area_id', $this->lang->line('area'), 'trim|required|xss_clean',array('required' => str_replace('{field}',$this->lang->line('area'),$this->lang->line('reqired_field_validation'))));
            $this->form_validation->set_rules('delivery_street', $this->lang->line('street'), 'trim|xss_clean');
            $this->form_validation->set_rules('delivery_block', $this->lang->line('block'), 'trim|xss_clean');
            $this->form_validation->set_rules('delivery_avenue', $this->lang->line('avenue'), 'trim|xss_clean');
            $this->form_validation->set_rules('delivery_house',  $this->lang->line('house_no'), 'trim|xss_clean');
            $this->form_validation->set_rules('delivery_country_id', $this->lang->line('country'), 'trim|xss_clean');

            if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return false;
            }

            $arr = array(
                'first_name'=>$this->input->post('delivery_first_name'),
                'last_name' =>$this->input->post('delivery_last_name'),
                'country'=>$this->input->post('delivery_country_id'),
                'area_id'=>$this->input->post('delivery_area_id'),
                'email'=>$this->input->post('delivery_email'),
                'mobile'=>$this->input->post('delivery_mobile'),
                'alternate_mobile' =>$this->input->post('delivery_alternate_mobile'),
                'house'=>$this->input->post('delivery_house'),
                'street'=>$this->input->post('delivery_street'),
                'block'=>$this->input->post('delivery_block'),
                'avenue'=>$this->input->post('delivery_avenue'),
                'user_id'=> $this->data['user']->id,
                'id' => $this->input->post('delivery_id'),
                'is_default' =>$this->input->post('delivery_is_default'),
            );
           //print_r( $arr);
            $address_id = $this->address_model->set_address($arr);
            $res = $this->address_model->get_address($this->data['user']->id, $address_id, true);
            $output ='';
            if($res){
            foreach($res as $row){
            $output .=  '<div class="address-column">';
            if($row['is_default'] ==1){
                $output .= '<div class="addresstype alignleft bluebg">'.$this->lang->line('default_address') .'</div>';
               } 
               $output .='<ul>
              <li>'.$row['first_name'].' '.$row['last_name'].'</li>';
            
              $address = '';
              if($row['area_id']>0){
                  $address .=$this->lang->line('area').' : '.$row['area'].' ';
              }
              if($row['block']!=''){
                  $address .=$this->lang->line('block').' : '.$row['block'].' ';
              }
              if($row['street_no']!=''){
                  $address .=$this->lang->line('street_no').' : '.$row['street'].' ';
              }
              if($row['avenue']!=''){
                  $address .=$this->lang->line('avenue').' : '.$row['avenue'].' ';
              }
              if($row['house_no']!=''){
                  $address .=$this->lang->line('house_no').' : '.$row['house'].' ';
              }
              $city ='';
              $country_name ='';
              if($row['city_id']>0){
                  $city = $this->lang->line('city').': '.$row['city'].', ';
              }
              if($row['country']>0){
                  $country_name = $this->lang->line('country').': '.$row['country_name'];
              }
              $output .='<li class="addr-details">'.$address .'</li>
              <li class="addr-state-country">'. $city. $country_name.' </li>
              <li class="addr-phone">'.$this->lang->line('mobile') .': '.$row['mobile'].'</li>';
              if($row['alternate_mobile']){
                $output .= '<li class="addr-phone">'.$this->lang->line('alternate_mobile') .': '.$row['alternate_mobile'].'</li>';
               } 
               $output .= '<li class="addr-phone">'.$this->lang->line('email') .': '.$row['email'] .'</li>';

               $output .= ' </ul>
          <span class="buttoncontrols">
              <button data-id="'.$row['id'].'"  class="btn btn-primary btn_wdth_fit btn-delivery-continue">'.$this->lang->line('continue').'</button>
          </span>';
          $output .= '  <div class="address-controls greybg textright fullwidth alignleft">        
          <span class="delete disableanchor"><a class="delete-checkout-address" data-id="'.$row['id'].'"  href="javascript:void(0)">'.$this->lang->line('delete').'</a></span> <span class="edit disableanchor"><a class="edit-address"  data-first-name="'.$row['first_name'].'" data-last-name="'.$row['last_name'] .'" data-mobile="'.$row['mobile'].'" data-alternate-mobile="'. $row['alternate_mobile'].'"
          data-email="'.$row['email'].'" data-street="'.$row['street'].'"
          data-block="'.$row['block'].'" data-avenue="'.$row['avenue'].'"
          data-house="'.$row['house_no'].'" data-area="'. $row['area_id'].'" data-country="'.$row['country'] .'"
          data-type="'.$row['type'].'"  data-is-default="'.$row['is_default'].'" data-id="'.$row['id'] .'" 
          href="javascript:void(0)">'. $this->lang->line('edit') 
         .'</a></span>
      </div>
  </div>';
            }}
            $this->response['error'] = false;
            $this->response['message'] =$this->lang->line('address_added_success') ;
            $this->response['data'] = $res;
            $this->response['address'] = $output;
            $this->response['address_id'] =$address_id;
            print_r(json_encode($this->response));
            return false;
        } else {
            $this->response['error'] = true;
            $this->response['message'] = $this->lang->line('access_not_allowed');
            print_r(json_encode($this->response));
            return false;
        }
    }
    public function edit_address()
    {
        if ($this->ion_auth->logged_in()) {
            $this->form_validation->set_rules('id', 'Id', 'trim|required|numeric|xss_clean');
           $this->form_validation->set_rules('type', 'Type', 'trim|xss_clean');
            
            $this->form_validation->set_rules('name', 'Name', 'trim|xss_clean|required');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|numeric|xss_clean|required');
            
           
            
            $this->form_validation->set_rules('area_id', 'Area', 'trim|xss_clean|required');
            $this->form_validation->set_rules('street', 'Street Name', 'trim|xss_clean|required');
            $this->form_validation->set_rules('block', 'Block', 'trim|numeric|xss_clean|required');
            $this->form_validation->set_rules('avenue', 'Avenue / Building', 'trim|xss_clean|required');
            $this->form_validation->set_rules('house', 'House No/ Flat No', 'trim|xss_clean|required');

            if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return false;
            }
            $this->address_model->set_address($_POST);
            $res = $this->address_model->get_address(null, $_POST['id'], true);
            $this->response['error'] = false;
            $this->response['message'] = 'Address updated Successfully';
            $this->response['data'] = $res;
            print_r(json_encode($this->response));
            return false;
        } else {
            $this->response['error'] = true;
            $this->response['message'] = 'Unauthorized access is not allowed';
            print_r(json_encode($this->response));
            return false;
        }
    }

    //delete_address
    public function delete_address()
    {
        if ($this->ion_auth->logged_in()) {
            $this->form_validation->set_rules('id', 'Id', 'trim|required|numeric|xss_clean');
            if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                $this->response['data'] = array();
                print_r(json_encode($this->response));
                return false;
            }
            $this->address_model->delete_address($_POST);
            $this->response['error'] = false;
            $this->response['message'] = 'Address Deleted Successfully';
            $this->response['data'] = array();
            print_r(json_encode($this->response));
            return false;
        } else {
            $this->response['error'] = true;
            $this->response['message'] = 'Unauthorized access is not allowed';
            print_r(json_encode($this->response));
            return false;
        }
    }

    //get_address
    public function get_address()
    {
        if ($this->ion_auth->logged_in()) {
            $res = $this->address_model->get_address($this->data['user']->id);
            $is_default_counter = array_count_values(array_column($res, 'is_default'));

            if (!isset($is_default_counter['1']) && !empty($res)) {
                update_details(['is_default' => '1'], ['id' => $res[0]['id']], 'addresses');
                $res = $this->address_model->get_address($this->data['user']->id);
            }
            if (!empty($res)) {
                $this->response['error'] = false;
                $this->response['message'] = 'Address Retrieved Successfully';
                $this->response['data'] = $res;
            } else {
                $this->response['error'] = true;
                $this->response['message'] = "No Details Found !";
                $this->response['data'] = array();
            }
            print_r(json_encode($this->response));
        } else {
            $this->response['error'] = true;
            $this->response['message'] = 'Unauthorized access is not allowed';
            print_r(json_encode($this->response));
            return false;
        }
    }

    public function get_address_list()
    {
        if ($this->ion_auth->logged_in()) {
            return $this->address_model->get_address_list($this->data['user']->id);
        } else {
            $this->response['error'] = true;
            $this->response['message'] = 'Unauthorized access is not allowed';
            print_r(json_encode($this->response));
            return false;
        }
    }

    public function get_areas()
    {
        if ($this->ion_auth->logged_in()) {
            $this->form_validation->set_rules('city_id', 'City Id', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                print_r(json_encode($this->response));
                return false;
            }

            $city_id = $this->input->post('city_id', true);
            $areas = fetch_details(NULL, 'areas');
            if (empty($areas)) {
                $this->response['error'] = true;
                $this->response['message'] = "No Areas found for this City.";
                print_r(json_encode($this->response));
                return false;
            }
            $this->response['error'] = false;
            $this->response['data'] = $areas;
            print_r(json_encode($this->response));
            return false;
        } else {
            $this->response['error'] = true;
            $this->response['message'] = 'Unauthorized access is not allowed';
            print_r(json_encode($this->response));
            return false;
        }
    }

    public function favorites()
    {
        if ($this->data['is_logged_in']) {
            $this->data['main_page'] = 'favorites';
            $this->data['title'] = 'Dashboard | ' . $this->data['web_settings']['site_title'];
            $this->data['keywords'] = 'Dashboard, ' . $this->data['web_settings']['meta_keywords'];
            $this->data['description'] = 'Dashboard | ' . $this->data['web_settings']['meta_description'];
            $this->data['products'] = get_favorites($this->data['user']->id);
            $this->data['settings'] = get_settings('system_settings', true);
            $this->load->view('front-end/' . THEME . '/template', $this->data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    public function manage_favorites()
    {       
        if ($this->data['is_logged_in']) {
            $this->form_validation->set_rules('product_id', 'Product Id', 'trim|numeric|required|xss_clean');
            $this->form_validation->set_rules('product_variant_id', 'Product Variant Id', 'trim|numeric|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                $this->response['data'] = array();
                $this->response['favorites'] = array();
            } else {
                $data = [
                    'user_id' => $this->data['user']->id,
                    'product_id' => $this->input->post('product_id', true),
                    'product_variant_id' => $this->input->post('product_variant_id', true),
                ];
                if (is_exist($data, 'favorites')) {
                    $this->db->delete('favorites', $data);
					$fav=get_favorites($this->data['user']->id);
					$favno=count($fav);
					$this->response['data']=$favno;
                    $this->response['error']   = false;
					$this->response['favorites'] =$fav;
                    $this->response['message'] = !empty($this->lang->line('product_removed_from_favorite')) ? $this->lang->line('product_removed_from_favorite') : "Product removed from favorite !" ;
                   // print_r($fav);
                    print_r(json_encode($this->response));
                    return false;
                }
                $data = escape_array($data);
                $this->db->insert('favorites', $data);
				$fav=get_favorites($this->data['user']->id);
					$favno=count($fav);
					$this->response['data']=$favno;
                $this->response['favorites'] =$fav;
                $this->response['error'] = false;
                $this->response['message'] = !empty($this->lang->line('product_added_to_favorite')) ? $this->lang->line('product_added_to_favorite') : 'Product Added to favorite';

                //print_r($fav);
                print_r(json_encode($this->response));
               return false;
            }
        } else {
          
            $this->response['error'] = true;
            $this->response['message'] = !empty($this->lang->line('login_first_message')) ? $this->lang->line('login_first_message') : "Login First to Add Products in Favorite List.";
            print_r(json_encode($this->response));
            return false;
        }
    }
    public function delete_favorites()
    {
       
        if ($this->data['is_logged_in']) {
            $this->form_validation->set_rules('product_id', 'Product Id', 'trim|numeric|required|xss_clean');
            $this->form_validation->set_rules('product_variant_id', 'Product Variant Id', 'trim|numeric|required|xss_clean');
             if (!$this->form_validation->run()) {
                $this->response['error'] = true;
                $this->response['message'] = validation_errors();
                $this->response['data'] = array();
            } else {
                $data = [
                    'user_id' => $this->data['user']->id,
                    'product_id' => $this->input->post('product_id'),
                    'product_variant_id' => $this->input->post('product_variant_id', true)
                ];
                if (is_exist($data, 'favorites')) {
                    $this->db->delete('favorites', $data);
					$fav=get_favorites($this->data['user']->id);
					$favno=count($fav);
					$this->response['data']=$favno;
                    $this->response['error']   = false;
					
                    $this->response['message'] = !empty($this->lang->line('product_removed_from_favorite')) ? $this->lang->line('product_removed_from_favorite') : 'Product Removed to favorite';
                    print_r(json_encode($this->response));
                    return false;
                }
                
            }
        } else {
            $this->response['error'] = true;
            $this->response['message'] =!empty($this->lang->line('login_first_message')) ? $this->lang->line('login_first_message') : "Login First to Add Products in Favorite List.";
            print_r(json_encode($this->response));
            return false;
        }
    }
    public function get_transactions()
	{
		if($this->ion_auth->logged_in())
		{			
			return $this->Transaction_model->get_transactions_list($this->data['user']->id);
		}
		else{
			redirect(base_url(),'refresh');
		}
	}

    public function get_wallet_transactions()
	{
		if($this->ion_auth->logged_in())
		{			
			return $this->Transaction_model->get_transactions_list($this->data['user']->id);
		}
		else{
			redirect(base_url(),'refresh');
		}
	}
}
