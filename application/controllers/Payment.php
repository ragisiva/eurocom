<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('paypal_lib');
        $this->load->model(['cart_model', 'address_model', 'order_model']);
        $this->data['is_logged_in'] = ($this->ion_auth->logged_in()) ? 1 : 0;
        $this->data['user'] = ($this->ion_auth->logged_in()) ? $this->ion_auth->user()->row() : array();
        $this->response['csrfName'] = $this->security->get_csrf_token_name();
        $this->response['csrfHash'] = $this->security->get_csrf_hash();
        $this->data['settings'] = get_settings('system_settings', true);
        $this->data['web_settings'] = get_settings('web_settings', true);
        $cookie_lang = ($this->session->userdata('lang') !='') ? $this->session->userdata('lang', TRUE) : 'english' ;
        if($cookie_lang=='arabic'){
            $this->data['lang_prefix'] = 'ar_';
        }
        else{
            $this->data['lang_prefix'] ='';
        }
    }

    public function paypal()
    {
        $this->form_validation->set_rules('order_id', 'Order', 'trim|required|xss_clean|numeric');
        if (!$this->form_validation->run()) {
            $this->response['error'] = true;
            $this->response['message'] = validation_errors();
            $this->response['data'] = array();
            print_r(json_encode($this->response));
            return false;
        }
        $user_id = $this->data['user']->id;
        $order_id = $this->input->post('order_id', true);

        $order = $this->db->where('id', $order_id)->get('orders')->row_array();
        if (empty($order)) {
            $this->response['error'] = true;
            $this->response['message'] = "No Order Found.";
            $this->response['data'] = array();
            print_r(json_encode($this->response));
            return false;
        }
        // Set variables for paypal form
        $returnURL = base_url() . 'payment/success';
        $cancelURL = base_url() . 'payment/cancel';
        $notifyURL = base_url() . 'app/v1/api/ipn';
        $txn_id = time() . "-" . rand();

        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('item_name', 'Test');
        $this->paypal_lib->add_field('custom', $this->data['user']->id . '|' . $this->data['user']->email);
        $this->paypal_lib->add_field('item_number', $order['id']);
        $this->paypal_lib->add_field('amount', $order['final_total']);
        // Render paypal form
        $this->paypal_lib->paypal_auto_form();
    }

    public function success()
    {
        //if (!$this->ion_auth->logged_in()) {
            //redirect(base_url());
        //}

        $this->data['main_page'] = 'payment-success';
        $this->data['title'] = 'Payment Cancel | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Payment Cancel, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Payment Cancel | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = '';
        $this->load->view('front-end/'.THEME.'/template', $this->data);
    }

    public function cancel()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect(base_url());
        }
        $this->data['main_page'] = 'payment-cancel';
        $this->data['title'] = 'Payment Cancel | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Payment Cancel, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Payment Cancel | ' . $this->data['web_settings']['meta_description'];
        $this->data['meta_description'] = '';
        $this->load->view('front-end/'.THEME.'/template', $this->data);
    }
}


function app_payment_status()
{
    $paypalInfo = $this->input->get();

    if (!empty($paypalInfo) && isset($_GET['st']) && strtolower($_GET['st']) == "completed") {
        $response['error'] = false;
        $response['message'] = "Pagesa është kryer me sukses.";
        $response['data'] = $paypalInfo;
    } elseif (!empty($paypalInfo) && isset($_GET['st']) && strtolower($_GET['st']) == "authorized") {
        $response['error'] = false;
        $response['message'] = "Your payment has been Authorized successfully. We will capture your transaction within 30 minutes, once we process your order. After successful capture Ads wil be credited automatically.";
        $response['data'] = $paypalInfo;
    } else {
        $response['error'] = true;
        $response['message'] = "Pagesa është refuzuar. ";
        $response['data'] = (isset($_GET)) ? $this->input->get() : "";
    }
    print_r(json_encode($response));
}
/* Capture all the authorized transactions
        We are using another library and API for this operation
        
    */
function do_capture()
{
    // Load PayPal library
    $this->config->load('paypal');

    $config = array(
        'Sandbox' => $this->config->item('Sandbox'),             // Sandbox / testing mode option.
        'APIUsername' => $this->config->item('APIUsername'),     // PayPal API username of the API caller
        'APIPassword' => $this->config->item('APIPassword'),     // PayPal API password of the API caller
        'APISignature' => $this->config->item('APISignature'),     // PayPal API signature of the API caller
        'APISubject' => '',                                     // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
        'APIVersion' => $this->config->item('APIVersion')        // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
    );

    // Show Errors
    if ($config['Sandbox']) {
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
    }

    $this->load->library('Paypal_pro', $config);

    $where = " `payment_type` = 'paypal' and (status = 'Pending' or status = 'pending') ";
    $q = $this->db->get_where('transaction', $where);
    $paypal_txns = $q->result_array();

    foreach ($paypal_txns as $transaction) {
        $DCFields = array(
            'authorizationid' => $transaction['transaction_id'],                 // Required. The authorization identification number of the payment you want to capture. This is the transaction ID returned from DoExpressCheckoutPayment or DoDirectPayment.
            'amt' => $transaction['amount'] . '.00',                             // Required. Must have two decimal places.  Decimal separator must be a period (.) and optional thousands separator must be a comma (,)
            'completetype' => 'Complete',                     // Required.  The value Complete indiciates that this is the last capture you intend to make.  The value NotComplete indicates that you intend to make additional captures.
            'currencycode' => 'USD',                     // Three-character currency code
            'invnum' => 'NonVoIP#' . $transaction['id'],                         // Your invoice number
            'note' => 'Transaction captured by nonVoIP system',       // Informational note about this setlement that is displayed to the buyer in an email and in his transaction history.  255 character max.
            'softdescriptor' => 'Transaction captured by nonVoIP system',                 // Per transaction description of the payment that is passed to the customer's credit card statement.
            'storeid' => '',                         // ID of the merchant store.  This field is required for point-of-sale transactions.  Max: 50 char
            'terminalid' => ''                        // ID of the terminal.  50 char max.  
        );

        $PayPalRequestData = array('DCFields' => $DCFields);
        $PayPalResult = $this->paypal_pro->DoCapture($PayPalRequestData);

        if (!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
            /* some error occured please display the approriate message */
        } else {
            /* Successful call.  Load view or whatever you need to do here. */
        }
    }
}
