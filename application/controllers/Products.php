<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['url', 'language', 'timezone_helper']);
        $this->load->model(['cart_model', 'category_model', 'rating_model','brand_model']);
        $this->load->library(['pagination']);
        $this->data['settings'] = get_settings('system_settings', true);
        $this->data['web_settings'] = get_settings('web_settings', true);
        $this->data['is_logged_in'] = ($this->ion_auth->logged_in()) ? 1 : 0;
        $this->data['user'] = ($this->ion_auth->logged_in()) ? $this->ion_auth->user()->row() : array();
        $this->response['csrfName'] = $this->security->get_csrf_token_name();
        $this->response['csrfHash'] = $this->security->get_csrf_hash();
        $cookie_lang = ($this->session->userdata('lang') !='') ? $this->session->userdata('lang', TRUE) : 'english' ;
        if($cookie_lang=='arabic'){
            $this->data['lang_prefix'] = 'ar_';
        }
        else{
            $this->data['lang_prefix'] ='';
        }
    }

    public function index()
    {
       $this->form_validation->set_data($this->input->get(null, true));
        $this->form_validation->set_rules('category', 'Category', 'trim|xss_clean');
        $this->form_validation->set_rules('per-page', 'Per Page', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('sort', 'Sort', 'trim|xss_clean');
        $this->form_validation->set_rules('min-price', 'Min Price', 'trim|xss_clean');
        $this->form_validation->set_rules('max-price', 'Max Price', 'trim|xss_clean');

        if (!empty($_GET) && !$this->form_validation->run()) {
            redirect(base_url('products'));
        }
		
        // $attribute_values = '';
        // $attribute_names = '';
        // foreach ($this->input->get(null, true) as $key => $value) {
        //     if (strpos($key, 'filter-') !== false) {
        //         if (!empty($attribute_values)) {
        //             $attribute_values .= "|" . $this->input->get($key, true);
        //         } else {
        //             $attribute_values = $this->input->get($key, true);
        //         }

        //         $key = str_replace('filter-', '', $key);
        //         if (!empty($attribute_names)) {
        //             $attribute_names .= "|" . $key;
        //         } else {
        //             $attribute_names = $key;
        //         }
        //     }
        // }

        //get attributes ids
        // $attribute_values = explode('|', $attribute_values);
        // $attribute_names = explode('|', $attribute_names);
        // $filter['attribute_value_ids'] = get_attribute_ids_by_value($attribute_values, $attribute_names);
        // $filter['attribute_value_ids'] = implode(',', $filter['attribute_value_ids']);

        $category_id = ($this->input->get('category')) ? $this->input->get('category') : null;
        $limit = ($this->input->get('per-page')) ? $this->input->get('per-page', true) : 40;
        $sort_by = ($this->input->get('sort')) ? $this->input->get('sort', true) : '';
        $brandid = ($this->input->get('brand')) ? explode(',',$this->input->get('brand', true)) : '';
        $min_price = ($this->input->get('min_price')) ? $this->input->get('min_price', true) : '';
        $max_price = ($this->input->get('max_price')) ? $this->input->get('max_price', true) : '';
        $attribute_values = ($this->input->get('attributes')) ? $this->input->get('attributes', true) : '';
        if($attribute_values!='')
            $filter['attribute_value_ids'] = $attribute_values;
        else
        $filter['attribute_value_ids'] = '';

        if (!empty($category_id)) {
            $category_id = explode('|', $category_id);
        }
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
        //Product Sorting
        $sort = '';
        $order = '';
        $filter['search'] =  null;
        if ($sort_by == "top-rated") {
            $filter['product_type'] = "top_rated_product_including_all_products";
        } elseif ($sort_by == "date-desc") {
            $sort = 'pv.date_added';
            $order = 'desc';
        } elseif ($sort_by == "date-asc") {
            $sort = 'pv.date_added';
            $order = 'asc';
        } elseif ($sort_by == "price-asc") {
            $sort = 'price';
            $order = 'asc';
        } elseif ($sort_by == "price-desc") {
            $sort = 'price';
            $order = 'desc';
        }
        $this->data['filter_min_price'] =0; $this->data['filter_max_price'] =500;
        if($min_price >0){
            $filter['min_price'] = $min_price;
            $this->data['filter_min_price'] = $min_price; 
        }
        if($max_price >0){
            $filter['max_price'] = $max_price;
            $this->data['filter_max_price'] = $max_price;
        }
       
        //    $this->data['total_count'] = $total_rows = fetch_product(null, $filter, null, $category_id, null, null, null, null, TRUE,$brandid);

        //   $total_items = fetch_product($user_id, $filter, null, $category_id, $limit, $offset, $sort, $order,false,$brandid);

        //   $this->data['total_variants']= count_products($user_id, $filter, null, $category_id, null, null, null, null, TRUE,$brandid);

         $this->data['total_count']= $total_rows = count_products($user_id, $filter, null, $category_id, null, null, null, null, TRUE,$brandid);
      //$this->data['total_count'] = $total_rows =$product_details['total'];
        $config['base_url'] = base_url('products');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['page_query_string'] = FALSE;

        $config['attributes'] = array('class' => 'page-link');
        $config['full_tag_open'] = '<div class="product-pagi-nav">';
        $config['full_tag_close'] = '</div>';

        $config['first_tag_open'] = '';
        $config['first_link'] = 'First';
        $config['first_tag_close'] = '';

        $config['last_tag_open'] = '';
        $config['last_link'] = 'Last';
        $config['last_tag_close'] = '';

        $config['prev_tag_open'] = '';
        $config['prev_link'] = 'PREV <i class="fa fa-angle-double-left"></i>';
        $config['prev_tag_close'] = '';

        $config['next_tag_open'] = '';
        $config['next_link'] = 'NEXT <i class="fa fa-angle-double-right"></i>';
        $config['next_tag_close'] = '';

        $config['cur_tag_open'] = '<a class="page-link active">';
        $config['cur_tag_close'] = '</a>';

        $config['num_tag_open'] = '';
        $config['num_tag_close'] = '';

        $page_no = (empty($this->uri->segment(2))) ? 1 : $this->uri->segment(2);
        if (!is_numeric($page_no)) {
            redirect(base_url('products'));
        }
		
        $offset = ($page_no - 1) * $limit;
        $this->pagination->initialize($config);
        $this->data['links'] =  $this->pagination->create_links();
		
        $this->data['main_page'] = 'product-listing';
        $this->data['title'] = 'Product Listing | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Product Listing, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Product Listing | ' . $this->data['web_settings']['meta_description'];
       
        //get all product id 
        
        $this->data['products'] = $product_details = fetch_product_variants($user_id, $filter, null, $category_id, $limit, $offset, $sort, $order,false,$brandid);
        
        $brands = $this->brand_model->get_categories($id = NULL, '', '', 'row_order',  'ASC', 'true', '', $ignore_status = '',$category_id);
        $brands =array_map("unserialize", array_unique(array_map("serialize", $brands)));
        $this->data['brands'] = $brands;
        if($brandid =='')
        $this->data['filter_brands'] = [];
        else
        $this->data['filter_brands'] = $brandid;
        $product_details = fetch_product_ids($category_id,$brandid,$limit,$offset);
      
        $product_ids = array_column($product_details,'id');  
        $this->data['attributes'] =$attributes = fetch_product_attributes(implode(',',$product_ids));
        if($attribute_values!=''){
            $this->data['filter_attributes'] = explode(',',$attribute_values);
        }
        else{
            $this->data['filter_attributes'] = [];
        }
        $this->data['filters'] = (isset($this->data['products']['filters'])) ? json_encode($this->data['products']['filters']) : "";
        $this->data['filters_key'] = 'all_products_listing';
        $this->data['filter_sort'] =isset($sort_by) ? $sort_by :'';
        $this->data['page_main_bread_crumb'] = "Product Listing";
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
    public function offers()
    {
       
     
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
       
       
            $filter['product_type'] = "products_on_sale";
       
         
            $sort = 'pv.date_added';
            $order = 'desc';
       
      //  $total_rows = fetch_product(null, $filter, null, null, null, null, null, null, TRUE,null);
        $this->data['total_count']= $total_rows = count_products(null, $filter, null, null, null, null, null, null, TRUE,null);
       
       
		
        $this->data['main_page'] = 'product-offers';
        $this->data['title'] = 'Product Listing | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = 'Product Listing, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = 'Product Listing | ' . $this->data['web_settings']['meta_description'];
        $this->data['products'] = fetch_product_variants($user_id, $filter, null, $category_id,15,0, $sort, $order);
      
        $this->data['page_main_bread_crumb'] = "Product Listing";
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
    public function category($category_slug = '')
    {
        $this->form_validation->set_data($this->input->get(null, true));
        $this->form_validation->set_rules('per-page', 'Per Page', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('sort', 'Sort', 'trim|xss_clean');
        $this->form_validation->set_rules('min-price', 'Min Price', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('max-price', 'Max Price', 'trim|numeric|xss_clean');
        if (!empty($_GET) && !$this->form_validation->run()) {
            redirect(base_url('products'));
        }

        $category_id = get_category_id_by_slug($category_slug);
        if (empty($category_id)) {
            redirect(base_url('products'));
        }
        $category = $this->category_model->get_categories($category_id, $limit = null, $offset = null, $sort = null, $order = null, $has_child_or_item = 'true');
        if (empty($category)) {
            redirect(base_url('products'));
        }
        $category = $category[0];

       
        $filter = array();
        $limit = ($this->input->get('per-page')) ? $this->input->get('per-page', true) : 40;
        $sort_by = ($this->input->get('sort')) ? $this->input->get('sort', true) : '';
		$brandid = ($this->input->get('brand')) ? explode(',',$this->input->get('brand', true)) : '';
        $min_price = ($this->input->get('min_price')) ? $this->input->get('min_price', true) : '';
        $max_price = ($this->input->get('max_price')) ? $this->input->get('max_price', true) : '';
        $attribute_values = ($this->input->get('attributes')) ? $this->input->get('attributes', true) : '';
        if($attribute_values!='')
            $filter['attribute_value_ids'] = $attribute_values;
        else
        $filter['attribute_value_ids'] = '';

        $category_id = $category['id'];
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
        //Product Sorting
        $sort = '';
        $order = '';
        $filter['search'] =  null;
        if ($sort_by == "top-rated") {
            $filter['product_type'] = "top_rated_product_including_all_products";
        } elseif ($sort_by == "date-desc") {
            $sort = 'pv.date_added';
            $order = 'desc';
        } elseif ($sort_by == "date-asc") {
            $sort = 'pv.date_added';
            $order = 'asc';
        } elseif ($sort_by == "price-asc") {
            $sort = 'price';
            $order = 'asc';
        } elseif ($sort_by == "price-desc") {
            $sort = 'price';
            $order = 'desc';
        }
        $this->data['filter_min_price'] =0; $this->data['filter_max_price'] =500;
        if($min_price >0){
            $filter['min_price'] = $min_price;
            $this->data['filter_min_price'] = $min_price;
        }
        if($max_price >0){
            $filter['max_price'] = $max_price;
            $this->data['filter_max_price'] = $max_price;
        }
       // $this->data['total_count'] = $total_rows = fetch_product($user_id, $filter, null, $category_id, null, null, null, null, TRUE,$brandid);
        $this->data['total_count']= $total_rows = count_products($user_id, $filter, null, $category_id, null, null, null, null, TRUE,$brandid);
		$brands = $this->brand_model->get_categories($id = NULL, '', '', 'row_order',  'ASC', 'true', '', $ignore_status = '',$category_id);
        $brands =array_map("unserialize", array_unique(array_map("serialize", $brands)));
         $config['base_url'] = base_url('products/category/' . $category_slug);
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['page_query_string'] = FALSE;

        $config['attributes'] = array('class' => 'page-link');
        $config['full_tag_open'] = '<div class="product-pagi-nav">';
        $config['full_tag_close'] = '</div>';

        $config['first_tag_open'] = '';
        $config['first_link'] = 'First';
        $config['first_tag_close'] = '';

        $config['last_tag_open'] = '';
        $config['last_link'] = 'Last';
        $config['last_tag_close'] = '';

        $config['prev_tag_open'] = '';
        $config['prev_link'] = 'PREV <i class="fa fa-angle-double-left"></i>';
        $config['prev_tag_close'] = '';

        $config['next_tag_open'] = '';
        $config['next_link'] = 'NEXT <i class="fa fa-angle-double-right"></i>';
        $config['next_tag_close'] = '';

        $config['cur_tag_open'] = '<a class="page-link active">';
        $config['cur_tag_close'] = '</a>';

        $config['num_tag_open'] = '';
        $config['num_tag_close'] = '';
        $page_no = (empty($this->uri->segment(4))) ? 1 : $this->uri->segment(4);
        if (!is_numeric($page_no)) {
            redirect(base_url('products'));
        }
        $offset = ($page_no - 1) * $limit;
       $this->pagination->initialize($config);
       $this->data['links'] =  $this->pagination->create_links();
        $page_title = $category['name'] . " " . ((!empty($this->data['sub_categories'])) ? "Subcategories" : "") . " " . ((!empty($this->data['sub_categories']) && !empty($this->data['products']['product'])) ? "&" : "") . " " . ((!empty($this->data['products']['product'])) ? "Products" : "");
        $this->data['main_page'] = 'product-listing';
        $this->data['title'] = $page_title.' | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = $page_title.',Product Listing, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = $page_title.' Product Listing | ' . $this->data['web_settings']['meta_description'];
        $this->data['left_breadcrumb'] = $category['name'];
        $category_lang = !empty($this->lang->line("category")) ? $this->lang->line("category") : "Category";
        $this->data['right_breadcrumb'] = array(
            '<a href="' . base_url('home/categories') . '">' . $category_lang . '</a>',
        );
		$this->data['brands'] = $brands;
        
        $this->data['products'] = $product_rows = fetch_product_variants(null, $filter, null, $category_id, $limit, $offset, $sort, $order,false,$brandid);
        $product_details = fetch_product_ids($category_id,$brandid,$limit,$offset);
      
        $product_ids = implode(',',array_column($product_details,'id'));  
        if($product_ids!='') 
            $this->data['attributes'] =$attributes = fetch_product_attributes($product_ids);
        else
        $this->data['attributes'] = [];
        if($attribute_values!=''){
            $this->data['filter_attributes'] = explode(',',$attribute_values);
        }
        else{
            $this->data['filter_attributes'] = [];
        }

            if($brandid =='')
            $this->data['filter_brands'] = [];
            else
            $this->data['filter_brands'] = $brandid;
        // $this->data['filters'] = (isset($this->data['products']['filters'])) ? json_encode($this->data['products']['filters']) : "";
        $this->data['filters_key'] = 'category_products_'.$category_slug;
        $this->data['filter_sort'] =isset($sort_by) ? $sort_by :'';
        $this->data['single_category'] = $category;
        $this->data['sub_categories'] = $this->category_model->sub_categories($category['id'], 1);
        $this->data['page_main_bread_crumb'] = $page_title;
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
	
	public function brands($category_slug = '')
    {
        $this->form_validation->set_data($this->input->get(null, true));

        $this->form_validation->set_rules('per-page', 'Per Page', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('sort', 'Sort', 'trim|xss_clean');
        $this->form_validation->set_rules('min-price', 'Min Price', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('max-price', 'Max Price', 'trim|numeric|xss_clean');
        if (!empty($_GET) && !$this->form_validation->run()) {
            redirect(base_url('products'));
        }

        $category_id = get_brand_id_by_slug($category_slug);
        if (empty($category_id)) {
            redirect(base_url('products'));
        }
        $category = $this->brand_model->get_categories($category_id, $limit = null, $offset = null, $sort = null, $order = null, $has_child_or_item = 'true');
        if (empty($category)) {
            redirect(base_url('products'));
        }
        $category = $category[0];

        // $attribute_values = '';
        // $attribute_names = '';
        // foreach ($this->input->get(null, true) as $key => $value) {
        //     if (strpos($key, 'filter-') !== false) {
        //         if (!empty($attribute_values)) {
        //             $attribute_values .= "|" . $this->input->get($key, true);
        //         } else {
        //             $attribute_values = $this->input->get($key, true);
        //         }

        //         $key = str_replace('filter-', '', $key);
        //         if (!empty($attribute_names)) {
        //             $attribute_names .= "|" . $key;
        //         } else {
        //             $attribute_names = $key;
        //         }
        //     }
        // }

        // //get attributes ids
        // $attribute_values = explode('|', $attribute_values);
        // $attribute_names = explode('|', $attribute_names);
        // $filter['attribute_value_ids'] = get_attribute_ids_by_value($attribute_values, $attribute_names);
        // $filter['attribute_value_ids'] = implode(',', $filter['attribute_value_ids']);
        $filter = array();
        $limit = ($this->input->get('per-page')) ? $this->input->get('per-page', true) : 40;
        $sort_by = ($this->input->get('sort')) ? $this->input->get('sort', true) : '';
		$brandid = ($this->input->get('brand')) ? $this->input->get('brand', true) : '';
        $brandid=$category['id'];
        $min_price = ($this->input->get('min_price')) ? $this->input->get('min_price', true) : '';
        $max_price = ($this->input->get('max_price')) ? $this->input->get('max_price', true) : '';
        $attribute_values = ($this->input->get('attributes')) ? $this->input->get('attributes', true) : '';
        if($attribute_values!='')
            $filter['attribute_value_ids'] = $attribute_values;
        else
        $filter['attribute_value_ids'] = '';
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
        //Product Sorting
        $sort = '';
        $order = '';
        $filter['search'] =  null;
        if ($sort_by == "top-rated") {
            $filter['product_type'] = "top_rated_product_including_all_products";
        } elseif ($sort_by == "date-desc") {
            $sort = 'pv.date_added';
            $order = 'desc';
        } elseif ($sort_by == "date-asc") {
            $sort = 'pv.date_added';
            $order = 'asc';
        } elseif ($sort_by == "price-asc") {
            $sort = 'price';
            $order = 'asc';
        } elseif ($sort_by == "price-desc") {
            $sort = 'price';
            $order = 'desc';
        }
        $this->data['filter_min_price'] =0; $this->data['filter_max_price'] =500;
        if($min_price >0){
            $filter['min_price'] = $min_price;
            $this->data['filter_min_price'] = $min_price;
        }
        if($max_price >0){
            $filter['max_price'] = $max_price;
            $this->data['filter_max_price'] = $max_price;
        }
        $this->data['total_count']= $total_rows = count_products($user_id, $filter, null, null, null, null, null, null, TRUE,$brandid);
        //$this->data['total_count'] = $total_rows = fetch_product_brand($user_id, $filter, null, $category_id, null, null, null, null, TRUE,$brandid);
		$brands = $this->brand_model->get_categories('', $limit, $offset, null, $order, false);
         $config['base_url'] = base_url('products/brands/' . $category_slug);
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['page_query_string'] = FALSE;

        $config['attributes'] = array('class' => 'page-link');
        $config['full_tag_open'] = '<div class="product-pagi-nav">';
        $config['full_tag_close'] = '</div>';

        $config['first_tag_open'] = '';
        $config['first_link'] = 'First';
        $config['first_tag_close'] = '';

        $config['last_tag_open'] = '';
        $config['last_link'] = 'Last';
        $config['last_tag_close'] = '';

        $config['prev_tag_open'] = '';
        $config['prev_link'] = 'PREV <i class="fa fa-angle-double-left"></i>';
        $config['prev_tag_close'] = '';

        $config['next_tag_open'] = '';
        $config['next_link'] = 'NEXT <i class="fa fa-angle-double-right"></i>';
        $config['next_tag_close'] = '';

        $config['cur_tag_open'] = '<a class="page-link active">';
        $config['cur_tag_close'] = '</a>';

        $config['num_tag_open'] = '';
        $config['num_tag_close'] = '';
        $page_no = (empty($this->uri->segment(4))) ? 1 : $this->uri->segment(4);
        if (!is_numeric($page_no)) {
            redirect(base_url('products'));
        }
        $offset = ($page_no - 1) * $limit;
       $this->pagination->initialize($config);
        $this->data['links'] =  $this->pagination->create_links();
        $page_title = $category['name'] . " " . ((!empty($this->data['sub_categories'])) ? "Subcategories" : "") . " " . ((!empty($this->data['sub_categories']) && !empty($this->data['products']['product'])) ? "&" : "") . " " . ((!empty($this->data['products']['product'])) ? "Products" : "");
        $this->data['main_page'] = 'product-listing';
        $this->data['title'] = $page_title.' | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = $page_title.',Product Listing, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = $page_title.' Product Listing | ' . $this->data['web_settings']['meta_description'];
        $this->data['left_breadcrumb'] = $category['name'];
        $category_lang = !empty($this->lang->line("category")) ? $this->lang->line("category") : "Category";
        $this->data['right_breadcrumb'] = array(
            '<a href="' . base_url('home/categories') . '">' . $category_lang . '</a>',
        );
		$this->data['brands'] ='';
        $product_details = fetch_product_ids(null,$brandid,$limit,$offset);
      
        $product_ids = implode(',',array_column($product_details,'id')); 
        if($product_ids!='') 
            $this->data['attributes'] =$attributes = fetch_product_attributes($product_ids);
        else
        $this->data['attributes'] = [];
        if($attribute_values!=''){
            $this->data['filter_attributes'] = explode(',',$attribute_values);
        }
        else{
            $this->data['filter_attributes'] = [];
        }
        $this->data['products'] = fetch_product_variants(null, $filter, null, null, $limit, $offset, $sort, $order,false,$brandid);
        // $this->data['filters'] = (isset($this->data['products']['filters'])) ? json_encode($this->data['products']['filters']) : "";
        $this->data['filters_key'] = 'category_products_'.$category_slug;
        $this->data['single_category'] = $category;
        $this->data['filter_sort'] =isset($sort_by) ? $sort_by :'';
        $this->data['sub_categories'] = $this->category_model->sub_categories($category['id'], 1);
        $this->data['page_main_bread_crumb'] = $page_title;
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }

    public function details($slug = '',$product_variant_id='')
    {
		$t = &get_instance();
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
        $slug = urldecode($slug);
        $product = fetch_product($user_id, ['slug' => $slug,'selected_variant_id' =>$product_variant_id]);
        if (empty($product)) {
            redirect(base_url('products'));
        }
        if(isset($product['product'][0]['default_product']->id)){
            $c_variant_id  = $product['product'][0]['default_product']->id;
        }
        else{
            $c_variant_id  = isset($product['product'][0]['variants'][0]['id']) ? $product['product'][0]['variants'][0]['id'] : '' ;
        }
        //set COOKIE FOR RECENTLY VIEWED
        if(array_key_exists('recently_viewed',$_COOKIE)){
            $cookieget = get_cookie('recently_viewed');
            $cookie_res = unserialize($cookieget);
            if(!in_array($c_variant_id,$cookie_res)){
                $cookie_res[] = $c_variant_id;
            }
            delete_cookie('recently_viewed');
            //again set cookie
            $cookievalue = serialize($cookie_res);
            $cookiearr = array(
                'name'=>'recently_viewed',
                'value' =>$cookievalue,
                'expire' => '86400'
            );
            $this->input->set_cookie($cookiearr);
        }
        else{
        $cookie_data[] =  $c_variant_id;
        $cookievalue = serialize($cookie_data);
        $cookiearr = array(
            'name'=>'recently_viewed',
            'value' =>$cookievalue,
            'expire' => '86400'
        );
        $this->input->set_cookie($cookiearr);
        
       }
		$brands =  $t->db->select(' * ')->where('id', $product['product'][0]['brand'])->get('brands')->result_array();
        $this->data['product'] = $product;
		$rescat = $t->db->select(' * ')->where('id', $product['product'][0]['category_id'])->get('categories')->result_array();
        $user_rating_limit = 5;
        $user_rating_offset = 0;
        $this->data['product_ratings'] = $this->rating_model->fetch_rating($product['product'][0]['id'], null, $user_rating_limit, $user_rating_offset);
        $this->data['my_rating'] = array();
        if ($this->ion_auth->logged_in()) {
            $this->data['my_rating'] = $this->rating_model->fetch_rating($product['product'][0]['id'], $this->data['user']->id);
        }
        $this->data['related_products'] = fetch_product($user_id, NULL, NULL, $product['product'][0]['category_id'], 12); 
        $this->data['main_page'] = 'product-page';
		$this->data['banner'] = $rescat[0]['banner'];
		$this->data['catname'] = $rescat[0]['name'];
        $this->data['ar_catname'] = $rescat[0]['ar_name'];
		$this->data['brand']=$brands[0]['name'];
        $this->data['ar_brand']=$brands[0]['ar_name'];
		$this->data['brandslug']=$brands[0]['slug'];
		$this->data['catslug'] = $rescat[0]['slug'];
        $this->data['title'] = $product['product'][0]['name'] . ' | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = $product['product'][0]['name'] . ', ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = $product['product'][0]['name'] . ' | ' . $this->data['web_settings']['meta_description'];
        $this->data['username'] = $this->session->userdata('username');
        $this->data['user_rating_limit'] = $user_rating_limit;
        $this->data['user_rating_offset'] = $user_rating_limit + $user_rating_offset;
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }

    public function get_details($product_id = '')
    {
        if (empty($product_id)) {
            return false;
        }
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
        $product = fetch_product($user_id, null, $product_id);
        if (isset($product['product']) && empty($product['product'])) {
            return false;
        }
        $product = $product['product'][0];
        $product['get_price'] = get_price_range_of_product($product['id']);
        print_r(json_encode($product));
    }

    public function section($section_id = '', $section_title = '')
    {
        if (empty($section_id)) {
            redirect(base_url());
        }
        $this->form_validation->set_data($this->input->get(null, true));
        $this->form_validation->set_rules('per-page', 'Per Page', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('sort', 'Sort', 'trim|xss_clean');
        $this->form_validation->set_rules('min-price', 'Min Price', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('max-price', 'Max Price', 'trim|numeric|xss_clean');
        if (!empty($_GET) && !$this->form_validation->run()) {
            redirect(base_url('products'));
        }
        $section = $this->db->where('id', $section_id)->get('sections')->row_array();
        if (empty($section)) {
            redirect(base_url());
        }

        $attribute_values = '';
        $attribute_names = '';
        foreach ($this->input->get(null, true) as $key => $value) {
            if (strpos($key, 'filter-') !== false) {
                if (!empty($attribute_values)) {
                    $attribute_values .= "|" . $this->input->get($key, true);
                } else {
                    $attribute_values = $this->input->get($key, true);
                }

                $key = str_replace('filter-', '', $key);
                if (!empty($attribute_names)) {
                    $attribute_names .= "|" . $key;
                } else {
                    $attribute_names = $key;
                }
            }
        }

        //get attributes ids
        $attribute_values = explode('|', $attribute_values);
        $attribute_names = explode('|', $attribute_names);
        $filter = array();
        $filter['attribute_value_ids'] = get_attribute_ids_by_value($attribute_values, $attribute_names);
        $filter['attribute_value_ids'] = implode(',', $filter['attribute_value_ids']);
        $product_ids = explode(',', $section['product_ids']);
        $product_ids = array_filter($product_ids);
        if (isset($section['product_type']) && !empty($section['product_type'])) {
            $filter['product_type'] = (isset($section['product_type'])) ? $section['product_type'] : null;
        }
        $product_categories = (isset($section['categories']) && !empty($section['categories']) && $section['categories'] != NULL) ? explode(',', $section['categories']) : null;
        $limit = ($this->input->get('per-page')) ? $this->input->get('per-page', true) : 12;
        $sort_by = ($this->input->get('sort')) ? $this->input->get('sort', true) : '';
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
        //Product Sorting
        $sort = '';
        $order = '';
        $filter['search'] =  null;
        if ($sort_by == "top-rated") {
            $filter['product_type'] = "top_rated_product_including_all_products";
        } elseif ($sort_by == "date-desc") {
            $sort = 'pv.date_added';
            $order = 'desc';
        } elseif ($sort_by == "date-asc") {
            $sort = 'pv.date_added';
            $order = 'asc';
        } elseif ($sort_by == "price-asc") {
            $sort = 'price';
            $order = 'asc';
        } elseif ($sort_by == "price-desc") {
            $sort = 'price';
            $order = 'desc';
        }
        $total_rows = fetch_product($user_id, $filter, $product_ids, $product_categories, null, null, null, null, TRUE);

        $config['base_url'] = base_url('products/section/' . $section_id . '/' . $section_title);
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $limit;
        $config['uri_segment'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 7;
        $config['reuse_query_string'] = TRUE;
        $config['page_query_string'] = FALSE;

        $config['attributes'] = array('class' => 'page-link');
        $config['full_tag_open'] = '<ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul>';

        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_link'] = 'First';
        $config['first_tag_close'] = '</li>';

        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_link'] = 'Last';
        $config['last_tag_close'] = '</li>';

        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_link'] = '<i class="fa fa-arrow-left"></i>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_link'] = '<i class="fa fa-arrow-right"></i>';
        $config['next_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $page_no = (empty($this->uri->segment(5))) ? 1 : $this->uri->segment(5);
        if (!is_numeric($page_no)) {
            redirect(base_url('products'));
        }
        $offset = ($page_no - 1) * $limit;
        $this->pagination->initialize($config);
        $this->data['links'] =  $this->pagination->create_links();
        $page_title = $section['title'] . " Products";
        $this->data['main_page'] = 'product-listing';
        $this->data['title'] = $page_title.' | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = $page_title.',Product Section, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = $page_title.' Product Section | ' . $this->data['web_settings']['meta_description'];
        $this->data['left_breadcrumb'] = $section['title'];
        $category_lang = !empty($this->lang->line("section")) ? $this->lang->line("section") : "Section";
        $this->data['right_breadcrumb'] = array(
            !empty($this->lang->line("section")) ? $this->lang->line("section") : "Section",
        );
        $this->data['products'] = fetch_product(null, $filter, $product_ids, $product_categories, $limit, $offset, $sort, $order);
        $this->data['filters'] = (isset($this->data['products']['filters'])) ? json_encode($this->data['products']['filters']) : "";
        $this->data['filters_key'] = 'products_section_'.$section_id;
        $this->data['page_main_bread_crumb'] = $page_title;
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }
    public function search()
    {
        $this->form_validation->set_data($this->input->get(null, true));
        $this->form_validation->set_rules('per-page', 'Per Page', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('sort', 'Sort', 'trim|xss_clean');
        $this->form_validation->set_rules('min-price', 'Min Price', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('max-price', 'Max Price', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('q', 'search', 'required|trim|xss_clean');
        // if (!empty($_GET) && !$this->form_validation->run()) {
        //     redirect(base_url('products'));
        // }

        // $attribute_values = '';
        // $attribute_names = '';
        // foreach ($this->input->get(null, true) as $key => $value) {
        //     if (strpos($key, 'filter-') !== false) {
        //         if (!empty($attribute_values)) {
        //             $attribute_values .= "|" . $this->input->get($key, true);
        //         } else {
        //             $attribute_values = $this->input->get($key, true);
        //         }

        //         $key = str_replace('filter-', '', $key);
        //         if (!empty($attribute_names)) {
        //             $attribute_names .= "|" . $key;
        //         } else {
        //             $attribute_names = $key;
        //         }
        //     }
        // }

        // //get attributes ids
        // $attribute_values = explode('|', $attribute_values);
        // $attribute_names = explode('|', $attribute_names);
        // $filter = array();
        // $filter['attribute_value_ids'] = get_attribute_ids_by_value($attribute_values, $attribute_names);
        // $filter['attribute_value_ids'] = implode(',', $filter['attribute_value_ids']);
        $filter = array();
        $limit = ($this->input->get('per-page')) ? $this->input->get('per-page', true) : 40;
        $sort_by = ($this->input->get('sort')) ? $this->input->get('sort', true) : '';
        $brandid = ($this->input->get('brand')) ? explode(',',$this->input->get('brand', true)) : '';
        $user_id = NULL;
        $min_price = ($this->input->get('min_price')) ? $this->input->get('min_price', true) : '';
        $max_price = ($this->input->get('max_price')) ? $this->input->get('max_price', true) : '';
        $attribute_values = ($this->input->get('attributes')) ? $this->input->get('attributes', true) : '';
        if($attribute_values!='')
            $filter['attribute_value_ids'] = $attribute_values;
        else
        $filter['attribute_value_ids'] = '';
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
        //Product Sorting
        $sort = '';
        $order = '';
        $filter['search'] =  $this->input->get('q', true);
        if ($sort_by == "top-rated") {
            $filter['product_type'] = "top_rated_product_including_all_products";
        } elseif ($sort_by == "date-desc") {
            $sort = 'pv.date_added';
            $order = 'desc';
        } elseif ($sort_by == "date-asc") {
            $sort = 'pv.date_added';
            $order = 'asc';
        } elseif ($sort_by == "price-asc") {
            $sort = 'price';
            $order = 'asc';
        } elseif ($sort_by == "price-desc") {
            $sort = 'price';
            $order = 'desc';
        }
        $this->data['filter_min_price'] =0; $this->data['filter_max_price'] =500;
        if($min_price >0){
            $filter['min_price'] = $min_price;
            $this->data['filter_min_price'] = $min_price;
        }
        if($max_price >0){
            $filter['max_price'] = $max_price;
            $this->data['filter_max_price'] = $max_price;
        }
        $this->data['total_count']= $total_rows = count_products($user_id, $filter, null, null, null, null, null, null, TRUE,$brandid);
        //$this->data['total_count'] =  $total_rows =fetch_product(null, $filter, null, null, null, null, null, null, TRUE,$brandid);

        // $config['base_url'] = base_url('products/search');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['page_query_string'] = FALSE;

        $config['attributes'] = array('class' => 'page-link');
        $config['full_tag_open'] = '<div class="product-pagi-nav">';
        $config['full_tag_close'] = '</div>';

        $config['first_tag_open'] = '';
        $config['first_link'] = 'First';
        $config['first_tag_close'] = '';

        $config['last_tag_open'] = '';
        $config['last_link'] = 'Last';
        $config['last_tag_close'] = '';

        $config['prev_tag_open'] = '';
        $config['prev_link'] = 'PREV <i class="fa fa-angle-double-left"></i>';
        $config['prev_tag_close'] = '';

        $config['next_tag_open'] = '';
        $config['next_link'] = 'NEXT <i class="fa fa-angle-double-right"></i>';
        $config['next_tag_close'] = '';

        $config['cur_tag_open'] = '<a class="page-link active">';
        $config['cur_tag_close'] = '</a>';

        $config['num_tag_open'] = '';
        $config['num_tag_close'] = '';
        $page_no = (empty($this->uri->segment(3))) ? 1 : $this->uri->segment(3);
        if (!is_numeric($page_no)) {
            redirect(base_url('products'));
        }
        $offset = ($page_no - 1) * $limit;
       $this->pagination->initialize($config);
       $this->data['links'] =  $this->pagination->create_links();
        $page_title = 'Search Result for "' . html_escape($_GET['q']) . '"';
        $this->data['main_page'] = 'product-listing';
        $this->data['title'] = $page_title.' | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = $page_title.',Product Section, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = $page_title.' Product Section | ' . $this->data['web_settings']['meta_description'];
        $this->data['left_breadcrumb'] = "Search";
        $category_lang = !empty($this->lang->line("section")) ? $this->lang->line("section") : "Section";
        $this->data['right_breadcrumb'] = array(
            !empty($this->lang->line("search")) ? $this->lang->line("search") : "Search",
        );

        $this->data['products'] =fetch_product_variants(null, $filter, null, null, $limit, $offset, $sort, $order,false,$brandid);
        $brands = $this->brand_model->get_categories($id = NULL, '', '', 'row_order',  'ASC', 'true', '', $ignore_status = '');
        $brands =array_map("unserialize", array_unique(array_map("serialize", $brands)));
        $this->data['brands'] = $brands;
        if($brandid =='')
        $this->data['filter_brands'] = [];
        else
        $this->data['filter_brands'] = $brandid;
        $product_details = fetch_product_ids('',$brandid,$limit,$offset);
      
       
        $product_ids = implode(',',array_column($product_details,'id'));  
        if($product_ids!='') 
            $this->data['attributes'] =$attributes = fetch_product_attributes($product_ids);
        else
        $this->data['attributes'] = [];
        if($attribute_values!=''){
            $this->data['filter_attributes'] = explode(',',$attribute_values);
        }
        else{
            $this->data['filter_attributes'] = [];
        }
        $this->data['filter_sort'] =isset($sort_by) ? $sort_by :'';
       // $this->data['products'] = fetch_product(null, $filter, null, null, $limit, $offset, $sort, $order);
        $this->data['filters'] = (isset($this->data['products']['filters'])) ? json_encode($this->data['products']['filters']) : "";
        $this->data['filters_key'] = 'products_search';
        $this->data['page_main_bread_crumb'] = $page_title;
        // $this->data['range_slider_prices'] = get_min_max_price_of_product();
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }

    public function tags($tag = '')
    {
        if (empty($tag)) {
            redirect(base_url());
        }
        $this->form_validation->set_data($this->input->get(null, true));
        $this->form_validation->set_rules('per-page', 'Per Page', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('sort', 'Sort', 'trim|xss_clean');
        $this->form_validation->set_rules('min-price', 'Min Price', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('max-price', 'Max Price', 'trim|numeric|xss_clean');
        if (!empty($_GET) && !$this->form_validation->run()) {
            redirect(base_url('products'));
        }

        // $attribute_values = '';
        // $attribute_names = '';
        // foreach ($this->input->get(null, true) as $key => $value) {
        //     if (strpos($key, 'filter-') !== false) {
        //         if (!empty($attribute_values)) {
        //             $attribute_values .= "|" . $this->input->get($key, true);
        //         } else {
        //             $attribute_values = $this->input->get($key, true);
        //         }

        //         $key = str_replace('filter-', '', $key);
        //         if (!empty($attribute_names)) {
        //             $attribute_names .= "|" . $key;
        //         } else {
        //             $attribute_names = $key;
        //         }
        //     }
        // }

        // //get attributes ids
        // $attribute_values = explode('|', $attribute_values);
        // $attribute_names = explode('|', $attribute_names);
        // $filter = array();
        // $filter['tags'] = xss_clean($tag);
        // $filter['attribute_value_ids'] = get_attribute_ids_by_value($attribute_values, $attribute_names);
        // $filter['attribute_value_ids'] = implode(',', $filter['attribute_value_ids']);
        $filter = array();
        $limit = ($this->input->get('per-page')) ? $this->input->get('per-page', true) : 12;
        $sort_by = ($this->input->get('sort')) ? $this->input->get('sort', true) : '';
       // $attribute_values = ($this->input->get('attributes')) ? $this->input->get('attributes', true) : '';
        $brandid = ($this->input->get('brand')) ? explode(',',$this->input->get('brand', true)) : '';
        $user_id = NULL;
        $min_price = ($this->input->get('min_price')) ? $this->input->get('min_price', true) : '';
        $max_price = ($this->input->get('max_price')) ? $this->input->get('max_price', true) : '';
        $attribute_values = ($this->input->get('attributes')) ? $this->input->get('attributes', true) : '';
        if($attribute_values!='')
            $filter['attribute_value_ids'] = $attribute_values;
        else
        $filter['attribute_value_ids'] = '';
        
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
        //Product Sorting
        $sort = '';
        $order = '';
        $filter['search'] =  null;
        if ($sort_by == "top-rated") {
            $filter['product_type'] = "top_rated_product_including_all_products";
        } elseif ($sort_by == "date-desc") {
            $sort = 'pv.date_added';
            $order = 'desc';
        } elseif ($sort_by == "date-asc") {
            $sort = 'pv.date_added';
            $order = 'asc';
        } elseif ($sort_by == "price-asc") {
            $sort = 'price';
            $order = 'asc';
        } elseif ($sort_by == "price-desc") {
            $sort = 'price';
            $order = 'desc';
        }
        //$this->data['total_count'] = $total_rows = fetch_product($user_id, $filter, null, null, null, null, null, null, TRUE);
        $this->data['total_count']= $total_rows = count_products($user_id, $filter, null, $category_id, null, null, null, null, TRUE,$brandid);
        // $config['base_url'] = base_url('products/search');
        // $config['total_rows'] = $total_rows;
        // $config['per_page'] = $limit;
        // $config['uri_segment'] = 3;
        // $config['use_page_numbers'] = TRUE;
        // $config['num_links'] = 7;
        // $config['reuse_query_string'] = TRUE;
        // $config['page_query_string'] = FALSE;

        // $config['attributes'] = array('class' => 'page-link');
        // $config['full_tag_open'] = '<ul class="pagination justify-content-center">';
        // $config['full_tag_close'] = '</ul>';

        // $config['first_tag_open'] = '<li class="page-item">';
        // $config['first_link'] = 'First';
        // $config['first_tag_close'] = '</li>';

        // $config['last_tag_open'] = '<li class="page-item">';
        // $config['last_link'] = 'Last';
        // $config['last_tag_close'] = '</li>';

        // $config['prev_tag_open'] = '<li class="page-item">';
        // $config['prev_link'] = '<i class="fa fa-arrow-left"></i>';
        // $config['prev_tag_close'] = '</li>';

        // $config['next_tag_open'] = '<li class="page-item">';
        // $config['next_link'] = '<i class="fa fa-arrow-right"></i>';
        // $config['next_tag_close'] = '</li>';

        // $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link">';
        // $config['cur_tag_close'] = '</a></li>';

        // $config['num_tag_open'] = '<li class="page-item">';
        // $config['num_tag_close'] = '</li>';
        $page_no = (empty($this->uri->segment(4))) ? 1 : $this->uri->segment(4);
        if (!is_numeric($page_no)) {
            redirect(base_url('products'));
        }
        $offset = ($page_no - 1) * $limit;
       // $this->pagination->initialize($config);
        //$this->data['links'] =  $this->pagination->create_links();
        $page_title = 'Products by tag "' . xss_clean($tag) . '"';
        $this->data['main_page'] = 'product-listing';
        $this->data['title'] = $page_title.' | ' . $this->data['web_settings']['site_title'];
        $this->data['keywords'] = $page_title.', Product Section, ' . $this->data['web_settings']['meta_keywords'];
        $this->data['description'] = $page_title.' Product Section | ' . $this->data['web_settings']['meta_description'];
        $this->data['left_breadcrumb'] = "Search";
        $category_lang = !empty($this->lang->line("section")) ? $this->lang->line("section") : "Section";
        $this->data['right_breadcrumb'] = array(
            !empty($this->lang->line("tags")) ? $this->lang->line("tags") : "Tags",
        );
        $this->data['products'] = fetch_product(null, $filter, null, null, $limit, $offset, $sort, $order);
        $this->data['filters'] = (isset($this->data['products']['filters'])) ? json_encode($this->data['products']['filters']) : "";
        $this->data['attributes'] =$attributes = fetch_product_attributes();
        if($attribute_values!=''){
            $this->data['filter_attributes'] = explode(',',$attribute_values);
        }
        else{
            $this->data['filter_attributes'] = [];
        }
        $this->data['filters_key'] = 'products_tags';
        $this->data['page_main_bread_crumb'] = $page_title;
        $this->load->view('front-end/' . THEME . '/template', $this->data);
    }

    // 9 save_rating
    public function save_rating()
    {
        /*
            user_id: 21
            product_id: 33
            rating: 4.2
            comment: 'Done' {optional}
        */
        if (!$this->ion_auth->logged_in()) {
            return false;
        }
        $this->form_validation->set_rules('product_id', 'Product Id', 'trim|numeric|xss_clean|required');
        $this->form_validation->set_rules('rating', 'Rating', 'trim|numeric|xss_clean|greater_than[0]|less_than[6]|required');
        $this->form_validation->set_rules('comment', 'Comment', 'trim|xss_clean');
        $_POST['user_id'] = $this->data['user']->id;
        if (!$this->form_validation->run()) {
            $this->response['error'] = true;
            $this->response['message'] = validation_errors();
            $this->response['data'] = array();
            echo json_encode($this->response);
        } else {
            if (!file_exists(FCPATH . REVIEW_IMG_PATH)) {
                mkdir(FCPATH . REVIEW_IMG_PATH, 0777);
            }

            $temp_array = array();
            $files = $_FILES;
            $images_new_name_arr = array();
            $images_info_error = "";
            $config = [
                'upload_path' =>  FCPATH . REVIEW_IMG_PATH,
                'allowed_types' => 'jpg|png|jpeg|gif',
                'max_size' => 8000,
            ];

            if (!empty($_FILES['images']['name'][0]) && isset($_FILES['images']['name'])) {
                $other_image_cnt = count($_FILES['images']['name']);
                $other_img = $this->upload;
                $other_img->initialize($config);

                for ($i = 0; $i < $other_image_cnt; $i++) {

                    if (!empty($_FILES['images']['name'][$i])) {

                        $_FILES['temp_image']['name'] = $files['images']['name'][$i];
                        $_FILES['temp_image']['type'] = $files['images']['type'][$i];
                        $_FILES['temp_image']['tmp_name'] = $files['images']['tmp_name'][$i];
                        $_FILES['temp_image']['error'] = $files['images']['error'][$i];
                        $_FILES['temp_image']['size'] = $files['images']['size'][$i];
                        if (!$other_img->do_upload('temp_image')) {
                            $images_info_error = 'Images :' . $images_info_error . ' ' . $other_img->display_errors();
                        } else {
                            $temp_array = $other_img->data();
                            resize_review_images($temp_array, FCPATH . REVIEW_IMG_PATH);
                            $images_new_name_arr[$i] = REVIEW_IMG_PATH . $temp_array['file_name'];
                        }
                    } else {
                        $_FILES['temp_image']['name'] = $files['images']['name'][$i];
                        $_FILES['temp_image']['type'] = $files['images']['type'][$i];
                        $_FILES['temp_image']['tmp_name'] = $files['images']['tmp_name'][$i];
                        $_FILES['temp_image']['error'] = $files['images']['error'][$i];
                        $_FILES['temp_image']['size'] = $files['images']['size'][$i];
                        if (!$other_img->do_upload('temp_image')) {
                            $images_info_error = $other_img->display_errors();
                        }
                    }
                }

                //Deleting Uploaded Images if any overall error occured
                if ($images_info_error != NULL || !$this->form_validation->run()) {
                    if (isset($images_new_name_arr) && !empty($images_new_name_arr || !$this->form_validation->run())) {
                        foreach ($images_new_name_arr as $key => $val) {
                            unlink(FCPATH . REVIEW_IMG_PATH . $images_new_name_arr[$key]);
                        }
                    }
                }
            }

            if ($images_info_error != NULL) {
                $this->response['error'] = true;
                $this->response['csrfName'] = $this->security->get_csrf_token_name();
                $this->response['csrfHash'] = $this->security->get_csrf_hash();
                $this->response['message'] =  $images_info_error;
                print_r(json_encode($this->response));
                return;
            }

            $res = $this->db->select('*')->join('product_variants pv', 'pv.id=oi.product_variant_id')->join('products p', 'p.id=pv.product_id')->where(['pv.product_id' => $_POST['product_id'], 'oi.user_id' => $_POST['user_id'], 'oi.active_status!=' => 'returned'])->limit(1)->get('order_items oi')->result_array();
            if (empty($res)) {
                $this->response['error'] = true;
                $this->response['message'] = 'You cannot review as the product is not purchased yet!';
                $this->response['data'] = array();
                echo json_encode($this->response);
                return;
            }

            $rating_data = fetch_details(['user_id' => $_POST['user_id'], 'product_id' => $_POST['product_id']], 'product_rating', 'images');
            $rating_images = $images_new_name_arr;
            if (isset($rating_data[0]['images']) && isset($rating_data) && !empty($rating_data[0]['images'])) {
                $existing_images = json_decode($rating_data[0]['images']);
                $rating_images = array_merge($existing_images, $images_new_name_arr);
            }

            $_POST['images'] = $rating_images;
            $this->rating_model->set_rating($_POST);
            $rating_data = $this->rating_model->fetch_rating((isset($_POST['product_id'])) ? $_POST['product_id'] : '', '', '25', '0', 'id', 'DESC');
            $rating['product_rating'] = $rating_data['product_rating'];
            $rating['no_of_rating'] = $rating_data['rating'][0]['no_of_rating'];
            $this->response['error'] = false;
            $this->response['message'] = 'Product Rated Successfully';
            $this->response['data'] = $rating;
            echo json_encode($this->response);
            return;
        }
    }

    public function delete_rating()
    {
        if (!$this->ion_auth->logged_in()) {
            return false;
        }
        $this->form_validation->set_rules('rating_id', 'Rating Id', 'trim|numeric|required|xss_clean');
        if (!$this->form_validation->run()) {
            $this->response['error'] = true;
            $this->response['message'] = validation_errors();
            $this->response['data'] = array();
            echo json_encode($this->response);
            return false;
        } else {
            $rating_data = fetch_details(['id' => $_POST['rating_id']], 'product_rating');
            if (empty($rating_data)) {
                $this->response['error'] = true;
                $this->response['message'] = 'Invalid Rating ID.';
                echo json_encode($this->response);
                return false;
            }
            $rating_data = $rating_data[0];
            if ($rating_data['user_id'] != $this->data['user']->id) {
                $this->response['error'] = true;
                $this->response['message'] = 'You are not authorised to delete this rating.';
                echo json_encode($this->response);
                return false;
            }
            $this->rating_model->delete_rating($_POST['rating_id']);
            $data = $this->rating_model->fetch_rating($rating_data['product_id']);
            $this->response['error'] = false;
            $this->response['message'] = 'Deleted Rating Successfully';
            $this->response['data'] = $data;
            echo json_encode($this->response);
        }
    }
    public function get_rating()
    {
        $this->form_validation->set_data($_GET);
        $this->form_validation->set_rules('limit', 'Limit', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('offset', 'Offset', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('product_id', 'Product', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('user_id', 'User', 'trim|numeric|xss_clean');
        if (!empty($_GET) && !$this->form_validation->run()) {
            $this->response['error'] = true;
            $this->response['message'] = validation_errors();
            $this->response['data'] = array();
            echo json_encode($this->response);
            return false;
        }

        $product_id = (isset($_GET['product_id'])) ? $_GET['product_id'] : null;
        $user_id = (isset($_GET['user_id'])) ? $_GET['user_id'] : null;
        $limit = (isset($_GET['limit'])) ? $_GET['limit'] : 12;
        $offset = (isset($_GET['offset'])) ? $_GET['offset'] : 0;
        $sort = (isset($_GET['sort'])) ? $_GET['sort'] : 'id';
        $order = (isset($_GET['order'])) ? $_GET['order'] : 'DESC';

        $data = $this->rating_model->fetch_rating($product_id, $user_id, $limit, $offset, $sort, $order);
        if (empty($data)) {
            $this->response['error'] = true;
            $this->response['message'] = 'No more reviews found.';
            $this->response['data'] = $data;
            echo json_encode($this->response);
            return false;
        }
        $this->response['error'] = false;
        $this->response['message'] = 'Ratings retrieved Successfully';
        $this->response['data'] = $data;
        echo json_encode($this->response);
        return false;
    }

public function product_quickview($product_id='',$product_variant_id=''){
        $lang_prefix = $this->data['lang_prefix'];
        $output = '';
       
           $output .= '<div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
           <div class="modal-center">
               <div class="modal-dialog .modal-align-center modal-lg">
                   <div class="modal-content">
                       <div class="modal-body quick-view-area">
                           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">'.$this->lang->line('close').'</span>
       
                           </button>';
        $t = &get_instance();
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
      //  $slug = urldecode($slug);
      if($product_variant_id!='')
        $product = fetch_product($user_id,array('selected_variant_id' =>$product_variant_id),$product_id);
        else
        $product = fetch_product($user_id,NULL,$product_id);
        if (empty($product)) {
            redirect(base_url('products'));
        }
		$brands =  $t->db->select(' * ')->where('id', $product['product'][0]['brand'])->get('brands')->result_array();
        $this->data['product'] = $product;
		$rescat = $t->db->select(' * ')->where('id', $product['product'][0]['category_id'])->get('categories')->result_array();
        
      
      
		$catname = ($lang_prefix =='') ? $rescat[0]['name'] : $rescat[0]['ar_name'];
        $ar_catname = $rescat[0]['ar_name'];
		$brand=($lang_prefix =='') ? $brands[0]['name'] : $brands[0]['ar_name'];
        $ar_brand=$brands[0]['ar_name'];
		$brandslug=$brands[0]['slug'];
		$catslug = $rescat[0]['slug'];
       $total_images =0; 
       $settings = $this->data['settings'];
        $this->data['username'] = $this->session->userdata('username');
        $stockupdate=0;
        $price_display  ='';
        $d_variant_ids = [];
        $sku = '';
        //print_r($product['product'][0]['default_product']);
        //print_r($product['product'][0]);
        if(isset($product['product'][0]['default_product'])){
            if($product['product'][0]['default_product']->images !=''){
                $image = json_decode($product['product'][0]['default_product']->images);
            //echo $image[0];
            $main_image =isset($image[0]) ? get_image_url($image[0]) : get_image_url($product['product'][0]['image']);
            }
            else{
                $main_image =get_image_url($product['product'][0]['image']);
            }
            $m = $lang_prefix.'variant_values'; 
        $product_name = $product['product'][0][$lang_prefix.'name'].' '.$product['product'][0]['default_product']->$m;
        
            if($product['product'][0]['type'] =='variable_product'){
                if($product['product'][0]['stock_type']==NULL){
                    if($product['product'][0]['stock']!=null && intval($product['product'][0]['stock']) <=0)
                    $stockupdate=1;
                
                else{
                    $stockupdate=0;
                }
                }
                elseif($product['product'][0]['stock_type']=='2'){
                    if($product['product'][0]['default_product']->stock!=NULL && intval($product['product'][0]['default_product']->stock) <=0){
                        $stockupdate=1;
                    }
                    else{
                        $stockupdate=0;
                    }
                }
                
                
            }
            else
            {
                if($product['product'][0]['stock_type'] ==NULL){
            
                    $stockupdate=0;
                }
                else{
                    if($product['product'][0]['stock']!=NULL && intval($product['product'][0]['stock']) <=0){
                        $stockupdate=1;
                    }
                    else{
                        $stockupdate=0;
                    }
                }
            } 



            // if($product['product'][0]['default_product']->stock !=NULL && intval($product['product'][0]['default_product']->stock) <=0 ){
            //     $stockupdate=1;
            // }
            // else{
            //     if($product['product'][0]['stock']!=NULL && intval($product['product'][0]['stock']) <=0){
            //         $stockupdate=1;
            //     }
            // }
            $d_attr_name = $lang_prefix.'attr_name';
            $d_selected_att = isset($product['product'][0]['default_product']->$d_attr_name) ? explode(',' ,$product['product'][0]['default_product']->$d_attr_name) :'';
            $d_variant_values = $lang_prefix.'variant_values';
            $d_selected_variants = isset($product['product'][0]['default_product']->$d_variant_values) ? explode(',' ,$product['product'][0]['default_product']->$d_variant_values) : '';
            $select_details =[];
            if(isset($product['product'][0]['default_product']->$d_variant_values) && isset($d_selected_variants)){
                foreach($d_selected_variants as $key =>$v){
                    $select_details[$key] = isset($d_selected_att[$key]) ? $d_selected_att[$key].': '.$v : '';
                }         
            }
            $select_details = implode(' ',$select_details);

            $price_display = '<div class="product-price">';
            if($product['product'][0]['default_product']->special_price > 0 && $product['product'][0]['default_product']->special_price< $product['product'][0]['default_product']->price ){
                $price_display .= '<span id="modal-price" >'.$settings['currency'] .number_format($product['product'][0]['default_product']->special_price,3) .'</span><span class="previous-price" id="modal-striped-price" >'. $settings['currency'] . number_format($product['product'][0]['default_product']->price,3).'</span>';
            }
            else{    
                $price_display .= '<span id="modal-price" >'.$settings['currency'] . number_format($product['product'][0]['default_product']->price,3) .'</span>';	 
            } 
            $price_display .= '</div>';
            $d_variant_ids = explode(',',$product['product'][0]['default_product']->variant_ids);
            $variant_id = $product['product'][0]['default_product']->id;
            $sku = isset($product['product'][0]['default_product']->sku) ? $product['product'][0]['default_product']->sku : $product['product'][0]['variants'][0]['sku'] ;
            if( $product['product'][0]['default_product']->is_favorite == 1) 
                $is_fav =' text-danger' ;
            else
                $is_fav =  '' ;
        }
        else{
            $main_image =$product['product'][0]['image'];
            $product_name = $product['product'][0][$lang_prefix.'name'];
            if (($product['product'][0]['stock_type'] != null && $product['product'][0]['stock_type'] != '')) {
                //Case 1 : Simple Product(simple product)
                    if ($product['product'][0]['stock_type'] == 0) {
                        if ($product['product'][0]['stock'] != null) {
                            $stock = intval($product['product'][0]['stock']);
                            if ($stock <= 0) {
                                $stockupdate=1;
                            }
                        }
                    }           
                }
                $select_details = '';
                $price = get_price_range_of_product($product['product'][0]['id']);
                if ($product['product'][0]['type'] == "simple_product") { 
                            
                    if($product['product'][0]['min_max_price']['special_price']!=0)
                    {
                    
                $price_display = '<div class="product-price">
                    <span id="price" >'.$settings['currency'] .number_format($product['product'][0]['min_max_price']['special_price'],3) .'</span>
                    <span class="previous-price" id="striped-price" >
                '.$settings['currency'].number_format($product['product'][0]['min_max_price']['min_price'],3).'
                    </span>
                    </div>';

                    
                    }
                    else{
                        $price_display = '<div class="product-price">
                    <span id="price" >'.$settings['currency'] . number_format($product['product'][0]['min_max_price']['min_price'],3) .'</span>
                    <span class="previous-price" id="striped-price" ></span>
                    </div>';
                    }
                    }
                else
                {
                    
                    $price_display = '<div class="product-price">
                    <span  id="price">  '.$price['range'].'</span>
                    <span class="previous-price" id="striped-price" ></span>
                    </div>';
                
                }
                if (count($product['product'][0]['variants']) <= 1) {
                    $variant_id = $product['product'][0]['variants'][0]['id'];
                } else {
                    $variant_id = "";
                }
                $sku = isset($product['product'][0]['variants'][0]['sku'] ) ? $product['product'][0]['variants'][0]['sku'] : '';
                    
        }
            
        $output .= '<section class="product-details-wrap">
       
        <div class="product-details-top">
        <div class="product-details-top-inner">
        

            <div class="product-image-gallery">
                <div class="app-figure" id="zoom-fig">
                    <a id="Zoom-1" class="MagicZoom"
                        title="Show your product in stunning detail with Magic Zoom Plus."
                        href="'.$main_image.'"
                        data-zoom-image-2x="'.$main_image.'">
                        <img src="'.$main_image.'" alt="" />
                    </a>
                    <div class="selectors">
                        <a data-zoom-id="Zoom-1" href="'.$main_image.'?h=1400"
                            data-image="'.$main_image.'?h=400"
                            data-zoom-image-2x="'.$main_image .'?h=2800"
                            data-image-2x="'.$main_image.'?h=800">
                            <img srcset="'.$main_image.'" />
                        </a>';
                       
            if (!empty($product['product'][0]['other_images']) && isset($product['product'][0]['other_images'])) {
                foreach ($product['product'][0]['other_images'] as $other_image) {
                    if($other_image!=base_url().'/assets/no-image.png')
                    {
                         $total_images++;
            
                        $output .= '<a data-zoom-id="Zoom-1" href="'.$other_image.'?h=1400"
                            data-image="'.$other_image.'?h=400"
                            data-zoom-image-2x="'.$other_image.'?h=2800"
                            data-image-2x="'.$other_image.'?h=800">
                            <img srcset="'.$other_image.'" />
                        </a>';
                         } }
            } 
                       
      $counting=$total_images;

            $variant_images_md = array_column($product['product'][0]['variants'], 'images_md','id');
            if (!empty($variant_images_md)) {
               
                foreach ($variant_images_md as $ikey =>$variant_images) {
                    if (!empty($variant_images)) {
                        $ik=0;
                        foreach ($variant_images as $image) {
                            if($image!=base_url().'/assets/no-image.png')
                            {
            
                                $output .= '<a data-zoom-id="Zoom-1" href="'.$image.'?h=1400" data-image="'.$image.'?h=400"
                            data-zoom-image-2x="'.$image.'?h=2800" data-image-2x="'.$image.'?h=800">
                            <img id="mimg_'.$ikey.'_'.$ik++.'" srcset="'.$image.'" />
                        </a>';
                        }  }
                    }
                    $counting++;
                }
            } 

            $output .= ' </div>
                </div>
            </div>';

            $output .= '<div class="product-details-info">

                <div class="detail-info">
                    <h2 class="title-detail" id="modal-title-detail">
                        '.$product_name .'
                    </h2>
                    <input type="hidden" id="modal_product_name_display" value="'.$product['product'][0][$lang_prefix.'name'] .'">
                    <div class="a-row">'.$select_details .'
                                </div>';
                  
                        $output .= '<div class="availability in-stock">';
                      
                        if($stockupdate==1){
                            $output .=  !empty($this->lang->line('out_of_stock')) ? $this->lang->line('out_of_stock') : 'Out of Stock' ;
                        }
                        else{
                            $output .=  !empty($this->lang->line('in_stock')) ? $this->lang->line('in_stock') : 'In Stock' ;
                        }
                        $output .= ' </div>
                    <div class="details-info-middlemb-3">'.$price_display. '</div>';


                                        $output .= '<form>';
                                        
                         if (isset($product['product'][0]['variant_attributes']) && !empty($product['product'][0]['variant_attributes'])) { 
                        foreach ($product['product'][0]['variant_attributes'] as $attribute) {
                            $attribute_values = explode(',', $attribute['values']);
                            $attribute_ids = explode(',', $attribute['ids']);
                            $ar_attribute_values = explode(',', $attribute['ar_values']);
                           
            // $attribute_set = $this->product_model->get_attribute_set($attribute['attr_name']); print_r($attribute_set);exit;
       
                         if($attribute['attr_name']=='colour') { 
                            $output .= '<div class="product_variant color">
                            <h2>'.$this->lang->line('select').' '.$attribute[$lang_prefix.'attr_name'] .'
                        </h2>
                            <div class="custom-radios2">';
                                $counting=$total_images+1; 
                                foreach ($attribute_values as $key => $row) { 
                                    $attVal = ($lang_prefix =='') ? $row : $ar_attribute_values[$key];
                                    if(in_array($attribute_ids[$key],$d_variant_ids))
                                    $d_checked =  'checked';
                                    else
                                    $d_checked ='';
                                    $output .= '<div>
                                    <input type="radio" class="modal-product-attributes colorradio"
                                        data-step="'.$counting.'" id="color-'.$attribute_ids[$key].'"
                                        name="'. $attribute['attr_name'].'"  data-name="'.$attribute[$lang_prefix.'attr_name'].'" data-name-value="'.$attVal.'"
                                        value="'.$attribute_ids[$key].'" '.$d_checked.'>
                                    <label for="color-'.$attribute_ids[$key] .'" 
                                        style="background-color: '.$row .';">
                                        <span>
                                        </span>
                                    </label>
                                </div>';
                                 $counting++; } 
                                 $output .= '</div>
                        </div>';
                     
                        
                         } 
                        elseif($attribute['attr_name']=='RAM'){
                            
                            $output .= ' <div class="product_variant color">
                            <h2>'.$this->lang->line('select').' '.$attribute[$lang_prefix.'attr_name'] .'
                        </h2>
                            <div class="custom-radios customradio3">';
                               $counting=$total_images+1; 
                                foreach ($attribute_values as $key => $row) { 
                                    $attVal = ($lang_prefix =='') ? $row : $ar_attribute_values[$key];
                                    if(in_array($attribute_ids[$key],$d_variant_ids))
                                    $d_checked =  'checked';
                                    else
                                    $d_checked ='';
                                    $output .= ' <div>
                                    <input type="radio" class="modal-product-attributes colorradio"
                                        data-step="'.$counting.'" id="color-'. $attribute_ids[$key].'"
                                        name="'.$attribute['attr_name'].'"  data-name="'. $attribute[$lang_prefix.'attr_name'].'" data-name-value="'.$attVal.'"
                                        value="'.$attribute_ids[$key] .'" '.$d_checked.'>
                                    <label for="color-'. $attribute_ids[$key].'"
                                        style="background-color: '.$row.';">
                                        <span>
                                        '.$attVal .'
                                        </span>
                                    </label>
                                </div>';
                                $counting++; } 
                                $output .= '</div>
                        </div>';
                     
                       }
                        else { 
                     
                            $output .= '  <div class="product_variant color">
                            <h2>'.$this->lang->line('select').' '.$attribute[$lang_prefix.'attr_name'] .'
                        </h2>
                            <div class="custom-radios customradio3">';
                                 $counting=$total_images+1; 
                                foreach ($attribute_values as $key => $row) { 
                                    $attVal = ($lang_prefix =='') ? $row : $ar_attribute_values[$key];
                                    if(in_array($attribute_ids[$key],$d_variant_ids))
                                    $d_checked =  'checked';
                                    else
                                    $d_checked ='';
                                    $output .= '  <div>
                                    <input type="radio" class="modal-product-attributes colorradio"
                                        data-step="'.$counting.'" id="color-'.$attribute_ids[$key].'"
                                        name="'.$attribute['attr_name'].'"  data-name="'. $attribute[$lang_prefix.'attr_name'].'" data-name-value="'.$attVal.'"
                                        value="'.$attribute_ids[$key].'" '.$d_checked.'>
                                    <label for="color-'.$attribute_ids[$key].'"
                                        style="background-color: '.$row .';">
                                        <span>
                                       '.$attVal.'
                                        </span>
                                    </label>
                                </div>';
                                $counting++; } 
                                $output .= '  </div>
                        </div>';


                         } 


                           }
             } 
                         if (!empty($product['product'][0]['variants']) && isset($product['product'][0]['variants'])) {
        foreach ($product['product'][0]['variants'] as $variant) {
         if($variant['variant_ids']!='')
            $output .= ' <input type="hidden" class="modal-product-variants" name="variants_ids"
                            data-image-index="'.$variant['id'].'" data-name=""
                            value="'.$variant['variant_ids'].'" data-id="'. $variant['id'].'"
                            data-price="'.$variant['price'].'" data-sku="'.$variant['sku'].'"
                            data-special_price="'.$variant['special_price'].'"
                            data-stock="'.$variant['stock'].'" />';
                       
            $total_images += count($variant['images']);
        }
        }
    
   

        $output .= ' <div class="details-info-middle-actions">

                            <div class="number-picker">
                                <div class="info-qty">
                                    <a id="qtyDown" class="qtydown" href="javascript:void(0);"><i class="fa fa-minus"></i></a>
                                    <span class="qty-val">1</span>
                                    <a id="qtyUp" class="qtyup" href="javascript:void(0);"><i class="fa fa-plus"></i></a>
                                    <input type="hidden" name="qty" id="mqty" value="1">
                                </div>
                            </div>';
                          
                                if($stockupdate==1) 
                                    $s_disable =  'disabled' ;
                                else
                                $s_disable = '';
                            $output .= ' <button type="button" class="btn btn-primary btn-add-to-cart add-to-cart" id="modal-add_cart" data-product-id="'.$product['product'][0]['id'].'" data-product-variant-id="'.$variant_id .'" '.$s_disable .' ><i class="fa fa-cart-arrow-down"></i>'.$this->lang->line('add_to_cart').'</button>';
                            $fawish = ($product['product'][0]['is_favorite'] == 1) ? ' text-danger' : '';
                            $output .= '<div class="details-info-top-actions"><button id="modal-fav" class="btn btn-wishlist add-to-fav-btn '. $fawish.'" data-product-id="'.$product['product'][0]['id'] .'" data-product-variant-id="'.$variant_id .'"  data-hint="'.$this->lang->line('wishlist').'"><i class="fa fa-heart"></i>
                                    '.$this->lang->line('wishlist').'
                            </div>
                        </div>

                        <br><br>';

          

                        $output .= '<div class="brief-description">

                            <p>
                                '.$product['product'][0][$lang_prefix.'short_description'] .'
                            </p>
                        </div>';

                       
                        $output .= ' <div class="details-info-bottom">
                            <ul class="list-inline additional-info">
                                <li class="sku"><label>'.$this->lang->line('sku').':</label>
                                    <span id="modal-sku">'. $sku .'</span>
                                </li>
                                <li><label>'.$this->lang->line('categories').':</label> <a
                                        href="'.base_url('products/category/' . $catslug).'">
                                       '.$catname.'
                                    </a></li>
                                <li class="brand"><label>'.$this->lang->line('brands').': </label> <a
                                        href="'.base_url('products/brands/' . $brandslug).'">
                                       '.$brand.'
                                    </a></li>
                                <li><label>'.$this->lang->line('tags').':</label>
                                <a href="#"> '.implode(',',$product['product'][0][$lang_prefix.'tags']).'</a>
                                   
                                </li>
                            </ul>
                            <div class="social-share"><label>'.$this->lang->line('share').':</label>
                                <ul class="list-inline social-links">
                                    <li>
                                        <a href="#" title="Facebook" target="_blank"><i
                                                class="fab fa-facebook-f"></i></a>
                                    </li>
                                    <li><a href="#" title="Twitter" target="_blank"><i
                                                class="fab fa-twitter"></i></a></li>
                                    <li>
                                        <a href="#" title="Linkedin" target="_blank"><i
                                                class="fab fa-linkedin"></i></a>
                                    </li>
                                    <li><a href="#" title="Tumblr" target="_blank"><i
                                                class="fab fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>

                    </form>
                </div>

                <!-- End Attr Info -->
            </div>
            </div>



            </div>
       
                           </section>
       
                           </div>
       
                           </div>
                       </div>
                   </div>
               </div>';
               $this->response['html'] = $output;
               print_r(json_encode($this->response));
          
              return false;
    }

    function load_more_products_grid() {
        $filter = array();
        $offset= ($this->input->post('offset')) ? $this->input->post('offset', true) : 12;
        $sort_by = ($this->input->post('sort')) ? $this->input->post('sort', true) : '';
		$brandid = ($this->input->post('brand')) ? explode(',',$this->input->post('brand', true)) : '';
        $min_price = ($this->input->post('min_price')) ? $this->input->post('min_price', true) : '';
        $max_price = ($this->input->post('max_price')) ? $this->input->post('max_price', true) : '';
        $attribute_values = ($this->input->post('attributes')) ? $this->input->post('attributes', true) : '';
        $url = ($this->input->post('url')) ? $this->input->post('url', true) : '';
        $q = ($this->input->get('q')) ? $this->input->get('q', true) : '';
        $filter['search'] =  $this->input->get('q', true);
        $category_id ='';
        //check url contain category/brand/search
        $url = explode('?',$url);
        $url_first_part = $url[0];
        $url_query_part = isset($url[1]) ? $url[1] : '';
        $url_segments = explode(base_url('products/'),$url_first_part);
        $url_parts = isset($url_segments[1]) ? explode('/',$url_segments[1]) : '';
        if($url_parts!=''){
            $url_part1 = isset($url_parts[0]) ? $url_parts[0] : '';
            $url_part2 = isset($url_parts[1]) ? $url_parts[1] : '';
            if($url_part1 == 'search'){
                $search = isset($url_query_part) ? explode('q=',$url_query_part) :'';
                if(isset($search[1]))
                    $filter['search'] =  $search[1];
                    else
                    $filter['search'] ='';
            }
            elseif($url_part1 == 'category'){
                $category_slug = $url_part2;
                $category_id = get_category_id_by_slug($category_slug);
            }
            elseif($url_part1 == 'brands'){
                $brand_slug = $url_part2;
                $brand_id = get_brand_id_by_slug($brand_slug);
                $brandid =[$brand_id]; 
            }
        }
        if($attribute_values!='')
            $filter['attribute_value_ids'] = $attribute_values;
        else
        $filter['attribute_value_ids'] = '';
       
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
        //Product Sorting
        $sort = '';
        $order = '';
      
        if ($sort_by == "top-rated") {
            $filter['product_type'] = "top_rated_product_including_all_products";
        } elseif ($sort_by == "date-desc") {
            $sort = 'pv.date_added';
            $order = 'desc';
        } elseif ($sort_by == "date-asc") {
            $sort = 'pv.date_added';
            $order = 'asc';
        } elseif ($sort_by == "price-asc") {
            $sort = 'price';
            $order = 'asc';
        } elseif ($sort_by == "price-desc") {
            $sort = 'price';
            $order = 'desc';
        }
        $this->data['filter_min_price'] =0; $this->data['filter_max_price'] =500;
        if($min_price >0){
            $filter['min_price'] = $min_price;
            $this->data['filter_min_price'] = $min_price;
        }
        if($max_price >0){
            $filter['max_price'] = $max_price;
            $this->data['filter_max_price'] = $max_price;
        }
     
        $limit = 12;
        $products = fetch_product_variants($user_id, $filter, null, $category_id, $limit, $offset, $sort, $order,false,$brandid);
        //get all product id 
       $output ='';
       $lang_prefix = $this->data['lang_prefix'];
       $settings =  $this->data['settings'];
       if (isset($products) && !empty($products['product'])) { 
      foreach ($products['product'] as $each) { 
        $product_name = $each[$lang_prefix.'name'];
        $product_slug =  '';
        $main_image =$each['image'];
        $product_id = $each['id'];
        $m = $lang_prefix.'variant_values'; 
      
        $product_stock = $each['stock'];

            $product_slug =  base_url('products/details/' . $each['slug']);
            if($each['special_price'] > 0 && $each['special_price'] < $each['price']){
                $discount_in_percentage = find_discount_in_percentage($each['special_price'], $each['price']);
            }
            else{
                $discount_in_percentage = 0;
            }
            if($each['type'] =='variable_product'){
                if($each['stock_type']==NULL){
                    if($product_stock!=null && intval($product_stock) <=0)
                    $stockupdate=1;
                   
                   else{
                       $stockupdate=0;
                   }
                }
                elseif($each['stock_type']=='2'){
                    if($each['variant_stock']!=NULL && intval($each['variant_stock']) <=0){
                        $stockupdate=1;
                    }
                    else{
                        $stockupdate=0;
                    }
                }
                
                
            }
            else
            {
                if($each['stock_type'] ==NULL){
              
                    $stockupdate=0;
                }
                else{
                    if($each['stock']!=NULL && intval($each['stock']) <=0){
                        $stockupdate=1;
                    }
                    else{
                        $stockupdate=0;
                    }
                }
            } 
           
            $image = json_decode($each['variant_images']);
            if(isset($image[0]) && $image[0] !=''){                                           
               $sm_image = get_image_url($image[0],'thumb', 'sm');
            }
            else{
                $sm_image =get_image_url($main_image,'thumb', 'sm');
            }
            $variant_id =$each['variant_id'] ;
            $modal = "";
            $class="add-to-cart";
            if ($this->data['is_logged_in']) {
                $is_favorite = is_exist(['user_id' => $this->data['user']->id,
                'product_id' => $product_id,
                'product_variant_id' => $variant_id],'favorites');
                if($is_favorite)
                    $is_favorite =1;
                else
                $is_favorite =0;
                }
                else
                $is_favorite =0;
            $product_slug .='/'.$variant_id;
                        
            $output .=' <div class="col item-display-grid">
               <div class="item">
                
                   <div class="item-hot-deal-product">
                       <div class="hot-deal-product-thumb">';
                      if($stockupdate==1) { 
                        $output .='<div class="outofstock">
                         <strong> '. $this->lang->line('out_of_stock') .'</strong>
                         </div>';
                       }
                       if($discount_in_percentage!='' && $discount_in_percentage>0 && $discount_in_percentage < 100){
  
                        $output .='<div class="cat-hover-percent">
                               <strong>'.$discount_in_percentage.' % </strong></div>';
                         } 
                         $output .='<div class="product-thumb">
                               <a class="product-thumb-link" href="'.$product_slug.'">
                                   <img alt="'.$product_name.'" src="'.$sm_image.'" class="first-thumb">

                               </a>
                               <div class="product2-buttons">
                               <a href="'.base_url('products/product_quickview/'.$product_id.'/'.$variant_id).'"
                   class="product-button hintT-top manual-ajax"
                   rel="modal:open" title="'. $this->lang->line('quick_view').'" data-hint="'.$this->lang->line('quick_view').'"><i
                       class="fa fa-search"></i></a>';
                   
                 
                       $output .=' <a href="#"  data-hint="'. $this->lang->line('add_to_cart').'" class="'. $class.' product-button hintT-top" data-product-id="'.$product_id.'"  data-product-qty="1" data-product-variant-id="'.$variant_id.'" data-izimodal-open="'.$modal.'" title="'.$this->lang->line('add_to_cart').'" ><i class="fa fa-shopping-cart"></i></a>';
               
                   
                    if($is_favorite == 1)
                     $is_fav = ' text-danger' ;
                    else
                    $is_fav ='';

                   $output .='  <a href="javascript:void(0);" data-product-id="'.$product_id.'" data-product-variant-id="'.$variant_id.'" title="'. $this->lang->line('add_to_wishlist').'" class="product-button hintT-top wishlist add-to-fav-btn lnr" data-hint="'.$this->lang->line('wishlist').'"><i class="fa fa-heart '.$is_fav.'"></i></a>
                                  
                               </div>
                           </div>
                       </div>
                       <div class="hot-deal-product-info">
                           <h3 class="title-product"><a href="'.$product_slug.'">'.$product_name.' '.$each[$m].'</a></h3>';
                        
               
                           $output .=' <div class="info-price">';
                           $output .='  <span>  ';              
                           if($each['special_price'] > 0 && $each['special_price']< $each['price']){
                            $output .='  <span >'.$settings['currency'].' '.number_format($each['special_price'],3).'</span><del>'.$settings['currency'] .' '. number_format($each['price'],3).'</del>';

                           }
                           else{
                               
                            $output .='<span>'.$settings['currency'] .' '.number_format($each['price'],3) .'</span>	 ';
                          } 
                          $output .='</span>';
                
               $output .='</div>
                          
                       </div>
                   </div>
               </div>
           </div>';
          }
       }
       $this->response['offset'] = isset($products['product']) ? count($products['product']) : 0;
      
       $this->response['error'] = false;
                $this->response['message'] = 'success';
                $this->response['data'] = $output;
                print_r(json_encode($this->response));
                return false;
    }

    function load_more_products_list() {
        $filter = array();
        $offset= ($this->input->post('offset')) ? $this->input->post('offset', true) : 12;
        $sort_by = ($this->input->post('sort')) ? $this->input->post('sort', true) : '';
		$brandid = ($this->input->post('brand')) ? explode(',',$this->input->post('brand', true)) : '';
        $min_price = ($this->input->post('min_price')) ? $this->input->post('min_price', true) : '';
        $max_price = ($this->input->post('max_price')) ? $this->input->post('max_price', true) : '';
        $attribute_values = ($this->input->post('attributes')) ? $this->input->post('attributes', true) : '';
        $url = ($this->input->post('url')) ? $this->input->post('url', true) : '';
        $q = ($this->input->get('q')) ? $this->input->get('q', true) : '';
        $filter['search'] =  $this->input->get('q', true);
        $category_id ='';
        //check url contain category/brand/search
        $url = explode('?',$url);
        $url_first_part = $url[0];
        $url_query_part = isset($url[1]) ? $url[1] : '';
        $url_segments = explode(base_url('products/'),$url_first_part);
        $url_parts = isset($url_segments[1]) ? explode('/',$url_segments[1]) : '';
        if($url_parts!=''){
            $url_part1 = isset($url_parts[0]) ? $url_parts[0] : '';
            $url_part2 = isset($url_parts[1]) ? $url_parts[1] : '';
            if($url_part1 == 'search'){
                $search = isset($url_query_part) ? explode('q=',$url_query_part) :'';
                if(isset($search[1]))
                    $filter['search'] =  $search[1];
                    else
                    $filter['search'] ='';
            }
            elseif($url_part1 == 'category'){
                $category_slug = $url_part2;
                $category_id = get_category_id_by_slug($category_slug);
            }
            elseif($url_part1 == 'brands'){
                $brand_slug = $url_part2;
                $brand_id = get_brand_id_by_slug($brand_slug);
                $brandid =[$brand_id]; 
            }
        }
        if($attribute_values!='')
            $filter['attribute_value_ids'] = $attribute_values;
        else
        $filter['attribute_value_ids'] = '';
       
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
        //Product Sorting
        $sort = '';
        $order = '';
      
        if ($sort_by == "top-rated") {
            $filter['product_type'] = "top_rated_product_including_all_products";
        } elseif ($sort_by == "date-desc") {
            $sort = 'pv.date_added';
            $order = 'desc';
        } elseif ($sort_by == "date-asc") {
            $sort = 'pv.date_added';
            $order = 'asc';
        } elseif ($sort_by == "price-asc") {
            $sort = 'price';
            $order = 'asc';
        } elseif ($sort_by == "price-desc") {
            $sort = 'price';
            $order = 'desc';
        }
        $this->data['filter_min_price'] =0; $this->data['filter_max_price'] =500;
        if($min_price >0){
            $filter['min_price'] = $min_price;
            $this->data['filter_min_price'] = $min_price;
        }
        if($max_price >0){
            $filter['max_price'] = $max_price;
            $this->data['filter_max_price'] = $max_price;
        }
       
        $limit = 12;
        $products = fetch_product_variants($user_id, $filter, null, $category_id, $limit, $offset, $sort, $order,false,$brandid);
        //get all product id 
       $output ='';
       $lang_prefix = $this->data['lang_prefix'];
       $settings =  $this->data['settings'];
       if (isset($products) && !empty($products['product'])) {
        foreach ($products['product'] as $each) {  
            $product_name = $each[$lang_prefix.'name'];
            $product_slug =  '';
            $main_image =$each['image'];
            $product_id = $each['id'];
            $m = $lang_prefix.'variant_values';                                    
            $product_stock = $each['stock'];                                
                $product_slug =  base_url('products/details/' . $each['slug']);
                if($each['special_price'] > 0 && $each['special_price'] < $each['price']){
                    $discount_in_percentage = find_discount_in_percentage($each['special_price'], $each['price']);
                }
                else{
                    $discount_in_percentage = 0;
                }
                if($each['type'] =='variable_product'){
                    if($each['stock_type']==NULL){
                        if($product_stock!=null && intval($product_stock) <=0)
                        $stockupdate=1;
                       
                       else{
                           $stockupdate=0;
                       }
                    }
                    elseif($each['stock_type']=='2'){
                        if($each['variant_stock']!=NULL && intval($each['variant_stock']) <=0){
                            $stockupdate=1;
                        }
                        else{
                            $stockupdate=0;
                        }
                    }
                    
                    
                }
                else
                {
                    if($each['stock_type'] ==NULL){
                  
                        $stockupdate=0;
                    }
                    else{
                        if($each['stock']!=NULL && intval($each['stock']) <=0){
                            $stockupdate=1;
                        }
                        else{
                            $stockupdate=0;
                        }
                    }
                } 
               
                $image = $each['variant_images'];
                if(isset($image[0]) && $image[0] !=''){                                           
                   $sm_image = get_image_url($image[0], 'thumb', 'sm');
                }
                else{
                    $sm_image = get_image_url($main_image,'thumb', 'sm');
                }
                $variant_id =$each['variant_id'] ;
                $modal = "";
                $class="add-to-cart";
                if ($this->data['is_logged_in']) {
                    $is_favorite = is_exist(['user_id' => $this->data['user']->id,
                    'product_id' => $product_id,
                    'product_variant_id' => $variant_id],'favorites');
                    if($is_favorite)
                        $is_favorite =1;
                    else
                    $is_favorite =0;
                    }
                    else
                    $is_favorite =0;
                $product_slug .='/'.$variant_id;
                      

                $output .='<div class="list-product-card item-display-list">
                <div class="list-product-card-inner">';
                if($stockupdate==1) {
                    $output .='<div class="label_product">
                            <span class="label_sale">'.$this->lang->line('out_of_stock').' </span>
                        </div>';
                } 
                if($discount_in_percentage!='' && $discount_in_percentage>0 && $discount_in_percentage < 100){
                    $output .='<div class="cat-hover-percent">
                    <strong>'.$discount_in_percentage.' % </strong>
                  
                </div>';
                }
                $output .=' <div class="product-card-left">
                        <a href="'.$product_slug.'" class="product-image"> <img alt="'.$product_name.'" src="'.$sm_image .'" class="first-thumb">
                            <ul class="list-inline product-badge">
                                <!---->
                                <!---->
                            </ul>
                        </a>
                    </div>
                    <div class="product-card-right">
                        <a href="'.$product_slug.'" class="product-name">
                            <h6>'.$product_name.' '.$each[$m].'</h6>
                        </a>
                        <div class="clearfix"></div>
                        <div class="product-price">';
                 
                        if($each['special_price'] > 0 && $each['special_price']< $each['price']){
                            $output .= $settings['currency'].' '. number_format($each['special_price'],3).' <span class="previous-price">'.$settings['currency'] .' '.number_format($each['price'],3).'</span>';

                         }
                          else{
                              
                            $output .= $settings['currency'] .' '. number_format($v_each['price'],3);
                        } 
                 
                $output .='</div>';
                    
                      
                        $output .='<a href="#"  data-hint="'. $this->lang->line('add_to_cart') .'" class="'.$class.' product-button hintT-top btn btn-default btn-add-to-cart" data-product-id="'.$product_id.'"  data-product-qty="1" data-product-variant-id="'.$variant_id.'" data-izimodal-open="'.$modal.'" title="'.$this->lang->line('add_to_cart') .'" ><i class="fa fa-shopping-cart"></i> '.$this->lang->line('add_to_cart') .'</a>';
                                  
                    if( $is_favorite == 1) 
                        $is_fav =' text-danger' ;
                    else
                         $is_fav =  '' ;
                                    
                    $output .='<div class="product-card-actions">
                                    <button  data-product-id="'.$product_id.'" data-product-variant-id="'.$variant_id.'" class="btn btn-wishlist wishlist add-to-fav-btn" id="modal-fav"><i class="fa fa-heart '. $is_fav.'" data-hint="'.$this->lang->line('wishlist').'"></i>
                                   '.$this->lang->line('wishlist').'
                    </button>
                    <br>

                      
                     
                    <a href="'.base_url('products/product_quickview/'.$product_id.'/'.$variant_id) .'"
                    class="product-button hintT-top manual-ajax"
                    rel="modal:open" title="'. $this->lang->line('quick_view') .'" data-hint="'.$this->lang->line('quick_view').'"><i
                        class="fa fa-search"></i>'.$this->lang->line('quick_view').'</a>

                


                        </div>
                    </div>
                </div>






            </div>';
            }}


       
       $this->response['offset'] = isset($products['product']) ? count($products['product']) : 0;
      
       $this->response['error'] = false;
                $this->response['message'] = 'success';
                $this->response['data'] = $output;
                print_r(json_encode($this->response));
                return false;
    }

    function load_more_offer_products_grid() {
        $filter = array();
        $offset= ($this->input->post('offset')) ? $this->input->post('offset', true) : 12;
        // $sort_by = ($this->input->post('sort')) ? $this->input->post('sort', true) : '';
		// $brandid = ($this->input->post('brand')) ? explode(',',$this->input->post('brand', true)) : '';
        // $min_price = ($this->input->post('min_price')) ? $this->input->post('min_price', true) : '';
        // $max_price = ($this->input->post('max_price')) ? $this->input->post('max_price', true) : '';
        // $attribute_values = ($this->input->post('attributes')) ? $this->input->post('attributes', true) : '';
        $url = ($this->input->post('url')) ? $this->input->post('url', true) : '';
        // $q = ($this->input->get('q')) ? $this->input->get('q', true) : '';
        // $filter['search'] =  $this->input->get('q', true);
        // $category_id ='';
        //check url contain category/brand/search
        // $url = explode('?',$url);
        // $url_first_part = $url[0];
        // $url_query_part = isset($url[1]) ? $url[1] : '';
        // $url_segments = explode(base_url('products/'),$url_first_part);
        // $url_parts = isset($url_segments[1]) ? explode('/',$url_segments[1]) : '';
        // if($url_parts!=''){
        //     $url_part1 = isset($url_parts[0]) ? $url_parts[0] : '';
        //     $url_part2 = isset($url_parts[1]) ? $url_parts[1] : '';
        //     if($url_part1 == 'search'){
        //         $search = isset($url_query_part) ? explode('q=',$url_query_part) :'';
        //         if(isset($search[1]))
        //             $filter['search'] =  $search[1];
        //             else
        //             $filter['search'] ='';
        //     }
        //     elseif($url_part1 == 'category'){
        //         $category_id = get_category_id_by_slug($category_slug);
        //     }
        //     elseif($url_part1 == 'brands'){
        //         $brand_slug = $url_part2;
        //         $brand_id = get_brand_id_by_slug($brand_slug);
        //         $brandid =[$brand_id]; 
        //     }
        // }
        // if($attribute_values!='')
        //     $filter['attribute_value_ids'] = $attribute_values;
        // else
        // $filter['attribute_value_ids'] = '';
       
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
        //Product Sorting
        $user_id = NULL;
        if ($this->data['is_logged_in']) {
            $user_id = $this->data['user']->id;
        }
       
       
            $filter['product_type'] = "products_on_sale";
       
         
            $sort = 'pv.date_added';
            $order = 'desc';
     
        $limit = 15;
        $products = fetch_product_variants($user_id, $filter, null, null, $limit, $offset, $sort, $order);
        //get all product id 
       $output ='';
       $lang_prefix = $this->data['lang_prefix'];
       $settings =  $this->data['settings'];
       if (isset($products) && !empty($products['product'])) { 
      foreach ($products['product'] as $each) { 
        $product_name = $each[$lang_prefix.'name'];
        $product_slug =  '';
        $main_image =$each['image'];
        $product_id = $each['id'];
        $m = $lang_prefix.'variant_values'; 
      
        $product_stock = $each['stock'];

            $product_slug =  base_url('products/details/' . $each['slug']);
            if($each['special_price'] > 0 && $each['special_price'] < $each['price']){
                $discount_in_percentage = find_discount_in_percentage($each['special_price'], $each['price']);
            }
            else{
                $discount_in_percentage = 0;
            }
            if($each['type'] =='variable_product'){
                if($each['stock_type']==NULL){
                    if($product_stock!=null && intval($product_stock) <=0)
                    $stockupdate=1;
                   
                   else{
                       $stockupdate=0;
                   }
                }
                elseif($each['stock_type']=='2'){
                    if($each['variant_stock']!=NULL && intval($each['variant_stock']) <=0){
                        $stockupdate=1;
                    }
                    else{
                        $stockupdate=0;
                    }
                }
                
                
            }
            else
            {
                if($each['stock_type'] ==NULL){
              
                    $stockupdate=0;
                }
                else{
                    if($each['stock']!=NULL && intval($each['stock']) <=0){
                        $stockupdate=1;
                    }
                    else{
                        $stockupdate=0;
                    }
                }
            } 
           
            $image = json_decode($each['variant_images']);
            if(isset($image[0]) && $image[0] !=''){                                           
               $sm_image = get_image_url($image[0],'thumb', 'sm');
            }
            else{
                $sm_image =get_image_url($main_image,'thumb', 'sm');
            }
            $variant_id =$each['variant_id'] ;
            $modal = "";
            $class="add-to-cart";
            if ($this->data['is_logged_in']) {
                $is_favorite = is_exist(['user_id' => $this->data['user']->id,
                'product_id' => $product_id,
                'product_variant_id' => $variant_id],'favorites');
                if($is_favorite)
                    $is_favorite =1;
                else
                $is_favorite =0;
                }
                else
                $is_favorite =0;
            $product_slug .='/'.$variant_id;
                        
            $output .=' <div class="col-md-4 col-sm-4 col-xs-12 offer-item-product">
            <div class="item-deal-product ">';
                
                      if($stockupdate==1) { 
                        $output .='<div class="outofstock">
                         <strong> '. $this->lang->line('out_of_stock') .'</strong>
                         </div>';
                       }
                       if($discount_in_percentage!='' && $discount_in_percentage>0 && $discount_in_percentage < 100){
  
                        $output .='<div class="cat-hover-percent">
                               <strong>'.$discount_in_percentage.' % </strong></div>';
                         } 
                         $output .='<div class="product-thumb">
                               <a class="product-thumb-link" href="'.$product_slug.'">
                                   <img alt="'.$product_name.'" src="'.$sm_image.'" class="first-thumb">

                                   </a>
                                   </div>
                                   <div class="product-info">
                                   <h3 class="title-product"> <a href="'.$product_slug.'" class="product-name">
                                   '.$product_name .' '. $each[$m].'
                               </a></h3>
                               <div class="clearfix"></div>
                               <div class="info-price"><span>';
                                
                                                         
                               if($each['special_price'] > 0 && $each['special_price']< $each['price']){
                                $output .=$settings['currency'].' '.number_format($each['special_price'],3).'<span class="previous-price">'.$settings['currency'].' '. number_format($each['price'],3).'</span>';

                                }
                               else{
                                  
                                $output .=$settings['currency'] .' '.number_format($each['price'],3) ;
                              } 
                              $output .= '</span> </div>';
                              $output .= ' <a href="#"  data-hint="'.$this->lang->line('add_to_cart').'" class="'.$class.' product-button hintT-top btn btn-primary btn-add-to-cart" data-product-id="'.$product_id.'"  data-product-qty="1" data-product-variant-id="'.$variant_id.'" data-izimodal-open="'.$modal.'" title="add to cart" ><i class="fa fa-shopping-cart"></i> '.$this->lang->line('add_to_cart').'</a>
                                                 
                              <br>';
                              if($is_favorite == 1)
                              $is_fav = 'text-danger';
                              else
                              $is_fav = '';

                                             
                              $output .= '<div class="product-card-actions">
                                             <button  data-product-id="'.$product_id.'" data-product-variant-id="'.$variant_id.'"  class="btn  btn-default btn-wishlist wishlist add-to-fav-btn" data-hint="'.$this->lang->line('wishlist').'"><i class="fa fa-heart '.$is_fav.'"></i>
                                             '. $this->lang->line('wishlist') .'
                             </button>';


                               
                              
                             $output .= '<a href="'.base_url('products/product_quickview/'.$product_id.'/'.$variant_id).'"
                             class="btn btn-default  product-button hintT-top manual-ajax"
                             rel="modal:open" title="'.$this->lang->line('quick_view') .'" data-hint="'.$this->lang->line('quick_view').'"><i
                                 class="fa fa-search"></i> '.$this->lang->line('quick_view').'</a>

                         


                                 </div>
                             </div>
                         </div>




                         </div>

                     ';
          }
       }
       $this->response['offset'] = isset($products['product']) ? count($products['product']) : 0;
      
       $this->response['error'] = false;
                $this->response['message'] = 'success';
                $this->response['data'] = $output;
                print_r(json_encode($this->response));
                return false;
    }

}
