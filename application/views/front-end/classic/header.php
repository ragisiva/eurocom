 <?php
    $this->load->model('category_model');
    $categories = $this->category_model->get_categories(null, 30);
    $language = get_languages();
    $cookie_lang = $this->input->cookie('language', TRUE);
    $web_settings = get_settings('web_settings', true);
    ?>
  <header class="header_area header_padding">
        <!--header top start-->
        <div class="header_top top_two">
            <div class="container">
                <div class="top_inner">
                    <div class="row align-items-center">
                        <div class="col-lg-8 col-md-8">
                        
                            <div class="follow_us">
                                <label>Follow Us:</label>
                                <ul class="follow_link">
                                   
                                    <li><a href="<?= $web_settings['instagram_link'] ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                    <li><p>Free shipping for Kuwait on orders over KD 60.000</p></li>
                                
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="top_right text-right">
                                <ul>
								
                                    <li class="top_links"><a href="#"><i class="ion-android-person"></i> My Account<i class="ion-ios-arrow-down"></i></a>
                                        <ul class="dropdown_links">
                                            <li><a href="<?=base_url('Login')?>">Login </a></li>
                                            <li><a href="<?=base_url('My_account')?>">My Account </a></li>
                                        </ul>
                                    </li>
									<li><a href="<?=base_url('home/contact-us')?>"> Contact Us</a></li>
                                    <li class="language"><a href="#"><img src="<?=base_url('assets/img/logo/language.png')?>" alt="">English<i class="ion-ios-arrow-down"></i></a>
                                        <ul class="dropdown_language">
                                            <li><a href="#"><img src="<?=base_url('assets/img/logo/language.png')?>" alt=""> English</a></li>
                                            <li><a href="#"><img src="<?=base_url('assets/img/logo/language2.png')?>" alt=""> Arabic</a></li>
                                        </ul>
                                    </li>
                                    


                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--header top start-->
        <!--header middel start-->
        <div class="header_middle middle_two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-3">
                        <div class="logo">
						<?php $logo = get_settings('web_logo'); ?>
                            <a href="<?= base_url() ?>"><img src="<?= base_url($logo) ?>" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9">
                        <div class="middel_right">
                            <div class="search-container search_two">
                                <form action="#">
                                    <div class="search_box">
									<select class='form-control search_product ' name="search" type="text"></select>
                                        
                                    </div>
                                </form>
                            </div>
                            <div class="middel_right_info">
<?php
if (isset($_SESSION['cartsession'])) {
                $cart_items = $this->cart_model->get_user_cart($_SESSION['cartsession']);
}
if (isset($_SESSION['cartsession'])) {
if(count($cart_items)==0)
{
$count=0;
}
else{
	$count=count($cart_items);
}
}
else
{
$count=0;	
}
				?>
				
                                <div class="header_wishlist">
                                    <a href="<?=base_url('my-account/favorites')?>"><span class="flaticon-heart"></span> Wish list </a>
                                    <span class="wishlist_quantity"><?php $fav=get_favorites($this->data['user']->id);echo count($fav); ?></span>
                                </div>
                                <div class="mini_cart_wrapper">
                                    <a href="javascript:void(0)"><span class="flaticon-bag"></span>My Cart </a>
                                    <span class="cart_quantity" id="cart-count"><?php echo $count; ?></span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--header middel end-->
        
        <!--mini cart-->
        <div class="mini_cart" id="cart-item-sidebar">
            
			<?php
			$total=0;
			if ($count != 0) {
                    foreach ($cart_items as $items) {
            ?>
            <div class="cart_item">
                <div class="cart_img">
                    <a href="#"><img src="<?= base_url($items['image']) ?>" alt="<?= html_escape($items['name']) ?>"></a>
                </div>
                <div class="cart_info">
                    <a href="#"><?= html_escape($items['name']) ?><?php if (!empty($items['product_variants'])) { ?> <br/>
                                         <?= str_replace(',', ' | ', $items['product_variants'][0]['variant_values']) ?>
                                     <?php } ?></a>

                    <span class="quantity">Qty: <?= $items['qty'] ?></span>
                    <span class="price_cart" data-price="<?php echo number_format($items['qty'] * $items['price'], 3); ?>"><?= $settings['currency'] . ' ' . number_format($items['qty'] * $items['price'], 3) ?></span>

                </div>
				<?php $total=$total+number_format($items['qty'] * $items['price'], 3);?>
                <div class="cart_remove">
                    <a href="#" class="remove-product" data-id="<?= $items['product_variant_id'] ?>"><i class="ion-android-close"></i></a>
                </div>
            </div>
           <?php
                    } ?>
					<div class="mini_cart_table">
                <div class="cart_total">
                    <span>Sub total:</span>
                    <span class="price" id="cart-subtotal"><?= $settings['currency'] ?> <?php echo number_format($total,3); ?></span>
                </div>
                <div class="cart_total mt-10">
                    <span>total:</span>
                    <span class="price" id="cart-total"><?= $settings['currency'] ?> <?php echo number_format($total,3); ?></span>
                </div>
            </div>

            <div class="mini_cart_footer">
                <div class="cart_button">
                    <a href="<?= base_url('cart') ?>">View cart</a>
                </div>
                <div class="cart_button">
                    <a class="active" href="<?= base_url('cart/checkout') ?>">Checkout</a>
                </div>

            </div>
             <?php } else { ?>
                 <h1 class="h4 text-center"><?= !empty($this->lang->line('empty_cart_message')) ? $this->lang->line('empty_cart_message') : 'Your Cart Is Empty' ?></h1>
         <?php }
             ?>
            

        </div>
        <!--mini cart end-->
        
        <!--header bottom satrt-->
        <div class="header_bottom bottom_two sticky-header">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="header_bottom_container">
                           
                            <div class="main_menu">
                                <nav>
                                    <ul>
                                        <li><a href="<?=base_url('home/brands')?>">Brands</a></li>
										<?php
										
                            foreach ($categories as $row) { 
							$child=$row['children'];
							if(count($child)>0)
							{
							?>
										<li><a href="#"><?= $row['name'] ?><i class="fa fa-angle-down"></i></a>
                                            <ul class="sub_menu pages">
											  <li><a href="<?= base_url('products/category/' . $row['slug']) ?>">View All</a></li>
											 <?php foreach ($child as $row1) { ?>
                                                <li><a href="<?= base_url('products/category/' . $row1['slug']) ?>"><?= $row1['name'] ?></a></li>
											 <?php } ?>              
                                            </ul>
                                        </li>
							<?php
							}
							
							  else
							{
								?>
							<li ><a href="<?= base_url('products/category/' . $row['slug']) ?>"> <?= $row['name'] ?></a></li>
								<?php
							}
							}
							 ?>
										
                                    </ul>
                                </nav>
                            </div>
							
							<!--<div class="mob_nmbr">
							<i class="lnr lnr-phone-handset"></i><span>+012 3456789</span>
							</div>-->
							
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!--header bottom end-->

    </header>
    <!--header area end-->


    <!--Offcanvas menu area start-->
    <div class="off_canvars_overlay"></div>
    <div class="Offcanvas_menu">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="canvas_open">
                        <span>MENU</span>
                        <a href="javascript:void(0)"><i class="ion-navicon"></i></a>
                    </div>
                    <div class="Offcanvas_menu_wrapper">

                        <div class="canvas_close">
                            <a href="#"><i class="ion-android-close"></i></a>
                        </div>


                        <div class="top_right text-right">
                            <ul>
                                <li class="top_links"><a href="#"><i class="ion-android-person"></i> My Account<i class="ion-ios-arrow-down"></i></a>
                                    <ul class="dropdown_links">
                                        <li><a href="<?=base_url('Login')?>">Login </a></li>
                                        <li><a href="<?=base_url('My_account')?>">My Account </a></li>
                                    </ul>
                                </li>
                                <li class="language"><a href="#"><img src="img/logo/language.png" alt="">English<i class="ion-ios-arrow-down"></i></a>
                                    <ul class="dropdown_language">
                                        <li><a href="#"><img src="<?=base_url('assets/img/logo/language.png')?>" alt=""> English</a></li>
                                        <li><a href="#"><img src="<?=base_url('assets/img/logo/language2.png')?>" alt=""> Arabic</a></li>
                                    </ul>
                                </li>
                                <!--<li class="currency"><a href="#">$ USD<i class="ion-ios-arrow-down"></i></a>
                                    <ul class="dropdown_currency">
                                        <li><a href="#">EUR – Euro</a></li>
                                        <li><a href="#">GBP – British Pound</a></li>
                                        <li><a href="#">INR – India Rupee</a></li>
                                    </ul>
                                </li>-->


                            </ul>
                        </div>
                        <div class="Offcanvas_follow">
                            <label>Follow Us:</label>
                            <ul class="follow_link">
                                <li><a href="<?= $web_settings['facebook_link'] ?>" target="_blank"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="<?= $web_settings['twitter_link'] ?>" target="_blank"><i class="ion-social-twitter"></i></a></li>
                                <li><a href="<?= $web_settings['instagram_link'] ?>" target="_blank"><i class="ion-social-instagram"></i></a></li>
                                <li><a href="<?= $web_settings['youtube_link'] ?>" target="_blank"><i class="ion-social-youtube"></i></a></li>
                            </ul>
                        </div>
                        <div class="search-container">
                            <form action="#">
                                <div class="search_box">
                                    <select class='form-control search_product ' name="search" type="text"></select>
                                </div>
                            </form>
                        </div>
                        <div id="menu" class="text-left ">                          
							
							
							<ul class="offcanvas_main_menu">
								 <li><a href="<?=base_url()?>"> Home</a></li>  
								 <li><a href="<?=base_url('home/brands')?>"> Brands</a></li>
<?php
										
                            foreach ($categories as $row) { 
							$child=$row['children'];
							if(count($child)>0)
							{
							?>
										<li class="menu-item-has-children"><a href="#"><?= $row['name'] ?></a>
                                            <ul class="sub-menu">
											<li><a href="<?= base_url('products/category/' . $row['slug']) ?>">View All</a></li>
											 <?php foreach ($child as $row1) { ?>
                                                <li><a href="<?= base_url('products/category/' . $row1['slug']) ?>"><?= $row1['name'] ?></a></li>
											 <?php } ?>              
                                            </ul>
                                        </li>
							<?php
							}
							
							  else
							{
								?>
							<li ><a href="<?= base_url('products/category/' . $row['slug']) ?>"> <?= $row['name'] ?></a></li>
								<?php
							}
							}
							 ?>								 
										
                                         
  <li class="menu-item-has-children"><a href="<?=base_url('home/contact-us')?>"> Contact Us</a></li>
							
							
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--Offcanvas menu area end-->