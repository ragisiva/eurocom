<?php $web_settings = get_settings('web_settings', true); ?>
<!-- footer starts -->
<?php $logo = get_settings('web_logo'); ?>
<footer class="footer_widgets call_to_action">
        <div class="container">
            <div class="footer_top">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="widgets_container contact_us">
                            <div class="footer_logo">
                                <a href="<?= base_url() ?>"><img src="<?= base_url($logo) ?>" alt=""></a>
                            </div>
                            <div class="footer_contact">
                                <!--<p>We are a team of designers and developers that
                                    create high quality Magento, Prestashop, Opencart...</p>-->
                                <!--<p><span>Online Store</span></p>-->
                                <p><span>Need Help?</span>Whatsapp: <a href="https://wa.me/+965<?= $web_settings['support_number'] ?>" target="_blank">+965<?= $web_settings['support_number'] ?></a><br>
								Email: <a href="mailto:<?= $web_settings['support_email'] ?>"><?= $web_settings['support_email'] ?></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6">
                        <div class="widgets_container widget_menu">
                            <h3>Other Links</h3>
                            <div class="footer_menu">
                                <ul>
                                    <li><a href="<?=base_url('home/about-us')?>">About Us</a></li>
                                    <li><a href="<?=base_url('home/track-my-order')?>">Track my order</a></li>
									<li><a href="<?=base_url('home/faq')?>">FAQ</a></li>
									<li><a href="offers.html">Offers</a></li>
									
									
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6">
                        <div class="widgets_container widget_menu">
                            <h3>Useful Links</h3>
                            <div class="footer_menu">
                                <ul>
								<li><a href="<?=base_url('home/contact-us')?>">Contact US</a></li>
                                    <li><a href="<?=base_url('home/privacy-policy')?>">Privacy Policy</a></li>
                                    <li><a href="<?=base_url('home/return-policy')?>">Return Policy</a></li>
                                    <li><a href="<?=base_url('home/terms-and-conditions')?>">Terms of Sale</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="widgets_container">
                            <h3>Newsletter Subscribe</h3>
                            <p>We’ll never share your email address with a third-party.</p>
                            <div class="subscribe_form">
                                <form id="mc-form" class="mc-form footer-newsletter">
                                    <input id="mc-email" type="email" autocomplete="off" placeholder="Enter you email address here..." />
                                    <button id="mc-submit">Subscribe</button>
                                </form>
                                <!-- mailchimp-alerts Start -->
                                <div class="mailchimp-alerts text-centre">
                                    <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                    <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                    <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                                </div><!-- mailchimp-alerts end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_bottom">
                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <div class="copyright_area">
                            <p>Copyright &copy; 2020 House of kids All Right Reserved.  | Powered By  <a href="http://chrisansgroup.com/" target="_blank">Chrisans Web Solutions</a></p>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5">
                        <div class="footer_payment text-right">
                            <a href="#"><img src="img/icon/payment.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--footer area end-->


    <!-- modal area start-->
    <div class="modal fade" id="modal_box" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal_body">
                   <div class="container">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                <div class="modal_tab" >
                                    
                                        <div class="tab-content product-details-large"  id="modal-images">
										</div>
                                        
                                       
                                       
                                    
                                    <div class="modal_tab_button"  >
                                      <ul class="nav product_navactive owl-carousel"  role="tablist"  id="tabimages" >  
										
                                          


										  


                               </ul>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-12">
                                <div class="modal_right">
                                    <div class="modal_title mb-10">
                                        <h2 class="product-title"></h2>
                                    </div>
                                    <div class="modal_price mb-10">
                                        <span class="new_price" id="modal-product-price"></span>
                                        <span class="old_price" id="modal-product-special-price-div"></span>
                                    </div>
                                    <div class="modal_description mb-15">
                                        <p id="modal-product-short-description"></p>
                                    </div>
                                    <div class="variants_selects ">
									<div class="row">
										<div id="modal-product-variant-attributes"></div>
            <div id="modal-product-variants-div"></div>
										</div>
                                        <div class="variants_color">
                                            
                                        </div>
                                        <div class="modal_add_to_cart">
                                           
                                                <input class="in-num" min="1" max="100" step="2" value="1" type="number">
                                                <button  id="modal-add-to-cart-button">add to cart</button>
                                            
                                        </div>
                                    </div>
                                    <div class="modal_social">
                                        <h2>Share this product</h2>
										<!--from https://www.buttons.social--><script>document.write('<a href="https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(document.URL)+'"target="_blank"title="Facebook"style="display:inline-block;vertical-align:middle;width:2em;height:2em;border-radius:10%;background:#3b5998;"><svg style="display:block;fill:#fff;height:44%;margin:28% auto;" viewBox="0 -256 864 1664"><path transform="matrix(1,0,0,-1,-95,1280)" d="M 959,1524 V 1260 H 802 q -86,0 -116,-36 -30,-36 -30,-108 V 927 H 949 L 910,631 H 656 V -128 H 350 V 631 H 95 v 296 h 255 v 218 q 0,186 104,288.5 104,102.5 277,102.5 147,0 228,-12 z" /></svg></a> <a href="https://twitter.com/share?url='+encodeURIComponent(document.URL)+'&text='+encodeURIComponent(document.title)+'"target="_blank"title="Twitter"style="display:inline-block;vertical-align:middle;width:2em;height:2em;border-radius:10%;background:#1b95e0;"><svg style="display:block;fill:#fff;height:36%;margin:32% auto;" viewBox="0 -256 1576 1280"><path transform="matrix(1,0,0,-1,-44,1024)" d="m 1620,1128 q -67,-98 -162,-167 1,-14 1,-42 0,-130 -38,-259.5 Q 1383,530 1305.5,411 1228,292 1121,200.5 1014,109 863,54.5 712,0 540,0 269,0 44,145 q 35,-4 78,-4 225,0 401,138 -105,2 -188,64.5 -83,62.5 -114,159.5 33,-5 61,-5 43,0 85,11 Q 255,532 181.5,620.5 108,709 108,826 v 4 q 68,-38 146,-41 -66,44 -105,115 -39,71 -39,154 0,88 44,163 Q 275,1072 448.5,982.5 622,893 820,883 q -8,38 -8,74 0,134 94.5,228.5 94.5,94.5 228.5,94.5 140,0 236,-102 109,21 205,78 -37,-115 -142,-178 93,10 186,50 z" /></svg></a> '+(/mobile|android|blackberry/i.test(navigator.userAgent)?'<a href="whatsapp://send?text='+encodeURIComponent(document.URL)+'"title="WhatsApp"style="display:inline-block;vertical-align:middle;width:2em;height:2em;border-radius:10%;background:#43d854;"><svg style="display:block;fill:#fff;height:44%;margin:28% auto;" viewBox="0 -256 1536 1548"><path transform="matrix(1,0,0,-1,0,1158)" d="m 985,562 q 13,0 98,-44 84,-44 89,-53 2,-5 2,-15 0,-33 -17,-76 -16,-39 -71,-65.5 -55,-26.5 -102,-26.5 -57,0 -190,62 -98,45 -170,118 -72,73 -148,185 -72,107 -71,194 v 8 q 3,91 74,158 24,22 52,22 6,0 18,-1 12,-2 19,-2 19,0 26.5,-6 7.5,-7 15.5,-28 8,-20 33,-88 25,-68 25,-75 0,-21 -34.5,-57.5 Q 599,735 599,725 q 0,-7 5,-15 34,-73 102,-137 56,-53 151,-101 12,-7 22,-7 15,0 54,48.5 39,48.5 52,48.5 z M 782,32 q 127,0 244,50 116,50 200,134 84,84 134,200.5 50,116.5 50,243.5 0,127 -50,243.5 -50,116.5 -134,200.5 -84,84 -200,134 -117,50 -244,50 -127,0 -243.5,-50 Q 422,1188 338,1104 254,1020 204,903.5 154,787 154,660 154,457 274,292 L 195,59 437,136 Q 595,32 782,32 z m 0,1382 q 153,0 293,-60 139,-60 240,-161 101,-101 161,-240.5 Q 1536,813 1536,660 1536,507 1476,367.5 1416,228 1315,127 1214,26 1075,-34 935,-94 782,-94 587,-94 417,0 L 0,-134 136,271 Q 28,449 28,660 q 0,153 60,292.5 60,139.5 161,240.5 101,101 240.5,161 139.5,60 292.5,60 z" /></svg></a> ':'')+'<a href="https://www.linkedin.com/shareArticle?url='+encodeURIComponent(document.URL)+'&title='+encodeURIComponent(document.title)+'"target="_blank"title="LinkedIn"style="display:inline-block;vertical-align:middle;width:2em;height:2em;border-radius:10%;background:#0077b5;"><svg style="display:block;fill:#fff;height:42%;margin:29% auto;" viewBox="0 -256 1536 1468"><path transform="matrix(1,0,0,-1,0,1132)" d="M 349,911 V -80 H 19 v 991 h 330 z m 21,306 q 1,-73 -50.5,-122 Q 268,1046 184,1046 h -2 q -82,0 -132,49 -50,49 -50,122 0,74 51.5,123 51.5,48 134.5,48 83,0 133,-48 50,-49 51,-123 z M 1536,488 V -80 h -329 v 530 q 0,105 -40,164.5 Q 1126,674 1040,674 977,674 934.5,639.5 892,605 871,554 860,524 860,473 V -80 H 531 q 2,399 2,647 0,248 -1,296 l -1,48 H 860 V 767 h -2 q 20,32 41,56 21,24 56.5,52 35.5,28 87.5,43.5 51,15.5 114,15.5 171,0 275,-113.5 Q 1536,707 1536,488 z" /></svg></a> <a href="https://plus.google.com/share?url='+encodeURIComponent(document.URL)+'"target="_blank"title="Google+"style="display:inline-block;vertical-align:middle;width:2em;height:2em;border-radius:10%;background:#dd4b39;"><svg style="display:block;fill:#fff;height:38%;margin:31% auto;" viewBox="0 -256 2304 1466"><path transform="matrix(1,0,0,-1,0,1117)" d="M 1437,623 Q 1437,415 1350,252.5 1263,90 1102,-1.5 941,-93 733,-93 584,-93 448,-35 312,23 214,121 116,219 58,355 0,491 0,640 q 0,149 58,285 58,136 156,234 98,98 234,156 136,58 285,58 286,0 491,-192 L 1025,990 Q 908,1103 733,1103 610,1103 505.5,1041 401,979 340,872.5 279,766 279,640 279,514 340,407.5 401,301 505.5,239 610,177 733,177 q 83,0 152.5,23 69.5,23 114.5,57.5 45,34.5 78.5,78.5 33.5,44 49,83 15.5,39 21.5,74 H 733 v 252 h 692 q 12,-63 12,-122 z m 867,122 V 535 H 2095 V 326 h -210 v 209 h -209 v 210 h 209 v 209 h 210 V 745 h 209 z" /></svg></a>');</script><!--end buttons.social-->                         
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
	
	
	
<!-- end -->
<!-- main content ends -->