<!-- Izimodal -->
<?php $favicon = get_settings('web_favicon'); ?>
<link rel="icon" href="<?=base_url($favicon)?>" type="image/gif" sizes="16x16">
<!-- intlTelInput -->

<link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/bootstrap.min.css">
    <!--owl carousel min css-->
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/owl.carousel.min.css">
    <!--slick min css-->
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/slick.css">
    <!--magnific popup min css-->
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/magnific-popup.css">
    <!--font awesome css-->
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/font.awesome.css">
    <!--ionicons min css-->
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/ionicons.min.css">
	<link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/flaticon.css">
    <!--animate css-->
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/animate.css">
    <!--jquery ui min css-->
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/jquery-ui.min.css">
    <!--slinky menu css-->
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/slinky.menu.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/plugins.css">
    
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/style.css">
	  <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/mixed.css">
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/select2.min.css' ?>">
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/select2-bootstrap4.min.css' ?>">
    <!--modernizr min js here-->
    <script src="<?= THEME_ASSETS_URL ;?>js/vendor/modernizr-3.7.1.min.js"></script>
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/sweetalert2.min.css' ?>">
<!-- Jquery -->
<link href="https://fonts.googleapis.com/css2?family=Dancing+Script&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Sunflower:wght@300&display=swap" rel="stylesheet">
<!-- Star rating js -->
