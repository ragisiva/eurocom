
<script src="<?= THEME_ASSETS_URL;?>js/vendor/jquery-3.4.1.min.js"></script>
<!--popper min js-->
<script src="<?= THEME_ASSETS_URL;?>js/popper.js"></script>
<!--bootstrap min js-->
<script src="<?= THEME_ASSETS_URL;?>js/bootstrap.min.js"></script>
<!--owl carousel min js-->
<script src="<?= THEME_ASSETS_URL;?>js/owl.carousel.min.js"></script>
<!--slick min js-->
<script src="<?= THEME_ASSETS_URL;?>js/slick.min.js"></script>
<!--magnific popup min js-->
<script src="<?= THEME_ASSETS_URL;?>js/jquery.magnific-popup.min.js"></script>
<!--jquery countdown min js-->
<script src="<?= THEME_ASSETS_URL;?>js/jquery.countdown.js"></script>
<!--jquery ui min js-->
<script src="<?= THEME_ASSETS_URL;?>js/jquery.ui.js"></script>
<!--jquery elevatezoom min js-->
<script src="<?= THEME_ASSETS_URL;?>js/jquery.elevatezoom.js"></script>
<!--isotope packaged min js-->
<script src="<?= THEME_ASSETS_URL;?>js/isotope.pkgd.min.js"></script>
<!--slinky menu js-->
<script src="<?= THEME_ASSETS_URL;?>js/slinky.menu.js"></script>
<!-- Plugins JS -->
<script src="<?= THEME_ASSETS_URL;?>js/plugins.js"></script>

<!-- Main JS -->
<script src="<?= THEME_ASSETS_URL;?>js/main.js"></script>

<!-- Date Range Picker -->
<script src="<?= THEME_ASSETS_URL . 'js/moment.min.js' ?>"></script>
<script src="<?= THEME_ASSETS_URL . 'js/daterangepicker.js' ?>"></script>
<script type="text/javascript" src="<?= THEME_ASSETS_URL . 'js/star-rating.js' ?>"></script>
<script type="text/javascript" src="<?= THEME_ASSETS_URL . 'js/theme.min.js' ?>"></script>
<script src="<?= THEME_ASSETS_URL . 'js/iziModal.min.js' ?>"></script>
<!-- Popper -->
<script src="<?= THEME_ASSETS_URL . 'js/popper.min.js' ?>"></script>
<!-- Bootstrap -->

<!-- Swiper JS -->
<script src="<?= THEME_ASSETS_URL . 'js/swiper-bundle.min.js' ?>"></script>
<!-- Select -->
<script src="<?= THEME_ASSETS_URL . 'js/select2.full.min.js' ?>"></script>
<!-- Bootstrap Tabs -->
<script src="<?= THEME_ASSETS_URL . 'js/bootstrap-tabs-x.min.js' ?>"></script>
<!-- ElevateZoom -->
<script src="<?= THEME_ASSETS_URL . 'js/jquery.ez-plus.js' ?>"></script>
<!-- Bootstrap Table -->
<script src="<?= THEME_ASSETS_URL . 'js/bootstrap-table.min.js' ?>"></script>
<!-- blockUI -->
<script src="<?= THEME_ASSETS_URL . 'js/jquery.blockUI.js' ?>"></script>
<!-- Sweeta Alert 2 -->
<script src="<?= THEME_ASSETS_URL . 'js/sweetalert2.min.js' ?>"></script>
<!-- Modernizr-custom.js -->
<script src="<?= THEME_ASSETS_URL . 'js/modernizr-custom.js' ?>"></script>
<!-- Lazy-Load.js -->
<script src="<?= THEME_ASSETS_URL . 'js/lazyload.min.js' ?>"></script>
<!-- intlTelInput -->
<script src="<?= THEME_ASSETS_URL . 'js/intlTelInput.js' ?>"></script>

<!-- Firebase.js -->
<script src="<?= THEME_ASSETS_URL . 'js/firebase-app.js'?>"></script>
<script src="<?= THEME_ASSETS_URL . 'js/firebase-auth.js'?>"></script>
<script src="<?= base_url('firebase-config.js')?>"></script>
<script type="text/javascript">
    base_url = "<?= base_url() ?>";
    currency = "<?= $settings['currency'] ?>";
    csrfName = "<?= $this->security->get_csrf_token_name() ?>";
    csrfHash = "<?= $this->security->get_csrf_hash() ?>";
</script>
<script src="<?= THEME_ASSETS_URL;?>js/custom.js"></script>
<script src="<?= THEME_ASSETS_URL . '/js/checkout.js' ?>"></script>
<script>

	
function gotopage()
{
page=$("#paging").val();

url=$("#url").val();
window.location.href=url+'/'+page;	
}
$(".edit-address").click(function() {
 
            $("#address_id").val($(this).attr('data-id'));
            $("#edit_name").val($(this).attr('data-name'));
            $("#edit_mobile").val($(this).attr('data-mobile'));
           
            $("#edit_street").val($(this).attr('data-street'));
            $("#edit_block").val($(this).attr('data-block'));
            $("#edit_avenue").val($(this).attr('data-avenue'));
            $("#edit_house").val($(this).attr('data-house'));
            $("#edit_area").val($(this).attr('data-area')).trigger('change');
            $('input[type=radio][value=' + $(this).attr('data-type').toLowerCase() + ']').attr('checked', true);
     });
	 $("input[name='address_id']").click(function()
{
  
  

 
   $('mobile').val($(this).attr('data-mobile'));
   
 
});
</script>