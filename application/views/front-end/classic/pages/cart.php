<div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content pading_big" >
						<h2>Cart</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product_details">
        <div class="container">
		
		 <form action="#"> 
                <div class="row">
                    <div class="col-12">
                        
						<div id="order-detail-content" class="table_block">
            <table id="cart_summary" class="std">
                <thead>
                <tr>
                    <th class="cart_product first_item"></th>
                    <th class="cart_description item"></th>    	
					<th class="cart_unit item">Unit Price</th>
										
                    <th class="cart_quantity item" width="50">Quantity</th>					
                    <th class="cart_total last_item">Total</th>
                </tr>
                </thead>
                <tbody>
                                                                                                                                                        
       <?php foreach ($cart as $key => $row) {
                                if (isset($row['qty']) && $row['qty'] != 0) {
                            ?>             
<tr id="product_5806_17409_0_0" class="cart_item last_item address_0 even shopping-cart__cart">
    <td class="cart_product">
             <a>
			 <input type="hidden" name="<?= 'id[' . $key . ']' ?>" id="id" value="<?= $row['id'] ?>">
                <img src="<?= $row['image'] ?>" alt="<?= $row['name']; ?>">
            </a>
            </td>
    <td class="cart_description coinzer">
        <p class="s_title_block">
            <a class="link link--black"><?= $row['name']; ?></a></p>
			 <?php if (!empty($row['product_variants'])) { ?>
                                                    <br><a href="#"><?= str_replace(',', ' | ', $row['product_variants'][0]['variant_values']) ?> </a>
                                                <?php } ?>
              </td>
    <td class="cart_unit">
		<span class="price" id="">
		<span> <?= $settings['currency'] . ' ' . number_format($row['price'], 3) ?>    </span>
		<!--<span class="without-reduction" style="text-decoration:line-through; color:#ccc;"> KWD 18. 00 </span>-->
        </span>
    </td>
	 
    <td class="cart_quantity">
        

					<div class="product-quantity">
                        <div class="qty">
                        <span class="minus bg-dark" data-min="<?= (isset($row['minimum_order_quantity']) && !empty($row['minimum_order_quantity'])) ? $row['minimum_order_quantity'] : 1 ?>" data-step="<?= (isset($row['minimum_order_quantity']) && !empty($row['quantity_step_size'])) ? $row['quantity_step_size'] : 1 ?>">-</span>
                        <input type="number" class="in-num itemQty count" name="qty" data-page="cart" data-id="<?= $row['id']; ?>" value="<?= $row['qty'] ?>" data-price="<?= $row['special_price']; ?>" data-step="<?= (isset($row['minimum_order_quantity']) && !empty($row['quantity_step_size'])) ? $row['quantity_step_size'] : 1 ?>" data-min="<?= (isset($row['minimum_order_quantity']) && !empty($row['minimum_order_quantity'])) ? $row['minimum_order_quantity'] : 1 ?>" data-max="<?= (isset($row['total_allowed_quantity']) && !empty($row['total_allowed_quantity'])) ? $row['total_allowed_quantity'] : '' ?>">
                        <span class="plus bg-dark" data-max="<?= (isset($row['total_allowed_quantity']) && !empty($row['total_allowed_quantity'])) ? $row['total_allowed_quantity'] : '' ?> "data-step="<?= (isset($row['minimum_order_quantity']) && !empty($row['quantity_step_size'])) ? $row['quantity_step_size'] : 1 ?>">+</span>
                        </div>
                                </div>
    </td>
    <td class="cart_total">
		<span class="price" id="total_product_price_5806_17409_0">
			                                  <?= $settings['currency'] . ' ' . number_format(($row['qty'] * $row['price']), 3) ?>                         		
		</span>

     <a rel="nofollow" class="remove-product cart_quantity_delete" name="remove_inventory" id="remove_inventory" data-id="<?= $row['id']; ?>" title="Remove From Cart" href="#"><span class="fa fa-times-circle"></span></a>
                            
    </td>
</tr>
 <?php }
                            } ?>
                    
                                                                                                                                                            
                    

                    
                                                                    


                
                </tbody>
                                <tfoot>

                </tfoot>
            </table>
        </div>
						
                     </div>
                 </div>
                 <!--coupon code area start-->
                
				
				<!--coupon code area start-->
                <div class="coupon_area mt-50">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="coupon_code left">
                                <h3>Coupon</h3>
                                <div class="coupon_inner">
                                    <p>Enter your coupon code if you have one.</p>
                                    <input placeholder="Coupon code" type="text">
                                    <button type="submit">Apply coupon</button>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-4 col-md-4"></div>
                        <div class="col-lg-4 col-md-4">
                            <div class="coupon_code right">
                                <h3>Cart Totals</h3>
                                <div class="coupon_inner">
                                    <div class="cart_subtotal">
                                        <p>SUB TOTAL</p>
                                        <p class="cart_amount"><?= $settings['currency'] . ' ' . number_format($cart['sub_total'], 3) ?></p>
                                    </div>
                                    <div class="cart_subtotal ">
                                        <p>DELIVERY CHARGES</p>
                                        <p class="cart_amount"> <?= $settings['currency'] . ' ' . number_format($cart['delivery_charge'], 3) ?></p>
                                    </div>
									<div class="cart_subtotal ">
                                        <p>GRAND TOTAL</p>
                                        <p class="cart_amount"> <?= $settings['currency'] . ' ' . number_format($cart['amount_inclusive_tax'], 3) ?></p>
                                    </div>

                                    <div class="checkout_btn">
                                        <a href="<?= base_url('cart/checkout') ?>">Proceed to Checkout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--coupon code area end-->
            </form>
        </div>
    </div>