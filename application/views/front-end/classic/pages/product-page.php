<?php $total_images = 0; ?>
<!-- breadcrumb -->
<div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
				<?php
$bannerfull=base_url().$banner;
				
				?>
                    <div class="breadcrumb_content pading_big" >
						<h2><?= $product['product'][0]['name'] ?></h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="product_details">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="product-details-tab">

                        <div id="img-1" class="zoomWrapper single-zoom">
                            <a href="#">
                                <img id="zoom1" src="<?= $product['product'][0]['image'] ?>" data-zoom-image="<?= $product['product'][0]['image'] ?>" alt="big-1">
                            </a>
                        </div>

                        <div class="single-zoom-thumb">
                            <ul class="s-tab-zoom owl-carousel single-product-active" id="gallery_01">
							 <li>
                                    <a href="#" class="elevatezoom-gallery active" data-update="" data-image="<?= $product['product'][0]['image'] ?>" data-zoom-image="<?= $product['product'][0]['image'] ?>">
                                        <img src="<?= $product['product'][0]['image'] ?>" alt="zo-th-1" />
                                    </a>

                                </li>
							<?php
                    if (!empty($product['product'][0]['other_images']) && isset($product['product'][0]['other_images'])) {
                        foreach ($product['product'][0]['other_images'] as $other_image) {
                            $total_images++;
                    ?>
                                <li>
                                    <a href="#" class="elevatezoom-gallery active" data-update="" data-image="<?= $other_image ?>" data-zoom-image="<?= $other_image ?>">
                                        <img src="<?= $other_image ?>" alt="zo-th-1" />
                                    </a>

                                </li>
							<?php }
                    } ?>
<?php
$counting=$total_images+1;

                    $variant_images_md = array_column($product['product'][0]['variants'], 'images_md');
                    if (!empty($variant_images_md)) {
                        foreach ($variant_images_md as $variant_images) {
                            if (!empty($variant_images)) {
                                foreach ($variant_images as $image) {
									if($image!='https://crisance.com/houseofkids/assets/no-image.png')
									{
                    ?>
					<li>
                                    <a href="#"  class="elevatezoom-gallery active" data-update="" data-image="<?= $image ?>" data-zoom-image="<?= $image ?>">
                                        <img id="<?php echo $counting;?>" src="<?= $image ?>" alt="zo-th-1" />
                                    </a>

                                </li>


									<?php }  }
                            }
							$counting++;
                        }
                    } ?>					
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="product_d_right">
                        <form action="#">

                            <h1><?= ucfirst($product['product'][0]['name']) ?> </h1>
                            <div class="product_nav">
                                <ul>
                                    <li class="prev"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                    <li class="next"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                            <!--<div class=" product_ratting">
                                <ul>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li class="review"><a href="#"> (customer review ) </a></li>
                                </ul>

                            </div>-->
							<?php if ($product['product'][0]['type'] == "simple_product") { ?>
							<div class="price_box">
                              <?php /*<p class="mb-0 mt-2 price" id="price">
                    <i><?= $settings['currency'] ?></i><?= number_format($product['product'][0]['min_max_price']['special_price']) ?>
                    <sup><span class="special-price striped-price"><s><i><?= $settings['currency'] ?></i><?= number_format($product['product'][0]['min_max_price']['min_price']) ?></s></span></sup>
                </p>
				*/?>
              <span class="current_price">
			  <?php
$stockupdate=0;
							 if (($product['product'][0]['stock_type'] != null && $product['product'][0]['stock_type'] != '')) {
            //Case 1 : Simple Product(simple product)
            if ($product['product'][0]['stock_type'] == 0) {
                if ($product['product'][0]['stock'] != null) {
                    $stock = intval($product['product'][0]['stock']);
                    if ($stock <= 0) {
						
					$stockupdate=1;
					
				
									 }
                }
            }
								}
			?>
			
                    <?php //$price = get_price_range_of_product($product['product'][0]['id']);
                    //echo $price['range'];?>
					<?php
					if($product_row['min_max_price']['special_price']!=0)
												 {
													 ?>
					 <span class="old_price"><?= $settings['currency'] ?> <?= number_format($product['product'][0]['min_max_price']['min_price'],3) ?></span>
                                        <span class="regular_price red"><?= $settings['currency'] ?> <?= number_format($product['product'][0]['min_max_price']['special_price'],3) ?></span>
												 <?php } else { ?><span class="regular_price"><?= $settings['currency'] ?> <?= number_format($product['product'][0]['min_max_price']['min_price'],3) ?> </span><?php }?>
                </span>  

                            </div>
							<?php } else { ?>
							<p class="mb-0 mt-2 price price_box"><span id="price" class="current_price">
                <?php $price = get_price_range_of_product($product['product'][0]['id']);
                    echo $price['range'];
                    ?>
                </span>
                    <sup>
                        <span class="special-price striped-price text-danger" id="product-striped-price-div">
                            <s id="striped-price"></s>
                        </span></sup>
                </p>
							<?php } ?>
							
							<!--<p>Lodge life is the brown travel diaper backpack by mara mea that is perfectly suited for your next outing with your baby. With a wonderfully woven material at the front exterior of the bag and a large white tassel, this bag is truly an eye-catching. This backpack is both a comfortable and practical style for outings with your baby, for it enables you to have both hands free. The straps can be easily adjusted. In addition, the bag can also be carried as a tote bag or attached to the stroller. Inside, you will find compartments for diapers, two elasticated pockets for baby food jars or bottles and a zipped compartment for personal things. The highly functional and integral metal bracket provides quick and easy access to the many things that a mother so often needs. Silk weaving by mara mea will make daily life, with or without your baby, that bit easier!</p>-->
							<?php if($stockupdate==1) { ?>
								
                                        <span class="label_sale red">Out of Stock </span>
                                   
							   <?php } ?>
							<div class="delvry_day">Delivery in 1-2 days</div>
							
							
							
									<?php if (isset($product['product'][0]['variant_attributes']) && !empty($product['product'][0]['variant_attributes'])) { ?>
                <?php foreach ($product['product'][0]['variant_attributes'] as $attribute) {
                    $attribute_values = explode(',', $attribute['values']);
                    $attribute_ids = explode(',', $attribute['ids']);
                ?>
				<?php if($attribute['attr_name']!='Color') { ?>
				<div class="variants_selects ">
									<div class="row">
										<div class="col-lg-4">
                                        <div class="variants_size">
                                            <h2>Select <?= $attribute['attr_name'] ?></h2>
                                            <select class="select_option attributes" name="<?= $attribute['attr_name'] ?>">
											 <option value="">Select <?= $attribute['attr_name'] ?></option>
											 <?php foreach ($attribute_values as $key => $row) { ?>
                                                <option  value="<?= $attribute_ids[$key] ?>"><?= $row ?></option>
                                                                       <?php } ?>
                                            </select>
                                        </div>
										</div>
										
										</div>
										</div>
										
											 <?php } else { ?>
											 <div class="product_variant color">
                                <label>Select <?= $attribute['attr_name'] ?></label>
                                <div class="custom-radios2">
<?php $counting=$total_images+1; foreach ($attribute_values as $key => $row) { ?>								
<div>
<input type="radio" class="attributes colorradio" data-step="<?= $counting ?>" id="color-<?= $attribute_ids[$key] ?>" name="<?= $attribute['attr_name'] ?>" value="<?= $attribute_ids[$key] ?>" >
<label for="color-<?= $attribute_ids[$key] ?>" style="background-color: <?= $row ?>;">
<span>
</span>
</label>
</div>
 <?php $counting++; } ?>
 </div>
										</div>



											 <?php } ?>
                    
                    
            <?php    }
            } ?>
            <?php if (!empty($product['product'][0]['variants']) && isset($product['product'][0]['variants'])) {
                foreach ($product['product'][0]['variants'] as $variant) {
            ?>
                    <input type="hidden" class="variants" name="variants_ids" data-image-index="<?= $total_images ?>" data-name="" value="<?= $variant['variant_ids'] ?>" data-id="<?= $variant['id'] ?>" data-price="<?= $variant['price'] ?>" data-special_price="<?= $variant['special_price'] ?>" data-stock="<?= $variant['stock'] ?>" />
            <?php
                    $total_images += count($variant['images']);
                }
            }
            ?>
										
										
										
										
										
										
										
                            
                            
							
						
						
								<span class="label_sale red" id="stockmsg" style="display:none;font-weight:bold;">Out of Stock </span>
								
								<div class="qty">
								<label>Quantity </label>
                        <span class="minus bg-dark">-</span>
                        <input type="number"  name="qty" class=" count in-num" value="<?= (isset($product['product'][0]['minimum_order_quantity']) && !empty($product['product'][0]['minimum_order_quantity'])) ? $product['product'][0]['minimum_order_quantity'] : 1 ?>" data-step="<?= (isset($product['product'][0]['minimum_order_quantity']) && !empty($product['product'][0]['quantity_step_size'])) ? $product['product'][0]['quantity_step_size'] : 1 ?>" data-min="<?= (isset($product['product'][0]['minimum_order_quantity']) && !empty($product['product'][0]['minimum_order_quantity'])) ? $product['product'][0]['minimum_order_quantity'] : 1 ?>" data-max="<?= (isset($product['product'][0]['total_allowed_quantity']) && !empty($product['product'][0]['total_allowed_quantity'])) ? $product['product'][0]['total_allowed_quantity'] : '' ?>">
                        <span class="plus bg-dark">+</span>
                    </div>
							
                            <div class="product_variant quantity">
							<a href="#" class="whishlist"><span class="lnr lnr-heart"></span></a>
							<?php
                if (count($product['product'][0]['variants']) <= 1) {
                    $variant_id = $product['product'][0]['variants'][0]['id'];
                } else {
                    $variant_id = "";
                }
                ?>
                                <a href="javascript:void(0);" class="button add_to_cart" type="button" id="add_cart" data-product-id="<?= $product['product'][0]['id'] ?>" data-product-variant-id="<?= $variant_id ?>">add to cart</a>

                            </div>
                            
                            <div class="product_meta">
                                <span>Category: <a href="<?= base_url('products/category/' . $catslug) ?>"><?php echo $catname; ?></a></span><br/>
								<span>Brand: <a href="<?= base_url('products/brands/' . $brandslug) ?>"><?php echo $brand; ?></a></span>
                            </div>

                        </form>
                        

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--product details end-->

    <!--product info start-->
    <div class="product_d_info">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="product_d_inner">
                        <div class="product_info_button">
                            <ul class="nav" role="tablist">
                                <li>
                                    <a class="active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">Description</a>
                                </li>
                                <!--<li>
                                    <a data-toggle="tab" href="#sheet" role="tab" aria-controls="sheet" aria-selected="false">Details</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Reviews (1)</a>
                                </li>-->
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="info" role="tabpanel">
                                <div class="product_info_content">
                                   <p> <?= $product['product'][0]['description'] ?></p>
								   <?php if($product['product'][0]['sizechart']!='') { ?>
								   <h4>Size Chart</h4>
								   <img class="img-responsive" src="<?php echo base_url().$product['product'][0]['sizechart'];?>">
								   <?php } ?>
                                </div>
                            </div>
                           <!-- <div class="tab-pane fade" id="sheet" role="tabpanel">
                               
                                <div class="product_info_content">
                                    <ul>
									<li>3 ways to wear: backpack, stroller bag, shopper.</li>
<li>1 large outside pocket for easy access to important things.</li>
<li>2 inside pockets for diapers and wipes.</li>
<li>2 inside pockets for baby food jars and bottles.</li>
<li>1 padded laptop pocket with press button fastening.</li>
<li>1 zip pocket for valuables.</li>
<li>1 key clip.</li>
<li>1 changing mat.</li>
<li>2 stroller clips.</li>
<li>Material and Measurements:</li>

<li>Size: 42 cm x 42 cm.</li>
<li>Material: 100% cotton, waxed.</li>
<li>Washing Instructions: Do not wash; wipe inside with a damp cloth.</li>
									</ul>
                                </div>
                            </div>-->

                           <!-- <div class="tab-pane fade" id="reviews" role="tabpanel">
                                <div class="reviews_wrapper">
                                    <h2>1 review for Donec eu furniture</h2>
                                    <div class="reviews_comment_box">
                                        <div class="comment_thmb">
                                            <img src="img/blog/comment2.jpg" alt="">
                                        </div>
                                        <div class="comment_text">
                                            <div class="reviews_meta">
                                                <div class="star_rating">
                                                    <ul>
                                                        <li><a href="#"><i class="ion-ios-star"></i></a></li>
                                                        <li><a href="#"><i class="ion-ios-star"></i></a></li>
                                                        <li><a href="#"><i class="ion-ios-star"></i></a></li>
                                                        <li><a href="#"><i class="ion-ios-star"></i></a></li>
                                                        <li><a href="#"><i class="ion-ios-star"></i></a></li>
                                                    </ul>
                                                </div>
                                                <p><strong>admin </strong>- September 12, 2018</p>
                                                <span>roadthemes</span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="comment_title">
                                        <h2>Add a review </h2>
                                        <p>Your email address will not be published. Required fields are marked </p>
                                    </div>
                                    <div class="product_ratting mb-10">
                                        <h3>Your rating</h3>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="product_review_form">
                                        <form action="#">
                                            <div class="row">
                                                <div class="col-12">
                                                    <label for="review_comment">Your review </label>
                                                    <textarea name="comment" id="review_comment"></textarea>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <label for="author">Name</label>
                                                    <input id="author" type="text">

                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <label for="email">Email </label>
                                                    <input id="email" type="text">
                                                </div>
                                            </div>
                                            <button type="submit">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--product info end-->

    <!--product area start-->
    <section class="product_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title">
                        <h2><span> <strong>Related</strong>Products</span></h2>
                    </div>
                    <div class="product_carousel product_column5 owl-carousel">
                        
                        <?php foreach ($related_products['product'] as $product_row) { ?> 
						 <div class="single_product">
                            <div class="product_name">
                                <h3><a href="<?= base_url('products/details/' . $product_row['slug']) ?>"><?= $product_row['name'] ?></a></h3>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="<?= base_url('products/details/' . $product_row['slug']) ?>"><img src="<?= $product_row['image_sm'] ?>" alt=""></a>
                                
                                <div class="action_links">
                                         <ul>
                                            <li class="quick_button"><a href="javascript:void(0);" class="quick-view-btn" data-tip="Quick View" data-product-id="<?= $product_row['id'] ?>" data-product-variant-id="<?= $product_row['variants'][0]['id'] ?>" data-izimodal-open="#quick-view"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                            <li class="wishlist"><a href="javascript:void(0);" class="add-to-fav-btn  <?= ($product_row['is_favorite'] == 1) ? 'fa text-danger' : '' ?>" data-product-id="<?= $product_row['id'] ?>" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
											<?php
                                                    if (count($product_row['variants']) <= 1) {
                                                        $variant_id = $product_row['variants'][0]['id'];
                                                        $modal = "";
                                                    } else {
                                                        $variant_id = "";
                                                        $modal = "#quick-view";
                                                    }
                                                    ?>
                                            <li class="compare"><a href="#"  data-tip="Add to Cart" class="add_to_cart" data-product-id="<?= $product_row['id'] ?>" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>" title="add to cart"><span class="lnr lnr-cart"></span></a></li>
                                        </ul>
                                    </div>
                            </div>
                            <div class="product_content">
                                <!--<div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>-->
                                <div class="product_footer d-flex align-items-center">
                                    <div class="price_box">
									<?php $price = get_price_range_of_product($product_row['id']);
                                                
                                                ?>
                                        <span class="regular_price"><?php echo $price['range'];?></span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
						<?php } ?>
                        
                        
                       
                       
                       
						
						
                    
					
					
					</div>
                </div>
            </div>

        </div>
    </section>