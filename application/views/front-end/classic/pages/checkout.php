<div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content pading_big" >
						<h2>Checkout</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <div class="product_details">
        <div class="container">
		
		<?php if ($this->data['is_logged_in']) {?>
		        <form class="needs-validation" id="checkout_form" method="POST" action="<?= base_url('cart/place-order') ?>">
		    <div class="accordion_area">
            <div class="row">
                <div class="col-lg-8">
                    <div id="accordion" class="card__accordion checkout_form">
                        
                        
                        <div class="card  card_dipult">
                            <div class="card-header card_accor" id="headingThree">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                   <h2> DELIVERY ADDRESS</h2>
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
								
								<div class="col-lg-6">										
										<a href="<?= base_url('my-account/manage-address') ?>"><button style="margin-bottom: 1%" class="button create_btn " type="submit" > <i class="fa fa-plus-square" aria-hidden="true"></i> Add New Address</button></a></div>
								
								<div class="clear"></div>
								 <?php if (isset($default_address) && !empty($default_address)) { 
							foreach ($default_address as $row) {
								 ?>
                                    <div class="address_box">
									  <label>
									  
										<input type="radio" name="address_id" id="<?= $row['id'] ?>" value="<?= $row['id'] ?>" data-mobile="<?= $row['mobile'] ?>">
										<?= $row['name'] ?>
									  </label>
									  <a href="javascript:void(0)" class="edit-link edit-address" data-name="<?= $row['name'] ?>" data-mobile="<?= $row['mobile'] ?>" data-mobile="<?= $row['mobile'] ?>" data-street="<?= $row['street'] ?>" data-block="<?= $row['block'] ?>" data-avenue="<?= $row['avenue'] ?>" data-house="<?= $row['house'] ?>" data-area="<?= $row['area_id'] ?>" data-type="<?= $row['type'] ?>" data-id="<?= $row['id'] ?>" data-toggle="modal" data-target="#address-modal">edit</a>

										<p><?= $row['area'] ?>, <?= $row['street'] ?>,<?= $row['block'] ?>, <?= $row['avenue'] ?>, <?= $row['house'] ?></p>											
									</div>
							<?php } } ?>
									 
									<input type="hidden" name="product_variant_id" value="<?= implode(',',array_column($cart, 'id')) ?>">
                    <input type="hidden" name="quantity" value="<?= implode(',',array_column($cart, 'qty')) ?>">
                    	<input type="hidden" name="mobile" id="mobile"  />
										
										<div class="clear"></div>
                                </div>
                            </div>
                        </div>
						
						 <div class="card  card_dipult">
                            <div class="card-header card_accor" id="headingThree">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                   <h2> BILLING ADDRESS</h2>
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
								
								<div class="col-lg-6">										
										<a href="<?= base_url('my-account/manage-address') ?>"><button style="margin-bottom: 1%" class="button create_btn " type="submit" > <i class="fa fa-plus-square" aria-hidden="true"></i> Add New Address</button></a></div>
								
								<div class="clear"></div>
								 <?php if (isset($default_address) && !empty($default_address)) { 
							foreach ($default_address as $row) {
								 ?>
                                    <div class="address_box">
									  <label>
									  
										<input type="radio" name="baddress_id" id="<?= $row['id'] ?>" value="<?= $row['id'] ?>" data-mobile="<?= $row['mobile'] ?>">
										<?= $row['name'] ?>
									  </label>
									  <a href="javascript:void(0)" class="edit-link edit-address" data-name="<?= $row['name'] ?>" data-mobile="<?= $row['mobile'] ?>" data-mobile="<?= $row['mobile'] ?>" data-street="<?= $row['street'] ?>" data-block="<?= $row['block'] ?>" data-avenue="<?= $row['avenue'] ?>" data-house="<?= $row['house'] ?>" data-area="<?= $row['area_id'] ?>" data-type="<?= $row['type'] ?>" data-id="<?= $row['id'] ?>" data-toggle="modal" data-target="#address-modal">edit</a>

										<p><?= $row['area'] ?>, <?= $row['street'] ?>,<?= $row['block'] ?>, <?= $row['avenue'] ?>, <?= $row['house'] ?></p>											
									</div>
							<?php } } ?>
									 
									
										<div class="clear"></div>
                                </div>
                            </div>
                        </div>
						
                        <div class="card  card_dipult">
                            <div class="card-header card_accor" id="headingfour">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseeight" aria-expanded="false" aria-controls="collapseeight">
                                     <h2>PAYMENTS</h2>
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <div id="collapseeight" class="collapse" aria-labelledby="headingfour" data-parent="#accordion">
                                <div class="card-body">
								 <?php if (isset($payment_methods['cod_method']) && $payment_methods['cod_method'] == 1) { ?>
                                    <label class="mb-20">
										<input id="cod" name="payment_method" type="radio" value="COD" required >
										 Cash on Delivery <img src="<?=base_url()?>/assets/front_end/classic/img/cash.png" alt="">
									  </label>
									  
									  <div class="clear"></div>
                                 <?php } ?>
                                    <label>
										<input type="radio" name="payment_method" id="gotap" value="Gotap"  required>
										  Bank cards <img src="<?=base_url()?>/assets/front_end/classic/img/icon/payment.png" class="knet" alt="">
									  </label>
									<div class="clear"></div>
<div class="col-lg-6 mt-20">									
									<input type="text" class="form-control" placeholder="Promo code" id="promocode_input">
									</div>
									<div class="col-lg-6 mt-20">
                                    <div class="input-group-append">
                                        <button class="button button-primary" id="redeem_btn"><?=!empty($this->lang->line('redeem')) ? $this->lang->line('redeem') : 'Redeem'?></button>
                                        <button class="button button-danger d-none" id="clear_promo_btn"><?=!empty($this->lang->line('clear')) ? $this->lang->line('clear') : 'Clear'?></button>
                                    </div>
									 </div>
									<div class="col-lg-12 mt-20">
									<div class="order_button">
                                    <button type="submit" id="place_order_btn">Finalize order</button>
                                </div>
								</div>
									  
                                </div>
								
                            </div>
                        </div>
                        

                        
                    </div>
                </div>
				
				
				<div class="col-lg-4 shopping_cart_area ">
					<div class=" offcanvas-cart">
        <div class="inner">
            <div class="head">
                <span class="title">Cart</span>
            </div>
            <div class=" customScroll ps ps--theme_default" data-ps-id="d13d5891-3f02-3a85-4f64-bf2d6986de83">
            	
                <ul class="minicart-product-list">

<?php if (isset($cart) && !empty($cart)) {
                                            foreach ($cart as $row) {
                                                if (isset($row['qty']) && $row['qty'] != 0) {
                                        ?>

                    <li>

                    	                   

             <a class="image">
                <img src="<?= $row['image_sm'] ?>" alt="">
            </a>

                        <div class="content">
                            <a href="#" class="title"><?= $row['name'] ?></a>
							 <?php if (!empty($row['product_variants'])) { ?>
                                                                    <?= str_replace(',', ' | ', $row['product_variants'][0]['variant_values']) ?>
                                                                <?php } ?>
							<div class="clear"></div>
						 <div class="width_50">
												
							<span class="quantity-price"><?= $row['qty'] ?> x <span class="amount"><?= number_format($row['qty'] * $row['price'], 3) ?></span></span>          
						</div>


                        <div class="width_50">
							<span class="ttlprce"><?= number_format($row['qty'] * $row['price'], 3) ?></span>
                            <!-- <a href="#" class="remove">×</a> -->
							</div>
                        </div>
<!--  -->

                    </li>

 <?php }
                                            }
                                        } ?>

                    



                    

                    
                </ul>

            <div class="ps__scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps__scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
            <div class="foot">

                <div class="sub-total">
                    <strong>SUB TOTAL  :</strong>
                    <span class="amount"><?= $settings['currency'] . ' ' . number_format($cart['sub_total'], 3) ?></span>
                    
                </div>

				<div class="sub-total">
                    <strong>DELIVERY CHARGES  :</strong>
                   <span class="amount"><?= $settings['currency'] . ' ' . number_format($cart['delivery_charge'], 3) ?></span>
                </div>

<!-- 
 <input type="hidden" id="shocartsubhid" value="17">

 -->

 
 <input type="hidden" id="shocartsubhid" value="17">

				
				 <div class="sub-total">
                    <strong>GRAND TOTAL :</strong>
                     <span class="amount"><?= $settings['currency'] ?> <span id="final_total"><?= number_format($cart['amount_inclusive_tax'], 2) ?></span></span>
                 </div>

            </div>
        </div>
    </div>
				</div>
				
				
            </div>
    </div>
		</form>
		<?php } else { ?>
		    <div class="accordion_area">
            <div class="row">
                <div class="col-lg-8">
                    <div id="accordion" class="card__accordion checkout_form">
                        <div class="card card_dipult">
                            <div class="card-header card_accor" id="headingOne">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h2>ALREADY REGISTERED</h2>

                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>

                                </button>

                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
								
								<div class="row">
                                    <div class="col-lg-6">
									<h4>First visit</h4>
										
										<button style="margin-bottom: 1%" class="button create_btn " type="button" onclick="javascript:$('#collapseTwo').toggle();scrollToAnchor('id1');" >Create an account</button>
                                        <button class="button create_btn " type="button" onclick="javascript:$('#collapseTwo').toggle();scrollToAnchor('id1');">Guest checkout</button>
										
										
									</div>
									
									<div class="col-lg-6">
										<div class="account_form">
                        <h4>Already registered?</h4>
						 <div id="infoMessage"><?php echo $message;?></div>
                        <form id="form1" name="form1" method="post" action="<?= base_url('home/login') ?>" class="form-submit-event3">
                            <p>
                                <label>User Name <span>*</span></label>
                                <input type="text" name="identity" required>
                            </p>
                            <p>
                                <label>Passwords <span>*</span></label>
                                <input type="password" name="password" required>
                            </p>
                            <div class="login_submit">
                                <a href="#">Forgot Password?</a>
                                <label for="remember">
                                    <input id="remember" type="checkbox" name="remember">
                                    Remember me
                                </label>
								  <input id="type" type="hidden" name="type" value="checkout">
                                <button type="submit" class="submit_btn">login</button>

                            </div>

                        </form>
                    </div>
									</div>
									</div>
									
                                </div>
                            </div>
                        </div>
						<form class="needs-validation" id="checkout_form" method="POST" action="<?= base_url('cart/place-order') ?>">
                        <div class="card  card_dipult">
						
                            <div class="card-header card_accor" id="headingTwo">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <h2>CREATE YOUR ACCOUNT</h2>
									<a name="id1"></a>
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>

                                </button>
                            </div>
							
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                  
								   <h4>Delivery address</h4>
								   
								   <div class="row">

                                <div class="col-lg-6 mb-20">
                                    <label>Full Name <span>*</span></label>
                                    <input type="text" name="name" placeholder="Name">
                                </div>
                                
								<div class="col-lg-6 mb-20">
                                    <label>Mobile <span>*</span></label>
                                    <input type="text" name="mobile" placeholder="Mobile Number">
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Email <span>*</span></label>
                                    <input type="text" name="email" placeholder="E-mail">
                                </div>
								
								
                                
                                <div class="col-lg-6 mb-20">
                                    <label for="country">Area <span>*</span></label>
                                    <select class="niceselect_option" name="area_id" id="edit_area">
                                        <option value="">Select Area</option>
                                          <?php foreach ($cities as $row) { ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                    <?php } ?>                                                                                                                                                                      <option value="Adan">Adan</option>
                                                                                                                                                                                                                              <option value="Abdullah Al-Salem">Abdullah Al-Salem</option>
                                                                                                                                    

                                    </select>
                                </div>

                                <div class="col-lg-6 mb-20">
                                    <label>Street Name <span>*</span></label>
                                    <input id="edit_street" name="street" placeholder="Street Name" type="text">
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Block<span>*</span></label>
                                    <input type="text" id="edit_block" name="block" placeholder="Block">
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Avenue / Building <span>*</span></label>
                                    <input type="text" id="edit_avenue" name="avenue" placeholder="Avenue / Building">
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>House No/ Flat No<span>*</span></label>
                                    <input type="text" name="house" id="edit_house" placeholder="House No/ Flat No">

                                </div>
								<div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="country" class="control-label"><?=!empty($this->lang->line('type')) ? $this->lang->line('type') : 'Type : '?></label>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" name="type" id="edit_home" value="home" />
                                <label for="home" class="form-check-label text-dark"><?=!empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home'?></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" name="type" id="edit_office" value="office" placeholder="Office" />
                                <label for="office" class="form-check-label text-dark"><?=!empty($this->lang->line('office')) ? $this->lang->line('office') : 'Office'?></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" name="type" id="edit_other" value="other" placeholder="Other" />
                                <label for="other" class="form-check-label text-dark"><?=!empty($this->lang->line('other')) ? $this->lang->line('other') : 'Other'?></label>
                            </div>
                        </div>
							<input type="hidden" name="product_variant_id" value="<?= implode(',',array_column($cart, 'id')) ?>">
                    <input type="hidden" name="quantity" value="<?= implode(',',array_column($cart, 'qty')) ?>">	
								<div class="col-lg-12 mb-10">
								
								<input id="account" name="account" value="1" type="checkbox" data-target="createp_account">
                                    <label for="account" data-toggle="collapse" data-target="#collapseExample" aria-controls="collapseOne">Create an account for Later Use?</label>
								
								<div class="collapse bdr_non" id="collapseExample">
									<div class="account_form mt-20">
									
										<div class="col-lg-6 mb-20">
											<p>
												<label>Passwords <span>*</span></label>
												<input type="password" id="password1" name="password">
											</p>
											</div>
											<div class="col-lg-6 mb-20">
											<p>
												<label>Confirm Password <span>*</span></label>
												<input type="password" id="password2" name="password2">
											</p>
											</div>
											

										
										</div>
								  </div>
								
								</div> 
								
								
								<div class="col-lg-12 mb-20">
								
								<input id="account2" type="checkbox" name="billingaddress" value="1" data-target="createp_account">
                                    <label for="account2" data-toggle="collapse" data-target="#anotheradd" aria-controls="collapseOne">Please use another address for invoice</label>
								
								<div class="collapse bdr_non" id="anotheradd">
									<div class="account_form mt-20 row">
										
										
										<div class="col-lg-6 mb-20">
                                    <label>Full Name <span>*</span></label>
                                    <input type="text" name="bname" placeholder="Name">
                                </div>
                               
								<div class="col-lg-6 mb-20">
                                    <label>Mobile <span>*</span></label>
                                    <input type="text" name="bmobile" placeholder="Mobile Number">
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Email <span>*</span></label>
                                    <input type="text" name="bemail" placeholder="E-mail">
                                </div>
								
								
                                
                                <div class="col-lg-6 mb-20">
                                    <label for="country">Area <span>*</span></label>
                                    <select class="niceselect_option" name="barea_id" id="barea_id">
                                        <option value="">Select Area</option>
                                           <?php foreach ($cities as $row) { ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                    <?php } ?>                                                                                                                                                                            <option value="Al Mansouriah">Al Mansouriah</option>
                                                                                                                                    

                                    </select>
                                </div>

                                <div class="col-lg-6 mb-20">
                                    <label>Street Name <span>*</span></label>
                                    <input id="edit_street1" name="bstreet" placeholder="Street Name">
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Block<span>*</span></label>
                                    <input type="text" id="edit_block1" name="bblock" placeholder="Block">
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Avenue / Building <span>*</span></label>
                                    <input type="text" id="edit_avenue" name="bavenue" placeholder="Avenue / Building">
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>House No/ Flat No<span>*</span></label>
                                    <input type="text" name="bhouse" id="edit_house1" placeholder="House No/ Flat No">

                                </div>
								
								
											

										
										</div>
								  </div>
								
								</div> 
								
								<div class="col-lg-12 mb-20">
									<div class="order_button">
                                    <button type="button" onclick="javascript:$('#collapseeight').toggle();scrollToAnchor('id1');">Save</button>
                                </div>
								</div>
								
								
								   
								</div>   
                                </div>
                            </div>
                        
						 
						 
						 
						 
						 
						 
						 
						 
						 
						 
						 
						 
						
                       
						
                        <div class="card  card_dipult">
                            <div class="card-header card_accor" id="headingfour">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseeight" aria-expanded="false" aria-controls="collapseeight">
                                     <h2>PAYMENTS</h2>
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <div id="collapseeight" class="collapse" aria-labelledby="headingfour" data-parent="#accordion">
                               <div class="card-body">
								 <?php if (isset($payment_methods['cod_method']) && $payment_methods['cod_method'] == 1) { ?>
                                    <label class="mb-20">
										<input id="cod" name="payment_method" type="radio" value="COD" required >
										 Cash on Delivery <img src="<?=base_url()?>/assets/front_end/classic/img/cash.png" alt="">
									  </label>
									  
									  <div class="clear"></div>
                                 <?php } ?>
                                    <label>
										<input type="radio" name="payment_method" id="gotap" value="Gotap"  required>
										  Bank cards <img src="<?=base_url()?>/assets/front_end/classic/img/icon/payment.png" class="knet" alt="">
									  </label>
									<div class="clear"></div>
<div class="col-lg-6 mt-20">									
									<input type="text" class="form-control" placeholder="Promo code" id="promocode_input">
									</div>
									<div class="col-lg-6 mt-20">
                                    <div class="input-group-append">
                                        <button class="button button-primary" id="redeem_btn"><?=!empty($this->lang->line('redeem')) ? $this->lang->line('redeem') : 'Redeem'?></button>
                                        <button class="button button-danger d-none" id="clear_promo_btn"><?=!empty($this->lang->line('clear')) ? $this->lang->line('clear') : 'Clear'?></button>
                                    </div>
									 </div>
									<div class="col-lg-12 mt-20">
									<div class="order_button">
                                    <button type="submit" id="place_order_btn">Finalize order</button>
                                </div>
								</div>
									  
                                </div>
								
                            </div>
                        </div>
                        

                        
                    </div>
                
				</form>
				</div>
				</div>
			
			
				<div class="col-lg-4 shopping_cart_area ">
					<div class=" offcanvas-cart">
        <div class="inner">
            <div class="head">
                <span class="title">Cart</span>
            </div>
            <div class=" customScroll ps ps--theme_default" data-ps-id="d13d5891-3f02-3a85-4f64-bf2d6986de83">
            	
                <ul class="minicart-product-list">



                   <?php if (isset($cart) && !empty($cart)) {
                                            foreach ($cart as $row) {
                                                if (isset($row['qty']) && $row['qty'] != 0) {
                                        ?>

                    <li>

                    	                   

             <a class="image">
                <img src="<?= $row['image_sm'] ?>" alt="">
            </a>

                        <div class="content">
                            <a href="#" class="title"><?= $row['name'] ?></a>
							 <?php if (!empty($row['product_variants'])) { ?>
                                                                    <?= str_replace(',', ' | ', $row['product_variants'][0]['variant_values']) ?>
                                                                <?php } ?>
							<div class="clear"></div>
						 <div class="width_50">
												
							<span class="quantity-price"><?= $row['qty'] ?> x <span class="amount"><?= number_format($row['qty'] * $row['price'], 3) ?></span></span>          
						</div>


                        <div class="width_50">
							<span class="ttlprce"><?= number_format($row['qty'] * $row['price'], 3) ?></span>
                            <!-- <a href="#" class="remove">×</a> -->
							</div>
                        </div>
<!--  -->

                    </li>

 <?php }
                                            }
                                        } ?>
                    
                </ul>

            <div class="ps__scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps__scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
           <div class="foot">

                <div class="sub-total">
                    <strong>SUB TOTAL  :</strong>
                    <span class="amount"><?= $settings['currency'] . ' ' . number_format($cart['sub_total'], 3) ?></span>
                    
                </div>

				<div class="sub-total">
                    <strong>DELIVERY CHARGES  :</strong>
                   <span class="amount"><?= $settings['currency'] . ' ' . number_format($cart['delivery_charge'], 3) ?></span>
                </div>

<!-- 
 <input type="hidden" id="shocartsubhid" value="17">

 -->

 
 <input type="hidden" id="shocartsubhid" value="17">

				
				 <div class="sub-total">
                    <strong>GRAND TOTAL :</strong>
                     <span class="amount"><?= $settings['currency'] ?> <span id="final_total"><?= number_format($cart['amount_inclusive_tax'], 2) ?></span></span>
                 </div>

            </div>
        </div>
    </div>
				</div>
				
				
            </div>
    </div>
	
		<?php }
		?>
                
                 <!--coupon code area start-->
                
				<!--Accordion area-->

    
	
	
	
	
	<!--Accordion area end-->
				<!--coupon code area start-->
               
                <!--coupon code area end-->
           
        </div>
    </div>
	<div class="modal fade edit-modal-lg" id="address-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><?=!empty($this->lang->line('edit_address')) ? $this->lang->line('edit_address') : 'Edit Address'?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('my-account/edit-address') ?>" method="POST" id="edit-address-form" class="mt-4">
                    <input type="hidden" name="id" id="address_id" value="" />
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                            <label for="name" class="control-label"><?=!empty($this->lang->line('name')) ? $this->lang->line('name') : 'Name'?></label>
                            <input type="text" class="form-control" id="edit_name" name="name" placeholder="Name" />
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                            <label for="mobile_number" class="control-label"><?=!empty($this->lang->line('mobile_number')) ? $this->lang->line('mobile_number') : 'Mobile Number'?></label>
                            <input type="text" class="form-control" id="edit_mobile" name="mobile" placeholder="Mobile Number" />
                        </div>
						 <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <label for="area" class="control-label">Area *</label>
                                <select name="area_id" id="edit_area" class="form-control">
                                    <?php foreach ($cities as $row) { ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <label for="city" class="control-label">Street Name *</label>
                                 <input type="text" class="form-control" id="edit_street" name="street" placeholder="Street Name" />
                            </div>
							  <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label for="block" class="control-label">Block *</label>
                                <input type="text" class="form-control" id="edit_block" name="block" placeholder="Block" />
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label for="state" class="control-label">Avenue / Building *</label>
                                <input type="text" class="form-control" id="edit_avenue" name="avenue" placeholder="Avenue / Building" />
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label for="country" class="control-label">House No/ Flat No*</label>
                                <input type="text" class="form-control" name="house" id="edit_house" placeholder="House No/ Flat No" />
                            </div>

                        
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="country" class="control-label"><?=!empty($this->lang->line('type')) ? $this->lang->line('type') : 'Type : '?></label>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" name="type" id="edit_home" value="home" />
                                <label for="home" class="form-check-label text-dark"><?=!empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home'?></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" name="type" id="edit_office" value="office" placeholder="Office" />
                                <label for="office" class="form-check-label text-dark"><?=!empty($this->lang->line('office')) ? $this->lang->line('office') : 'Office'?></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" name="type" id="edit_other" value="other" placeholder="Other" />
                                <label for="other" class="form-check-label text-dark"><?=!empty($this->lang->line('other')) ? $this->lang->line('other') : 'Other'?></label>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <input type="submit" class="btn btn-primary" id="edit-address-submit-btn" value="Save" />
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center mt-2">
                            <div id="edit-address-result"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
	
    <!--shopping cart area end -->