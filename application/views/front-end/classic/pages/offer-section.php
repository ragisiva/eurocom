<?php $offers =  get_offers(); ?>
<!-- Swiper -->
<div class="swiper-container banner-swiper swiper-container-initialized swiper-container-horizontal">
    <div class="swiper-wrapper">
        <?php foreach($offers as $row){ ?>
            <div class="swiper-slide swiper-slide-active">
                <a href="<?=$row['link']?>">
                    <img class="pic-1 lazy loaded" data-ll-status="loaded" src="<?=base_url($row['image'])?>">
                </a>
            </div>
        <?php } ?>
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination banner-swiper-pagination"></div>
</div>