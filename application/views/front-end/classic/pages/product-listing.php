<!-- breadcrumb -->
<div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content pading_big" >
						<h2><?= $single_category['name'] ?></h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
 
    <!--breadcrumbs area end-->

    <!--shop  area start-->
    <div class="shop_area">
        <div class="container">
            <div class="row">

                <div class="col-lg-9 col-md-12">
                    <!--shop wrapper start-->
                    <!--shop toolbar start-->
                  
                    
                    <div class="shop_toolbar_wrapper">
                        <!--<div class="shop_toolbar_btn">

                            <button data-role="grid_3" type="button" class="active btn-grid-3" data-toggle="tooltip" title="3"></button>

                            <button data-role="grid_4" type="button" class=" btn-grid-4" data-toggle="tooltip" title="4"></button>

                        </div>-->
						 <form class="" action="#">
						 
                       <div class="product-sorting">
                          <div class="btn-group"><a class="btn  dropdown-toggle" data-toggle="dropdown" href="#">Default sorting <span class="caret"></span></a>
							<ul class="dropdown-menu" id="sortdropdown">
                            <li><a data-value=''>Default sorting</a></li>
                            <li><a data-value='top-rated'>Sort by popularity</a></li>
                            <li><a data-value='date-desc'>Sort by latest</a></li>
                            <li><a data-value='price-asc'>Sort by price: low to high</a></li>
                            <li><a data-value='price-desc'>Sort by price: high to low</a></li>
							</ul>
							<input id="product_sort_by" type="hidden" value=""/>
						</div>
                        </div>
								
								 <div class="product-sorting">
                                    <div class="btn-group"><a class="btn  dropdown-toggle" data-toggle="dropdown" href="#">Items Per Page   <span class="caret"></span></a>
										<ul class="dropdown-menu" id="per_page_products">
											<li><a data-value=12>12</a></li>
                            <li><a data-value=24>24</a></li>
                            <li><a data-value=48>48</a></li>
                            <li><a data-value=96>96</a></li>
										</ul>
									</div>
                                </div>
								
								<div class="product-sorting">
                                    <div class="btn-group"><a class="btn  dropdown-toggle" data-toggle="dropdown" href="#">Shop by Brands   <span class="caret"></span></a>
										<ul class="dropdown-menu" id="brandsli">
										 <?php if (isset($brands) && !empty($brands)) { ?>
                        <?php foreach ($brands as $row) { ?>
											<li><a data-value=<?php echo $row['id'] ?>><?php echo $row['name'] ?></a></li>
										 <?php } } ?>
										</ul>
									</div>
                                </div>
						
						
						
						 </form>
                       <!-- <div class="page_amount">
                            <p>Showing 1–9 of 21 results</p>
                        </div>-->
                    </div>
                    <!--shop toolbar end-->

                    <div class="row shop_wrapper">
					 <?php if (isset($products) && !empty($products['product'])) { ?>
					 <?php foreach ($products['product'] as $product_row) { ?>
                        <div class="col-lg-4 col-md-4 col-6 ">
						
                            <div class="single_product">
                            
                            <div class="product_thumb">
							 <?php
$stockupdate=0;
							 if (($product_row['stock_type'] != null && $product_row['stock_type'] != '')) {
            //Case 1 : Simple Product(simple product)
            if ($product_row['stock_type'] == 0) {
                if ($product_row['stock'] != null) {
                    $stock = intval($product_row['stock']);
                    if ($stock <= 0) {
						
					$stockupdate=1;
					
				
									 }
                }
            }
								}
			?>
                                <a class="primary_img" href="<?= base_url('products/details/' . $product_row['slug']) ?>"><img src="<?= $product_row['image_sm'] ?>" alt="" <?php if($stockupdate==1) { ?> class="out_stock_img"<?php } ?>></a>
                               <?php if($stockupdate==1) { ?>
								<div class="label_product">
                                        <span class="label_sale">Out of Stock </span>
                                    </div>
							   <?php } ?>
                                <div class="action_links">
                                       <ul>
                                            <li class="quick_button"><a href="javascript:void(0);" class="quick-view-btn" data-tip="Quick View" data-product-id="<?= $product_row['id'] ?>" data-product-variant-id="<?= $product_row['variants'][0]['id'] ?>" data-izimodal-open="#quick-view"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                            <li class="wishlist"><a href="javascript:void(0);" class="add-to-fav-btn lnr lnr-heart <?= ($product_row['is_favorite'] == 1) ? 'fa text-danger' : '' ?>" data-product-id="<?= $product_row['id'] ?>" title="Add to Wishlist"></a></li>
                                             <?php
                                                    if (count($product_row['variants']) <= 1) {
                                                        $variant_id = $product_row['variants'][0]['id'];
                                                        $modal = "";
														$class="add_to_cart";
                                                    } else {
                                                        $variant_id = "";
                                                        $modal = "#quick-view";
														$class="quick-view-btn";
                                                    }
                                                    ?>
											<li class="compare"><a href="#"  data-tip="Add to Cart" class="<?php echo $class; ?>" data-product-id="<?= $product_row['id'] ?>" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>" title="add to cart"><span class="lnr lnr-cart"></span></a></li>
                                        </ul>
                                    </div>
                            </div>
                            <div class="product_content">
                                <div class="product_name">
                                <h3><a href="<?= base_url('products/details/' . $product_row['slug']) ?>"><?= $product_row['name'] ?></a></h3>
                            </div>
                                <div class="product_footer d-flex align-items-center">
                                    <div class="price_box">
                                       <?php $price = get_price_range_of_product($product_row['id']);
                                                
                                                ?>
												 <?php if ($product_row['type'] == "simple_product") { 
												 if($product_row['min_max_price']['special_price']!=0)
												 {
												 ?>
                                         <span class="old_price"><?= $settings['currency'] ?> <?= number_format($product_row['min_max_price']['min_price'],3) ?></span>
                                        <span class="regular_price red"><?= $settings['currency'] ?> <?= number_format($product_row['min_max_price']['special_price'],3) ?></span>
												 <?php
												 }
												 else{?>
												<span class="regular_price"><?= $settings['currency'] ?> <?= number_format($product_row['min_max_price']['min_price'],3) ?></span>	 
												 <?php }
												 }
												else
												{
													?>
													<span class="regular_price"><?php echo $price['range'];?></span>
													<?php
												}
												 ?>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        </div>
					 <?php } } ?>
                        
                      
                       
                        
                        
                        
                        
                        
                        
						
						
						
						
                        
                        <nav class="text-center mt-4 ">
                        <?= (isset($links)) ? $links : '' ?>

 					
                    </nav>

                    
                    <!--shop toolbar end-->
                    <!--shop wrapper end-->
					</div>
                </div>
                <div class="col-lg-3 col-md-12">
                    <!--sidebar widget start-->
                    <aside class="sidebar_widget">
                        <div class="widget_inner">
                            <div class="widget_list widget_filter">
                                <h2>Filter by price</h2>
                                <form action="#">
                                    <div id="slider-range"></div>
                                    <button type="submit">Filter</button>
                                    <input type="text" name="text" id="amount" />

                                </form>
                            </div>
                             <div class="widget_list widget_categories">
							
							<h2 class="mb-0">categories</h2>
							
							<div id="accordion" class="card__accordion">
							
						
<?php
								$i=1;
    $categories1 = $this->category_model->get_categories(null, 30);								
                            foreach ($categories1 as $row1) { 
							$child=$row1['children'];
							if(count($child)>0)
							{
							?>
						
                        <div class="card card_dipult">
                            <div class="card-header card_accor" id="heading<?php echo $i; ?>">
                                <button class="btn btn-link <?php if($i==1) { ?>collapsed<?php } ?>" data-toggle="collapse" data-target="#heading<?php echo $i; ?>" aria-expanded="true" aria-controls="heading<?php echo $i; ?>">
                                   <h3><?= $row1['name'] ?></h3>
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>

                            <div id="heading<?php echo $i; ?>" class="collapse " aria-labelledby="heading<?php echo $i; ?>" data-parent="#accordion">
                                <div class="card-body">
														<ul>
														
											 <?php foreach ($child as $row2) { ?>
                                                <li><a href="<?= base_url('products/category/' . $row2['slug']) ?>"><?= $row2['name'] ?></a></li>
											 <?php } ?> 
															
                                                            
														</ul></div>
							</div>
						</div>
						
						
						
						
					<?php
							}
							
							  else
							{
								?>
								<div class="card card_dipult">
                            <div class="card-header card_accor" >
                                <a class="btn btn-link" href="<?= base_url('products/category/' . $row1['slug']) ?>">
                                   <h3><?= $row1['name'] ?></h3>
                                </a>
                            </div>                            
						</div>
							<?php }
$i++;							
							}
							?>
								
                        
                        
                    </div>
							
							
							
							
                                
                               
                            </div>

                            <div class="widget_list widget_categories">
                                <h2>Our Brands</h2>
                                <ul>
								 <?php if (isset($brands) && !empty($brands)) { ?>
                        <?php foreach ($brands as $row) { ?> 
                                    <li><label>
                                        <input type="checkbox">
                                     <?php echo $row['name']; ?>
                                        <span class="checkmark"></span></label>
                                    </li>
                                   <?php } ?>
                    <?php } ?>  
                                </ul>
                            </div>
                            
                        </div>
                        <div class="shop_sidebar_banner">
                            <a href="#"><img src="img/bg/banner9.jpg" alt=""></a>
                        </div>
                    </aside>
                    <!--sidebar widget end-->
                </div>
            </div>
        </div>
    </div>