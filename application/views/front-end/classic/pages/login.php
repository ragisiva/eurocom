<div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content pading_big" >
						<h2>Login</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product_details">
        <div class="container">
		<div class="row">
		  <!--login area start-->
                <div class="col-lg-6 col-md-6">
                    <div class=" user-login-register">
                        <h2>Login</h2>
						<form id="form1" name="form1" method="post" action="<?= base_url('home/login') ?>" class="form-submit-event">
                        <div id="infoMessage"><?php echo $message;?></div>
						<div class="d-flex justify-content-center">
                <div class="form-group" id="error_box"></div>
            </div>
                            <p>
                                <label>User Name<span>*</span></label>
                                <input type="text" name="identity" required>
                            </p>
                            <p>
                                <label>Passwords <span>*</span></label>
                                <input type="password" name="password" required>
                            </p>
                            <div class="login_submit">
                                <a href="forgot_password">Forgot Password?</a>
                                <label for="remember">
                                    <input id="remember" type="checkbox" name="remember">
                                    Remember me
                                </label>
								<div class="clear"></div>
                                <button type="submit" class="submit_btn">login</button>

                            </div>
</form>
                        
                    </div>
                </div>
                <!--login area start-->

                <!--register area start-->
                <div class="col-lg-6 col-md-6">
                    <div class="account_form register">
                        <h2>Register</h2>
                        <form id="form2" name="form2" method="post" action='#' id='sign-up-form' class='sign-up-form'>
							<p>
                                <label>Your Name <span>*</span></label>
                                <input type="text" placeholder="Username" id="name" name="name" required>
                            </p>
                            <p>
                                <label>Email address <span>*</span></label>
                                <input type="text" id="email" name="email">
                            </p>
                            <p>
                                <label>Mobile <span>*</span></label>
                                <input type="number" name="phonenumber" id="phonenumber" required>
                            </p>
							<p>
                                <label>Password <span>*</span></label>
                                <input type="password" id="password1" name="password1" required>
                            </p>
                            <div class="login_submit">
                                <button type="submit" >Register</button>
                            </div>
							 <div id='sign-up-error' class='alert alert-success text-center p-3' ></div>
                        </form>
                    </div>
                </div>
                <!--register area end-->
			 </div>	
        </div>
    </div>