<div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content pading_big" >
						<h2>Orders</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- end breadcrumb -->

<div class="product_details">
        <div class="container">
		<div class="row">
		  <!--login area start-->
                
               
			   <div class="account_dashboard">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <!-- Nav tabs -->
                        <div class="dashboard_tab_button">
                           <?php $this->load->view('front-end/' . THEME . '/pages/my-account-sidebar') ?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                        <!-- Tab panes -->
                        <div class="tab-content dashboard_content">
                            <div class="tab-pane fade show active" id="dashboard">
                                <h3>Orders </h3>
                               
								<div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Order</th>
                                              <th>Date</th>
                                                <th>Status</th>
                                                <th>Total</th>
                                                <th>Actions</th>
												
                                            </tr>
                                        </thead>
                                        <tbody>
                        <?php foreach ($orders['order_data'] as $row) { ?>
						
						<tr>
                                                <td><?= $row['id'] ?></td>
                                               <td><?= date('d/m/Y',strtotime($row['date_added'])); ?></td>
                                                <td>
												
												 <ul id="progressbar">
												<?php
                                            $status = array('received', 'processed', 'shipped', 'delivered');
                                            $i = 1;
                                            foreach ($item['status'] as $value) { ?>
                                                <?php
                                                $class = '';
                                                if ($value[0] == "cancelled" || $value[0] == "returned") {
                                                    $class = 'cancel';
                                                    $status = array();
                                                } elseif (($ar_key = array_search($value[0], $status)) !== false) {
                                                    unset($status[$ar_key]);
                                                }
                                                ?> <li class="active <?= $class ?>" id="step<?= $i ?>">
                                                    <p><?= strtoupper($value[0]) ?></p>
                                                    <p><?= $value[1] ?></p>
                                                </li>
                                            <?php
                                                $i++;
                                            } ?>
                                            <?php foreach ($status as $value) { ?>
											<li  <?php if($value==$row['active_status']) { ?> class="active" <?php } ?> id="step<?= $i ?>">
                                                    <p><?= strtoupper($value) ?></p>
                                                </li>
                                            <?php $i++;
                                            } ?></ul>
											
											</td>
                                                <td>	<?= $settings['currency'] ?> <?= number_format($row['final_total'], 3) ?> </td>
                                                <td class="cart_button"><a href="<?= base_url('my-account/order-details/' . $row['id']) ?>" class="view active">view</a><br/><?php if ($row['is_cancelable'] && !$row['is_already_cancelled']) { ?><a href="javascript:void(0);" class="update-order view" data-status="cancelled" data-order-id="<?= $row['id'] ?>">Cancel</a><?php } ?><?php if ($row['is_returnable'] && !$row['is_already_returned']) { ?><a href="javascript:void(0);" class="update-order view" data-status="returned" data-order-id="<?= $row['id'] ?>">Return</a><?php } ?></td>
                                           
											</tr>
                            
                        <?php } ?>
						</tbody>
                                    </table>
                               
                        <div class="text-center">
                            <?= $links ?>
                        </div>
                            </div>
                            
                          
                            
							
                           
                       




					   </div>
                    </div>
                </div>
            </div>
			   
			   
			 </div>	
        </div>
    </div>