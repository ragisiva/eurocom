 <div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content pading_big" >

       
                   
						<h2>Contact Us</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="product_details pt_0 contact">
        <div class="container">
		
			<div class="shipping_inner">
			<div class="single_shipping"></div>
                       <!-- <div class="single_shipping">
                            <div class="shipping_icone">
                               <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </div>
                            <div class="shipping_content">
                                <h2>Address</h2>
                                <p>Lorem Ipsum, Simply dummy, Kuwait</p>
                            </div>
                        </div>
                        <div class="single_shipping">
                            <div class="shipping_icone">
                               <i class="fa fa-phone" aria-hidden="true"></i>
                            </div>
                            <div class="shipping_content">
                                <h2>Phone</h2>
                                <p>+012 3456789</p>
                            </div>
                        </div>-->
                        <div class="single_shipping" style="border-left:0;">
                            <div class="shipping_icone">
                               <i class="fa fa-whatsapp" aria-hidden="true"></i>
                            </div>
                            <div class="shipping_content">
                                <h2>Whatsapp</h2>
                                <p>+965<?= $web_settings['support_number'] ?></p>
                            </div>
                        </div>
                        <div class="single_shipping">
                            <div class="shipping_icone">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </div>
                            <div class="shipping_content">
                                <h2>Email</h2>
                                <p><?= $web_settings['support_email'] ?></p>
                            </div>
                        </div>
                    </div>
			
                
                 <!--coupon code area start-->
                
				<!--Accordion area-->
    <div class="accordion_area">
            <div class="row">
			
			 <div class="col-lg-2"></div>	
			
                <div class="col-lg-8 text-center">
				
				<h5 class="mb-20">Enquiry</h5>
				
                    <div class="checkout_form add_new_address text-left">
                                   
								   
								   <div class="row">

                                <div class="col-lg-6 mb-20">
                                    <label>First Name <span>*</span></label>
                                    <input type="text">
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Last Name <span>*</span></label>
                                    <input type="text">
                                </div>
								<div class="col-lg-6 mb-20">
                                    <label>Mobile <span>*</span></label>
                                    <input type="text">
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Email <span>*</span></label>
                                    <input type="text">
                                </div>                              
                                <div class="col-lg-12 mb-20">
                                    <label>Comments <span>*</span></label>
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                                
								
								
								
								
								
								
								<div class="col-lg-12 mb-20 text-center">
									<div class="order_button">
                                    <button type="submit">Submit</button>
                                </div>
								</div>
								
								
								   
								</div>   
                                </div>
                </div>
				
				
				<div class="col-lg-12 text-center">
				<h5 class="mb-20">Follow Us</h5>
				<div class="link_follow">
                            <ul>
                                <li><a href="<?= $web_settings['facebook_link'] ?>" target="_blank"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="<?= $web_settings['twitter_link'] ?>" target="_blank"><i class="ion-social-twitter" target="_blank"></i></a></li>
                                <li><a href="<?= $web_settings['instagram_link'] ?>" target="_blank"><i class="ion-social-instagram" target="_blank"></i></a></li>
                                <li><a href="<?= $web_settings['youtube_link'] ?>" target="_blank"><i class="ion-social-youtube" target="_blank"></i></a></li>
                            </ul>
                        </div>
				</div>
				
				
            </div>
			
			
    </div>
    <!--Accordion area end-->
				<!--coupon code area start-->
               
                <!--coupon code area end-->
           
        </div>
    </div>