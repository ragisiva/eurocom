<!-- breadcrumb -->
<div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content pading_big" >
						<h2>Address</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- end breadcrumb -->

<div class="product_details">
        <div class="container">
		<div class="row">
		  <!--login area start-->
                
               
			   <div class="account_dashboard">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <!-- Nav tabs -->
                        <div class="dashboard_tab_button">
                           <?php $this->load->view('front-end/' . THEME . '/pages/my-account-sidebar') ?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                        <!-- Tab panes -->
                        <div class="tab-content dashboard_content">
                            <div class="tab-pane fade show active" id="dashboard">
                                <h3>Address </h3>
                               
								<form action="<?= base_url('my-account/add-address') ?>" method="POST" id="add-address-form" class="mt-4 p-4">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <label for="name" class="control-label"><?=!empty($this->lang->line('name')) ? $this->lang->line('name') : 'Name *'?></label>
                                <input type="text" class="form-control" id="address_name" name="name" placeholder="Name" />
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <label for="mobile_number" class="control-label"><?=!empty($this->lang->line('mobile_number')) ? $this->lang->line('mobile_number') : 'Mobile Number *'?></label>
                                <input type="text" class="form-control" id="mobile_number" name="mobile" placeholder="Mobile Number" />
                            </div>
                             <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <label for="area" class="control-label">Area *</label>
                                <select name="area_id" id="area" class="form-control">
                                    <?php foreach ($cities as $row) { ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <label for="city" class="control-label">Street Name *</label>
                                 <input type="text" class="form-control" id="street" name="street" placeholder="Street Name" />
                            </div>
                           

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label for="block" class="control-label">Block *</label>
                                <input type="text" class="form-control" id="block" name="block" placeholder="Block" />
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label for="state" class="control-label">Avenue / Building *</label>
                                <input type="text" class="form-control" id="avenue" name="avenue" placeholder="Avenue / Building" />
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label for="country" class="control-label">House No/ Flat No*</label>
                                <input type="text" class="form-control" name="house" id="house" placeholder="Country" />
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <label for="country" class="control-label"><?=!empty($this->lang->line('type')) ? $this->lang->line('type') : 'Type : '?></label>
                                <div class="form-check form-check-inline">
                                    <input type="radio" class="form-check-input" name="type" id="home" value="home" />
                                    <label for="home" class="form-check-label text-dark"><?=!empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home'?></label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" class="form-check-input" name="type" id="office" value="office" placeholder="Office" />
                                    <label for="office" class="form-check-label text-dark"><?=!empty($this->lang->line('office')) ? $this->lang->line('office') : 'Office'?></label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" class="form-check-input" name="type" id="other" value="other" placeholder="Other" />
                                    <label for="other" class="form-check-label text-dark"><?=!empty($this->lang->line('other')) ? $this->lang->line('other') : 'Other'?></label>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                <input type="submit" class="btn btn-primary" id="save-address-submit-btn" value="Save" />
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                <div id="save-address-result"></div>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <div class="card-body">
                        <table id="address_list_table" class='table-striped' data-toggle="table" data-url="<?= base_url('my-account/get-address-list') ?>" data-click-to-select="true" data-side-pagination="server" data-pagination="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-search="true" data-show-columns="true" data-show-refresh="true" data-trim-on-search="false" data-sort-name="id" data-sort-order="asc" data-mobile-responsive="true" data-toolbar="" data-show-export="true" data-maintain-selected="true" data-export-types='["txt","excel"]' data-query-params="queryParams">
                            <thead>
                                <tr>
                                    <th data-field="id" data-sortable="true">ID</th>
                                    <th data-field="name" data-sortable="false"><?=!empty($this->lang->line('name')) ? $this->lang->line('name') : 'Name'?></th>
                                    <th data-field="type" data-sortable="false" class="col-md-5"><?=!empty($this->lang->line('type')) ? $this->lang->line('type') : 'Type'?></th>
                                    <th data-field="mobile" data-sortable="false"><?=!empty($this->lang->line('mobile_number')) ? $this->lang->line('mobile_number') : 'Mobile'?></th>
                                   
                                   
                                    
                                    <th data-field="area" data-sortable="false"><?=!empty($this->lang->line('are')) ? $this->lang->line('area') : 'Area'?></th>
                                    <th data-field="street" data-sortable="false">Street Name</th>
             
                                    <th data-field="block" data-sortable="false">Block</th>
                                    <th data-field="avenue" data-sortable="false">Avenue / Building</th>
									<th data-field="house" data-sortable="false">House No/ Flat No</th>
                                    <th data-field="action" data-events="editAddress" data-sortable="true"><?=!empty($this->lang->line('action')) ? $this->lang->line('action') : 'Action'?></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                            </div>
                            
                          
                            
							
                           
                       




					   </div>
                    </div>
                </div>
            </div>
			   
			   
			 </div>	
        </div>
    </div>
	
	

<div class="modal fade edit-modal-lg" id="address-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><?=!empty($this->lang->line('edit_address')) ? $this->lang->line('edit_address') : 'Edit Address'?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('my-account/edit-address') ?>" method="POST" id="edit-address-form" class="mt-4">
                    <input type="hidden" name="id" id="address_id" value="" />
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                            <label for="name" class="control-label"><?=!empty($this->lang->line('name')) ? $this->lang->line('name') : 'Name'?></label>
                            <input type="text" class="form-control" id="edit_name" name="name" placeholder="Name" />
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                            <label for="mobile_number" class="control-label"><?=!empty($this->lang->line('mobile_number')) ? $this->lang->line('mobile_number') : 'Mobile Number'?></label>
                            <input type="text" class="form-control" id="edit_mobile" name="mobile" placeholder="Mobile Number" />
                        </div>
						 <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <label for="area" class="control-label">Area *</label>
                                <select name="area_id" id="edit_area" class="form-control">
                                    <?php foreach ($cities as $row) { ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <label for="city" class="control-label">Street Name *</label>
                                 <input type="text" class="form-control" id="edit_street" name="street" placeholder="Street Name" />
                            </div>
							  <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label for="block" class="control-label">Block *</label>
                                <input type="text" class="form-control" id="edit_block" name="block" placeholder="Block" />
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label for="state" class="control-label">Avenue / Building *</label>
                                <input type="text" class="form-control" id="edit_avenue" name="avenue" placeholder="Avenue / Building" />
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label for="country" class="control-label">House No/ Flat No*</label>
                                <input type="text" class="form-control" name="house" id="edit_house" placeholder="House No/ Flat No" />
                            </div>

                        
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="country" class="control-label"><?=!empty($this->lang->line('type')) ? $this->lang->line('type') : 'Type : '?></label>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" name="type" id="edit_home" value="home" />
                                <label for="home" class="form-check-label text-dark"><?=!empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home'?></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" name="type" id="edit_office" value="office" placeholder="Office" />
                                <label for="office" class="form-check-label text-dark"><?=!empty($this->lang->line('office')) ? $this->lang->line('office') : 'Office'?></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" name="type" id="edit_other" value="other" placeholder="Other" />
                                <label for="other" class="form-check-label text-dark"><?=!empty($this->lang->line('other')) ? $this->lang->line('other') : 'Other'?></label>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <input type="submit" class="btn btn-primary" id="edit-address-submit-btn" value="Save" />
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center mt-2">
                            <div id="edit-address-result"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    window.editAddress = {
        'click .edit-address': function(e, value, row, index) {
            $("#address_id").val(row.id);
            $("#edit_name").val(row.name);
            $("#edit_mobile").val(row.mobile);
           
            $("#edit_street").val(row.street);
            $("#edit_block").val(row.block);
            $("#edit_avenue").val(row.avenue);
            $("#edit_house").val(row.house);
            $("#edit_area").val(row.area_id).trigger('change');
            $('input[type=radio][value=' + row.type.toLowerCase() + ']').attr('checked', true);
        }
    };
</script>