<?php $current_url = current_url(); ?>
<ul role="tablist" class="nav flex-column dashboard-list">
                                <li><a   id="dashboard" href="<?= base_url('my-account') ?>" class="nav-link <?=($current_url==base_url('my-account')) ? 'active' : ''?>">Dashboard</a></li>
                                <li> <a  id="order-history" href="<?= base_url('my-account/orders') ?>"  class="nav-link <?=($current_url==base_url('my-account/orders')) ? 'active' : ''?>">Orders</a></li>
                                <li><a href="<?= base_url('my-account/favorites') ?>"  class="nav-link <?=($current_url==base_url('my-account/favorites')) ? 'active' : ''?>">Whishlist </a></li>
                                <li><a href="<?= base_url('my-account/manage-address') ?>"  class="nav-link <?=($current_url==base_url('my-account/manage-address')) ? 'active' : ''?>">Addresses</a></li>
                                <li><a href="<?= base_url('my-account/profile') ?>"  class="nav-link">Account details</a></li>
                                <li><a href="<?= base_url('login/logout') ?>" class="nav-link <?=($current_url==base_url('login/logout')) ? 'active' : ''?>">logout</a></li>
                            </ul>