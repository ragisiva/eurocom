 <div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content pading_big" >

       
                   
						<h2>FAQ</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="accordion_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="accordion" class="card__accordion">
					
					 
					 <?php $i=1; foreach($faq['data'] as $row){ ?>
					 <div class="card  card_dipult">
                            <div class="card-header card_accor" id="heading<?php echo $i; ?>">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse<?php echo $i; ?>">
                                    <h3> <?=html_escape($row['question'])?></h3>
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>

                                </button>
                            </div>
                            <div id="collapse<?php echo $i; ?>" class="collapse" aria-labelledby="heading<?php echo $i; ?>" data-parent="#accordion">
                                <div class="card-body">
                                    <p> <?=html_escape($row['answer'])?></p>
                                </div>
                            </div>
                        </div>
					 
					  <?php $i++;} ?>
					 
					 </div>
                            </div>
                        </div>
						
						
						
						
						   </div>
                </div>
            </div>
        </div>
    </div>
    <!--Accordion area end-->
		
        </div>
    </div>