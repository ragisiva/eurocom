<!-- breadcrumb -->
<div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content pading_big" >
						<h2>Wishlist</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- end breadcrumb -->

<div class="product_details">
        <div class="container">
		<div class="row">
		  <!--login area start-->
                
               
			   <div class="account_dashboard">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <!-- Nav tabs -->
                        <div class="dashboard_tab_button">
                           <?php $this->load->view('front-end/' . THEME . '/pages/my-account-sidebar') ?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                        <!-- Tab panes -->
                        <div class="tab-content dashboard_content">
                            <div class="tab-pane fade show active" id="dashboard">
                                <h3>Wishlist </h3>
                               <div class="table-responsive">
                                     <table class="cart-wishlist-table table">
                    <thead>
                        <tr>
                            <th class="name" colspan="2">Product</th>
                            <th class="price">Unit Price</th>
                            <th class="add-to-cart">&nbsp;</th>
                            <th class="remove">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php
                            if (isset($products) && !empty($products)) {
                                foreach ($products as $row) { ?>
                        <tr>
                            <td class="thumbnail"><a href="<?= base_url('products/details/' . $row['slug']) ?>"><img src="<?= $row['image_sm'] ?>"></a></td>
                            <td class="name"> <a href="<?= base_url('products/details/' . $row['slug']) ?>"><?= $row['name'] ?></a></td>
                            <td class="price"><?php if ($row['type'] == "simple_product") { 
												 if($row['min_max_price']['special_price']!=0)
												 {
												 ?>
                                         <span class="old_price"><?= $settings['currency'] ?> <?= number_format($row['min_max_price']['min_price'],3) ?></span>
                                        <span class="regular_price red"><?= $settings['currency'] ?> <?= number_format($row['min_max_price']['special_price'],3) ?></span>
												 <?php
												 }
												 else{?>
												<span class="regular_price"><?= $settings['currency'] ?> <?= number_format($row['min_max_price']['min_price'],3) ?></span>	 
												 <?php }
												 }
												else
												{
													?>
													<span class="regular_price"><?php echo $price['range'];?></span>
													<?php
												}
												 ?></td>
												 <?php
                                                    if (count($row['variants']) <= 1) {
                                                        $variant_id = $row['variants'][0]['id'];
                                                        $modal = "";
														$class="add_to_cart";
                                                    } else {
                                                        $variant_id = "";
                                                        $modal = "#quick-view";
														$class="quick-view-btn";
                                                    }
                                                    ?>
                            <td ><a href="javascript:void(0);" class="<?php echo $class; ?> add-to-cart buttn2" data-product-id="<?= $row['id'] ?>" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>"><span class="lnr lnr-cart"></span> Add to Cart</a></td>
                            <td class="remove"><a href="javascript:void(0);" class="btn fa-heart add-to-fav-btn fa text-danger" data-product-id="<?= $row['id'] ?>" class="btn">×</a></td>
                        </tr>
                            <?php }
                            } else { ?> 
                       <tr>
					    <td class="thumbnail" colspan="5"><h1 class="h2">No Favorite Products Found.</h1>
                                        <a href="<?= base_url('products') ?>" class="button button-rounded button-warning">Go to Shop</a></td>
					    </tr>
							<?php } ?>
                    </tbody>
                </table>
                                </div>
								
								
								
                            </div>
                            
                          
                            
							
                           
                       




					   </div>
                    </div>
                </div>
            </div>
			   
			   
			 </div>	
        </div>
    </div>
