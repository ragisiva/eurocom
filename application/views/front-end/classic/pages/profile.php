<!-- breadcrumb -->
<div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content pading_big" >
						<h2>Profile</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- end breadcrumb -->

<div class="product_details">
        <div class="container">
		<div class="row">
		  <!--login area start-->
                
               
			   <div class="account_dashboard">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <!-- Nav tabs -->
                        <div class="dashboard_tab_button">
                           <?php $this->load->view('front-end/' . THEME . '/pages/my-account-sidebar') ?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                        <!-- Tab panes -->
                        <div class="tab-content dashboard_content">
                            <div class="tab-pane fade show active" id="dashboard">
                                <h3>Profile </h3>
                               <div class="myaccount-content account-details">
                                <div class="account-details-form">
								<form class="form-submit-event1" method="POST" action="<?= base_url('login/update_user') ?>">
								 <div class="row learts-mb-n30">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="username" class="col-sm-12 col-form-label"><?=!empty($this->lang->line('username')) ? $this->lang->line('username') : 'Username'?>*</label>
                            <input type="text" class="form-control" id="username" placeholder="Type Username here" name="username" value="<?= $users->username ?>">
                        </div>

                        <?php if ($identity_column == 'email') { ?>
                            <div class="form-group col-md-6">
                                <label for="email" class="col-sm-12 col-form-label"><?=!empty($this->lang->line('email')) ? $this->lang->line('email') : 'Email'?>*</label>
                                <input type="text" class="form-control" id="email" placeholder="Type Email here" name="email" value="<?= $users->email ?>">
                            </div>
                        <?php } else { ?>
                            <div class="form-group col-md-6">
                                <label for="mobile" class="col-sm-12 col-form-label"><?=!empty($this->lang->line('mobile')) ? $this->lang->line('mobile') : 'Mobile'?>*</label>
                                <div class="col-sm-10">
                                    <input type="phone" class="form-control" id="mobile" placeholder="Type Mobile No. here" name="mobile" value="<?= $users->mobile ?>">
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="old" class="col-sm-12 col-form-label"><small><?=!empty($this->lang->line('old_password')) ? $this->lang->line('old_password') : 'Old Password'?></small></label>
                        <input type="password" class="form-control" id="old" placeholder="Type Old Password here" name="old">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="new" class="col-sm-12 col-form-label"><?=!empty($this->lang->line('new_password')) ? $this->lang->line('new_password') : 'New Password'?></label>
                            <input type="password" class="form-control" id="new" placeholder="Type New Password here" name="new">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="new_confirm" class="col-sm-12 col-form-label"><?=!empty($this->lang->line('confirm_new_password')) ? $this->lang->line('confirm_new_password') : 'Confirm New Password'?></label>
                            <input type="password" class="form-control" id="new_confirm" placeholder="Type Confirm Password here" name="new_confirm">
                        </div>
                    </div>
                    <button type="submit" class="button button-success btn-5 submit_btn"><?=!empty($this->lang->line('save')) ? $this->lang->line('save') : 'Save'?></button>
                    <div class="d-flex justify-content-center mt-3">
                        <div class="form-group" id="error_box">
                        </div>
                    </div>
					</div>
                </form>
                            </div>
                            
                          
                            
						</div>
					</div>	
                           
                       




					   </div>
                    </div>
                </div>
            </div>
			   
			   
			 </div>	
        </div>
    </div>
