<div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content pading_big" >
						<h2>Dashboard</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product_details">
        <div class="container">
		<div class="row">
		  <!--login area start-->
                
               
			   <div class="account_dashboard">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <!-- Nav tabs -->
                        <div class="dashboard_tab_button">
                           <?php $this->load->view('front-end/' . THEME . '/pages/my-account-sidebar') ?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                        <!-- Tab panes -->
                        <div class="tab-content dashboard_content">
                            <div class="tab-pane fade show active" id="dashboard">
                                <h3>Dashboard </h3>
                                <p>From your account dashboard. you can easily check &amp; view your <a href="#">recent orders</a>, manage your <a href="#">shipping and billing addresses</a> and <a href="#">Edit your password and account details.</a></p>
								<div class="row">
								<div class='col-md-2 card text-center border-0 mr-3 mb-3'>
                    <a href='<?= base_url('my-account/profile') ?>' class="link-color">
                        <div class='card-header bg-transparent'>
                            <?= !empty($this->lang->line('profile')) ? $this->lang->line('profile') : 'PROFILE' ?>
                        </div>
                        <div class='card-body'>
                            <i class="fa fa-user-circle dashboard-icon link-color fa-lg"></i>
                        </div>
                    </a>
                </div>
                <div class='col-md-2 card text-center border-0 mr-3 mb-3'>
                    <a href='<?= base_url('my-account/orders') ?>' class="link-color">
                        <div class='card-header bg-transparent'>
                            <?= !empty($this->lang->line('orders')) ? $this->lang->line('orders') : 'ORDERS' ?>
                        </div>
                        <div class='card-body'>
                            <i class="fa fa-history dashboard-icon link-color fa-lg"></i>
                        </div>
                    </a>
                </div>
               
                <div class='col-md-2 card text-center border-0 mr-3 mb-3'>
                    <a href='<?= base_url('my-account/Favorite') ?>' class="link-color">
                        <div class='card-header bg-transparent'>
                            Wishlist
                        </div>
                        <div class='card-body'>
                            <i class="fa fa-heart dashboard-icon link-color fa-lg"></i>
                        </div>
                    </a>
                </div>
                <div class='col-md-2 card text-center border-0 mr-3 mb-3'>
                    <a href='<?= base_url('my-account/manage-address') ?>' class="link-color">
                        <div class='card-header bg-transparent'>
                            <?= !empty($this->lang->line('address')) ? $this->lang->line('address') : 'ADDRESS' ?>
                        </div>
                        <div class='card-body'>
                            <i class="fa fa-id-badge dashboard-icon link-color fa-lg"></i>
                        </div>
                    </a>
                </div>
				</div>
                            </div>
                            
                          
                            
							
                           
                       




					   </div>
                    </div>
                </div>
            </div>
			   
			   
			 </div>	
        </div>
    </div>
    <!--shopping cart area end -->

				
				
                <!--coupon code area end-->
            
    <!--product details end-->

    <!--product info start-->
   
    <!--product info end-->

    <!--product area start-->
   
    <!--product area end-->

    <!--call to action start-->