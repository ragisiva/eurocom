 <!--slider area start-->
    <section class="slider_section slider_two mb-50">
        <div class="slider_area owl-carousel">
		<?php if (isset($sliders) && !empty($sliders)) { ?>
                        <?php foreach ($sliders as $row) { ?>
            <div class="single_slider d-flex align-items-center" data-bgimg="<?= base_url($row['image']) ?>">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="slider_content">
                                <h2><?= $row['title1en'] ?></h2>
                                <h1><?= $row['title2en'] ?></h1>
                                
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            
            
        
		 <?php } ?>
                    <?php } ?>
		
		
		</div>
    </section>
    <!--slider area end-->

    <!--shipping area start-->
    

	
	
	<div class="banner-group mb-50 mobile_hide">
	<div class="container-fluid">
		<div class="row">
		<?php $offers =  get_offers(); 
		$offernew=array();
		$offernew[]=$offers[0];
		$offernew[]=$offers[1];
		?>
		 <?php
		$i=1;
		 foreach($offernew as $row){
		?>
			<div class="col-sm-6 col-lg-4">
				<div class="banners banner-large banner-overlay banner-overlay-light banner-lg banner-1">
					<a href="<?=$row['link']?>">
						<div class="lazy-overlay"></div><span class=" lazy-load-image-background blur lazy-load-image-loaded"><img alt="banner" src="<?=base_url($row['image'])?>" width="100" height="510"></span>
					</a>
					<?php
					if($i==1)
					{?>
					<div class="banner-content banner-content-top">
						<h4 class="banner-subtitle">Fun</h4>
						<h3 class="banner-title">Water Play Toys</h3>
						
					</div>
					<?php }?>
					<?php
					if($i==2)
					{?>
					<div class="banner-content banner-content-top">
						<h4 class="banner-subtitle">Shop</h4>
						<h3 class="banner-title">Baby Bibs</h3>
						
					</div>
					<?php }?>
				</div>
			</div>
			
		<?php 

$i++;

		}?>
		<div class="col-sm-12 col-md-12 col-lg-4">
				<div class="row">	
		 <?php
		 $offernew1=array();
		$offernew1[]=$offers[2];
		$offernew1[]=$offers[3];
		$i=1;
		 foreach($offernew1 as $row){
		?>
					<div class="col-lg-12 col-md-6 col-sm-6">
						<div class="banners banner-overlay">
							<a href="<?=$row['link']?>">
								<div class="lazy-overlay bg-3"></div><span class=" lazy-load-image-background blur lazy-load-image-loaded"><img alt="banner" src="<?=base_url($row['image'])?>" height="245" width="100"></span>
							</a>
							<?php 
							if($i==1)
							{
								?>
							<div class="banner-content banner-content-top">
								<h4 class="banner-subtitle text-grey">Discover Our Wide Range of</h4>
								<h3 class="banner-title">Feeding Collection</h3><a class="btn btn-outline-white banner-link" href="<?=$row['link']?>">Discover Now<i class="icon-long-arrow-right"></i></a>
							</div>
							<?php }?>
							<?php 
							if($i==2)
							{
								?>
							<div class="banner-content banner-content-top">
								<h4 class="banner-subtitle text-grey">Back to School</h4>
								<h3 class="banner-title">Backpacks</h3><a class="btn btn-outline-gray banner-link" href="<?=$row['link']?>">Shop Now<i class="icon-long-arrow-right"></i></a>
							</div>
							<?php }?>
						</div>
					</div>
					<?php 

$i++;

		}?>
				
				
				</div>
			</div>
		</div>
	</div>
</div>	
	 <?php foreach ($sections as $count_key => $row) {
  if (!empty($row['product_details'])) {
            if ($row['style'] == 'default') { 
			if ($row['id'] == 2) {
			?>	 
	<section class="product_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title">
                        <h2><span> <strong><?= ucfirst($row['title']) ?></strong></span></h2>
                    </div>
                    <div class="product_carousel product_column4 owl-carousel">
                        
						 <?php foreach ($row['product_details'] as $product_row) { ?>
						
						<div class="single_product">
                           
                            <div class="product_thumb">
							<?php
$stockupdate=0;
							 if (($product_row['stock_type'] != null && $product_row['stock_type'] != '')) {
            //Case 1 : Simple Product(simple product)
            if ($product_row['stock_type'] == 0) {
                if ($product_row['stock'] != null) {
                    $stock = intval($product_row['stock']);
                    if ($stock <= 0) {
						
					$stockupdate=1;
					
				
									 }
                }
            }
								}
			?>
                                <a class="primary_img" href="<?= base_url('products/details/' . $product_row['slug']) ?>"><img src="<?= $product_row['image_sm'] ?>" alt="" <?php if($stockupdate==1) { ?> class="out_stock_img"<?php } ?>></a>
                                <?php if($stockupdate==1) { ?>
								<div class="label_product">
                                        <span class="label_sale">Out of Stock </span>
                                    </div>
							   <?php } ?>
                                <div class="action_links">
                                        <ul>
                                            <li class="quick_button"><a href="javascript:void(0);" class="quick-view-btn" data-tip="Quick View" data-product-id="<?= $product_row['id'] ?>" data-product-variant-id="<?= $product_row['variants'][0]['id'] ?>" data-izimodal-open="#quick-view"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                            <li class="wishlist"><a href="javascript:void(0);" class="add-to-fav-btn lnr lnr-heart <?= ($product_row['is_favorite'] == 1) ? 'fa text-danger' : '' ?>" data-product-id="<?= $product_row['id'] ?>" title="Add to Wishlist"></a></li>
                                             <?php
                                                    if (count($product_row['variants']) <= 1) {
                                                        $variant_id = $product_row['variants'][0]['id'];
                                                        $modal = "";
														$class="add_to_cart";
                                                    } else {
                                                        $variant_id = "";
                                                        $modal = "#quick-view";
														$class="quick-view-btn";
                                                    }
                                                    ?>
											<li class="compare"><a href="#"  data-tip="Add to Cart" class="<?php echo $class; ?>" data-product-id="<?= $product_row['id'] ?>" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>" title="add to cart"><span class="lnr lnr-cart"></span></a></li>
                                        </ul>
                                    </div>
                            </div>
                            <div class="product_content">
                                 <div class="product_name">
                                <h3><a href="<?= base_url('products/details/' . $product_row['slug']) ?>"><?= $product_row['name'] ?></a></h3>
                            </div>
                                <div class="product_footer d-flex align-items-center">
                                    <div class="price_box">
									<?php $price = get_price_range_of_product($product_row['id']);
                                                
                                                ?>
                                        <?php $price = get_price_range_of_product($product_row['id']);
                                                
                                                ?>
												 <?php if ($product_row['type'] == "simple_product") { 
												 if($product_row['min_max_price']['special_price']!=0)
												 {
												 ?>
												 <span class="old_price"><?= $settings['currency'] ?> <?= number_format($product_row['min_max_price']['min_price'],3) ?></span>
                                        <span class="regular_price red"><?= $settings['currency'] ?> <?= number_format($product_row['min_max_price']['special_price'],3) ?></span>
										
												 <?php
												 }
												 else{?>
												<span class="regular_price"><?= $settings['currency'] ?> <?= number_format($product_row['min_max_price']['min_price'],3) ?></span>	 
												 <?php }
												 }
												else
												{
													?>
													<span class="regular_price"><?php echo $price['range'];?></span>
													<?php
												}
												 ?>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
						 <?php } ?>
						
						
						
						
                        
						
						
						
                       
                        
						
						
						
						
                        
                        
                    
					
					
					
					</div>
                </div>
            </div>

        </div>
    </section>
	 <?php }
	 
	 if($row['id']==3)
	 {
		 ?>
	<section class="small_product_area mb-50 bg_gray_img" style="padding:50px 0;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title">
                        <h2><span class="bg_gray_img"> <strong><?= ucfirst($row['title']) ?></strong></span></h2>
                    </div>
                    <div class="product_carousel small_product product_column3 owl-carousel">
					 <?php foreach ($row['product_details'] as $product_row) { ?>
                        <div class="single_product">
                            <div class="product_content">
                                <h3><a href="<?= base_url('products/details/' . $product_row['slug']) ?>"><?= $product_row['name'] ?></a></h3>
                                <!--<div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>-->
                                <div class="price_box">
								<?php $price = get_price_range_of_product($product_row['id']);
                                                
                                                ?>
                                   <?php $price = get_price_range_of_product($product_row['id']);
                                                
                                                ?>
												 <?php if ($product_row['type'] == "simple_product") { 
												 if($product_row['min_max_price']['special_price']!=0)
												 {
												 ?>
                                         <span class="old_price"><?= $settings['currency'] ?> <?= number_format($product_row['min_max_price']['min_price'],3) ?></span>
                                        <span class="regular_price red"><?= $settings['currency'] ?> <?= number_format($product_row['min_max_price']['special_price'],3) ?></span>
												 <?php
												 }
												 else{?>
												<span class="regular_price"><?= $settings['currency'] ?> <?= number_format($product_row['min_max_price']['min_price'],3) ?></span>	 
												 <?php }
												 }
												else
												{
													?>
													<span class="regular_price"><?php echo $price['range'];?></span>
													<?php
												}
												 ?>
                                   
                                </div>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="<?= base_url('products/details/' . $product_row['slug']) ?>"><img src="<?= $product_row['image_sm'] ?>" alt=""></a>
                            </div>
                        </div>
						 <?php } ?>
                        
                       
                       
                    
					
					
					</div>
                </div>
            </div>
        </div>
    </section>	 
		 
	<?php	 
	 }
	 if($row['id']==1)
	 {?>
 <section class="product_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title">
                        <h2><span> <strong><?= ucfirst($row['title']) ?></strong></span></h2>
                       
                    </div>

                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="brake" role="tabpanel">
                    <div class="product_carousel product_column4 owl-carousel">
                       
                          <div class="single_product_list"> 
						 <?php
							$i=1;
						 foreach ($row['product_details'] as $product_row) { ?>	
							
                       
                        
                            <div class="single_product">
                                
                                <div class="product_thumb">
								<?php
$stockupdate=0;
							 if (($product_row['stock_type'] != null && $product_row['stock_type'] != '')) {
            //Case 1 : Simple Product(simple product)
            if ($product_row['stock_type'] == 0) {
                if ($product_row['stock'] != null) {
                    $stock = intval($product_row['stock']);
                    if ($stock <= 0) {
						
					$stockupdate=1;
					
				
									 }
                }
            }
								}
			?>
                                    <a class="primary_img" href="<?= base_url('products/details/' . $product_row['slug']) ?>"><img src="<?= $product_row['image_sm'] ?>" <?php if($stockupdate==1) { ?> class="out_stock_img"<?php } ?> alt=""></a>
									<?php if($stockupdate==1) { ?>
								<div class="label_product">
                                        <span class="label_sale">Out of Stock </span>
                                    </div>
							   <?php } ?>
                                    <!--<a class="secondary_img" href="#"><img src="img/product/product7.jpg" alt=""></a>-->
                                    

                                    <div class="action_links">
                                        <ul>
                                            <li class="quick_button"><a href="javascript:void(0);" class="quick-view-btn" data-tip="Quick View" data-product-id="<?= $product_row['id'] ?>" data-product-variant-id="<?= $product_row['variants'][0]['id'] ?>" data-izimodal-open="#quick-view"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                            <li class="wishlist"><a href="javascript:void(0);" class="add-to-fav-btn  <?= ($product_row['is_favorite'] == 1) ? 'fa text-danger' : '' ?>" data-product-id="<?= $product_row['id'] ?>" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
											<?php
                                                    if (count($product_row['variants']) <= 1) {
                                                        $variant_id = $product_row['variants'][0]['id'];
                                                        $modal = "";
														$class="add_to_cart";
														
                                                    } else {
                                                        $variant_id = "";
                                                        $modal = "#quick-view";
														$class="quick-view-btn";
                                                    }
                                                    ?>
                                            <li class="compare"><a href="#"  data-tip="Add to Cart" class="<?php echo $class; ?>" data-product-id="<?= $product_row['id'] ?>" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>" title="add to cart"><span class="lnr lnr-cart"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="product_content">
                                  <div class="product_name">
                                    <h3><a href="<?= base_url('products/details/' . $product_row['slug']) ?>"><?= $product_row['name'] ?></a></h3>
                                    <!--<p class="manufacture_product"><a href="#">Accessories</a></p>-->
                                </div>
                                    <div class="product_footer d-flex align-items-center">
                                        <div class="price_box">
										<?php $price = get_price_range_of_product($product_row['id']);
                                                
                                                ?>
                                           <?php $price = get_price_range_of_product($product_row['id']);
                                                
                                                ?>
												 <?php if ($product_row['type'] == "simple_product") { 
												 if($product_row['min_max_price']['special_price']!=0)
												 {
												 ?>
                                         <span class="old_price"><?= $settings['currency'] ?> <?= number_format($product_row['min_max_price']['min_price'],3) ?></span>
                                        <span class="regular_price red"><?= $settings['currency'] ?> <?= number_format($product_row['min_max_price']['special_price'],3) ?></span>
												 <?php
												 }
												 else{?>
												<span class="regular_price"><?= $settings['currency'] ?> <?= number_format($product_row['min_max_price']['min_price'],3) ?></span>	 
												 <?php }
												 }
												else
												{
													?>
													<span class="regular_price"><?php echo $price['range'];?></span>
													<?php
												}
												 ?>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                            
                        
                       <?php $i++; 
					   if($i==3) { echo "</div><div class='single_product_list'>"; $i=1; } 
					   } ?>
                        
                
				
				
				
				
				
				
				
                
             </div>   
            </div>
        </div>
		  </div>
         
    </section>
    <!--product area end-->
	
	<?php
	 }


	 }} 
	 
	 
	 }

	 ?>
	
	
    <!--small product area end-->
	
    <!--product area start-->
    
	
	
	
	 


    <!--product area start-->
    
    <!--product area end-->
    <!--banner area start-->
	<?php /*
    <section class="banner_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner_container">
					
                        <div class="single_banner">
                            <div class="banner_thumb">
                                <a href="products-details.html"><img src="img/bg/banner3.jpg" alt=""></a>
                                <div class="banner_text">
                                    <h3>20% off</h3>
                                    <h2>Neon Tulle Ruffle Tutu</h2>
                                    <a href="products-details.html">Shop Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="single_banner">
                            <div class="banner_thumb">
                                <a href="products-details.html"><img src="img/bg/banner4.jpg" alt=""></a>
                                <div class="banner_text">
                                    <h3>10% Off</h3>
                                    <h2>New-Borns Gifts</h2>
                                    <a href="products-details.html">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	*/?>
    <!--banner area end-->

    <!--product area start-->
    
    <!--product area end-->

    <!--banner area start-->
  
    <!--banner area end-->

    <!--custom product area-->
  
    <!--custom product end-->
    
    <!--brand area start-->
    <div class="brand_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
				
				<div class="section_title">
                        <h2><span> <strong>Our Brands</strong></span></h2>
                    </div>
				
                    <div class="brand_container owl-carousel">
                       <?php if (isset($brands) && !empty($brands)) { ?>
                        <?php foreach ($brands as $row) { ?> 
						
						<div class="single_brand">
                            <a href="<?=base_url('home/brands')?>"><img src="<?= $row['image'] ?>" alt=""></a>
                        </div>
                       <?php } ?>
                    <?php } ?>
                       
                        
                        
                        
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--brand area end-->
    <div class="banner-group mb-50 destop_hide">
	<div class="container-fluid">
		<div class="row">
		<?php $offers =  get_offers(); 
		$offernew=array();
		$offernew[]=$offers[0];
		$offernew[]=$offers[1];
		?>
		 <?php
		$i=1;
		 foreach($offernew as $row){
		?>
			<div class="col-sm-6 col-lg-4">
				<div class="banners banner-large banner-overlay banner-overlay-light banner-lg banner-1">
					<a href="<?=$row['link']?>">
						<div class="lazy-overlay"></div><span class=" lazy-load-image-background blur lazy-load-image-loaded"><img alt="banner" src="<?=base_url($row['image'])?>" width="100" height="510"></span>
					</a>
					<?php
					if($i==1)
					{?>
					<div class="banner-content banner-content-top">
						<h4 class="banner-subtitle">Fun</h4>
						<h3 class="banner-title">Water Play Toys</h3>
						
					</div>
					<?php }?>
					<?php
					if($i==2)
					{?>
					<div class="banner-content banner-content-top">
						<h4 class="banner-subtitle">Shop</h4>
						<h3 class="banner-title">Baby Bibs</h3>
						
					</div>
					<?php }?>
				</div>
			</div>
			
		<?php 

$i++;

		}?>
		<div class="col-sm-12 col-md-12 col-lg-4">
				<div class="row">	
		 <?php
		 $offernew1=array();
		$offernew1[]=$offers[2];
		$offernew1[]=$offers[3];
		$i=1;
		 foreach($offernew1 as $row){
		?>
					<div class="col-lg-12 col-md-6 col-sm-6">
						<div class="banners banner-overlay">
							<a href="<?=$row['link']?>">
								<div class="lazy-overlay bg-3"></div><span class=" lazy-load-image-background blur lazy-load-image-loaded"><img alt="banner" src="<?=base_url($row['image'])?>" height="245" width="100"></span>
							</a>
							<?php 
							if($i==1)
							{
								?>
							<div class="banner-content banner-content-top">
								<h4 class="banner-subtitle text-grey">Discover Our Wide Range of</h4>
								<h3 class="banner-title">Feeding Collection</h3><a class="btn btn-outline-white banner-link" href="<?=$row['link']?>">Discover Now<i class="icon-long-arrow-right"></i></a>
							</div>
							<?php }?>
							<?php 
							if($i==2)
							{
								?>
							<div class="banner-content banner-content-top">
								<h4 class="banner-subtitle text-grey">Back to School</h4>
								<h3 class="banner-title">Backpacks</h3><a class="btn btn-outline-gray banner-link" href="<?=$row['link']?>">Shop Now<i class="icon-long-arrow-right"></i></a>
							</div>
							<?php }?>
						</div>
					</div>
					<?php 

$i++;

		}?>
				
				
				</div>
			</div>
		</div>
	</div>
</div>	