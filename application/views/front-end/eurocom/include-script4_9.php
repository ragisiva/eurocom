
 <script type="text/javascript" src="<?= THEME_ASSETS_URL ;?>js/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="<?= THEME_ASSETS_URL ;?>js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="<?= THEME_ASSETS_URL ;?>js/jquery.fancybox.js"></script>
    <script type="text/javascript" src="<?= THEME_ASSETS_URL ;?>js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?= THEME_ASSETS_URL ;?>js/owl.carousel.js"></script>
    <script type="text/javascript" src="<?= THEME_ASSETS_URL ;?>js/TimeCircles.js"></script>
    <script type="text/javascript" src="<?= THEME_ASSETS_URL ;?>js/jquery.countdown.js"></script>
    <script type="text/javascript" src="<?= THEME_ASSETS_URL ;?>js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="<?= THEME_ASSETS_URL ;?>js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?= THEME_ASSETS_URL ;?>js/slideshow/jquery.themepunch.revolution.js"></script>
    <script type="text/javascript" src="<?= THEME_ASSETS_URL ;?>js/slideshow/jquery.themepunch.plugins.min.js"></script>

    <!-- Sweeta Alert 2 -->
<script src="<?= THEME_ASSETS_URL . 'js/sweetalert2.min.js' ?>"></script>
    <script src="<?= THEME_ASSETS_URL ;?>js/magiczoomplus.js" type="text/javascript"></script>
    
<!-- Firebase.js -->
<!-- <script src="<?= THEME_ASSETS_URL . 'js/firebase-app.js'?>"></script>
<script src="<?= THEME_ASSETS_URL . 'js/firebase-auth.js'?>"></script>
<script src="<?= base_url('firebase-config.js')?>"></script>-->
<script type="text/javascript">
    base_url = "<?= base_url() ?>";
    currency = "<?= $settings['currency'] ?>";
    csrfName = "<?= $this->security->get_csrf_token_name() ?>";
    csrfHash = "<?= $this->security->get_csrf_hash() ?>";
</script> 
<script type="text/javascript" src="<?= THEME_ASSETS_URL ;?>js/theme.js"></script>

<script type="text/javascript">
        var mzOptions = {};
        mzOptions = {
            onZoomReady: function() {
                console.log('onReady', arguments[0]);
            },
            onUpdate: function() {
                console.log('onUpdated', arguments[0], arguments[1], arguments[2]);
            },
            onZoomIn: function() {
                console.log('onZoomIn', arguments[0]);
            },
            onZoomOut: function() {
                console.log('onZoomOut', arguments[0]);
            },
            onExpandOpen: function() {
                console.log('onExpandOpen', arguments[0]);
            },
            onExpandClose: function() {
                console.log('onExpandClosed', arguments[0]);
            }
        };
        var mzMobileOptions = {};

        function isDefaultOption(o) {
            return magicJS.$A(magicJS.$(o).byTag('option')).filter(function(opt) {
                return opt.selected && opt.defaultSelected;
            }).length > 0;
        }

        function toOptionValue(v) {
            if (/^(true|false)$/.test(v)) {
                return 'true' === v;
            }
            if (/^[0-9]{1,}$/i.test(v)) {
                return parseInt(v, 10);
            }
            return v;
        }

        function makeOptions(optType) {
            var value = null,
                isDefault = true,
                newParams = Array(),
                newParamsS = '',
                options = {};
            magicJS.$(magicJS.$A(magicJS.$(optType).getElementsByTagName("INPUT"))
                    .concat(magicJS.$A(magicJS.$(optType).getElementsByTagName('SELECT'))))
                .forEach(function(param) {
                    value = ('checkbox' == param.type) ? param.checked.toString() : param.value;

                    isDefault = ('checkbox' == param.type) ? value == param.defaultChecked.toString() :
                        ('SELECT' == param.tagName) ? isDefaultOption(param) : value == param.defaultValue;

                    if (null !== value && !isDefault) {
                        options[param.name] = toOptionValue(value);
                    }
                });
            return options;
        }

        function updateScriptCode() {
            var code = '&lt;script&gt;\nvar mzOptions = ';
            code += JSON.stringify(mzOptions, null, 2).replace(/\"(\w+)\":/g, "$1:") + ';';
            code += '\n&lt;/script&gt;';

            magicJS.$('app-code-sample-script').changeContent(code);
        }

        function updateInlineCode() {
            var code = '&lt;a class="MagicZoom" data-options="';
            code += JSON.stringify(mzOptions).replace(/\"(\w+)\":(?:\"([^"]+)\"|([^,}]+))(,)?/g, "$1: $2$3; ").replace(/\{([^{}]*)\}/, "$1").replace(/\s*$/, '');
            code += '"&gt;';

            magicJS.$('app-code-sample-inline').changeContent(code);
        }

        function applySettings() {
            MagicZoom.stop('Zoom-1');
            mzOptions = makeOptions('params');
            mzMobileOptions = makeOptions('mobile-params');
            MagicZoom.start('Zoom-1');
            updateScriptCode();
            updateInlineCode();
            try {
                prettyPrint();
            } catch (e) {}
        }

        function copyToClipboard(src) {
            var
                copyNode,
                range, success;

            if (!isCopySupported()) {
                disableCopy();
                return;
            }
            copyNode = document.getElementById('code-to-copy');
            copyNode.innerHTML = document.getElementById(src).innerHTML;

            range = document.createRange();
            range.selectNode(copyNode);
            window.getSelection().addRange(range);

            try {
                success = document.execCommand('copy');
            } catch (err) {
                success = false;
            }
            window.getSelection().removeAllRanges();
            if (!success) {
                disableCopy();
            } else {
                new magicJS.Message('Settings code copied to clipboard.', 3000,
                    document.querySelector('.app-code-holder'), 'copy-msg');
            }
        }

        function disableCopy() {
            magicJS.$A(document.querySelectorAll('.cfg-btn-copy')).forEach(function(node) {
                node.disabled = true;
            });
            new magicJS.Message('Sorry, cannot copy settings code to clipboard. Please select and copy code manually.', 3000,
                document.querySelector('.app-code-holder'), 'copy-msg copy-msg-failed');
        }

        function isCopySupported() {
            if (!window.getSelection || !document.createRange || !document.queryCommandSupported) {
                return false;
            }
            return document.queryCommandSupported('copy');
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.dropdown-menu li a').click(function(event) {
                var option = $(event.target).text();
                $(event.target).parents('.btn-group').find('.dropdown-toggle').html(option + '');
            });
        });
    </script>

    <script>
        var myVar;

        function myFunction() {
            myVar = setTimeout(showPage, 3000);
        }

        function showPage() {
            document.getElementById("loader").style.display = "none";
            document.getElementById("myDiv").style.display = "block";
        }
    </script>
