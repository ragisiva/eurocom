<?php $web_settings = get_settings('web_settings', true); ?>
<?php
if (isset($_SESSION['cartsession'])) {
                $cart_items = $this->cart_model->get_user_cart($_SESSION['cartsession']);
}
if (isset($_SESSION['cartsession'])) {
if(count($cart_items)==0)
{
$count=0;
}
else{
	$count=count($cart_items);
} 
}
else
{
$count=0;	
}
				?>
<!-- footer starts -->
<?php $logo = get_settings('web_logo'); ?>
<section class="service-section no-border">
    <?php if($web_settings['return_mode'] ==1){ ?>
    <div class="col-6 col-xl-3">
        <div class="service-widget">
            <i class="service-icon fa fa-money"></i>
            <div class="service-content">
                <h3 class="service-title">
                    <?= $web_settings['return_title'] ?>
                </h3>
                <p>
                    <?= $web_settings['return_description'] ?>
                </p>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if($web_settings['shipping_mode'] ==1){ ?>
    <div class="col-6 col-xl-3">
        <div class="service-widget">
            <i class="service-icon fa fa-truck"></i>
            <div class="service-content">
                <h3 class="service-title">
                    <?= $web_settings['shipping_title'] ?>
                </h3>
                <p>
                    <?= $web_settings['shipping_title'] ?>
                </p>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if($web_settings['support_mode'] ==1){ ?>
    <div class="col-6 col-xl-3">
        <div class="service-widget">
            <i class="service-icon fa fa-credit-card"></i>
            <div class="service-content">
                <h3 class="service-title">
                    <?= $web_settings['support_title'] ?>
                </h3>
                <p>
                    <?= $web_settings['support_description'] ?>
                </p>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if($web_settings['safety_security_mode'] ==1){ ?>
    <div class="col-6 col-xl-3">
        <div class="service-widget">
            <i class="service-icon fa fa-shield"></i>
            <div class="service-content">
                <h3 class="service-title">
                    <?= $web_settings['safety_security_title'] ?>
                </h3>
                <p>
                    <?= $web_settings['safety_security_description'] ?>
                </p>
            </div>
        </div>
    </div>
    <?php } ?>
</section>

<footer class="footer">
    <div class="footer-middle">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="widget">
                        <div class="footer-logo">
                            <a href="<?= base_url() ?>"><img src="<?= base_url($logo) ?>" alt=""></a>
                        </div>
                        <ul class="contact-info">
                            <li>
                                <span class="contact-info-label">
                                    <?=!empty($this->lang->line('phone')) ? $this->lang->line('phone') : 'Phone'?>
                                </span><a href="tel:<?= $web_settings['support_number'] ?>">
                                    <?= $web_settings['support_number'] ?>
                                </a>
                            </li>
                            <li>
                                <span class="contact-info-label">
                                    <?=!empty($this->lang->line('email')) ? $this->lang->line('email') : 'Email'?>
                                </span> <a href="mailto:<?= $web_settings['support_email'] ?>"><span
                                        class="__cf_email__" data-cfemail="4900272f260920273d252c3c3b26672a2624">
                                        <?= $web_settings['support_email'] ?>
                                    </span></a>
                            </li>
                        </ul>
                        <div class="social-icons">
                            <a href="#" class="social-icon social-instagram fab fa-facebook-f" target="_blank"
                                title=" <?=!empty($this->lang->line('facebook')) ? $this->lang->line('facebook') : 'facebook'?>"></a>
                            <a href="#" class="social-icon social-facebook fab fa-instagram" target="_blank"
                                title=" <?=!empty($this->lang->line('instagram')) ? $this->lang->line('instagram') : 'Instagram'?>"></a>
                        </div>

                    </div>

                </div>

                <div class="footer-nav col-lg-2 col-sm-6">
                    <div class="widget mobile-hide tab-visibile">
                        <h4 class="widget-title">
                            <?=!empty($this->lang->line('information')) ? $this->lang->line('information') : 'INFORMATION'?>
                        </h4>
                        <ul class="links">
                            <li><a href="<?= base_url().'about-us' ?>">
                                    <?=!empty($this->lang->line('about_us')) ? $this->lang->line('about_us') : 'About Us'?>
                                </a></li>
                            <li><a href="<?= base_url().'delivery-information' ?>">
                                    <?=!empty($this->lang->line('delivery_information')) ? $this->lang->line('delivery_information') : 'Delivery Informations'?>
                                </a></li>
                            <li><a href="<?= base_url().'privacy-policy' ?>">
                                    <?=!empty($this->lang->line('privacy_policy')) ? $this->lang->line('privacy_policy') : 'Privacy Policy'?>
                                </a></li>
                            <li><a href="<?= base_url().'terms-conditions' ?>">
                                    <?=!empty($this->lang->line('terms_conditions')) ? $this->lang->line('terms_conditions') : 'Terms &amp; Conditions'?>
                                </a></li>
                            <li><a href="<?= base_url().'contact-us' ?>">
                                    <?=!empty($this->lang->line('contact_us')) ? $this->lang->line('contact_us') : 'Contact Us'?>
                                </a></li>
                            <li><a href="<?= base_url().'returns' ?>">
                                    <?=!empty($this->lang->line('returns')) ? $this->lang->line('returns') : 'Returns'?>
                                </a></li>
                        </ul>
                    </div>

                    <div id="accordion" role="tablist" aria-multiselectable="true" class="widget desk-hide tab-hide">
                        <div role="tab" id="headingOne">
                            <h4 class="card-header py-3 border-0 text-uppercase text-bold">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                    aria-expanded="false" aria-controls="collapseOne">
                                    <?=!empty($this->lang->line('information')) ? $this->lang->line('information') : 'INFORMATION'?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="card-block">
                                <a class="footer-nav__link" href="<?= base_url().'about-us' ?>">
                                    <?=!empty($this->lang->line('about_us')) ? $this->lang->line('about_us') : 'About Us'?>
                                </a>
                                <a class="footer-nav__link" href="<?= base_url().'delivery-information' ?>">
                                    <?=!empty($this->lang->line('delivery_information')) ? $this->lang->line('delivery_information') : 'Delivery Informations'?>
                                </a>
                                <a class="footer-nav__link" href="<?= base_url().'privacy-policy' ?>">
                                    <?=!empty($this->lang->line('privacy_policy')) ? $this->lang->line('privacy_policy') : 'Privacy Policy'?>
                                </a>
                                <a class="footer-nav__link" href="<?= base_url().'terms-conditions' ?>">
                                    <?=!empty($this->lang->line('terms_condition')) ? $this->lang->line('terms_condition') : 'Terms &amp; Conditions'?>
                                </a>
                                <a class="footer-nav__link" href="<?= base_url().'contact-us' ?>">
                                    <?=!empty($this->lang->line('contact_us')) ? $this->lang->line('contact_us') : 'Contact Us'?>
                                </a>
                                <a class="footer-nav__link" href="<?= base_url().'returns' ?>">
                                    <?=!empty($this->lang->line('returns')) ? $this->lang->line('returns') : 'Returns'?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer-nav col-lg-2 col-sm-6">
                    <div class="widget mobile-hide tab-visibile">
                        <h4 class="widget-title"><?=!empty($this->lang->line('corporate_info')) ? $this->lang->line('corporate_info') : 'Corporate Info'?></h4>
                        <ul class="links">
                            <li><a href="<?= base_url().'login' ?>">
                                    <?=!empty($this->lang->line('login')) ? $this->lang->line('login') : 'Login'?>
                                </a></li>
                            <li><a href="<?= base_url().'register' ?>">
                                    <?=!empty($this->lang->line('register')) ? $this->lang->line('register') : 'Register'?>
                                </a></li>
                            <li><a href="<?= base_url().'new-arrivals?sort=date-desc' ?>">
                                    <?=!empty($this->lang->line('new_arrivals')) ? $this->lang->line('new_arrivals') : 'New Arrivals'?>
                                </a></li>
                            <li><a href="<?= base_url().'special-offers' ?>">
                                    <?=!empty($this->lang->line('special_offer')) ? $this->lang->line('special_offer') : 'Special Offer'?>
                                </a></li>
                            <li><a href="<?= base_url().'services' ?>">
                                    <?=!empty($this->lang->line('services')) ? $this->lang->line('services') : 'Services'?>
                                </a></li>
                            <li><a href="<?= base_url().'careers' ?>">
                                    <?=!empty($this->lang->line('careers')) ? $this->lang->line('careers') : 'Careers'?>
                                </a></li>
                        </ul>
                    </div>

                    <div id="accordion" role="tablist" aria-multiselectable="true" class="widget desk-hide tab-hide">
                        <div role="tab" id="headingTwo">
                            <h4 class="card-header py-3 border-0 text-uppercase text-bold">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                                    aria-expanded="false" aria-controls="collapseTwo">
                                    <?=!empty($this->lang->line('corporate_info')) ? $this->lang->line('corporate_info') : 'Corporate Info'?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="card-block">
                                <a class="footer-nav__link" href="<?= base_url().'new-arrivals' ?>">
                                    <?=!empty($this->lang->line('new_arrivals')) ? $this->lang->line('new_arrivals') : 'New Arrivals'?>
                                </a>
                                <a class="footer-nav__link" href="<?= base_url().'login' ?>">
                                    <?=!empty($this->lang->line('login')) ? $this->lang->line('login') : 'Login'?>
                                </a>
                                <a class="footer-nav__link" href="<?= base_url().'register' ?>">
                                    <?=!empty($this->lang->line('register')) ? $this->lang->line('register') : 'Register'?>
                                </a>
                                <a class="footer-nav__link" href="<?= base_url().'special-offers' ?>">
                                    <?=!empty($this->lang->line('special_offer')) ? $this->lang->line('special_offer') : 'Special Offer'?>
                                </a>
                                <a class="footer-nav__link" href="<?= base_url().'services' ?>">
                                    <?=!empty($this->lang->line('services')) ? $this->lang->line('services') : 'Services'?>
                                </a>
                                <a class="footer-nav__link" href="<?= base_url().'careers' ?>">
                                    <?=!empty($this->lang->line('careers')) ? $this->lang->line('careers') : 'Careers'?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer-nav col-lg-4 col-sm-6">
                    <div class="widget widget-newsletter mobile-hide tab-visibile">
                        <h4 class="widget-title">
                            <?=!empty($this->lang->line('subscribe_newsletter')) ? $this->lang->line('subscribe_newsletter') : 'Subscribe  Newsletter'?>
                        </h4>
                        <p>
                            <?=!empty($this->lang->line('newletter_info')) ? $this->lang->line('newletter_info') : 'Get all the latest information on events, sales and offers. Sign up for newsletter:'?>
                        </p>
                        <form action="<?= base_url('home/newsletter-submit') ?>" class="mb-0" id="newsletter_form">
                            <input type="text" class="form-control m-b-3" name="newsletter_email" id="newsletter_email" 
                                placeholder="<?=!empty($this->lang->line('email_address')) ? $this->lang->line('email_address') : 'Email Address'?>"
                                required>
                            <input type="submit" id="newsletter-btn" class="btn btn-primary shadow-none"
                                value="<?=!empty($this->lang->line('subscribe')) ? $this->lang->line('subscribe') : 'Subscribe'?>">
                        </form>
                    </div>

                    <div id="accordion" role="tablist" aria-multiselectable="true" class="widget desk-hide tab-hide">
                        <div role="tab" id="headingthree">
                            <h4 class="card-header py-3 border-0 text-uppercase text-bold">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree"
                                    aria-expanded="false" aria-controls="collapsethree">
                                    <?=!empty($this->lang->line('subscribe_newsletter')) ? $this->lang->line('subscribe_newsletter') : 'Subscribe  Newsletter'?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapsethree" class="collapse" role="tabpanel" aria-labelledby="headingthree">
                            <div class="card-block">
                                <div class="widget widget-newsletter">
                                    <p>
                                    <?=!empty($this->lang->line('newletter_info')) ? $this->lang->line('newletter_info') : 'Get all the latest information on events, sales and offers. Sign up for newsletter:'?>
                                    </p>
                                    <form action="<?= base_url('home/newsletter-submit') ?>" class="mb-0" id="newsletter_form">
                                        <input type="text" class="form-control m-b-3" id="newsletter_email"  name="newsletter_email"
                                            placeholder="<?=!empty($this->lang->line('email_address')) ? $this->lang->line('email_address') : 'Email Address'?>"
                                            required>
                                        <input type="submit" id="newsletter-btn" class="btn btn-primary shadow-none"
                                            value="<?=!empty($this->lang->line('subscribe')) ? $this->lang->line('subscribe') : 'Subscribe'?>">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 mobile-hide">
                    <p class="footer-copyright py-3 pr-4 mb-0"><?=!empty($this->lang->line('copy_right')) ? $this->lang->line('copy_right') : "Copyright &copy;
". date('Y')." International Eurocom." ?><?=!empty($this->lang->line('all_right')) ? $this->lang->line('all_right') : 'All Rights Reserved.'?> <?=!empty($this->lang->line('powered_by')) ? $this->lang->line('powered_by') : 'Powered by Chrisans Web Solutions'?>
                    </p>
                </div>
                <div class="col-md-10 desk-hide">
                    <p class="footer-copyright py-3 pr-4 mb-0"><?=!empty($this->lang->line('copy_right')) ? $this->lang->line('copy_right') : "Copyright &copy;
". date('Y')." International Eurocom." ?><br> <?=!empty($this->lang->line('all_right')) ? $this->lang->line('all_right') : 'All Rights Reserved.'?><br> <?=!empty($this->lang->line('powered_by')) ? $this->lang->line('powered_by') : 'Powered by Chrisans Web Solutions'?>
                    </p>
                </div>
                <div class="col-md-2 text-right">
                    <img src="<?= THEME_ASSETS_URL ;?>images/payments.png" alt="payment methods"
                        class="footer-payments py-3">
                </div>
            </div>
        </div>

    </div>

</footer>
<!-- End Footer -->
</div>


<!-- modal area start-->
<div class="modal fade" id="modal_box" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal_body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12">
                            <div class="modal_tab">

                                <div class="tab-content product-details-large" id="modal-images">
                                </div>




                                <div class="modal_tab_button">
                                    <ul class="nav product_navactive owl-carousel" role="tablist" id="tabimages">







                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-12">
                            <div class="modal_right">
                                <div class="modal_title mb-10">
                                    <h2 class="product-title"></h2>
                                </div>
                                <div class="modal_price mb-10">
                                    <span class="new_price" id="modal-product-price"></span>
                                    <span class="old_price" id="modal-product-special-price-div"></span>
                                </div>
                                <div class="modal_description mb-15">
                                    <p id="modal-product-short-description"></p>
                                </div>
                                <div class="variants_selects ">
                                    <div class="row">
                                        <div id="modal-product-variant-attributes"></div>
                                        <div id="modal-product-variants-div"></div>
                                    </div>
                                    <div class="variants_color">

                                    </div>
                                    <div class="modal_add_to_cart">

                                        <input class="in-num" min="1" max="100" step="2" value="1" type="number">
                                        <button id="modal-add-to-cart-button">add to cart</button>

                                    </div>
                                </div>
                                <div class="modal_social">
                                    <h2>Share this product</h2>
                                    <!--from https://www.buttons.social-->
                                    <script>document.write('<a href="https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '"target="_blank"title="Facebook"style="display:inline-block;vertical-align:middle;width:2em;height:2em;border-radius:10%;background:#3b5998;"><svg style="display:block;fill:#fff;height:44%;margin:28% auto;" viewBox="0 -256 864 1664"><path transform="matrix(1,0,0,-1,-95,1280)" d="M 959,1524 V 1260 H 802 q -86,0 -116,-36 -30,-36 -30,-108 V 927 H 949 L 910,631 H 656 V -128 H 350 V 631 H 95 v 296 h 255 v 218 q 0,186 104,288.5 104,102.5 277,102.5 147,0 228,-12 z" /></svg></a> <a href="https://twitter.com/share?url=' + encodeURIComponent(document.URL) + '&text=' + encodeURIComponent(document.title) + '"target="_blank"title="Twitter"style="display:inline-block;vertical-align:middle;width:2em;height:2em;border-radius:10%;background:#1b95e0;"><svg style="display:block;fill:#fff;height:36%;margin:32% auto;" viewBox="0 -256 1576 1280"><path transform="matrix(1,0,0,-1,-44,1024)" d="m 1620,1128 q -67,-98 -162,-167 1,-14 1,-42 0,-130 -38,-259.5 Q 1383,530 1305.5,411 1228,292 1121,200.5 1014,109 863,54.5 712,0 540,0 269,0 44,145 q 35,-4 78,-4 225,0 401,138 -105,2 -188,64.5 -83,62.5 -114,159.5 33,-5 61,-5 43,0 85,11 Q 255,532 181.5,620.5 108,709 108,826 v 4 q 68,-38 146,-41 -66,44 -105,115 -39,71 -39,154 0,88 44,163 Q 275,1072 448.5,982.5 622,893 820,883 q -8,38 -8,74 0,134 94.5,228.5 94.5,94.5 228.5,94.5 140,0 236,-102 109,21 205,78 -37,-115 -142,-178 93,10 186,50 z" /></svg></a> ' + (/mobile|android|blackberry/i.test(navigator.userAgent) ? '<a href="whatsapp://send?text=' + encodeURIComponent(document.URL) + '"title="WhatsApp"style="display:inline-block;vertical-align:middle;width:2em;height:2em;border-radius:10%;background:#43d854;"><svg style="display:block;fill:#fff;height:44%;margin:28% auto;" viewBox="0 -256 1536 1548"><path transform="matrix(1,0,0,-1,0,1158)" d="m 985,562 q 13,0 98,-44 84,-44 89,-53 2,-5 2,-15 0,-33 -17,-76 -16,-39 -71,-65.5 -55,-26.5 -102,-26.5 -57,0 -190,62 -98,45 -170,118 -72,73 -148,185 -72,107 -71,194 v 8 q 3,91 74,158 24,22 52,22 6,0 18,-1 12,-2 19,-2 19,0 26.5,-6 7.5,-7 15.5,-28 8,-20 33,-88 25,-68 25,-75 0,-21 -34.5,-57.5 Q 599,735 599,725 q 0,-7 5,-15 34,-73 102,-137 56,-53 151,-101 12,-7 22,-7 15,0 54,48.5 39,48.5 52,48.5 z M 782,32 q 127,0 244,50 116,50 200,134 84,84 134,200.5 50,116.5 50,243.5 0,127 -50,243.5 -50,116.5 -134,200.5 -84,84 -200,134 -117,50 -244,50 -127,0 -243.5,-50 Q 422,1188 338,1104 254,1020 204,903.5 154,787 154,660 154,457 274,292 L 195,59 437,136 Q 595,32 782,32 z m 0,1382 q 153,0 293,-60 139,-60 240,-161 101,-101 161,-240.5 Q 1536,813 1536,660 1536,507 1476,367.5 1416,228 1315,127 1214,26 1075,-34 935,-94 782,-94 587,-94 417,0 L 0,-134 136,271 Q 28,449 28,660 q 0,153 60,292.5 60,139.5 161,240.5 101,101 240.5,161 139.5,60 292.5,60 z" /></svg></a> ' : '') + '<a href="https://www.linkedin.com/shareArticle?url=' + encodeURIComponent(document.URL) + '&title=' + encodeURIComponent(document.title) + '"target="_blank"title="LinkedIn"style="display:inline-block;vertical-align:middle;width:2em;height:2em;border-radius:10%;background:#0077b5;"><svg style="display:block;fill:#fff;height:42%;margin:29% auto;" viewBox="0 -256 1536 1468"><path transform="matrix(1,0,0,-1,0,1132)" d="M 349,911 V -80 H 19 v 991 h 330 z m 21,306 q 1,-73 -50.5,-122 Q 268,1046 184,1046 h -2 q -82,0 -132,49 -50,49 -50,122 0,74 51.5,123 51.5,48 134.5,48 83,0 133,-48 50,-49 51,-123 z M 1536,488 V -80 h -329 v 530 q 0,105 -40,164.5 Q 1126,674 1040,674 977,674 934.5,639.5 892,605 871,554 860,524 860,473 V -80 H 531 q 2,399 2,647 0,248 -1,296 l -1,48 H 860 V 767 h -2 q 20,32 41,56 21,24 56.5,52 35.5,28 87.5,43.5 51,15.5 114,15.5 171,0 275,-113.5 Q 1536,707 1536,488 z" /></svg></a> <a href="https://plus.google.com/share?url=' + encodeURIComponent(document.URL) + '"target="_blank"title="Google+"style="display:inline-block;vertical-align:middle;width:2em;height:2em;border-radius:10%;background:#dd4b39;"><svg style="display:block;fill:#fff;height:38%;margin:31% auto;" viewBox="0 -256 2304 1466"><path transform="matrix(1,0,0,-1,0,1117)" d="M 1437,623 Q 1437,415 1350,252.5 1263,90 1102,-1.5 941,-93 733,-93 584,-93 448,-35 312,23 214,121 116,219 58,355 0,491 0,640 q 0,149 58,285 58,136 156,234 98,98 234,156 136,58 285,58 286,0 491,-192 L 1025,990 Q 908,1103 733,1103 610,1103 505.5,1041 401,979 340,872.5 279,766 279,640 279,514 340,407.5 401,301 505.5,239 610,177 733,177 q 83,0 152.5,23 69.5,23 114.5,57.5 45,34.5 78.5,78.5 33.5,44 49,83 15.5,39 21.5,74 H 733 v 252 h 692 q 12,-63 12,-122 z m 867,122 V 535 H 2095 V 326 h -210 v 209 h -209 v 210 h 209 v 209 h 210 V 745 h 209 z" /></svg></a>');</script>
                                    <!--end buttons.social-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="overlays"></div>
<?php
    $this->load->model('category_model');
    $categories = $this->category_model->get_categories(null, 30);
    $language = get_languages();
    $cookie_lang = $this->input->cookie('language', TRUE);
    $web_settings = get_settings('web_settings', true);
    $logo = get_settings('web_logo'); 
    ?>
<aside class="sidebar-menu-wrap">
    <div class="sidebar-menu-header">
        <h4><?=!empty($this->lang->line('menu')) ? $this->lang->line('menu') : 'Menu'?></h4>

        <div class="sidebar-menu-close">
            <i class="fa fa-close"></i>
        </div>
    </div>
    <?php
		if($categories){
            ?>
    <ul class="list-inline sidebar-menu sidebar-nav">
        <?php
		foreach ($categories as $row) { 
            $child=$row['children']; 
            if(count($child)>0)
            { ?>
        <li class="dropdown multi-level">
            <a href="<?= base_url('products/category/' . $row['slug']) ?>" class="dropdown-toggle menu-item"
                data-toggle="dropdown">
                <span class="menu-item-icon">
                    <?php if($row['cat_icon'] !=''){?>
                    <i class="<?=  $row['cat_icon'] ?>"></i>
                    <?php  }?>
                </span>
                <?= ($lang_prefix=='') ? ucfirst(strtolower($row[$lang_prefix.'name'])) : $row[$lang_prefix.'name']  ?>
            </a>

            <ul class="list-inline dropdown-menu animated fadeInLeft" style="display: none;">
                <?php foreach ($child as $row1) { ?>
                <li><a href="<?= base_url('products/category/' . $row1['slug']) ?>">
                        <?= ($lang_prefix=='') ?strtoupper($row1['name']) : $row1[$lang_prefix.'name'] ?>
                    </a></li>
                <?php } ?>
                <li><a href="<?= base_url('products/category/' . $row['slug']) ?>"><?=!empty($this->lang->line('view_all')) ? $this->lang->line('view_all') : ' view All'?></a></li>
            </ul>
        </li>
        <?php } else{?>
        <li>
            <a href="<?= base_url('products/category/' . $row['slug']) ?>" class="menu-item" target="_self">
                <span class="menu-item-icon">
                    <?php if($row['cat_icon'] !=''){?>
                    <i class="<?=  $row['cat_icon'] ?>"></i>
                    <?php  }?>
                </span>
                <?= ($lang_prefix=='') ? ucfirst(strtolower($row[$lang_prefix.'name'])) : $row[$lang_prefix.'name']  ?>
            </a>


        </li>
        <?php }
} ?>


    </ul>
    <?php } ?>

</aside>



<div id="cart_side" class="add_to_cart right">
    <a href="javascript:void(0)" class="overlay" onclick="closeCart()"></a>
    <div class="cart-inner">
        <div class="cart_top">
            <h3><?=!empty($this->lang->line('cart')) ? $this->lang->line('cart') : 'cart'?></h3>
            <div class="close-cart">
                <a href="javascript:void(0)" onclick="closeCart()">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <div class="cart_media">
            <?php
			$total=0;
			if ($count != 0) {
                   
            ?>
            <ul class="cart_product">
                <?php
            foreach ($cart_items as $items) { ?>
                <li class="cart_item" id="popcart_<?= $items['product_variant_id'] ?>">
                    <div class="media">
                        <a href="#">
                            <?php
                            if($items['pv_images']!='' && $items['pv_images'] !=null){
                                $pv_image = explode('"',$items['pv_images']);  
                                $pv_image = $pv_image[1] ;                         
                            }
                           else{
                               $pv_image = $items['image'];
                           }
                            if($pv_image!='' && $pv_image != base_url() && file_exists($pv_image)){?>
                                <img class="mr-3" src="<?= base_url($pv_image) ?>"
                                alt="<?= html_escape($items['name']) ?>">
                           <?php }
                           
                        else{?>
                            <img class="mr-3" src="<?= base_url().'/assets/no-image.png' ?>"
                                alt="<?= html_escape($items['name']) ?>">
                            <?php } ?>
                        </a>
                        <div class="media-body">
                            <a href="#">
                                <h4>
                                    <?= html_escape($items[$lang_prefix.'name']) ?>
                                    <?php if (!empty($items['product_variants'])) { ?> 
                                    <?= str_replace(',', '  ', $items['product_variants'][0][$lang_prefix.'variant_values']) ?>
                                    <?php } ?>
                                </h4>
                                <small>
                                   
                                </small>
                            </a>
                            <h4 class="cart_info">
                                <?php
                                if($items['special_price']>0 && $items['special_price']<$items['price']){
                                    $cprice= $items['special_price'];
                                }
                                else{
                                    $cprice= $items['price'];
                                }
                                ?>
                                <span class="price_cart" data-price="<?php echo $items['qty'] * $cprice; ?>">
                                    <?= $items['qty'] ?> x
                                    <?= $settings['currency'] . ' ' .$cprice ?>
                                </span>
                            </h4>
                        </div>

                        <div class="attr-product pull-right">

                            <div class="info-qty product-sm-quantity">
                                <a class="qty-down" href="#"><i class="fa fa-minus"></i></a>
                                <span class="qty-val">
                                    <?= $items['qty'] ?>
                                </span>
                                <a class="qty-up" href="#"><i class="fa fa-plus"></i></a>
                                <input type="hidden" name="qty" value="<?= $items['qty'] ?>" data-page="cart"
                                    data-id="<?= $items['product_variant_id'] ?>" data-price="<?= $cprice; ?>"
                                    data-step="<?= (isset($items['minimum_order_quantity']) && !empty($items['quantity_step_size'])) ? $items['quantity_step_size'] : 1 ?>"
                                    data-min="<?= (isset($items['minimum_order_quantity']) && !empty($items['minimum_order_quantity'])) ? $items['minimum_order_quantity'] : 1 ?>"
                                    data-max="<?= (isset($items['total_allowed_quantity']) && !empty($items['total_allowed_quantity'])) ? $items['total_allowed_quantity'] : '' ?>">
                            </div>
                        </div>

                    </div>
                    <div class="close-circle cart_remove">
                        <a href="javascript:void(0)" class="remove-product"
                            data-id="<?= $items['product_variant_id'] ?>">
                            <i class="fas fa-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <?php $total = $total + ($items['qty']*$cprice);
             } ?>
            </ul>
            <ul class="cart_total">
                <li>
                    <div class="total">
                        <h5><?=!empty($this->lang->line('subtotal')) ? $this->lang->line('subtotal') : ' Subtotal'?> : <span>
                                <?= $settings['currency'] ?> <span class="cart-subtotal">
                                    <?php echo number_format($total,3); ?>
                                </span>
                            </span></h5>
                    </div>
                </li>
                <li>
                    <div class="buttons">
                        <a href="<?= base_url('cart') ?>" class="btn btn-solid btn-block btn-solid-sm view-cart"><?=!empty($this->lang->line('view_cart')) ? $this->lang->line('view_cart') : ' View Cart'?></a>
                        <a href="<?= base_url('cart/checkout') ?>"
                            class="btn btn-solid btn-solid-sm btn-block checkout"><?=!empty($this->lang->line('checkout')) ? $this->lang->line('checkout') : ' Checkout'?></a>
                    </div>
                </li>
            </ul>
            <?php } 
            else{
                ?>
            <h1 class="h4 text-center">
                <?= !empty($this->lang->line('empty_cart_message')) ? $this->lang->line('empty_cart_message') : 'Your Cart Is Empty' ?>
            </h1>
            <?php 
            }?>
        </div>
    </div>
</div>



<div id="wish_side" class="add_to_cart right">
    <a href="javascript:void(0)" class="overlay" onclick="closeWish()"></a>
    <div class="cart-inner">
        <div class="cart_top">
            <h3>  <?= !empty($this->lang->line('wishlist')) ? $this->lang->line('wishlist') : 'Wishlist' ?></h3>
            <div class="close-cart">
                <a href="javascript:void(0)" onclick="closeWish()">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <div class="cart_media">
            <?php
         if ($this->data['is_logged_in']) {
            $favorite_products = get_favorites($this->data['user']->id);
            if (isset($favorite_products) && !empty($favorite_products)) {?>
            <ul class="cart_product">
                <?php
                foreach($favorite_products as $favorite_product){
                    if ($favorite_product['stock_type'] == 0) {
                        if ($favorite_product['stock'] != null) {
                            $fstock = intval($favorite_product['stock']);
                            if ($fstock <= 0) {
                                
                            $fstockupdate=1;
                            
                        
                                             }
                        }
                    }?>
                <li id="fav-<?= $favorite_product['variant_id'] ?>">
                    <div class="media">
                        <a href="<?= base_url('products/details/' . $favorite_product['slug'].'/'.$favorite_product['variant_id']) ?>">
                            <img src="<?= $favorite_product['image_sm'] ?>"></a>
                        <div class="media-body">
                            <a class="product-name"
                                href="<?= base_url('products/details/' . $favorite_product['slug'].'/'.$favorite_product['variant_id']) ?>">
                                <h4>
                                    <?= $favorite_product[$lang_prefix.'name'].' '.$favorite_product[$lang_prefix.'variant_values'] ?>
                                </h4>
                            </a>
                            <?php
                                    $modal = "";
									$class="add_to_cart";
                            ?>
                            <a href="javascript:void(0);" class="btn btn-dark btn-sm add-to-cart" data-product-qty="1"
                                data-product-id="<?= $favorite_product['id'] ?>"
                                data-product-variant-id="<?= $favorite_product['variant_id'] ?>" data-izimodal-open="<?= $modal ?>">
                                <?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'Add to Cart' ?>
                            </a>
                           

                        </div>


                    </div>
                    <div class="close-circle">
                        <a href="javascript:void(0);" class="btn btn-view delete-from-fav fa text-danger"
                            data-product-id="<?= $favorite_product['id'] ?>" data-product-variant-id="<?= $favorite_product['variant_id'] ?>"><i class="fas fa-trash"></i></a>
                    </div>
                </li>
                <?php } ?>
            </ul>
            <ul class="cart_total">

                <li>
                    <div class="buttons">
                        <a href="<?= base_url('my-account/favorites') ?>"
                            class="btn btn-solid btn-block btn-solid-sm view-cart"><?= !empty($this->lang->line('view_wishlist')) ? $this->lang->line('view_wishlist') : 'View Wishlist' ?></a>

                    </div>
                </li>
            </ul>
            <?php }
             else{
                ?>
            <h1 class="h4 text-center">
                <?= !empty($this->lang->line('empty_wishlist_message')) ? $this->lang->line('empty_wishlist_message') : 'Your wishlist is empty!!' ?>
            </h1>
            <a href="<?= base_url() ?>" class="btn btn-solid btn-block btn-solid-sm view-cart">  <?= !empty($this->lang->line('continue_shopping')) ? $this->lang->line('continue_shopping') : 'Continue Shopping' ?></a>
            <?php 
            }}
             else{
                ?>
            <h1 class="h4 text-center">
            <?= !empty($this->lang->line('login_warning')) ? $this->lang->line('login_warning') : 'To view your wishlist please login' ?>
            </h1>
            <a href="<?= base_url('login') ?>" class="btn btn-solid btn-block btn-solid-sm view-cart"> <?= !empty($this->lang->line('login')) ? $this->lang->line('login') : 'Login' ?></a>
            <?php 
            }?> 
        </div>
    </div>
</div>
<!-- end -->
<!-- main content ends -->
<div id="product_modal">
  
</div>

