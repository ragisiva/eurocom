<?php
    $this->load->model('category_model');
    $categories = $this->category_model->get_categories(null, 30);
    $language = get_languages();
    $cookie_lang = ($this->session->userdata('lang') !='') ? $this->session->userdata('lang', TRUE) : 'english' ;
    $this->lang->load('web_labels_lang', $cookie_lang );
   // echo  $cookie_lang;
    $web_settings = get_settings('web_settings', true);
    $logo = get_settings('web_logo'); 
    $top_sub_categories = $this->category_model->get_top_sub_categories();
    $drop_brands = get_all_brands(24,0);
   // $drop_brands =array_map("unserialize", array_unique(array_map("serialize", $drop_brands)));
    //print_r($brands);
    ?>
 <?php


if ($this->session->userdata('cartsession')!='') {
    
                $cart_items = $this->cart_model->get_user_cart($this->session->userdata('cartsession'));
}
if (isset($_SESSION['cartsession'])) {
if(count($cart_items)==0)
{
$count=0;
}
else{
	$count=count($cart_items);
}
}
else
{
$count=0;	
}
				?>
        <div id="header">
 
            <div class="sub-header4 mobile-hide">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 hidden-xs">
                            <ul class="top-info top-info-left">
                                <li class="top-contact">
                                    <p><i class="fa fa-phone"></i> <?= !empty($this->lang->line('call_us')) ? $this->lang->line('call_us') : 'Call Us' ?>: <?= $web_settings['support_number'] ?></p>
                                </li>


                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="top-info">
                                <ul class="top-info-right">
                                    <li><a href="<?= base_url() ?>"><?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?></li>
                                    <li><a href="<?= base_url().'about-us' ?>"><?= !empty($this->lang->line('about_us')) ? $this->lang->line('about_us') : 'About Us' ?></li>

                                    <li><a href="<?= base_url().'services' ?>"><?= !empty($this->lang->line('services')) ? $this->lang->line('services') : 'Services' ?></li>
                                    <li><a href="<?= base_url().'careers' ?>"><?= !empty($this->lang->line('careers')) ? $this->lang->line('careers') : 'Careers' ?></li>
                                    <li><a href="<?= base_url().'contact-us' ?>"><?= !empty($this->lang->line('contact_us')) ? $this->lang->line('contact_us') : 'Contact Us' ?></li>
                                   <?php
                                   if($cookie_lang =='english'){ ?>
                                    <li class="top-language">
                                        <a class="language-selected" href="javascript:void(0);"  onclick="change_language('arabic')"><img alt="" src="<?= THEME_ASSETS_URL ;?>images/ar.png"> عربى </a>
                                    </li>
                                    <?php }
                                    else{
                                        ?>
                                        <li class="top-language">
                                            <a class="language-selected" href="javascript:void(0);" onclick="change_language('english')"><img alt="" src="<?= THEME_ASSETS_URL ;?>images/ar.png"> ENGLISH </a>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Sub Header 2 -->
            <div class="header4">
                <div class="container">
                    <div class="row flex-nowrap justify-content-between position-relative">
                        <div class="header-column-left mobile-only">
                            <div class="sidebar-menu-icon-wrap desk-hide">
                                <div class="sidebar-menu-icon">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>

                            <div class="logo4">
                            <a href="<?= base_url() ?>"><img src="<?= base_url($logo) ?>" alt=""></a>
                            </div>
                        </div>




                        <div class="header-search-wrap justify-content-center mobile-hide">

                            <form class="ps-form--quick-search" action="<?=base_url('products/search') ?>" method="get" autocomplete="off">
                            <input class="form-control" type="text" name="q" placeholder="<?= !empty($this->lang->line('search_placeholder')) ? $this->lang->line('search_placeholder') : 'I\'m looking for...' ?>" id="input-search" >
                                <div class="form-group--icon">
                                    <select class="form-control" id="search_cat" dir="rtl" lang="AR">
                                    <option value="0" selected="selected"><?= !empty($this->lang->line('all')) ? $this->lang->line('all') : 'All' ?></option>
                                    <?php
										if($categories){
                                        foreach ($categories as $row) {?>
                                        
                                        <option class="level-0" value="<?= $row['id'] ?>"><?= ($lang_prefix=='') ? ucfirst(strtolower($row[$lang_prefix.'name'])) : $row[$lang_prefix.'name'] ?></option>
                                        <?php }} ?>

                                    </select>
                                </div>
                                <button><i class="fal fa-search"></i></button>
                                <div class="ps-panel--search-result">
                                    <div class="ps-panel__content">
                                       
                                    </div>
                                    <div class="ps-panel__footer text-center">
                                       </div>
                                </div>
                            </form>
                        </div>
                        <div class="header-column-right d-flex">
                            <div class="wrap-register-cart">

                                <div class="col-auto">

                                    <div class="header-tools justify-content-end">
                                        <div class="register-box mobile-hide tab-hide">
                                           <?php
                                           if(isset($this->data['user']->id) && $this->data['user']->id >0){?>
                                            <ul>
                                            <li><a href="<?= base_url('my-account') ?>"><?= !empty($this->lang->line('my_account')) ? $this->lang->line('my_account') : 'My Account' ?></a></li>
                                            <li><a href="<?= base_url('my-account/orders') ?>"><?= !empty($this->lang->line('orders')) ? $this->lang->line('orders') : 'Orders' ?></a></li>
                                        </ul>
                                         <?php  }
                                         else { ?>
                                         <ul>
                                                <li><a href="<?= base_url('login') ?>"><?= !empty($this->lang->line('login')) ? $this->lang->line('login') : 'Login' ?></a></li>
                                                <li><a href="<?= base_url('register') ?>"><?= !empty($this->lang->line('register')) ? $this->lang->line('register') : 'Register' ?></a></li>
                                            </ul>
                                            <?php } ?>
                                           
                                        </div>
                                        <div class="header-login lang-ar mobile-hide tab-visibile">
                                        <?php
                                   if($cookie_lang =='english'){ ?>
                                     <a href="javascript:void(0);" onclick="change_language('arabic')">العربية</a>
                                  <?php }
                                   else{?>
                                        <a href="javascript:void(0);" onclick="change_language('english')">ENGLISH</a>
                                  <?php } ?>
                                        </div>


                                        <div class="header-login desk-hide">
                                            <a href="<?= base_url('login') ?>" data-toggle="tooltip" data-placement="top" title=""
                                                data-original-title="<?= !empty($this->lang->line('my_account')) ? $this->lang->line('my_account') : 'My Account' ?>"><i class="flaticon-user"></i></a>
                                        </div>

                                        <div class="header-wishlist">
                                            <a onclick="openWish()"><span id="wishlist-count" class="wishlist-count"><?php
                                             if(isset($this->data['user']->id) && $this->data['user']->id!='') 
                                               $fav=get_favorites($this->data['user']->id);
                                            else
                                                $fav=[];
                                            echo count($fav); ?></span><i
                                                    class="flaticon-heart"></i></a>
                                        </div>
                                        <div class="header-cart">
                                            <a onclick="openCart()"><span id="cart-count" class="cart-count"><?php echo $count; ?></span><i
                                                    class="fal fa-shopping-cart"></i></a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row  desk-hide">


                        <div class="ps-search--mobile">
                            <form class="ps-form--search-mobile" action="<?=base_url('products/search') ?>" method="get" autocomplete="off">
                                <div class="form-group--nest">
                                    <input class="form-control" id="input-mobile-search" type="text" name="q" placeholder="<?= !empty($this->lang->line('search_place_holder')) ? $this->lang->line('search_place_holder') : 'Search...' ?>">
                                    <button><i class="icon-magnifier"></i></button>
                                </div>
                                <div class="ps-panel--search-result" id="mobile-result">
                                    <div class="ps-panel__content" id="mobile-search-content">
                                       
                                    </div>
                                    <div class="ps-panel__footer text-center" id="mobile-search-footer">
                                       </div>
                                </div>
                            </form>

                            <div class="header-login lang-ar tab-hide">
                            <?php
                                   if($cookie_lang =='english'){ ?>
                                     <a href="javascript:void(0);" onclick="change_language('arabic')">العربية</a>
                                  <?php }
                                   else{?>
                                        <a href="javascript:void(0);" onclick="change_language('english')">ENGLISH</a>
                                  <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="deliver-tab-mobile desk-hide">
                <a href="#">
                    <div class="del-icon"> <i class="fas fa-map-marker-alt"></i></div>
                    <div class="mobile-delv-detail">
                        <span>
                            Deliver to Within - Kuwait City
                        </span>

                    </div>
                </a>

            </div> -->

            <!-- End Header2 -->
 
            <div class="header-nav2 header-nav12">
                <div class="container">

                    <div class="menumain_bordr">

                        <div class="category-nav <?php if($main_page == 'home') echo 'show'; ?>">
                            <div class="category-dropdown category-nav-inner" data-items="13">

                            <?= !empty($this->lang->line('all_categories')) ? $this->lang->line('all_categories') : 'All Categories' ?>
                                <i class="fa fa-bars"></i>
                            </div>
                            <div class="category-dropdown-wrap <?php if($main_page == 'home') echo 'show'; ?>">
                                <div class="category-dropdown">
                                    <ul class="list-inline vertical-megamenu list-category-dropdown">

                                    <?php
										
                                        foreach ($categories as $row) { 
                                        $child=$row['children'];
                                        if(count($child)>0)
                                        {
                                        ?>
                                        
                                        <li class="has-cat-mega">
                                            <a href="<?= base_url('products/category/' . $row['slug']) ?>" >
                                            <?php if($row['cat_icon'] !=''){?>
                                               <i class="<?=  $row['cat_icon'] ?>"></i> 

                                          <?php  }?>
                                           
                                            <?= ($lang_prefix=='') ? ucfirst(strtolower($row['name'])) : $row[$lang_prefix.'name'] ?></a>
                                            <div class="cat-mega-menu cat-mega-style1">
                                                <div class="row">
                                                <?php $sub1 = 0; $m =0;$h=1;
                                                    foreach ($child as $row1) { 
                                                       if($h<=6){
                                                        $child1=$row1['children'];
                                                      if(count($child1)>0)
                                                      {
                                                        if($sub1%2==0){?>
                                                    <div class="col-md-4 col-sm-3">
                                                        <div class="list-cat-mega-menu">
                                                            <?php } ?>
                                                           
                                                            <h2 class="title-cat-mega-menu">
                                                                <a href="<?= base_url('products/category/' . $row1['slug']) ?>"><?= ($lang_prefix=='') ?strtoupper($row1['name']) : $row1[$lang_prefix.'name'] ?></a>
                                                            </h2>
                                                           
                                                            <ul>
                                                                <?php
                                                                $g=1;
                                                                  foreach ($child1 as $row2) {
                                                                      if($g<=5){?>
                                                                <li><a href="<?= base_url('products/category/' . $row2['slug']) ?>"><?= ($lang_prefix=='') ?ucfirst(strtolower($row2['name'])) : $row2[$lang_prefix.'name']  ?></a></li>
                                                               <?php } $g++;
                                                               }
                                                               if(count($child1)<5){
                                                                   for($g=$g-1;$g<5;$g++){
                                                                       echo '<li class="empty-list"></li>';
                                                                   }
                                                               }?>
                                                            </ul>
                                                            <?php 
                                                            $sub1++;
                                                            if($sub1%2==0){?>
                                                        </div>
                                                    </div>
                                                     <?php 
                                                    }?>
                                                            <?php } 
                                                            else{
                                                                ?>
                                                            <div class="not-mega-menu list-menu col-md-12">
                                                                <a href="<?= base_url('products/category/' . $row1['slug']) ?>"><?= ($lang_prefix=='') ?strtoupper($row1['name']) : $row1[$lang_prefix.'name'] ?></a>
                                                            </div>
                                                            <?php } } $h++;
                                                           
                                                           
                                                    } ?>
                                                </div>
                                            </div>
                                        </li>
                                                  
                                               
                                        <?php } 
                                         else
                                         {
                                             ?>
                                         <li ><a href="<?= base_url('products/category/' . $row['slug']) ?>"  class="menu-item"> 
                                         <?php if($row['cat_icon'] !=''){?>
                                               <i class="<?=  $row['cat_icon'] ?>"></i> 

                                          <?php  }?>
                                          <?= ($lang_prefix=='') ? ucfirst(strtolower($row['name'])) : $row[$lang_prefix.'name'] ?></a></li>
                                             <?php
                                         }
                                         }
                                          ?>

                                        <li class="more-categories">
                                            <a href="#" class="menu-item"><strong><i class="icon-plus"></i>  <?= !empty($this->lang->line('all_categories')) ? $this->lang->line('all_categories') : 'All Categories' ?></strong></a>
                                        </li>


                                    </ul>
                                    <a href="#" class="expand-category-link"></a>
                                </div>
                            </div>

                        </div>
                        <!-- End Category Dropdown -->





                        <nav class="main-nav navbar navbar-expand-sm">
                            <ul class="navbar-nav horizontal-megamenu">

                            <?php
                                         if($top_sub_categories){?>
                                <li class="has-mega-menu">
                                    <a href="#"> <?= !empty($this->lang->line('sub_category')) ? $this->lang->line('sub_category') : 'Sub Category' ?></a>
                                    <div class="mega-menu mega-menu-style1">
                                        <div class="row">
                                       <?php
                                            $sub_level = 0;$sub_count = 1;
                                             foreach($top_sub_categories as $sc_row){
                                                $sc_child=$sc_row['children'];
                                                if(count($sc_child)>0)
                                                {
                                                    if($sub_count <=6){
                                                    if($sub_level%2==0){
                                                        echo ' <div class="col-md-4 col-sm-3">
                                                        <div class="list-cat-mega-menu">';
                                                     } 
                                                     $fc_name = ($lang_prefix=='') ? strtoupper($sc_row['name']) : $sc_row[$lang_prefix.'name'];

                                                    echo '<h2 class="title-cat-mega-menu"> '.$fc_name.'
                                                     </h2>';
                                                  echo ' <ul>';
                                                  $sc_2 = 1;
                                                  foreach ($sc_child as $sc_row1) {
                                                    if($sc_2<=5){
                                                        $sc_name = ($lang_prefix=='') ? ucfirst(strtolower($sc_row1['name'])) : $sc_row1[$lang_prefix.'name'];
                                                    echo ' <li><a href="'.base_url('products/category/' . $sc_row1['slug']) .'">'.$sc_name.'</a></li>';
                                                    $sc_2++;
                                                    }
                                                   }
                                                   echo ' </ul>';
                                                   $sub_level++;
                                                   if($sub_level%2==0){
                                                                echo '</div>

                                                    </div>';
                                                   
                                                    }
                                                    $sub_count++;
                                                }
                                                }

                                             }?>
                                        
                                            
                                        </div>
                                    </div>
                                </li>
                                             <?php  }?>


                                <li>
                                    <a href="<?= base_url('special-offers') ?>"><?= !empty($this->lang->line('offers')) ? $this->lang->line('offers') : 'Offers' ?></a>

                                </li>
                                <li>
                                    <a href="<?= base_url('products?sort=date-desc') ?>"><?= !empty($this->lang->line('new_arrivals')) ? $this->lang->line('new_arrivals') : 'New Arrivals' ?></a>

                                </li>
                                <li  class="has-mega-menu"><a href="<?= base_url('brands') ?>"><?= !empty($this->lang->line('brands')) ? $this->lang->line('brands') : 'Brands' ?></a>
                                <div class="mega-menu mega-menu-style1" style="width:150% !important; padding:10px !important;">
                                        <div class="row">
                                       <?php
                                            $sub_level = 0;$sub_count = 1;
                                            if($drop_brands){
                                             foreach($drop_brands as $sc_row){
                                               
                                                echo ' <div class="col-md-2 col-sm-2">
                                                        <div class="list-cat-mega-menu">';
                                                    
                                                     $fc_name = ($lang_prefix=='') ? strtoupper($sc_row['name']) : $sc_row[$lang_prefix.'name'];

                                                    echo ' <a href="'.base_url('products/brands/' . $sc_row['slug']).'"> <img src="'.base_url($sc_row['image']).'" alt="">
                                                     </a>';
                                                echo '</div>

                                                    </div>';
                                                   
                                                  
                                                }

                                             }?>
                                        
                                            
                                        </div>
                                    </div>
                              
                            </li>
                                <!-- <li><a href="#">Product Gallery</a></li> -->



                            </ul>
                            <a href="#" class="toggle-mobile-menu">
                                <!--<span>Menu</span>-->
                            </a>

                        </nav>

                        <!-- <span class="navigation-text">

                            <a href="#">
                                <div class="del-icon"><i class="fas fa-map-marker-alt"></i></div>

                                <div class="deliver-detail">
                                    <span class="nav-line-1">
                                        Deliver to Within
                                    </span>
                                    <span class="nav-line-2">
                                        Kuwait City
                                    </span>
                                </div>
                            </a>

                        </span> -->

                        <!-- End Main Nav -->
                    </div>




                </div>


            </div>
        </div>