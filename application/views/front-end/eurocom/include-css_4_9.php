<!-- Izimodal -->
<?php $favicon = get_settings('web_favicon'); ?>
<link rel="icon" href="<?=base_url($favicon)?>" type="image/x-icon" sizes="16x16">
<!-- intlTelInput -->


<link rel="shortcut icon" type="image/x-icon" href="<?= THEME_ASSETS_URL ;?>images/faviocn.png">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>fonts/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>css/font-linearicons.css" />
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>css/flaticon.css" />
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/vendor/vendor.min.css">
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>fonts/Linearicons/Linearicons/Font/demo-files/demo.css">
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>css/jquery.fancybox.css" />
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>css/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>css/owl.transitions.css" />
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>css/owl.theme.css" />
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>css/jquery.mCustomScrollbar.css" />
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>js/slideshow/settings.css" />
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>css/theme.css" media="all" />
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>css/mixed.css" media="all" />

    <link href="<?= THEME_ASSETS_URL ;?>css/magiczoomplus.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/sweetalert2.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?= THEME_ASSETS_URL ;?>css/responsive.css" media="all" />
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@200;300;400;500;700;800&display=swap" rel="stylesheet">