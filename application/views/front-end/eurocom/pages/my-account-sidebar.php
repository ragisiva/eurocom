<?php $current_url = current_url(); ?>

                            <ul class="list-inline account-sidebar">
                            <li class="<?=($current_url==base_url('my-account')) ? 'active' : ''?>"><a  id="dashboard" href="<?= base_url('my-account') ?>"><i class="fas fa-tachometer-alt"></i>
                            <?= !empty($this->lang->line('dashboard')) ? $this->lang->line('dashboard') : 'Dashboard' ?>
                        </a></li>
                            <li class="<?=($current_url==base_url('my-account/orders')) ? 'active' : ''?>"><a id="order-history" href="<?= base_url('my-account/orders') ?>"><i class="fas fa-cart-arrow-down"></i>
                            <?= !empty($this->lang->line('my_order')) ? $this->lang->line('my_order') : 'My Order' ?>
                        </a></li>

                            <li class="<?=($current_url==base_url('my-account/manage-address') || $current_url==base_url('my-account/add-new-address')) ? 'active' : ''?>"><a href="<?= base_url('my-account/manage-address') ?>"><i class="fas fa-address-book"></i>
                            <?= !empty($this->lang->line('my_address')) ? $this->lang->line('my_address') : 'My Address' ?>
                        </a></li>
                            <li class="<?=($current_url==base_url('my-account/profile')) ? 'active' : ''?>"><a href="<?= base_url('my-account/profile') ?>"><i class="fas fa-user-circle"></i>
                            <?= !empty($this->lang->line('my_profile')) ? $this->lang->line('my_profile') : 'My Profile' ?>
                        </a></li>

                            <li class="<?=($current_url==base_url('my-account/favorites')) ? 'active' : ''?>"><a href="<?= base_url('my-account/favorites') ?>" ><i class="far fa-heart"></i>
                            <?= !empty($this->lang->line('my_wishlist')) ? $this->lang->line('my_wishlist') : 'My Wishlist' ?>
                        </a></li>


                            <li class="<?=($current_url==base_url('login/logout')) ? 'active' : ''?>"><a  href="<?= base_url('login/logout') ?>"><i class="fas fa-sign-out-alt"></i>
                            <?= !empty($this->lang->line('logout')) ? $this->lang->line('logout') : 'Logout' ?>
                        </a></li>
                        </ul>