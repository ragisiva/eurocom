<!-- breadcrumb -->
<div class="container">
		
		<div class="page_name">
			<div class="row">
		 <div class="col-md-6 ">	
		<h3><?= !empty($this->lang->line('my_address')) ? $this->lang->line('my_address') : 'My Address' ?></h3>
			</div>
			 <div class="col-md-6 ">	
        <div class="breadcrumb">			
            <ul class="list-inline">
                <li><a href="<?= base_url() ?>"><?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?></a></li>               
                <li class="active"><?= !empty($this->lang->line('my_address')) ? $this->lang->line('my_address') : 'My Address' ?></li>
            </ul>
        </div>
		</div>		 
		</div>
			</div>
		
		
		
    </div>	

<!-- end breadcrumb -->


<div id="content">
    <section class="account-wrap">
        <div class="container">
            <div class="account-wrap-inner">

                <div class="account-left">
                    <?php $this->load->view('front-end/' . THEME . '/pages/my-account-sidebar') ?>

                </div>


                <div class="account-right">
                    <div class="panel-wrap">
                        <div class="panel">
                            <div class="panel-header">
                                <h4><?= !empty($this->lang->line('my_address')) ? $this->lang->line('my_address') : 'My Address' ?></h4>

                            </div>
                            <div class="panel-body">
                            <div class="my-addresses">
                                        <div class="address-card-wrap">
                                            <div class="row">
                                               
                                                <?php 
                                                if($addresses){
                                                    foreach($addresses as $row){?>
                                                <div class="col-xl-4 col-lg-4 d-flex" id="addr_<?= $row['id'] ?>">
                                                    <address class="address-card <?php if($row['is_default'] == 1) echo 'active'; ?>"><div class="address-card-data">
                                                        <span><?= $row['name'] ?></span> 
                                                        <span><?= $row['type'] ?></span> <!----> 
                                                        <span><?= $row['address'] ?></span> 
                                                        <span><?= $row['area'] ?>, <?= $row['pincode'] ?></span>
                                                        <span><?= $row['mobile'] ?></span> 
                                                    </div> 
                                                        <div class="address-card-actions">
                                                            <a href="<?= base_url('my-account/add-new-address/'.$row['id']) ?>" class="btn btn-edit-address"><?= !empty($this->lang->line('edit')) ? $this->lang->line('edit') : 'Edit' ?> </a> 
                                                            <button type="button" class="btn btn-delete-address delete-address" data-id="<?= $row['id'] ?>"> <?= !empty($this->lang->line('delete')) ? $this->lang->line('delete') : 'Delete' ?></button></div></address>
                                                </div>

                                                    <?php } } ?>

                                               

                                                <div class="col-md-12">
                                                    <a href="<?= base_url('my-account/add-new-address') ?>" class="btn btn-lg btn-default btn-add-new-address"><?=  strtoupper(!empty($this->lang->line('add_new_address')) ? $this->lang->line('add_new_address') : 'Add New Address') ?></a>
                                                </div>


                                            </div>
                                        </div>
                                    </div>  
                                    </div>
                            </div>
                        </div>
                    </div>


                </div>



            </div>

        </section>


    </div>

                       