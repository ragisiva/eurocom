<div class="container">

        <div class="page_name">
            <div class="row">
                <div class="col-md-6 ">
                    <h3><?=!empty($this->lang->line('delivery_information')) ? $this->lang->line('delivery_information') : 'DELIVERY INFORMATION'?></h3>
                </div>
                <div class="col-md-6 ">
                    <div class="breadcrumb">
                        <ul class="list-inline">
                            <li><a href="<?= base_url() ?>"><?=!empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home'?></a></li>

                            <li class="active"><?=!empty($this->lang->line('delivery_information')) ? $this->lang->line('delivery_information') : 'Delivery Information'?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>



    </div>
	<div id="content">

        <section class="custom-page-wrap clearfix">
            <div class="container">
                <div class="custom-page-content clearfix">
                            <?= $delivery_informations ?>
                </div>
            </div>
        </section>
    </div>
   
    <!--breadcrumbs area end-->