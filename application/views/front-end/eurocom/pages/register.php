
		
	<div class="container">
		
		<div class="page_name">
			<div class="row">
		 <div class="col-md-6 ">	
		<h3><?= !empty($this->lang->line('register')) ? $this->lang->line('register') : 'register' ?></h3>
			</div>
			 <div class="col-md-6 ">	
        <div class="breadcrumb">			
            <ul class="list-inline">
                <li><a href="<?= base_url() ?>"><?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?></a></li>

                <li class="active"><?= !empty($this->lang->line('register')) ? $this->lang->line('register') : 'register' ?></li>
            </ul>
        </div> 
		</div>		 
		</div>
			</div>
		
		
		
    </div>	
    <div id="content">
        <section class="form-wrap register-wrap">
            <div class="container">
                <div class="form-wrap-inner register-wrap-inner">
                    <form id="form2" name="form2" method="post" action='#' id='sign-up-form' class='sign-up-form' >
                    <form method="POST" action="#" id='sign-up-form' class='sign-up-form'>
                        <div class="form-group">
                            <label for="first-name"><?= !empty($this->lang->line('first_name')) ? $this->lang->line('first_name') : 'First Name' ?><span>*</span></label>
                             <input type="text" name="first_name" value="" id="first-name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="last-name"> <?= !empty($this->lang->line('last_name')) ? $this->lang->line('last_name') : 'Last Name' ?><span>*</span></label>
                            <input type="text" name="last_name" value="" id="last-name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="email"><?= !empty($this->lang->line('email')) ? $this->lang->line('email') : 'Email' ?><span>*</span></label> 
                            <input type="text" name="email" value="" id="email" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="phone"><?= !empty($this->lang->line('phone')) ? $this->lang->line('phone') : 'Phone' ?><span>*</span></label>
                            <input type="text" name="phone" value="" id="phone" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="password"><?= !empty($this->lang->line('password')) ? $this->lang->line('password') : 'Password' ?><span>*</span></label> 
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                        <div class="form-group"><label for="confirm-password"><?= !empty($this->lang->line('confirm_password')) ? $this->lang->line('confirm_password') : 'Confirm Password' ?><span>*</span></label>
                         <input type="password" name="password_confirmation" id="confirm-password" class="form-control">
                        </div>
                        <div class="form-group p-t-5"><img src="https://fleetcart.envaysoft.com/captcha/image?1054488125" title="Update Code" onclick="this.setAttribute('src','https://fleetcart.envaysoft.com/captcha/image?1054488125?_='+Math.random());var captcha=document.getElementById('captcha');if(captcha){captcha.focus()}"
                                style="cursor: pointer; width: 180px; height: 50px;"> <input type="text" name="captcha" id="captcha" placeholder="Enter captcha code" class="captcha-input"></div>
                        <div class="form-check terms-and-conditions"><input type="hidden" name="privacy_policy" value="0"> <input type="checkbox" name="privacy_policy" value="1" id="terms"> <label for="terms" class="form-check-label">
                        <?= !empty($this->lang->line('i_agree_to_the')) ? $this->lang->line('i_agree_to_the') : ' I agree to the ' ?><a href="<?= base_url('privacy-policy') ?>"><?= !empty($this->lang->line('privacy_policy')) ? $this->lang->line('privacy_policy') : 'Privacy Policy' ?></a></label></div> 
                   
            <div class="login_submit">
                <button type="submit" data-loading="" class="btn btn-primary btn-create-account"><?= !empty($this->lang->line('create_account')) ? $this->lang->line('create_account') : 'CREATE ACCOUNT' ?></button>
            </div>
			<div id='sign-up-error' class='alert alert-success text-center p-3' ></div>
        </form>
         <!-- <span class="sign-in-with">
            Or, Sign Up With
    </span>
                    <ul class="list-inline social-login">
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Google" class="google"><i class="fab fa-google"></i></a></li>
                    </ul>  -->
                    <span class="have-an-account">
                    <?= !empty($this->lang->line('already_have_account')) ? $this->lang->line('already_have_account') : 'Already Have an Account?' ?> 
        </span> <a href="<?= base_url('login') ?>" class="btn btn-default btn-sign-in">
        <?= !empty($this->lang->line('sign_in')) ? $this->lang->line('sign_in') : 'SIGN IN' ?> 
        </a></div>

            </div>

        </section>


    </div>


