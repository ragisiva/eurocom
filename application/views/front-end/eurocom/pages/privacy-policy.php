<div class="container">

<div class="page_name">
    <div class="row">
        <div class="col-md-6 ">
            <h3><?= !empty($this->lang->line('privacy_policy')) ? $this->lang->line('privacy_policy') : 'Privacy Policy' ?></h3>
        </div>
        <div class="col-md-6 ">
            <div class="breadcrumb">
                <ul class="list-inline">
                    <li><a href="<?= base_url() ?>"><?=!empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home'?></a></li>

                    <li class="active"><?= !empty($this->lang->line('privacy_policy')) ? $this->lang->line('privacy_policy') : 'Privacy Policy' ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>



</div>


	
    <div id="content">

<section class="custom-page-wrap clearfix">
    <div class="container">
        <div class="custom-page-content clearfix">
              <?= $privacy_policy ?>
              </div>
            </div>
        </section>
    </div>
   
    <!--breadcrumbs area end-->