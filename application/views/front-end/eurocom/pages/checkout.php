<div class="container">
<?php
$countries = $this->setting_model->country_list();?>
    <div class="page_name">
        <div class="row">
            <div class="col-md-6 ">
                <h3>
                    <?= !empty($this->lang->line('checkout')) ? $this->lang->line('checkout') : 'Checkout' ?>
                </h3>
            </div>
            <div class="col-md-6 ">
                <div class="breadcrumb">
                    <ul class="list-inline">
                        <li><a href="<?= base_url() ?>">
                                <?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?>
                            </a></li>
                        <li><a href="<?= base_url('cart') ?>">
                                <?= !empty($this->lang->line('cart')) ? $this->lang->line('cart') : 'Cart' ?>
                            </a></li>
                        <li class="active">
                            <?= !empty($this->lang->line('checkout')) ? $this->lang->line('checkout') : 'Checkout' ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



</div>
<!--breadcrumbs area end-->

<div id="content">
    <section class="checkout-wrap">

        <div class="container">


            <!-- End List Service -->
            <div class="list-tab-product">
                <div class="col-12 ">
                    <div class="row">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-xs-12 checkout-left1">
                            <div class="title-tab-product">
                                <ul>
                                    <?php if (!$this->data['is_logged_in']) {?>
                                    <li class="checkout-links active"><a href="#" data-id="register"> <i class="fa fa-sign-out"
                                                aria-hidden="true"></i> <span>
                                                <?= !empty($this->lang->line('login_register')) ? $this->lang->line('login_register') : 'LOGIN/REGISTER' ?>
                                            </span></a></li>
                                    <?php } ?>
                                    <?php if (!$this->data['is_logged_in']) {?>
                                    <li id="confirm-address" class="checkout-links"><a href="#" data-id="address" disabled> <i class="fa fa-address-book"
                                                aria-hidden="true"></i> <span>
                                                <?= !empty($this->lang->line('confirm_address')) ? $this->lang->line('confirm_address') : 'CONFIRM ADDRESS' ?>
                                            </span></a></li>
                                            <?php } else{ ?>
                                                <li id="billing-address" class="checkout-links active"><a href="#" data-id="billing-address-tab"  disabled>  <i class="fa fa-address-book"
                                                aria-hidden="true"></i> <span>
                                                <?= !empty($this->lang->line('billing_address')) ? $this->lang->line('billing_address') : 'BILLING ADDRESS' ?>
                                            </span></a></li>
                                            <li id="delivery-address" class="checkout-links"><a href="#" data-id="delivery-address-tab"  disabled> <i class="fa fa-address-book"
                                                aria-hidden="true"></i> <span>
                                                <?= !empty($this->lang->line('delivery_address')) ? $this->lang->line('delivery_address') : 'DELIVERY ADDRESS' ?>
                                            </span></a></li>
                                            <?php } ?>
                                    <li id="review-link" class="checkout-links" ><a href="#" data-id="review-tab"  disabled> <i class="fa fa-eye" aria-hidden="true"></i>
                                            <span>
                                                <?= !empty($this->lang->line('review_order')) ? $this->lang->line('review_order') : 'REVIEW ORDER' ?>
                                            </span></a></li>
                                    <li id="payment-link" class="checkout-links"><a href="#" data-id="payment-tab"  disabled><i class="fa fa-credit-card"
                                                aria-hidden="true"></i> <span>
                                                <?= !empty($this->lang->line('payment-method')) ? $this->lang->line('payment-method') : 'PAYMENT METHOD' ?>
                                            </span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-xs-12 checkout-right1">
                            <div class="content-tab-product">
                            <div id="checkout-message-box"></div>
                                <!-------------------------------------------------------------register----------------------------------------------------------------------->
                                <?php if (!$this->data['is_logged_in']) {?>
                                <div id="register" class="tab-pane active">
                             
                                    <div class="row">
                                        <div class=" col-xl-4 col-md-5 col-sm-12 col-xs-12 loginbox_fit">
                                          
                                           
                                                <div class="login-reg bg_width_fit">
                                                    <form id="form1" name="form1" method="post"
                                                    action="<?= base_url('home/login') ?>" class="form-submit-event1">
                                                    <div id="infoMessage"> <?php echo isset($message) ?  $message : '';?></div>
                                                    <div class="justify-content-center">
                                                        <div class="form-group" id="error_box"></div>
                                                    </div>
                                                    <h3>
                                                        <?= !empty($this->lang->line('sign_in')) ? $this->lang->line('sign_in') : 'Sign In' ?>
                                                    </h3>
                                                    <ul>

                                                        <li>
                                                            <span class="email noSwipe">
                                                                <input type="text" type="text" id="identity"
                                                                    name="identity" class="form-control"
                                                                    placeholder="<?= !empty($this->lang->line('email_id')) ? $this->lang->line('email_id') : 'email_id' ?>">
                                                            </span>

                                                        </li>
                                                        <li>
                                                            <span class="password noSwipe">
                                                                <input type="password" name="password" id="password"
                                                                    class="form-control"
                                                                    placeholder="<?= !empty($this->lang->line('password')) ? $this->lang->line('password') : 'Password' ?>">

                                                                <span class="field-validation-valid"
                                                                    data-valmsg-for="Password"
                                                                    data-valmsg-replace="true"></span>
                                                            </span>
                                                            <span class="eyeicon"></span>

                                                        </li>
                                                        <li>
                                                            <input id="type" type="hidden" name="type" value="checkout">
                                                            <button type="submit" data-loading=""
                                                                class="btn btn-primary btn-proceed-to-checkout btn_wdth_fit">
                                                                <?= !empty($this->lang->line('sign_in')) ? $this->lang->line('sign_in') : 'Sign In' ?>
                                                            </button>
                                                        </li>

                                                        <li>
                                                            <div class="forgotpass">
                                                                <a  class="bluetext" style="font-size:13px;" data-toggle="modal" data-target="#forgotPassword"> <?= !empty($this->lang->line('forgot_password')) ? $this->lang->line('forgot_password') : 'Forgot Password' ?>?</a>
                                                            </div>
                                                        </li>

                                                    </ul>
                                                </form>
                                                <span class="buttoncontrols">
                                                <button type="button" class="btn btn-dark btn-proceed-to-checkout btn_wdth_fit" id="account_guest" ><?= !empty($this->lang->line('checkout_as_guest')) ? $this->lang->line('checkout_as_guest') : 'CHECKOUT AS GUEST' ?></button>
                                            </span>
                                                
                                               
                                                </div>
                                            
                                        </div>
                                        <div class="col-xl-5 col-md-7 col-sm-12 col-xs-12 loginbox_fit">
                                            <div class="septext "><?= !empty($this->lang->line('or')) ? $this->lang->line('or') : 'OR' ?></div>



                                            <div class="login-reg loginbox_fit">
                                                <h3><?= !empty($this->lang->line('register')) ? $this->lang->line('register') : 'Register' ?></h3>
                                                <form id="register_form" class="horizontal-form">
                                                <ul>

                                                    <li>
                                                        <span class="email noSwipe">

                                                            <input type="text" name="email" id="email"
                                                                class="form-control" placeholder="<?= !empty($this->lang->line('email')) ? $this->lang->line('email') : 'Email' ?>" required>                                                            
                                                        </span>

                                                    </li>
                                                    <li>
                                                        <span class="name noSwipe">

                                                            <input type="text" name="first_name" id="first_name"
                                                                class="form-control" placeholder="<?= !empty($this->lang->line('first_name')) ? $this->lang->line('first_name') : 'First Name' ?>" required>
                                                           
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <span class="name noSwipe">
                                                            <input type="text" name="last_name" id="last_name"
                                                                class="form-control" placeholder="<?= !empty($this->lang->line('last_name')) ? $this->lang->line('last_name') : 'Last Name' ?>"  required>         
                                                        </span>
                                                    </li>
                                                    <li class="hide_guest">
                                                        <span class="password noSwipe">

                                                            <input type="password" id="password" name="password"
                                                                class="form-control" placeholder="<?=!empty($this->lang->line('password')) ? $this->lang->line('password') : 'Password'?>"  required>
                                                          
                                                        </span>
                                                        <span class="eyeicon"></span>

                                                    </li>
                                                    <li class="hide_guest">
                                                        <span class="password noSwipe">

                                                            <input type="password" id="confirm_password" name="confirm_password"
                                                                class="form-control" placeholder="<?=!empty($this->lang->line('confirm_password')) ? $this->lang->line('confirm_password') : 'Confirm Password '?>"  required>
                                                         
                                                        </span>
                                                        <span class="eyeicon"></span>
                                                    </li>
                                                    <li>
                                                        <span class="country noSwipe">

                                                        
                                                            <select class="form-control" id="country_id" name="country_id"  required>
                                                            <option value="117" ><?= $this->lang->line('kuwait') ?></option>
                                                                <?php
                                                                /*
                                                                if($countries){
                                                                    foreach($countries as $each){?>
                                                                        <option value="<?= $each['id'] ?>" <?php if(strtolower($each['country']) == 'kuwait') echo 'selected'; ?>><?= $each['country'] ?></option>
                                                                 <?php  }
                                                                }*/
                                                                ?>
                                                               
                                                            </select>
                                                            <span class="field-validation-valid"
                                                                data-valmsg-for="SelectedCountry"
                                                                data-valmsg-replace="true"></span>
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <span class="phone noSwipe">

                                                            <input type="text" name="mobile" id="mobile"
                                                                class="form-control" placeholder="<?= !empty($this->lang->line('mobile_number')) ? $this->lang->line('mobile_number') : 'Mobile Number' ?>"  required>
                                                          
                                                        </span>
                                                    </li>
                                                    <li class="how-you-know noSwipe hide_guest">
                                                        <span class="caption fontbold"><?=  $this->lang->line('how_do_know_about_us') ?></span>
                                                        <select class="form-control" name="how_to_know">
                                                            <option value="" >--<?=  $this->lang->line('select') ?>-- </option>
                                                            <option value="Facebook" label="Facebook"><?=  $this->lang->line('facebook') ?></option>
                                                            <option value="Twitter" label="Twitter"><?=  $this->lang->line('twitter') ?></option>
                                                            <option value="Search Engine" label="Search Engine"><?=  $this->lang->line('search_engine') ?>
                                                            </option>
                                                            <option value="Email" label="Email"><?=  $this->lang->line('email') ?></option>
                                                            <option value="Instagram" label="Instagram"><?=  $this->lang->line('instagram') ?>
                                                            </option>
                                                            <option value="Snapchat" label="SnapChat"><?=  $this->lang->line('snap_chat') ?></option>
                                                            <option value="Blog" label="Blog"><?=  $this->lang->line('blog') ?></option>
                                                            <option value="Youtube" label="Youtube"><?=  $this->lang->line('youtube') ?></option>
                                                            <option value="Friend" label="Friend"><?=  $this->lang->line('friend') ?></option>
                                                            <option value="Eurocom Showroom" label="Eurocom Showroom"><?=  $this->lang->line('eurocom_shop') ?></option>
                                                            <option value="Other" label="Other"><?=  $this->lang->line('other') ?>
                                                               </option>
                                                        </select>
                                                        <!-- ngIf: isEditable==true -->


                                                    </li>
                                                    <li class="email-updates hide_guest">

                                                        <div class="form-check">
                                                            <input type="checkbox" class="form-check-input" value="1"
                                                                id="newsletter_subscribe" name="newsletter_subscribe">
                                                            <label class="form-check-label" for="newsletter_subscribe"><?=  $this->lang->line('email_subscribe_description') ?></label>
                                                        </div>

                                                    </li>
                                                    <li class="hide_guest">

                                                        <span class="caption terms">

                                                            <div class="form-check">
                                                                <input type="checkbox" class="form-check-input"
                                                                    value="1" id="terms_conditions_field" name="terms_conditions_field">
                                                                <label class="form-check-label" for="terms_conditions_field"><?=  $this->lang->line('terms_conditions') ?></label>
                                                            </div>



                                                        </span>
                                                    </li>
                                                    
                                                    <li>
                                                        <button type="submit"
                                                            class="btn btn-primary btn-proceed-to-checkout btn_wdth_fit" id="register_account" >
                                                            <?= $this->lang->line('submit')  ?> </button>
                                                    </li>


                                                </ul> 
                                               
                                                            </form>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <!-------------------------------------------------------------address----------------------------------------------------------------------->
                              
                                <?php if (!$this->data['is_logged_in']) {?>
                                <div id="address"
                                    class="tab-pane" >
                                    <div class="tabtitle alignleft fullwidth">
                                            <h3><?= $this->lang->line('confirm_address') ?></h3>
                                            <span><?= $this->lang->line('delivery_address_sub') ?></span>
                                        </div>
                                   
                                    <!----------------------------------------------New Addres----------------------->
                                <div class="row">
                                    <div class="col-sm-6 billing-address-guest">
                                        <div class="address-column newaddress alignleft fullwidth"
                                          >
                                           <form id="guest-billing" name="guest-billing" method="post" action="#" >
                                                <ul class="editfields">
                                                    <li>
                                                    
                                                        <label for="first_name"><?= $this->lang->line('first_name') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="first_name" 
                                                                class="form-control" placeholder="<?= $this->lang->line('first_name') 
                                                ?>">
                                                         
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="last_name"><?= $this->lang->line('last_name') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="last_name" 
                                                                class="form-control" placeholder="<?= $this->lang->line('last_name') 
                                                ?>">
                                                         
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="email"><?= $this->lang->line('email') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="email" 
                                                                class="form-control" placeholder="<?= $this->lang->line('email') 
                                                ?>">
                                                         
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="Countries"><?= $this->lang->line('country') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                        <select class="form-control"  name="country_id"  required>
                                                        <option value="117" ><?= $this->lang->line('kuwait') ?></option> 
                                                         <?php
                                                         /*
                                                         if($countries){
                                                             foreach($countries as $each){?>
                                                                 <option value="<?= $each['id'] ?>" <?php if(strtolower($each['country']) == 'kuwait') echo 'selected'; ?>><?= $each['country'] ?></option>
                                                          <?php  }
                                                         }*/
                                                         ?>
                                                        
                                                     </select>
                                                    </span>

                                                    </li>
                                                    <li>

                                                        <label for="Areas"><?= $this->lang->line('area') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                        <select class="niceselect_option form-control" name="area_id"
                                                >
                                                <option value="">---<?= !empty($this->lang->line('select_area')) ? $this->lang->line('select_area') : 'Select Area' ?>----</option>
                                                <?php foreach ($cities as $row) { ?>
                                        <optgroup label="<?= $row['name'] ?>">
                                       <?php
                                        $areas = get_areaslist_undercity($row['id']);

                                        foreach($areas as $a_row){?>
                                        <option value="<?= $a_row['id'] ?>" ><?= $a_row['name'] ?></option>
                                    <?php }?>
                                    </optgroup>
                               <?php } ?>
                                                

                                            </select>
                                                        </span>

                                                    </li>

                                                    <li>
                                                        <label for="Block"><?= $this->lang->line('block') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="block"
                                                                class="form-control" placeholder="<?= $this->lang->line('block') ?>">
                                                          
                                                        </span>
                                                    </li>

                                                    <li>
                                                        <label for="Avenue"> <?=!empty($this->lang->line('avenue')) ? $this->lang->line('avenue') : 'Avenue / Building'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="avenue" 
                                                                class="form-control" placeholder="<?= $this->lang->line('avenue') ?>">
                                                            
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="Street"> <?=!empty($this->lang->line('street_no')) ? $this->lang->line('street_no') : 'Street No'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="street" 
                                                                class="form-control" placeholder="<?= $this->lang->line('street_no') ?>">
                                                            
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="BuildingNo"> <?=!empty($this->lang->line('house_no')) ? $this->lang->line('house_no') : 'House No/ Flat No '; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="house" 
                                                                class="form-control" placeholder="<?= $this->lang->line('house_no') ?>">
                                                            
                                                        </span>
                                                    </li>
                                                   
                                                    <li>
                                                        <label for="MobileNo1"> <?=!empty($this->lang->line('mobile')) ? $this->lang->line('mobile') : 'Mobile'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="mobile" 
                                                                class="form-control" placeholder="<?=!empty($this->lang->line('mobile')) ? $this->lang->line('mobile') : 'Mobile'; ?>">
                                                          
                                                        </span>

                                                    </li>
                                                    <li>
                                                        <label for="MobileNo2"><?=!empty($this->lang->line('alternate_mobile')) ? $this->lang->line('alternate_mobile') : 'Alternate Mobile'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="alternate_mobile" id="alternate_mobile"
                                                                class="form-control" placeholder="<?=!empty($this->lang->line('alternate_mobile')) ? $this->lang->line('alternate_mobile') : 'Alternate Mobile'; ?>">
                                                            <span class="field-validation-valid"
                                                                data-valmsg-for="MobileNo2"
                                                                data-valmsg-replace="true"></span>
                                                        </span>
                                                    </li>
                                                    <li>
                                                   
                                                    <span class="">
                                                    <div class="form-check">
                                                            <input type="checkbox" id="account2" class="form-check-input" name="billingaddress" value="1">
                                                            <label for="account2" style="padding-top:0"><?=!empty($this->lang->line('another_address_for_invoice')) ? $this->lang->line('another_address_for_invoice') : 'Please use
                                                    another address for invoice '?></label>
                                                        </div>
                                                
                                            </span>
                                                    </li>
                                            
                                                </ul>
                                        </form>

                                               </div>
                                    </div>

                                    <!-----------------------------------------------New Address end---------------------------->
                                    <div class="col-sm-6 delivery-address-guest" style="display:none">
                                        <div class="address-column newaddress alignleft fullwidth"
                                           > <form id="guest-delivery" name="guest-delivery" method="post" action="#" >
                                                <ul class="editfields">
                                                    <li>
                                                    
                                                        <label for="first_name"><?= $this->lang->line('first_name') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="bfname" id="bfirst_name" 
                                                                class="form-control" placeholder="<?= $this->lang->line('first_name') 
                                                ?>">
                                                         
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="last_name"><?= $this->lang->line('last_name') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="blname" id="blast_name"
                                                                class="form-control" placeholder="<?= $this->lang->line('last_name') 
                                                ?>">
                                                         
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="email"><?= $this->lang->line('email') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="bemail" id="bemail"
                                                                class="form-control" placeholder="<?= $this->lang->line('email') 
                                                ?>">
                                                         
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="Countries"><?= $this->lang->line('country') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                        <select class="form-control" id="bcountry_id" name="bcountry_id"  required>
                                                        <option value="117" ><?= $this->lang->line('kuwait') ?></option>
                                                         <?php /*
                                                         if($countries){
                                                             foreach($countries as $each){?>
                                                                 <option value="<?= $each['id'] ?>" <?php if(strtolower($each['country']) == 'kuwait') echo 'selected'; ?>><?= $each['country'] ?></option>
                                                          <?php  }
                                                         }*/
                                                         ?>
                                                        
                                                     </select>
                                                    </span>

                                                    </li>
                                                    <li>

                                                        <label for="Areas"><?= $this->lang->line('area') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                        <select class="niceselect_option form-control" name="barea_id"
                                                id="barea_id">
                                                <option value="">---<?= !empty($this->lang->line('select_area')) ? $this->lang->line('select_area') : 'Select Area' ?>----</option>
                                                <?php foreach ($cities as $row) { ?>
                                        <optgroup label="<?= $row['name'] ?>">
                                       <?php
                                        $areas = get_areaslist_undercity($row['id']);

                                        foreach($areas as $a_row){?>
                                        <option value="<?= $a_row['id'] ?>" ><?= $a_row['name'] ?></option>
                                        <?php }?>
                                        </optgroup>
                                        <?php } ?>
                                                

                                            </select>
                                                        </span>

                                                    </li>

                                                    <li>
                                                        <label for="Block"><?= $this->lang->line('block') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="bblock" id="bblock"
                                                                class="form-control" placeholder="<?= $this->lang->line('block') ?>">
                                                          
                                                        </span>
                                                    </li>

                                                    <li>
                                                        <label for="Avenue"> <?=!empty($this->lang->line('avenue')) ? $this->lang->line('avenue') : 'Avenue / Building'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="bavenue" id="bavenue"
                                                                class="form-control" placeholder="<?= $this->lang->line('avenue') ?>">
                                                            
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="Street"> <?=!empty($this->lang->line('street_no')) ? $this->lang->line('street_no') : 'Street No'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="bstreet" id="bstreet"
                                                                class="form-control" placeholder="<?= $this->lang->line('street_no') ?>">
                                                            
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="BuildingNo"> <?=!empty($this->lang->line('house_no')) ? $this->lang->line('house_no') : 'House No/ Flat No '; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="bhouse" id="bhouse"
                                                                class="form-control" placeholder="<?= $this->lang->line('house_no') ?>">
                                                            
                                                        </span>
                                                    </li>
                                                   
                                                    <li>
                                                        <label for="MobileNo1"> <?=!empty($this->lang->line('mobile')) ? $this->lang->line('mobile') : 'Mobile'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="bmobile" id="bmobile"
                                                                class="form-control" placeholder="<?=!empty($this->lang->line('mobile')) ? $this->lang->line('mobile') : 'Mobile'; ?>">
                                                          
                                                        </span>

                                                    </li>
                                                    <li>
                                                        <label for="MobileNo2"><?=!empty($this->lang->line('alternate_mobile')) ? $this->lang->line('alternate_mobile') : 'Alternate Mobile'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="balternate_mobile" id="balternate_mobile"
                                                                class="form-control" placeholder="<?=!empty($this->lang->line('alternate_mobile')) ? $this->lang->line('alternate_mobile') : 'Alternate Mobile'; ?>">
                                                            <span class="field-validation-valid"
                                                                data-valmsg-for="MobileNo2"
                                                                data-valmsg-replace="true"></span>
                                                        </span>
                                                    </li>
                                                   
                                            
                                                </ul></form>
                                                    </div>

                                               </div>
                                    </div>
                                       
                                    <div class="clearfix"></div>
                                    <div class="buttoncontrols">
                                                
                                                    <span class="button disableanchor"><button id="continue-address"
                                                            class="btn btn-primary btn-sm"><?= $this->lang->line('continue') ?> </button></span>

                                        </div>

                                </div>
                                <?php } 
                                else{ ?>
<!-------------------------------------------- DELIVERY ADDREESS ------------------------------->
`                                   <div id="billing-address-tab"
                                    class="tab-pane <?php if ($this->data['is_logged_in']) echo 'active';?>">
                                    <div class="tabtitle alignleft fullwidth">
                                            <h3><?= $this->lang->line('confirm_address') ?></h3>
                                            <span><?= $this->lang->line('delivery_address_sub') ?></span>
                                        </div>
                                        <div class="exist-address-box"><div class="row">
                                    <?php if ($this->data['is_logged_in']){
                                        if (isset($default_address) && !empty($default_address)) { 
							                foreach ($default_address as $row) {?>
                                                <div class="col-xl-4 col-md-4 col-sm-6 col-xs-12" id="addr-<?= $row['id'] ?>">   
                                                <div class="address-column">
                                                  <?php  if($row['is_default'] ==1){ ?>
                                                <div class="addresstype alignleft bluebg"> <?= $this->lang->line('default_address') ?></div>
                                                    <?php } ?>
                                                <ul>
                                                    <li><?= $row['first_name'].' '.$row['last_name'] ?></li>
                                                    <?php
                                                    $address = '';
                                                    if($row['area_id']>0){
                                                        $address .=$this->lang->line('area').' : '.$row['area'];
                                                    }
                                                    if($row['block']!=''){
                                                        $address .=$this->lang->line('block').' : '.$row['block'];
                                                    }
                                                    if($row['street_no']!=''){
                                                        $address .=$this->lang->line('street_no').' : '.$row['street'];
                                                    }
                                                    if($row['avenue']!=''){
                                                        $address .=$this->lang->line('avenue').' : '.$row['avenue'];
                                                    }
                                                    if($row['house_no']!=''){
                                                        $address .=$this->lang->line('house_no').' : '.$row['house'];
                                                    }
                                                    $city ='';
                                                    $country_name ='';
                                                    if($row['city_id']>0){
                                                        $city = $this->lang->line('city').': '.$row['city'].', ';
                                                    }
                                                    if($row['country']>0){
                                                        $country_name = $this->lang->line('country').': '.$row['country_name'];
                                                    }?>
                                                    <li class="addr-details"><?= $address ?></li>
                                                    <li class="addr-state-country"><?= $city. $country_name; ?> </li>
                                                    <li class="addr-phone"><?= $this->lang->line('mobile') .': '.$row['mobile'] ?> </li>
                                                    <?php if($row['alternate_mobile']){?>
                                                    <li class="addr-phone"><?= $this->lang->line('alternate_mobile') .': '.$row['alternate_mobile'] ?> </li>
                                                    <?php } ?>
                                                    <li class="addr-phone"><?= $this->lang->line('email') .': '.$row['email'] ?> </li>

                                                </ul>
                                                <span class="buttoncontrols">
                                                    <button                                                        data-id="<?= $row['id'] ?>"  class="btn btn-primary btn_wdth_fit btn-billing-continue"><?= $this->lang->line('continue') ?></button>
                                                </span>
                                            <div class="address-controls greybg textright fullwidth alignleft">
                                                <span class="delete disableanchor"><a class="delete-checkout-address" data-id="<?= $row['id'] ?>"  href="javascript:void(0)"><?= $this->lang->line('delete') 
                                                ?></a></span>
                                                <span class="edit disableanchor"><a class="edit-address"  data-first-name="<?= $row['first_name'] ?>" data-last-name="<?= $row['last_name'] ?>" data-mobile="<?= $row['mobile'] ?>" data-alternate-mobile="<?= $row['alternate_mobile'] ?>"
                                                data-email="<?= $row['email'] ?>" data-street="<?= $row['street'] ?>"
                                                data-block="<?= $row['block'] ?>" data-avenue="<?= $row['avenue'] ?>"
                                                data-house="<?= $row['house_no'] ?>" data-area="<?= $row['area_id'] ?>" data-country="<?= $row['country'] ?>"
                                                data-type="<?= $row['type'] ?>"  data-is-default="<?= $row['is_default'] ?>" data-id="<?= $row['id'] ?>" 
                                                href="javascript:void(0)"><?= $this->lang->line('edit') 
                                                ?></a></span>
                                            </div>
                                        </div>
                                        </div>
                                           <?php }

                                        }
                                    } ?>
                                    </div>
                                    <div class="clearfix"></div>

                                        <div class="addnewaddress alignleft disableanchor">
                                            <a class="bluecolor" role="button" id="add-new-address"><?= $this->lang->line('add_new_address') 
                                                ?></a>
                                        </div>
                                   


                                    <div class="clearfix"></div>
                                </div>

                                    <!----------------------------------------------New Addres----------------------->
                                    <div class="col-sm-6 new-address-form" style="display:none">
                                        <div class="address-column newaddress alignleft fullwidth"
                                            id="addAddress">
                                            <form action="<?= base_url('my-account/checkout-address-change') ?>" id="checkout-address-form" method="post"
                                                class="ng-pristine ng-valid" novalidate="novalidate">
                                                <ul class="editfields">
                                                    <li>
                                                    <input name="billing_id" id="billing_id" type="hidden"  value="">
                                                        <label for="billing_first_name"><?= $this->lang->line('first_name') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="billing_first_name" id="billing_first_name"
                                                                class="form-control" placeholder="<?= $this->lang->line('first_name') 
                                                ?>">
                                                         
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="billing_last_name"><?= $this->lang->line('last_name') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="billing_last_name" id="billing_last_name"
                                                                class="form-control" placeholder="<?= $this->lang->line('last_name') 
                                                ?>">
                                                         
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="billing_email"><?= $this->lang->line('email') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="billing_email" id="billing_email"
                                                                class="form-control" placeholder="<?= $this->lang->line('email') 
                                                ?>">
                                                         
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="Countries"><?= $this->lang->line('country') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                        <select class="form-control" id="billing_country_id" name="billing_country_id"  required>
                                                        <option value="117" ><?= $this->lang->line('kuwait') ?></option>
                                                         <?php /*
                                                         if($countries){
                                                             foreach($countries as $each){?>
                                                                 <option value="<?= $each['id'] ?>" <?php if(strtolower($each['country']) == 'kuwait') echo 'selected'; ?>><?= $each['country'] ?></option>
                                                          <?php  }
                                                         }*/
                                                         ?>
                                                        
                                                     </select>
                                                    </span>

                                                    </li>
                                                    <li>

                                                        <label for="Areas"><?= $this->lang->line('area') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                        <select class="niceselect_option form-control" name="billing_area_id"
                                                id="billing_area_id">
                                                <option value="">---<?= !empty($this->lang->line('select_area')) ? $this->lang->line('select_area') : 'Select Area' ?>----</option>
                                                <?php foreach ($cities as $row) { ?>
                                        <optgroup label="<?= $row['name'] ?>">
                                       <?php
                                        $areas = get_areaslist_undercity($row['id']);

                                        foreach($areas as $a_row){?>
                                        <option value="<?= $a_row['id'] ?>" ><?= $a_row['name'] ?></option>
                                    <?php }?>
                                    </optgroup>
                               <?php } ?>
                                                

                                            </select>
                                                        </span>

                                                    </li>

                                                    <li>
                                                        <label for="Block"><?= $this->lang->line('block') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="billing_block" id="billing_block"
                                                                class="form-control" placeholder="<?= $this->lang->line('block') ?>">
                                                          
                                                        </span>
                                                    </li>

                                                    <li>
                                                        <label for="Avenue"> <?=!empty($this->lang->line('avenue')) ? $this->lang->line('avenue') : 'Avenue / Building'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="billing_avenue" id="billing_avenue"
                                                                class="form-control" placeholder="<?= $this->lang->line('avenue') ?>">
                                                            
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="Street"> <?=!empty($this->lang->line('street_no')) ? $this->lang->line('street_no') : 'Street No'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="billing_street" id="billing_street"
                                                                class="form-control" placeholder="<?= $this->lang->line('street_no') ?>">
                                                            
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="BuildingNo"> <?=!empty($this->lang->line('house_no')) ? $this->lang->line('house_no') : 'House No/ Flat No '; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="billing_house" id="billing_house"
                                                                class="form-control" placeholder="<?= $this->lang->line('house_no') ?>">
                                                            
                                                        </span>
                                                    </li>
                                                   
                                                    <li>
                                                        <label for="MobileNo1"> <?=!empty($this->lang->line('mobile')) ? $this->lang->line('mobile') : 'Mobile'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="billing_mobile" id="billing_mobile"
                                                                class="form-control" placeholder="<?=!empty($this->lang->line('mobile')) ? $this->lang->line('mobile') : 'Mobile'; ?>">
                                                          
                                                        </span>

                                                    </li>
                                                    <li>
                                                        <label for="MobileNo2"><?=!empty($this->lang->line('alternate_mobile')) ? $this->lang->line('alternate_mobile') : 'Alternate Mobile'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="billing_alternate_mobile" id="billing_alternate_mobile"
                                                                class="form-control" placeholder="<?=!empty($this->lang->line('alternate_mobile')) ? $this->lang->line('alternate_mobile') : 'Alternate Mobile'; ?>">
                                                            <span class="field-validation-valid"
                                                                data-valmsg-for="MobileNo2"
                                                                data-valmsg-replace="true"></span>
                                                        </span>
                                                    </li>
                                                    <li class="email-updates hide_guest">

                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" value="1"
                                                            id="is_default" name="is_default">
                                                        <label class="form-check-label" for="is_default" style="padding-top:0"><?=  $this->lang->line('make_default_address') ?></label>
                                                    </div>

                                                    </li>
                                                </ul>
                                                <span class="buttoncontrols">
                                                    <span class="button disableanchor"><button 
                                                            class="btn btn-sm greygradient" id="cancel-address-form"><?= $this->lang->line('cancel') ?></button></span>
                                                    <span class="button disableanchor"><button id="save-address-form"
                                                            class="btn btn-primary btn-sm"><?= $this->lang->line('save') ?> </button></span>

                                                </span>
                                        </form>
                                        </div>
                                    </div>

                                    <!-----------------------------------------------New Address end---------------------------->

                                </div>



                                <div id="delivery-address-tab"
                                    class="tab-pane">
                                    <div class="tabtitle alignleft fullwidth">
                                            <h3><?= $this->lang->line('delivery_address') ?></h3>
                                            <span><?= $this->lang->line('delivery_address_sub') ?></span>
                                        </div>
                                        <div class="del-exist-address-box"><div class="row">
                                    <?php if ($this->data['is_logged_in']){
                                        if (isset($default_address) && !empty($default_address)) { 
							                foreach ($default_address as $row) {?>
                                                <div class="col-xl-4 col-md-4 col-sm-6 col-xs-12" id="del-addr-<?= $row['id'] ?>">   
                                                <div class="address-column">
                                                  <?php  if($row['is_default'] ==1){ ?>
                                                <div class="addresstype alignleft bluebg"> <?= $this->lang->line('default_address') ?></div>
                                                    <?php } ?>
                                                <ul>
                                                    <li><?= $row['first_name'].' '.$row['last_name'] ?></li>
                                                    <?php
                                                    $address = '';
                                                    if($row['area_id']>0){
                                                        $address .=$this->lang->line('area').' : '.$row['area'];
                                                    }
                                                    if($row['block']!=''){
                                                        $address .=$this->lang->line('block').' : '.$row['block'];
                                                    }
                                                    if($row['street_no']!=''){
                                                        $address .=$this->lang->line('street_no').' : '.$row['street'];
                                                    }
                                                    if($row['avenue']!=''){
                                                        $address .=$this->lang->line('avenue').' : '.$row['avenue'];
                                                    }
                                                    if($row['house_no']!=''){
                                                        $address .=$this->lang->line('house_no').' : '.$row['house'];
                                                    }
                                                    $city ='';
                                                    $country_name ='';
                                                    if($row['city_id']>0){
                                                        $city = $this->lang->line('city').': '.$row['city'].', ';
                                                    }
                                                    if($row['country']>0){
                                                        $country_name = $this->lang->line('country').': '.$row['country_name'];
                                                    }?>
                                                    <li class="addr-details"><?= $address ?></li>
                                                    <li class="addr-state-country"><?= $city. $country_name; ?> </li>
                                                    <li class="addr-phone"><?= $this->lang->line('mobile') .': '.$row['mobile'] ?> </li>
                                                    <?php if($row['alternate_mobile']){?>
                                                    <li class="addr-phone"><?= $this->lang->line('alternate_mobile') .': '.$row['alternate_mobile'] ?> </li>
                                                    <?php } ?>
                                                    <li class="addr-phone"><?= $this->lang->line('email') .': '.$row['email'] ?> </li>

                                                </ul>
                                                <span class="buttoncontrols">
                                                    <button                                                        data-id="<?= $row['id'] ?>"  class="btn btn-primary btn_wdth_fit btn-delivery-continue"><?= $this->lang->line('continue') ?></button>
                                                </span>
                                            <div class="address-controls greybg textright fullwidth alignleft">
                                            <span class="delete disableanchor"><a class="delete-checkout-address" data-id="<?= $row['id'] ?>"  href="javascript:void(0)"><?= $this->lang->line('delete') 
                                                ?></a></span>
                                                <span class="edit disableanchor"><a class="edit-address"  data-first-name="<?= $row['first_name'] ?>" data-last-name="<?= $row['last_name'] ?>" data-mobile="<?= $row['mobile'] ?>" data-alternate-mobile="<?= $row['alternate_mobile'] ?>"
                                                data-email="<?= $row['email'] ?>" data-street="<?= $row['street'] ?>"
                                                data-block="<?= $row['block'] ?>" data-avenue="<?= $row['avenue'] ?>"
                                                data-house="<?= $row['house_no'] ?>" data-area="<?= $row['area_id'] ?>" data-country="<?= $row['country'] ?>"
                                                data-type="<?= $row['type'] ?>"  data-is-default="<?= $row['is_default'] ?>" data-id="<?= $row['id'] ?>" 
                                                href="javascript:void(0)"><?= $this->lang->line('edit') 
                                                ?></a></span>
                                            </div>
                                        </div>
                                        </div>
                                           <?php }

                                        }
                                    } ?>
                                    </div>
                                    <div class="clearfix"></div>

                                        <div class="addnewaddress alignleft disableanchor">
                                            <a class="bluecolor" role="button" id="del-add-new-address"><?= $this->lang->line('add_new_address') 
                                                ?></a>
                                        </div>
                                   


                                    <div class="clearfix"></div>
                                </div>

                                    <!----------------------------------------------New Addres----------------------->
                                    <div class="col-sm-6 del-new-address-form" style="display:none">
                                        <div class="address-column newaddress alignleft fullwidth"
                                            id="addAddress">
                                            <form action="<?= base_url('my-account/delivery-checkout-address-change') ?>" id="del-checkout-address-form" method="post"
                                                class="ng-pristine ng-valid" novalidate="novalidate">
                                                <ul class="editfields">
                                                    <li>
                                                    <input name="delivery_id" id="delivery_id" type="hidden"  value="">
                                                        <label for="delivery_first_name"><?= $this->lang->line('first_name') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="delivery_first_name" id="delivery_first_name"
                                                                class="form-control" placeholder="<?= $this->lang->line('first_name') 
                                                ?>">
                                                         
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="delivery_last_name"><?= $this->lang->line('last_name') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="delivery_last_name" id="delivery_last_name"
                                                                class="form-control" placeholder="<?= $this->lang->line('last_name') 
                                                ?>">
                                                         
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="delivery_email"><?= $this->lang->line('email') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="delivery_email" id="delivery_email"
                                                                class="form-control" placeholder="<?= $this->lang->line('email') 
                                                ?>">
                                                         
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="Countries"><?= $this->lang->line('country') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                        <select class="form-control" id="delivery_country_id" name="delivery_country_id"  required>
                                                        <option value="117" ><?= $this->lang->line('kuwait') ?></option>
                                                         <?php /*
                                                         if($countries){
                                                             foreach($countries as $each){?>
                                                                 <option value="<?= $each['id'] ?>" <?php if(strtolower($each['country']) == 'kuwait') echo 'selected'; ?>><?= $each['country'] ?></option>
                                                          <?php  }
                                                         }*/
                                                         ?>
                                                        
                                                     </select>
                                                    </span>

                                                    </li>
                                                    <li>

                                                        <label for="Areas"><?= $this->lang->line('area') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                        <select class="niceselect_option form-control" name="delivery_area_id"
                                                id="delivery_area_id">
                                                <option value="">---<?= !empty($this->lang->line('select_area')) ? $this->lang->line('select_area') : 'Select Area' ?>----</option>
                                                <?php foreach ($cities as $row) { ?>
                                        <optgroup label="<?= $row['name'] ?>">
                                       <?php
                                        $areas = get_areaslist_undercity($row['id']);

                                        foreach($areas as $a_row){?>
                                        <option value="<?= $a_row['id'] ?>" ><?= $a_row['name'] ?></option>
                                    <?php }?>
                                    </optgroup>
                               <?php } ?>
                                                

                                            </select>
                                                        </span>

                                                    </li>

                                                    <li>
                                                        <label for="Block"><?= $this->lang->line('block') 
                                                ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="delivery_block" id="delivery_block"
                                                                class="form-control" placeholder="<?= $this->lang->line('block') ?>">
                                                          
                                                        </span>
                                                    </li>

                                                    <li>
                                                        <label for="Avenue"> <?=!empty($this->lang->line('avenue')) ? $this->lang->line('avenue') : 'Avenue / Building'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="delivery_avenue" id="delivery_avenue"
                                                                class="form-control" placeholder="<?= $this->lang->line('avenue') ?>">
                                                            
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="Street"> <?=!empty($this->lang->line('street_no')) ? $this->lang->line('street_no') : 'Street No'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="delivery_street" id="delivery_street"
                                                                class="form-control" placeholder="<?= $this->lang->line('street_no') ?>">
                                                            
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <label for="BuildingNo"> <?=!empty($this->lang->line('house_no')) ? $this->lang->line('house_no') : 'House No/ Flat No '; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="delivery_house" id="delivery_house"
                                                                class="form-control" placeholder="<?= $this->lang->line('house_no') ?>">
                                                            
                                                        </span>
                                                    </li>
                                                   
                                                    <li>
                                                        <label for="MobileNo1"> <?=!empty($this->lang->line('mobile')) ? $this->lang->line('mobile') : 'Mobile'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="delivery_mobile" id="delivery_mobile"
                                                                class="form-control" placeholder="<?=!empty($this->lang->line('mobile')) ? $this->lang->line('mobile') : 'Mobile'; ?>">
                                                          
                                                        </span>

                                                    </li>
                                                    <li>
                                                        <label for="MobileNo2"><?=!empty($this->lang->line('alternate_mobile')) ? $this->lang->line('alternate_mobile') : 'Alternate Mobile'; ?></label>
                                                        <span class="field noSwipe">
                                                            <input type="text" name="delivery_alternate_mobile" id="delivery_alternate_mobile"
                                                                class="form-control" placeholder="<?=!empty($this->lang->line('alternate_mobile')) ? $this->lang->line('alternate_mobile') : 'Alternate Mobile'; ?>">
                                                            <span class="field-validation-valid"
                                                                data-valmsg-for="MobileNo2"
                                                                data-valmsg-replace="true"></span>
                                                        </span>
                                                    </li>
                                                    <li class="email-updates hide_guest">

                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" value="1"
                                                            id="delivery_is_default" name="delivery_is_default">
                                                        <label class="form-check-label" for="delivery_is_default" style="padding-top:0"><?=  $this->lang->line('make_default_address') ?></label>
                                                    </div>

                                                    </li>
                                                </ul>
                                                <span class="buttoncontrols">
                                                    <span class="button disableanchor"><button 
                                                            class="btn btn-sm greygradient" id="del-cancel-address-form"><?= $this->lang->line('cancel') ?></button></span>
                                                    <span class="button disableanchor"><button id="del-save-address-form"
                                                            class="btn btn-primary btn-sm"><?= $this->lang->line('save') ?> </button></span>

                                                </span>
                                        </form>
                                        </div>
                                    </div>

                                    <!-----------------------------------------------New Address end---------------------------->




                                </div>
<?php } ?>
                                <!-------------------------------------------------------------review----------------------------------------------------------------------->

                                <div id="review-tab" class="tab-pane">

                                    <div class="tabtitle  ">
                                        <h3><?= !empty($this->lang->line('shopping_cart')) ? $this->lang->line('shopping_cart') : 'Cart' ?></h3>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="shopping-cart">
                <?php
                        if($cart['sub_total'] >0){?>
                    <div class="shopping-cart-inner">
                        
                        <div class="table-responsive">
                            <table class="table table-border shopping-cart-table">
                                <thead>
                                    <tr>
                                        <th class="bdr_lft"><?= !empty($this->lang->line('image')) ? $this->lang->line('image') : 'Image' ?></th>
                                        <th class="bdr_lft"><?= !empty($this->lang->line('product')) ? $this->lang->line('product') : 'Product' ?></th>
                                        <th class="bdr_lft"><?= !empty($this->lang->line('price')) ? $this->lang->line('price') : 'Price' ?></th>
                                        <th class="bdr_lft"><?= !empty($this->lang->line('quantity')) ? $this->lang->line('quantity') : 'Quantity' ?></th>
                                        <th class="bdr_lft"><?= !empty($this->lang->line('subtotal')) ? $this->lang->line('subtotal') : 'Subtotal' ?></th>
                                        <th class="bdr_lft"><button class="btn-remove"><i class="las la-times"></i></button></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                
                              foreach ($cart as $key => $row) {
                                if (isset($row['qty']) && $row['qty'] != 0) {
                                ?>
                                <tr id="cart_<?= $row['product_variant_id'] ?>" class="cart_item1 cart_<?= $row['product_variant_id'] ?>">
                                        <td class="bdr_lft">
                                            <div class="product-image"><a href="#">
                                                    <img src="<?= $row['image'] ?>" alt="">
                                                </a></div>
                                        </td>
                                        
                                        <td class="bdr_lft">
                                            <div class="id">
                                                <input type="hidden" name="<?= 'id[' . $key . ']' ?>" id="id" value="<?= $row['id'] ?>">
                                            </div>
                                            
                                            <a href="#" class="product-name"><?= $row[$lang_prefix.'name']; ?>
                                            <?php if (!empty($row['product_variants'])) { ?>
                                                    <?= str_replace(',', '   ', $row['product_variants'][0][$lang_prefix.'variant_values']) ?>
                                                <?php } ?>
                                                </a>
                                        </td>
                                        <td class="bdr_lft"><label><?= !empty($this->lang->line('price')) ? $this->lang->line('price') : 'Price' ?>:</label> 
                                       <?php 
                                       if($row['special_price'] >0) 
                                       $qprice = $row['special_price'];
                                       else
                                       $qprice =$row['price'];
                                       if(number_format($row['special_price'],2) >0) 
                                                    echo '<del class="old-price">'.$settings['currency'] . ' ' . number_format($row['price'], 2).'</del><span class="product-price" >'. $settings['currency'] . ' ' . number_format($row['special_price'], 2).'</span>';
                                                    else
                                                    echo '<span class="product-price price_cart">'. $settings['currency'] . ' ' . number_format($row['price'], 2).'</span>';
                                                    ?></td>
                                                   
                                        <td class="bdr_lft item-quantity"><label><?= !empty($this->lang->line('quantity')) ? $this->lang->line('quantity') : 'Quantity' ?>:</label>
                                            <div class="number-picker num-block skin-2 product-quantity">
                                                <div class="info-qty  num-in">
                                                    <a class="qty-down minus dis" href="#" ><i class="fa fa-minus"></i></a>
                                                   <span class="qty-val"><?= $row['qty'] ?></span>
                                                 
                                                    <a class="qty-up plus" href="#" ><i class="fa fa-plus"></i></a>
                                                    <input type="hidden" name="qty" value="<?= $row['qty'] ?>" data-page="cart" data-id="<?= $row['product_variant_id']; ?>" data-price="<?= $qprice; ?>" data-step="<?= (isset($row['minimum_order_quantity']) && !empty($row['quantity_step_size'])) ? $row['quantity_step_size'] : 1 ?>" data-min="<?= (isset($row['minimum_order_quantity']) && !empty($row['minimum_order_quantity'])) ? $row['minimum_order_quantity'] : 1 ?>" data-max="<?= (isset($row['total_allowed_quantity']) && !empty($row['total_allowed_quantity'])) ? $row['total_allowed_quantity'] : '' ?>">
                                                </div>
                                            </div>
                                        </td>
                                        <td class="bdr_lft total-price cart_info1">
                                            <label><?= !empty($this->lang->line('subtotal')) ? $this->lang->line('subtotal') : 'Subtotal' ?>:</label> <span class="product-price price_cart1" data-price=" <?php if($row['special_price'] >0) 
                                                    echo ($row['qty'] * $row['special_price']);
                                                    else
                                                    echo ($row['qty'] * $row['price']);
                                                    ?>">
                                        <?php if(number_format($row['special_price'],2) >0) 
                                                    echo  $settings['currency'] . ' ' . number_format(($row['qty'] * $row['special_price']), 2);
                                                    else
                                                    echo $settings['currency'] . ' ' . number_format(($row['qty'] * $row['price']), 2);
                                                    ?>
                                       </span></td>
                                        <td class="bdr_lft cart_remove"><button class="btn-remove product-removal remove-product" data-id="<?= $row['product_variant_id']; ?>"><i class="fa fa-times" name="remove_inventory"  id="remove_inventory"  title="<?= !empty($this->lang->line('remove_from_cart')) ? $this->lang->line('remove_from_cart') : 'Remove From Cart' ?>"></i></button></td>
                                    </tr>
                                    <?php }
                            } ?>
                                   
                                </tbody>
                            </table>
                        </div>
                        <button class="btn btn-primary btn-review-continue"> <?= $this->lang->line('continue') ?>  </button>
                        </div>
                        <?php } ?>


                                      

                                    

                                    </div>
                                  
                                </div>

                                <!--------------------------------------------------------------payment---------------------------------------------------------------------->

                                <div id="payment-tab" class="tab-pane">
                                <form class="needs-validation" id="checkout_form" method="POST"
                        action="<?= base_url('cart/place-order') ?>">

                                    <div class="tabtitle  fullwidth">
                                        <h3><?= $this->lang->line('review_order') ?> </h3>
                                        <span><?= $this->lang->line('checkout_review_notes') ?> </span>
                                    </div>
                                    <div class="shadow-radius-box alignleft fullwidth whitebg">
                                        <div class="review-details alignleft">
                                        <ul class="list-inline order-summary-list">
                                        <li><label><?= !empty($this->lang->line('delivery_charges')) ? $this->lang->line('delivery_charges') : 'Delivery Charges' ?></label>:<span class="price-amount">
                                            <?= $settings['currency'] . ' <span id="cart-shipping1">' . number_format($cart['delivery_charge'], 3).'</span>' ?>
                                        </span></li>
                                            <li><label><?= !empty($this->lang->line('subtotal')) ? $this->lang->line('subtotal') : 'Subtotal' ?></label>: <span class="price-amount">
                                            <?= $settings['currency'] . ' <span class="cart-subtotal1">' . number_format($cart['sub_total'], 3).'</span>' ?>
                                        </span></li>
                                        <li id="promocodeBox" style="display:none;"> <label><?= !empty($this->lang->line('promocode_discounts')) ? $this->lang->line('promocode_discounts') : 'Promocode Discount' ?>(<span id="promocode_text"></span>) :</label><span class="price-amount">- <?= $settings['currency'] ?><span id="promocode_amount"></span></li>
                        </ul>
                                        </div>
                                        <div class="review-totalamt bluebg alignright"><span class="total-price">
                                        <?= $settings['currency'] ?>
                                        <span id="cart-total1"><?= number_format($cart['amount_inclusive_tax'], 2) ?></span>
                                    </span> </div>
                                    </div>
                                    <!--shadow-radius-box-->
                                    <div class="paymentmode alignleft fullwidth">
                                        <div class="tabtitle alignleft fullwidth">
                                            <h3><?= $this->lang->line('choose_payment') ?></h3>
                                        </div>
                                        <div class="shadow-radius-box alignleft fullwidth whitebg">
                                            <ul class="choosepayment ">

                                                <li>
                                                    <div class="form-radio">
                                                        <input type="radio" name="payment_method" id="knet"
                                                            value="knet" checked> <label for="knet"><img
                                                                src="<?= THEME_ASSETS_URL ;?>images/knet.jpg"
                                                                alt="Payment Method"></label>
                                                    </div>

                                                </li>
                                                <!-- <li>
                                                    <div class="form-radio">
                                                        <input type="radio" name="form.payment_method" id="visa"
                                                            value="visa"> <label for="visa"><img
                                                                src="<?= THEME_ASSETS_URL ;?>images/visa-master.jpg"
                                                                alt="Payment Method"></label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-radio">
                                                        <input type="radio" name="form.payment_method" id="cod"
                                                            value="cod"> <label for="cod"><img
                                                                src="<?= THEME_ASSETS_URL ;?>images/cod.png"
                                                                alt="Payment Method"></label>
                                                    </div>



                                                </li> -->
                                            </ul>
                                        </div>
                                        <!--shadow-radius-box-->
                                        <div class="voucher fullwidth alignleft">
                                           
                                                <!-- <div class="barcode"><img
                                                        src="<?= THEME_ASSETS_URL ;?>images/barcode.jpg" alt="barcode">
                                                </div> -->
                                                <div class="row">
                                                   
                                                        <div class="col-lg-3">
                                                            <div class="form-group" id="promo-input-box">
                                                                <input type="text" name="promo_code" class="form-control"
                                                                    placeholder="<?= !empty($this->lang->line('promocode_text')) ? $this->lang->line('promocode_text') : 'PROMO CODE' ?>" id="promocode_input" style="width:100%;">
                                                            </div>
                                                            <div class="form-group" id="promo-text-box" style="display:none;">                                                                
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <div class="input-group-append">
                                                                <button class="btn btn-primary btn-sm btn-theme" id="redeem_btn">
                                                                    <?=!empty($this->lang->line('apply')) ? $this->lang->line('apply') : 'Apply'?>
                                                                </button>
                                                                <button class="btn btn-primary btn-sm btn-theme d-none"
                                                                    id="clear_promo_btn">
                                                                    <?=!empty($this->lang->line('clear')) ? $this->lang->line('clear') : 'Clear'?>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
    
    
                                            



                                            <!-- ngIf: WalletEnabled == true -->
                                        </div>
                                        <!--voucher-->
                                        <div id="paymentresult" class="payoverlay alignleft fullwidth">
                                            <div class="paymentnotify info-msg textcenter"></div>
                                        </div>
                                    </div>
                          
                        <?php if ($this->data['is_logged_in']) { ?>
                            <input type="hidden"  id="account_type" value="user">
                            <input type="hidden" name="address_id" id="user_address_id" value="">
                            <input type="hidden" name="baddress_id" id="user_baddress_id" value="">
                            <!-- <input type="hidden" name="product_variant_id"
                                            value="<?= implode(',',array_column($cart, 'id')) ?>">
                            <input type="hidden" name="quantity"
                                            value="<?= implode(',',array_column($cart, 'qty')) ?>"> -->
                           
                            <?php }
                            else{
                                echo '<input type="hidden"  id="account_type" value="guest">';
                            } 
                          ?>
                            <input type="hidden" id="final_total" name="final_total" value="<?= $cart['amount_inclusive_tax'] ?>">
                            <input type="hidden" id="cartSubTotal" name="cartSubTotal" value="<?= $cart['sub_total'] ?>">
                        <input type="hidden" name="delivery_charge" value="<?= number_format($cart['delivery_charge'], 3) ?>">
                            
                                        
                                    <div class="cartbuttonwrap fullwidth alignleft">
                                        <span class="buttoncontrols">

                                            <input type="submit" class="btn btn-primary btn-proceed-to-checkout" value="<?= $this->lang->line('proceed_to_payment')?>" name="place-order-btn" id="save-order">  
                                        </span>
                                    </div>
                               
                                    <!--cartbuttonwrap-->

                                    </form>

                                </div>



                               

                            </div>
                            <!-- End Content Tab -->
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- End List Tab Product -->

        </div>

    </section>


</div>

<form id="knet_form" action="<?= base_url('knet/request') ?>" method="POST">
    <input type="hidden" name="knet_order_id" id="knet_order_id" value="">
    <input type="hidden" name="ekart_security_token" id="csrf_token" value="">
</form>
<div class="modal fade" id="forgotPassword" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title text-left"><?= !empty($this->lang->line('forgot_password')) ? $this->lang->line('forgot_password') : 'Forgot Password' ?></h4>
          <button type="button" class="close text-right" data-dismiss="modal">&times;</button>
         
        </div>
        <form id="forgot_password_form" name="forgot_password_form" method="POST" action="<?= base_url('auth/forgot_password') ?>" class="form-submit-event6">
        <div class="modal-body">
        
        <div id="infoMessage"></div>
                    <div class="d-flex justify-content-center">
                <div class="form-group" id="error_box"></div>
            </div>
                        <div class="form-group"><label for="email"><?= !empty($this->lang->line('email')) ? $this->lang->line('email') : 'Email' ?><span>*</span></label> <input type="text"  value="" id="email" class="form-control" name="identity" required></div>
                        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" data-loading="" class="btn btn-primary btn-sign-in submit_btn">
                         <?= !empty($this->lang->line('submit')) ? $this->lang->line('submit') : 'Submit' ?>
                </button></form> 
        </div>
      </div>
      
    </div>
  </div>
