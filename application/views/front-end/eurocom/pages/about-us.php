<div class="container">

        <div class="page_name">
            <div class="row">
                <div class="col-md-6 ">
                    <h3><?=!empty($this->lang->line('about_us')) ? $this->lang->line('about_us') : 'About Us'?></h3>
                </div>
                <div class="col-md-6 ">
                    <div class="breadcrumb">
                        <ul class="list-inline">
                            <li><a href="<?= base_url() ?>"><?=!empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home'?></a></li>

                            <li class="active"><?=!empty($this->lang->line('about_us')) ? $this->lang->line('about_us') : 'About Us'?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>



    </div>
    <div id="content">

<section class="custom-page-wrap clearfix">
    <div class="container">
        <div class="custom-page-content clearfix">
            <p><img style="display: block; margin-left: auto; margin-right: auto;"  src="<?= THEME_ASSETS_URL ;?>images/about.jpeg" alt="" width="100%" height="100%"></p>


        <?= $about_us ?>
        </div>
                </div>
            </div>
        </section>





    </div>