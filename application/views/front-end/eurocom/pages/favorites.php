<div class="container">

    <div class="page_name">
        <div class="row">
            <div class="col-md-6 ">
                <h3><?=!empty($this->lang->line('my_wishlist')) ? $this->lang->line('my_wishlist') : 'My Wishlist'?></h3>
            </div>
            <div class="col-md-6 ">
                <div class="breadcrumb">
                    <ul class="list-inline">
                        <li><a href="<?= base_url() ?>"><?=!empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home'?></a></li>

                        <li class="active"><?=!empty($this->lang->line('my_wishlist')) ? $this->lang->line('my_wishlist') : 'My Wishlist'?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



</div>



<div id="content">
    <section class="account-wrap">
        <div class="container">
            <div class="account-wrap-inner">

                <div class="account-left">
                    <?php $this->load->view('front-end/' . THEME . '/pages/my-account-sidebar') ?>

                </div>


                <div class="account-right">
                    <div class="panel-wrap">
                        <div class="panel">
                            <div class="panel-header">
                                <h4><?=!empty($this->lang->line('my_wishlist')) ? $this->lang->line('my_wishlist') : 'My Wishlist'?></h4>

                            </div>
                            <div class="panel-body">
                            <div class="order-details-middle mt-3">
                            <?php
                            if (isset($products) && !empty($products)) {?>
<div class="table-responsive">
    <table class="table table-borderless order-details-table">
        <thead>
            <tr>
                <th><?= !empty($this->lang->line('image')) ? $this->lang->line('image') : 'Image' ?></th>
                <th><?= !empty($this->lang->line('product')) ? $this->lang->line('product') : 'Product' ?></th>
                <th><?= !empty($this->lang->line('price')) ? $this->lang->line('price') : 'Price' ?></th>
                <th><?= !empty($this->lang->line('availability')) ? $this->lang->line('availability') : 'Availability' ?></th>
                <th><?= !empty($this->lang->line('action')) ? $this->lang->line('action') : 'Action' ?></th>
            </tr>
        </thead>
        <tbody>
                    
					<?php
                            
                                foreach ($products as $row) { 
                                    if ($row['stock_type'] == 0) {
                                        if ($row['stock'] != null) {
                                            $stock = intval($row['stock']);
                                            if ($stock <= 0) {
                                                
                                            $stockupdate=1;
                                            
                                        
                                                             }
                                        }
                                    }
                                                        ?>
                        <tr id="fav-<?= $row['id'] ?>">
                            <td><div class="product-image"><a href="<?= base_url('products/details/' . $row['slug']) ?>"><img src="<?= $row['image_sm'] ?>"></a></div></td>
                            <td class="name"> <a class="product-name" href="<?= base_url('products/details/' . $row['slug']) ?>"><?= $row[$lang_prefix.'name'] ?></a></td>
                            <td class="price"><label><?= !empty($this->lang->line('price')) ? $this->lang->line('price') : 'Price' ?></label>
                            <?php if ($row['type'] == "simple_product") { 
												 if($row['min_max_price']['special_price']!=0)
												 {
												 ?>
                                         <span class="old_price"><?= $settings['currency'] ?> <?= number_format($row['min_max_price']['min_price'],3) ?></span>
                                        <span class="product-price red"><?= $settings['currency'] ?> <?= number_format($row['min_max_price']['special_price'],3) ?></span>
												 <?php
												 }
												 else{?>
												<span class="product-price"><?= $settings['currency'] ?> <?= number_format($row['min_max_price']['min_price'],3) ?></span>	 
												 <?php }
												 }
												else
												{
													?>
													<span class="product-price"><?php echo $price['range'];?></span>
													<?php
												}
												 ?></td>
                                                    <td><label><?= !empty($this->lang->line('availability')) ? $this->lang->line('availability') : 'Availability' ?></label>
                                                    <?php if($stockupdate == 1){?>
                                                        <span class="badge badge-outstock">
                                                        <?= !empty($this->lang->line('out_of_stock')) ? $this->lang->line('out_of_stock') : 'Out of Stock' ?>
                                                        </span>
                                                       <?php } 
                                                       else{ ?>
                                                       <span class="badge badge-success">
                                                       <?= !empty($this->lang->line('in_stock')) ? $this->lang->line('in_stock') : 'In Stock' ?>
                                                        </span>
                                                       <?php } ?></td>

												 
                            <td ><label> <?= !empty($this->lang->line('action')) ? $this->lang->line('action') : 'Action' ?></label> <span class="product-price">
                            <?php
                                                    if (count($row['variants']) <= 1) {
                                                        $variant_id = $row['variants'][0]['id'];
                                                        $modal = "";
														$class="add_to_cart";
                                                        ?>
                                                        <a href="javascript:void(0);" class="btn btn-view add-to-cart" data-product-qty="1" data-product-id="<?= $row['id'] ?>" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>"><i class="fas fa-cart-arrow-down"></i>
                                                        <?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'Add to Cart' ?>
                                                    </a>
                                                    <?php
                                                    } else {
                                                        $variant_id = "";
                                                        $modal = "#quick-view";
														$class="quick-view-btn";
                                                        ?>
                                                        <a href="<?= base_url('products/product_quickview/'. $row['id']) ?>"
                                                        class="btn btn-view manual-ajax"
                                                        rel="modal:open" title="<?= !empty($this->lang->line('quick_view')) ? $this->lang->line('quick_view') : 'Quick View' ?>"><i class="fa fa-shopping-cart"></i></a>
                                                        <?php
                                                    } ?>
 <a href="javascript:void(0);" class="btn btn-view delete-from-fav fa text-danger" data-product-id="<?= $row['id'] ?>"><i class="fas fa-trash"></i>
 <?= !empty($this->lang->line('delete')) ? $this->lang->line('delete') : 'Delete' ?>
                                        </a>
                         
                                                </span></td>
                        </tr>
                            <?php }
                             ?>
                    </tbody>
                </table>
                                </div>
                         <?php } else { ?> 
                            <?= !empty($this->lang->line('empty_wishlist_message')) ? $this->lang->line('empty_wishlist_message') : 'Your wishlist is empty!!' ?>
							<?php } ?>
								
                                </div>
                                </div>
                            </div>
                        </div>


                    </div>



                </div>

    </section>


</div>