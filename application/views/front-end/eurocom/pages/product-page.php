<?php $total_images = 0; 

$stockupdate=0;
$price_display  ='';
$d_variant_ids = [];
$sku = '';
//print_r($product['product'][0]['default_product']);
//print_r($product['product'][0]);
if(isset($product['product'][0]['default_product'])){
    if($product['product'][0]['default_product']->images !=''){
        $image = json_decode($product['product'][0]['default_product']->images);
       //echo $image[0];
      $main_image =isset($image[0]) ? get_image_url($image[0]) : get_image_url($product['product'][0]['image']);
    }
    else{
        $main_image =get_image_url($product['product'][0]['image']);
    }
    $m = $lang_prefix.'variant_values'; 
  $product_name = $product['product'][0][$lang_prefix.'name'].' '.$product['product'][0]['default_product']->$m;
  
    if($product['product'][0]['type'] =='variable_product'){
        if($product['product'][0]['stock_type']==NULL){
            if($product['product'][0]['stock']!=null && intval($product['product'][0]['stock']) <=0)
            $stockupdate=1;
        
        else{
            $stockupdate=0;
        }
        }
        elseif($product['product'][0]['stock_type']=='2'){
            if($product['product'][0]['default_product']->stock!=NULL && intval($product['product'][0]['default_product']->stock) <=0){
                $stockupdate=1;
            }
            else{
                $stockupdate=0;
            }
        }
        
        
    }
    else
    {
        if($product['product'][0]['stock_type'] ==NULL){

            $stockupdate=0;
        }
        else{
            if($product['product'][0]['stock']!=NULL && intval($product['product'][0]['stock']) <=0){
                $stockupdate=1;
            }
            else{
                $stockupdate=0;
            }
        }
    } 
    $d_attr_name = $lang_prefix.'attr_name';
    $d_selected_att = isset($product['product'][0]['default_product']->$d_attr_name) ? explode(',' ,$product['product'][0]['default_product']->$d_attr_name) :'';
    $d_variant_values = $lang_prefix.'variant_values';
    $d_selected_variants = isset($product['product'][0]['default_product']->$d_variant_values) ? explode(',' ,$product['product'][0]['default_product']->$d_variant_values) : '';
    $select_details =[];
    if(isset($product['product'][0]['default_product']->$d_variant_values) && isset($d_selected_variants)){
        foreach($d_selected_variants as $key =>$v){
            $select_details[$key] = isset($d_selected_att[$key]) ? $d_selected_att[$key].': '.$v : '';
        }         
    }
    $select_details = implode(' ',$select_details);

    $price_display = '<div class="product-price">';
    if($product['product'][0]['default_product']->special_price > 0 && $product['product'][0]['default_product']->special_price< $product['product'][0]['default_product']->price ){
        $price_display .= '<span id="price" >'.$settings['currency'] .number_format($product['product'][0]['default_product']->special_price,3) .'</span><span class="previous-price" id="striped-price" >'. $settings['currency'] . number_format($product['product'][0]['default_product']->price,3).'</span>';
     }
    else{    
        $price_display .= '<span id="price" >'.$settings['currency'] . number_format($product['product'][0]['default_product']->price,3) .'</span>';	 
    } 
    $price_display .= '</div>';
    $d_variant_ids = explode(',',$product['product'][0]['default_product']->variant_ids);
    $variant_id = $product['product'][0]['default_product']->id;
    $sku = isset($product['product'][0]['default_product']->sku) ? $product['product'][0]['default_product']->sku : $product['product'][0]['variants'][0]['sku'] ;
    if( $product['product'][0]['default_product']->is_favorite == 1) 
        $is_fav =' text-danger' ;
    else
         $is_fav =  '' ;
}
else{
    $main_image =$product['product'][0]['image'];
    $product_name = $product['product'][0][$lang_prefix.'name'];
    if (($product['product'][0]['stock_type'] != null && $product['product'][0]['stock_type'] != '')) {
        //Case 1 : Simple Product(simple product)
            if ($product['product'][0]['stock_type'] == 0) {
                if ($product['product'][0]['stock'] != null) {
                    $stock = intval($product['product'][0]['stock']);
                    if ($stock <= 0) {
                        $stockupdate=1;
                    }
                }
            }           
        }
        $select_details = '';
        $price = get_price_range_of_product($product['product'][0]['id']);
        if ($product['product'][0]['type'] == "simple_product") { 
					
            if($product['product'][0]['min_max_price']['special_price']!=0)
            {
            
           $price_display = '<div class="product-price">
            <span id="price" >'.$settings['currency'] .number_format($product['product'][0]['min_max_price']['special_price'],3) .'</span>
            <span class="previous-price" id="striped-price" >
           '.$settings['currency'].number_format($product['product'][0]['min_max_price']['min_price'],3).'
            </span>
            </div>';

            
            }
            else{
                $price_display = '<div class="product-price">
            <span id="price" >'.$settings['currency'] . number_format($product['product'][0]['min_max_price']['min_price'],3) .'</span>
            <span class="previous-price" id="striped-price" ></span>
            </div>';
             }
            }
           else
           {
             
            $price_display = '<div class="product-price">
            <span  id="price">  '.$price['range'].'</span>
            <span class="previous-price" id="striped-price" ></span>
            </div>';
           
           }
           if (count($product['product'][0]['variants']) <= 1) {
            $variant_id = $product['product'][0]['variants'][0]['id'];
        } else {
            $variant_id = "";
        }
        $sku = isset($product['product'][0]['variants'][0]['sku'] ) ? $product['product'][0]['variants'][0]['sku'] : '';
        if($product['product'][0]['is_favorite'] == 1)
        $is_fav =' text-danger' ;
        else
             $is_fav =  '' ;
             
}
$enabled_id = '';$c=0;
if (!empty($product['product'][0]['variants']) && isset($product['product'][0]['variants'])) {
    foreach ($product['product'][0]['variants'] as $variant) {
        $combinations = isset($variant['variant_ids']) ? explode(',',$variant['variant_ids']) : [];
        foreach($d_variant_ids as $ds){
            if(in_array($ds,$combinations)){
                $a_k = array_search($ds,$combinations);
                unset($combinations[$a_k]);
                if($enabled_id =='')
                $enabled_id = implode(',',$combinations);
                else
                $enabled_id = $enabled_id.','.implode(',',$combinations);
                $c++;
            }
        }
    }} 
    if($enabled_id!=''){   
        $enabled_id = explode(',',$enabled_id);   
    } else{
        $enabled_id = [];
    }

?>

<!-- breadcrumb -->
<div class="container">
		
		<div class="page_name">
			<div class="row">
		 <div class="col-md-6 ">	
		<h3><?= !empty($this->lang->line('product_details')) ? $this->lang->line('product_details') : 'Product Details' ?></h3>
			</div> 
			 <div class="col-md-6 ">	
        <div class="breadcrumb">			
             <ul class="list-inline">
                <li><a href="<?= base_url() ?>"><?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?></a></li>
                <li><a href="<?= base_url('products/category/' . $catslug) ?>">
                    <?php echo  ($lang_prefix=='') ? $catname : $ar_catname; ?>
                </a></li>
                <li class="active">
                <?= $product_name ?>
            </li>
            </ul>
        </div>
		</div>		 
		</div>
			</div>
		
		
		
    </div>	
		

<div id="content">



    <section class="product-details-wrap">
        <div class="container">
            <div class="product-details-top">
                <div class="product-details-top-inner">
                

                    <div class="product-image-gallery">
                        <div class="app-figure" id="zoom-fig">
                            <a id="Zoom-1" class="MagicZoom"
                                title="Show your product in stunning detail with Magic Zoom Plus."
                                href="<?= $main_image ?>"
                                data-zoom-image-2x="<?= $main_image ?>">
                                <img src="<?= $main_image ?>" alt="" />
                            </a>
                            <div class="selectors">
                                <a data-zoom-id="Zoom-1" href="<?= $main_image ?>?h=1400"
                                    data-image="<?= $main_image ?>?h=400"
                                    data-zoom-image-2x="<?= $main_image ?>?h=2800"
                                    data-image-2x="<?= $main_image ?>?h=800">
                                    <img srcset="<?= $main_image ?>" />
                                </a>
                                <?php
                    if (!empty($product['product'][0]['other_images']) && isset($product['product'][0]['other_images'])) {
                        foreach ($product['product'][0]['other_images'] as $other_image) {
                            if($other_image!=base_url().'/assets/no-image.png')
                            {
                                 $total_images++;
                    ?>
                                <a data-zoom-id="Zoom-1" href="<?= $other_image ?>?h=1400"
                                    data-image="<?= $other_image ?>?h=400"
                                    data-zoom-image-2x="<?= $other_image ?>?h=2800"
                                    data-image-2x="<?= $other_image ?>?h=800">
                                    <img srcset="<?= $other_image ?>" />
                                </a>
                                <?php } }
                    } ?>
                                <?php
$counting=$total_images;

                    $variant_images_md = array_column($product['product'][0]['variants'], 'images_md','id');
                    if (!empty($variant_images_md)) {
                       
                        foreach ($variant_images_md as $i_key =>$variant_images) {
                            if (!empty($variant_images)) {
                                $ik=0;
                                foreach ($variant_images as $image) {
									if($image!=base_url().'/assets/no-image.png')
									{
                    ?>
                                <a data-zoom-id="Zoom-1" href="<?= $image ?>?h=1400" data-image="<?= $image ?>?h=400"
                                    data-zoom-image-2x="<?= $image ?>?h=2800" data-image-2x="<?= $image ?>?h=800">
                                    <img id="img_<?php echo $i_key.'_'.$ik++;?>" srcset="<?= $image ?>" />
                                </a>
                                <?php }  }
                            }
							$counting++;
                        }
                    } ?>

                            </div>
                        </div>
                    </div>

                    <div class="product-details-info">

                        <div class="detail-info">
                            <h2 class="title-detail">
                                <?= $product_name ?>
                            </h2>
                            <input type="hidden" id="product_name_display" value="<?= $product['product'][0][$lang_prefix.'name'] ?>">
                            <div id="aa_row" class="a-row"><?=  $select_details ?></div>
                           
                            <div class="availability in-stock">
                                <?php
                                if($stockupdate==1){
                                  echo !empty($this->lang->line('out_of_stock')) ? $this->lang->line('out_of_stock') : 'Out of Stock' ;
                                }
                                else{
                                    echo !empty($this->lang->line('in_stock')) ? $this->lang->line('in_stock') : 'In Stock' ;
                                } ?>
                            </div>
                            <div class="details-info-middlemb-3">
                          <?= $price_display ?>
                            </div>


                            <form>
                               
                                <?php if (isset($product['product'][0]['variant_attributes']) && !empty($product['product'][0]['variant_attributes'])) { ?>
                                <?php foreach ($product['product'][0]['variant_attributes'] as $attribute) {
                    $attribute_values = explode(',', $attribute['values']);
                    $attribute_ids = explode(',', $attribute['ids']);
                    $ar_attribute_values = explode(',', $attribute['ar_values']);
                    // $attribute_set = $this->product_model->get_attribute_set($attribute['attr_name']); print_r($attribute_set);exit;
               $counting=$total_images+1;?>
                                <?php 
                                
                                if(strtolower($attribute['attr_name'])=='colour') { ?>
                                    <div class="product_variant color">
                                    <h2><?= !empty($this->lang->line('select')) ? $this->lang->line('select') : 'Select' ?>
                                        <?= $attribute[$lang_prefix.'attr_name'] ?>
                                </h2>
                                    <div class="custom-radios2">
                                        <?php $counting=$total_images+1; 
                                        foreach ($attribute_values as $key => $row) { 
                                            $attVal = ($lang_prefix =='') ? $row : $ar_attribute_values[$key];
                                            if(in_array($attribute_ids[$key],$d_variant_ids)){
                                            $d_checked =  'checked';
                                            }
                                            else{
                                            $d_checked ='';
                                            if(!in_array($attribute_ids[$key],$enabled_id)){
                                                $d_checked ='disabled';
                                            }
                                            }
                                            ?>
                                        <div>
                                            <input type="radio" class="attributes colorradio"
                                                data-step="<?= $counting ?>" id="color-<?= $attribute_ids[$key] ?>"
                                                name="<?= $attribute['attr_name'] ?>"
                                                data-name="<?= $attribute[$lang_prefix.'attr_name'] ?>" data-name-value=" <?= ($lang_prefix =='') ? $row : $ar_attribute_values[$key] ?>" 
                                                value="<?= $attribute_ids[$key] ?>" <?= $d_checked ?>>
                                            <label for="color-<?= $attribute_ids[$key] ?>"
                                                style="background-color: <?= $row ?>;">
                                                <span>
                                                </span>
                                            </label>
                                        </div>
                                        <?php $counting++; } ?>
                                    </div>
                                </div>
                             
                                
                                <?php } 
                                elseif($attribute['attr_name']=='RAM'){?>
                                 <div class="product_variant color">
                                    <h2><?= !empty($this->lang->line('select')) ? $this->lang->line('select') : 'Select' ?>
                                        <?= $attribute[$lang_prefix.'attr_name'] ?>
                                </h2>
                                    <div class="custom-radios customradio3">
                                        <?php $counting=$total_images+1; 
                                        foreach ($attribute_values as $key => $row) { 
                                            $attVal = ($lang_prefix =='') ? $row : $ar_attribute_values[$key];
                                            if(in_array($attribute_ids[$key],$d_variant_ids))
                                            {
                                                $d_checked =  'checked';
                                            }
                                            else{
                                                $d_checked ='';
                                                if(!in_array($attribute_ids[$key],$enabled_id)){
                                                    $d_checked ='disabled';
                                                }
                                            }?>
                                        <div>
                                            <input type="radio" class="attributes colorradio"
                                                data-step="<?= $counting ?>" id="color-<?= $attribute_ids[$key] ?>"
                                                name="<?= $attribute['attr_name'] ?>"  data-name="<?= $attribute[$lang_prefix.'attr_name'] ?>" data-name-value=" <?= ($lang_prefix =='') ? $row : $ar_attribute_values[$key] ?>"
                                                value="<?= $attribute_ids[$key] ?>"  <?=  $d_checked ?>>
                                            <label for="color-<?= $attribute_ids[$key] ?>"
                                                style="background-color: <?= $row ?>;">
                                                <span>
                                                <?= ($lang_prefix =='') ? $row : $ar_attribute_values[$key] ?>
                                                </span>
                                            </label>
                                        </div>
                                        <?php $counting++; } ?>
                                    </div>
                                </div>
                             
                                <?php }
                                else { ?>
                             
                             <div class="product_variant color">
                                    <h2><?= !empty($this->lang->line('select')) ? $this->lang->line('select') : 'Select' ?>
                                        <?= $attribute[$lang_prefix.'attr_name'] ?>
                                </h2>
                                    <div class="custom-radios customradio3">
                                        <?php $counting=$total_images+1; 
                                        foreach ($attribute_values as $key => $row) { 
                                            $attVal = ($lang_prefix =='') ? $row : $ar_attribute_values[$key];
                                            if(in_array($attribute_ids[$key],$d_variant_ids))
                                            $d_checked =  'checked';
                                            else{
                                            $d_checked ='';
                                            if(!in_array($attribute_ids[$key],$enabled_id)){
                                                $d_checked ='disabled';
                                            }
                                            }?>
                                        <div>
                                            <input type="radio" class="attributes colorradio"
                                                data-step="<?= $counting ?>" id="color-<?= $attribute_ids[$key] ?>"
                                                name="<?= $attribute['attr_name'] ?>"
                                                data-name="<?= $attribute[$lang_prefix.'attr_name'] ?>" data-name-value=" <?= ($lang_prefix =='') ? $row : $ar_attribute_values[$key] ?>" 
                                                value="<?= $attribute_ids[$key] ?>"  <?=  $d_checked ?>>
                                            <label for="color-<?= $attribute_ids[$key] ?>"
                                                style="background-color: <?= $row ?>;" >
                                                <span>
                                                <?= ($lang_prefix =='') ? $row : $ar_attribute_values[$key] ?>
                                                </span>
                                            </label>
                                        </div>
                                        <?php $counting++; } ?>
                                    </div>
                                </div>


                                <?php } ?>


                                <?php    }
            } ?>
                                <?php if (!empty($product['product'][0]['variants']) && isset($product['product'][0]['variants'])) {
                foreach ($product['product'][0]['variants'] as $variant) {
            ?>
                                <input type="hidden" class="variants" name="variants_ids"
                                    data-image-index="<?= $variant['id'] ?>" data-name=""
                                    value="<?= $variant['variant_ids'] ?>" data-id="<?= $variant['id'] ?>"
                                    data-price="<?= $variant['price'] ?>" data-sku="<?= $variant['sku'] ?>"
                                    data-special_price="<?= $variant['special_price'] ?>"
                                    data-stock="<?= $variant['stock'] ?>" />
                                <?php
                    $total_images += count($variant['images']);
                }
            }
           
            ?>

                                <div class="details-info-middle-actions">

                                    <div class="number-picker">
                                        <div class="info-qty">
                                            <a id="pqtyDown" class="qtydown" href="javascript:void(0);"><i class="fa fa-minus"></i></a>
                                            <span class="qty-val">1</span>
                                            <a id="pqtyUp" class="qtyup" href="javascript:void(0);"><i class="fa fa-plus"></i></a>
                                            <input type="hidden" name="qty" id="prd-qty" value="1">
                                        </div>
                                    </div>
                                    
                                    <button type="button" class="btn btn-primary btn-add-to-cart add_tocart" id="add_cart" data-product-id="<?= $product['product'][0]['id'] ?>" data-product-variant-id="<?= $variant_id ?>" <?php
                                        if($stockupdate==1) echo 'disabled' ;?> ><i class="fa fa-cart-arrow-down"></i>
                                        <?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'ADD TO CART' ?>
                                    </button>

                                    <div class="details-info-top-actions"><button id="add-fav" class="btn btn-wishlist add-to-fav-btn <?=  $is_fav ?>" data-product-variant-id="<?= $variant_id ?>" data-product-id="<?=  $product['product'][0]['id'] ?>"><i
                                                class="fa fa-heart "></i>
                                                <?= !empty($this->lang->line('wishlist')) ? $this->lang->line('wishlist') : 'WISHLIST' ?>
                                    </div>
                                </div>

                                <br><br>

                  

                                <div class="brief-description">

                                    <p>
                                        <?= $product['product'][0][$lang_prefix.'short_description'] ?>
                                    </p>
                                </div>

                                <!--End desktop View Area-->
                                <div class="details-info-bottom">
                                    <ul class="list-inline additional-info">
                                        <li class="sku"><label><?= !empty($this->lang->line('sku')) ? $this->lang->line('sku') : 'SKU' ?>:</label>
                                            <span id="sku"><?= $sku ?></span>
                                        </li>
                                        <li><label><?= !empty($this->lang->line('categories')) ? $this->lang->line('categories') : 'Categories' ?>:</label> <a
                                                href="<?= base_url('products/category/' . $catslug) ?>">
                                                <?php echo ($lang_prefix =='') ?  $catname : $ar_catname; ?>
                                            </a></li>
                                        <li class="brand"><label><?= !empty($this->lang->line('brand')) ? $this->lang->line('brand') : 'brand' ?>: </label> <a
                                                href="<?= base_url('products/brands/' . $brandslug) ?>">
                                                <?php echo ($lang_prefix =='') ?   $brand : $ar_brand; ?>
                                            </a></li>
                                        <li><label><?= !empty($this->lang->line('tags')) ? $this->lang->line('tags') : 'Tags' ?>:</label>
                                        <a href="#"> <?= implode(',',$product['product'][0][$lang_prefix.'tags']) ;?></a>
                                            <!-- <a href="#">Gadgets</a>,
                                                <a href="#">New Arrivals</a>,
                                                <a href="#">Gear</a>,
                                                <a href="#">Networking</a>,
                                                <a href="#">Notebook</a> -->
                                        </li>
                                    </ul>
                                    <div class="social-share"><label><?= !empty($this->lang->line('share')) ? $this->lang->line('share') : 'Share' ?>:</label>
                                        <ul class="list-inline social-links">
                                            <li>
                                                <a href="#" title="Facebook" target="_blank"><i
                                                        class="fab fa-facebook-f"></i></a>
                                            </li>
                                            <li><a href="#" title="Twitter" target="_blank"><i
                                                        class="fab fa-twitter"></i></a></li>
                                            <li>
                                                <a href="#" title="Linkedin" target="_blank"><i
                                                        class="fab fa-linkedin"></i></a>
                                            </li>
                                            <li><a href="#" title="Tumblr" target="_blank"><i
                                                        class="fab fa-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <!-- End Attr Info -->
                    </div>
                </div>



            </div>





        <div class="product-details-bottom">


            <div class="product-details-bottom-inner">
                <div class="product-details-tab clearfix">
                    <ul class="nav nav-tabs tabs">
                        <li class="nav-item"><a href="#description" data-toggle="tab" class="nav-link active">
                        <?= !empty($this->lang->line('description')) ? $this->lang->line('description') : 'Description' ?>
                            </a></li>
                        <?php
                              if($product['product'][0]['sizechart']!='') { 
                                  ?>
                        <li class="nav-item"><a href="#sizechart" data-toggle="tab" class="nav-link">
                        <?= !empty($this->lang->line('sizechart')) ? $this->lang->line('sizechart') : 'Sizechart' ?>
                            </a></li>
                        <?php } ?>
                        <?php
                              if($product['product'][0][$lang_prefix.'specification']!='') { 
                                  ?>
                        <li class="nav-item"><a href="#specification" data-toggle="tab" class="nav-link">
                        <?= !empty($this->lang->line('specification')) ? $this->lang->line('specification') : 'Specification' ?> 
                            </a></li>
                        <?php } ?>
                        <!--<li class="nav-item"><a href="#reviews" data-toggle="tab" class="nav-link">
                        Reviews (0)
                    </a></li>-->
                    </ul>
                    <div class="tab-content">
                        <div id="description" class="tab-pane description active">
                            <p>
                                <?= $product['product'][0][$lang_prefix.'description'] ?>
                            </p>
                        </div>
                        <?php if($product['product'][0][$lang_prefix.'specification']!='') { ?>
                        <div id="specification" class="tab-pane specification">
                            <p>
                                <?= $product['product'][0][$lang_prefix.'specification'] ?>
                            </p>
                        </div>
                        <?php } ?>
                        <?php if($product['product'][0]['sizechart']!='') { ?>
                        <div id="sizechart" class="tab-pane sizechart">
                            <div class="specification-inner">

                                <h4> <?= !empty($this->lang->line('sizechart')) ? $this->lang->line('sizechart') : 'Sizechart' ?></h4>
                                <img class="img-responsive"
                                    src="<?php echo base_url().$product['product'][0]['sizechart'];?>">

                            </div>
                        </div>
                        <?php } ?>

                    </div>
                </div>
            </div>

        </div>

<?php if($related_products['product']){?>
        <div class="content-popular12 mb-3">
            <div class="popular-cat-title popular-cat-label">
                <label> <?= !empty($this->lang->line('related_products')) ? $this->lang->line('related_products') : 'Related Products' ?></label>

            </div>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="best1">
                    <div class="popular-cat-slider popular-cat-slider12 slider-home5">
                        <div class="wrap-item">

                        <?php foreach ($related_products['product'] as $each) { 
                             if (count($each['variants']) >= 1 ) {
                                $product_name = $each[$lang_prefix.'name'];
                                $product_slug =  '';
                                $main_image =$each['image_sm'];
                                $product_id = $each['id'];
                                $m = $lang_prefix.'variant_values'; 
                               // $is_favorite = $each['is_favorite'];
                                $product_stock = $each['stock'];
                             //   print_r($each['variants']);
                                foreach($each['variants'] as $v_each){
                                    $product_slug =  base_url('products/details/' . $each['slug']);
                                    if($v_each['special_price'] > 0 && $v_each['special_price'] < $v_each['price']){
                                        $discount_in_percentage = find_discount_in_percentage($v_each['special_price'], $v_each['price']);
                                    }
                                    else{
                                        $discount_in_percentage = 0;
                                    }
                                    if($each['type'] =='variable_product'){
                                        if($each['stock_type']==NULL){
                                            if($product_stock!=null && intval($product_stock) <=0)
                                            $stockupdate=1;
                                           
                                           else{
                                               $stockupdate=0;
                                           }
                                        }
                                        elseif($each['stock_type']=='2'){
                                            if($v_each['stock']!=NULL && intval($v_each['stock']) <=0){
                                                $stockupdate=1;
                                            }
                                            else{
                                                $stockupdate=0;
                                            }
                                        }
                                        
                                        
                                    }
                                    else
                                    {
                                        if($each['stock_type'] ==NULL){
                                      
                                            $stockupdate=0;
                                        }
                                        else{
                                            if($each['stock']!=NULL && intval($each['stock']) <=0){
                                                $stockupdate=1;
                                            }
                                            else{
                                                $stockupdate=0;
                                            }
                                        }
                                    } 
                                   
                                    $image = $v_each['images'];
                                    if(isset($image[0]) && $image[0] !=''){                                           
                                       $sm_image = $image[0];
                                    }
                                    else{
                                        $sm_image =$main_image;
                                    }
                                    $variant_id =$v_each['id'] ;
                                    $modal = "";
                                    $class="add-to-cart";
                                    if($v_each['is_favorite'] >0)
                                        $is_favorite =1;
                                    else
                                    $is_favorite =0;
                                    $product_slug .='/'.$variant_id;
                                    ?>

                                    <div class="item">
                                    <div class="item-hot-deal-product">
                                      <div class="hot-deal-product-thumb">
                                         
                                  <?php if($stockupdate==1) { ?>
                                                               <div class="outofstock">
                                                   <strong> <?= !empty($this->lang->line('out_of_stock')) ? $this->lang->line('out_of_stock') : 'Out of Stock' ?></strong>
                                               </div>
                                  <?php }
                                 if($discount_in_percentage!='' && $discount_in_percentage>0 && $discount_in_percentage < 100){
                                      ?>
                                      <div class="cat-hover-percent">
                                                                   <strong><?= $discount_in_percentage ?>% </strong>
                                                                   <!--<span>+10% for Member</span>-->
                                                               </div>
                                     
                                      <?php
                                  } ?>
                                  <!-- <div class="cat-hover-percent">
                                               <strong>10%</strong>
                                              
                                           </div> -->
                                           <div class="product-thumb">
                                               <a class="product-thumb-link" href="<?= $product_slug ?>">
                                                   <img alt="" src="<?= $sm_image ?>"
                                                       class="first-thumb">
       
                                               </a>
                                               <div class="product2-buttons">
                                                   <a href="<?= base_url('products/product_quickview/'.$product_id.'/'.$variant_id) ?>"
                                                       class="product-button hintT-top manual-ajax"
                                                       rel="modal:open" data-hint="<?= !empty($this->lang->line('quick_view')) ? $this->lang->line('quick_view') : 'Quick View' ?>"><i
                                                           class="fa fa-search"></i></a>
                                                           
                                                           <a href="#"  data-hint="<?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'Add to Cart' ?>" class="<?php echo $class; ?> product-button hintT-top" data-product-id="<?= $product_id ?>"  data-product-qty="1" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>"  ><i class="fa fa-shopping-cart"></i></a>
                                                    
                                                   
                                                   <a href="javascript:void(0);"  data-product-id="<?=  $product_id ?>" data-product-variant-id="<?= $variant_id ?>"  data-hint="<?= !empty($this->lang->line('add_to_wishlist')) ? $this->lang->line('add_to_wishlist') : 'Add to Wishlist' ?>" class="product-button hintT-top wishlist add-to-fav-btn lnr " ><i class="fa fa-heart <?= ( $is_favorite == 1) ? ' text-danger' : '' ?>"></i></a>
                                                 
                                               </div>
                                           </div>
                                       </div>
                                       <div class="hot-deal-product-info">
                                           <h3 class="title-product"><a href="<?= $product_slug ?>"><?= $product_name ?> <?php echo $v_each[$m]; ?></a></h3>
                                           <div class="info-price">
                                         
                                        
                                                       <span><?php 
                                                     
                                                           if($v_each['special_price'] > 0 && $v_each['special_price']< $v_each['price']){?>
                                                               <span ><?= $settings['currency'] ?> <?= number_format($v_each['special_price'],3) ?></span><del><?= $settings['currency'] ?> <?= number_format($v_each['price'],3) ?></del>
   
                                                           <?php }
                                                           else{
                                                               ?>
                                                                   <span ><?= $settings['currency'] ?> <?= number_format($v_each['price'],3) ?></span>	 
                                                          <?php } ?>
                                                      </span>
                                                      
                                           </div>
                                       </div>
                                   </div>
                               </div>

                              <?php  }

                            }
                            ?>
                        
                        
                        
                        
                        
                     <?php   } ?>
                            <!-- End Item -->
                         
                        </div>
                    </div>
                </div>


            </div>
        </div>

<?php }?>


</div>



</section>

</div>