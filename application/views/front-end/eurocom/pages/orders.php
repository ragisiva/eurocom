<div class="container">

    <div class="page_name">
        <div class="row">
            <div class="col-md-6 ">
                <h3> <?= !empty($this->lang->line('my_order')) ? $this->lang->line('my_order') : 'My Order' ?></h3>
            </div>
            <div class="col-md-6 ">
                <div class="breadcrumb">
                    <ul class="list-inline">
                        <li><a href="<?= base_url() ?>"> <?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?></a></li>

                        <li class="active" ><?= !empty($this->lang->line('my_order')) ? $this->lang->line('my_order') : 'My Order' ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



</div>



<div id="content">
    <section class="account-wrap">
        <div class="container">
            <div class="account-wrap-inner">

                <div class="account-left">
                    <?php $this->load->view('front-end/' . THEME . '/pages/my-account-sidebar') ?>

                </div>


                <div class="account-right">
                    <div class="panel-wrap">
                        <div class="panel">
                            <div class="panel-header">
                                <h4><?= !empty($this->lang->line('my_order')) ? $this->lang->line('my_order') : 'My Order' ?></h4>

                            </div>
                            <div class="panel-body">
                                <div class="order-details-middle mt-3">

                                    <div class="table-responsive">
                                        <table class="table table-borderless order-details-table">
                                            <thead>
                                                <tr>

                                                    <th><?= !empty($this->lang->line('order_id')) ? $this->lang->line('order_id') : 'Order ID' ?></th>
                                                    <th><?= !empty($this->lang->line('date')) ? $this->lang->line('date') : 'Date' ?></th>
                                                    <th><?= !empty($this->lang->line('status')) ? $this->lang->line('status') : 'Status' ?></th>

                                                    <th><?= !empty($this->lang->line('total')) ? $this->lang->line('total') : 'Total' ?></th>
                                                    <th><?= !empty($this->lang->line('action')) ? $this->lang->line('action') : 'Action' ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <?php foreach ($orders['order_data'] as $row) { ?>
                                                    <td><label><?= !empty($this->lang->line('order_id')) ? $this->lang->line('order_id') : 'Order ID' ?></label> <span class="product-price">
                                                            <?= $row['id'] ?>
                                                        </span></td>

                                                    <td><label><?= !empty($this->lang->line('date')) ? $this->lang->line('date') : 'Date' ?></label> <span class="product-price">
                                                            <?= date('M d, Y',strtotime($row['date_added'])); ?>
                                                        </span></td>

                                                    <td><label><?= !empty($this->lang->line('status')) ? $this->lang->line('status') : 'Status' ?></label><span class="badge badge-info">
                                                    <?= ucfirst(str_replace('_',' ',$row['active_status'])); ?>
                                                        </span>
                                                        

                                                    </td>
                                                  
                                                    <td><label><?= !empty($this->lang->line('total')) ? $this->lang->line('total') : 'Total' ?></label> <span class="product-price">
                                                            <?= $settings['currency'] ?>
                                                            <?= number_format($row['final_total'], 3) ?>
                                                        </span></td>

                                                    <td><label> <?= !empty($this->lang->line('status')) ? $this->lang->line('action') : 'action' ?></label> <span class="product-price">
                                                            <a href="<?= base_url('my-account/order-details/' . $row['id']) ?>"
                                                            class="btn btn-view btn-info btn-sm"><i class="fas fa-eye"></i>
                                                            <?= !empty($this->lang->line('view')) ? $this->lang->line('view') : 'View' ?>
                                                            </a>&nbsp;&nbsp;
                                                            <br />
                                                            <?php if ($row['is_cancelable'] && !$row['is_already_cancelled'] && ($row['active_status']=='received' || $row['active_status']=='processed')) { ?><a class="btn btn-warning btn-sm"
                                                                href="javascript:void(0);" class="update-order view"
                                                                data-status="cancelled"
                                                                data-order-id="<?= $row['id'] ?>">  <?= !empty($this->lang->line('cancel')) ? $this->lang->line('cancel') : 'Cancel' ?></a>
                                                            <?php } ?>
                                                            <?php if ($row['is_returnable'] && !$row['is_already_returned'] && $row['active_status']=='delivered' ) { ?>
                                                                <!-- <a
                                                                href="javascript:void(0);" class="update-order view"
                                                                data-status="returned"
                                                                data-order-id="<?= $row['id'] ?>">Return</a>  -->
                                                            <?php } ?>
                                                        </span></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <div class="text-center">
                                            <?= $links ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>



                </div>

    </section>


</div>