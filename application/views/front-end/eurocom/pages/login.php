<div class="container">
		
		<div class="page_name">
			<div class="row">
		 <div class="col-md-6 ">	
		<h3><?= !empty($this->lang->line('login')) ? $this->lang->line('login') : 'Login' ?></h3>
			</div>
			 <div class="col-md-6 ">	
        <div class="breadcrumb">			
            <ul class="list-inline">
                <li><a href="<?= base_url() ?>"><?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?></a></li>

                <li class="active"><?= !empty($this->lang->line('login')) ? $this->lang->line('login') : 'Login' ?></li>
            </ul>
        </div>
		</div>		 
		</div>
			</div>
		
		
		
    </div>
    <div id="content">
        <section class="form-wrap login-wrap">
            <div class="container">
                <div class="form-wrap-inner login-wrap-inner">
                  
                    <form id="form1" name="form1" method="POST" action="<?= base_url('home/login') ?>" class="form-submit-event">
                    <div id="infoMessage"><?php echo isset($message) ? $message : '';?></div>
                    <div class="d-flex justify-content-center">
                <div class="form-group" id="error_box"></div>
            </div>
                        <div class="form-group"><label for="email"><?= !empty($this->lang->line('email')) ? $this->lang->line('email') : 'Email' ?><span>*</span></label> <input type="text"  value="" id="email" class="form-control" name="identity" required></div>
                        <div class="form-group"><label for="password"><?= !empty($this->lang->line('password')) ? $this->lang->line('password') : 'Password' ?><span>*</span></label> <input type="password" name="password" id="password" class="form-control" required></div>
                        <div class="form-check remember-me"><input type="hidden" name="remember_me" value="0"> 
                        <!-- <input type="checkbox" name="remember_me" value="1" id="remember"> <label for="remember" class="form-check-label">Remember me</label> -->
                    </div>
                        <a href="<?= base_url('forgot-password') ?>" class="forgot-password">
                        <?= !empty($this->lang->line('forgot_password')) ? $this->lang->line('forgot_password') : 'Password' ?>?
                </a> <button type="submit" data-loading="" class="btn btn-primary btn-sign-in submit_btn">
                <?= !empty($this->lang->line('sign_in')) ? $this->lang->line('sign_in') : 'SIGN IN' ?> 
                </button></form> 
                <!-- <span class="sign-in-with">
                Or, Continue With
        </span>
                    <ul class="list-inline social-login">
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Google" class="google"><i class="fab fa-google"></i></a></li>
                    </ul>  -->
                    <span class="have-an-account">
                    <?= !empty($this->lang->line('dont_have_account')) ? $this->lang->line('dont_have_account') : 'Don\'t Have an Account?' ?> 
            </span> <a href="<?= base_url('register') ?>" class="btn btn-default btn-create-account">
            <?= !empty($this->lang->line('create_account')) ? $this->lang->line('create_account') : 'CREATE ACCOUNT' ?>
            </a></div>

            </div>

        </section>


    </div>
