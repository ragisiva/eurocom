<div class="container">
		
		<div class="page_name">
			<div class="row">
		 <div class="col-md-6 ">	
		<h3><?= !empty($this->lang->line('hot_deals')) ? $this->lang->line('hot_deals') : 'Hot Deals' ?></h3>
			</div>
			 <div class="col-md-6 ">	
        <div class="breadcrumb">			
            <ul class="list-inline">
                <li><a href="<?= base_url() ?>"><?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?></a></li>

                <li class="active"><?= !empty($this->lang->line('hot_deals')) ? $this->lang->line('hot_deals') : 'Hot Deals' ?></li>
            </ul>
        </div>
		</div>		 
		</div>
			</div>
		
		
		
    </div>

<!-- Swiper -->
<div id="content">
        <section class="form-wrap login-wrap">
            <div class="container">
                
				
      
            <div role="tabpanel" class="tab-pane fade in active"  id="product-list">

                                        <div class="list-view-products row">

                                        <?php if (isset($products) && !empty($products['product'])) { ?>
					                    <?php foreach ($products['product'] as $each) {  
                                                 $stockupdate=0;
                                                 $product_name = $each[$lang_prefix.'name'];
                                                 $product_slug =  '';
                                                 $main_image =$each['image'];
                                                 $product_id = $each['id'];
                                                 $m = $lang_prefix.'variant_values'; 
                                               
                                                 $product_stock = $each['stock'];
                                         
                                                     $product_slug =  base_url('products/details/' . $each['slug']);
                                                     if($each['special_price'] > 0 && $each['special_price'] < $each['price']){
                                                         $discount_in_percentage = find_discount_in_percentage($each['special_price'], $each['price']);
                                                     }
                                                     else{
                                                         $discount_in_percentage = 0;
                                                     }
                                                     if($each['type'] =='variable_product'){
                                                         if($each['stock_type']==NULL){
                                                             if($product_stock!=null && intval($product_stock) <=0)
                                                             $stockupdate=1;
                                                            
                                                            else{
                                                                $stockupdate=0;
                                                            }
                                                         }
                                                         elseif($each['stock_type']=='2'){
                                                             if($each['variant_stock']!=NULL && intval($each['variant_stock']) <=0){
                                                                 $stockupdate=1;
                                                             }
                                                             else{
                                                                 $stockupdate=0;
                                                             }
                                                         }
                                                         
                                                         
                                                     }
                                                     else
                                                     {
                                                         if($each['stock_type'] ==NULL){
                                                       
                                                             $stockupdate=0;
                                                         }
                                                         else{
                                                             if($each['stock']!=NULL && intval($each['stock']) <=0){
                                                                 $stockupdate=1;
                                                             }
                                                             else{
                                                                 $stockupdate=0;
                                                             }
                                                         }
                                                     } 
                                                    
                                                     $image = json_decode($each['variant_images']);
                                                     if(isset($image[0]) && $image[0] !=''){                                           
                                                        $sm_image = get_image_url($image[0],'thumb', 'sm');
                                                     }
                                                     else{
                                                         $sm_image =get_image_url($main_image,'thumb', 'sm');
                                                     }
                                                     $variant_id =$each['variant_id'] ;
                                                     $modal = "";
                                                     $class="add-to-cart";
                                                     if ($is_logged_in) {
                                                         $is_favorite = is_exist(['user_id' => $user->id,
                                                         'product_id' => $product_id,
                                                         'product_variant_id' => $variant_id],'favorites');
                                                         if($is_favorite)
                                                             $is_favorite =1;
                                                         else
                                                         $is_favorite =0;
                                                         }
                                                         else
                                                         $is_favorite =0;
                                                     $product_slug .='/'.$variant_id;
                                                          ?>

                                            <div class="col-md-4 col-sm-4 col-xs-12 offer-item-product">
                                                <div class="item-deal-product ">
                                                <?php if($stockupdate==1) { ?>
                                                            <div class="outofstock">
                                                <strong> <?= !empty($this->lang->line('out_of_stock')) ? $this->lang->line('out_of_stock') : 'Out of Stock' ?></strong>
                                            </div>
							   <?php }
                               if($discount_in_percentage!='' && $discount_in_percentage>0 && $discount_in_percentage < 100){
                                   ?>
                                   <div class="cat-hover-percent">
                                                                <strong><?= $discount_in_percentage ?>% </strong>
                                                                <!--<span>+10% for Member</span>-->
                                                            </div>
                                  
                                   <?php
                               } ?>
                                                    <div class="product-thumb">
                                                        <a href="<?= $product_slug ?>" class="product-thumb-link"> <img alt="<?= $product_name ?>" src="<?= $sm_image ?>" class="first-thumb">
                                                     
                                                        </a>
                                                    </div>
                                                    <div class="product-info">
                                                    <h3 class="title-product"> <a href="<?= $product_slug ?>" class="product-name">
                                                            <?= $product_name ?> <?php echo $each[$m]; ?>
                                                        </a></h3>
                                                        <div class="clearfix"></div>
                                                        <div class="info-price"><span>
                                                        <?php 
                                                         
                                                         if($each['special_price'] > 0 && $each['special_price']< $each['price']){?>
                                                            <?= $settings['currency'] ?> <?= number_format($each['special_price'],3) ?><span class="previous-price"><?= $settings['currency'] ?> <?= number_format($each['price'],3) ?></span>
 
                                                         <?php }
                                                         else{
                                                             ?>
                                                               <?= $settings['currency'] ?> <?= number_format($each['price'],3) ?> 
                                                        <?php } ?>
                                                     </span> </div>
                                                     
                                                        <a href="#"  data-hint="<?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'Add to Cart' ?>" class="<?php echo $class; ?> product-button hintT-top btn btn-primary btn-add-to-cart" data-product-id="<?=  $product_id ?>"  data-product-qty="1" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>" title="add to cart" ><i class="fa fa-shopping-cart"></i> <?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'Add to Cart' ?></a>
                                                 
                                                     <br>
                                                                    
                                                                    <div class="product-card-actions">
                                                                    <button  data-product-id="<?=  $product_id ?>" data-product-variant-id="<?= $variant_id ?>"  class="btn  btn-default btn-wishlist wishlist add-to-fav-btn"><i class="fa fa-heart  <?= ( $is_favorite == 1) ? ' text-danger' : '' ?>"></i>
                                                                    <?= !empty($this->lang->line('wishlist')) ? $this->lang->line('wishlist') : 'Wishlist' ?>
                                                    </button>


                                                      
                                                     
                                                    <a href="<?= base_url('products/product_quickview/'.$product_id.'/'.$variant_id) ?>"
                                                    class="btn btn-default  product-button hintT-top manual-ajax"
                                                    rel="modal:open" title="<?= !empty($this->lang->line('quick_view')) ? $this->lang->line('quick_view') : 'Quick View' ?>"><i
                                                        class="fa fa-search"></i> <?= !empty($this->lang->line('quick_view')) ? $this->lang->line('quick_view') : 'Quick View' ?></a>

                                                


                                                        </div>
                                                    </div>
                                                </div>






                                            </div>
                                            <?php }}?>


                                       


                                        </div>
                                        <!--list-card-main-->

                                        <nav class="text-center mt-4 ">
                                        <?php
                       // (isset($links)) ? $links : '';
                         if(isset($products['product']) && count($products['product']) < $total_count) {?>
              <h1 class="load-more-grid load-more-offer-grid"><?= $this->lang->line('load_more') ?></h1>
            <input type="hidden" id="row-grid" value="<?php echo count($products['product']); ?>">
            <input type="hidden" id="all-grid" value="<?php echo $total_count; ?>">
				<?php } ?>		
                                        </nav>

            
      
    </div>
        </section>
    </div>
   