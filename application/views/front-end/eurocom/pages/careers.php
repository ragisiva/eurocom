<div id="content">

<section class="custom-page-wrap clearfix">
    <div class="container">
        <div class="custom-page-content clearfix">

    <div class="text-center">
        <h1><?=!empty($this->lang->line('careers')) ? $this->lang->line('careers') : 'Careers'?></h1>
      
    </div>
    <div class="contact-form-wrap careers-filed">

<div class="contact-form-inner">

    <div class="contact-form-right">
        <h3 class="title"><?=!empty($this->lang->line('join_our_team')) ? $this->lang->line('join_our_team') : 'JOIN OUR TEAM'?></h3>
        <div class="contact-form">
            <form method="POST" action="#">
                <input type="hidden" name="_token" value="qlXELnUdxuUlNtbJel2FT2RdtA8BSEb4k0MS9ZSw">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">
                            <?=!empty($this->lang->line('name')) ? $this->lang->line('name') : 'Name'?><span>*</span>
</label>
                            <input type="text" name="name" value="" id="name" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">
                            <?=!empty($this->lang->line('email')) ? $this->lang->line('email') : 'Email' ?><span>*</span>
</label>
                            <input type="text" name="email" value="" id="email" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="subject">
                            <?=!empty($this->lang->line('phone')) ? $this->lang->line('phone') : 'Phone'?><span>*</span>
</label>
                            <input type="text" name="subject" value="" id="subject" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="subject">
                            <?=!empty($this->lang->line('position_applied_for')) ? $this->lang->line('position_applied_for') : 'Position Applied For'?>  <span>*</span>
</label>
                            <input type="text" name="subject" value="" id="subject" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="message">
                            <?=!empty($this->lang->line('upload_resume')) ? $this->lang->line('upload_resume') : 'Upload Resume' ?>  <span>*</span>
</label>
                            <input type="file" name="subject" value="" id="subject" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group p-t-5">
                            <img src="https://fleetcart.envaysoft.com/captcha/image?307823714" style="cursor:pointer;width:180px;height:50px;" title="Update Code" onclick="this.setAttribute('src','https://fleetcart.envaysoft.com/captcha/image?307823714?_='+Math.random());var captcha=document.getElementById('captcha');if(captcha){captcha.focus()}">
                            <input type="text" name="captcha" class="captcha-input" placeholder="Enter captcha code">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-lg btn-primary" data-loading>
                        <?=!empty($this->lang->line('send_message')) ? $this->lang->line('send_message') : 'Send Message' ?>
</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
        </div>
    </section>
 </div>