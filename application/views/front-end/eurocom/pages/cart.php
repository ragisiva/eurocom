<div class="container">
		
		<div class="page_name">
			<div class="row">
		 <div class="col-md-6 ">	
		<h3><?= !empty($this->lang->line('cart')) ? $this->lang->line('cart') : 'Cart' ?></h3>
			</div>
			 <div class="col-md-6 ">	
        <div class="breadcrumb">			
        <ul class="list-inline">
                <li><a href="<?= base_url() ?>"><?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?></a></li>

                <li class="active"><?= !empty($this->lang->line('cart')) ? $this->lang->line('cart') : 'Cart' ?></li>
            </ul>
        </div>
		</div>		 
		</div>
			</div>
		
		
		
    </div>	
  

    <div id="content">
        <section class="shopping-cart-wrap">
            <div class="container">
                <div class="shopping-cart">
                <?php
                        if($cart['sub_total'] >0){?>
                    <div class="shopping-cart-inner">
                        
                        <div class="table-responsive">
                            <table class="table table-borderless shopping-cart-table">
                                <thead>
                                    <tr>
                                        <th><?= !empty($this->lang->line('image')) ? $this->lang->line('image') : 'Image' ?></th>
                                        <th><?= !empty($this->lang->line('product')) ? $this->lang->line('product') : 'Product' ?></th>
                                        <th><?= !empty($this->lang->line('price')) ? $this->lang->line('price') : 'Price' ?></th>
                                        <th><?= !empty($this->lang->line('quantity')) ? $this->lang->line('quantity') : 'Quantity' ?></th>
                                        <th><?= !empty($this->lang->line('subtotal')) ? $this->lang->line('subtotal') : 'Subtotal' ?></th>
                                        <th><button class="btn-remove"><i class="las la-times"></i></button></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                
                              foreach ($cart as $key => $row) {
                                if (isset($row['qty']) && $row['qty'] != 0) {
                                ?>
                                <tr id="cart_<?= $row['product_variant_id'] ?>" class="cart_item1 cart_<?= $row['product_variant_id'] ?>">
                                        <td>
                                            <div class="product-image"><a href="#">
                                                    <img src="<?= $row['image'] ?>" alt="">
                                                </a></div>
                                        </td>
                                        
                                        <td>
                                            <div class="id">
                                                <input type="hidden" name="<?= 'id[' . $key . ']' ?>" id="id" value="<?= $row['id'] ?>">
                                            </div>
                                            
                                            <a href="#" class="product-name"><?= $row[$lang_prefix.'name']; ?>
                                            <?php if (!empty($row['product_variants'])) { ?>
                                                    <?= str_replace(',', '   ', $row['product_variants'][0][$lang_prefix.'variant_values']) ?>
                                                <?php } ?>
                                                </a>
                                        </td>
                                        <td ><label><?= !empty($this->lang->line('price')) ? $this->lang->line('price') : 'Price' ?>:</label> 
                                       <?php 
                                       if($row['special_price'] >0) 
                                       $qprice = $row['special_price'];
                                       else
                                       $qprice =$row['price'];
                                       if(number_format($row['special_price'],2) >0) 
                                                    echo '<del class="old-price">'.$settings['currency'] . ' ' . number_format($row['price'], 2).'</del><span class="product-price" >'. $settings['currency'] . ' ' . number_format($row['special_price'], 2).'</span>';
                                                    else
                                                    echo '<span class="product-price price_cart">'. $settings['currency'] . ' ' . number_format($row['price'], 2).'</span>';
                                                    ?></td>
                                        <td class="item-quantity"><label><?= !empty($this->lang->line('quantity')) ? $this->lang->line('quantity') : 'Quantity' ?>:</label>
                                            <div class="number-picker num-block skin-2 product-quantity">
                                                <div class="info-qty  num-in">
                                                    <a class="qty-down minus dis" href="#" ><i class="fa fa-minus"></i></a>
                                                   <span class="qty-val"><?= $row['qty'] ?></span>
                                                 
                                                    <a class="qty-up plus" href="#" ><i class="fa fa-plus"></i></a>
                                                    <input type="hidden" name="qty" value="<?= $row['qty'] ?>" data-page="cart" data-id="<?= $row['product_variant_id']; ?>" data-price="<?= $qprice; ?>" data-step="<?= (isset($row['minimum_order_quantity']) && !empty($row['quantity_step_size'])) ? $row['quantity_step_size'] : 1 ?>" data-min="<?= (isset($row['minimum_order_quantity']) && !empty($row['minimum_order_quantity'])) ? $row['minimum_order_quantity'] : 1 ?>" data-max="<?= (isset($row['total_allowed_quantity']) && !empty($row['total_allowed_quantity'])) ? $row['total_allowed_quantity'] : '' ?>">
                                                </div>
                                            </div>
                                        </td>
                                        <td class="total-price cart_info1">
                                            <label><?= !empty($this->lang->line('subtotal')) ? $this->lang->line('subtotal') : 'Subtotal' ?>:</label> <span class="product-price price_cart1" data-price=" <?php if($row['special_price'] >0) 
                                                    echo ($row['qty'] * $row['special_price']);
                                                    else
                                                    echo ($row['qty'] * $row['price']);
                                                    ?>">
                                        <?php if(number_format($row['special_price'],2) >0) 
                                                    echo  $settings['currency'] . ' ' . number_format(($row['qty'] * $row['special_price']), 2);
                                                    else
                                                    echo $settings['currency'] . ' ' . number_format(($row['qty'] * $row['price']), 2);
                                                    ?>
                                       </span></td>
                                        <td class="cart_remove"><button class="btn-remove product-removal remove-product" data-id="<?= $row['product_variant_id']; ?>"><i class="fa fa-times  fas fa-window-close text-danger" name="remove_inventory"  id="remove_inventory"  title="<?= !empty($this->lang->line('remove_from_cart')) ? $this->lang->line('remove_from_cart') : 'Remove From Cart' ?>"></i></button></td>
                                    </tr>
                                    <?php }
                            } ?>
                                   
                                </tbody>
                            </table>
                        </div>
                        <?php if (!empty($save_for_later['variant_id'])) { ?>
                    <div class="cart-table-wrapper">
                        <table class="table table-responsive-sm table-cart-product">
                            <h4 class="h4"><?= !empty($this->lang->line('save_for_later')) ? $this->lang->line('save_for_later') : 'Save For Later' ?></h1>
                                <thead>
                                    <tr >
                                        <th class="text-muted"><?= !empty($this->lang->line('image')) ? $this->lang->line('image') : 'Image' ?></th>
                                        <th class="text-muted"><?= !empty($this->lang->line('product')) ? $this->lang->line('product') : 'Product' ?></th>
                                        <th class="text-muted"><?= !empty($this->lang->line('price')) ? $this->lang->line('price') : 'Price' ?></th>
                                        <th class="text-muted"><?= !empty($this->lang->line('quantity')) ? $this->lang->line('quantity') : 'Quantity' ?></th>
                                        <th class="text-muted"><?= !empty($this->lang->line('quantity')) ? $this->lang->line('quantity') : 'Subtotal' ?></th>
                                        <th class="text-muted"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($save_for_later as $key => $row) {
                                        if (isset($row['qty']) && $row['qty'] != 0) {
                                    ?>
                                            <tr class="cart-product-desc-list">
                                                <td>
                                                    <div class="cart-product-image">
                                                        <a href="<?= $row['slug']; ?>">
                                                            <img src="<?= $row['image'] ?>" alt="">
                                                        </a>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="id">
                                                        <input type="hidden" name="<?= 'id[' . $key . ']' ?>" id="id" value="<?= $row['id'] ?>">
                                                    </div>
                                                    <h2 class="cart-product-title">
                                                        <a href="<?= $row['slug']; ?>"><?= $row['name']; ?></a>
                                                        <?php if (!empty($row['product_variants'])) { ?>
                                                            <br><?= str_replace(',', ' | ', $row['product_variants'][0]['variant_values']) ?>
                                                        <?php } ?>
                                                        <br><button class="btn remove-product button button-warning move-to-cart button-sm" data-id="<?= $row['id']; ?>"><?= !empty($this->lang->line('move_to_cart')) ? $this->lang->line('move_to_cart') : 'Move to cart' ?></button>
                                                    </h2>
                                                </td>
                                                <td class="text-muted p-0"><?php
                                                if(number_format($row['special_price'],2) >0) 
                                                    echo '<span class="old-price">'.$settings['currency'] . ' ' . number_format($row['price'], 2).'</span><span class="product-price">'. $settings['currency'] . ' ' . number_format($row['special_price'], 2).'</span>';
                                                    else
                                                    echo '<span class="product-price">'. $settings['currency'] . ' ' . number_format($row['price'], 2).'</span>';
                                                    ?></td>
                                                <td class="itemQty">
                                                    <?= $row['qty'] ?>
                                                </td>
                                                <td class="text-muted p-0"><?= $settings['currency'] . ' ' . number_format($row['special_price'], 2) ?></td>
                                                <td>

                                                    <div class="product-removal">
                                                        <i class="remove-product fas fa-trash-alt text-danger" name="remove_inventory" id="remove_inventory" data-id="<?= $row['id']; ?>"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                    <?php }
                                    } ?>
                                </tbody>
                        </table>
                    </div>
                <?php } ?>
                        <!-- <div class="coupon-wrap">
                            <form>
                                <div class="form-group">
                                    <div class="form-input"><input type="text" placeholder="Enter coupon code" class="form-control">
                                  
                                    </div> <button type="submit" class="btn btn-primary btn-apply-coupon">
                                    <?= !empty($this->lang->line('apply_coupon')) ? $this->lang->line('apply_coupon') : 'APPLY COUPON' ?>
            </button></div>
                            </form>
                          
                        </div> -->
                    </div>


                    <aside class="order-summary-wrap">
                        <div class="order-summary">
                            <div class="order-summary-top">
                                <h3 class="section-title"><?= !empty($this->lang->line('order_summary')) ? $this->lang->line('order_summary') : 'Order Summary' ?></h3>
                            </div>
                            <div class="order-summary-middle">
                                <ul class="list-inline order-summary-list">
                                    <li><label><?= !empty($this->lang->line('subtotal')) ? $this->lang->line('subtotal') : 'Subtotal' ?></label> <span class="price-amount"><?= $settings['currency'];?> <span class="cart-subtotal1"><?= number_format($cart['sub_total'], 2) ?></span></span></li>
                                    <li><label><?= !empty($this->lang->line('delivery_charges')) ? $this->lang->line('delivery_charges') : 'Delivery Charges' ?></label> <span class="price-amount"><?= $settings['currency'] ?> <span  id="cart-shipping1"><?= number_format($cart['delivery_charge'], 3) ?></span></span></li>
                                    <!---->
                                </ul>
                                
                                <div class="order-summary-total"><label><?= !empty($this->lang->line('grand_total')) ? $this->lang->line('grand_total') : 'Grand Total' ?></label> <span class="total-price"><?= $settings['currency'] ?> <span id="cart-total1"><?= number_format($cart['amount_inclusive_tax'], 2) ?></span></span></div>
                            </div>
                            <div class="order-summary-bottom"><a href="<?= base_url('cart/checkout') ?>" id="checkout"  class="btn btn-primary btn-proceed-to-checkout">
                           <?= !empty($this->lang->line('proceed_to_checkout')) ? $this->lang->line('proceed_to_checkout') : 'PROCEED TO CHECKOUT' ?>
                    </a></div>
                        </div>
                    </aside>

<?php } 
else{
    echo '<h4>'. !empty($this->lang->line('empty_cart_message')) ? $this->lang->line('empty_cart_message') : 'Your cart is empty!!'.' </h4>';
}
?>
                </div>

            </div>

        </section>


    </div>
