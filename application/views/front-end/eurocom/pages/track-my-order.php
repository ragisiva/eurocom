 <div class="breadcrumbs_area"  >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content pading_big" >

       
                   
						<h2>Track My Order</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="product_details">
        <div class="container">
		<div class="row">
		  <!--login area start-->
                
               <div class="lost-password">
                <p>To track your order please enter your Order ID in the box below and press the "Track" button. This was given to you on your receipt and in the confirmation email you should have received.</p>
                <form action="#" id="formbuilder">
                    <div class="row learts-mb-n30">
					
                        <div class="col-12 mb-10">
                            <label for="userName">Order Id</label>
                            <input id="orderid" name="orderid" type="text" required>
                        </div>
						<div class="clear"></div>
						
                        <div class="col-12 text-center ">
							<button type="submit" class="buttn">Track Now</button>
                        </div>
                    </div>
                </form>
				<div class="row learts-mb-n30">
				<div id="results" class="col-3 table"></div>
            </div>
			</div>   
			   
			 </div>	
        </div>
    </div>
    <!--breadcrumbs area end-->