<div class="container">
		
		<div class="page_name">
			<div class="row">
		 <div class="col-md-6 ">	
		<h3><?= !empty($this->lang->line('forgot_password')) ? $this->lang->line('forgot_password') : 'Forgot Password' ?></h3>
			</div>
			 <div class="col-md-6 ">	
        <div class="breadcrumb">			
            <ul class="list-inline">
                <li><a href="<?= base_url() ?>"><?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?></a></li>

                <li class="active"><?= !empty($this->lang->line('forgot_password')) ? $this->lang->line('forgot_password') : 'Forgot Password' ?></li>
            </ul>
        </div>
		</div>		 
		</div>
			</div>
		
		
		
    </div>
    <div id="content">
        <section class="form-wrap login-wrap">
            <div class="container">
                <div class="form-wrap-inner login-wrap-inner">
                  
                    <form id="forgot_password_form" name="forgot_password_form" method="POST" action="<?= base_url('auth/forgot_password') ?>" class="form-submit-event6">
                    <div id="infoMessage"></div>
                    <div class="d-flex justify-content-center">
                <div class="form-group" id="error_box"></div>
            </div>
                        <div class="form-group"><label for="email"><?= !empty($this->lang->line('email')) ? $this->lang->line('email') : 'Email' ?><span>*</span></label> <input type="text"  value="" id="email" class="form-control" name="identity" required></div>
                         <button type="submit" data-loading="" class="btn btn-primary btn-sign-in submit_btn">
                         <?= !empty($this->lang->line('submit')) ? $this->lang->line('submit') : 'Submit' ?>
                </button></form> 
                <!-- <span class="sign-in-with">
                Or, Continue With
        </span>
                    <ul class="list-inline social-login">
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Google" class="google"><i class="fab fa-google"></i></a></li>
                    </ul>  -->
                    <!-- <span class="have-an-account">
                Don't Have an Account?
            </span> <a href="<?= base_url('register') ?>" class="btn btn-default btn-create-account">
                CREATE ACCOUNT
            </a> -->
        </div>

            </div>

        </section>


    </div>
