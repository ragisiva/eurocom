<div class="container">
		
		<div class="page_name">
			<div class="row">
		 <div class="col-md-6 ">	
		<h3>Reset Password</h3>
			</div>
			 <div class="col-md-6 ">	
        <div class="breadcrumb">			
            <ul class="list-inline">
                <li><a href="<?= base_url() ?>">Home</a></li>

                <li class="active">Reset Password</li>
            </ul>
        </div>
		</div>		 
		</div>
			</div>
		
		
		
    </div>
    <div id="content">
        <section class="form-wrap login-wrap">
            <div class="container">
                <div class="form-wrap-inner login-wrap-inner">
                  
                    <form id="forgot_password_form" name="reset_password_form" method="POST" action="<?= base_url('auth/reset_password') ?>" class="form-submit-event">
                    <div id="infoMessage"></div>
                    <div class="d-flex justify-content-center">
                <div class="form-group" id="error_box"></div>
            </div>
                        <div class="form-group"><label for="email">New Password<span>*</span></label> <input type="hidden"  value="<?= $code ?>" id="code" class="form-control" name="code"
                        ><input type="password"  value="" id="new" class="form-control" name="password" required></div>
                        <div class="form-group"><label for="email">Confirm Password<span>*</span></label> <input type="password"  value="" id="confirm" class="form-control" name="confirm_password" required></div>
                         <button type="submit" data-loading="" class="btn btn-primary btn-sign-in submit_btn">
                 RESET PASSWORD
                </button></form> 
                <!-- <span class="sign-in-with">
                Or, Continue With
        </span>
                    <ul class="list-inline social-login">
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Google" class="google"><i class="fab fa-google"></i></a></li>
                    </ul>  -->
                    <!-- <span class="have-an-account">
                Don't Have an Account?
            </span> <a href="<?= base_url('register') ?>" class="btn btn-default btn-create-account">
                CREATE ACCOUNT
            </a> -->
        </div>

            </div>

        </section>


    </div>
