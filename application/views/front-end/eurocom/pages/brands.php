<div class="container">
		
		<div class="page_name">
			<div class="row">
		 <div class="col-md-6 ">	
		<h3><?= !empty($this->lang->line('home')) ? $this->lang->line('brand') : 'Brands' ?></h3>
			</div>
			 <div class="col-md-6 ">	
        <div class="breadcrumb">			
            <ul class="list-inline">
                <li><a href="<?= base_url() ?>"><?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?></a></li>

                <li class="active"><?= !empty($this->lang->line('home')) ? $this->lang->line('brand') : 'Brands' ?></li>
            </ul>
        </div>
		</div>		 
		</div>
			</div>
		
		
		
    </div>
    <div id="content">

<section class="custom-page-wrap brand clearfix ">
    <div class="container">
        
        
        <div class="row">
				
				<?php
				$def=base_url()."assets/no-image.png";
				if (isset($brands) && !empty($brands)) { ?>
                        <?php foreach ($brands as $row) { ?>
                            <div class="col-6 col-lg-2 col-md-2  col-sm-3">
                            <div class="zoom-image-thumb">
                                 <a href="<?= base_url('products/brands/' . $row['slug']) ?>">
                                 <img src="<?php if($row['image']!='') { echo $row['image']; } else { echo $def; } ?>" alt=""></a>
                        </div>
                        </div>
					
                  <?php } ?>
                    <?php } ?>
                    
                       
            </div>
        </section>





    </div>
    <!-- End Content -->