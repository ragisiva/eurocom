<!-- breadcrumb -->
 
		
<div class="container">
		
		<div class="page_name">
			<div class="row">
		 <div class="col-md-6 ">	
		<h3><?= $this->lang->line('my_profile') ?></h3>
			</div>
			 <div class="col-md-6 ">	
        <div class="breadcrumb">			
            <ul class="list-inline">
                <li><a href="<?= base_url() ?>"><?= $this->lang->line('home') ?></a></li>

                <li class="active"><?= $this->lang->line('my_profile') ?></li>
            </ul>
        </div>
		</div>		 
		</div>
			</div>
		
		
		
    </div>
<!-- end breadcrumb -->


<div id="content">
    <section class="account-wrap">
        <div class="container">
            <div class="account-wrap-inner">

                <div class="account-left">
                    <?php $this->load->view('front-end/' . THEME . '/pages/my-account-sidebar') ?>

                </div>


                <div class="account-right">
                    <div class="panel-wrap">
                        <div class="panel">
                            <div class="panel-header">
                                <h4><?= $this->lang->line('my_profile') ?></h4>

                            </div>
                            <div class="panel-body">
                            <div class="my-profile"> 
								<form class="form-submit-event1" method="POST" action="<?= base_url('login/update_user') ?>">
                                <div class="form-group" id="error_box">
                        </div>
                                <div class="row">
                                <div class="col-md-6">
                                                    <div class="form-group"><label for="first-name">
                                                    <?= $this->lang->line('first_name') ?><span>*</span></label> <input type="text" name="first_name" value="<?= $users->first_name ?>" id="first-name" class="form-control"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"><label for="last-name">
                                                    <?= $this->lang->line('last_name') ?><span>*</span></label> <input type="text" name="last_name" value="<?= $users->last_name ?>" id="last-name" class="form-control"></div>
                                                </div>  <div class="col-md-6">
                                                    <div class="form-group"><label for="email">
                                                    <?= $this->lang->line('email') ?><span>*</span></label> <input type="text" name="email" value="<?= $users->email ?>" id="email" class="form-control"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"><label for="phone">
                                                    <?= $this->lang->line('phone') ?><span>*</span></label> <input type="text" name="phone" value="<?= $users->mobile ?>" id="phone" class="form-control"></div>
                                                </div>
                                       
                                                <div class="col-md-6">
                                                    <div class="form-group"><label for="password">
                                                    <?= $this->lang->line('old_password') ?>
                                    </label> <input type="password" name="old" id="old" class="form-control"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"><label for="password">
                                                    <?= $this->lang->line('password') ?>
                                    </label> <input type="password" name="new" id="new" class="form-control"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"><label for="confirm-password">
                                                    <?= $this->lang->line('confirm_password') ?>
                                    </label> <input type="password" name="new_confirm" id="new_confirm" class="form-control"></div>
                                                </div>
                                            </div> <button type="submit" data-loading="" class="btn btn-lg btn-primary btn-save-changes submit_btn">
                                            <?= $this->lang->line('save') ?>
                        </button></form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>



            </div>

        </section>


    </div>



