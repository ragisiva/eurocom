<!-- breadcrumb -->
<div class="container">
		
		<div class="page_name">
			<div class="row">
		 <div class="col-md-6 ">	
		<h3>  <?= !empty($this->lang->line('add_new_address')) ? $this->lang->line('add_new_address') : 'Add New Address' ?></h3>
			</div>
			 <div class="col-md-6 ">	
        <div class="breadcrumb">			
            <ul class="list-inline">
                <li><a href="<?= base_url() ?>"><?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?></a></li>               
                <li class="active">  <?= !empty($this->lang->line('add_new_address')) ? $this->lang->line('add_new_address') : 'Add New Address' ?></li>
            </ul>
        </div>
		</div>		 
		</div>
			</div>
		
		
		
    </div>	

<!-- end breadcrumb -->


<div id="content">
    <section class="account-wrap">
        <div class="container">
            <div class="account-wrap-inner">

                <div class="account-left">
                    <?php $this->load->view('front-end/' . THEME . '/pages/my-account-sidebar') ?>

                </div>


                <div class="account-right">
                    <div class="panel-wrap">
                        <div class="panel">
                            <div class="panel-header">
                                <h4>  <?= !empty($this->lang->line('add_new_address')) ? $this->lang->line('add_new_address') : 'Add New Address' ?></h4>
                            <a href="<?= base_url('my-account/manage-address') ?>" class="right btn btn-primary">  <?= !empty($this->lang->line('back')) ? $this->lang->line('back') : 'Back' ?></a>
                            </div>
                            <div class="panel-body">
                           
                            <div class="my-addresses">
                                        <form id="add-address-form" action="<?= base_url('my-account/add-address') ?>" method="post">
                                            <input name="id" type="hidden"  value="<?= $id ?>">
											
											<div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                <div id="save-address-result"></div>
                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group"><label for="email">
                                                <?=!empty($this->lang->line('first_name')) ? $this->lang->line('first_name') : 'First Name '; ?><span>*</span></label>
                                                    <input type="text" name="first_name" id="first_name" class="form-control"  value="<?= $first_name ?>">
                                                    <!---->
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group"><label for="phone">
                                                <?=!empty($this->lang->line('last_name')) ? $this->lang->line('last_name') : 'Last Name '; ?><span>*</span></label>
                                                    <input type="text" name="last_name" id="last_name" class="form-control"  value="<?= $last_name ?>">
                                                    <!---->
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group"><label for="phone">
                                                <?=!empty($this->lang->line('email')) ? $this->lang->line('email') : 'Email'; ?><span>*</span></label>
                                                    <input type="text" name="email" id="email" class="form-control"  value="<?= $email ?>">
                                                    <!---->
                                                </div>
                                            </div>
							<div class="col-md-6">
                                                <div class="form-group"><label for="phone">
                                                <?=!empty($this->lang->line('mobile_number')) ? $this->lang->line('mobile_number') : 'Mobile Number'; ?><span>*</span></label>
                                                    <input type="text" name="mobile" id="mobile" class="form-control"  value="<?= $mobile ?>">
                                                    <!---->
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                        <div class="form-group"><label for="billing-city">
                                                        <?=!empty($this->lang->line('area')) ? $this->lang->line('area') : 'Area'; ?><span>*</span></label>
                                    <select name="area_id" id="area" class="form-control">
                                        <option value="">---<?= !empty($this->lang->line('select_area')) ? $this->lang->line('select_area') : 'Select Area' ?>---</option>
                                    <?php foreach ($cities as $row) { ?>
                                        <optgroup label="<?= $row['name'] ?>">
                                       <?php
                                        $areas = get_areaslist_undercity($row['id'] );
                                        foreach($areas as $a_row){?>
                                        <option value="<?= $a_row['id'] ?>" <?php if($area_id == $a_row['id']) echo 'selected'; ?>><?= $a_row['name'] ?></option>
                                    <?php }?>
                                    </optgroup>
                               <?php } ?>
                                </select>
                                                            <!---->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                <div class="form-group"><label for="phone">
                                                <?=!empty($this->lang->line('block')) ? $this->lang->line('block') : 'Block'; ?><span>*</span></label>
                                                    <input type="text" name="block" id="block" class="form-control"  value="<?= $block ?>">
                                                    <!---->
                                                </div>
                                            </div>
							<div class="col-md-6">
                                                <div class="form-group"><label for="phone">
                                                <?=!empty($this->lang->line('street_no')) ? $this->lang->line('street_no') : 'Street No'; ?><span>*</span></label>
                                                    <input type="text" name="street" id="street" class="form-control"  value="<?= $street ?>">
                                                    <!---->
                                                </div>
                                            </div>
							
							
							<div class="col-md-6">
                                                <div class="form-group"><label for="phone">
                                                <?=!empty($this->lang->line('avenue')) ? $this->lang->line('avenue') : 'Avenue / Building'; ?>  <span>*</span></label>
                                                    <input type="text" name="avenue" id="avenue" class="form-control"  value="<?= $avenue ?>">
                                                    <!---->
                                                </div>
                                            </div>
							
							<div class="col-md-6">
                                                <div class="form-group"><label for="phone">
                                                <?=!empty($this->lang->line('house_no')) ? $this->lang->line('house_no') : 'House No/ Flat No '; ?><span>*</span></label>
                                                    <input type="text" name="house" id="house" class="form-control" value="<?= $house ?>">
                                                    <!---->
                                                </div>
                                            </div>
							
                                            <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                                <label for="country" class="control-label"><?=!empty($this->lang->line('type')) ? $this->lang->line('type') : 'Type : '?></label>
                                <div class="row" style="margin:0">
                                <div class="form-check form-check-inline col-md-3">
                                    <input type="radio" class="form-check-input" name="type" id="home" value="home" <?php if($type == 'home') echo 'checked';?> />
                                    <label for="home" class="form-check-label text-dark"><?=!empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home'?></label>
                                </div>
                                <div class="form-check form-check-inline col-md-3">
                                    <input type="radio" class="form-check-input" name="type" id="office" value="office" <?php if($type == 'office') echo 'checked';?>  placeholder="Office" />
                                    <label for="office" class="form-check-label text-dark"><?=!empty($this->lang->line('office')) ? $this->lang->line('office') : 'Office'?></label>
                                </div>
                                <div class="form-check form-check-inline col-md-3">
                                    <input type="radio" class="form-check-input" name="type" id="other" value="other" <?php if($type == 'other') echo 'checked';?> placeholder="Other" />
                                    <label for="other" class="form-check-label text-dark"><?=!empty($this->lang->line('other')) ? $this->lang->line('other') : 'Other'?></label>
                                </div></div>
                            </div>
							<div class="col-md-12 col-sm-12 col-xs-12">
                            <label>    <input type="checkbox" name="is_default" value="1" <?php if($is_default ==1) echo 'checked';?> /> <?=!empty($this->lang->line('make_default_address')) ? $this->lang->line('make_default_address') : 'Make this address as default'?></label>
                            </div>
									
								
										
                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                <input type="submit" class="btn btn-primary" id="save-address-submit-btn" value="<?= $this->lang->line('save') ?>" />
                            </div>
                           
                        </div>
                    </form>
                                    </div>
                        </div>
                    </div>


                </div>



            </div>

        </section>


    </div>

                       