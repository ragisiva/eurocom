<div id="content">


<section class="home-section-wrap">

    <div class="container">
        <div class="row">
        <div class="home-section-inner">

                <div class="home-slider-wrap">
                    <div class="banner-home4 simple-owl-slider">
                        <div class="wrap-item">
                        <?php if (isset($sliders) && !empty($sliders)) { ?>
                        <?php foreach ($sliders as $row) { ?>
                            <div class="item-banner4 active">
                                <div class="banner-thumb">
                                    <a href="<?= $row['link'] ?>"><img src="<?= base_url($row['image']) ?>" alt="" /></a>
                                </div>
                                <!--<div class="banner-info rotate-text">
                                <h2>Glasse for Men’s</h2>
                                <h3>Model 2016</h3>
                            </div>-->
                            </div>
                            <?php } ?>
                    <?php } ?>
                            



                        </div>
                    </div>
                </div>
                <?php
                $offers =  get_offers(); 
		 $offernew1=array();
         $offernew=array();
		$offernew[]=$offers[0];
		$offernew[]=$offers[1];
		$offernew1[]=$offers[2];
		$offernew1[]=$offers[3];
		$i=1;
		 
		?>
                <div class="home-banner-wrap">
                <?php foreach($offernew1 as $row){?>
                    <div class="item-adv-simple adv-home6">
                        <a href="<?=$row['link']?>"><img src="<?=base_url($row['image'])?>" alt=""></a>
                    </div>
                    <?php
                } ?>
                  

                </div>


            </div>


        </div>


    </div>
</section>



<div class="container">
<?php //print_r(json_encode($sections)) ;?>

<?php
    if(isset($sections[0])){
        if (!empty($sections[0]['product_details'])) {
            $tab_categories = $sections[0]['tab_categories']; 
            $tab_products = $sections[0]['product_details']; 
      
            if($sections[0]['total_products'] >1){?>
    <!-- End Privacy Shipping -->
    <div class="hot-deal-tab-slider hot-deal-tab-slider12">
        <div class="hot-deal-tab-countdown" data-date="12/15/2016"></div>
        <div class="hot-deal-tab-title">
            <label><?= !empty($this->lang->line('hot_deals')) ? $this->lang->line('hot_deals') : 'Hot Deals' ?></label>
            <ul class="nav nav-pills">
                <?php
                if($tab_categories){
                    $start = 0;
                    foreach($tab_categories as $cat_id => $cat_name){
                        if(isset($tab_products[$cat_id]['product']) && count($tab_products[$cat_id]['product']) >0){?>
                        <li <?php if($start == 0) echo 'class="active"';?>><a href="#cat_<?= $cat_id ?>" data-toggle="tab"><?= $cat_name ?></a></li>
                   <?php 
                   $start++; }}
                }?>
              
            </ul>
        </div>
        <div class="tab-content">

            <?php if($tab_products){
                $j=0;
                foreach($tab_products as $pro_cat => $products){
                  
                    if(isset($products['product']) && count($products['product']) >0){
                    ?>
                                
                    <div role="tabpanel" class="tab-pane fade <?php if($j==0) echo 'in active' ; ?>" id="cat_<?= $pro_cat ?>">
                    <div class="hot-deal-slider slider-home2">
                        <div class="wrap-item">
                           <?php if($products['product']) {
                               foreach($products['product'] as $each){
                            
                                    $product_name = $each[$lang_prefix.'name'];
                                    $product_slug =  '';
                                    $main_image =$each['image'];
                                    $product_id = $each['id'];
                                    $m = $lang_prefix.'variant_values'; 
                                   // $is_favorite = $each['is_favorite'];
                                    $product_stock = $each['stock'];
                                 //   print_r($each['variants']);
                                  
                                        $product_slug =  base_url('products/details/' . $each['slug']);
                                        if($each['special_price'] > 0 && $each['special_price'] < $each['price']){
                                            $discount_in_percentage = find_discount_in_percentage($each['special_price'], $each['price']);
                                        }
                                        else{
                                            $discount_in_percentage = 0;
                                        }
                                        if($each['type'] =='variable_product'){
                                            if($each['stock_type']==NULL){
                                                if($product_stock!=null && intval($product_stock) <=0)
                                                $stockupdate=1;
                                               
                                               else{
                                                   $stockupdate=0;
                                               }
                                            }
                                            elseif($each['stock_type']=='2'){
                                                if($each['variant_stock']!=NULL && intval($each['variant_stock']) <=0){
                                                    $stockupdate=1;
                                                }
                                                else{
                                                    $stockupdate=0;
                                                }
                                            }
                                            
                                            
                                        }
                                        else
                                        {
                                            if($each['stock_type'] ==NULL){
                                          
                                                $stockupdate=0;
                                            }
                                            else{
                                                if($each['stock']!=NULL && intval($each['stock']) <=0){
                                                    $stockupdate=1;
                                                }
                                                else{
                                                    $stockupdate=0;
                                                }
                                            }
                                        } 
                                       
                                        $image = json_decode($each['variant_images']);

                                        if(isset($image[0]) && $image[0] !=''){                                           
                                           $sm_image = get_image_url($image[0], 'thumb', 'sm');
                                        }
                                        else{
                                            $sm_image =get_image_url($main_image,'thumb', 'sm');
                                        }
                                        $variant_id =$each['variant_id'] ;
                                        $modal = "";
                                        $class="add-to-cart";
                                       if ($is_logged_in) {
                                            $is_favorite = is_exist(['user_id' => $user->id,
                                            'product_id' => $product_id,
                                            'product_variant_id' => $variant_id],'favorites');
                                            if($is_favorite)
                                                $is_favorite =1;
                                            else
                                            $is_favorite =0;
                                            }
                                            else
                                            $is_favorite =0;
                                            $product_slug =  base_url('products/details/' . $each['slug'].'/'.$variant_id);
                                        ?>

                                        <div class="item">
                                        <div class="item-hot-deal-product">
                                          <div class="hot-deal-product-thumb">
                                             
                                      <?php if($stockupdate==1) { ?>
                                                                   <div class="outofstock">
                                                       <strong> <?= !empty($this->lang->line('out_of_stock')) ? $this->lang->line('out_of_stock') : 'Out of Stock' ?></strong>
                                                   </div>
                                      <?php }
                                     if($discount_in_percentage!='' && $discount_in_percentage>0 && $discount_in_percentage < 100){
                                          ?>
                                          <div class="cat-hover-percent">
                                                                       <strong><?= $discount_in_percentage ?>% </strong>
                                                                       <!--<span>+10% for Member</span>-->
                                                                   </div>
                                         
                                          <?php
                                      } ?>
                                      <!-- <div class="cat-hover-percent">
                                                   <strong>10%</strong>
                                                  
                                               </div> -->
                                               <div class="product-thumb">
                                                   <a class="product-thumb-link" href="<?= $product_slug ?>">
                                                       <img alt="" src="<?= $sm_image ?>"
                                                           class="first-thumb">
           
                                                   </a>
                                                   <div class="product2-buttons">
                                                       <a href="<?= base_url('products/product_quickview/'.$product_id.'/'.$variant_id) ?>"
                                                           class="product-button hintT-top manual-ajax"
                                                           rel="modal:open"  data-hint="<?= $this->lang->line('quick_view')?>" ><i
                                                               class="fa fa-search"></i></a>
                                                               
                                                               <a href="#"  data-hint="<?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'Add to Cart' ?>" class="<?php echo $class; ?> product-button hintT-top" data-product-id="<?= $product_id ?>"  data-product-qty="1" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>" ><i class="fa fa-shopping-cart"></i></a>
                                                        
                                                       
                                                       <a href="javascript:void(0);"  data-product-id="<?=  $product_id ?>" data-product-variant-id="<?= $variant_id ?>"   class="product-button hintT-top wishlist add-to-fav-btn lnr "  data-hint="<?= $this->lang->line('quick_view')?>"><i class="fa fa-heart <?= ( $is_favorite == 1) ? ' text-danger' : '' ?>"></i></a>
                                                     
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="hot-deal-product-info">
                                               <h3 class="title-product"><a href="<?= $product_slug ?>"><?= $product_name ?> <?php echo $each[$m]; ?></a></h3>
                                               <div class="info-price">
                                             
                                            
                                                           <span><?php 
                                                         
                                                               if($each['special_price'] > 0 && $each['special_price']< $each['price']){?>
                                                                   <span ><?= $settings['currency'] ?> <?= number_format($each['special_price'],3) ?></span><del><?= $settings['currency'] ?> <?= number_format($each['price'],3) ?></del>
       
                                                               <?php }
                                                               else{
                                                                   ?>
                                                                       <span ><?= $settings['currency'] ?> <?= number_format($each['price'],3) ?></span>	 
                                                              <?php } ?>
                                                          </span>
                                                          
                                               </div>
                                           </div>
                                       </div>
                                   </div>

                                  <?php 

                                
                                ?>
                            <!-- End Item -->
                             <?php  $j++; }
                           }?>
                            
                         
                        </div>
                    </div>
    
                </div>
              <?php  }}
            } ?>
           
        </div>
    </div>
    <!-- End Hot Deal Tab -->
    <?php 
        }}
        } ?>
    <div class="box-adv10 box-adv-col2">
    
        <div class="row">
        <?php
		$i=1;
		 foreach($offernew as $row){
		?>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="item-adv-simple">
                    <a href="<?=$row['link']?>"><img src="<?=base_url($row['image'])?>" alt=""></a>
                </div>
            </div>
            <?php 
        $i++;
        } ?>
            
        </div>
    </div>
    <!-- End Adv -->

    <!-- End Red Box -->

    <!-- End Yellow Box -->

    <!-- End Pink Box -->

    <!-- End Blue Box -->

    <!-- End Green Box -->

    <!-- End Yellow Box -->
    <div class="content-popular12">
        <div class="popular-cat-title popular-cat-label">
            <label><?= !empty($this->lang->line('best_seller')) ? $this->lang->line('best_seller') : 'Best Seller' ?></label>
            <ul class="nav nav-pills">
                <li class="active"><a href="#new_product" data-toggle="tab"><?= !empty($this->lang->line('latest_product')) ? $this->lang->line('latest_product') : 'Latest Product' ?></a></li>
                <li><a href="#recently_viewed" data-toggle="tab"> <?= !empty($this->lang->line('recently_Viewed')) ? $this->lang->line('recently_Viewed') : 'Recently Viewed' ?></a></li>
                <li><a href="#top_selling" data-toggle="tab"><?= !empty($this->lang->line('top_selling')) ? $this->lang->line('top_selling') : 'Top Selling' ?> </a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="new_product">
                <div class="popular-cat-slider slider-home5">
                    <div class="wrap-item">
                    <?php if($latest_product['product']) {
                               foreach($latest_product['product'] as $each){
                                $product_name = $each[$lang_prefix.'name'];
                                $product_slug =  '';
                                $main_image =$each['image'];
                                $product_id = $each['id'];
                                $m = $lang_prefix.'variant_values'; 
                               // $is_favorite = $each['is_favorite'];
                                $product_stock = $each['stock'];
                             //   print_r($each['variants']);
                              
                                    $product_slug =  base_url('products/details/' . $each['slug']);
                                    if($each['special_price'] > 0 && $each['special_price'] < $each['price']){
                                        $discount_in_percentage = find_discount_in_percentage($each['special_price'], $each['price']);
                                    }
                                    else{
                                        $discount_in_percentage = 0;
                                    }
                                    if($each['type'] =='variable_product'){
                                        if($each['stock_type']==NULL){
                                            if($product_stock!=null && intval($product_stock) <=0)
                                            $stockupdate=1;
                                           
                                           else{
                                               $stockupdate=0;
                                           }
                                        }
                                        elseif($each['stock_type']=='2'){
                                            if($each['variant_stock']!=NULL && intval($each['variant_stock']) <=0){
                                                $stockupdate=1;
                                            }
                                            else{
                                                $stockupdate=0;
                                            }
                                        }
                                        
                                        
                                    }
                                    else
                                    {
                                        if($each['stock_type'] ==NULL){
                                      
                                            $stockupdate=0;
                                        }
                                        else{
                                            if($each['stock']!=NULL && intval($each['stock']) <=0){
                                                $stockupdate=1;
                                            }
                                            else{
                                                $stockupdate=0;
                                            }
                                        }
                                    } 
                                   
                                    $image = json_decode($each['variant_images']);

                                    if(isset($image[0]) && $image[0] !=''){                                           
                                       $sm_image = get_image_url($image[0], 'thumb', 'sm');
                                    }
                                    else{
                                        $sm_image =get_image_url($main_image,'thumb', 'sm');
                                    }
                                    $variant_id =$each['variant_id'] ;
                                    $modal = "";
                                    $class="add-to-cart";
                                   if ($is_logged_in) {
                                        $is_favorite = is_exist(['user_id' => $user->id,
                                        'product_id' => $product_id,
                                        'product_variant_id' => $variant_id],'favorites');
                                        if($is_favorite)
                                            $is_favorite =1;
                                        else
                                        $is_favorite =0;
                                        }
                                        else
                                        $is_favorite =0;
                                        $product_slug =  base_url('products/details/' . $each['slug'].'/'.$variant_id);
                                    ?>

                                    <div class="item">
                                    <div class="item-hot-deal-product">
                                      <div class="hot-deal-product-thumb">
                                         
                                  <?php if($stockupdate==1) { ?>
                                                               <div class="outofstock">
                                                   <strong> <?= !empty($this->lang->line('out_of_stock')) ? $this->lang->line('out_of_stock') : 'Out of Stock' ?></strong>
                                               </div>
                                  <?php }
                                 if($discount_in_percentage!='' && $discount_in_percentage>0 && $discount_in_percentage < 100){
                                      ?>
                                      <div class="cat-hover-percent">
                                                                   <strong><?= $discount_in_percentage ?>% </strong>
                                                                   <!--<span>+10% for Member</span>-->
                                                               </div>
                                     
                                      <?php
                                  } ?>
                                  <!-- <div class="cat-hover-percent">
                                               <strong>10%</strong>
                                              
                                           </div> -->
                                           <div class="product-thumb">
                                               <a class="product-thumb-link" href="<?= $product_slug ?>">
                                                   <img alt="" src="<?= $sm_image ?>"
                                                       class="first-thumb">
       
                                               </a>
                                               <div class="product2-buttons">
                                                   <a href="<?= base_url('products/product_quickview/'.$product_id.'/'.$variant_id) ?>"
                                                       class="product-button hintT-top manual-ajax"
                                                       rel="modal:open"   data-hint="<?= $this->lang->line('quick_view')?>"><i
                                                           class="fa fa-search"></i></a>
                                                           
                                                           <a href="#"  data-hint="<?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'Add to Cart' ?>" class="<?php echo $class; ?> product-button hintT-top" data-product-id="<?= $product_id ?>"  data-product-qty="1" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>"  ><i class="fa fa-shopping-cart"></i></a>
                                                    
                                                   
                                                   <a href="javascript:void(0);"  data-product-id="<?=  $product_id ?>" data-product-variant-id="<?= $variant_id ?>"  class="product-button hintT-top wishlist add-to-fav-btn lnr "  data-hint="<?= $this->lang->line('add_to_wishlist')?>"><i class="fa fa-heart <?= ( $is_favorite == 1) ? ' text-danger' : '' ?>"></i></a>
                                                 
                                               </div>
                                           </div>
                                       </div>
                                       <div class="hot-deal-product-info">
                                           <h3 class="title-product"><a href="<?= $product_slug ?>"><?= $product_name ?> <?php echo $each[$m]; ?></a></h3>
                                           <div class="info-price">
                                         
                                        
                                                       <span><?php 
                                                     
                                                           if($each['special_price'] > 0 && $each['special_price']< $each['price']){?>
                                                               <span ><?= $settings['currency'] ?> <?= number_format($each['special_price'],3) ?></span><del><?= $settings['currency'] ?> <?= number_format($each['price'],3) ?></del>
   
                                                           <?php }
                                                           else{
                                                               ?>
                                                                   <span ><?= $settings['currency'] ?> <?= number_format($each['price'],3) ?></span>	 
                                                          <?php } ?>
                                                      </span>
                                                      
                                           </div>
                                       </div>
                                   </div>
                               </div>
                            <!-- End Item -->
                             <?php  $j++; }
                           }?>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade " id="recently_viewed">
                <div class="popular-cat-slider popular-cat-slider12 slider-home5">
                    <div class="wrap-item">                        
                    <?php if(isset($recently_viewed_product['product']) && $recently_viewed_product['product']) {
                               foreach($recently_viewed_product['product'] as $each){
                                $product_name = $each[$lang_prefix.'name'];
                                $product_slug =  '';
                                $main_image =$each['image'];
                                $product_id = $each['id'];
                                $m = $lang_prefix.'variant_values'; 
                               // $is_favorite = $each['is_favorite'];
                                $product_stock = $each['stock'];
                             //   print_r($each['variants']);
                              
                                    $product_slug =  base_url('products/details/' . $each['slug']);
                                    if($each['special_price'] > 0 && $each['special_price'] < $each['price']){
                                        $discount_in_percentage = find_discount_in_percentage($each['special_price'], $each['price']);
                                    }
                                    else{
                                        $discount_in_percentage = 0;
                                    }
                                    if($each['type'] =='variable_product'){
                                        if($each['stock_type']==NULL){
                                            if($product_stock!=null && intval($product_stock) <=0)
                                            $stockupdate=1;
                                           
                                           else{
                                               $stockupdate=0;
                                           }
                                        }
                                        elseif($each['stock_type']=='2'){
                                            if($each['variant_stock']!=NULL && intval($each['variant_stock']) <=0){
                                                $stockupdate=1;
                                            }
                                            else{
                                                $stockupdate=0;
                                            }
                                        }
                                        
                                        
                                    }
                                    else
                                    {
                                        if($each['stock_type'] ==NULL){
                                      
                                            $stockupdate=0;
                                        }
                                        else{
                                            if($each['stock']!=NULL && intval($each['stock']) <=0){
                                                $stockupdate=1;
                                            }
                                            else{
                                                $stockupdate=0;
                                            }
                                        }
                                    } 
                                   
                                    $image = json_decode($each['variant_images']);

                                    if(isset($image[0]) && $image[0] !=''){                                           
                                       $sm_image = get_image_url($image[0], 'thumb', 'sm');
                                    }
                                    else{
                                        $sm_image =get_image_url($main_image,'thumb', 'sm');
                                    }
                                    $variant_id =$each['variant_id'] ;
                                    $modal = "";
                                    $class="add-to-cart";
                                   if ($is_logged_in) {
                                        $is_favorite = is_exist(['user_id' => $user->id,
                                        'product_id' => $product_id,
                                        'product_variant_id' => $variant_id],'favorites');
                                        if($is_favorite)
                                            $is_favorite =1;
                                        else
                                        $is_favorite =0;
                                        }
                                        else
                                        $is_favorite =0;
                                        $product_slug =  base_url('products/details/' . $each['slug'].'/'.$variant_id);
                                    ?>

                                    <div class="item">
                                    <div class="item-hot-deal-product">
                                      <div class="hot-deal-product-thumb">
                                         
                                  <?php if($stockupdate==1) { ?>
                                                               <div class="outofstock">
                                                   <strong> <?= !empty($this->lang->line('out_of_stock')) ? $this->lang->line('out_of_stock') : 'Out of Stock' ?></strong>
                                               </div>
                                  <?php }
                                 if($discount_in_percentage!='' && $discount_in_percentage>0 && $discount_in_percentage < 100){
                                      ?>
                                      <div class="cat-hover-percent">
                                                                   <strong><?= $discount_in_percentage ?>% </strong>
                                                                   <!--<span>+10% for Member</span>-->
                                                               </div>
                                     
                                      <?php
                                  } ?>
                                  <!-- <div class="cat-hover-percent">
                                               <strong>10%</strong>
                                              
                                           </div> -->
                                           <div class="product-thumb">
                                               <a class="product-thumb-link" href="<?= $product_slug ?>">
                                                   <img alt="" src="<?= $sm_image ?>"
                                                       class="first-thumb">
       
                                               </a>
                                               <div class="product2-buttons">
                                                   <a href="<?= base_url('products/product_quickview/'.$product_id.'/'.$variant_id) ?>"
                                                       class="product-button hintT-top manual-ajax"
                                                       rel="modal:open" 
                                                       data-hint="<?= $this->lang->line('quick_view')?>"><i
                                                           class="fa fa-search"></i></a>
                                                           
                                                           <a href="#"  data-hint="<?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'Add to Cart' ?>" class="<?php echo $class; ?> product-button hintT-top" data-product-id="<?= $product_id ?>"  data-product-qty="1" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>" ><i class="fa fa-shopping-cart"></i></a>
                                                    
                                                   
                                                   <a href="javascript:void(0);"  data-product-id="<?=  $product_id ?>" data-product-variant-id="<?= $variant_id ?>"   class="product-button hintT-top wishlist add-to-fav-btn lnr "  data-hint="<?= $this->lang->line('add_to_wishlist')?>"><i class="fa fa-heart <?= ( $is_favorite == 1) ? ' text-danger' : '' ?>"></i></a>
                                                 
                                               </div>
                                           </div>
                                       </div>
                                       <div class="hot-deal-product-info">
                                           <h3 class="title-product"><a href="<?= $product_slug ?>"><?= $product_name ?> <?php echo $each[$m]; ?></a></h3>
                                           <div class="info-price">
                                         
                                        
                                                       <span><?php 
                                                     
                                                           if($each['special_price'] > 0 && $each['special_price']< $each['price']){?>
                                                               <span ><?= $settings['currency'] ?> <?= number_format($each['special_price'],3) ?></span><del><?= $settings['currency'] ?> <?= number_format($each['price'],3) ?></del>
   
                                                           <?php }
                                                           else{
                                                               ?>
                                                                   <span ><?= $settings['currency'] ?> <?= number_format($each['price'],3) ?></span>	 
                                                          <?php } ?>
                                                      </span>
                                                      
                                           </div>
                                       </div>
                                   </div>
                               </div>
                            <!-- End Item -->
                             <?php  $j++; }
                           }?>
                    </div>
                </div>
            </div>
          
            <div role="tabpanel" class="tab-pane fade" id="top_selling">
                <div class="popular-cat-slider slider-home5">
                    <div class="wrap-item">
                    <?php if($top_selling_product['product']) {
                               foreach($top_selling_product['product'] as $each){
                                $product_name = $each[$lang_prefix.'name'];
                                $product_slug =  '';
                                $main_image =$each['image'];
                                $product_id = $each['id'];
                                $m = $lang_prefix.'variant_name'; 
                               // $is_favorite = $each['is_favorite'];
                                $product_stock = $each['stock'];
                             //   print_r($each['variants']);
                              
                                    
                                    if($each['special_price'] > 0 && $each['special_price'] < $each['price']){
                                        $discount_in_percentage = find_discount_in_percentage($each['special_price'], $each['price']);
                                    }
                                    else{
                                        $discount_in_percentage = 0;
                                    }
                                    if($each['type'] =='variable_product'){
                                        if($each['stock_type']==NULL){
                                            if($product_stock!=null && intval($product_stock) <=0)
                                            $stockupdate=1;
                                           
                                           else{
                                               $stockupdate=0;
                                           }
                                        }
                                        elseif($each['stock_type']=='2'){
                                            if($each['variant_stock']!=NULL && intval($each['variant_stock']) <=0){
                                                $stockupdate=1;
                                            }
                                            else{
                                                $stockupdate=0;
                                            }
                                        }
                                        
                                        
                                    }
                                    else
                                    {
                                        if($each['stock_type'] ==NULL){
                                      
                                            $stockupdate=0;
                                        }
                                        else{
                                            if($each['stock']!=NULL && intval($each['stock']) <=0){
                                                $stockupdate=1;
                                            }
                                            else{
                                                $stockupdate=0;
                                            }
                                        }
                                    } 
                                   
                                    $image = json_decode($each['variant_images']);

                                    if(isset($image[0]) && $image[0] !=''){                                           
                                       $sm_image = get_image_url($image[0], 'thumb', 'sm');
                                    }
                                    else{
                                        $sm_image =get_image_url($main_image,'thumb', 'sm');
                                    }
                                    $variant_id =$each['variant_id'] ;
                                    $modal = "";
                                    $class="add-to-cart";
                                   if ($is_logged_in) {
                                        $is_favorite = is_exist(['user_id' => $user->id,
                                        'product_id' => $product_id,
                                        'product_variant_id' => $variant_id],'favorites');
                                        if($is_favorite)
                                            $is_favorite =1;
                                        else
                                        $is_favorite =0;
                                        }
                                        else
                                        $is_favorite =0;
                                        $product_slug =  base_url('products/details/' . $each['slug'].'/'.$variant_id);
                                    ?>

                                    <div class="item">
                                    <div class="item-hot-deal-product">
                                      <div class="hot-deal-product-thumb">
                                         
                                  <?php if($stockupdate==1) { ?>
                                                               <div class="outofstock">
                                                   <strong> <?= !empty($this->lang->line('out_of_stock')) ? $this->lang->line('out_of_stock') : 'Out of Stock' ?></strong>
                                               </div>
                                  <?php }
                                 if($discount_in_percentage!='' && $discount_in_percentage>0 && $discount_in_percentage < 100){
                                      ?>
                                      <div class="cat-hover-percent">
                                                                   <strong><?= $discount_in_percentage ?>% </strong>
                                                                   <!--<span>+10% for Member</span>-->
                                                               </div>
                                     
                                      <?php
                                  } ?>
                                  <!-- <div class="cat-hover-percent">
                                               <strong>10%</strong>
                                              
                                           </div> -->
                                           <div class="product-thumb">
                                               <a class="product-thumb-link" href="<?= $product_slug ?>">
                                                   <img alt="" src="<?= $sm_image ?>"
                                                       class="first-thumb">
       
                                               </a>
                                               <div class="product2-buttons">
                                                   <a href="<?= base_url('products/product_quickview/'.$product_id.'/'.$variant_id) ?>"
                                                       class="product-button hintT-top manual-ajax"
                                                       rel="modal:open"   data-hint="<?= $this->lang->line('quick_view')?>"><i
                                                           class="fa fa-search"></i></a>
                                                           
                                                           <a href="#"  data-hint="<?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'Add to Cart' ?>" class="<?php echo $class; ?> product-button hintT-top" data-product-id="<?= $product_id ?>"  data-product-qty="1" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>"  ><i class="fa fa-shopping-cart"></i></a>
                                                    
                                                   
                                                   <a href="javascript:void(0);"  data-product-id="<?=  $product_id ?>" data-product-variant-id="<?= $variant_id ?>"  class="product-button hintT-top wishlist add-to-fav-btn lnr "  data-hint="<?= $this->lang->line('add_to_wishlist')?>"><i class="fa fa-heart <?= ( $is_favorite == 1) ? ' text-danger' : '' ?>"></i></a>
                                                 
                                               </div>
                                           </div>
                                       </div>
                                       <div class="hot-deal-product-info">
                                           <h3 class="title-product"><a href="<?= $product_slug ?>"><?= $product_name ?> <?php echo $each[$m]; ?></a></h3>
                                           <div class="info-price">
                                         
                                        
                                                       <span><?php 
                                                     
                                                           if($each['special_price'] > 0 && $each['special_price']< $each['price']){?>
                                                               <span ><?= $settings['currency'] ?> <?= number_format($each['special_price'],3) ?></span><del><?= $settings['currency'] ?> <?= number_format($each['price'],3) ?></del>
   
                                                           <?php }
                                                           else{
                                                               ?>
                                                                   <span ><?= $settings['currency'] ?> <?= number_format($each['price'],3) ?></span>	 
                                                          <?php } ?>
                                                      </span>
                                                      
                                           </div>
                                       </div>
                                   </div>
                               </div>
                            <!-- End Item -->
                             <?php  $j++; }
                           }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Popular Product -->



    <!-- End World of Aloshop -->
    <div class="manufacturers-banner mt-lg-5 mt-sm-5">
        <h2 class="title-home3"> <?=!empty($this->lang->line('our_brands')) ? $this->lang->line('our_brands') : 'Our Brands'?></h2>
        <div class="manufacture-slider">
            <div class="wrap-item">
            <?php if (isset($brands) && !empty($brands)) { ?>
                        <?php foreach ($brands as $row) { ?> 
                <div class="item-manufacture">
                    <div class="zoom-image-thumb">
                        <a href="<?=base_url('home/brands')?>"><img src="<?= $row['image'] ?>" alt="" /></a>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
               
            </div>
        </div>
    </div>
    <!-- End Manufacturers -->

    <div class="col-lg-4 col-md-5 col-sm-5 col-xs-12  desk-hide tab-hide">

    <?php foreach($offernew1 as $row){?>
        <div class="item-adv-simple adv-home6 mar_top_30">
            <a href="<?=$row['link']?>"><img src="<?=base_url($row['image'])?>" alt=""></a>
        </div>
       <?php }?>



    </div>


</div>
</div>