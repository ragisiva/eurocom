<div class="container">

    <div class="page_name">
        <div class="row">
            <div class="col-md-6 ">
                <h3><?= $this->lang->line('my_account') ?></h3>
            </div>
            <div class="col-md-6 ">
                <div class="breadcrumb">
                    <ul class="list-inline">
                        <li><a href="<?= base_url() ?>"><?= $this->lang->line('home') ?></a></li>

                        <li class="active"><?= $this->lang->line('my_account') ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



</div>

<div id="content">
    <section class="account-wrap">
        <div class="container">
            <div class="account-wrap-inner">

                <div class="account-left">
                    <?php $this->load->view('front-end/' . THEME . '/pages/my-account-sidebar') ?>

                </div>


                <div class="account-right">
                    <div class="panel-wrap">
                        <div class="panel">
                            <div class="panel-header">
                                <h4><?= $this->lang->line('dashboard') ?></h4>
                              
                            </div>
                            <div class="panel-body">
                                <p><?= $this->lang->line('dashboard_content') ?></p>
                                <div class="row">
                                    <div class='col-md-2 card text-center border-0 mr-3 mb-3'>
                                        <a href='<?= base_url('my-account/profile') ?>' class="link-color">
                                            <div class='card-header bg-transparent'>
                                                <?= !empty($this->lang->line('profile')) ? $this->lang->line('profile') : 'PROFILE' ?>
                                            </div>
                                            <div class='card-body'>
                                                <i class="fa fa-user-circle dashboard-icon link-color fa-lg"></i>
                                            </div>
                                        </a>
                                    </div>
                                    <div class='col-md-2 card text-center border-0 mr-3 mb-3'>
                                        <a href='<?= base_url('my-account/orders') ?>' class="link-color">
                                            <div class='card-header bg-transparent'>
                                                <?= !empty($this->lang->line('orders')) ? $this->lang->line('orders') : 'ORDERS' ?>
                                            </div>
                                            <div class='card-body'>
                                                <i class="fa fa-history dashboard-icon link-color fa-lg"></i>
                                            </div>
                                        </a>
                                    </div>

                                    <div class='col-md-2 card text-center border-0 mr-3 mb-3'>
                                        <a href='<?= base_url('my-account/Favorite') ?>' class="link-color">
                                            <div class='card-header bg-transparent'>
                                            <?= !empty($this->lang->line('wishlist')) ? $this->lang->line('wishlist') : 'WISHLIST' ?>  
                                            </div>
                                            <div class='card-body'>
                                                <i class="fa fa-heart dashboard-icon link-color fa-lg"></i>
                                            </div>
                                        </a>
                                    </div>
                                    <div class='col-md-2 card text-center border-0 mr-3 mb-3'>
                                        <a href='<?= base_url('my-account/manage-address') ?>' class="link-color">
                                            <div class='card-header bg-transparent'>
                                                <?= !empty($this->lang->line('address')) ? $this->lang->line('address') : 'ADDRESS' ?>
                                            </div>
                                            <div class='card-body'>
                                                <i class="fa fa-id-badge dashboard-icon link-color fa-lg"></i>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>



        </div>

    </section>


</div>

