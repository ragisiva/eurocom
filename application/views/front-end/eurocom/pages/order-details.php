<!-- Demo header-->
<div class="container">

    <div class="page_name">
        <div class="row">
            <div class="col-md-6 ">
                <h3>
                    <?= !empty($this->lang->line('order_details')) ? $this->lang->line('order_details') : 'Order Details' ?>
                </h3>
            </div>
            <div class="col-md-6 ">
                <div class="breadcrumb">
                    <ul class="list-inline">
                        <li><a href="<?= base_url() ?>">
                                <?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?>
                            </a></li>

                        <li class="active">
                            <?= !empty($this->lang->line('order_details')) ? $this->lang->line('order_details') : 'Order Details' ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



</div>
<!-- end breadcrumb -->

<div id="content">
    <section class="account-wrap">
        <div class="container">
            <div class="account-wrap-inner">

                <div class="account-left">
                    <?php $this->load->view('front-end/' . THEME . '/pages/my-account-sidebar') ?>

                </div>


                <div class="account-right">
                    <div class="panel-wrap">
                        <div class="panel">
                            <div class="panel-header">
                                <h4>
                                    <?= !empty($this->lang->line('order_details')) ? $this->lang->line('order_details') : 'Order Details' ?>
                                   </h4>
                                   <a href="<?= base_url('my-account/orders/') ?>"
                                                                        class='btn btn-info btn-sm float-right'
                                                                        style="color: #ffffff;">
                                                                        <?= !empty($this->lang->line('back_to_list')) ? $this->lang->line('back_to_list') : 'Back to Order' ?>
                                                                    </a>

                            </div>
                            <div class="panel-body">
                                <div class="shopping-cart">
                                    <div class="shopping-cart-inner">


                                        <div class="row  order_details">
                                            <div class="col-md-6 col-12 learts-mb-30">
                                                <div class="oderadd">
                                                    <h4 class="title">
                                                        <?= !empty($this->lang->line('order_details')) ? $this->lang->line('order_details') : 'Order Details' ?>
                                                    </h4>
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('order_id')) ? $this->lang->line('order_id') : 'Order ID' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= $order['id'] ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('order_date')) ? $this->lang->line('order_date') : 'Order Date' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= date('d-m-Y h:i A',strtotime($order['date_added'])) ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            if($order['promo_code'] !=''){?>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('applied_coupon')) ? $this->lang->line('applied_coupon') : 'Applied Coupon' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= $order['promo_code']  ?>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                            <?php
                                                                if($order['active_status'] !=''){?>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('status')) ? $this->lang->line('status') : 'Status' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= str_replace('_',' ',strtoupper($order['active_status'])) ?>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('delivery_date_time')) ? $this->lang->line('delivery_date_time') : 'Delivery Date & Time' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <span>
                                                                        <?= $order['delivery_date'] ?>
                                                                    </span><span>
                                                                        <?= $order['delivery_time'] ?>
                                                                    </span> <br>
                                                                </td>
                                                            </tr>

                                                      
                                                    <tr><th colspan="3"><h4 class="title"> <?= !empty($this->lang->line('order_summary')) ? $this->lang->line('order_summary') : 'Order Summary' ?></h4>
                                                                </th>
                                                      
                                                                <tr>
                                                                    <th>
                                                                        <?= !empty($this->lang->line('total_order_price')) ? $this->lang->line('total_order_price') : 'Total Order Price' ?>
                                                                    </th>
                                                                    <td>:</td>
                                                                    <td>+
                                                                        <?= $settings['currency'] . ' ' . number_format($order['total'], 2) ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>
                                                                        <?= !empty($this->lang->line('delivery_charge')) ? $this->lang->line('delivery_charge') : 'Delivery Charge' ?>
                                                                    </th>
                                                                    <td>:</td>
                                                                    <td>+
                                                                        <?= $settings['currency'] . ' ' . number_format($order['delivery_charge'], 2) ?>
                                                                    </td>
                                                                </tr>
                                                                <!-- <tr>
                                                                    <th>
                                                                        <?= !empty($this->lang->line('tax')) ? $this->lang->line('tax') : 'Tax' ?>
                                                                        - (
                                                                        <?= $order['total_tax_percent'] ?>%)
                                                                    </th>
                                                                    <td>+
                                                                        <?= $settings['currency'] . ' ' . number_format($order['total_tax_amount'], 2) ?>
                                                                    </td>
                                                                </tr> -->
                                                                <?php if (!empty($order['promo_code']) && !empty($order['promo_discount'])) { ?>
                                                                <tr>
                                                                    <th>
                                                                        <?= !empty($this->lang->line('promocode_discounts')) ? $this->lang->line('promocode_discounts') : 'Promocode Discount' ?>
                                                                        - (
                                                                        <?= $order['promo_code'] ?>)
                                                                    </th>
                                                                    <td>:</td>
                                                                    <td>-
                                                                        <?= $settings['currency'] . ' ' . number_format($order['promo_discount'], 2) ?>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>
                                                                <!-- <tr>
                                                                    <th>
                                                                        <?= !empty($this->lang->line('wallet_used')) ? $this->lang->line('wallet_used') : 'Wallet Used' ?>
                                                                    </th>
                                                                    <td>-
                                                                        <?= $settings['currency'] . ' ' . number_format($order['wallet_balance'], 2) ?>
                                                                    </td>
                                                                </tr> -->
                                                                <tr class="h6">
                                                                    <th>
                                                                        <?= !empty($this->lang->line('final_total')) ? $this->lang->line('final_total') : 'Final Total' ?>
                                                                    </th>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <?= $settings['currency'] . ' ' . number_format($order['final_total'], 2) ?><span
                                                                            class="small text-muted">
                                                                            <?= !empty($this->lang->line('via')) ? $this->lang->line('via') : 'via' ?>
                                                                            (
                                                                            <?= $order['payment_method'] ?>)
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                <td colspan="3">
                                                                    <?php
                                                                    if($order['active_status'] !='cancelled' && $order['active_status'] !='returned' 
                                                                   && $order['active_status'] !='awaiting'
                                                                    && $order['active_status'] !='payment_cancelled'
                                                                    && $order['active_status'] !='payment_failed'){?><a target="_blank"
                                                                        href="<?= base_url('my-account/order-invoice/' . $order['id']) ?>"
                                                                        class='btn btn-primary btn-sm '
                                                                        style="color: #ffffff;">
                                                                        <?= !empty($this->lang->line('invoice')) ? $this->lang->line('invoice') : 'Invoice' ?>
                                                                    </a>
                                                                   <?php } ?>
                                                                    <?php if ($order['is_cancelable'] && !$order['is_already_cancelled'] && ($order['active_status']=='received' || $order['active_status']=='processed')) { ?>

                                                                    <a class="btn btn-danger update-order  btn-sm"
                                                                        style="color:#fff;" data-status="cancelled"
                                                                        data-order-id="<?= $order['id'] ?>">
                                                                        <?= !empty($this->lang->line('cancel')) ? $this->lang->line('cancel') : 'Cancel' ?>
                                                                    </a>

                                                                    <?php } ?>
                                                                    <?php if ($order['is_returnable'] && !$order['is_already_returned'] && $order['active_status']=='delivered') { ?>

                                                                    <!-- <a class="btn btn-warning update-order  btn-sm update-order "
                                                                        style="color:#fff;" data-status="returned"
                                                                        data-order-id="<?= $order['id'] ?>">
                                                                        <?= !empty($this->lang->line('return')) ? $this->lang->line('return') : 'Return' ?>
                                                                    </a> -->

                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- /.col -->
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12 learts-mb-30">
                                                <?php $address = json_decode($order['address']);
                                                   ?>
                                                <div class="oderadd">
                                                    <h4 class="title">
                                                        <?= !empty($this->lang->line('billing_address')) ? $this->lang->line('billing_address') : 'Billing Address' ?>
                                                    </h4>
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('name')) ? $this->lang->line('name') : 'Name' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= $address->name ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('email')) ? $this->lang->line('email') : 'Email' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= $address->email ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('mobile')) ? $this->lang->line('mobile') : 'Mobile' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= $address->mobile ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('address_type')) ? $this->lang->line('address_type') : 'Address Type' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= $address->type ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('house_no')) ? $this->lang->line('house_no') : 'House No/ Flat No ' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= $address->house ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('avenue')) ? $this->lang->line('avenue') : 'Avenue / Building' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= $address->avenue ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('street_no')) ? $this->lang->line('street_no') : 'Street' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= $address->street ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('block')) ? $this->lang->line('block') : 'Block' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= $address->block ?>
                                                                </td>
                                                            </tr>
                                                           
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('area')) ? $this->lang->line('area') : 'Area' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= $address->area ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <?= !empty($this->lang->line('city')) ? $this->lang->line('city') : 'City' ?>
                                                                </th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?= $address->city ?>
                                                                </td>
                                                            </tr>


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row  order_details">
                                            
                                            <div class="table-responsive col-md-12">
                                                <table class="table table-borderless shopping-cart-table">
                                                    <thead>
                                                        <tr>
                                                        <th><?= !empty($this->lang->line('image')) ? $this->lang->line('image') : 'Image' ?></th>
                                        <th><?= !empty($this->lang->line('product')) ? $this->lang->line('product') : 'Product' ?></th>
                                        <th><?= !empty($this->lang->line('price')) ? $this->lang->line('price') : 'Price' ?></th>
                                        <th><?= !empty($this->lang->line('quantity')) ? $this->lang->line('quantity') : 'Quantity' ?></th>
                                        <th><?= !empty($this->lang->line('subtotal')) ? $this->lang->line('subtotal') : 'Subtotal' ?></th>
                                                            <th><?= !empty($this->lang->line('status')) ? $this->lang->line('status') : 'Status' ?></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php foreach ($order['order_items'] as $key => $item) { ?>
                                                        <tr>
                                                            <td>
                                                                <div class="product-image">
                                                                    <img class="align-self-center img-fluid"
                                                                        src="<?= $item['image_sm'] ?>" width="180 "
                                                                        height="180">
                                                                </div>
                                                            </td>
                                                            <td><a href="#" class="product-name">
                                                                    <?= $item[$lang_prefix.'name'] ?>
                                                                
                                               <?= $item[$lang_prefix.'variant_name'] ?></a>
                                                            </td>
                                                            <td><label><?= !empty($this->lang->line('price')) ? $this->lang->line('price') : 'Price' ?>:</label>

                                                                <?php if($item['special_price'] >0) 
                                                echo '<span class="product-price">'. $settings['currency'].' '.number_format($item['special_price'],2).'</span><del>'. $settings['currency'].' '.number_format($item['price'],2).'</del>';
                                                else
                                                echo '<span class="product-price">'. $settings['currency'].' '. number_format($item['price'],2).'</span>';
                                            ?>
                                                            </td>
                                                            <td><label><?= !empty($this->lang->line('quantity')) ? $this->lang->line('quantity') : 'Quantity' ?>:</label>
                                                                <span class="product-price">
                                                                    <?= $item['quantity'] ?>
                                                                </span>
                                                            </td>
                                                            <td><label><?= !empty($this->lang->line('subtotal')) ? $this->lang->line('subtotal') : 'Subtotal' ?>:</label> <span class="product-price">
                                                                    <?= $settings['currency'].' '.number_format($item['sub_total'],2) ?>
                                                                </span></td>
                                                            <td><label><?= !empty($this->lang->line('status')) ? $this->lang->line('status') : 'Status' ?>:</label>
                                                                <ul>
                                                                    <?php
                                            $status = array('awaiting','received', 'processed', 'shipped', 'delivered','payment_failed','payment_cancelled');
                                            $i = 1;
                                            foreach ($item['status'] as $value) { ?>
                                                                    <?php
                                            
                                                ?>
                                                                    <li class="active" id="step<?= $i ?>">
                                                                        <strong>
                                                                            <?= strtoupper($value[0]) ?>
                                                                        </strong>
                                                                        <small> <?= !empty($this->lang->line('at')) ? $this->lang->line('at') : 'at' ?> 
                                                                            <?= $value[1] ?>
                                            </small>
                                                                    </li>
                                                                    <?php
                                                $i++;
                                            } ?>
                                                            </td>
                                                            <td>
                                                                <?php if (!$item['is_already_cancelled'] && $item['is_cancelable'] && ($item['active_status']=='received' || $item['active_status']=='processed')) { ?>
                                                                <button
                                                                    class="btn btn-danger btn-sm update-order-item"
                                                                    data-status="cancelled"
                                                                    data-item-id="<?= $item['id'] ?>">
                                                                    <?= !empty($this->lang->line('cancel')) ? $this->lang->line('cancel') : 'Cancel' ?>
                                                                </button><br>
                                                                <?php } ?>
                                                                <?php if (!$item['is_already_cancelled'] && !$item['is_already_returned'] && $item['is_returnable'] &&($item['active_status']=='delived' )) { ?>
                                                                <!-- <button
                                                                    class="btn btn-warning btn-sm   update-order-item"
                                                                    data-status="returned"
                                                                    data-item-id="<?= $item['id'] ?>">
                                                                    <?= !empty($this->lang->line('return')) ? $this->lang->line('return') : 'Return' ?>
                                                                </button> -->
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>





                    </div>
                </div>


            </div>



        </div>

    </section>


</div>