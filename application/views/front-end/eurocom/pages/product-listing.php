<!-- breadcrumb -->
<div class="container">
		
		<div class="page_name">
			<div class="row">
		 <div class="col-md-6 ">	
		<h3><?= ($lang_prefix=='') ? strtoupper($single_category['name']) : $single_category[$lang_prefix.'name']
         ?></h3>
			</div>
			 <div class="col-md-6 ">	
        <div class="breadcrumb">			
            <ul class="list-inline">
                <li><a href="<?= base_url() ?>"><?= !empty($this->lang->line('home')) ? $this->lang->line('home') : 'Home' ?></a></li>
                <!-- <li><a href="#">Accessories</a></li> -->
                <li class="active"><?=  ($lang_prefix=='') ? ucfirst(strtolower($single_category['name'])) : $single_category[$lang_prefix.'name']?></li>
            </ul>
        </div>
		</div>		 
		</div>
			</div>
		
		
		
    </div>	
    <div id="content">
        <div class="content-shop left-sidebar">
            <div class="container">
                <div class="row flex-row-reverse">
                    <div class="product-search-right main-content">
                        <div class="main-content-shop">

<?php //print_r($products['product']) ?>
                        <form class="" action="#">
                            <!-- End List Shop Cat -->
                            <div class="shop-tab-product">
                                <div class="shop-tab-title">
                                    <h2><?= ($lang_prefix=='') ? strtoupper($single_category['name']) : $single_category[$lang_prefix.'name'] ?>
                                    </h2>

                                    <div class="sorting-bar content-right">
                                        <ul class="shop-tab-select nav nav-pills">
                                            <li class="active">
                                                <a href="#product-grid" class="grid-tab" data-toggle="tab"></a>
                                            </li>
                                            <li>
                                                <a href="#product-list" class="list-tab" data-toggle="tab"></a>
                                            </li>
                                        </ul>

                                        <div class="toolbox-item toolbox-sort">
                                            <label><?= !empty($this->lang->line('sort_by')) ? $this->lang->line('sort_by') : 'Sort By' ?>:</label>
                                            <div class="select-custom">
                                                <select name="orderby" class="form-control" id="sortdropdown"
                                                onchange="sort_product(this.value)">
                                            <option value="" <?php if($filter_sort =='') echo 'selected'; ?> ><?= !empty($this->lang->line('default_sorting')) ? $this->lang->line('default_sorting') : 'Default sorting' ?></option>
                                            <option value="top-rated" <?php if($filter_sort =='top-rated') echo 'selected'; ?>><?= !empty($this->lang->line('sort_popularity')) ? $this->lang->line('sort_popularity') : 'Sort by popularity' ?></option>
                                          
                                            <option value="date-desc" <?php if($filter_sort =='date-desc') echo 'selected'; ?>><?= !empty($this->lang->line('sort_newness')) ? $this->lang->line('sort_newness') : 'Sort by newness' ?></option>
                                            <option value="price-asc" <?php if($filter_sort =='price-asc') echo 'selected'; ?>><?= !empty($this->lang->line('sort_price_low_to_high')) ? $this->lang->line('sort_price_low_to_high') : 'Sort by price: low to high' ?></option>
                                            <option value="price-desc" <?php if($filter_sort =='price-desc') echo 'selected'; ?>><?= !empty($this->lang->line('sort_price_hight_to_low')) ? $this->lang->line('sort_price_hight_to_low') : 'Sort by price: high to low' ?></option>
                                            </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    
                                    <div role="tabpanel" class="tab-pane fade in active" id="product-grid">

                                        <div class="grid-view-products">
                                            
                                        <?php 
                                        if (isset($products) && !empty($products['product'])) { ?>
					                     <?php foreach ($products['product'] as $each){
                                       
                                    $product_name = $each[$lang_prefix.'name'];
                                    $product_slug =  '';
                                    $main_image =$each['image'];
                                    $product_id = $each['id'];
                                    $m = $lang_prefix.'variant_values'; 
                                  
                                    $product_stock = $each['stock'];
                            
                                        $product_slug =  base_url('products/details/' . $each['slug']);
                                        if($each['special_price'] > 0 && $each['special_price'] < $each['price']){
                                            $discount_in_percentage = find_discount_in_percentage($each['special_price'], $each['price']);
                                        }
                                        else{
                                            $discount_in_percentage = 0;
                                        }
                                        if($each['type'] =='variable_product'){
                                            if($each['stock_type']==NULL){
                                                if($product_stock!=null && intval($product_stock) <=0)
                                                $stockupdate=1;
                                               
                                               else{
                                                   $stockupdate=0;
                                               }
                                            }
                                            elseif($each['stock_type']=='2'){
                                                if($each['variant_stock']!=NULL && intval($each['variant_stock']) <=0){
                                                    $stockupdate=1;
                                                }
                                                else{
                                                    $stockupdate=0;
                                                }
                                            }
                                            
                                            
                                        }
                                        else
                                        {
                                            if($each['stock_type'] ==NULL){
                                          
                                                $stockupdate=0;
                                            }
                                            else{
                                                if($each['stock']!=NULL && intval($each['stock']) <=0){
                                                    $stockupdate=1;
                                                }
                                                else{
                                                    $stockupdate=0;
                                                }
                                            }
                                        } 
                                       
                                        $image = json_decode($each['variant_images']);
                                        if(isset($image[0]) && $image[0] !=''){                                           
                                           $sm_image = get_image_url($image[0],'thumb', 'sm');
                                        }
                                        else{
                                            $sm_image =get_image_url($main_image,'thumb', 'sm');
                                        }
                                        $variant_id =$each['variant_id'] ;
                                        $modal = "";
                                        $class="add-to-cart";
                                        if ($is_logged_in) {
                                            $is_favorite = is_exist(['user_id' => $user->id,
                                            'product_id' => $product_id,
                                            'product_variant_id' => $variant_id],'favorites');
                                            if($is_favorite)
                                                $is_favorite =1;
                                            else
                                            $is_favorite =0;
                                            }
                                            else
                                            $is_favorite =0;
                                        $product_slug .='/'.$variant_id;
                                        ?>

                                                    <div class="col item-display-grid">
                                                    <div class="item">
                                                     <div class="item-hot-deal-product">
                                                       <div class="hot-deal-product-thumb">
                                                          
                                                       <?php if($stockupdate==1) { ?>
                                                                   <div class="outofstock">
                                                       <strong> <?= !empty($this->lang->line('out_of_stock')) ? $this->lang->line('out_of_stock') : 'Out of Stock' ?></strong>
                                                   </div>
                                      <?php }
                                     if($discount_in_percentage!='' && $discount_in_percentage>0 && $discount_in_percentage < 100){
                                          ?>
                                          <div class="cat-hover-percent">
                                                                       <strong><?= $discount_in_percentage ?>% </strong>
                                                                       <!--<span>+10% for Member</span>-->
                                                                   </div>
                                         
                                          <?php
                                      } ?>
                                                   <!-- <div class="cat-hover-percent">
                                                                <strong>10%</strong>
                                                               
                                                            </div> -->
                                                            <div class="product-thumb">
                                                   <a class="product-thumb-link" href="<?= $product_slug ?>">
                                                       <img alt="" src="<?= $sm_image ?>"
                                                           class="first-thumb">
           
                                                   </a>
                                                   <div class="product2-buttons">
                                                       <a href="<?= base_url('products/product_quickview/'.$product_id.'/'.$variant_id) ?>"
                                                           class="product-button hintT-top manual-ajax"
                                                           rel="modal:open"  data-hint="<?= $this->lang->line('quick_view')?>" ><i
                                                               class="fa fa-search"></i></a>
                                                               
                                                               <a href="#"  data-hint="<?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'Add to Cart' ?>" class="<?php echo $class; ?> product-button hintT-top" data-product-id="<?= $product_id ?>"  data-product-qty="1" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>"  ><i class="fa fa-shopping-cart"></i></a>
                                                        
                                                       
                                                       <a href="javascript:void(0);"  data-product-id="<?=  $product_id ?>" data-product-variant-id="<?= $variant_id ?>"   class="product-button hintT-top wishlist add-to-fav-btn lnr " data-hint="<?= $this->lang->line('add_to_wishlist')?>" ><i class="fa fa-heart <?= ( $is_favorite == 1) ? ' text-danger' : '' ?>"></i></a>
                                                     
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="hot-deal-product-info">
                                               <h3 class="title-product"><a href="<?= $product_slug ?>"><?= $product_name ?> <?php echo $each[$m]; ?></a></h3>
                                               <div class="info-price">
                                             
                                            
                                                           <span><?php 
                                                         
                                                               if($each['special_price'] > 0 && $each['special_price']< $each['price']){?>
                                                                   <span ><?= $settings['currency'] ?> <?= number_format($each['special_price'],3) ?></span><del><?= $settings['currency'] ?> <?= number_format($each['price'],3) ?></del>
       
                                                               <?php }
                                                               else{
                                                                   ?>
                                                                       <span ><?= $settings['currency'] ?> <?= number_format($each['price'],3) ?></span>	 
                                                              <?php } ?>
                                                          </span>
                                                          
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                                </div>
                                       <?php     
                                           

                                              
                                               
                                       } }?>
                                       



                                        </div>
                                        <!--grid end-->
                                        <nav class="text-center mt-4 ">
                        <?php
                      echo  (isset($links)) ? $links : '';
                       /*  if(isset($products['product']) && count($products['product']) < $total_count) {?>
              <h1 class="load-more-grid"><?= $this->lang->line('load_more') ?></h1>
            <input type="hidden" id="row-grid" value="<?php echo count($products['product']); ?>">
            <input type="hidden" id="all-grid" value="<?php echo $total_count; ?>">
				<?php } */
                ?>
 					
                    </nav>

                                        <!-- End Sort Pagibar -->
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="product-list">

                                        <div class="list-view-products">

                                        <?php if (isset($products) && !empty($products['product'])) { ?>
					                    <?php foreach ($products['product'] as  $each){
                               
                                    $product_name = $each[$lang_prefix.'name'];
                                    $product_slug =  '';
                                    $main_image =$each['image'];
                                    $product_id = $each['id'];
                                    $m = $lang_prefix.'variant_values';                                    
                                    $product_stock = $each['stock'];                                
                                        $product_slug =  base_url('products/details/' . $each['slug']);
                                        if($each['special_price'] > 0 && $each['special_price'] < $each['price']){
                                            $discount_in_percentage = find_discount_in_percentage($each['special_price'], $each['price']);
                                        }
                                        else{
                                            $discount_in_percentage = 0;
                                        }
                                        if($each['type'] =='variable_product'){
                                            if($each['stock_type']==NULL){
                                                if($product_stock!=null && intval($product_stock) <=0)
                                                $stockupdate=1;
                                               
                                               else{
                                                   $stockupdate=0;
                                               }
                                            }
                                            elseif($each['stock_type']=='2'){
                                                if($each['variant_stock']!=NULL && intval($each['variant_stock']) <=0){
                                                    $stockupdate=1;
                                                }
                                                else{
                                                    $stockupdate=0;
                                                }
                                            }
                                            
                                            
                                        }
                                        else
                                        {
                                            if($each['stock_type'] ==NULL){
                                          
                                                $stockupdate=0;
                                            }
                                            else{
                                                if($each['stock']!=NULL && intval($each['stock']) <=0){
                                                    $stockupdate=1;
                                                }
                                                else{
                                                    $stockupdate=0;
                                                }
                                            }
                                        } 
                                       
                                        $image = json_decode($each['variant_images']);
                                        
                                        if(isset($image[0]) && $image[0] !=''){                                           
                                           $sm_image = get_image_url($image[0], 'thumb', 'sm');
                                        }
                                        else{
                                            $sm_image = get_image_url($main_image,'thumb', 'sm');
                                        }
                                        $variant_id =$each['variant_id'] ;
                                        $modal = "";
                                        $class="add-to-cart";
                                        if ($is_logged_in) {
                                            $is_favorite = is_exist(['user_id' => $user->id,
                                            'product_id' => $product_id,
                                            'product_variant_id' => $variant_id],'favorites');
                                            if($is_favorite)
                                                $is_favorite =1;
                                            else
                                            $is_favorite =0;
                                            }
                                            else
                                            $is_favorite =0;
                                        $product_slug .='/'.$variant_id;
                                        ?>
                                                        <div class="list-product-card item-display-list">
                                                        <div class="list-product-card-inner">
                                                        <?php if($stockupdate==1) { ?>
                                                            <div class="label_product">
                                                                    <span class="label_sale"><?= !empty($this->lang->line('out_of_stock')) ? $this->lang->line('out_of_stock') : 'Out of Stock' ?> </span>
                                                                </div>
                                                        <?php }  
                                                         if($discount_in_percentage!='' && $discount_in_percentage>0 && $discount_in_percentage < 100){
                                          ?>
                                          <div class="cat-hover-percent">
                                                                       <strong><?= $discount_in_percentage ?>% </strong>
                                                                       <!--<span>+10% for Member</span>-->
                                                                   </div>
                                         
                                          <?php
                                      } ?>
                                                            <div class="product-card-left">
                                                                <a href="<?= $product_slug ?>" class="product-image"> <img alt="<?= $product_name ?>" src="<?= $sm_image ?>" class="first-thumb">
                                                                    <ul class="list-inline product-badge">
                                                                        <!---->
                                                                        <!---->
                                                                    </ul>
                                                                </a>
                                                            </div>
                                                            <div class="product-card-right">
                                                                <a href="<?= $product_slug ?>" class="product-name">
                                                                    <h6><?= $product_name.' '.$each[$m] ?></h6>
                                                                </a>
                                                                <div class="clearfix"></div>
                                                                <div class="product-price">

                                                               <?php 
                                                         
                                                               if($each['special_price'] > 0 && $each['special_price']< $each['price']){?>
                                                                 <?= $settings['currency'] ?> <?= number_format($each['special_price'],3) ?><span class="previous-price"><?= $settings['currency'] ?> <?= number_format($each['price'],3) ?></span>
       
                                                               <?php }
                                                               else{
                                                                   ?>
                                                                     <?= $settings['currency'] ?> <?= number_format($v_each['price'],3) ?>
                                                              <?php } ?>
                                                        
                                                                
                                                              </div>
                                                             
                                                                <a href="#"  data-hint="<?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'Add to Cart' ?>" class="<?php echo $class; ?> product-button hintT-top btn btn-default btn-add-to-cart" data-product-id="<?=  $product_id ?>"  data-product-qty="1" data-product-variant-id="<?= $variant_id ?>" data-izimodal-open="<?= $modal ?>"  ><i class="fa fa-shopping-cart"></i> <?= !empty($this->lang->line('add_to_cart')) ? $this->lang->line('add_to_cart') : 'Add to Cart' ?></a>
                                                        
                                                                            
                                                                            <div class="product-card-actions">
                                                                            <button  data-product-id="<?=  $product_id ?>" data-product-variant-id="<?= $variant_id ?>"   class="btn btn-wishlist wishlist add-to-fav-btn" data-hint="<?= $this->lang->line('add_to_wishlist')?>"><i class="fa fa-heart  <?= ( $is_favorite == 1) ? ' text-danger' : '' ?>" ></i>
                                                                            <?= !empty($this->lang->line('wishlist')) ? $this->lang->line('wishlist') : 'Wishlist' ?>
                                                            </button>
        <br>
        
                                                              
                                                             
                                                            <a href="<?= base_url('products/product_quickview/'.$product_id.'/'.$variant_id) ?>"
                                                            class="product-button hintT-top manual-ajax"
                                                            rel="modal:open"  data-hint="<?= $this->lang->line('quick_view')?>"><i
                                                                class="fa fa-search"></i> <?= !empty($this->lang->line('quick_view')) ? $this->lang->line('quick_view') : 'Quick View' ?></a>
        
                                                        
        
        
                                                                </div>
                                                            </div>
                                                        </div>
        
        
        
        
        
        
                                                    </div>
                                                   
                                                    <?php  

}}
?>

                                       


                                        </div>
                                        <!--list-card-main-->

                                        <nav class="text-center mt-4 ">
                                            <?php
                                          echo  (isset($links)) ? $links : '' ;
/*
                                            if(isset($products['product']) && count($products['product']) < $total_count) {?> 	
                                             <h1 class="load-more-list"><?= $this->lang->line('load_more') ?></h1>
            <input type="hidden" id="row-list" value="<?php echo count($products['product']); ?>">
            <input type="hidden" id="all-list" value="<?php echo $total_count; ?>">
				<?php } */
                ?>
                                        </nav>

                                        
                                        
                                        <!-- End Sort Pagibar -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Shop Tab -->
                        </div>
                        <!-- End Main Content Shop -->
                    </div>

                    <div class="sidebar-overlay"></div>
                    <div class="sidebar-toggle"><i class="fas fa-sliders-h"></i></div>

                    <div class="product-search-left  sidebar mt-2 mobile-sidebar">
                        <div class="sidebar-shop sidebar-left d-none d-lg-block">
                            <div class="widget widget-filter">
                            <?php if (isset($brands) && !empty($brands)) { ?>
                                <div class="box-filter category-filter">
                                    <h2 class="widget-title"><?= !empty($this->lang->line('brand')) ? $this->lang->line('brand') : 'Brand' ?></h2>
                                   
                        
                                    <div class="filter-checkbox" style="max-height:600px; overflow-y:auto;" >
                                    <?php $uniq_brands =[];
                                    foreach ($brands as $row) { 
                                        if(!in_array($row['name'],$uniq_brands)) {?> 
                                        <div class="form-check"><input type="checkbox" class="brand_filter" name="brand" id="brand-<?= $row['id'] ?>" value="<?php echo $row['id'] ?>" <?php if(in_array($row['id'],$filter_brands)) echo 'checked';?>> <label for="brand-<?= $row['id'] ?>"><?= ($lang_prefix=='') ? strtoupper($row[$lang_prefix.'name']) : $row[$lang_prefix.'name'] ?></label></div>
                                        <?php $uniq_brands[] =$row['name'];
                                     }} ?>
                                
                                    </div>
                                   
                                </div>
                                <?php } ?>
                                <!-- End Category -->
                                <div class="box-filter price-filter">
                                    <h2 class="widget-title"><?= !empty($this->lang->line('price')) ? $this->lang->line('price') : 'Price' ?></h2>
                                    <div class="inner-price-filter">

                                        <div class="range-filter">
                                            <label><?= $settings['currency'] ?></label>
                                            <form action="#">
                                            <div id="amount"></div>
                                            <button class="btn-filter"><?= !empty($this->lang->line('filter')) ? $this->lang->line('filter') : 'Filter' ?></button>
                                            <div id="slider-range"></div>
                                            <input type="hidden" name="min_price" id="filter_min_price" value="<?= $filter_min_price ;?>"/>
                                            <input type="hidden" name="max_price" id="filter_max_price" value="<?= $filter_max_price ;?>"/>

                                </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Price -->
                                <!-- <div class=" sidebar_widget">
                                    <h2 class="widget-title"><?= !empty($this->lang->line('categories')) ? $this->lang->line('categories') : 'Categories' ?></h2>
                                    
                                    <div id="accordion" class="card__accordion">
							
						
                            <?php
                                                            $i=1;
                                $categories1 = $this->category_model->get_categories(null, 30);								
                                                        foreach ($categories1 as $row1) { 
                                                        $child=$row1['children'];
                                                        if(count($child)>0)
                                                        {
                                                        ?>
                                                    
                                                    <div class="card card_dipult">
                                                        <div class="card-header card_accor" id="heading<?php echo $i; ?>">
                                                            <button class="btn btn-link <?php if($i==1) { ?>collapsed<?php } ?>" data-toggle="collapse" data-target="#heading<?php echo $i; ?>" aria-expanded="true" aria-controls="heading<?php echo $i; ?>">
                                                               <h3><?= $row1[$lang_prefix.'name'] ?></h3>
                                                                <i class="fa fa-plus"></i>
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                            
                                                        <div id="heading<?php echo $i; ?>" class="collapse " aria-labelledby="heading<?php echo $i; ?>" data-parent="#accordion">
                                                            <div class="card-body">
                                                                                    <ul>
                                                                                    
                                                                         <?php foreach ($child as $row2) { ?>
                                                                            <li><a href="<?= base_url('products/category/' . $row2['slug']) ?>"><?= $row2[$lang_prefix.'name'] ?></a></li>
                                                                         <?php } ?> 
                                                                                        
                                                                                        
                                                                                    </ul></div>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    
                                                    
                                                <?php
                                                        }
                                                        
                                                          else
                                                        {
                                                            ?>
                                                            <div class="card card_dipult">
                                                        <div class="card-header card_accor" >
                                                            <a class="btn btn-link" href="<?= base_url('products/category/' . $row1['slug']) ?>">
                                                               <h3><?= $row1[$lang_prefix.'name'] ?></h3>
                                                            </a>
                                                        </div>                            
                                                    </div>
                                                        <?php }
                            $i++;							
                                                        } 
                                                        ?>
                                                            
                                                    
                                                    
                                                
                                    </div>
                                    </div> -->
                                     
                                    <?php
                                    if($attributes['attributes']){
                                        foreach($attributes['attributes'] as $a_id =>$a_row){
                                            echo ' <div class="box-filter color-filter">
                                            <h2 class="widget-title">'.$a_row.'</h2>
                                            <div class="filter-checkbox">';
                                         
                                            foreach($attributes['attribute_values'][$a_id] as $av_row){?>
                                                <div class="form-check"><input type="checkbox" class="attribute_filter" name="attributes" id="attribute-<?= $av_row['attribute_value_id'] ?>" value="<?= $av_row['attribute_value_id'] ?>" <?php if(in_array($av_row['attribute_value_id'],$filter_attributes)) echo 'checked';?>> <label for="attribute-<?= $av_row['attribute_value_id'] ?>"><?= $av_row['attribute_value'] ?></label></div>
                                            <?php }?>

                                            </div>
                                </div> 
                                       <?php  }
                                    } ?>
                               
                                <!-- End Manufacturers -->
                            </div>
                            <!-- End Filter -->

                            <!-- End Vote -->

                            <!-- End Adv -->
                        </div>
                        <!-- End Sidebar Shop -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Content Shop -->
    </div>
    <!--breadcrumbs area end-->

  