<!DOCTYPE HTML>
<html lang='en-US' >
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?= $title ?></title>
    <meta name="keywords" content='<?= $keywords ?>'>
    <meta name="description" content='<?= $description ?>'>
    <?php $this->load->view('front-end/' . THEME . '/include-css'); ?>
</head>
<body >
<div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <div class="wrap" >
    <?php $this->load->view('front-end/' . THEME . '/header'); ?>
    <?php $this->load->view('front-end/' . THEME . '/pages/' . $main_page); ?>
    <?php $this->load->view('front-end/' . THEME . '/footer'); ?>
    </div>
    <?php $this->load->view('front-end/' . THEME . '/include-script'); ?>
</body>
</html>