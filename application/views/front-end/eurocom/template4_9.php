<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
    <meta name="keywords" content='<?= $keywords ?>'>
    <meta name="description" content='<?= $description ?>'>
    <?php $this->load->view('front-end/' . THEME . '/include-css'); ?>
</head>
<body onload="myFunction()" style="margin:0;" class="page-template">
<div id="loader"></div>
    <div class="wrap" style="display:none;" id="myDiv">
    <?php $this->load->view('front-end/' . THEME . '/header'); ?>
    <?php $this->load->view('front-end/' . THEME . '/pages/' . $main_page); ?>
    <?php $this->load->view('front-end/' . THEME . '/footer'); ?>
    </div>
    <?php $this->load->view('front-end/' . THEME . '/include-script'); ?>
</body>
</html>