<!-- Izimodal -->
<?php 
 $cookie_lang=($this->session->userdata('lang') !='') ? $this->session->userdata('lang', TRUE) : 'english' ;
 ?>
<!-- Izimodal -->
<?php $favicon = get_settings('web_favicon'); ?>
<link rel="shortcut icon" type="image/x-icon" href="<?=base_url($favicon)?>" >
<link rel="stylesheet" href="https://use.fontawesome.com/4d43ecb3b5.css">
<?php
if( $cookie_lang == 'english'){?>
   <link rel="preload" href="<?= THEME_ASSETS_URL ;?>css/font-main.css" as="style" 
onload="this.rel='stylesheet'"><noscript>
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/font-main.css"></noscript> 
    <link rel="preload" href="<?= THEME_ASSETS_URL ;?>css/bootstrap.min.css" as="style" 
onload="this.rel='stylesheet'"><noscript>
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/bootstrap.min.css"></noscript> 
 
 <link rel="preload" href="<?= THEME_ASSETS_URL ;?>css/plugins.css" as="style" 
onload="this.rel='stylesheet'"><noscript>
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/plugins.css"></noscript> 
    
    <link rel="preload" href="<?= THEME_ASSETS_URL ;?>js/slideshow/settings.css" as="style" 
onload="this.rel='stylesheet'"><noscript>
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>js/slideshow/settings.css"></noscript> 
<?php } 
else{ ?>
   <link rel="preload" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/css/font-main.css" as="style" 
onload="this.rel='stylesheet'"><noscript>
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/css/font-main.css"></noscript> 

    <link rel="preload" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/css/bootstrap.min.css" as="style" 
onload="this.rel='stylesheet'"><noscript>
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/css/bootstrap.min.css"></noscript> 
 
 <link rel="preload" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/css/plugins.css" as="style" 
onload="this.rel='stylesheet'"><noscript>
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/css/plugins.css"></noscript> 
    
    <link rel="preload" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/js/slideshow/settings.css" as="style" 
onload="this.rel='stylesheet'"><noscript>
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/js/slideshow/settings.css"></noscript> 
<?php } ?>
    
    <?php
if( $cookie_lang == 'english'){?>

    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/theme.css">

    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/mixed.css">
    <link rel="preload" href="<?= THEME_ASSETS_URL ;?>css/magiczoomplus.css" as="style" 
onload="this.rel='stylesheet'"><noscript>
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/magiczoomplus.css"></noscript>
 
     
<link rel="preload" href="<?= THEME_ASSETS_URL ;?>css/responsive.css" as="style" 
onload="this.rel='stylesheet'"><noscript><link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/responsive.css"></noscript>
    <?php } 
    else{ ?>
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/css/theme.css">

    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/css/mixed.css">
    <link rel="preload" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/css/magiczoomplus.css" as="style" 
onload="this.rel='stylesheet'"><noscript>
    <link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/css/magiczoomplus.css"></noscript>
 
<link rel="preload" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/css/responsive.css" as="style" 
onload="this.rel='stylesheet'"><noscript><link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?><?= $cookie_lang ?>/css/responsive.css"></noscript>
<?php } ?>
<link rel="preload" href="<?= THEME_ASSETS_URL ;?>css/sweetalert2.min.css" as="style" 
onload="this.rel='stylesheet'"><noscript><link rel="stylesheet" href="<?= THEME_ASSETS_URL ;?>css/sweetalert2.min.css"></noscript>
 
