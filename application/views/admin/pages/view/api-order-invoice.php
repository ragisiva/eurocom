
<section class="content" <?php
if($lang_prefix =='') echo "style=''"; else echo "style='direction:rtl'" ;?>>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info " id="section-to-print">
                    <div class="row m-3">
                        <div class="col-md-12 d-flex justify-content-between">
                            <h2 class="text-left">
                                <img src="<?= base_url()  . get_settings('logo') ?>" class="d-block " style="max-width:250px;max-height:100px;">
                            </h2>
                            <h2 class="text-right">
                               <?php echo $this->lang->line('mobile') ?>:  <?= $settings['support_number'] ?>
                            </h2>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row m-3 d-flex justify-content-between">
                        <div class="col-sm-4 invoice-col"> <?php echo $this->lang->line('from') ?><address>
                                <strong><?= $settings['app_name'] ?></strong><br>
                                <?php echo $this->lang->line('email') ?>: <?= $settings['support_email'] ?><br>
                                <?php echo $this->lang->line('mobile') ?>: <?= $settings['support_number'] ?><br>
                                <?php if (isset($settings['tax_name']) && !empty($settings['tax_name'])) { ?>
                                    <b><?= $settings['tax_name'] ?></b> : <?= $settings['tax_number'] ?><br>
                                <?php } ?>
                                <?php if (!empty($items[0]['delivery_boy'])) { ?>Delivery By: <?= $items[0]['delivery_boy'] ?><?php } ?>
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col"> <?php echo $this->lang->line('to') ?> <address>
                            <?php
                            $address = json_decode($order_detls[0]['address']);
                            $invoice_address ='';
                            $invoice_address .= ($address->house !='') ? $address->house.', ' : '';
                            $invoice_address .= ($address->avenue !='') ? $address->avenue.', ' : '';
                            $invoice_address .= ($address->block !='') ? $address->block.', ' : '';
                            $invoice_address .= ($address->street !='') ? $address->street.', ' : '';
                            $invoice_address .= ($address->area !='') ? $address->area.', ' : '';
                            $invoice_address .= ($address->city !='') ? $address->city.', ' : '';
                            ?>
                                <strong><?= $address->name ?></strong><br>
                                <?=  $address->type ?><br>
                                <?=  $invoice_address ?><br>
                                <?= $address->mobile ?><br>
                                <?= $address->email ?><br>
                            </address>
                        </div>
                        <!-- /.col -->
                        <?php if (!empty($order_detls[0]['id'])) { ?>
                            <div class="col-sm-4 invoice-col text-right">
                                <br> <b><?php echo $this->lang->line('retail_invoice') ?></b>
                                <br> <b><?php echo $this->lang->line('no') ?> : </b>#<?= $order_detls[0]['id'] ?>
                                <br> <b><?php echo $this->lang->line('date') ?>: </b><?= date('d-m-Y h:i A', strtotime($order_detls[0]['date_added'])) ?>
                                <br>
                            </div>
                        <?php } ?>
                    </div>
                    <!-- /.row -->
                    <!-- Table row -->
                    <div class="row m-3">
                        <div class="col-xs-12 table-responsive">
                            <table class="table borderless text-center text-sm">
                                <thead class="">
                                    <tr>
                                        <th><?= $this->lang->line('sl_no') ?></th>
                                        <th><?= $this->lang->line('product_code') ?></th>
                                        <th><?= $this->lang->line('name') ?></th>
                                        <th><?= $this->lang->line('price') ?></th>
                                        <!-- <th>Tax (%)</th> -->
                                        <th><?= $this->lang->line('quantity') ?></th>
                                        <!-- <th>Tax Amount (<?= $settings['currency'] ?>)</th> -->
                                        <th><?= $this->lang->line('sub_total') ?> (<?= $settings['currency'] ?>)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    $total = $quantity = $total_tax = $total_discount = 0;

                                    foreach ($order_detls[0]['order_items'] as $row) {
                                        $price = 0;
                                        if($row['special_price'] >0)
                                            $price =$row['special_price'];
                                        else
                                            $price =$row['price'];
                                        $total += floatval($price) * floatval($row['quantity']);
                                        $quantity += floatval($row['quantity']);
                                        $total_tax += floatval($row['tax_amount']); ?>
                                        <tr>
                                            <td>
                                                <?= $i ?>
                                                <br>
                                            </td>
                                            <td>
                                                <?= $row['product_variant_id'] ?><br>
                                            </td>
                                            <td class="w-25">
                                                <?= $row[$lang_prefix.'name'] ?><?= $row[$lang_prefix.'variant_name'] ?>
                                                
                                            </td>
                                            <td>
                                                <?= $settings['currency'] . ' ' . number_format($price, 2) ?>
                                                <br>
                                            </td>

                                            <!-- <td>
                                                <?= ($row['tax_percent']) ? $row['tax_percent'] : '0' ?>
                                                <br>
                                            </td> -->
                                            <td>
                                                <?= $row['quantity'] ?>
                                                <br>
                                            </td>
                                            <!-- <td>
                                                <?= ($row['tax_amount']) ? $settings['currency'] . ' ' . number_format($row['tax_amount'], 2) : '0' ?>
                                                <br>
                                            </td> -->
                                            <td>
                                                <?= $settings['currency'] . ' ' . number_format(floatval($price) * $row['quantity'], 2) ?>
                                                <br>
                                            </td>
                                        </tr>
                                    <?php $i++;
                                    } ?>
                                </tbody>
                                <tbody>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <!-- <th></th> -->
                                        <th><?= $this->lang->line('total') ?></th>
                                        <th>
                                            <?= $quantity ?><br>
                                        </th>
                                        <!-- <th>
                                            <?= $settings['currency'] . ' ' . number_format($total_tax, 2) ?>
                                        </th> -->
                                        <th>
                                            <?= $settings['currency'] . ' ' . number_format($total, 2) ?>
                                            <br>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <div class="row m-2 text-right">
                        <!-- accepted payments column -->
                        <div class="col-md-9 offset-md-2">
                            <!--<p class="lead">Payment Date: </p>-->
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th><?= $this->lang->line('total_order_price') ?></th>
                                            <td>+
                                                <?= $settings['currency'] . ' ' . number_format($total, 2) ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><?= $this->lang->line('delivery_charge') ?></th>
                                            <td>+
                                                <?php $total += $order_detls[0]['delivery_charge'];
                                                echo $settings['currency'] . ' ' . number_format($order_detls[0]['delivery_charge'], 2); ?>
                                            </td>
                                        </tr>
                                        <!-- <tr>
                                            <th>Tax - (<?= $items[0]['tax_percent'] ?>%)</th>
                                            <td>+
                                                <?php $total += $total_tax;
                                                echo $settings['currency'] . ' ' . number_format($total_tax, 2); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Wallet Used</th>
                                            <td>-
                                                <?php $total -= $order_detls[0]['wallet_balance'];
                                                echo  $settings['currency'] . ' ' . number_format($order_detls[0]['wallet_balance'], 2); ?>
                                            </td>
                                        </tr> -->
                                        <?php
                                        if (isset($promo_code[0]['promo_code'])) { ?>
                                            <tr>
                                                <th> <?= !empty($this->lang->line('promocode_discounts')) ? $this->lang->line('promocode_discounts') : 'Promocode Discount' ?>
                                                                        - (
                                                                            <?= $promo_code[0]['promo_code'] ?>-  <?= floatval($promo_code[0]['discount']); ?>
                                                    <?= ($promo_code[0]['discount_type'] == 'percentage') ? '%' : ' '; ?>)
                                                                      
                                                </th>
                                                <td>-
                                                    <?php
                                                    echo $settings['currency'] . ' ' .number_format($order_detls[0]['promo_discount'],2);
                                                    $total = $total - $order_detls[0]['promo_discount'];
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <?php
                                        if (isset($order_detls[0]['discount']) && $order_detls[0]['discount'] > 0 && $order_detls[0]['discount'] != NULL) { ?>
                                            <tr>
                                                <th>Special Discount
                                                    <?= $settings['currency'] ?>(<?= $order_detls[0]['discount'] ?> %)</th>
                                                <td>-
                                                    <?php echo $special_discount = round($total * $order_detls[0]['discount'] / 100, 2);
                                                    $total = floatval($total - $special_discount);
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        <tr>
                                            <th><?= $this->lang->line('total_payable') ?></th>
                                            <td>
                                                <?= $settings['currency'] . '  ' . number_format($total, 2) ?>
                                            </td>
                                        </tr>
                                      
                                        <tr>
                                            <th><?= $this->lang->line('final_total') ?></th>
                                            <td>
                                                <?= $settings['currency'] . '  ' . number_format($order_detls[0]['final_total'], 2) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
                <!--/.card-->
            </div>
            <!--/.col-md-12-->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <?php if (isset($print_btn_enabled) && $print_btn_enabled) { ?>
        <div class="col-12">
            <div class="text-center">
                <button class="btn btn-primary" onclick="window.print();"><?= $this->lang->line('print') ?></button>
            </div>
        </div>
    <?php } ?>
</section>