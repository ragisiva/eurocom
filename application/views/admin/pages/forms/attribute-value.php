<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h4>Attribute Value</h4>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('admin/home') ?>">Home</a></li>
            <li class="breadcrumb-item active">Attribute Value</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-info">
            <!-- form start -->
            <form class="form-horizontal form-submit-event" action="<?= base_url('admin/attribute_value/add_attribute_value'); ?>" method="POST" enctype="multipart/form-data">
              <div class="card-body">
                <?php if (isset($fetched_data[0]['id'])) { ?>
                  <input type="hidden" name="edit_attribute_value" value="<?= @$fetched_data[0]['id'] ?>">
                <?php  } ?>
                <div class="form-group row">
                  <label for="attributes" class="col-sm-2 col-form-label">Select Attributes <span class='text-danger text-sm'>*</span></label>
                  <div class="col-sm-10">
                    <select class="form-control" id="attributes" name="attributes_id">
                      <option value=""> None </option>
                      <?php foreach ($attributes as $row) {
                      ?>
                        <option value="<?= $row['id'] ?>" <?= (isset($fetched_data[0]['attribute_id']) && $fetched_data[0]['attribute_id'] == $row['id']) ? 'selected' : '' ?>> <?= $row['name'] ?>
                        </option>
                      <?php
                      } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="value" class="col-sm-2 col-form-label">Value <span class='text-danger text-sm'>*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="value" placeholder="value" name="value" value="<?= @$fetched_data[0]['value'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="value" class="col-sm-2 col-form-label">Arabic Value <span class='text-danger text-sm'>*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="ar_value" placeholder="Arabic value" name="ar_value" value="<?= @$fetched_data[0]['ar_value'] ?>">
                  </div>
                </div>
                <div class="form-group">
                  <button type="reset" class="btn btn-warning">Reset</button>
                  <button type="submit" class="btn btn-success" id="submit_btn"><?= (isset($fetched_data[0]['id'])) ? 'Update Attribute Value' : 'Add Attribute Value' ?></button>
                </div>
              </div>
              <div class="d-flex justify-content-center">
                <div class="form-group" id="error_box">
                </div>
              </div>
            </form>
          </div>
          <!--/.card-->
        </div>
        <!--/.col-md-12-->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>