<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['language_id'] = "2";
$lang['is_rtl'] = "1";
//header
$lang['call_us'] = "اتصل بنا";
$lang['home'] = "الصفحة الرئيسية";
$lang['contact_us'] = "اتصل بنا";
$lang['services'] = "خدمات";
$lang['careers'] = "وظائف";
$lang['about_us'] = "معلومات عنا";
$lang['language'] = "لغة";
$lang['search_placeholder'] = "أبحث عن...";
$lang['search_place_holder'] = "بحث...";
$lang['all'] = "الجميع";
$lang['login'] = "تسجيل الدخول";
$lang['register'] = "يسجل";
$lang['my_account'] = "حسابي";
$lang['order'] = "ترتيب";
$lang['checkout'] = "الدفع";
$lang['all_categories'] = "جميع الفئات";
$lang['sub_category'] = "تصنيف فرعي";
$lang['offers'] = "عروض";
$lang['hot_deals'] = "صفقات ساخنة";
$lang['brands'] = "العلامات التجارية";
$lang['to'] = "إلى";
$lang['from'] = "من عند";
$lang['retail_invoice'] = "فاتورة بيع بالتجزئة";
$lang['date'] = "تاريخ";
$lang['no'] = "لا";
$lang['sl_no'] = "لا";
$lang['product_code'] = "كود المنتج";
$lang['total_payable'] = "إجمالي المدفوعات";
$lang['print'] = "مطبعة";
//footer
$lang['phone'] = "هاتف";
$lang['email'] = "بريد الالكتروني";
$lang['facebook'] = "موقع التواصل الاجتماعي الفيسبوك";
$lang['instagram'] = "انستغرام";
$lang['information'] = "معلومة";
$lang['corporate_info'] = "معلومات الشركة";
$lang['subscribe_newsletter'] = "اشترك في النشرة الإخبارية";
$lang['subscribe'] = "الإشتراك";
$lang['email_address'] = "عنوان البريد الإلكتروني";
$lang['newletter_info'] = "احصل على أحدث المعلومات حول الأحداث والمبيعات والعروض. الاشتراك في النشرة الإخبارية:";
$lang['delivery_information'] = "معلومات التوصيل";
$lang['returns'] = "عائدات";
$lang['terms_conditions'] = "الشروط والأحكام";
$lang['privacy_policy'] = "سياسة خاصة";
$lang['new_arrival'] = "قادم جديد";
$lang['special_offer'] = "عرض خاص";
$lang['copy_right'] = "حقوق النشر © 2021 International Eurocom.";
$lang['all_right'] ="كل الحقوق محفوظة.";
$lang['powered_by'] = "Powered by Chrisans Web Solutions";
$lang['view_all'] = "مشاهدة الكل";

//cart
$lang['sub_total'] = "المجموع الفرعي";
$lang['subtotal'] = "المجموع الفرعي";
$lang['empty_cart_message'] = "عربة التسوق فارغة!!";
$lang['cart'] = "عربة التسوق";
$lang['login_warning'] = "لعرض قائمة الرغبات الخاصة بك الرجاء تسجيل الدخول";
$lang['wishlist'] = "قائمة الرغبات";
$lang['add_to_wishlist'] = "أضف إلى قائمة الامنيات";
$lang['view_cart'] = "عرض عربة التسوق";
$lang['add_to_cart'] = "أضف إلى السلة";
$lang['quick_view'] = "نظرة سريعة";
$lang['view_wishlist'] = "عرض قائمة الرغبات";
$lang['empty_wishlist_message'] = "قائمة رغباتك فارغة!!";
$lang['continue_shopping'] = "مواصلة التسوق";
$lang['image'] = "صورة";
$lang['product'] = "المنتج";
$lang['quantity'] = "كمية";
$lang['remove_from_cart'] = "إزالة من عربة التسوق";
$lang['apply_coupon'] = "قدم قسيمة";
$lang['order_summary'] = "ملخص الطلب";
$lang['delivery_charges'] = "رسوم التوصيل";
$lang['grand_total'] = "المبلغ الإجمالي";
$lang['proceed_to_checkout'] = "باشرالخروج من الفندق";
//common
$lang['out_of_stock'] = "إنتهى من المخزن";
$lang['in_stock'] = "في الأوراق المالية";
$lang['best_seller'] = "الأكثر مبيعا";
$lang['top_selling'] = "الأكثر مبيعا";
$lang['latest_product'] = "أحدث المنتجات";
$lang['recently_Viewed'] = "شوهدت مؤخرا";
//login 
$lang['password'] = "كلمه السر";
$lang['sign_in'] = "تسجيل الدخول";
$lang['dont_have_account']="ليس لديك حساب؟";
$lang['create_account']="إنشاء حساب";
//product_list_page
$lang['sort_by']="صنف حسب";
$lang['sort_price_hight_to_low']="الترتيب حسب السعر: من الأعلى إلى الأقل";
$lang['sort_price_low_to_high']="الترتيب حسب السعر: من الأقل إلى الأعلى";
$lang['sort_newness']="الترتيب حسب الحداثة";
$lang['sort_popularity']="الترتيب حسب الشعبية";
$lang['default_sorting']="الفرز الافتراضى";
$lang['brand']="ماركة";
$lang['price']="سعر";
$lang['filter']="منقي";
$lang['categories']="فئات";
//product details
$lang['product_details']="تفاصيل المنتج";
$lang['select']="يختار";
$lang['sku']="SKU";
$lang['tags']="العلامات";
$lang['share']="يشارك";
$lang['description']="وصف";
$lang['sizechart']="حجم الرسم البياني";
$lang['specification']="تحديد";
$lang['related_products']="منتجات ذات صله";

//checkout
$lang['billing_address']="عنوان وصول الفواتير";
$lang['add_new_address']="أضف عنوان جديد";
$lang['edit']="تعديل";
$lang['delivery_address']="عنوان التسليم";
$lang['payment']="قسط";
$lang['cash_on_delivery']="الدفع عند الاستلام";
$lang['cash_on_delivery_subtitle']="ادفع نقدا عند الاستلام.";
$lang['knet']="كي نت";
$lang['knet_subtitle']="الدفع عن طريق بطاقة كي نت.";
$lang['visa']="فيزا ماستركارد";
$lang['visa_subtitle']="ادفع عن طريق فيزا ، ماستر كارد.";
$lang['promo_code']="رمز ترويجي";
$lang['apply']="تطبيق";
$lang['clear']="واضح";
$lang['promocode_discounts']="خصم بروموكود";
$lang['terms_agree_text']="أنا أوافق على";
$lang['pay']="يدفع";
$lang['already_Registered']="مسجل بالفعل";
$lang['checkout_as_guest']="الخروج كضيف";

$lang['first_name']="الاسم الأول";
$lang['name']="اسم";
$lang['payment_method'] = "طريقة الدفع او السداد";
$lang['last_name']="الكنية";
$lang['mobile_number']="رقم الهاتف المحمول";
$lang['area']="منطقة";
$lang['select_area']="حدد المنطقة";
$lang['block']="حاجز";
$lang['street_no']="رقم الشارع";
$lang['street']="رقم الشارع";
$lang['avenue']="شارع / مبنى";
$lang['house_no']="رقم المنزل / رقم الشقة";
$lang['type']="نوع";
$lang['office']="مكتب";
$lang['other']="آخر";
$lang['make_default_address']="اجعل هذا العنوان افتراضيًا";
$lang['create_account_later']="انشئ حسابا للاستعمال لاحقا؟";
$lang['confirm_password']="تأكيد كلمة المرور";
$lang['another_address_for_invoice']="الرجاء استخدام عنوان آخر للتسليم";
$lang['back']="خلف";

$lang['my_address']="عنواني";
$lang['my_wishlist']="قائمة امنياتي";
$lang['delete']="حذف";
$lang['position_applied_for']="المنصب المتقدم لشغله بالطلب";
$lang['upload_resume']="تحميل استئناف";
$lang['send_message']="أرسل رسالة";
$lang['contact_title']="ترك رسالة";
$lang['subject']="موضوعات";
$lang['message']="رسالة";
$lang['availability']="التوفر";
$lang['action']="عمل";
$lang['submit']="يقدم";

//myaccount
$lang['dashboard']="لوحة القيادة";
$lang['my_order']="طلبي";
$lang['my_profile']="ملفي";
$lang['logout']="تسجيل خروج";


//my order
$lang['order_details']="تفاصيل الطلب";
$lang['back_to_list']="العودة للقائمة";
$lang['order_id']="رقم التعريف الخاص بالطلب";
$lang['order_date']="تاريخ الطلب";
$lang['applied_coupon']="القسيمة المطبقة";
$lang['status']="حالة";
$lang['delivery_date_time']="تاريخ ووقت التسليم";
$lang['total_order_price']="إجمالي سعر الطلب";
$lang['delivery_charge']="رسوم التوصيل";
$lang['tax']="ضريبة";
$lang['final_total']="المجموع النهائي";
$lang['via']="عبر";
$lang['invoice']="فاتورة";
$lang['cancel']="يلغي";
$lang['return'] = "يعود";
$lang['mobile'] = "متحرك";
$lang['address_type'] = "نوع العنوان";
$lang['city'] = "مدينة";
$lang['at'] = "في";
$lang['total'] = "المجموع";
$lang['date'] = "تاريخ";
$lang['view'] = "رأي";

$lang['payment_completed'] = "الدفع تم";
$lang['payment_completed_success'] = "اكتمل الدفع بنجاح..";
$lang['thankyou_for_shopping'] = "شكرا للتسوق معنا.";
$lang['payment_cancelled'] = "تم إلغاء / فشل الدفع";
$lang['payment_cancelled_message'] = "يبدو أن عملية الدفع فشلت أو ألغيت ، يرجى المحاولة مرة أخرى.";
$lang['try_again'] = "حاول مجددا";

$lang['menu'] = "قائمة طعام";
$lang['products'] = "منتجات";
// $lang['my_account'] = "My Account";
// $lang['my_orders'] = "My Orders";
$lang['favorite'] = "مفضل";



$lang['shopping_cart'] = "عربة التسوق";
$lang['close'] = "قريب";
$lang['return_to_shop'] = "العودة الى المتجر";
$lang['faq'] = "أسئلة وأجوبة";
$lang['pages'] = "الصفحات";
$lang['social_media'] = "وسائل التواصل الاجتماعي";

$lang['reviews'] = "المراجعات";

$lang['address'] = "عنوان";

$lang['select_city'] = "اختر مدينة";

$lang['pincode'] = "الرمز السري";
$lang['state'] = "ولاية";
$lang['country'] = "دولة";

$lang['alternate_mobile'] = "بديل موبايل";
$lang['landmark'] = "معلم معروف";

$lang['edit_address'] = "تعديل العنوان";

$lang['save_for_later'] = "احفظ لوقت لاحق";
$lang['remove'] = "يزيل";

$lang['move_to_cart'] = "انتقل الكرت";
$lang['category'] = "فئة";

$lang['your_cart'] = "عربتك";

$lang['redeem'] = "يسترد";

$lang['preferred_delivery_date_time'] = "تاريخ / وقت التسليم المفضل";
$lang['select_payment_method'] = "اختار طريقة الدفع";

$lang['create_a_new_address'] = "قم بإنشاء عنوان جديد";
$lang['shipping_address'] = "عنوان الشحن";
$lang['save'] = "يحفظ";
$lang['username'] = "اسم المستخدم";


$lang['profile'] = "الملف الشخصي";
$lang['orders'] = "الطلب ";
$lang['notification'] = "تنبيه";
$lang['wallet'] = "محفظة";
$lang['transaction'] = "عملية تجارية";
$lang['no_favorite_product_message'] = "لم يتم العثور على منتجات مفضلة";
$lang['amazing_categories'] = "فئات مذهلة";
$lang['view_more'] = "عرض المزيد";
$lang['mobile_app'] = "تطبيق الهاتف المحمول";

$lang['place_on'] = "ضع على";

$lang['shipping_details'] = "تفاصيل الشحن";

$lang['promocode_discount'] = "خصم بروموكود";
$lang['wallet_used'] = "المحفظة المستخدمة";

$lang['view_details'] = "عرض التفاصيل";

$lang['payment_completed_message'] = "اكتمل الدفع بنجاح";
$lang['thank_you_for_shopping_with_us'] = "شكرا للتسوق معنا";

$lang['top_rated'] = "أعلى التقييمات";
$lang['newest_first'] = "الأحدث أولاً";
$lang['oldest_first'] = "الأقدم أولا";
$lang['price_low_to_high'] = "السعر من الارخص للاعلى";
$lang['price_high_to_low'] = "السعر الاعلى الى الادنى";
$lang['relevance'] = "ملاءمة";
$lang['sale'] = "أوكازيون";

$lang['back_to_top'] = "عد إلى الأعلى";
$lang['go_to_shop'] = "اذهب إلى المتجر";
$lang['details'] = "تفاصيل";
$lang['remove_from_favorite'] = "إزالة من المفضلة";

$lang['rating'] = "تقييم";

$lang['old_password'] = "كلمة سر قديمة";
$lang['new_password'] = "كلمة مرور جديدة";
$lang['confirm_new_password'] = "تأكيد كلمة المرور الجديدة";
$lang['reset'] = "إعادة ضبط";
$lang['update_profile'] = "تحديث الملف";
$lang['transactions'] = "المعاملات";
$lang['newsletter'] = "النشرة الإخبارية";
$lang['useful_links'] = "روابط مفيدة";

$lang['follow_us'] = "تابعنا";
$lang['find_us'] = "تجدنا";

$lang['mail_us'] = "راسلنا بالبريد الإلكتروني";
$lang['forgot_password'] = "هل نسيت كلمة السر";
$lang['see_all'] = "اظهار الكل";

$lang['show'] = "تبين";
$lang['product_listing'] = "قائمة المنتجات";

$lang['product_added_to_favorite'] ='تمت إضافة المنتج إلى المفضلة';
$lang['max_allowed_quantity_is'] ='الكمية القصوى المسموح بها هي';
$lang['min_allowed_quantity_is'] ='الكمية الدنيا المسموح بها هي';
$lang['line_total'] ='مجموعه الخط';
$lang['see_all_results'] ='مشاهدة كل النتائج';
$lang['no_product_found'] ='لا يوجد منتج !!';
$lang['product_removed_from_favorite']='تمت إزالة المنتج من المفضلة!';
$lang['login_first_message']='قم بتسجيل الدخول أولاً لإضافة منتجات في قائمة المفضلة.';
$lang['product_variant']='متغير المنتج';
$lang['max_allowed_cart_item']='يمكن إضافة %s صنف كحد أقصى فقط !؛';
$lang['item_added_to_cart']='تمت إضافة العنصر إلى عربة التسوق.';
$lang['item_notremove_msg']='لا يمكن إزالة هذا العنصر من سلة التسوق.';
$lang['cart_already_empty']='عربة التسوق فارغة بالفعل!';
$lang['product_clear_msg']='المنتج مسح من سلة التسوق!';
$lang['cart_login_msg']='الرجاء تسجيل الدخول أولا لاستخدام عربة التسوق';
$lang['product_retrive_cart_msg']='المنتج المسترجع من سلة التسوق ...!';
$lang['order_place_success']= 'تم تقديم الطلب بنجاح';
$lang['our_brands'] = "العلامات التجارية";
$lang['terms_condition'] = "الشروط والأحكام";
$lang['new_arrivals'] = "قادم جديد";
$lang['promocode_usage_limit_exceed'] = 'لا يمكن استرداد هذا الرمز الترويجي لأنه يتجاوز حد الاستخدام';
$lang['promocode_applied'] ='تم تطبيق الرمز الترويجي.';
$lang['promocode_already_used'] ='تم بالفعل استرداد العرض الترويجي. لا يمكن إعادة استخدامها';
$lang['promocode_applied_amount_less'] ='{field} هذا الرمز الترويجي ينطبق فقط على المبلغ الأكبر من أو يساوي';
$lang['promocode_applied_user_limit_excees'] = "هذا الرمز الترويجي ينطبق فقط على مستخدمي{field} الأوائل";
$lang['promocode_expired'] = 'الرمز الترويجي غير متوفر أو منتهي الصلاحية';
$lang['cart_quantity_exceed_message']='تجاوزت كمية أحد المنتجات الحد المسموح به ، يرجى اقتطاع بعض الكمية من أجل شراء الصنف';
$lang['product_outofstock']="أحد المنتجات غير متوفر في المخزون.";
$lang['product_stock_avaliables']= "مخزون متاح للشراء.";
$lang['order_status_cant_change_message']= "لا يمكنك تحديث الحالة بمجرد إلغاء / إرجاع العنصر";
$lang['order_status_already_marked_not_change']= "تم بالفعل وضع علامة على الطلب على أنه٪s. لا يمكنك تعيينه مرة أخرى!";
$lang['order_status_cant_cancel_only_return_change']= "لا يمكنك إلغاء الطلب / المنتج الذي تم تسليمه أو إرجاعه. يمكنك فقط أن تعيد ذلك!";
$lang['order_status_cant_return_only_cancel_change']="لا يمكنك إرجاع طلب / عنصر لم يتم تسليمه. يجب أولاً وضع علامة على أنه تم تسليمه ومن ثم يمكنك إعادته!";
$lang['one_of_the_order_item_not_delivered']="لم يتم تسليم أحد عناصر الطلب بعد!";
$lang['order_item_not_delivered']="لم يتم تسليم عنصر الطلب بعد!";
$lang['one_ofthe_order_item_not_returned']="لا يمكن إرجاع أحد عناصر الطلب!";
$lang['order_item_not_returned']="لا يمكن إرجاع عنصر الطلب!";
$lang['one_of_the_order_item_can_canceled']="يمكن إلغاء أحد عناصر الطلب حتى%s فقط";
$lang['the_order_item_can_canceled']="يمكن إلغاء عنصر الطلب حتى%s فقط";
$lang['one_of_the_order_item_canot_canceled']="لا يمكن إلغاء أحد عناصر الطلب!";
$lang['the_order_item_canot_canceled']="لا يمكن إلغاء عنصر الطلب!";
$lang['return_request_already_submitted']="تم إرسال طلب الإرجاع بالفعل!";
$lang['return_request_submitted_success']="تم إرسال طلب الإرجاع بنجاح!";
$lang['invalid_status_message']="تم تجاوز حالة غير صالحة";
$lang['product_retrive_success']="تم استرداد المنتجات بنجاح";
$lang['product_not_found']="لم يتم العثور على المنتجات!";
$lang['join_our_team'] ='انضم إلى فريقنا';
$lang['incorrect_login'] ='تسجيل الدخول غير صحيح';
$lang['already_have_account'] ='هل لديك حساب؟';
$lang['reg_email_validation'] ='البريد الإلكتروني مسجل بالفعل. الرجاء تسجيل الدخول';
$lang['reg_mobile_validation'] = 'رقم الهاتف المحمول مسجل بالفعل. الرجاء تسجيل الدخول';
$lang['i_agree_to_the'] = 'أنا أوافق على';
$lang['comfirm_password_validation'] ='تأكيد كلمة المرور غير متطابقة';
$lang['mail_not_send_msg'] = 'لا يمكن إرسال البريد. الرجاء معاودة المحاولة في وقت لاحق.';
$lang['register_succcess_msg'] = 'مسجل بنجاح';
$lang['register_succcess_msg_checkout']='اكتمل تسجيلك بنجاح. تم إرسال بريد التأكيد إلى بريدك المسجل. يمكنك تسجيل الدخول مباشرة من هنا.';
$lang['reqired_field_validation'] = 'الحقل {field} مطلوب.';
$lang['valid_email_validation'] ='يجب أن يحتوي الحقل {field} على عنوان بريد إلكتروني صالح.';
$lang['min_length_validation'] = 'يجب ألا يقل طول الحقل {field} عن {param} من الأحرف.';
$lang['numeric_field_validation'] = 'The {field} field must be at least {param} characters in length.';
$lang['product_not_found'] = 'المنتج غير موجود';

$lang['forgot_password_email_send'] = "إرسال رابط إعادة تعيين كلمة المرور إلى بريدك الإلكتروني";
$lang['forgot_password_email_not_send'] = 'البريد الإلكتروني لا يرسل. حدث خطأ ما';
$lang['email_not_found'] ='البريد الإلكتروني غير موجود في النظام';

$lang['dashboard_content'] ='من لوحة تحكم حسابك. يمكنك بسهولة التحقق من طلباتك الأخيرة وعرضها ، وإدارة عناوين الشحن والفواتير الخاصة بك وتعديل كلمة المرور وتفاصيل الحساب.';
$lang['profile_update_success'] ='تحديث الملف الشخصي بنجاح';
$lang['address_added_success'] = 'تمت إضافة العنوان بنجاح';
$lang['access_not_allowed'] = 'الوصول غير المصرح به غير مسموح به';

$lang['already_subscribed'] ='مشترك بالفعل';
$lang['subscribe_success'] = 'تم الاشتراك بنجاح';
$lang['contact_success'] ='إرسال البريد بنجاح. سوف نعود اليك قريبا.';

$lang['load_more'] ='تحميل المزيد';
$lang['loading'] ='جار التحميل';
$lang['hide'] ='إخفاء';

$lang['login_register'] = 'تسجيل الدخول / تسجيل';
$lang['confirm_address'] = 'تأكيد العنوان';
$lang['review_order'] = 'مراجعة الطلب';
$lang['payment-method'] = 'طريقة الدفع';
$lang['email_id'] ='البريد الإلكتروني';
$lang['how_do_know_about_us']='كيف تعرفت علينا';
$lang['facebook']='Facebook';
$lang['twitter']='Twitter';
$lang['search_engine']='Search Engine';
$lang['instagram']='Instagram';
$lang['snap_chat']='Snap Chat';
$lang['blog']='Blog';
$lang['friend']='Friend';
$lang['youtube']='You tube';
$lang['eurocom_shop']='Eurocom Showroom';
$lang['other']='Other';
$lang['email_subscribe_description']='أرغب في تلقي تحديثات عبر البريد الإلكتروني من موقع eurocom.com';
$lang['delivery_address_sub'] = 'سنقوم بالتوصيل إلى عنوانك هنا';
$lang['default_address'] = 'العنوان الافتراضي';
$lang['continue'] = 'استمر';
$lang['checkout_review_notes'] = 'Our delivery time depends on your selected delivery option';
$lang['proceed_to_payment'] = 'PROCEED TO PAYMENT';
$lang['choose_payment'] = 'Choose payment method';
$lang['promocode_text'] = 'Do you have a voucher/coupon code?';
$lang['or'] = 'OR';