<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order_model extends CI_Model
{

    public function update_order($set, $where, $isjson = false, $table = 'orders')
    {
        $set = escape_array($set);

        if ($isjson == true) {
            $field = array_keys($set); // active_status
            $current_status = $set[$field[0]]; //processed
            $res = fetch_details($where, $table, '*');
            $priority_status = [
                'received' => 0,
                'processed' => 1,
                'shipped' => 2,
                'delivered' => 3,
                'cancelled' => 4,
                'returned' => 5,
                'awaiting'=>6,
                'payment_cancelled' =>7,
                'payment_failed' =>9
            ];
            if (count($res) >= 1) {
                $i = 0;
                foreach ($res  as $row) {
                    $set = array();
                    $temp = array();
                    $active_status = array();
                    $active_status[$i] = json_decode($row['status'], 1);
                    $current_selected_status = end($active_status[$i]);
                    $temp = $active_status[$i];
                    $cnt = count($temp);
                    $currTime = date('Y-m-d H:i:s');
                    $min_value = (!empty($temp)) ? $priority_status[$current_selected_status[0]] : -1;
                    $max_value = $priority_status[$current_status];
                    if ($current_status == 'returned'  || $current_status == 'cancelled') {
                        $temp[$cnt] = [$current_status, $currTime];
                    }
                    elseif ($current_status == 'awaiting'  || $current_status == 'payment_cancelled' || $current_status == 'payment_failed') {
                        $temp[$cnt] = [$current_status, $currTime];
                    } else {
                        foreach ($priority_status  as $key => $value) {
                            if ($value > $min_value && $value <= $max_value) {
                                $temp[$cnt] = [$key, $currTime];
                            }
                            ++$cnt;
                        }
                    }
                    $set = [$field[0] => json_encode(array_values($temp))];
                    $this->db->trans_start();
                    $this->db->set($set)->where(['id' => $row['id']])->update($table);
                    $this->db->trans_complete();
                    $response = FALSE;
                    if ($this->db->trans_status() === TRUE) {
                        $response = TRUE;
                    }
                    ++$i;
                }
                return $response;
            }
        } else {
            $this->db->trans_start();
            $this->db->set($set)->where($where)->update($table);
            $this->db->trans_complete();
            $response = FALSE;
            if ($this->db->trans_status() === TRUE) {
                $response = TRUE;
            }
            return $response;
        }
    }

    public function update_order_item($id, $status)
    {
        $res = validate_order_status($id, $status);
        if ($res['error']) {
            $response['error'] = (isset($res['return_request_flag'])) ? false : true;
            $response['message'] = $res['message'];
            $response['data'] = $res['data'];
            return $response;
        }

        $order_item_details = fetch_details(['id' => $id], 'order_items', 'order_id');
        $order_details =  fetch_orders($order_item_details[0]['order_id']);
        if (!empty($order_details) && !empty($order_item_details)) {

            $order_details = $order_details['order_data'];
            $order_items_details = $order_details[0]['order_items'];
            $key = array_search($id, array_column($order_items_details, 'id'));
            $order_id = $order_details[0]['id'];
            $user_id = $order_details[0]['user_id'];
            $order_counter = $order_items_details[$key]['order_counter'];
            $order_cancel_counter = $order_items_details[$key]['order_cancel_counter'];
            $order_return_counter = $order_items_details[$key]['order_return_counter'];
            $user_res = fetch_details(['id' => $user_id], 'users', 'fcm_id');
            $fcm_ids = array();
            if (!empty($user_res[0]['fcm_id'])) {
                $fcm_ids[0][] = $user_res[0]['fcm_id'];
            }


            if ($this->update_order(['status' => $status], ['id' => $id], true, 'order_items')) {
                $this->order_model->update_order(['active_status' => $status], ['id' => $id], false, 'order_items');
                if (($order_counter == intval($order_cancel_counter) + 1 && $status == 'cancelled') ||  ($order_counter == intval($order_return_counter) + 1 && $status == 'returned')) {
                    if ($this->update_order(['status' => $status], ['id' => $order_id], true)) {
                        $this->update_order(['active_status' => $status], ['id' => $order_id]);
                    }
                }
            }

            $response['error'] = false;
            $response['message'] = 'Status Updated Successfully';
            $response['data'] = array();
            return $response;
        }
    }
    public function get_area_by_id($id){
        return $this->db->select('name')->where('id',$id)->get('areas')->row('name'); 
    }
    public function get_country_by_id($id){
        return $this->db->select('country')->where('id',$id)->get('country')->row('country'); 
    }
    public function get_city_by_area($area_id){
        return $this->db->select('c.name')->join('areas a', 'a.city_id=c.id')->where('a.id',$area_id)->get('cities c')->row('c.name');
        
    }
    public function place_order($data)
    {
        $data = escape_array($data);

        $CI = &get_instance();
        $CI->load->model('Address_model');
       

        $response = array();
        $user = fetch_details(['id' => $data['user_id']], 'users');
        $product_variant_id = explode(',', $data['product_variant_id']);
        $quantity = explode(',', $data['quantity']);
        $otp = mt_rand(100000, 999999);
        if(!$user){
            $user[0] = array(
                'email' =>$data['email'],
                'first_name' =>$data['first_name'],
                'last_name' =>$data['last_name'],
                'mobile' => $data['mobile'],
                'area' =>$data['area_id'] ,
                'street' =>$data['street']                     
            );
        }
        $check_current_stock_status = validate_stock($product_variant_id, $quantity);

        if (isset($check_current_stock_status['error']) && $check_current_stock_status['error']) {
            return json_encode($check_current_stock_status);
        }

        /* Calculating Final Total */

        $total = 0;
        $product_variant = $this->db->select('pv.*,JSON_EXTRACT(pv.images,"$[0]") as pv_images,tax.percentage as tax_percentage,tax.title as tax_name,p.name as product_name,p.ar_name as ar_product_name,p.image as img')
            ->join('products p ', 'pv.product_id=p.id', 'left')
            ->join('categories c', 'p.category_id = c.id', 'left')
            ->join('`taxes` tax', 'tax.id = p.tax', 'LEFT')
            ->where_in('pv.id', $product_variant_id)->order_by('FIELD(pv.id,' . $data['product_variant_id'] . ')')->get('product_variants pv')->result_array();

        if (!empty($product_variant)) {

            $system_settings = get_settings('system_settings', true);
            $delivery_charge = (isset($data['delivery_charge'])) ? $data['delivery_charge'] : 0;
            $gross_total = 0;
            $cart_data = [];
            for ($i = 0; $i < count($product_variant); $i++) {
                $pv_price[$i] = ($product_variant[$i]['special_price'] > 0 && $product_variant[$i]['special_price'] != null) ? $product_variant[$i]['special_price'] :  $product_variant[$i]['price'];
                $pv_special_price[$i] = ($product_variant[$i]['special_price'] > 0 && $product_variant[$i]['special_price'] != null) ? $product_variant[$i]['special_price'] : 0;

                $subtotal[$i] = $pv_price[$i]  * $quantity[$i];
                $pro_name[$i] = $product_variant[$i]['product_name'];
                $ar_pro_name[$i] = $product_variant[$i]['ar_product_name'];
				$pro_image[$i] = $product_variant[$i]['img'];
                $variant_info = get_variants_values_by_id($product_variant[$i]['id']);
                $product_variant[$i]['variant_name'] = (isset($variant_info[0]['variant_values']) && !empty($variant_info[0]['variant_values'])) ? $variant_info[0]['variant_values'] : "";

                if(isset( $product_variant[$i]['pv_images']) && !empty( $product_variant[$i]['pv_images']))
               {
                $pv_image = explode('"',$product_variant[$i]['pv_images']);  
                $pv_image = $pv_image[1] ;   
               }
               else{
                $pv_image = $product_variant[$i]['img'];
               }
                  $product_variant[$i]['variant_image'] = $pv_image;
                $product_variant[$i]['ar_variant_name'] = (isset($variant_info[0]['ar_variant_values']) && !empty($variant_info[0]['ar_variant_values'])) ? $variant_info[0]['ar_variant_values'] : "";

                $tax_percentage[$i] = (!empty($product_variant[$i]['tax_percentage'])) ? $product_variant[$i]['tax_percentage'] : 0;
                if ($tax_percentage[$i] != NUll && $tax_percentage[$i] > 0) {
                    $tax_amount[$i] = round($subtotal[$i] *  $tax_percentage[$i] / 100, 2);
                } else {
                    $tax_amount[$i] = 0;
                    $tax_percentage[$i] = 0;
                }
                $gross_total += $subtotal[$i] + $tax_amount[$i];
                $total += $subtotal[$i] + $tax_amount[$i];
                $total = round($total, 2);
                $gross_total  = round($gross_total, 2);

                array_push($cart_data, array(
                    'name' => $pro_name[$i],
                    'ar_name' => $ar_pro_name[$i],
                    'variant_name' => $product_variant[$i]['variant_name'],
                    'ar_variant_name' => $product_variant[$i]['ar_variant_name'],
                    'tax_amount' => $tax_amount[$i],
                    'qty' => $quantity[$i],
					'image'=>$pro_image[$i],
                    'price' => $pv_price[$i],
                    'sub_total' => $subtotal[$i],
                ));
            }
            $system_settings = get_settings('system_settings', true);

            /* Calculating Promo Discount */
            if (isset($data['promo_code']) && !empty($data['promo_code'])) {

                $promo_code = validate_promo_code($data['promo_code'], $data['user_id'], $data['final_total']);

                if ($promo_code['error'] == false) {

                    if ($promo_code['data'][0]['discount_type'] == 'percentage') {
                        $promo_code_discount =  floatval($total  * $promo_code['data'][0]['discount'] / 100);
                    } else {
                        $promo_code_discount = floatval($total - $promo_code['data'][0]['discount']);
                    }
                    if ($promo_code_discount <= $promo_code['data'][0]['max_discount_amount']) {
                        $total = floatval($total) - $promo_code_discount;
                    } else {
                        $total = floatval($total) - $promo_code['data'][0]['max_discount_amount'];
                        $promo_code_discount = $promo_code['data'][0]['max_discount_amount'];
                    }
                } else {
                    return $promo_code;
                }
            }
            else{
                $promo_code_discount =0;
            }

            $final_total = $total + $delivery_charge;
            $final_total = round($final_total, 2);

            /* Calculating Wallet Balance */
            $total_payable = $final_total;
            // if ($data['is_wallet_used'] == '1' && $data['wallet_balance_used'] <= $final_total) {

            //     /* function update_wallet_balance($operation,$user_id,$amount,$message="Balance Debited") */
            //     $wallet_balance = update_wallet_balance('debit', $data['user_id'], $data['wallet_balance_used'], "Used against Order Placement");
            //     if ($wallet_balance['error'] == false) {
            //         $total_payable -= $data['wallet_balance_used'];
            //         $Wallet_used = true;
            //     } else {
            //         $response['error'] = true;
            //         $response['message'] = $wallet_balance['message'];
            //         return $response;
            //     }
            // } else {
            //     if ($data['is_wallet_used'] == 1) {
            //         $response['error'] = true;
            //         $response['message'] = 'Wallet Balance should not exceed the total amount';
            //         return $response;
            //     }
            // }
            $status = (isset($data['active_status'])) ? $data['active_status'] : 'received';
            $order_data = [
                'user_id' => $data['user_id'],
                'mobile' => $data['mobile'],
                'total' => $gross_total,
                'promo_discount' => (isset($promo_code_discount) && $promo_code_discount != NULL) ? $promo_code_discount : '0',
                'total_payable' => $total_payable,
                'delivery_charge' => $delivery_charge,
                'wallet_balance' => (isset($Wallet_used) && $Wallet_used == true) ? $data['wallet_balance_used'] : '0',
                'final_total' => $final_total,
                'discount' => '0',
                'payment_method' => $data['payment_method'],
                'status' =>  json_encode(array(array($status, date("d-m-Y h:i:sa")))),
                'active_status' => $status,
                'promo_code' => (isset($data['promo_code'])) ? $data['promo_code'] : ' '
            ];
            if (isset($data['delivery_date']) && !empty($data['delivery_date']) && !empty($data['delivery_time']) && isset($data['delivery_time'])) {
                $order_data['delivery_date'] = date('Y-m-d', strtotime($data['delivery_date']));
                $order_data['delivery_time'] = $data['delivery_time'];
            }
            $baddress =array();
            $address_data = $CI->address_model->get_address('', $data['address_id'], true);
            if ($data['address_id'] >0  && !empty($address_data)) {
                $order_data['mobile'] = (!empty($address_data[0]['mobile'])) ? $address_data[0]['mobile'] : '';
             //   $order_data['latitude'] = $address_data[0]['latitude'];
               // $order_data['longitude'] = $address_data[0]['longitude'];
               // $order_data['address'] = (!empty($address_data[0]['address'])) ? $address_data[0]['address'] . ', ' : '';
               // $order_data['address'] .= (!empty($address_data[0]['landmark'])) ? $address_data[0]['landmark'] . ', ' : '';
               $baddress['name']= (!empty($address_data[0]['name'])) ? $address_data[0]['name']  : '';
               $baddress['area']= (!empty($address_data[0]['area'])) ? $address_data[0]['area']  : '';
               $baddress['street'] = (!empty($address_data[0]['street'])) ? $address_data[0]['street']  : '';
               $baddress['block'] = (!empty($address_data[0]['block'])) ? $address_data[0]['block']  : '';
              $baddress['avenue'] = (!empty($address_data[0]['avenue'])) ? $address_data[0]['avenue']  : '';
              $baddress['house'] = (!empty($address_data[0]['house'])) ? $address_data[0]['house'] : '';
              $baddress['city'] = (!empty($address_data[0]['city'])) ? $address_data[0]['city'] : '';
              $baddress['country'] = (!empty($address_data[0]['country_name'])) ? $address_data[0]['country_name'] : '';
              $baddress['mobile'] = (!empty($address_data[0]['mobile'])) ? $address_data[0]['mobile'] : '';
              $baddress['email'] = (!empty($address_data[0]['email'])) ? $address_data[0]['email'] : '';
              $baddress['type'] = (!empty($address_data[0]['type'])) ? $address_data[0]['type'] : '';
           }
            else{
                if(!empty($data['area_id'])){
                    $city = $this->get_city_by_area($data['area_id']);
                }
                else{
                    $city = '';
                }
                if(!empty($data['country_id'])){
                    $country = $this->get_country_by_id($data['country_id']);
                }
                else{
                    $country  = '';
                }
              $baddress['name'] = (!empty($data['first_name'])) ? $data['first_name'].' '.$data['last_name'] : '';
               $baddress['area'] = (!empty($data['area_id'])) ? $this->get_area_by_id($data['area_id']) : '';
               $baddress['street'] = (!empty($data['street'])) ? $data['street'] : '';
               $baddress['block']  = (!empty($data['block'])) ? $data['block']  : '';
               $baddress['avenue'] = (!empty($data['avenue'])) ?$data['avenue']  : '';
               $baddress['house']  = (!empty($data['house'])) ? $data['house'] : '';
               $baddress['city'] = $city;
              $baddress['mobile'] = (!empty($data['mobile'])) ? $data['mobile'] : '';
              $baddress['country'] = (!empty($data['country_id'])) ? $country : '';
              $baddress['alternate_mobile'] = (!empty($data['alternate_mobile'])) ? $data['alternate_mobile'] : '';
              $baddress['email'] = (!empty($data['email'])) ? $data['email'] : '';
              $baddress['type'] = (!empty($data['type'])) ? $data['type'] : '';
           }
           $order_data['address'] =json_encode($baddress);
               $invoiceaddress =array();
               $address_datab = $CI->address_model->get_address('', $data['baddress_id'], true);
               if ($data['baddress_id'] >0 && !empty($address_datab)) {
                $invoiceaddress['name']= (!empty($address_datab[0]['name'])) ? $address_datab[0]['name']  : '';
                $invoiceaddress['area']= (!empty($address_datab[0]['area'])) ? $address_datab[0]['area']  : '';
                $invoiceaddress['street'] = (!empty($address_datab[0]['street'])) ? $address_datab[0]['street']  : '';
                $invoiceaddress['block'] = (!empty($address_datab[0]['block'])) ? $address_datab[0]['block']  : '';
                $invoiceaddress['avenue'] = (!empty($address_datab[0]['avenue'])) ? $address_datab[0]['avenue']  : '';
                $invoiceaddress['house'] = (!empty($address_datab[0]['house'])) ? $address_datab[0]['house'] : '';
                $invoiceaddress['city'] = (!empty($address_datab[0]['city'])) ? $address_datab[0]['city'] : '';
                $invoiceaddress['mobile'] = (!empty($address_datab[0]['mobile'])) ? $address_datab[0]['mobile'] : '';
                $invoiceaddress['alternate_mobile'] = (!empty($address_datab[0]['alternate_mobile'])) ? $address_datab[0]['alternate_mobile'] : '';
                $baddress['country'] = (!empty($address_data[0]['country_name'])) ? $address_data[0]['country_name'] : '';
                $invoiceaddress['email'] = (!empty($address_datab[0]['email'])) ? $address_datab[0]['email'] : '';
                $invoiceaddress['type'] = (!empty($address_datab[0]['type'])) ? $address_datab[0]['type'] : '';
               }
               else{
                if($data['billingaddress']==1){
                    if(!empty($data['barea_id'])){
                        $bcity =$this->get_city_by_area($data['barea_id']);
                    }
                    else{
                        $bcity = '';
                    }
                    if(!empty($data['bcountry_id'])){
                        $bcountry = $this->get_country_by_id($data['bcountry_id']);
                    }
                    else{
                        $bcountry  = '';
                    }
                    $invoiceaddress['name'] = (!empty($data['bfname'])) ? $data['bfname'].' '.$data['blname'] : '';
                    $invoiceaddress['area'] = (!empty($data['barea_id'])) ? $this->get_area_by_id($data['barea_id']) : '';
                    $invoiceaddress['street'] = (!empty($data['bstreet'])) ? $data['bstreet'] : '';
                    $invoiceaddress['block']  = (!empty($data['bblock'])) ? $data['bblock']  : '';
                    $invoiceaddress['avenue'] = (!empty($data['bavenue'])) ?$data['bavenue']  : '';
                    $invoiceaddress['house']  = (!empty($data['bhouse'])) ? $data['bhouse'] : '';
                    $invoiceaddress['city'] = $city;
                    $invoiceaddress['mobile'] = (!empty($data['bmobile'])) ? $data['bmobile'] : '';
                    $invoiceaddress['alternate_mobile'] = (!empty($data['alternate_mobile'])) ? $data['alternate_mobile'] : '';
                    $invoiceaddress['country'] = (!empty($data['bcountry_id'])) ? $bcountry : '';
                    $invoiceaddress['email'] = (!empty($data['bemail'])) ? $data['bemail'] : '';
                    $invoiceaddress['type'] = (!empty($data['btype'])) ? $data['btype'] : '';
                }
                else{
                    $invoiceaddress  = $baddress;
                }
               
            }
            $order_data['invoiceaddress'] =json_encode($invoiceaddress);
            if ($system_settings['is_delivery_boy_otp_setting_on'] == '1') {
                $order_data['otp'] = $otp;
            } else {
                $order_data['otp'] = 0;
            }

            $this->db->insert('orders', $order_data);
            $last_order_id = $this->db->insert_id();

            for ($i = 0; $i < count($product_variant); $i++) {
                $product_variant_data[$i] = [
                    'user_id' => $data['user_id'],
                    'order_id' => $last_order_id,
                    'product_name' => $product_variant[$i]['product_name'],
                    'ar_product_name' => $product_variant[$i]['ar_product_name'],
                    'variant_name' => $product_variant[$i]['variant_name'],
                    'ar_variant_name' => $product_variant[$i]['ar_variant_name'],
                    'variant_image' =>  $product_variant[$i]['variant_image'],
                    'product_variant_id' => $product_variant[$i]['id'],
                    'quantity' => $quantity[$i],
                    'price' => $pv_price[$i],
                    'special_price' => $pv_special_price[$i],
                    'tax_percent' => $tax_percentage[$i],
                    'tax_amount' => $tax_amount[$i],
                    'sub_total' => $subtotal[$i],
                    'status' =>  json_encode(array(array($status, date("d-m-Y h:i:sa")))),
                    'active_status' => $status,
                    
                ];

                $this->db->insert('order_items', $product_variant_data[$i]);
            }
            $product_variant_ids = explode(',', $data['product_variant_id']);

            $qtns = explode(',', $data['quantity']);
            if(trim(strtoupper($data['payment_method']))=='COD')
                update_stock($product_variant_ids, $qtns);

            $overall_total = array(
                'total_amount' => array_sum($subtotal),
                'delivery_charge' => $delivery_charge,
                'tax_amount' => array_sum($tax_amount),
                'tax_percentage' => array_sum($tax_percentage),
                'discount' =>  $order_data['promo_discount'],
                'wallet' =>  $order_data['wallet_balance'],
                'final_total' =>  $order_data['final_total'],
                'otp' => $otp,
                'address' => (isset($order_data['address'])) ? $order_data['address'] : '',
                'payment_method' => $data['payment_method']
            );
            if (trim(strtoupper($data['payment_method'])) == 'COD' ) {
                $overall_order_data = array(
                    'cart_data' => $cart_data,
                    'order_data' => $overall_total,
                    'subject' => 'Order received successfully',
                    'user_data' => $user[0],
                    'system_settings' => $system_settings,
                    'user_msg' => 'Hello, Dear ' . ucfirst($user[0]['first_name'].' '.$user[0]['last_name']) . ', We have received your order successfully. Your order summaries are as followed',
                    'otp_msg' => 'Here is your OTP. Please, give it to delivery boy only while getting your order.',
                    'order_id' =>$last_order_id
                );
                $system_settings = get_settings('system_settings',true);
                if(isset($system_settings['support_email']) && !empty($system_settings['support_email'])){
                    send_mail($system_settings['support_email'], 'New order placed ID #'.$last_order_id, 'New order received for '.$system_settings['app_name'].' please process it.');
                }
                $mail_response = send_mail($invoiceaddress['email'], 'Order received successfully', $this->load->view('admin/pages/view/email-template.php', $overall_order_data, TRUE));
               
            }
            else{ 
                $mail_response = ''; 
            }
            if($data['user_id'] >0){
                $temp_user_id = $data['user_id'];
            }
            else
            $temp_user_id = 0;

            $data['user_id'] = $this->session->userdata('cartsession');
            if(trim(strtoupper($data['payment_method']))=='COD') 
                $this->cart_model->remove_from_cart($data);
            $data['user_id'] = $temp_user_id;
            $user_balance = fetch_details(['id' => $data['user_id']], 'users','balance');

            $response['error'] = false;
            $response['message'] = $this->lang->line('order_place_success');
            $response['order_id'] = $last_order_id;
            $response['order_item_data'] = $product_variant_data;
            $response['balance'] = $user_balance;
            return $response;
        } else {
            $user_balance = fetch_details(['id' => $data['user_id']], 'users','balance');
            
            $response['error'] = true;
            $response['message'] = $this->lang->line('order_place_success');
            $response['balance'] = $user_balance;
            return $response;
        }
    }

    public function get_order_details($where = NULL, $status = false)
    {
        $res = $this->db->select('oi.*,oi.id as order_item_id,p.*,v.product_id,o.*,o.id as order_id,o.total as order_total,o.wallet_balance,oi.active_status as oi_active_status,u.email,u.username as uname,o.status as order_status,p.name as pname,p.type,(SELECT username FROM users db where db.id=o.delivery_boy_id ) as delivery_boy,(SELECT status FROM orders o where o.id=oi.order_id  ) as order_status ')
            ->join('product_variants v ', ' oi.product_variant_id = v.id', 'left')
            ->join('products p ', ' p.id = v.product_id ', 'left')
            ->join('users u ', ' u.id = oi.user_id', 'left')
            ->join('orders o ', 'o.id=oi.order_id', 'left');
        if (isset($where) && $where != NULL) {
            $res->where($where);
            if ($status == true) {
                $res->group_Start()
                    ->where_not_in(' `oi`.active_status ', array('cancelled', 'returned'))
                    ->group_End();
            }
        }
        if (!isset($where) && $status == true) {
            $res->where_not_in(' `oi`.active_status ', array('cancelled', 'returned'));
        }
        $order_result = $res->get(' `order_items` oi')->result_array();
        if (!empty($order_result)) {
            for ($i = 0; $i < count($order_result); $i++) {
                $order_result[$i] = output_escaping($order_result[$i]);
            }
        }
        return $order_result;
    }

    public function get_orders_list(
        $delivery_boy_id = NULL,
        $offset = 0,
        $limit = 10,
        $sort = " o.id ",
        $order = 'ASC'
    ) {
        if (isset($_GET['offset'])) {
            $offset = $_GET['offset'];
        }
        if (isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }

        if (isset($_GET['search']) and $_GET['search'] != '') {
            $search = $_GET['search'];

            $filters = [
                'u.username' => $search,
                'db.username' => $search,
                'u.email' => $search,
                'o.id' => $search,
                'o.mobile' => $search,
                'o.address' => $search,
                'o.wallet_balance' => $search,
                'o.total' => $search,
                'o.final_total' => $search,
                'o.total_payable' => $search,
                'o.payment_method' => $search,
                'o.delivery_charge' => $search,
                'o.delivery_time' => $search,
                'o.status' => $search,
                'o.active_status' => $search,
                'date_added' => $search
            ];
        }

        $count_res = $this->db->select(' COUNT(o.id) as `total` ')
            ->join(' `users` u', 'u.id= o.user_id', 'left')
            ->join('users db ', ' db.id = o.delivery_boy_id', 'left');
        if (!empty($_GET['start_date']) && !empty($_GET['end_date'])) {

            $count_res->where(" DATE(date_added) >= DATE('" . $_GET['start_date'] . "') ");
            $count_res->where(" DATE(date_added) <= DATE('" . $_GET['end_date'] . "') ");
        }

        if (isset($filters) && !empty($filters)) {
            $this->db->group_Start();
            $count_res->or_like($filters);
            $this->db->group_End();
        }

        if (isset($delivery_boy_id)) {
            $count_res->where("o.delivery_boy_id", $delivery_boy_id);
        }

        if (isset($_GET['user_id']) && $_GET['user_id'] != null) {
            $count_res->where("o.user_id", $_GET['user_id']);
        }

        if (isset($_GET['order_status']) && !empty($_GET['order_status'])) {
            $count_res->where('active_status', $_GET['order_status']);
        }

        $product_count = $count_res->get('`orders` o')->result_array();

        foreach ($product_count as $row) {
            $total = $row['total'];
        }

        $search_res = $this->db->select(' o.* , u.username, db.username as delivery_boy ')
            ->join(' `users` u', 'u.id= o.user_id', 'left')
            ->join(' `users` db ', 'db.id = o.delivery_boy_id', 'left');

        if (!empty($_GET['start_date']) && !empty($_GET['end_date'])) {
            $search_res->where(" DATE(date_added) >= DATE('" . $_GET['start_date'] . "') ");
            $search_res->where(" DATE(date_added) <= DATE('" . $_GET['end_date'] . "') ");
        }

        if (isset($filters) && !empty($filters)) {
            $search_res->group_Start();
            $search_res->or_like($filters);
            $search_res->group_End();
        }

        if (isset($delivery_boy_id)) {
            $search_res->where("o.delivery_boy_id", $delivery_boy_id);
        }

        if (isset($_GET['user_id']) && !empty($_GET['user_id'])) {
            $search_res->where("o.user_id", $_GET['user_id']);
        }

        if (isset($_GET['order_status']) && !empty($_GET['order_status'])) {
            $search_res->where('active_status', $_GET['order_status']);
        }

        $user_details = $search_res->order_by($sort, "DESC")->limit($limit, $offset)->get('`orders` o')->result_array();

        $i = 0;
        foreach ($user_details as $row) {
            $user_details[$i]['items'] = $this->db->select('oi.*,p.name as name, u.username as uname,u.first_name as first_name,u.last_name as last_name,u.mobile as user_mobile,(SELECT status FROM orders o where o.id=oi.order_id  ) as order_status  ')
                ->join('product_variants v ', ' oi.product_variant_id = v.id', 'left')
                ->join('products p ', ' p.id = v.product_id ', 'left')
                ->join('users u ', ' u.id = oi.user_id', 'left')
                ->where('oi.order_id', $row['id'])
                ->get(' `order_items` oi  ')->result_array();
            ++$i;
        }

        $bulkData = array();
        $bulkData['total'] = $total;
        $rows = array();
        $tempRow = array();
        $tota_amount = 0;
        $final_tota_amount = 0;
        $currency_symbol = get_settings('currency');
        foreach ($user_details as $row) {
            if (!empty($row['items'])) {
                $items = $row['items'];
                $items1 = '';
                $temp = '';
                $total_amt = 0;
                foreach ($items as $item) {
                    $temp .= "<b>ID :</b>" . $item['id'] . "<b> Product Variant Id :</b> " . $item['product_variant_id'] . "<b> Name : </b>" . $item['name'] . " <b>Price : </b>" . $item['price'] . " <b>QTY : </b>" . $item['quantity'] . " <b>Subtotal : </b>" . $item['quantity'] * $item['price'] . "<br>------<br>";
                    $total_amt += $item['sub_total'];
                }

                $items1 = $temp;
                $temp = '';
                if (!empty($row['items'][0]['order_status'])) {
                    $status = json_decode($row['items'][0]['order_status'], 1);
                    foreach ($status as $st) {
                        $temp .= @$st[0] . " : " . @$st[1] . "<br>------<br>";
                    }
                }

                if (trim($row['active_status']) == 'awaiting') {
                    $active_status = '<label class="badge badge-secondary">' . $row['active_status'] . '</label>';
                }
                if (trim($row['active_status']) == 'payment_cancelled') {
                    $active_status = '<label class="badge badge-danger">Payment Cancelled</label>';
                }
                if (trim($row['active_status']) == 'payment_failed') {
                    $active_status = '<label class="badge badge-danger">Payment Failed</label>';
                }
                if ($row['active_status'] == 'received') {
                    $active_status = '<label class="badge badge-primary">' . $row['active_status'] . '</label>';
                }
                if ($row['active_status'] == 'processed') {
                    $active_status = '<label class="badge badge-info">' . $row['active_status'] . '</label>';
                }
                if ($row['active_status'] == 'shipped') {
                    $active_status = '<label class="badge badge-warning">' . $row['active_status'] . '</label>';
                }
                if ($row['active_status'] == 'delivered') {
                    $active_status = '<label class="badge badge-success">' . $row['active_status'] . '</label>';
                }
                if ($row['active_status'] == 'returned' || $row['active_status'] == 'cancelled') {
                    $active_status = '<label class="badge badge-danger">' . $row['active_status'] . '</label>';
                }
                $address = json_decode($row['address']);
                $status = $temp;
                $discounted_amount = $row['total'] * $row['items'][0]['discount'] / 100; /*  */
                $final_total = $row['total'] - $discounted_amount;
                $discount_in_rupees = $row['total'] - $final_total;
                $discount_in_rupees = floor($discount_in_rupees);
                $tempRow['id'] = $row['id'];
                $tempRow['user_id'] = $row['user_id'];
                $tempRow['name'] = isset($row['items'][0]['first_name']) ? $row['items'][0]['first_name'].'  '.$row['items'][0]['last_name'] : $address->name ;
                $tempRow['mobile'] = ($row['user_mobile']!='') ? $row['user_mobile'] :  $address->mobile;
                $tempRow['delivery_charge'] = $currency_symbol . ' ' . $row['delivery_charge'];
                $tempRow['items'] = $items1;
                $tempRow['total'] = $currency_symbol . ' ' . $row['total'];
                $tota_amount += intval($row['total']);
                $tempRow['wallet_balance'] = $currency_symbol . ' ' . $row['wallet_balance'];
                $tempRow['discount'] = $currency_symbol . ' ' . $discount_in_rupees . '(' . $row['items'][0]['discount'] . '%)';
                $tempRow['qty'] = $row['items'][0]['quantity'];
                $tempRow['final_total'] = $currency_symbol . ' ' . $row['total_payable'];
                $final_tota_amount += intval($row['final_total']);
                $tempRow['deliver_by'] = $row['delivery_boy'];
                $tempRow['payment_method'] = $row['payment_method'];
                $b_address ='';
                $b_address .= ($address->house !='') ? $address->house.', ' : '';
                $b_address .= ($address->avenue !='') ? $address->avenue.', ' : '';
                $b_address .= ($address->block !='') ? $address->block.', ' : '';
                $b_address .= ($address->street !='') ? $address->street.', ' : '';
                $b_address .= ($address->area !='') ? $address->area.', ' : '';
                $b_address .= ($address->city !='') ? $address->city.', ' : '';
                $i_address = json_decode($row['invoiceaddress']);
                $invoice_address ='';
                $invoice_address .= ($i_address->house !='') ? $i_address->house.', ' : '';
                $invoice_address .= ($i_address->avenue !='') ? $i_address->avenue.', ' : '';
                $invoice_address .= ($i_address->block !='') ? $i_address->block.', ' : '';
                $invoice_address .= ($i_address->street !='') ? $i_address->street.', ' : '';
                $invoice_address .= ($i_address->area !='') ? $i_address->area.', ' : '';
                $invoice_address .= ($i_address->city !='') ? $i_address->city.', ' : '';

                $tempRow['address'] =  '<strong>'.$address->name.'</strong><br>
               '.$address->type.'<br>
                '.$b_address .'<br>
              '.$address->mobile.'<br>
              '.$address->email.'<br>';
              $tempRow['delivery_address'] =  '<strong>'.$i_address->name.'</strong><br>
               '.$i_address->type.'<br>
                '.$invoice_address
                 .'<br>
              '.$i_address->mobile.'<br>
              '.$i_address->email.'<br>';
                $tempRow['delivery_date'] = $row['delivery_date'];
                $tempRow['delivery_time'] = $row['delivery_time'];
                $tempRow['status'] = $status;
                $tempRow['active_status'] = $active_status;
                $tempRow['date_added'] = date('d-m-Y', strtotime($row['date_added']));
                $operate = '<a href=' . base_url('admin/orders/edit_orders') . '?edit_id=' . $row['id'] . '" class="btn btn-primary btn-xs mr-1 mb-1" title="View" ><i class="fa fa-eye"></i></a>';
                if (!$this->ion_auth->is_delivery_boy()) {
                    $operate = '<a href=' . base_url('admin/orders/edit_orders') . '?edit_id=' . $row['id'] . ' class="btn btn-primary btn-xs mr-1 mb-1" title="View" ><i class="fa fa-eye"></i></a>';
                    $operate .= '<a href="javascript:void(0)" class="delete-orders btn btn-danger btn-xs mr-1 mb-1" data-id=' . $row['id'] . ' title="Delete" ><i class="fa fa-trash"></i></a>';
                    $operate .= '<a href="' . base_url() . 'admin/invoice?edit_id=' . $row['id'] . '" class="btn btn-info btn-xs mr-1 mb-1" title="Invoice" ><i class="fa fa-file"></i></a>';
                } else {
                    $operate = '<a href=' . base_url('delivery_boy/orders/edit_orders') . '?edit_id=' . $row['id'] . ' class="btn btn-primary btn-xs mr-1 mb-1" title="View"><i class="fa fa-eye"></i></a>';
                }
                $tempRow['operate'] = $operate;
                $rows[] = $tempRow;
            }
        }
        if (!empty($user_details)) {
            $tempRow['id'] = '-';
            $tempRow['user_id'] = '-';
            $tempRow['name'] = '-';
            $tempRow['mobile'] = '-';
            $tempRow['delivery_charge'] = '-';
            $tempRow['items'] = '-';
            $tempRow['total'] = '<span class="badge badge-danger">' . $currency_symbol . ' ' . $tota_amount . '</span>';
            $tempRow['wallet_balance'] = '-';
            $tempRow['discount'] = '-';
            $tempRow['qty'] = '-';
            $tempRow['final_total'] = '<span class="badge badge-danger">' . $currency_symbol . ' ' . $final_tota_amount . '</span>';
            $tempRow['deliver_by'] = '-';
            $tempRow['payment_method'] = '-';
            $tempRow['address'] = '-';
            $tempRow['delivery_time'] = '-';
            $tempRow['status'] = '-';
            $tempRow['active_status'] = '-';
            $tempRow['wallet_balance'] = '-';
            $tempRow['date_added'] = '-';
            $tempRow['operate'] = '-';
            array_push($rows, $tempRow);
        }
        $bulkData['rows'] = $rows;
        print_r(json_encode($bulkData));
    }


    function get_order_items($order_id){
        $order_items = $this->db->select('id,product_name as name,ar_product_name as ar_name,tax_amount,quantity as qty,variant_image as image,price,sub_total,product_variant_id,variant_name,ar_variant_name') ->where('order_id', $order_id)
                ->get(' `order_items`')->result_array();
        return $order_items;
    }
}
