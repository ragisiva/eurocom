<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(['ion_auth', 'form_validation']);
        $this->load->helper(['url', 'language', 'function_helper']);
    }
    function getBrand($brand_name){

        $query = $this->db->query("select * from brands where name='".$brand_name."'");
        if($query->num_rows() > 0){
                $row = $query->row();
                $brand_id = $row->id;          
             
        }
        else{
            $brand_data = array(
                'name' =>$brand_name,
                'parent_id' =>0,
                'slug'=> create_unique_slug($brand_name, 'brands'),
                'status' =>1                
            );
            $this->db->insert('brands', $brand_data);
            $brand_id =$this->db->insert_id(); 
            
        }
        return $brand_id;
    }

    function getCategory($main_category,$sub_category1,$sub_category2){
        $sub_category_id2 = $main_category_id = $sub_category_id1 = 0;
        //main category 
        if($main_category !=''){
            $query = $this->db->query("select * from categories where name='".$main_category."'");
            if($query->num_rows() > 0){    
                $m_row = $query->row();            
               $main_category_id = $m_row->id;                     
            }
            else{
                $main_cat_data = array(
                    'name' =>$main_category,
                    'slug' => create_unique_slug($main_category, 'categories'),
                    'parent_id' =>0,
                    'status' =>1
                );
                $this->db->insert('categories', $main_cat_data);
                $main_category_id =$this->db->insert_id(); 
            }
        }
        //subcategory 1
        if($sub_category1 !=''){
            $query1 = $this->db->query("select * from categories where name='".$sub_category1."' and parent_id=".$main_category_id."");
            if($query1->num_rows() > 0){     
                $s1_row = $query->row();             
               $sub_category_id1 = $s1_row->id;                     
            }
            else{
                $sub_category1_data = array(
                    'name' =>$sub_category1,
                    'slug' => create_unique_slug($sub_category1, 'categories'),
                    'parent_id' =>$main_category_id,
                    'status' =>1
                );
                $this->db->insert('categories', $sub_category1_data);
                $sub_category_id1 =$this->db->insert_id(); 
            }
        }
        //subcategory 2
        if($sub_category2 !=''){
            $query2 = $this->db->query("select * from categories where name='".$sub_category2."' and parent_id=".$sub_category_id1."");
            if($query2->num_rows() > 0){  
                $s2_row = $query->row();              
               $sub_category_id2 =  $s2_row->id    ;                 
            }
            else{
                $sub_category2_data = array(
                    'name' =>$sub_category2,
                    'slug' => create_unique_slug($sub_category2, 'categories'),
                    'parent_id' =>$sub_category_id1,
                    'status' =>1
                );
                $this->db->insert('categories', $sub_category2_data);
                $sub_category_id2 =$this->db->insert_id(); 
            }
        }
       if($sub_category_id2 >0){
        return $sub_category_id2;
       }
       elseif($sub_category_id1 >0){
        return $sub_category_id1;
       }
       elseif($main_category_id >0){
        return $main_category_id;
       }
    }
   function addProduct($product_data, $product_attr_data,$pro_variance_data){
            $this->db->insert('products', $product_data);
            $p_id = $this->db->insert_id();
            $product_attr_data['product_id']= $p_id;
            $this->db->insert('product_attributes', $product_attr_data);
            $pro_variance_data['product_id']= $p_id;
            $this->db->insert('product_variants', $pro_variance_data);
            return $p_id ;
   }
}