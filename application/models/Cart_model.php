<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Cart_model extends CI_Model
{
    function add_to_cart($data)
    {
        $data = escape_array($data);
       $product_variant_id = explode(',', $data['product_variant_id']);
        $qty = explode(',', $data['qty']);

        $check_current_stock_status = validate_stock($product_variant_id, $qty );
        if (!empty($check_current_stock_status) && $check_current_stock_status['error'] == true) {
            $check_current_stock_status['csrfName'] = $this->security->get_csrf_token_name();
            $check_current_stock_status['csrfHash'] = $this->security->get_csrf_hash();
            print_r(json_encode($check_current_stock_status));
            return true;
        }

        for ($i = 0; $i < count($product_variant_id); $i++) {
            $cart_data = [
                'user_id' => $data['user_id'],
                'customer_id' => $data['customer_id'],
                'product_variant_id' => $product_variant_id[$i],
                'qty' => $qty[$i],
                'is_saved_for_later' => (isset($data['is_saved_for_later']) && !empty($data['is_saved_for_later']) && $data['is_saved_for_later'] == '1') ? $data['is_saved_for_later'] : '0',
            ];

            if ($this->db->select('*')->where(['user_id' => $data['user_id'], 'product_variant_id' => $product_variant_id[$i]])->get('cart')->num_rows() > 0) {
                $this->db->set($cart_data)->where(['user_id' => $data['user_id'], 'product_variant_id' => $product_variant_id[$i]])->update('cart');
            } else {
                $this->db->insert('cart', $cart_data);
            }
        }
        return false;
    }

    function remove_from_cart($data)
    {
        if (isset($data['user_id']) && !empty($data['user_id'])) {
            $this->db->where('user_id', $data['user_id']);
            if (isset($data['product_variant_id'])) {
                $product_variant_id = explode(',', $data['product_variant_id']);
                $this->db->where_in('product_variant_id', $product_variant_id);
            }
            return $this->db->delete('cart');
        } else {
            return false;
        }
    }

    function get_user_cart($user_id, $is_saved_for_later = 0, $product_variant_id = '')
    {
        $q = $this->db->join('product_variants pv', 'pv.id=c.product_variant_id')
            ->join('products p', 'p.id=pv.product_id')
            ->where(['user_id' => $user_id, 'qty !=' => '0', 'is_saved_for_later' => $is_saved_for_later]);
        if (!empty($product_variant_id)) {
            $q->where('c.product_variant_id', $product_variant_id);
        }
        $res =  $q->select('c.*,p.name,p.slug,p.ar_name,p.id,p.image,p.short_description,p.ar_short_description,p.minimum_order_quantity,p.quantity_step_size,p.total_allowed_quantity,pv.price,pv.special_price,pv.id as product_variant_id, JSON_EXTRACT(pv.images,"$[0]") as pv_images')->order_by('c.id', 'DESC')->get('cart c')->result_array();
        if (!empty($res)) {
            $res = array_map(function ($d) {
                $variants = get_variants_values_by_pid($d['id'],[1],$d['product_variant_id']);
                $d['variant_values'] = str_replace(',',' ',output_escaping($variants[0]['variant_values']));
                $d['ar_variant_values'] = str_replace(',',' ',output_escaping($variants[0]['ar_variant_values']));
                $d['minimum_order_quantity'] =  isset($d['minimum_order_quantity']) && !empty($d['minimum_order_quantity'])?$d['minimum_order_quantity']:1;
                $d['quantity_step_size'] =  isset($d['quantity_step_size']) && !empty($d['quantity_step_size'])?$d['quantity_step_size']:1;
                $d['total_allowed_quantity'] =  isset($d['total_allowed_quantity']) && !empty($d['total_allowed_quantity'])?$d['total_allowed_quantity']:'';
                $d['product_variants'] = get_variants_values_by_id($d['product_variant_id']);
                
                return $d;
            }, $res);
        }
        return $res;
    }

    function get_cart_by_customer($user_ID){
        return $this->db->where('customer_id',$user_ID)->get('cart')->num_rows();
    }
    function update_cart_by_customer($user_ID,$cart_session_id){
        if($this->db->where('customer_id',$user_ID)->get('cart')->num_rows() >0){
            $customer_cart = $this->db->where('customer_id',$user_ID)->get('cart')->result_array();
            $session_cart= $this->db->where('user_id',$cart_session_id)->get('cart')->result_array();
            //customer products 
            $customer_cart_products  = array_column($customer_cart,'product_variant_id','id'); 
            $customer_cart_qtys  = array_column($customer_cart,'qty','id'); 
            $customer_cart_ses  = array_column($customer_cart,'user_id','id'); 
            $session_cart_qtys  = array_column( $session_cart,'qty','id'); 
            $session_cart_products =  array_column($session_cart,'product_variant_id','id'); 
            $this->session->unset_userdata('cartsession');
            
            foreach( $customer_cart_products as $cart_id => $product_v_id){
                $ses_id = $customer_cart_ses[$cart_id];
                if(in_array($product_v_id,$session_cart_products) ){                    
                    $s_cart_id = array_search($product_v_id,$session_cart_products); 
                    $c_qty = $session_cart_qtys[$s_cart_id] + $customer_cart_qtys[$cart_id];
                    $this->db->where('id',$s_cart_id)->delete('cart');
                    $this->db->set(['qty'=>$c_qty])->where(['id' => $cart_id])->update('cart');
                }
            }
            $this->session->set_userdata('cartsession',$ses_id);
            $this->db->set(['user_id'=> $ses_id])->where(['customer_id' => $user_ID])->update('cart');
            $this->db->set(['user_id'=> $ses_id,'customer_id' => $user_ID])->where(['user_id'=> $cart_session_id])->update('cart');
        }
    }
    function update_cart_customer($user_ID,$cart_session_id){
        if($this->db->where('user_id',$cart_session_id)->get('cart')->num_rows() >0){
           $this->db->set(['customer_id'=>$user_ID])->where(['user_id' => $cart_session_id])->update('cart');
        }
    }
}
