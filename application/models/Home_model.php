<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Home_model extends CI_Model
{

    public function count_new_orders($type = '')
    {
        $res = $this->db->select('count(id) as counter');
        if (!empty($type) && $type != 'api') {
            if ($this->ion_auth->is_delivery_boy()) {
                $user_id = $this->session->userdata('user_id');
                $this->db->where('o.delivery_boy_id', $user_id);
            }
        }
        $res = $this->db->get('`orders` o')->result_array();
        return $res[0]['counter'];
    }

    public function count_orders_by_status($status)
    {
        $res = $this->db->select('count(id) as counter');
        $this->db->where('active_status', $status);
        $res = $this->db->get('`orders` o')->result_array();
        return $res[0]['counter'];
    }

    public function count_new_users()
    {
        $res = $this->db->select('count(u.id) as counter')->join('users_groups ug', ' ug.`user_id` = u.`id` ')
            ->where('ug.group_id=2')
            ->get('`users u`')->result_array();
        return $res[0]['counter'];
    }

    public function count_delivery_boys()
    {
        $res = $this->db->select('count(u.id) as counter')->where('ug.group_id', '3')->join('users_groups ug', 'ug.user_id=u.id')
            ->get('`users` u')->result_array();
        return $res[0]['counter'];
    }

    public function count_products()
    {
        $res = $this->db->select('count(id) as counter ')
            ->get('`products`')->result_array();
        return $res[0]['counter'];
    }

    public function count_products_stock_low_status()
    {
        $count_res = $this->db->select('*')->join('product_variants', 'product_variants.product_id = p.id');
        $count_res->or_where('p.stock  <=', '5');
        $count_res->or_where('product_variants.stock  <=', '5');
        $product_count = $count_res->get('products p')->num_rows();
        return $product_count;
    }

    public function count_products_availability_status()
    {
        $count_res = $this->db->select('*')->join('product_variants', 'product_variants.product_id = p.id');
        $count_res->or_where('p.availability ', '1');
        $count_res->or_where('product_variants.availability', '1');
        $product_count = $count_res->get('products p')->num_rows();
        return $product_count;
    }
}
